#include<iostream>

using namespace std;



void main() {
	setlocale(LC_ALL, "rus");
	
	/*1. ������� �� ����� �������������� �� ��������� ������������ X x Y x Z.*/
	
	// ���

	int size;
	cout << "������� ������� ���������������\n";
	cin >> size;

	for (int i = 0; i <= size * 2; ++i) {
		for (int j = 0; j <= size * 2; ++j) {
//			         ���� �����.
			if (j >= size && i == 0) {
				cout << '*';
				continue;
			}
//                       ���. ����.        ����. ����.           ����.          ����
			else if ((j == size - i || j == size * 2 - i || j == size * 2) && i < size) {
				cout << '*';
				continue;
			}
//          ��������           �����.                    ����� ������
			else if ((j <= size && i == size) || (j == size * 2 && i == size)) {
				cout << '*';
				continue;
			}
//                   ���. ����.  ����. ����.               ����.                ���
			else if ((j == 0 || j == size || j == (size * 2) - (i - size)) && i > size) {
				cout << '*';
				continue;
			}
//                          ��� �����.
			else if (j <= size && i == size * 2) {
				cout << '*';
				continue;
			}
			cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	
	// �������������� (������������ ������ ��� �������)
	
	int X, Y, Z;;
	cout << "������� ������� ���������������: X, Y, Z\n";
	cin >> X >> Y >> Z;

	for (int i = 0; i <= Y + Z - 1; ++i) {
		for (int j = 0; j <= X + Z - 1; ++j) {
//			����     �����.
			if (j > Z && i == 0) {
				cout << '*';
				continue;
			}
//          ����       ���.����.         ����.����.
			else if ((j == Z - i || j == X + Z - 1 - i) && i < Z) {
				cout << '*';
				continue;
			}
//                    �����. ��������            ������.      ����
			else if ((j < X && i == Z) || (j == X + Z - 1 && i < Y)) {
				cout << '*';
				continue;
			}
//          ���           ���. � ����. ������.                                ����.
			else if ((((j == 0 || j == X - 1) && i > Z) || (j == X + Z - 1 - i + Y - 1 && i > Y - 1))) {
				cout << '*';
				continue;
			}
//          ���      �����.
			else if (j < X && i == Y + Z - 1) {
				cout << '*';
				continue;
			}
			cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");
	
	// 3D

	cout << "������� ������� ���������������\n";
	cin >> X >> Y >> Z;

	for (int i = 0; i <= Y + Z - 1; ++i) {
		for (int j = 0; j <= X + Z - 1; ++j) {
//          ����    �����.
			if (j > Z && i == 0) {
				cout << '*';
				continue;
			}
//          ����       ���. ����.       ����. ����.
			else if ((j == Z - i || j == X + Z - 1 - i) && i < Z) {
				cout << '*';
				continue;
			}
//                 �����. ���� ��������      ������ ������� ������.    ����� �����. ������.    �����. ��� ��������
			else if ((j < X && i == Z) || (j == X + Z - 1 && i < Y) || (j == Z && i < Y) || (j >= Z && i == Y - 1)) {
				cout << '*';
				continue;
			}
//				������� ���.����.  ����. �����. ����.                ��� ���. ����.                       ��� ������ ����.
			else if ((((j == 0 || j == X - 1) && i > Z) || (j == Z + Y - 1 - i && i > Y - 1) || (j == Z + Y + X - 1 - i && i > Y - 1))) {
				cout << '*';
				continue;
			}
//          ���              �����.
			else if (j < X - 1 && i == Y + Z - 1) {
				cout << '*';
				continue;
			}
			cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");
	

	/*2. � ����� � ���������� �������� 15 ����� �����. ���������� ����� ����� ������� ����������� ������� �����. �� ����� ������� ��������� ������������ ����� ������� � ���������� ����� ���� �����, �
	�������� ������� ��������.*/
	
	int num1, num2;
	int chain, chainMax = 0, startMax = 0;
	int start, i;
	int previous;
	bool checkPrevious;
	int repeat = 0;

	cout << "������� 15 �����\n";

	for (i = 1; i <= 15; previous = num2, i++) {
		if (i != 1) {
			if (num1 > num2) {
				chain--;
			}
		}
		checkPrevious = false;
		chain = 2;
		cin >> num1;

		if (i == 1) {
			chain = 1;
			previous = num1--;
		}
		else if (i != 1) {
			if (num1 >= previous) {
				checkPrevious = true;
				chain++;
			}
		}

		cin >> num2;
		i++;
		repeat++;
		for (start = checkPrevious == true ? i - 2 : i - 1; num1 <= num2; i++) {
			if (i >= 15) {
				break;
			}
			chain++;
			if (chain > chainMax) {
				chainMax = chain;
				startMax = start;
			}
			if (repeat >= 3) {
				repeat = -10;
				chainMax--;
			}
			num1 = num2;
			cin >> num2;
		}
	}
	cout << "\n����� ������ ������� ����������� �����:\n\t\t\t����� �������: " << chainMax << " �����\n\t\t\t� ������� ����� �������: " << startMax << endl << endl;

	system("pause");
	system("cls");

	/*3. ������������ ������ ���� (�� ����������� ����, �����, ���). ���������� ���� ������, ��������������� ��������� ����. ��� ��������� ��������� ������� �������� �� ����, ��� 01.01.01 � ��� �����������.*/

	int day, month, year;
	bool flag, checkDate = false;

	do {
		if (!checkDate) {
			cout << "������� ����, �����, ���\n";
			checkDate = true;
		}
		else {
			system("cls");
			cout << "������...\n��������� ����: ����, �����, ���\n";
		}
		cin >> day >> month >> year;
		switch (month) {
		case 4:
		case 6:
		case 9:
		case 11:
			(day == 31) ? flag = false : flag = true;
			break;
		case 2:
			(day == 30 || day == 31) ? flag = false : flag = true;
			if (day == 29) {
				(year % 4 == 0 && year % 100 != 0) ? flag = true : (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) ? flag = true : flag = false;
			}
			break;
		default:
			flag = true;
		}
	} while (day < 1 || day > 31 || month < 1 || month > 12 || year == 0 || flag == false);

	if (year >= 2001) {
		enum dayOfWeek { SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY };
		int sumOfDays = 0;
		for (; year >= 2001; month = 12, --year) {
			for (; month >= 1; day = 31, --month) {
				for (; day >= 1; sumOfDays++, --day) {
					if (day == 31) {
						switch (month) {
						case 4:
						case 6:
						case 9:
						case 11:
							day = 30;
							break;
						case 2:
							(year % 4 == 0 && year % 100 != 0) ? day = 29 : (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) ? day = 29 : day = 28;
							break;
						}
					}
				}
			}
		}
		int dayOfWeek = sumOfDays % 7;
		switch (dayOfWeek) {
		case MONDAY:
			cout << "�����������\n";
			break;
		case TUESDAY:
			cout << "�������\n";
			break;
		case WEDNESDAY:
			cout << "�����\n";
			break;
		case THURSDAY:
			cout << "�������\n";
			break;
		case FRIDAY:
			cout << "�������\n";
			break;
		case SATURDAY:
			cout << "�������\n";
			break;
		case SUNDAY:
			cout << "�����������\n";
			break;
		}
	}
	else {
		enum dayOfWeek { MONDAY, SUNDAY, SATURDAY, FRIDAY, THURSDAY, WEDNESDAY, TUESDAY };
		int sumOfDays = 0;
		for (; year < 2001; month = 1, ++year) {
			for (; month <= 12; day = 1, ++month) {
				for (; day <= 31; ++day) {
					if (month == 2 && day == 29) {
						bool checkYear;
						(year % 4 == 0 && year % 100 != 0) ? checkYear = true : (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) ? checkYear = true : checkYear = false;
						if (checkYear) {
							day = 32;
						}
						else {
							day = 32;
							break;
						}
					}
					bool checkMonth = false;
					if (day == 30) {
						switch (month) {
						case 4:
						case 6:
						case 9:
						case 11:
							day = 32;
							checkMonth = true;
							break;
						default:
							day = 32;
							checkMonth = true;
							sumOfDays++;
						}
					}
					sumOfDays++;
					if (checkMonth) {
						break;
					}
				}
			}
		}
		int dayOfWeek = sumOfDays % 7;
		switch (dayOfWeek) {
		case MONDAY:
			cout << "�����������\n";
			break;
		case TUESDAY:
			cout << "�������\n";
			break;
		case WEDNESDAY:
			cout << "�����\n";
			break;
		case THURSDAY:
			cout << "�������\n";
			break;
		case FRIDAY:
			cout << "�������\n";
			break;
		case SATURDAY:
			cout << "�������\n";
			break;
		case SUNDAY:
			cout << "�����������\n";
			break;
		}
	}
	cout << endl;


}