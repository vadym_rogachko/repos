#include<iostream>
#include<Time.h>

using namespace std;




void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	/*1. ���� 2 ������� ����������� M � N ��������������. ���������� ���������� � ������ ������ ����� �������� ������ ���� �������� ��� ����������.*/

	{
		const int M = 15, N = 25, SIZE = 10;
		int arr1[M]{ 2,12,1,7,5,12,8,17,4,4,3,14,5,11,15 }, arr2[N]{ 4,17,10,22,1,5,3,9,18,16,25,3,15,12,4,15,7,29,11,8,17,24,33,5,8 }, arr3[SIZE]{};

		cout << "������ ������:\n\n\n";
		for (int i = 0; i < M; ++i) {
			cout << arr1[i] << ' ';
		}

		cout << "\n\n\n������ ������:\n\n\n";
		for (int i = 0; i < N; ++i) {
			cout << arr2[i] << ' ';
		}


		bool flag = false, repeat;

		for (int i = 0, k = 0; i < M; ++i) {
			repeat = false;
			for (int j = 0; j < N; ++j) {
				if (arr1[i] == arr2[j]) {
					if (k != 0) {
						for (int l = 0; l < k; l++) {
							if (arr1[i] == arr3[l]) {
								repeat = true;
								break;
							}
						}
					}
					if (!repeat) {
						arr3[k] = arr1[i];
						++k;
						flag = true;
						break;
					}
				}
			}
		}


		if (flag) {
			cout << "\n\n\n������ ������:\n\n\n";
			for (int i = 0; i < SIZE; ++i) {
				cout << arr3[i] << ' ';
			}
			cout << endl << endl << endl;
		}
		else {
			cout << "\n\n\n� ������ � ������ �������� ���� ����� ���������\n\n";
		}
	}
	system("pause");
	system("cls");

	/*2. ���� 2 ������� ����������� M � N ��������������. ���������� ���������� � ������ ������ �� �������� ������� �������, ������� ��� �� ������ �������, ��� ����������.*/

	{
		const int M = 25, N = 15, SIZE = 10;
		int arr1[M]{ 4,17,10,22,1,5,3,9,18,16,25,3,15,12,4,15,7,29,11,8,17,24,33,5,8 }, arr2[N]{ 2,12,1,7,5,12,7,17,4,4,3,14,5,11,15 }, arr3[SIZE]{};

		cout << "������ ������:\n\n\n";
		for (int i = 0; i < M; ++i) {
			cout << arr1[i] << ' ';
		}

		cout << "\n\n\n������ ������:\n\n\n";
		for (int i = 0; i < N; ++i) {
			cout << arr2[i] << ' ';
		}


		bool flag, repeat, noNum = true;

		for (int i = 0, k = 0; i < M; ++i) {
			flag = true, repeat = false;
			for (int j = 0; j < N; ++j) {
				if (arr1[i] == arr2[j]) {
					flag = false;
					break;
				}
			}

			if (flag) {
				noNum = false;
				if (k != 0) {
					for (int l = 0; l < k; ++l) {
						if (arr1[i] == arr3[l]) {
							repeat = true;
							break;
						}
					}
				}
				if (!repeat) {
					arr3[k] = arr1[i];
					++k;
				}
			}
		}


		if (!noNum) {
			cout << "\n\n\n������ ������:\n\n\n";
			for (int i = 0; i < SIZE; ++i) {
				cout << arr3[i] << ' ';
			}
		}
		else {
			cout << "\n\n\n��� �������� ������� ������� ���� �� ������ �������";
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*3. ������� ������ �� 20 ��������� ����� � ��������� �� -10 �� 20. ���������� ������������ ���������� ������ ������ ������������� ���������, �� ����������� �� ������,
	     �� �������������� �������. ������� ��������� ��������.*/

	{
		const int SIZE = 20;
		int arr[SIZE]{};
		short counter = 0, maxInaRow = 0, firstNum = 0, maxFirstNum;
		bool plus, first = true, flag = false;

		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 31 - 10;
			cout << arr[i] << ' ';
		}


		for (int i = 0, start = i; i < SIZE; ++i) {
			plus = arr[i] > 0 ? true : false;
			if (plus) {
				++counter;
				if (counter > maxInaRow) {
					maxInaRow = counter;
					maxFirstNum = firstNum;
				}
			}
			else {
				counter = 0;
			}
			if (plus && first) {
				firstNum = i;
				first = false;
				flag = true;
			}
			else if (!plus) {
				first = true;
			}
		}


		if (flag) {
			cout << "\n\n\n������������ ���������� ������ ������ ������������� ���������: " << maxInaRow << endl << endl << endl;;
			for (int i = maxFirstNum; i < maxFirstNum + maxInaRow; ++i) {
				cout << arr[i] << ' ';
			}
			cout << endl << endl << endl;
		}
		else {
			cout << "\n\n\n� ������� ���� ������������� ���������\n\n\n";
		}
		system("pause");
		system("cls");
	}

	/*4. ������� ������ �� 10 ��������� ����� � ��������� �� -20 �� 20. ������������� ������ ���, ����� ������� ��� ��� ������������� ��������, � ����� ������������� (0 ������� �������������).*/

	{
		const int SIZE = 10;
		int arr[SIZE]{};

		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 41 - 20;
			cout << arr[i] << ' ';
		}
		cout << endl << endl << endl;


		bool plus = false;
		short index, minusNum;

		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] >= 0 && !plus) {
				plus = true;
				index = i;
				continue;
			}
			if (arr[i] < 0 && plus) {
				minusNum = arr[i];
				for (int j = i; j > index; --j) {
					arr[j] = arr[j - 1];
				}
				arr[index] = minusNum;
				plus = false;
				i = index;
			}
		}


		cout << "��������������� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << ' ';
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*5. ������� ������ �� 10 ����� �����. ����� ������, ������ �� ���� ��� 0, � ��������� �������������� ������ �������� ���������� -1.*/

	{
		const int SIZE = 10;
		int arr[SIZE]{};

		cout << "������� 10 ����� �����\n";
		for (int i = 0; i < SIZE; ++i) {
			cin >> arr[i];
		}

		cout << "\n��� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << ' ';
		}


		bool zero = false;
		short index;

		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] == 0 && !zero) {
				index = i;
				zero = true;
				continue;
			}
			if (arr[i] != 0 && zero) {
				swap(arr[index], arr[i]);
				i = index;
				zero = false;
			}
		}


		cout << "\n\n\n����� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] == 0) {
				arr[i] = -1;
			}
			cout << arr[i] << ' ';
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*6. ������� ������ �� 10 ��������� ����� � ��������� �� -20 �� 20. �������� ��� ������������� �������� �� ��������.*/

	{
		const int SIZE = 10;
		int arr[SIZE]{};

		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 41 - 20;
			cout << arr[i] << ' ';
		}


		cout << "\n\n\n����� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] < 0) {
				arr[i] = -arr[i];
			}
			cout << arr[i] << ' ';
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*7. ������� ������ �� 10 ������������ �����. ������������� ������ ���, ����� ������� ��� ��� �������� � ������������ ������, � ����� ��� ���.*/

	{
		const int SIZE = 10;
		double arr[SIZE]{};

		cout << "������� 10 ����� ������������ �����\n";
		for (int i = 0; i < SIZE; ++i) {
			cin >> arr[i];
		}

		cout << "\n��� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << '\t';
		}
		cout << endl << endl;


		double num;
		bool flag;

		for (int i = 0; i < SIZE; ++i) {
			flag = false;
			if (int(arr[i]) == arr[i]) {
				for (int j = i + 1; j < SIZE; ++j) {
					if (int(arr[j]) != arr[j]) {
						flag = true;
						num = arr[j];
						for (int k = j; k > i; --k) {
							arr[k] = arr[k - 1];
						}
						break;
					}
				}
				if (flag) {
					arr[i] = num;
				}
			}
		}


		cout << "����� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << '\t';
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*8. ��� ������ �� 10 ��������� ���� char. �������� � ��� ��� ����� �� ������� ����� (��������, �0� ���������� �� ���, �1� ���������� �� �B� � �.�.).*/

	{
		const int SIZE = 11;
		char arr[SIZE]{ '2','0','5','4','1','0','3','1','0','1','4' };


		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i];
		}


		cout << "\n\n\n����� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = int(arr[i]) + 17;
			cout << arr[i];
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*9. ������� ������ �� 10 ����� ��������� ����� � ��������� �� 0 �� 100. ����� �������, ����������� ������� � �������� ��������������� �������� �������.*/

	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int sum = 0;

		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 101;
			sum += arr[i];
			cout << arr[i] << ' ';
		}


		double midNum = double(sum) / 10;
		cout << "\n\n\n������� �������������� �������� ������� " << midNum << endl;
		int index = 0;
		double num = double(arr[index]) - midNum;
		if (num < 0) {
			num = -num;
		}

		for (int i = 1; i < SIZE; ++i) {
			double checkNum = double(arr[i]) - midNum;
			if (checkNum < 0) {
				checkNum = -checkNum;
			}
			if (checkNum < num) {
				num = checkNum;
				index = i;
			}
		}


		cout << "���������� ����� ��������, ����������� �������� � �������� ���������������      �������� ������� " << index + 1 << endl << "�������� �������� " << arr[index] << endl << endl;
	}
	system("pause");
	system("cls");

	/*10. ����������� ����������� ����� ������� �� N ���������. ����������� ������ ��������� ������������
	     (��������, ������ 0,1,2,3,4,5,6,7,8,9 ��� ����������� ������ ������ �� 3 �������� ������ ��� 7,8,9,0,1,2,3,4,5,6).*/

	{
		const int SIZE = 10;
		int arr[SIZE]{ 0,1,2,3,4,5,6,7,8,9 };
		short shift;

		do {
			system("cls");
			for (int i = 0; i < SIZE; ++i) {
				cout << arr[i] << ' ';
			}
			cout << "\n\n\n������� ���������� ��������� ��� ������ (�� 1 �� 10): ";
			cin >> shift;
		} while (shift < 1 || shift > 10);

		bool dir;
		cout << "\n�������� ����������� ������:\n\t\t\t     Any Key - ������\n\t\t\t     0\t     - �����\n";
		cin >> dir;


		if (dir) {
			for (int i = 0; i < shift; ++i) {
				short num = arr[SIZE - 1];
				for (int j = SIZE - 1; j > 0; --j) {
					arr[j] = arr[j - 1];
				}
				arr[0] = num;
			}
		}
		else {
			for (int i = 0; i < shift; ++i) {
				short num = arr[0];
				for (int j = 0; j < SIZE - 1; ++j) {
					arr[j] = arr[j + 1];
				}
				arr[SIZE - 1] = num;
			}
		}


		cout << "\n\n������ �������" << (dir ? " ������ " : " ����� ");
		cout << "�� " << shift;
		switch (shift) {
		case 1:
			cout << " �������:\n\n\n";
			break;
		case 2:
		case 3:
		case 4:
			cout << " ��������:\n\n\n";
			break;
		default :
			 cout << " ���������:\n\n\n";
		}
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << ' ';
		}
		cout << endl << endl << endl;
	}








}