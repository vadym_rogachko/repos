#include<iostream>
#include<conio.h>
#include<time.h>

using namespace std;															// � ������� ����� ��������� ��������� ����� �����


void Next() {
	system("pause");
	system("cls");
}


void main() {

	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	/*1. ��������� ��������� � �������� �������������, ���������� ���������� �� ���� ����� */
	
	cout << "��������� ��������� � �������� �������������, ���������� ���������� �� ����     �����\n\n";

	// ����� �� �����, ��������� � ���������� �� �����
	{
		int a, b;
		cout << "������� ��� �����\n";
		cin >> a >> b;
		cout << endl;
		int *ptr_a = &a, *ptr_b = &b;
		if (*ptr_a == *ptr_b) {
			cout << a << " � " << b << " �����\n\n";
		}
		else if (*ptr_a > *ptr_b) {
			cout << "���������� ����� = " << a << endl << endl;
		}
		else {
			cout << "���������� ����� = " << b << endl << endl;
		}
	}
	Next();

	// ������ ��������

	// �������� �������� ���������, � ������� ������� �����, ���������� � ���������, ������� "���������" �� ���������� � ���������� ��������� (����� �� ����� � ������� ��������� �������������)
	{
		int a, b;
		cout << "������� ��� �����\n";
		cin >> a >> b;
		cout << endl;
		int *ptr_a = &a, *ptr_b = &b, *ptr_max = nullptr;
		if (*ptr_a == *ptr_b) {
			cout << *ptr_a << " � " << *ptr_b << " �����\n\n";
		}
		else {
			ptr_max = *ptr_a > *ptr_b ? ptr_a : ptr_b;
			cout << "���������� ����� = " << *ptr_max << endl << endl;
		}
	}
	Next();

	// � ������ ��������� ������� ����� ���������� � ���������� ��������� � ������� ��������� ������ ������ ����������, ������� �������� ����������, ��������� �������� ������������� ���������� (����� ������� ���, �� �������� ������ �������� ����)
	{
		int a, b;
		cout << "������� ��� �����\n";
		cin >> a >> b;
		cout << endl;
		int *ptr_a = &a, *ptr_b = &b, *ptr_max = nullptr;
		if (*ptr_a == *ptr_b) {
			cout << *ptr_a << " � " << *ptr_b << " �����\n\n";
		}
		else {
			ptr_max = *ptr_a > *ptr_b ? &a : &b;
			cout << "���������� ����� = " << *ptr_max << endl << endl;
		}
	}
	Next();

	// ����� �� �����, ��������� �������� ������������� (���������� � ���������� �� ������)
	{
		int a, b;
		cout << "������� ��� �����\n";
		cin >> a >> b;
		cout << endl;
		int *ptr_a = &a, *ptr_b = &b;
		if (*ptr_a == *ptr_b) {
			cout << *ptr_a << " � " << *ptr_b << " �����\n\n";
		}
		else if (*ptr_a > *ptr_b) {
			cout << "���������� ����� = " << *ptr_a << endl << endl;
		}
		else {
			cout << "���������� ����� = " << *ptr_b << endl << endl;
		}
	}
	Next();

	/*2. ��������� ��������� � �������� �������������, ���������� ���� �����, ���������� � ���������� */
	
	cout << "��������� ��������� � �������� �������������, ���������� ���� �����, ���������� � ����������\n\n";

	{
		int num;
		cout << "������� �����\n";
		cin >> num;
		cout << endl;
		int *ptr_num = &num;
		// cout << num << (*ptr_num < 0 ? " ������������� �����\n\n" : " ������������ �����\n\n"); - ����� �� �����, ��������� � ���������� �� �����
		cout << *ptr_num << (*ptr_num < 0 ? " ������������� �����\n\n" : " ������������ �����\n\n"); // - ����� �� �����, ��������� � ���������� �� ������, ��������� �������� ������������� ���������
	}
	Next();

	/*3. ��������� ��������� � �������� �������������, �������� ������� �������� ���� ���������� */
	
	cout << "��������� ��������� � �������� �������������, �������� ������� �������� ����    ����������\n\n";

	{
		int a, b;
		cout << "������� ��� �����\n";
		cin >> a >> b;
		//cout << "\na = " << a << "\nb = " << b << endl;
		int *ptr_a = &a, *ptr_b = &b;
		cout << "\na = " << *ptr_a << "\nb = " << *ptr_b << endl;
		*ptr_a = *ptr_a + *ptr_b;
		*ptr_b = *ptr_a - *ptr_b;
		*ptr_a = *ptr_a - *ptr_b;
		// cout << "\na = " << a << "\nb = " << b << endl << endl; - �� �����
		cout << "\na = " << *ptr_a << "\nb = " << *ptr_b << endl << endl; // �������� ������������� ��������� (������ �� ������)
	}
	Next();

	/*4. �������� ����������� �����������, ��������� ������ �����������.*/

	cout << "\t\t\t       �����������\n";
	_getch();

	{
		double num1, num2, res;
		short action;
		while (true) {
			do {
				system("cls");
				cout << "������� ��� �����\n";
				cin >> num1 >> num2;
				do {
					system("cls");
					cout << "�������� ��������: 1. +\n\t\t   2. -\n\t\t   3. *\n\t\t   4. /\n";
					cin >> action;
				} while (action != 1 && action != 2 && action != 3 && action != 4);
				if (action == 4 && num2 == 0) {
					cout << "\n������� �� ���� ����������\n\n";
					system("pause");
				}
			} while (action == 4 && num2 == 0);
			system("cls");
			cout << endl;
			double *ptr_num1 = &num1, *ptr_num2 = &num2, *ptr_res = &res;
			switch (action) {
			case 1:
				*ptr_res = (*ptr_num1) + (*ptr_num2);
				cout << *ptr_num1 << " + " << *ptr_num2 << " = ";
				break;
			case 2:
				*ptr_res = (*ptr_num1) - (*ptr_num2);
				cout << *ptr_num1 << " - " << *ptr_num2 << " = ";
				break;
			case 3:
				*ptr_res = (*ptr_num1) * (*ptr_num2);
				cout << *ptr_num1 << " * " << *ptr_num2 << " = ";
				break;
			case 4:
				*ptr_res = (*ptr_num1) / (*ptr_num2);
				cout << *ptr_num1 << " / " << *ptr_num2 << " = ";
				break;
			}
			// cout << res << endl << endl;
			cout << *ptr_res << endl << endl;
			Next();
			cout << "��� ������ ������� Esc\n";
			int code = _getch();
			if (code == 27) {
				break;
			}
		}
	}
	Next();

	/*5. ��������� ���������� �� ������ ����� �����, ��������� ����� ��������� �������. ������������ � ��������� ���������� ���������� ��� ����������� �� �������, � ����� �������� ������������� */

	cout << "��������� ���������� �� ������ ����� �����, ��������� ����� ��������� �������.  ������������ � ��������� ���������� ���������� ��� ����������� �� �������, �    ����� �������� �������������\n\n\n";

	{
		const int SIZE = 10;
		int arr[SIZE]{};
		auto *ptr_arr = arr;
		int sum = 0;
		int *ptr_sum = &sum;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr = rand() % 9 + 1;
			cout << *ptr_arr << ' ';
			++ptr_arr;
		}
		ptr_arr = arr;
		cout << endl;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_sum += *ptr_arr;
			++ptr_arr;
		}
		// cout << "\n����� ��������� ������� = " << sum << endl << endl;
		cout << "\n����� ��������� ������� = " << *ptr_sum << endl << endl;
	}
	Next();

	/*6. ��������� ����� ����������� �� ������ ����� �����, ����������� ���� ������ � ������. ������������ � ��������� ���������� ���������� ��� ����������� �� �������, � ����� �������� ������������� */

	cout << "��������� ����� ����������� �� ������ ����� �����, ����������� ���� ������ �    ������. ������������ � ��������� ���������� ���������� ��� ����������� ��       �������, � ����� �������� �������������\n\n";

	{
		const int SIZE = 10;
		int arr1[SIZE]{}, arr2[SIZE]{};
		auto *ptr_arr1 = arr1, *ptr_arr2 = arr2;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr1 = rand() % 9 + 1;
			cout << *ptr_arr1 << ' ';
			++ptr_arr1;
		}
		ptr_arr1 = arr1;
		cout << endl;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr2 = *ptr_arr1;
			cout << *ptr_arr2 << ' ';
			++ptr_arr1, ++ptr_arr2;
		}
		cout << endl << endl;
	}
	Next();

	/*7. ��������� ���������� �� ������ ����� �����, �������� ������� ���������� ��������� ������� �� ���������������. ������������ � ��������� ���������� ���������� ��� ����������� �� �������,
	     � ����� �������� ������������� */

	cout << "��������� ���������� �� ������ ����� �����, �������� ������� ����������         ��������� ������� �� ���������������. ������������ � ��������� ����������       ���������� ��� ����������� �� �������, � ����� �������� �������������\n\n\n";

	{
		const int SIZE = 10;
		int arr[SIZE]{};
		auto *ptr_arr = arr;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr = rand() % 9 + 1;
			cout << *ptr_arr << ' ';
			++ptr_arr;
		}
		cout << endl << endl;
		ptr_arr = arr;
		for (int i = 0; i < SIZE / 2; ++i) {
			*(ptr_arr + i) = *(ptr_arr + i) + *(ptr_arr + SIZE - 1 - i);
			*(ptr_arr + SIZE - 1 - i) = *(ptr_arr + i) - *(ptr_arr + SIZE - 1 - i);
			*(ptr_arr + i) = *(ptr_arr + i) - *(ptr_arr + SIZE - 1 - i);
		}
		ptr_arr = arr;
		for (int i = 0; i < SIZE; ++i) {
			cout << *ptr_arr << ' ';
			++ptr_arr;
		}
		cout << endl << endl;
	}
	Next();

	/*8. ��������� ����� ����������� �� ������� ����� �����, ����������� ���� ������ � ������ ���, ����� �� ������ ������� �������� ���������� � �������� �������. ������������ � ��������� ���������� ����������
	     ��� ����������� �� �������, � ����� �������� ������������� */

	cout << "��������� ����� ����������� �� ������� ����� �����, ����������� ���� ������ �   ������ ���, ����� �� ������ ������� �������� ���������� � �������� �������.     ������������ � ��������� ���������� ���������� ��� ����������� �� �������, �    ����� �������� �������������\n\n\n";

	{
		const int SIZE = 10;
		int arr1[SIZE]{}, arr2[SIZE];
		auto *ptr_arr1 = arr1, *ptr_arr2 = arr2;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr1 = rand() % 9 + 1;
			cout << *ptr_arr1 << ' ';
			++ptr_arr1;
		}
		--ptr_arr1;
		// ��� ptr_arr1 = arr1 + SIZE - 1;
		cout << endl << endl;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr2 = *ptr_arr1;
			cout << *ptr_arr2 << ' ';
			++ptr_arr2, --ptr_arr1;
		}
		cout << endl << endl;
	}
	Next();

	/*9. ��������� ���������� �� ������ ����� �����, ����� ����������� � ������������ ������� �������. ������������ � ��������� ���������� ���������� ��� ����������� �� �������, � ����� �������� �������������*/

	cout << "��������� ���������� �� ������ ����� �����, ����� ����������� � ������������    ������� �������. ������������ � ��������� ���������� ���������� ��� ����������� �� �������, � ����� �������� �������������\n\n\n";

	{
		const int SIZE = 10;
		int arr[SIZE]{};
		auto *ptr_arr = arr;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr = rand() % 99 + 1;
			cout << *ptr_arr << ' ';
			++ptr_arr;
		}
		cout << endl;
		ptr_arr = arr;
		int min = *ptr_arr, max = *ptr_arr;
		int *ptr_min = &min, *ptr_max = &max;
		for (int i = 0; i < SIZE; ++i) {
			if (*ptr_arr < *ptr_min) {
				*ptr_min = *ptr_arr;
			}
			if (*ptr_arr > *ptr_max) {
				*ptr_max = *ptr_arr;
			}
			++ptr_arr;
		}
		//cout << "\n������������ ������� ������ = " << max << endl << "����������� ������� ������� = " << min << endl << endl << endl;
		cout << "\n������������ ������� ������ = " << *ptr_max << endl << "����������� ������� ������� = " << *ptr_min << endl << endl << endl;
	}
	Next();

	/*10. ��������� ���������� �� ������ ����� �����, ����������� ����������� ����� ������� �� �������� ����� ���������. ����������� ������ �������� �������������.
	      ������������ � ��������� ���������� ���������� ��� ����������� �� �������, � ����� �������� �������������*/

	cout << "��������� ���������� �� ������ ����� �����, ����������� ����������� �����       ������� �� �������� ����� ���������. ����������� ������ �������� �������������. ������������ � ��������� ���������� ���������� ��� ����������� �� �������, �    ����� �������� �������������\n\n\n";
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int *ptr_arr = arr;
		int shift, count, temp;
		int *ptr_temp = &temp;
		for (int i = 0; i < SIZE; ++i) {
			*ptr_arr = rand() % 9 + 1;
			cout << *ptr_arr << ' ';
			++ptr_arr;
		}
		cout << endl;
		ptr_arr = arr;
		cout << "\n�������� ���������� ��� ������: 1. ������\n\t\t\t\t2. �����\n";
		do {
			shift = _getch();
		} while (shift != '1' && shift != '2');
		cout << "������� ���������� ��������� ��� ������ ";
		cin >> count;
		int copyCount = count;
		if (shift == '1') {
			while (count--) {
				ptr_arr = arr + SIZE - 1;
				*ptr_temp = *ptr_arr;
				for (int i = SIZE - 1; i > 0; --i) {
					*ptr_arr = *(ptr_arr - 1);
					--ptr_arr;
				}
				*ptr_arr = *ptr_temp;
			}
		}
		else {
			while (count--) {
				ptr_arr = arr;
				*ptr_temp = *ptr_arr;
				for (int i = 0; i < SIZE - 1; ++i) {
					*ptr_arr = *(ptr_arr + 1);
					++ptr_arr;
				}
				*ptr_arr = *ptr_temp;
			}
		}
		ptr_arr = arr;
		cout << "\n������ ������� �� " << copyCount << " ����. " << (shift == '1' ? "������\n\n" : "�����\n\n");
		for (int i = 0; i < SIZE; ++i) {
			cout << *ptr_arr << ' ';
			++ptr_arr;
		}
		cout << endl << endl;
	}



}