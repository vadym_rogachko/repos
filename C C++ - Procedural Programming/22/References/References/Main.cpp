#include"Header.h"
#include<iostream>
#include<time.h>
#include<iomanip>																									

using namespace std;

void Next() {
	system("pause");
	system("cls");
}



void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	/*1. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ����� � ������������ ��� ��������� � ���� ����������-�������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ������ � ��� ������, �          ���������� ����� � ������������ ��� ��������� � ���� ����������-�������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};
		InitArray(p_arrD, size);
		ShowArray(p_arrD, size);
		cout << endl;
		int sum = 0;
		int mult = 1;
		GetSumMulti(p_arrD, size, sum, mult);
		cout << "����� ��������� = " << sum << endl;
		cout << "������������ ��������� = " << mult << endl << endl;

		delete[] p_arrD;
		p_arrD = nullptr;  
	}
	Next();

	/*2. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ���������� �������������, ������������� � ������� ��������� �������, ��������� �������� ������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ������ � ��� ������, �          ���������� ���������� �������������, ������������� � ������� ��������� �������, ��������� �������� ������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};
		InitArray(p_arrD, size, -9);
		ShowArray(p_arrD, size, 3);
		cout << endl;
		int pos = 0;
		int neg = 0;
		int zero = 0;
		GetPosNegZero(p_arrD, size, pos, neg, zero);
		cout << "���������� ������������� ��������� " << pos << endl;
		cout << "���������� ������������� ��������� " << neg << endl;
		cout << "���������� ������� ��������� " << zero << endl << endl;

		delete[] p_arrD;;
		p_arrD = nullptr;
	}
	Next();

	/*3. �������� �������, ������� �������� ��������� �� ��������� ������ � ��� ������, � ���������� ������� �������������� ��������� �������, � ����� ���������� ������ � �������� ���������, ��������� �������� ������*/
	
	// ������ ������� (��������� ������������ ������)

	{
		cout << "�������� �������, ������� �������� ��������� �� ��������� ������ � ��� ������, ����������� ������� �������������� ��������� �������, � ����� ���������� ������ ��������� ���������, ��������� �������� ������\n\n";
		int size = GetSize();
		cout << endl;
		int **pp_arrD = new int*[size];
		for (int i = 0; i < size; ++i) {
			pp_arrD[i] = new int[size] {};
		}
		InitArrayTwoDim(pp_arrD, size);
		ShowArrayTwoDim(pp_arrD, size);
		cout << endl;
		double arithMean = 0;
		int even = 0;
		int uneven = 0;
		GetArithMeanAndCountEvenUneven(pp_arrD, size, arithMean, even, uneven);
		cout << "������� �������������� = " << arithMean << endl;
		cout << "���������� ������ ��������� " << even << endl;
		cout << "��������� �������� ��������� " << uneven << endl << endl;

		for (int i = 0; i < size; ++i) {
			delete[] pp_arrD[i];
		}
		delete[] pp_arrD;
		pp_arrD = nullptr;
	}
	Next();

	// ������ ������� (������ ����������)

	{
		const int SIZE = 5;
		int *p_arrD1 = new int[SIZE] {};
		int *p_arrD2 = new int[SIZE] {};
		int *p_arrD3 = new int[SIZE] {};
		int *p_arrD4 = new int[SIZE] {};
		int *p_arrD5 = new int[SIZE] {};
		int *p_arrPtr[] = { p_arrD1, p_arrD2, p_arrD3, p_arrD4, p_arrD5 };
		cout << endl;
		InitArrayTwoDim(p_arrPtr, SIZE);
		ShowArrayTwoDim(p_arrPtr, SIZE);
		cout << endl;
		double arithMean = 0;
		int even = 0;
		int uneven = 0;
		GetArithMeanAndCountEvenUneven(p_arrPtr, SIZE, arithMean, even, uneven);
		cout << "������� �������������� = " << arithMean << endl;
		cout << "���������� ������ ��������� " << even << endl;
		cout << "��������� �������� ��������� " << uneven << endl << endl;

		delete p_arrD1; delete p_arrD2; delete p_arrD3;  delete p_arrD4; delete p_arrD5;
		p_arrD1 = p_arrD2 = p_arrD3 = p_arrD4 = p_arrD5 = nullptr;
	}
	Next();

	// ������ ������� (��������� �� ��������� ����������� ������)

	{
		const int SIZE = 5;
		int arr[SIZE][SIZE]{};
		int *p_arr = *arr;
		for (int i = 0; i < SIZE * SIZE; ++i) {
			p_arr[i] = rand() % 9 + 1;
		}
		for (int i = 0; i < SIZE * SIZE; ++i) {
			if (i % SIZE == 0) {
				cout << endl;
			}
			cout << setw(2) << p_arr[i];
		}
		cout << endl << endl;
		double arithMean = 0;
		int even = 0;
		int uneven = 0;
		GetArithMeanAndCountEvenUneven_(p_arr, SIZE * SIZE, arithMean,even, uneven);
		cout << "������� �������������� = " << arithMean << endl;
		cout << "���������� ������ ��������� " << even << endl;
		cout << "��������� �������� ��������� " << uneven << endl << endl;
	}
	Next();

	/*4. �������� ��������� ������� ��� ������ � ������������ ��������:
	   	 a. ������� ������������� ������������ ������
		 b. ������� ������������� ������������� �������
		 c. ������� ������ ������������� �������
		 d. ������� �������� ������������� �������
		 e. ������� ���������� �������� � ����� �������
		 f. ������� ������� �������� �� ���������� �������
		 g. ������� �������� �������� �� ���������� ������� */

	{
		cout << "�������� ��������� ������� ��� ������ � ������������ ��������:\na.������� ������������� ������������ ������\nb.������� ������������� ������������� �������\nc.������� ������ ������������� �������\nd.������� �������� ������������� �������\ne.������� ���������� �������� � ����� �������\nf.������� ������� �������� �� ���������� �������\ng.������� �������� �������� �� ���������� �������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arr = nullptr;
		Calloc(p_arr, size);
		InitArrayD(p_arr, size, 1, 9);
		ShowArrayD(p_arr, size, 2);
		cout << "\ne.\n";
		int num = GetNum();
		cout << endl;
		AddToArrayEnd(p_arr, size, num);
		ShowArrayD(p_arr, size, num > 99 ? 4 : num > 9 ? 3 : 2);
		cout << "\nf.\n";
		num = GetNum();
		cout << endl;
		int index;
		do {
			index = GetInd();
		} while (index >= size);
		cout << endl;
		PasteToArrayInd(p_arr, size, num, index);
		ShowArrayD(p_arr, size, num > 99 ? 4 : num > 9 ? 3 : 2);
		cout << "\ng.\n";
		do {
			index = GetInd();
		} while (index >= size);
		cout << endl;
		DelElemInArrayInd(p_arr, size, index);
		ShowArrayD(p_arr, size, num > 99 ? 4 : num > 9 ? 3 : 2);
		cout << endl;
		Free(p_arr);
	}
	Next();

	/*5. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������. ������� ������ ������� �� ������� ��� ������������� ����� � ������� ��������� �� ����� ������������ ������, ��������� �������� ������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������������� ������ ������� �� ������� ��� ������������� ����� � ������� ��������� ������� ������������ ������, ��������� �������� ������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};
		InitArrayD(p_arrD, size, -9, 9);
		ShowArrayD(p_arrD, size, 3);
		cout << endl;
		DelNegNums(p_arrD, size);
		cout << " ����� ������:\n\n";
		ShowArrayD(p_arrD, size, 3);
		cout << endl;

		delete[] p_arrD;
		p_arrD = nullptr;
	}
	Next();

	/*6. ������� �������, ����������� ��������� ���� ��������� � ����� �������*/

	{
		cout << "������� �������, ����������� ��������� ���� ��������� � ����� �������\n\n";
		int sizeArr = GetSize();
		cout << endl;
		int *p_arrD = new int[sizeArr] {};
		InitArrayD(p_arrD, sizeArr, 1, 9);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;
		cout << "���� ���������:\n\n";
		int sizeBk = GetSize();
		cout << endl;
		int *p_blockD = new int[sizeBk] {};
		InitArrayD(p_blockD, sizeBk, 1, 9);
		ShowArrayD(p_blockD, sizeBk, 2);
		cout << endl;
		AddBlockToArrayEnd(p_arrD, sizeArr, p_blockD, sizeBk);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;

		delete[] p_arrD;
		delete[] p_blockD;
		p_arrD = p_blockD = nullptr;
	}
	Next();

	/*7. ������� �������, ����������� ��������� ���� ���������, ������� � ������������� ������� �������*/

	{
		cout << "������� �������, ����������� ��������� ���� ���������, ������� � �������������  ������� �������\n\n";
		int sizeArr = GetSize();
		cout << endl;
		int *p_arrD = new int[sizeArr] {};
		InitArrayD(p_arrD, sizeArr, 1, 9);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;
		cout << "���� ���������:\n\n";
		int sizeBk = GetSize();
		cout << endl;
		int *p_blockD = new int[sizeBk] {};
		InitArrayD(p_blockD, sizeBk, 1, 9);
		ShowArrayD(p_blockD, sizeBk, 2);
		cout << endl;
		int index;
		do {
			index = GetInd();
		} while (index > sizeArr);
		cout << endl;
		PasteBlockToArrayInd(p_arrD, sizeArr, p_blockD, sizeBk, index);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;

		delete[] p_arrD;
		delete[] p_blockD;
		p_arrD = p_blockD = nullptr;
	}
	Next();

	/*8. ������� �������, ����������� ������� ���� ���������, ������� � ������������� ������� �������*/

	{
		cout << "������� �������, ����������� ������� ���� ���������, ������� � �������������    ������� �������\n\n";
		int sizeArr = GetSize();
		cout << endl;
		int *p_arrD = new int[sizeArr] {};
		InitArrayD(p_arrD, sizeArr, 1, 9);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;
		int sizeBk;
		cout << "���� ���������:\n\n";
		do {
			sizeBk = GetSize();
		} while (sizeBk > sizeArr);
		cout << endl;
		int index;
		do {
			index = GetInd();
		} while (index + sizeBk > sizeArr);
		cout << endl;
		DelBlockInArrayInd(p_arrD, sizeArr, sizeBk, index);
		if (sizeArr == 0) {
			cout << "��� �������� ������� �������\n\n";
		}
		else {
			ShowArrayD(p_arrD, sizeArr, 2);
		}
		cout << endl;

		delete[] p_arrD;
		p_arrD = nullptr;
	}
	Next();

	/*9. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������. ������� ������ ������� �� ������� ��� ������� ����� � ������� ��������� �� ����� ������������ ������, ��������� �������� ������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������������� ������ ������� �� ������� ��� ������� ����� � ������� ��������� �� ����������������� ������, ��������� �������� ������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};
		InitArrayD(p_arrD, size, 1, 99);
		ShowArrayD(p_arrD, size, 3);
		cout << endl;
		DelPrimeNums_(p_arrD, size);
		//DelPrimeNums(p_arrD, size);
		ShowArrayD(p_arrD, size, 3);
		cout << endl;

		delete[] p_arrD;
		p_arrD = nullptr;
	}
	Next();

	/*10. �������� �������, ������� �������� ��������� �� ����������� ������ � ��� ������. ������� ������������ �������������, ������������� � ������� �������� � ��������� ������������ �������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ����������� ������ � ��� ������.������� ������������ �������������, ������������� � ������� �������� � ��������������������� �������\n\n";
		const int SIZE = 10;
		int arr[SIZE]{};
		InitArray(arr, SIZE, -9, 9);
		ShowArray(arr, SIZE, 3);
		cout << endl;
		int *p_arrPos, *p_arrNeg, *p_arrZero;
		AllocPosNegZero(arr, SIZE, p_arrPos, p_arrNeg, p_arrZero);
		cout << "������ � �������������� ����������:\n\n";
		ShowArray(p_arrPos + 1, p_arrPos[0], 2);
		cout << endl;
		cout << "������ � �������������� ����������:\n\n";
		ShowArray(p_arrNeg + 1, p_arrNeg[0], 3);
		cout << endl;
		cout << "������ � �������� ����������:\n\n";
		ShowArray(p_arrZero + 1, p_arrZero[0], 2);
		cout << endl;

		delete[] p_arrPos;
		delete[] p_arrNeg;
		delete[] p_arrZero;
		p_arrPos = p_arrNeg = p_arrZero = nullptr;
	}
	Next();

	

}