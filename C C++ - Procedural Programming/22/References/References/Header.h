#pragma once

int GetSize();
int GetNum();
int GetInd();

template<typename T>
void InitArray(T *p_arr, int size, int min = 1, int max = 9) {
	for (int i = 0; i < size; ++i) {
		p_arr[i] = rand() % (max - min + 1) + min;
	}
}

template<typename T>
void ShowArray(T *p_arr, int size, int width = 2) {
	for (int i = 0; i < size; ++i) {
		cout << setw(width) << p_arr[i];
	}
	cout << endl;
}

template<typename T>
void InitArrayTwoDim(T **pp_arr, int size, int min = 1, int max = 9) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			pp_arr[i][j] = rand() % (max - min + 1) + min;
		}
	}
}

template<typename T>
void ShowArrayTwoDim(T **pp_arr, int size, int width = 2) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			cout << setw(width) << pp_arr[i][j];
		}
		cout << endl;
	}
}

void GetSumMulti(int *p_arr, int size, int &sum, int &mult);

void GetPosNegZero(int *p_arr, int size, int &pos, int &neg, int &zero);

void GetArithMeanAndCountEvenUneven(int **pp_arr, int size, double &arithMean, int &even, int &uneven);
void GetArithMeanAndCountEvenUneven_(int *p_arr, int size, double &arithMean, int &even, int &uneven);

void DelNegNums(int *&p_arr, int &size);

/*a. ������� ������������� ������������ ������*/

template<typename T>
void Calloc(T *&ref_ptr, int size) {
	delete[] ref_ptr;
	ref_ptr = new T[size]{};
}

/*b. ������� ������������� ������������� �������*/

template<typename T>
void InitArrayD(T &ref_arr, int size, int min, int max) {
	for (int i = 0; i < size; ++i) {
		ref_arr[i] = T((rand() % (max - min + 1) + min) + (rand() % 100 / 100.));
	}
}

/* c. ������� ������ ������������� �������*/

template<typename T>
void ShowArrayD(T &ref_arr, int size, int width) {
	for (int i = 0; i < size; ++i) {
		cout << setw(width) << ref_arr[i];
	}
	cout << endl;
}

/* d. ������� �������� ������������� �������*/

template<typename T>
void Free(T &ref) {
	delete[] ref;
	ref = nullptr;
}

/*e. ������� ���������� �������� � ����� �������*/

template <typename T>
void AddToArrayEnd(T *&ref_arr, int &ref_size, int num) {
	++ref_size;
	T *p_arrTemp = new T[ref_size]{};
	p_arrTemp[ref_size - 1] = num;
	for (int i = 0; i < ref_size - 1; ++i) {
		p_arrTemp[i] = ref_arr[i];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

/*f. ������� ������� �������� �� ���������� �������*/

template<typename T>
void PasteToArrayInd(T *&ref_arr, int &ref_size, int num, int index) {
	++ref_size;
	T *p_arrTemp = new T[ref_size]{};
	for (int i = 0; i < ref_size; ++i) {
		if (i == index) {
			p_arrTemp[i] = num;
			continue;
		}
		if (i < index) {
			p_arrTemp[i] = ref_arr[i];
			continue;
		}
		p_arrTemp[i] = ref_arr[i - 1];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

/*g. ������� �������� �������� �� ���������� �������*/

template<typename T>
void DelElemInArrayInd(T *&ref_arr, int &ref_size, int index) {
	--ref_size;
	T *p_arrTemp = new T[ref_size];
	for (int i = 0; i < ref_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = ref_arr[i];
			continue;
		}
		p_arrTemp[i] = ref_arr[i + 1];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

/*6. ������� �������, ����������� ��������� ���� ��������� � ����� �������*/

template<typename T, typename N>
void AddBlockToArrayEnd(T *&ref_arr, int &ref_size, N &ref_block, int size) {
	ref_size += size;
	T *p_arrTemp = new T[ref_size]{};
	for (int i = 0; i < ref_size; ++i) {
		if (i < ref_size - size) {
			p_arrTemp[i] = ref_arr[i];
			continue;
		}
		p_arrTemp[i] = ref_block[i - (ref_size - size)];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

/*7. ������� �������, ����������� ��������� ���� ���������, ������� � ������������� ������� �������*/

template<typename T, typename N>
void PasteBlockToArrayInd(T *&ref_arr, int &ref_size, N &ref_block, int size, int index) {
	ref_size += size;
	T *p_arrTemp = new T[ref_size]{};
	for (int i = 0; i < ref_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = ref_arr[i];
			continue;
		}
		if (i == index) {
			for (int j = 0; j < size; ++j) {
				p_arrTemp[i++] = ref_block[j];
			}
			--i;
			continue;
		}
		p_arrTemp[i] = ref_arr[i - size];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

/*8. ������� �������, ����������� ������� ���� ���������, ������� � ������������� ������� �������*/

template<typename T>
void DelBlockInArrayInd(T *&ref_arr, int &ref_size, int size, int index) {
	ref_size -= size;
	T *p_arrTemp = new T[ref_size]{};
	for (int i = 0; i < ref_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = ref_arr[i];
			continue;
		}
		p_arrTemp[i] = ref_arr[i + size];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

void DelPrimeNums(int *&ref_arr, int &ref_size);
void DelPrimeNums_(int *&ref_arr, int &ref_size);

void AllocPosNegZero(int *p_arr, int size, int *&p_arrPos, int *&p_arrNeg, int *&p_arrZero);