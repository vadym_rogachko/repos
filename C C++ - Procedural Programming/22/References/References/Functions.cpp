#include"Header.h"
#include<iostream>
#include<iomanip>

using namespace std;



int GetSize() {
	int size;
	do {
		cout << "������� ������ �������\n";
		cin >> size;
	} while (size < 1);
	return size;
}

int GetNum() {
	int num;
	cout << "������� �����\n";
	cin >> num;
	return num;
}

int GetInd() {
	int index;
	do {
		cout << "������� ������\n";
		cin >> index;
	} while (index < 0);
	return index;
}

/*1. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ����� � ������������ ��� ��������� � ���� ����������-�������*/

void GetSumMulti(int *p_arr, int size, int &sum, int &mult) {
	for (int i = 0; i < size; ++i) {
		sum += p_arr[i];
	}
	for (int i = 0; i < size; ++i) {
		mult *= p_arr[i];
	}
}

/*2. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ���������� �������������, ������������� � ������� ��������� �������, ��������� �������� ������*/

void GetPosNegZero(int *p_arr, int size, int &pos, int &neg, int &zero) {
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? ++pos : p_arr[i] < 0 ? ++neg : ++zero;
	}
}

/*3. �������� �������, ������� �������� ��������� �� ��������� ������ � ��� ������, � ���������� ������� �������������� ��������� �������, � ����� ���������� ������ � �������� ���������, ��������� �������� ������*/

void GetArithMeanAndCountEvenUneven(int **pp_arr, int size, double &arithMean, int &even, int &uneven) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			pp_arr[i][j] % 2 == 0 ? ++even : ++uneven;
			arithMean += pp_arr[i][j];
		}
	}
	arithMean /= size * size;
}

void GetArithMeanAndCountEvenUneven_(int *p_arr, int size, double &arithMean, int &even, int &uneven) {
	for (int i = 0; i < size; ++i) {
		p_arr[i] % 2 == 0 ? ++even : ++uneven;
		arithMean += p_arr[i];
	}
	arithMean /= size;
}

/*4. �������� ��������� ������� ��� ������ � ������������ ��������:
	a. ������� ������������� ������������ ������
	b. ������� ������������� ������������� �������
	c. ������� ������ ������������� �������
	d. ������� �������� ������������� �������
	e. ������� ���������� �������� � ����� �������
	f. ������� ������� �������� �� ���������� �������
	g. ������� �������� �������� �� ���������� ������� */

// ��� � ������������ ����� ���������� ���������

/*5. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������. ������� ������ ������� �� ������� ��� ������������� ����� � ������� ��������� �� ����� ������������ ������, ��������� �������� ������*/

void DelNegNums(int *&ref_arr, int &ref_size) {
	for (int i = 0; i < ref_size; ++i) {
		if (ref_arr[i] < 0) {
			for (int j = i; j < ref_size - 1; ++j) {
				ref_arr[j] = ref_arr[j + 1];
			}
			--ref_size, --i;
		}
	}
	int *p_arrTemp = new int[ref_size]{};
	for (int i = 0; i < ref_size; ++i) {
		p_arrTemp[i] = ref_arr[i];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

/*6. ������� �������, ����������� ��������� ���� ��������� � ����� �������*/
/*7. ������� �������, ����������� ��������� ���� ���������, ������� � ������������� ������� �������*/
/*8. ������� �������, ����������� ������� ���� ���������, ������� � ������������� ������� �������*/

// � ������������ ����� ���������� ���������

/*9. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������.������� ������ ������� �� ������� ��� ������� ����� � ������� ��������� �� ����� ������������ ������, ��������� �������� ������*/

// ������ �������

void DelPrimeNums(int *&ref_arr, int &ref_size) {
	for (int i = 0; i < ref_size; ++i) {
		bool flag = true;
		int num = 1;
		while (num++ <= ref_arr[i] / 2) {
			if (ref_arr[i] == 2) {
				flag = false;
				break;
			}
			flag = ref_arr[i] % num == 0 ? true : false;
			if (flag) {
				break;
			}
		}
		if (!flag) {
			for (int j = i; j < ref_size - 1; ++j) {
				ref_arr[j] = ref_arr[j + 1];
			}
			--ref_size, --i;
		}
	}
	int *p_arrTemp = new int[ref_size]{};
	for (int i = 0; i < ref_size; ++i) {
		p_arrTemp[i] = ref_arr[i];
	}

	delete[] ref_arr;
	ref_arr = p_arrTemp;
}

// ������ �������

void DelPrimeNums_(int *&ref_arr, int &ref_size) {
	for (int i = 0; i < ref_size; ++i) {
		bool flag = true;
		int num = 1;
		while (num++ <= ref_arr[i] / 2) {
			if (ref_arr[i] == 2) {
				flag = false;
				break;
			}
			flag = ref_arr[i] % num == 0 ? true : false;
			if (flag) {
				break;
			}
		}
		if (!flag) {
		DelElemInArrayInd(ref_arr, ref_size, i);
			--i;
		}
	}
}

/*10. �������� �������, ������� �������� ��������� �� ����������� ������ � ��� ������. ������� ������������ �������������, ������������� � ������� �������� � ��������� ������������ �������*/

void AllocPosNegZero(int *p_arr, int size, int *&ref_arrPos, int *&ref_arrNeg, int *&ref_arrZero) {
	int sizePos = 0, sizeNeg = 0, sizeZero = 0;
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? ++sizePos : p_arr[i] < 0 ? ++sizeNeg : ++sizeZero;
	}
	ref_arrPos = new int[sizePos + 1]{};
	ref_arrNeg = new int[sizeNeg + 1]{};
	ref_arrZero = new int[sizeZero + 1]{};
	ref_arrPos[0] = sizePos;
	ref_arrNeg[0] = sizeNeg;
	ref_arrZero[0] = sizeZero;
	sizePos = sizeNeg = sizeZero = 1;
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? ref_arrPos[sizePos++] = p_arr[i] : p_arr[i] < 0 ? ref_arrNeg[sizeNeg++] = p_arr[i] : ref_arrZero[sizeZero++] = p_arr[i];
	}
}