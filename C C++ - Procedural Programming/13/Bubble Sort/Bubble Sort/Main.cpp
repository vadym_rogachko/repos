#include<iostream>
#include<time.h>
#include<Windows.h>

using namespace std;




void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	
	/*1. ������� ������ ��������� ����� ������������ 10 ���������. 
      � ����������� �� ���������� ������������� ��������� ���������� �������. ���� ������������ ������ 1, ���������� ���� �� ��������, ���� 0, �� �� �����������.*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int size = SIZE;
		short action;
		bool flag;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand();
			cout << arr[i] << ' ';
		}
		do {
			cout << "\n\n\n�������� ����������: \n0. �� �����������\n1. �� ��������\n";
			cin >> action;
		} while (action != 0 && action != 1);
		if (!action) {
			for (int i = 0; i < SIZE; ++i) {
				flag = true;
				for (int j = 0; j < size - 1; ++j) {
					if (arr[j] > arr[j + 1]) {
						swap(arr[j], arr[j + 1]);
						flag = false;
					}
				}
				--size;
				if (flag) {
					break;
				}
			}
		}
		else {
			for (int i = 0; i < SIZE; ++i) {
				flag = true;
				for (int j = 0; j < size - 1; ++j) {
					if (arr[j] < arr[j + 1]) {
						swap(arr[j], arr[j + 1]);
						flag = false;
					}
				}
				--size;
				if (flag) {
					break;
				}
			}
		}
		cout << "\n\n���������������� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << ' ';
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*2. ������� ������ ��������� ����� � ��������� �� -20 �� +20. ���������� ����� ������� ������� ������������� ��������� (������ ������ �������������� �������� � ������ ������� �������������� ��������) �
	     ������������� ��������, ����������� ����� ����.*/
	
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		int j, size;
		bool flag;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 41 - 20;
			cout << arr[i] << ' ';
		}
		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] < 0) {
				j = i;
				for (int k = SIZE - 1; k >= 0; --k) {
					if (arr[k] < 0) {
						size = k;
						break;
					}
				}
				break;
			}
		}
		int size2 = size;
		if (j != size) {
			for (int i = j + 1; i < size - 1; ++j) {
				flag = true;
				for (int k = i; k < size2 - 1; ++k) {
					if (arr[k] > arr[k + 1]) {
						swap(arr[k], arr[k + 1]);
						flag = false;
					}
				}
				--size2;
				if (flag) {
					break;
				}
			}
			cout << "\n\n\n������ � ���������������� ���������� ����� �������� �������������� �������:\n\n\n";
			for (int i = 0; i < SIZE; ++i) {
				cout << arr[i] << ' ';
			}
		}
		else {
			cout << "\n\n\n� ������� ������ ���� ������������� �����";
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*3. ������� ������ �� 40 ����� ����� �� ���������� �� 1 �� 20. 
      ����������: 
      - ������� ��������� ����� �� ���� �� ��������� � ����� ������� ����� ���������� ����� � �������; 
      - ������������� �������� �������, ����������� ����� �� ��������� ������� �� ��������, � �������� �������, ����������� ������ �� ��������� ������� �� �����������. */

	// ������� ������ ������ �������� �� 20 ����� ��� �������� ������ ���� �� 40, �.�. � ��������� ������ ����� ���������� ����� � � ����� ����� ��� ������� � ����������� ����� ����� �� �� ������� ������,
    // � � ������� �������,��� ����� ����� ���� �������, � �� ���.
    // � ������� ��������� �� ����� ������� � ���� ����������� - ����� � ������ �� �������, �� ���� ���� ����� ����� ���� 40 � �������� �� 1 �� 40 ���� 20 �� ���������� �� 1 �� 20

	{
		const int SIZE = 20;
		int arr[SIZE]{ 17,12,15,4,1,5,9,16,13,8,3,11,20,2,19,14,7,10,6,18 };
		int k;
		bool flag;
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << ' ';
		}
		int num = rand() % 20 + 1;
		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] == num) {
				k = i;
				break;
			}
		}
		cout << "\n\n\n��������� �����: " << num << "\n����� ��������� � ������� � " << k + 1 << " �������\n";
		int size = k;
		for (int i = 0; i < k - 1; ++i) {
			flag = true;
			for (int j = 0; j < size - 1; ++j) {
				if (arr[j] < arr[j + 1]) {
					swap(arr[j], arr[j + 1]);
					flag = false;
				}
			}
			--size;
			if (flag) {
				break;
			}
		}
		size = SIZE;
		for (int i = k + 1; i < SIZE; ++i) {
			flag = true;
			for (int j = k + 1; j < size - 1; ++j) {
				if (arr[j] > arr[j + 1]) {
					swap(arr[j], arr[j + 1]);
					flag = false;
				}
			}
			--size;
			if (flag) {
				break;
			}
		}
		cout << "\n\n������ � ���������������� ���������� ����� � ������ �� " << k + 1 << " �������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			if (i == k) {
				SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
				cout << arr[i] << ' ';
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
				continue;
			}
			cout << arr[i] << ' ';
		}
		cout << endl << endl << endl;
	}









}