#include<iostream>
#include<windows.h>
#include<time.h>
#include"Functions.h"

using namespace std;

/*10. ���� ��������� ����� �������� 8�8 � ��������� ����. ��������� ������ ��������� � ������������ ���������� ������ ���� � ��������� ���� ����. ������ ��������� ����� � ������� ���� ����, ��� ������� �� ������� ��� ������ �����, ��������� � ������ ������
������ ���� ���. (��� ��� ������� ��������� ���� ��� ������ ��������� ������ ����� ����������, �� ������������� ������� ���������� ������ �� ���� �������� 6�6). � ��������� ���������� ������������ ��������*/

// ������ ������������ ������� �� ���� ��������� �������� �� ����� 6 �� 6, �� ����� 7 �� 7 - ��������� ��������� ������� ������������ ������, ��������� ����� �����. 8 �� 8 - ��� ��������� ������� ������������ ����� �����

enum yMoves { UP_LONG = -2, UP_SHORT = -1, DOWN_LONG = 2, DOWN_SHORT = 1 };
enum xMoves { LEFT_SHORT = -1, LEFT_LONG = -2, RIGHT_SHORT = 1, RIGHT_LONG = 2 };
static bool flag = false;

void HorseMove() {
	char hor, vert;
	int x, y;
	const int SIZE = 8;
	int arr[SIZE][SIZE]{};
	int num = 1;
	cout << "������� ���������� ������ ����\n\n(a, b, c, d, e, f, g, h) :\n";
	do {
		cin >> hor;
	} while (hor < 65 || hor > 104 || (hor > 72 && hor < 97));
	cout << "\n(1, 2, 3, 4, 5, 6, 7, 8) :\n";
	do {
		cin >> vert;
	} while (vert < 49 || vert > 56);
	if (hor >= 65 && hor <= 72) {
		x = hor - 65;
	}
	else {
		x = hor - 97;
	}
	y = vert - 49;
	int yMove = 777;
	int xMove = 777;
	Move(arr, num, y, x, yMove, xMove);
}

void ShowChessBoard(int arr[][8], int size) {
	system("cls");
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			cout << arr[i][j] << (arr[i][j] > 9 ? " " : "  ");
		}
		cout << endl;
	}
	cout << endl;
}

void Move(int arr[][8], int num, int y, int x, int yMove, int xMove, int size) {
	arr[y][x] = num;
	if (num == 64) {
		flag = true;
	}
	if (flag) {
		if (CheckField(arr)) {
			ShowChessBoard(arr, size);
			system("pause");
			exit(0);
		}
	}
	++num;
	if (y + UP_SHORT >= 0 && x + LEFT_LONG >= 0 && (y + UP_SHORT != yMove || x + LEFT_LONG != xMove) && arr[y + UP_SHORT][x + LEFT_LONG] == 0) {
		Move(arr, num, y + UP_SHORT, x + LEFT_LONG, -UP_SHORT, -LEFT_LONG);
	}
	if (y + UP_LONG >= 0 && x + LEFT_SHORT >= 0 && (y + UP_LONG != yMove || x + LEFT_SHORT != xMove) && arr[y + UP_LONG][x + LEFT_SHORT] == 0) {
		Move(arr, num, y + UP_LONG, x + LEFT_SHORT, -UP_LONG, -LEFT_SHORT);
	}
	if (y + UP_LONG >= 0 && x + RIGHT_SHORT <= size - 1 && (y + UP_LONG != yMove || x + RIGHT_SHORT != xMove) && arr[y + UP_LONG][x + RIGHT_SHORT] == 0) {
		Move(arr, num, y + UP_LONG, x + RIGHT_SHORT, -UP_LONG, -RIGHT_SHORT);
	}
	if (y + UP_SHORT >= 0 && x + RIGHT_LONG <= size - 1 && (y + UP_SHORT != yMove || x + RIGHT_LONG != xMove) && arr[y + UP_SHORT][x + RIGHT_LONG] == 0) {
		Move(arr, num, y + UP_SHORT, x + RIGHT_LONG, -UP_SHORT, -RIGHT_LONG);
	}
	if (y + DOWN_SHORT <= size + 1 && x + RIGHT_LONG <= size - 1 && (y + DOWN_SHORT != yMove || x + RIGHT_LONG != xMove) && arr[y + DOWN_SHORT][x + RIGHT_LONG] == 0) {
		Move(arr, num, y + DOWN_SHORT, x + RIGHT_LONG, -DOWN_SHORT, -RIGHT_LONG);
	}
	if (y + DOWN_LONG <= size - 1 && x + RIGHT_SHORT <= size - 1 && (y + DOWN_LONG != yMove || x + RIGHT_SHORT != xMove) && arr[y + DOWN_LONG][x + RIGHT_SHORT] == 0) {
		Move(arr, num, y + DOWN_LONG, x + RIGHT_SHORT, -DOWN_LONG, -RIGHT_SHORT);
	}
	if (y + DOWN_LONG <= size - 1 && x + LEFT_SHORT >= 0 && (y + DOWN_LONG != yMove || x + LEFT_SHORT != xMove) && arr[y + DOWN_LONG][x + LEFT_SHORT] == 0) {
		Move(arr, num, y + DOWN_LONG, x + LEFT_SHORT, -DOWN_LONG, -LEFT_SHORT);
	}
	if (y + DOWN_SHORT <= size - 1 && x + LEFT_LONG >= 0 && (y + DOWN_SHORT != yMove || x + LEFT_LONG != xMove) && arr[y + DOWN_SHORT][x + LEFT_LONG] == 0) {
		Move(arr, num, y + DOWN_SHORT, x + LEFT_LONG, -DOWN_SHORT, -LEFT_LONG);
	}
	arr[y][x] = 0;
	--num;
	if (y + UP_SHORT >= 0 && x + LEFT_LONG >= 0 && arr[y + UP_SHORT][x + LEFT_LONG] == num) {
		Move(arr, num, y + UP_SHORT, x + LEFT_LONG, -UP_SHORT, -LEFT_LONG);
	}
	if (y + UP_LONG >= 0 && x + LEFT_SHORT >= 0 && arr[y + UP_LONG][x + LEFT_SHORT] == num) {
		Move(arr, num, y + UP_LONG, x + LEFT_SHORT, -UP_LONG, -LEFT_SHORT);
	}
	if (y + UP_LONG >= 0 && x + RIGHT_SHORT <= size - 1 && arr[y + UP_LONG][x + RIGHT_SHORT] == num) {
		Move(arr, num, y + UP_LONG, x + RIGHT_SHORT, -UP_LONG, -RIGHT_SHORT);
	}
	if (y + UP_SHORT >= 0 && x + RIGHT_LONG <= size - 1 && arr[y + UP_SHORT][x + RIGHT_LONG] == num) {
		Move(arr, num, y + UP_SHORT, x + RIGHT_LONG, -UP_SHORT, -RIGHT_LONG);
	}
	if (y + DOWN_SHORT <= size + 1 && x + RIGHT_LONG <= size - 1 && arr[y + DOWN_SHORT][x + RIGHT_LONG] == num) {
		Move(arr, num, y + DOWN_SHORT, x + RIGHT_LONG, -DOWN_SHORT, -RIGHT_LONG);
	}
	if (y + DOWN_LONG <= size - 1 && x + RIGHT_SHORT <= size - 1 && arr[y + DOWN_LONG][x + RIGHT_SHORT] == num) {
		Move(arr, num, y + DOWN_LONG, x + RIGHT_SHORT, -DOWN_LONG, -RIGHT_SHORT);
	}
	if (y + DOWN_LONG <= size - 1 && x + LEFT_SHORT >= 0 && arr[y + DOWN_LONG][x + LEFT_SHORT] == num) {
		Move(arr, num, y + DOWN_LONG, x + LEFT_SHORT, -DOWN_LONG, -LEFT_SHORT);
	}
	if (y + DOWN_SHORT <= size - 1 && x + LEFT_LONG >= 0 && arr[y + DOWN_SHORT][x + LEFT_LONG] == num) {
		Move(arr, num, y + DOWN_SHORT, x + LEFT_LONG, -DOWN_SHORT, -LEFT_LONG);
	}
}

bool CheckField(int arr[][8], int size) {
	int counter = 0;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			if (arr[i][j] != 0) {
				++counter;
			}
		}
	}
	if (counter == 64) {
		return true;
	}
	else {
		return false;
	}
}