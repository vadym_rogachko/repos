#include<iostream>
#include<time.h>
#include<conio.h>
#include"Functions.h"

using namespace std;


void Next() {
	system("pause");
	system("cls");
}



void main() {
	
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	{
		cout << "�������� ������������� ������� (int, double, char) ��� ���������� ���������     �����:\n- ������������� ���������� �������;\n- ����� ������� �� �����;\n- ����������� ������������� � ������������ �������� �� ������� ��������� �������- ���������� ��������� �� ����������� �������� ��� ������ ������ �������\n\n";
		const int SIZE = 5;
		int arr[SIZE][5]{};
		double dr[SIZE][5]{};
		char ch[SIZE][5]{};
		InitArray(arr); InitArray(dr); InitArray(ch);
		ShowArray(arr); GetMaxMinMainDiag(arr);
		ShowArray(dr);  GetMaxMinMainDiag(dr);
		ShowArray(ch);  GetMaxMinMainDiag(ch);
		SortArrayRows(arr, 5); SortArrayRows(dr, 5); SortArrayRows(ch, 5);
		cout << "\n\n ��������������� ��������� �������:\n\n";
		ShowArray(arr); ShowArray(dr); ShowArray(ch);
	}
	Next();
	{
		const int ROW = 3;
		const int COL = 11;
		char arr[ROW][COL]{};
		short code;
		char choose;
		do {
			system("cls");
			cout << "�������� ����: 1. � ���������\n\t       2. � �����������\n";
			cin >> choose;
		} while (choose != '1' && choose != '2');
		if (choose == '1') {
			do {
				code = TicTacToe(arr, ROW, COL);
				_getch();
			} while (code != 27);
		}
		else {
			do {
				system("cls");
				cout << "��������, ��� ����� ������ ������ : 1. ��\n\t\t\t\t   2. ���������\n";
				cin >> choose;
			} while (choose != '1' && choose != '2');
			if (choose == '1') {
				do {
					code = TicTacToeComp(arr, ROW, COL);
					_getch();
				} while (code != 27);
			}
			else {
				do {
					code = TicTacToeCompFirst(arr, ROW, COL);
					_getch();
				} while (code != 27);
			}
		}
	}
	system("cls");
	{
		cout << "�������� ������� ��� �������� ����������� ����� � ������ �������                ��������� (2-36)\n\n";
		int num = GetNum();
		GetNonDecNum(num);
	}
	Next();
	{
		long long num = GetBinNum();
		system("cls");
		cout << endl << num << "  2--->10  " << GetDecNum(num) << endl << endl;
	}
	Next();
	{
		const int SIZE = 4;
		int arr[SIZE][SIZE]{};
		short code;
		cout << "����� ���������� � ���� ��������!!!\n\nEnter - ������� ���\n" << char(24) << ' ' << char(25) << ' ' << char(26) << ' ' << char(27) << " - ����������\nR - ������ ������\nEsc - �����\n\n";
		Next();
		do {
			code = Fifteen(arr, SIZE);
		} while (code != 27);
	}
	Next();
	{
		cout << "�������� ����������� ������� ���������� ����������� ������ �������� ���� �����  �����\n\n";
		int num1, num2;
		cout << "������� ��� �����\n";
		cin >> num1 >> num2;
		if (num1 == 0) {
			num1 = num2;
		}
		if (num2 == 0) {
			num2 = num1;
		}
		if (num1 < 0) {
			num1 = -num1;
		}
		if (num2 < 0) {
			num2 = -num2;
		}
		cout << "\n���������� ����� ��������" << " = " << (num1 > num2 ? GetMaxComFact(num1, num2) : GetMaxComFact(num2, num1)) << endl << endl;
	}																																			 
	Next();																														
	{																																		
		cout << "�������� ����������� ������� ���������� ���������� �����\n\n";                                                                   
		int num;
		cout << "������� �����\n";
		cin >> num;
		system("cls");
		cout << endl << num << "! = " << GetFactorial(num) << endl << endl;
	}
	Next();
	{
		cout << "�������� ����������� ������� ���������� ������� �����\n\n";
		int num, pow;
		cout << "������� �����\n";
		cin >> num;
		cout << "������� �������\n";
		cin >> pow;
		system("cls");
		cout << endl << num << "^" << pow << " = " << GetPower(num, pow) << endl << endl;
	}
	Next();
	{
		cout << "\t\t   ����� ���������� � ���� \"���� � ������\"\n\n��������� ���������� �������������� ����� ��� ������������� ����.\n�������� ���������� ����������� �����.\n������� �������������� ����� ��� ������������� ����.\n����   - ��������� �����.\n������ - ��������� �����, ������� �� ������ �����\n\n�����!\n\n��� ������ ������� ����� 0\n\n";
		Next();
		char ch;
		do {
			system("cls");
			BullsAndCows();
			cout << "����������? (Y/N)\n";
			cin >> ch;
		} while (ch == 'Y' || ch == 'y');
	}
	Next();
	{
		HorseMove();
	}
}