#include<iostream>
#include<time.h>
#include<conio.h>
#include<windows.h>
#include"Functions.h"

using namespace std;


HANDLE handle_ = GetStdHandle(STD_OUTPUT_HANDLE);


/*5. �������� ���� ���������*/

int Fifteen(int arr[][4], int size) {
	system("cls");
	bool unsolvable;
	do {
		CreateField(arr, size);
		unsolvable = CheckField(arr, size);
	} while (unsolvable);
	enum codes {ESC = 27,
				FIRST_CODE = 224,
				UP = 72,
				DOWN = 80,
				LEFT = 75,
				RIGHT = 77,
				ENTER = 13,
				R = 82,
				r = 114};
	static int record = 2000000000;
	int code;
	int move, coord, x, y;
	int emptyElement, xEmp, yEmp;
	bool theEnd;
	coord = move = 0;
	while (true) {
		theEnd = IsEndOfGame(arr, size);
		if (theEnd) {
			ShowField(arr, size, coord, move);
			cout << "\n �� ��������!!!\n";
			if (record != 2000000000) {
				cout << "\n������ " << "\n�����:  " << record << endl << endl;
			}
			if (move < record) {
				cout << " �����������!!!\n\n�����  ������: ";
				record = move;
				cout << record;
			}
			cout << endl;
			system("pause");
			code = _getch();
			break;
		}
		emptyElement = ShowField(arr, size, coord, move);
		yEmp = emptyElement / size;
		xEmp = emptyElement - yEmp * size;
		y = coord / size;
		x = coord - y * size;
		cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
		code = _getch();
		if (code == R || code == r) {
			return code;
		}
		if (code == ESC) {
			ShowField(arr, size, coord, move);
			cout << "\nBye!\n\n";
			break;
		}
		if (code == ENTER) {
			if (((x - xEmp == 1 || x - xEmp == -1) && y - yEmp == 0) || ((y - yEmp == 1 || y - yEmp == -1) && x - xEmp == 0)) {
				swap(arr[y][x], arr[yEmp][xEmp]);
				coord = emptyElement;
				++move;
			}
			continue;
		}
		if (code == FIRST_CODE) {
			code = _getch();
			switch (code) {
			case UP:
				if (y != 0) {
					coord -= 4;
					if (coord == emptyElement && yEmp != 0) {
						coord -= 4;
					}
					else if (coord == emptyElement && yEmp == 0) {
						coord += 4;
					}
				}
				break;
			case DOWN:
				if (y != size - 1) {
					coord += 4;
					if (coord == emptyElement && yEmp != size - 1) {
						coord += 4;
					}
					else if (coord == emptyElement && yEmp == size - 1) {
						coord -= 4;
					}
				}
				break;
			case LEFT:
				if (x != 0) {
					--coord;
					if (coord == emptyElement && xEmp != 0) {
						--coord;
					}
					else if (coord == emptyElement && xEmp == 0) {
						++coord;
					}
				}
				break;
			case RIGHT:
				if (x != size - 1) {
					++coord;
					if (coord == emptyElement && xEmp != size - 1) {
						++coord;
					}
					else if (coord == emptyElement && xEmp == size - 1) {
						--coord;
					}
				}
			}
		}
	}
	return code;
}

void CreateField(int arr[][4], int size) {
	const int SIZE = 15;
	int arrTemp[SIZE]{};
	for (int i = 0; i < SIZE; ++i) {
		arrTemp[i] = i + 1;
	}
	for (int i = 0, index, counter = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			while (counter < 15) {
				index = rand() % 15;
				if (arrTemp[index] != 0) {
					arr[i][j] = arrTemp[index];
					arrTemp[index] = 0;
					++counter;
					break;
				}
			}
		}
	}
	arr[3][3] = 16;
	swap(arr[3][3], arr[rand() % 4][rand() % 4]);
}

bool CheckField(int arr[][4], int size) {
	int pos, row, col, sum = 0;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			if (arr[i][j] == 16) {
				row = i + 1;
				continue;
			}
			col = j + 1;
			for (int k = i; k < size; ++k) {
				for (int q = col; q < size; ++q) {
					if (arr[i][j] > arr[k][q]) {
						++sum;
					}
				}
				col = 0;
			}
		}
	}
	sum += row;
	return sum % 2;
}

bool IsEndOfGame(int arr[][4], int size) {
	int num = 1;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			if (arr[i][j] != num) {
				return false;
			}
			++num;
		}
	}
	return true;
}

int ShowField(int arr[][4], int size, int coord, int move) {
	setlocale(LC_ALL, "C");
	system("cls");
	int coordEmpty;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			if (arr[i][j] == 16) {
				cout  << "   " << char(179);
				coordEmpty = i * size + j;
				continue;
			}
			if (i == coord / size && j == coord - coord / size * size) {
				SetConsoleTextAttribute(handle_, 12);
				cout << arr[i][j];
				SetConsoleTextAttribute(handle_, 7);
				if (arr[i][j] < 10) {
					cout << "  ";
				}
				else {
					cout << ' ';
				}
				cout << char(179);
				continue;
			}
			cout << arr[i][j];
			if (arr[i][j] < 10) {
				cout << "  ";
			}
			else {
				cout << ' ';
			}
			cout << char(179);
		}
		cout << endl;
	}
	setlocale(LC_ALL, "rus");
	cout << "\n\n    ��� " << move << endl;
	return coordEmpty;
}