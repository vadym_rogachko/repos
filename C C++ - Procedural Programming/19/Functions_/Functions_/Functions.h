#pragma once

void InitArray(int[][5], int = 5, int = 10, int = 99);
void InitArray(double[][5], int = 5, int = 10, int = 99);
void InitArray(char[][5], int = 5, int = 33, int = 126);

void ShowArray(int[][5], int = 5);
void ShowArray(double[][5], int = 5);
void ShowArray(char[][5], int = 5);

void GetMaxMinMainDiag(int[][5], int = 5);
void GetMaxMinMainDiag(double[][5], int = 5);
void GetMaxMinMainDiag(char[][5], int = 5);

/* ���������� ��������� �� ����������� �������� ��� ������ ������ ������� */

template <typename T>
void SortArrayRows(T arr[][5], int size) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < 5; ++j) {
			for (int k = j; k > 0; --k) {
				if (arr[i][k] < arr[i][k - 1]) {
					swap(arr[i][k], arr[i][k - 1]);
					continue;
				}
				break;
			}
		}
	}
}


int TicTacToe(char [][11], int, int);	   void CreateField(char[][11], int, int);		void ShowField(char[][11], int, int, short, short, int, int);		bool IsWin(char[][11], int, int);
int TicTacToeComp(char[][11], int, int);
int TicTacToeCompFirst(char[][11], int, int);

void GetNonDecNum(int);		int GetNum();		short GetNumSyst();			void ShowNonDecNum(char[], int, int, short, bool);

int GetDecNum(long long);			long long GetBinNum();		void CheckNum(long long);		void InitArrayForBin(int[], int);

int Fifteen(int[][4], int);			void CreateField(int[][4], int);		int ShowField(int[][4], int, int, int);			bool CheckField(int[][4], int);		bool IsEndOfGame(int[][4], int);

int GetMaxComFact(int, int);

long long GetFactorial(short);

int GetPower(int, int);

void BullsAndCows();			void GetRandNum(int[], int);		int GetNumber(int[], int);		int GetBullsAndCows(int[], int[], int, int);

void HorseMove();				void ShowChessBoard(int[][8], int = 8);			void Move(int[][8], int, int, int, int, int, int = 8);			bool CheckField(int[][8], int = 8);