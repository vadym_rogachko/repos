#include<iostream>
#include<time.h>
#include"Functions.h"

using namespace std;


/*9. �������� ���� ����� � �������. ��������� "����������" �������������� ����� � �������� ������ ������� ���. ����� ����� ������������� ����� ��������� ��������, ������� ���� �����
     ������� (����) � ������� ���� ������� � ����� �� ������ ����� (������). ����� ����������� ����� �� ����� ���������� ������� ���������� ��������� ������������� �������.
	 � ��������� ���������� ������������ ��������*/


void BullsAndCows()
{
	const int SIZE = 4;
	int arr1[SIZE]{}, arr2[SIZE]{};
	GetRandNum(arr1, SIZE);
	int bullsAndCows;
	int attempt = 0;
	do {
		int num = GetNumber(arr2, SIZE);
		if (num == 0) {
			return;
		}
		++attempt;
		bullsAndCows = GetBullsAndCows(arr1, arr2, SIZE, SIZE);
		cout << "\n�� �������� " << num << "\n\n���� " << bullsAndCows / 10 << endl << "������ " << bullsAndCows % 10 << endl;
	} while (bullsAndCows != 11 * SIZE);
	cout << "\n�� �������� �����!!!\n\n";
	cout << "�������: " << attempt << endl;
}

void GetRandNum(int arr[], int size) {
	arr[0] = rand() % 9 + 1;
	bool repeat;
	for (int i = 1; i < size; ++i) {
		do {
			repeat = false;
			arr[i] = rand() % 10;
			for (int j = i - 1; j >= 0; --j) {
				if (arr[i] == arr[j]) {
					repeat = true;
					break;
				}
			} 
		} while (repeat);
	}
}

int GetNumber(int arr[], int size) {
	int num;
	bool repeat;
	do {
		cout << "\n������� �������������� �����\n";
		cin >> num;
		if (num == 0) {
			return 0;;
		}
		int copynum = num;
		for (int i = size - 1; i >= 0; --i) {
			arr[i] = copynum % 10;
			copynum /= 10;
		}
		repeat = false;
		for (int i = 1; i < size; ++i) {
				for (int j = i - 1; j >= 0; --j) {
					if (arr[i] == arr[j]) {
						repeat = true;
						break;
					}
				}
				if (repeat) {
					break;
				}
		}
	} while (num < 1000 || num > 9999 || repeat);
	return num;
}

int GetBullsAndCows(int arr1[], int arr2[], int size, int dig) {
	if (dig < 0) return 0;
	for (int i = 0; i < size; ++i) {
		if (arr1[i] == arr2[dig - 1] && dig - 1 == i) {
			return 11 + GetBullsAndCows(arr1, arr2, size, dig - 1);
		}
		else if (arr1[i] == arr2[dig - 1]) {
			return 10 + GetBullsAndCows(arr1, arr2, size, dig - 1);
		}
	}
	return GetBullsAndCows(arr1, arr2, size, dig - 1);
}