#include<iostream>

using namespace std;


/*1. �������� ������������� ������� (int, double, char) ��� ���������� ��������� �����:
- ������������� ���������� �������;
- ����� ������� �� �����;
- ����������� ������������� � ������������ �������� �� ������� ��������� �������;
- ���������� ��������� �� ����������� �������� ��� ������ ������ �������*/

void InitArray(int arr[][5], int size, int min, int max) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < 5; ++j) {
			arr[i][j] = rand() % (max - min + 1) + min;
		}
	}
}

void InitArray(double arr[][5], int size, int min, int max) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < 5; ++j) {
			arr[i][j] = (rand() % (max - min + 1) + min) / 10.;
		}
	}
}

void InitArray(char arr[][5], int size, int min, int max) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < 5; ++j) {
			arr[i][j] = rand() % (max - min + 1) + min;
		}
	}
}


void ShowArray(int arr[][5], int size) {
	cout << "\n  ";
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < 5; ++j) {
			cout << arr[i][j] << ' ';
		}
		cout << "\n  ";
	}
	cout << endl;
}

void ShowArray(double arr[][5], int size) {
	cout << "\n  ";
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < 5; ++j) {
			cout << arr[i][j] << (int(arr[i][j]) == arr[i][j] ? "   " : " ");
		}
		cout << "\n  ";
	}
	cout << endl;
}

void ShowArray(char arr[][5], int size) {
	cout << "\n  ";
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < 5; ++j) {
			cout << arr[i][j] << ' ';
		}
		cout << "\n  ";
	}
	cout << endl;
}


void GetMaxMinMainDiag(int arr[][5], int size) {
	int min, max;
	short minInd, maxInd;
	min = max = arr[0][0];
	minInd = maxInd = 0;
	for (int i = 1; i < size; ++i) {
		if (arr[i][i] < min) {
			min = arr[i][i];
			minInd = i;
		}
		if (arr[i][i] > max) {
			max = arr[i][i];
			maxInd = i;
		}
	}
	cout << "������������ ������� �� ������� ��������� ������� � " << maxInd + 1 << " ������, � " << maxInd + 1 << " �������\n";
	cout << "������������ ������� �� ������� ��������� ������� = " << max << endl;
	cout << "����������� ������� �� ������� ��������� ������� � " << minInd + 1 << " ������, � " << minInd + 1 << " �������\n";
	cout << "����������� ������� �� ������� ��������� ������� = " << min << endl;
}

void GetMaxMinMainDiag(double arr[][5], int size) {
	double min, max;
	short minInd, maxInd;
	min = max = arr[0][0];
	minInd = maxInd = 0;
	for (int i = 1; i < size; ++i) {
		if (arr[i][i] < min) {
			min = arr[i][i];
			minInd = i;
		}
		if (arr[i][i] > max) {
			max = arr[i][i];
			maxInd = i;
		}
	}
	cout << "������������ ������� �� ������� ��������� ������� � " << maxInd + 1 << " ������, � " << maxInd + 1 << " �������\n";
	cout << "������������ ������� �� ������� ��������� ������� = " << max << endl;
	cout << "����������� ������� �� ������� ��������� ������� � " << minInd + 1 << " ������, � " << minInd + 1 << " �������\n";
	cout << "����������� ������� �� ������� ��������� ������� = " << min << endl;
}

void GetMaxMinMainDiag(char arr[][5], int size) {
	char min, max;
	short minInd, maxInd;
	min = max = arr[0][0];
	minInd = maxInd = 0;
	for (int i = 1; i < size; ++i) {
		if (arr[i][i] < min) {
			min = arr[i][i];
			minInd = i;
		}
		if (arr[i][i] > max) {
			max = arr[i][i];
			maxInd = i;
		}
	}
	cout << "������������ ������� �� ������� ��������� ������� � " << maxInd + 1 << " ������, � " << maxInd + 1 << " �������\n";
	cout << "������������ ������� �� ������� ��������� ������� " << max << endl;
	cout << "����������� ������� �� ������� ��������� ������� � " << minInd + 1 << " ������, � " << minInd + 1 << " �������\n";
	cout << "����������� ������� �� ������� ��������� ������� " << min << endl;
}

// � ���������� ���� ������� ��������� ���������� ��� ���� �������, ������� ����� ����� ������� ��������� ������� (� Functions.h), ���� ��� �� ���������� (� ��� ���������� � ��� ������������)