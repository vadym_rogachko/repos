#include<iostream>
#include<conio.h>
#include<windows.h>
#include"Functions.h"

using namespace std;

HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);


/*2. �������� ���� ���������-������*/

// ��� ������ ���� - � ��������� � � �����������
// ��������� ����� ����� (����� ��������� �������) - ������ ������� ���������. ������� 3 ������ ���������, ��� ����� ������� ����������, ����� ��������� ����� ����, ��� ��� ���� ��� �������� ��� ��� ������ �� ������� ������, � ��������� ���� ��������� ���
// ����� ��������� ����� �������� �� ������� ������ �� ������� ������� � ���, ��� ��� ����� ������� - ��������� ���������� ���� ���������� ��� ������ ������ �������� - ��������� ������� ����� if (����� � ����� ��� �����������) 

int TicTacToe(char arr[][11], int row, int col) {
	static int winCross;
	static int winZero;
	CreateField(arr, row, col);
	enum codes {ESC=27,
			    FIRST_CODE=224,
		   	    UP=72,
			    DOWN=80,
			    LEFT=75,
			    RIGHT=77,
			    ENTER=13};
	int code;
	short y = row / 2;
	short x = col / 2;
	short mid = col / 2;
	short move = 0;
	bool crossMove = true;
	bool win = false;
	bool theEnd = false;
	char symb = 'x';
	char symbConst;
	arr[y][x] = symb;
	while (true) {
		ShowField(arr, row, col, y, x, winCross, winZero);
		if (move >= 5) {
			win = IsWin(arr, row, col);
		}
		if (move == 9) {
			theEnd = true;
		}
		if (win) {
			crossMove = 1 - crossMove;
			crossMove ? ++winCross : ++winZero;
			cout << (crossMove ? "�������� ��������\n\n" : "������ ��������\n\n");
			break;
		}
		if (theEnd) {
			cout << "�����\n\n";
			break;
		}
		symb = crossMove ? 'x' : 'o';
		symbConst = crossMove ? 'X' : 'O';
		code = _getch();
		if (code == ESC) {
			cout << "\nBye!\n";
			break;
		}
		if (code == ENTER) {
			if (arr[y][x] == symb) {
				arr[y][x] = symbConst;
				crossMove = 1 - crossMove;
				++move;
			}
			continue;
		}
		if (code == FIRST_CODE) {
			code = _getch();
			switch (code) {
			case UP:
				if (y > 0) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					if (arr[--y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case DOWN:
				if (y < row - 1) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					if (arr[++y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case LEFT:
				if (x == mid || x == mid + 4) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					x -= 4;
					if (arr[y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case RIGHT:
				if (x == mid - 4 || x == mid) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					x += 4;
					if (arr[y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			}
		}
	}
	return code;
}

int TicTacToeComp(char arr[][11], int row, int col) {
	static int winCross;
	static int winZero;
	CreateField(arr, row, col);
	enum codes {
		ESC = 27,
		FIRST_CODE = 224,
		UP = 72,
		DOWN = 80,
		LEFT = 75,
		RIGHT = 77,
		ENTER = 13
	};
	int code;
	short y = row / 2;
	short x = col / 2;
	short mid = col / 2;
	short move = 0;
	bool crossMove = true;
	bool win = false;
	bool theEnd = false;
	char symb = 'x';
	char symbConst;
	arr[y][x] = symb;
	while (true) {
		ShowField(arr, row, col, y, x, winCross, winZero);
		if (move >= 5) {
			win = IsWin(arr, row, col);
		}
		if (move == 9) {
			theEnd = true;
		}
		if (win) {
			crossMove = 1 - crossMove;
			crossMove ? ++winCross : ++winZero;
			cout << (crossMove ? "�������� ��������\n\n" : "������ ��������\n\n");
			break;
		}
		if (theEnd) {
			cout << "�����\n\n";
			break;
		}
		symb = crossMove ? 'x' : 'o';
		symbConst = crossMove ? 'X' : 'O';
		if (symb == 'x') {
			code = _getch();
		}
		else {
			code = 88;
		}
		if (code == 88) {
			y = rand() % 3;
			x = rand() % 3;
			if (x == 0) {
				x = 1;
			}
			else if (x == 1) {
				x = 5;
			}
			else {
				x = 9;
			}
			if (arr[y][x] != 'X' && arr[y][x] != 'O') {
				arr[y][x] = symbConst;
				crossMove = 1 - crossMove;
				++move;
			}
		}
		if (code == ESC) {
			cout << "\nBye!\n";
			break;
		}
		if (code == ENTER) {
			if (arr[y][x] == symb) {
				arr[y][x] = symbConst;
				crossMove = 1 - crossMove;
				++move;
			}
			continue;
		}
		if (code == FIRST_CODE) {
				code = _getch();
			switch (code) {
			case UP:
				if (y > 0) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					if (arr[--y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case DOWN:
				if (y < row - 1) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					if (arr[++y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case LEFT:
				if (x == mid || x == mid + 4) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					x -= 4;
					if (arr[y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case RIGHT:
				if (x == mid - 4 || x == mid) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					x += 4;
					if (arr[y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			}
		}
	}
	return code;
}

int TicTacToeCompFirst(char arr[][11], int row, int col) {
	static int winCross;
	static int winZero;
	CreateField(arr, row, col);
	enum codes {
		ESC = 27,
		FIRST_CODE = 224,
		UP = 72,
		DOWN = 80,
		LEFT = 75,
		RIGHT = 77,
		ENTER = 13
	};
	int code;
	short y = row / 2;
	short x = col / 2;
	short mid = col / 2;
	short move = 0;
	bool crossMove = true;
	bool win = false;
	bool theEnd = false;
	char symb = 'x';
	char symbConst;
	while (true) {
		ShowField(arr, row, col, y, x, winCross, winZero);
		if (move >= 5) {
			win = IsWin(arr, row, col);
		}
		if (move == 9) {
			theEnd = true;
		}
		if (win) {
			crossMove = 1 - crossMove;
			crossMove ? ++winCross : ++winZero;
			cout << (crossMove ? "�������� ��������\n\n" : "������ ��������\n\n");
			break;
		}
		if (theEnd) {
			cout << "�����\n\n";
			break;
		}
		symb = crossMove ? 'x' : 'o';
		symbConst = crossMove ? 'X' : 'O';
		if (symb == 'o') {
			code = _getch();
		}
		else {
			code = 88;
		}
		if (code == 88) {
			y = rand() % 3;
			x = rand() % 3;
			if (x == 0) {
				x = 1;
			}
			else if (x == 1) {
				x = 5;
			}
			else {
				x = 9;
			}
			if (arr[y][x] != 'X' && arr[y][x] != 'O') {
				arr[y][x] = symbConst;
				crossMove = 1 - crossMove;
				++move;
			}
		}
		if (code == ESC) {
			cout << "\nBye!\n";
			break;
		}
		if (code == ENTER) {
			if (arr[y][x] == symb) {
				arr[y][x] = symbConst;
				crossMove = 1 - crossMove;
				++move;
			}
			continue;
		}
		if (code == FIRST_CODE) {
			code = _getch();
			switch (code) {
			case UP:
				if (y > 0) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					if (arr[--y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case DOWN:
				if (y < row - 1) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					if (arr[++y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case LEFT:
				if (x == mid || x == mid + 4) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					x -= 4;
					if (arr[y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			case RIGHT:
				if (x == mid - 4 || x == mid) {
					if (arr[y][x] == symb) {
						arr[y][x] = '_';
					}
					x += 4;
					if (arr[y][x] == '_') {
						arr[y][x] = symb;
					}
				}
				break;
			}
		}
	}
	return code;
}

void CreateField(char arr[][11], int row, int col) {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			if (j == 3 || j == 7) {
				arr[i][j] = char(179);
			}
			else {
				arr[i][j] = '_';
			}
		}
	}
}

void ShowField(char arr[][11], int row, int col, short y, short x, int winCross, int winZero) {
	system("cls");
	cout << "\n\t\t\t    ��������        ������\n\n\t\t\t        " << winCross << "\t       " << winZero << "\n\t\t\t          ";
	setlocale(LC_ALL, "C");
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			if (i == y && j == x) {
				SetConsoleTextAttribute(handle, 15);
				cout << arr[i][j];
				SetConsoleTextAttribute(handle, 7);
				continue;
			}
			cout << arr[i][j];
		}
		cout << "\n\t\t\t          ";
	}
	setlocale(LC_ALL, "rus");
	cout << endl;
}

bool IsWin(char arr[][11], int row, int col) {
	int sum;
	for (int i = 0; i < row; ++i) {
		sum = 0;
		for (int j = 1; j < col; j += 4) {
			sum += arr[i][j];
		}
		if (sum / 3 == 88 || sum / 3 == 79) {
			return true;
		}
	}
	for (int i = 1; i < col; i += 4) {
		sum = 0;
		for (int j = 0; j < row; ++j) {
			sum += arr[j][i];
		}
		if (sum / 3 == 88 || sum / 3 == 79) {
			return true;
		}
	}
	sum = 0;
	for (int i = 0, j = 1; i < row; ++i, j += 4) {
		sum += arr[i][j];
	}
	if (sum / 3 == 88 || sum / 3 == 79) {
		return true;
	}
	sum = 0;
	for (int i = 0, j = col - 2; i < row; ++i, j -= 4) {
		sum += arr[i][j];
	}
	if (sum / 3 == 88 || sum / 3 == 79) {
		return true;
	}
	return false;
}