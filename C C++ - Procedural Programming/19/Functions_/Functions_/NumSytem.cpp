#include<iostream>
#include"Functions.h"

using namespace std;



/*3. �������� ������� ��� �������� ����������� ����� � ������ ������� ���������� (2-36)*/

void GetNonDecNum(int num) {
	short syst = GetNumSyst();
	const int SIZE = 32;
	int temp;
	int copyNum = num;
	bool neg = false;
	char arr[SIZE]{};
	if (num < 0) {
		num = -num;
		neg = true;
	}
	for (int i = SIZE - 1; num != 0; --i) {
			temp = num % syst;
			arr[i] = temp < 10 ? temp + 48 : temp + 55;
			num /= syst;
	}
	ShowNonDecNum(arr, copyNum, SIZE, syst, neg);
}

int GetNum() {
	int num;
	cout << "������� �����\n";
	cin >> num;
	return num;
}

short GetNumSyst() {
	short syst = 10;
	while (syst == 10 || syst < 2 || syst > 36) {
		cout << "������� ������� ��������� (2 - 36)\n";
		cin >> syst;
	}
	return syst;
}

void ShowNonDecNum(char arr[], int num, int size, short syst, bool neg) {
	system("cls");
	cout << endl << num << "  10--->" << syst << "  ";
	short index;
	for (int i = 0; i < size; ++i) {
		if (arr[i] != 0) {
			index = i;
			break;
		}
	}
	if (neg) {
		cout << "-";
	}
	for (int i = index; i < size; ++i) {
		cout << arr[i];
	}
	cout << endl << endl;
}


/*4. �������� ������� ��� �������� �����, ����������� � �������� ����, � ���������� �������������*/

int GetDecNum(long long num) {
	short index;
	const int SIZE = 19;
	int arr[SIZE]{};
	InitArrayForBin(arr, SIZE);
	for (int i = SIZE - 1; i >= 0; --i) {
		arr[i] = num % 10;
		num /= 10;
		if (num == 0) {
			break;
		}
	}
	for (int i = 0; i < SIZE; ++i) {
		if (arr[i] != 2) {
			index = i;
			break;
		}
	}
	for (int i = index; i < SIZE; ++i) {
		num = num * 2 + arr[i];
	}
	return num;
}

long long GetBinNum() {
	system("cls");
	cout << "�������� ������� ��� �������� �����, ����������� � �������� ����, � ����������  �������������\n\n";
	long long num;
	cout << "������� ����� � �������� ����\n";
	cin >> num;
	CheckNum(num);
	return num;
}

void CheckNum(long long num) {
	while (num != 0) {
		if (num % 10 < 0 || num % 10 > 1) {
			GetBinNum();
			return;
		}
		num /= 10;
	}
}

void InitArrayForBin(int arr[], int size) {
	for (int i = 0; i < size; ++i) {
		arr[i] = 2;
	}
}