#include"Header.h"
#include<iostream>
#include<time.h>
#include<iomanip>					// � ������� ����� ������� ����� ����� ������������, ���� � ��������� �������� ����� ���� ������� ������������ ��������� (������ ������� �� ������� �� ��������), ��� �
#include<windows.h>					// ������������ ���������� �������� ������ (���� ���������� ���������), ����� ��������� � �������� ������ ����� �������, � �� ��������� �� ������ ������� - ��� ���������� 
									// ������������������ ������ ������� - �������� ������� � ����������� �� ��������� (����� ����� � ������� ��� ����� ������ �� ����)
using namespace std;

void Next() {
	system("pause");
	system("cls");
}



void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	
	/*1. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ����� � ������������ ��� ��������� � ���� ����������-����������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ������ � ��� ������, �          ���������� ����� � ������������ ��� ��������� � ���� ����������-����������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};		// � ������� ����� ������� ����� ����� ������������
		InitArray(p_arrD, size);
		ShowArray(p_arrD, size);
		cout << endl;
		int *p_sumD = new int(0);
		int *p_multD = new int(1);
		GetSumMulti(p_arrD, size, p_sumD, p_multD);
		cout << "����� ��������� = " << *p_sumD << endl;
		cout << "������������ ��������� = " << *p_multD << endl << endl;

		delete[] p_arrD;
		delete p_sumD;
		delete p_multD;
		p_arrD = p_sumD = p_multD = nullptr;   // �������������, �.�. ������ ��������� ����� ��������� ����� �����
	}
	Next();

	/*2. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ���������� �������������, ������������� � ������� ��������� �������*/
	
	{
		cout << "�������� �������, ������� �������� ��������� �� ������ � ��� ������, �          ���������� ���������� �������������, ������������� � ������� ��������� �������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};
		InitArray(p_arrD, size, -9);
		ShowArray(p_arrD, size, 3);
		cout << endl;
		int *p_posD = new int(0);
		int *p_negD = new int(0);
		int *p_zeroD = new int(0);
		GetPosNegZero(p_arrD, size, p_posD, p_negD, p_zeroD);
		cout << "���������� ������������� ��������� " << *p_posD << endl;
		cout << "���������� ������������� ��������� " << *p_negD << endl;
		cout << "���������� ������� ��������� " << *p_zeroD << endl << endl;

		delete[] p_arrD;
		delete p_posD;
		delete p_negD;
		delete p_zeroD;
		p_arrD = p_posD = p_negD = p_zeroD = nullptr;
	}
	Next();

	/*3. �������� �������, ������� �������� ��������� �� ��������� ������ � ��� ������, � ���������� ������� �������������� ��������� �������, � ����� ���������� ������ � �������� ���������*/
	
	// ������ ������� (��������� ������������ ������) - ������� ��������� ��������� �� ���������

	{
		cout << "�������� �������, ������� �������� ��������� �� ��������� ������ � ��� ������, ����������� ������� �������������� ��������� �������, � ����� ���������� ������ ��������� ���������\n\n";
		int size = GetSize();
		cout << endl;
		int **pp_arrD = new int*[size];
		for (int i = 0; i < size; ++i) {
			pp_arrD[i] = new int[size] {};
		}
		InitArrayTwoDim(pp_arrD, size);
		ShowArrayTwoDim(pp_arrD, size);
		cout << endl;
		double *p_arithMeanD = new double(0);
		int *p_evenD = new int(0);
		int *p_unevenD = new int(0);
		GetArithMeanAndCountEvenUneven(pp_arrD, size, p_arithMeanD, p_evenD, p_unevenD);
		cout << "������� �������������� = " << *p_arithMeanD << endl;
		cout << "���������� ������ ��������� " << *p_evenD << endl;
		cout << "��������� �������� ��������� " << *p_unevenD << endl << endl;
		
		for (int i = 0; i < size; ++i) {
			delete[] pp_arrD[i];
		}
		delete[] pp_arrD;
		delete p_arithMeanD;
		delete p_evenD;
		delete p_unevenD;

		pp_arrD = nullptr;
		p_arithMeanD = nullptr;
		p_evenD = p_unevenD = nullptr;
	}
	Next();

	// ������ ������� (������ ����������) - ������� ���������� ��������� �� ���������

	{
		const int SIZE = 5;
		int *p_arrD1 = new int[SIZE] {};
		int *p_arrD2 = new int[SIZE] {};
		int *p_arrD3 = new int[SIZE] {};
		int *p_arrD4 = new int[SIZE] {};
		int *p_arrD5 = new int[SIZE] {};
		int *p_arrPtr[] = { p_arrD1, p_arrD2, p_arrD3, p_arrD4, p_arrD5 };
		cout << endl;
		InitArrayTwoDim(p_arrPtr, SIZE);
		ShowArrayTwoDim(p_arrPtr, SIZE);
		cout << endl;
		double *p_arithMeanD = new double(0);
		int *p_evenD = new int(0);
		int *p_unevenD = new int(0);
		GetArithMeanAndCountEvenUneven(p_arrPtr, SIZE, p_arithMeanD, p_evenD, p_unevenD);
		cout << "������� �������������� = " << *p_arithMeanD << endl;
		cout << "���������� ������ ��������� " << *p_evenD << endl;
		cout << "��������� �������� ��������� " << *p_unevenD << endl << endl;

		delete p_arrD1; delete p_arrD2; delete p_arrD3;  delete p_arrD4; delete p_arrD5;
		delete p_arithMeanD;
		delete p_evenD;
		delete p_unevenD;

		p_arithMeanD = nullptr;
		p_evenD = p_unevenD = p_arrD1 = p_arrD2 = p_arrD3 = p_arrD4 = p_arrD5 = nullptr;
	}
	Next();

	// ������ ������� (��������� �� ��������� ����������� ������) - ������� ��� ���������� �� ���������

	{
		const int SIZE = 5;
		int arr[SIZE][SIZE]{};
		int *p_arr = *arr;
		// ����� �������, ��� ������ ��������� ������ ���� ���������� - ���������� � ��������� ����� � ������� ������ ������� (���������� � ������ �������� �������� ������ ������)
		// (� ����������� ��������� �������, ��� � � ����������, �������� ���� ���� �� ������ ����� �������, � ������ ��� � ��������, ������� - �������� �������)
		for (int i = 0; i < SIZE * SIZE; ++i) {
			p_arr[i] = rand() % 9 + 1;
		}
		for (int i = 0; i < SIZE * SIZE; ++i) {
			if (i % SIZE == 0) {
				cout << endl;
			}
			cout << setw(2) << p_arr[i];
		}
		cout << endl << endl;
		double *p_arithMeanD = new double(0);
		int *p_evenD = new int(0);
		int *p_unevenD = new int(0);
		GetArithMeanAndCountEvenUneven_(p_arr, SIZE * SIZE, p_arithMeanD, p_evenD, p_unevenD);
		cout << "������� �������������� = " << *p_arithMeanD << endl;
		cout << "���������� ������ ��������� " << *p_evenD << endl;
		cout << "��������� �������� ��������� " << *p_unevenD << endl << endl;

		delete p_arithMeanD;
		delete p_evenD;
		delete p_unevenD;

		p_arithMeanD = nullptr;
		p_evenD = p_unevenD = nullptr;
	}
	Next();

	/*4. �������� �������, ����������� � �������� ���������, ��������� �� ��� ������� (� � �) � ������� ��������. ������� ���������, �������� �� ������ � ������������� ������� � � ����������
	     ��������� �� ������ ���������� ���������, ���� ���������� 0 � ��������� ������*/

	{
		cout << "�������� �������, ����������� � �������� ���������, ��������� �� ��� ������� (� � �) � ������� ��������. ������� ���������, �������� �� ������ � �������������  ������� � � ���������� ��������� �� ������ ���������� ���������, ���� ����������0 � ��������� ������\n\n";
		int sizeA = GetSize();
		int sizeB = GetSize();
		if (sizeA < sizeB) {
			swap(sizeA, sizeB);
		}
		int *A = new int[sizeA] {};
		int *B = new int[sizeB] {};
		InitArray(A, sizeA);
		InitArray(B, sizeB);
		cout << "\n������ A\n\n";
		ShowArray(A, sizeA);
		cout << endl;
		cout << "������� B\n\n";
		ShowArray(B, sizeB);
		cout << endl;
		int *p_subset = GetPtrSubset(A, B, sizeA, sizeB);
		if (p_subset) {
			cout << "������ B �������� ������������� ������� A\n\n";
			for (int i = 0; i < sizeA; ++i) {
				if (A + i == p_subset) {
					SetConsoleTextAttribute(handle, 2);
				}
				if (A + i == p_subset + sizeB) {
					SetConsoleTextAttribute(handle, 7);
				}
				cout << setw(2) << A[i];
			}
			cout << endl;
			cout << "\n��������� ��������:\n\n";
			for (int i = 0; i < sizeB; ++i) {
				cout << setw(2) << *(p_subset + i);
			}
			cout << endl << endl;
		}
		else {
			cout << "������ B �� �������� ������������� ������� A\n\n";
		}

		delete[] A;
		delete[] B;
		A = B = p_subset = nullptr;
	}
	Next();

	/*5. �������� ��������� ������� ��� ������ � ������������ ��������:
		 a. ������� ������������� ������������ ������
		 b. ������� ������������� ������������� �������
		 c. ������� ������ ������������� �������
		 d. ������� �������� ������������� �������
		 e. ������� ���������� �������� � ����� �������
		 f. ������� ������� �������� �� ���������� �������
		 g. ������� �������� �������� �� ���������� ������� */

	{
	cout << "�������� ��������� ������� ��� ������ � ������������ ��������:\na.������� ������������� ������������ ������\nb.������� ������������� ������������� �������\nc.������� ������ ������������� �������\nd.������� �������� ������������� �������\ne.������� ���������� �������� � ����� �������\nf.������� ������� �������� �� ���������� �������\ng.������� �������� �������� �� ���������� �������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arr = nullptr;
		// a
		//Calloc(&p_arr, size);						 // ������������������ ������ ������� - �������� ������� � ����������� �� ���������			
		p_arr = Malloc(p_arr, size);		
		// b
		InitArrayD(p_arr, size, 1, 9);
		// c
		ShowArrayD(p_arr, size, 2);
		cout << "\ne.\n";
		int num = GetNum();
		cout << endl;
		// e
		//AddToArrayEnd_(&p_arr, &size, num);
		p_arr = AddToArrayEnd(p_arr, &size, num);
		ShowArrayD(p_arr, size, num > 99 ? 4 : num > 9 ? 3 : 2);
		cout << "\nf.\n";
		num = GetNum();
		cout << endl;
		int index;
		do {
			index = GetInd();
		} while (index >= size);
		cout << endl;
		// f
		//PasteToArrayInd_(&p_arr, &size, num, index);
		p_arr = PasteToArrayInd(p_arr, &size, num, index);
		ShowArrayD(p_arr, size, num > 99 ? 4 : num > 9 ? 3 : 2);
		cout << "\ng.\n";
		do {
			index = GetInd();
		} while (index >= size);
		cout << endl;
		// g
		//DelElemInArrayInd_(&p_arr, &size, index);
		p_arr = DelElemInArrayInd(p_arr, &size, index);
		ShowArrayD(p_arr, size, num > 99 ? 4 : num > 9 ? 3 : 2);
		cout << endl;
		// d
		//FreeNull(&p_arr);
		Free(p_arr);
		p_arr = nullptr;
	}
	Next();

	/*6. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������. ������� ������ ������� �� ������� ��� ������������� ����� � ������� ��������� �� ����� ������������ ������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ������������ ������ � ���       ������. ������� ������ ������� �� ������� ��� ������������� ����� � �������     ��������� �� ����� ������������ ������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};
		InitArrayD(p_arrD, size, -9, 9);
		ShowArrayD(p_arrD, size, 3);
		cout << endl;
		p_arrD = DelNegNums(p_arrD, &size);
		cout << " ����� ������:\n\n";
		ShowArrayD(p_arrD, size, 3);
		cout << endl;

		delete[] p_arrD;
		p_arrD = nullptr;
	}
	Next();

	/*7. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� �������� ����� ��������*/

	{
		cout << "���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� ��������   ����� ��������\n\n";
		int M = GetSize();
		cout << endl;
		int *A = new int[M] {};
		InitArrayD(A, M, 1, 9);
		cout << "������ A:\n\n";
		ShowArrayD(A, M, 2);
		cout << endl;
		int N = GetSize();
		cout << endl;
		int *B = new int[N] {};
		InitArrayD(B, N, 1, 9);
		cout << "������ B:\n\n";
		ShowArrayD(B, N, 2);
		cout << endl;
		int size = 0;
		int *AB = nullptr;
		for (int i = 0; i < M; ++i) {
			//AddToArrayEnd_(&AB, &size, A[i]);
			AB = AddToArrayEnd(AB, &size, A[i]);
		}
		for (int i = 0; i < N; ++i) {
			//AddToArrayEnd_(&AB, &size, B[i]);
			AB = AddToArrayEnd(AB, &size, B[i]);
		}
		cout << "������ AB:\n\n";
		ShowArrayD(AB, size, 2);
		cout << endl;

		delete[] A;
		delete[] B;
		delete[] AB;
		A = B = AB = nullptr;
	}
	Next();

	/*8. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� ����� �������� ���� �������� ��� ����������*/

	{
		cout << "���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� �����      �������� ���� �������� ��� ����������\n\n";
		int M = GetSize();
		cout << endl;
		int *A = new int[M] {};
		InitArrayD(A, M, 1, 9);
		cout << "������ A:\n\n";
		ShowArrayD(A, M, 2);
		cout << endl;
		int N = GetSize();
		cout << endl;
		int *B = new int[N] {};
		InitArrayD(B, N, 1, 9);
		cout << "������ B:\n\n";
		ShowArrayD(B, N, 2);
		cout << endl;
		int size = 0;
		int *AB = nullptr;
		for (int i = 0; i < N; ++i) {
			if (A[0] == B[i]) {
				//AddToArrayEnd_(&AB, &size, A[0]);
				AB = AddToArrayEnd(AB, &size, A[0]);
				break;
			}
		}
		for (int i = 1; i < M; ++i) {
			bool flag = true;
			for (int j = 0; j < i; ++j) {
				if (A[i] == A[j]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				for (int j = 0; j < N; ++j) {
					if (A[i] == B[j]) {
						//AddToArrayEnd_(&AB, &size, A[i]);
						AB = AddToArrayEnd(AB, &size, A[i]);
						break;
					}
				}
			}
		}
		if (size == 0) {
		cout << "� �������� ���� ����� ���������\n";
		}
		else {
			cout << "������ AB:\n\n";
			ShowArrayD(AB, size, 2);
		}
		cout << endl;

		delete[] A;
		delete[] B;
		delete[] AB;

		A = B = AB = nullptr;
	}
	Next();

	/*9. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� �������� ������� A, ������� �� ���������� � ������ B, ��� ����������*/

	{
		cout << "���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� ��������   ������� A, ������� �� ���������� � ������ B, ��� ����������\n\n";
		int M = GetSize();
		cout << endl;
		int *A = new int[M] {};
		InitArrayD(A, M, 1, 9);
		cout << "������ A:\n\n";
		ShowArrayD(A, M, 2);
		cout << endl;
		int N = GetSize();
		cout << endl;
		int *B = new int[N] {};
		InitArrayD(B, N, 1, 9);
		cout << "������ B:\n\n";
		ShowArrayD(B, N, 2);
		cout << endl;
		int size = 0;
		int *AB = nullptr;
		bool flag = true;
		for (int i = 0; i < N; ++i) {
			if (A[0] == B[i]) {
				flag = false;
				break;
			}
		}
		if (flag) {
			//AddToArrayEnd_(&AB, &size, A[0]);
			AB = AddToArrayEnd(AB, &size, A[0]);
		}
		for (int i = 1; i < M; ++i) {
			flag = true;
			for (int j = 0; j < i; ++j) {
				if (A[j] == A[i]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				for (int j = 0; j < N; ++j) {
					if (A[i] == B[j]) {
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				//AddToArrayEnd_(&AB, &size, A[i]);
				AB = AddToArrayEnd(AB, &size, A[i]);
			}
		}
		if (size == 0) {
			cout << "��� �������� ������� A ����������� � ������� B\n";
		}
		else {
			cout << "������ AB:\n\n";
			ShowArrayD(AB, size, 2);
		}
		cout << endl;

		delete[] A;
		delete[] B;
		delete[] AB;
		A = B = AB = nullptr;
	}
	Next();

	/*10. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� �������� �������� A � B, ������� �� �������� ������ ��� ���, ��� ����������*/

	{
		cout << "10. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ����������     ������� ������ ������ ���������� ���������� �������, � ������� ����� �������    �������� �������� A � B, ������� �� �������� ������ ��� ���, ��� ����������\n\n";
		int M = GetSize();
		cout << endl;
		int *A = new int[M] {};
		InitArrayD(A, M, 1, 9);
		cout << "������ A:\n\n";
		ShowArrayD(A, M, 2);
		cout << endl;
		int N = GetSize();
		cout << endl;
		int *B = new int[N] {};
		InitArrayD(B, N, 1, 9);
		cout << "������ B:\n\n";
		ShowArrayD(B, N, 2);
		cout << endl;
		int size = 0;
		int *AB = nullptr;
		bool flag = true;
		for (int i = 0; i < N; ++i) {
			if (A[0] == B[i]) {
				flag = false;
				break;
			}
		}
		if (flag) {
			//AddToArrayEnd_(&AB, &size, A[0]);
			AB = AddToArrayEnd(AB, &size, A[0]);
		}
		for (int i = 1; i < M; ++i) {
			flag = true;
			for (int j = 0; j < i; ++j) {
				if (A[j] == A[i]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				for (int j = 0; j < N; ++j) {
					if (A[i] == B[j]) {
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				//AddToArrayEnd_(&AB, &size, A[i]);
				AB = AddToArrayEnd(AB, &size, A[i]);
			}
		}
		flag = true;
		for (int i = 0; i < M; ++i) {
			if (B[0] == A[i]) {
				flag = false;
				break;
			}
		}
		if (flag) {
			//AddToArrayEnd_(&AB, &size, B[0]);
			AB = AddToArrayEnd(AB, &size, B[0]);
		}
		for (int i = 1; i < N; ++i) {
			flag = true;
			for (int j = 0; j < i; ++j) {
				if (B[j] == B[i]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				for (int j = 0; j < M; ++j) {
					if (B[i] == A[j]) {
						flag = false;
						break;
					}
				}
			}
			if (flag) {
				//AddToArrayEnd_(&AB, &size, B[i]);
				AB = AddToArrayEnd(AB, &size, B[i]);
			}
		}
		if (size == 0) {
			cout << "��� �������� �������� �������� ������\n";
		}
		else {
			cout << "������ AB:\n\n";
			ShowArrayD(AB, size, 2);
		}
		cout << endl;

		delete[] A;
		delete[] B;
		delete[] AB;
		A = B = AB = nullptr;
	}
	Next();

	/*11. ������� �������, ����������� ��������� ���� ��������� � ����� �������*/

	{
		cout << "������� �������, ����������� ��������� ���� ��������� � ����� �������\n\n";
		int sizeArr = GetSize();
		cout << endl;
		int *p_arrD = new int[sizeArr] {};
		InitArrayD(p_arrD, sizeArr, 1, 9);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;
		cout << "���� ���������:\n\n";
		int sizeBk = GetSize();
		cout << endl;
		int *p_blockD = new int[sizeBk] {};
		InitArrayD(p_blockD, sizeBk, 1, 9);
		ShowArrayD(p_blockD, sizeBk, 2);
		cout << endl;
		//AddBlockToArrayEnd_(&p_arrD, &sizeArr, p_blockD, sizeBk);
		p_arrD = AddBlockToArrayEnd(p_arrD, &sizeArr, p_blockD, sizeBk);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;

		delete[] p_arrD;
		delete[] p_blockD;
		p_arrD = p_blockD = nullptr;
	}
	Next();

	/*12. ������� �������, ����������� ��������� ���� ���������, ������� � ������������� ������� �������*/

	{
		cout << "12. ������� �������, ����������� ��������� ���� ���������, ������� �            ������������� ������� �������\n\n";
		int sizeArr = GetSize();
		cout << endl;
		int *p_arrD = new int[sizeArr] {};
		InitArrayD(p_arrD, sizeArr, 1, 9);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;
		cout << "���� ���������:\n\n";
		int sizeBk = GetSize();
		cout << endl;
		int *p_blockD = new int[sizeBk] {};
		InitArrayD(p_blockD, sizeBk, 1, 9);
		ShowArrayD(p_blockD, sizeBk, 2);
		cout << endl;
		int index;
		do {
			index = GetInd();
		} while (index > sizeArr);
		cout << endl;
		//PasteBlockToArrayInd_(&p_arrD, &sizeArr, p_blockD, sizeBk, index);
		p_arrD = PasteBlockToArrayInd(p_arrD, &sizeArr, p_blockD, sizeBk, index);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;

		delete[] p_arrD;
		delete[] p_blockD;
		p_arrD = p_blockD = nullptr;
	}
	Next();
	
	/*13. ������� �������, ����������� ������� ���� ���������, ������� � ������������� ������� �������*/

	{
		cout << "������� �������, ����������� ������� ���� ���������, ������� � �������������    ������� �������\n\n";
		int sizeArr = GetSize();
		cout << endl;
		int *p_arrD = new int[sizeArr] {};
		InitArrayD(p_arrD, sizeArr, 1, 9);
		ShowArrayD(p_arrD, sizeArr, 2);
		cout << endl;
		int sizeBk;
		cout << "���� ���������:\n\n";
		do {
			sizeBk = GetSize();
		} while (sizeBk > sizeArr);
		cout << endl;
		int index;
		do {
			index = GetInd();
		} while (index + sizeBk > sizeArr);
		cout << endl;
		//DelBlockInArrayInd_(&p_arrD, &sizeArr, sizeBk, index);
		p_arrD = DelBlockInArrayInd(p_arrD, &sizeArr, sizeBk, index);
		if (sizeArr == 0) {
			cout << "��� �������� ������� �������\n\n";
		}
		else {
			ShowArrayD(p_arrD, sizeArr, 2);
		}
		cout << endl;

		delete[] p_arrD;
		p_arrD = nullptr;
	}
	Next();

	/*14. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������. ������� ������ ������� �� ������� ��� ������� ����� � ������� ��������� �� ����� ������������ ������*/

	{
		cout << "�������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������������� ������ ������� �� ������� ��� ������� ����� � ������� ��������� �� ����������������� ������\n\n";
		int size = GetSize();
		cout << endl;
		int *p_arrD = new int[size] {};
		InitArrayD(p_arrD, size, 1, 99);
		ShowArrayD(p_arrD, size, 3);
		cout << endl;
		p_arrD = DelPrimeNums_(p_arrD, &size);
		//p_arrD = DelPrimeNums(p_arrD, &size);
		ShowArrayD(p_arrD, size, 3);
		cout << endl;

		delete[] p_arrD;
		p_arrD = nullptr;
	}
	Next();

	/*15. �������� �������, ������� �������� ��������� �� ����������� ������ � ��� ������. ������� ������������ �������������, ������������� � ������� �������� � ��������� ������������ �������*/

	// ��� ������������ �������, ������ ������� ���������� � �������� �� � ������� (��������� ������� - ��������� �� ���������)

	{
		cout << "�������� �������, ������� �������� ��������� �� ����������� ������ � ��� ������.������� ������������ �������������, ������������� � ������� �������� � ��������������������� �������\n\n";
		const int SIZE = 10; 
		int arr[SIZE]{};
		InitArray(arr, SIZE, -9, 9);
		ShowArray(arr, SIZE, 3);
		cout << endl;
		int *p_arrPos, *p_arrNeg, *p_arrZero;
		AllocPosNegZero(arr, SIZE, &p_arrPos, &p_arrNeg, &p_arrZero);
		cout << "������ � �������������� ����������:\n\n";
		ShowArrayD(p_arrPos + 1, p_arrPos[0], 2);
		cout << endl;
		cout << "������ � �������������� ����������:\n\n";
		ShowArrayD(p_arrNeg + 1, p_arrNeg[0], 3);
		cout << endl;
		cout << "������ � �������� ����������:\n\n";
		ShowArrayD(p_arrZero + 1, p_arrZero[0], 2);
		cout << endl;

		delete[] p_arrPos;
		delete[] p_arrNeg;
		delete[] p_arrZero;
		p_arrPos = p_arrNeg = p_arrZero = nullptr;
	}
	Next();
	
	// ������ ����������

	{
		const int SIZE = 10;
		int arr[SIZE]{};
		InitArray(arr, SIZE, -9, 9);
		ShowArray(arr, SIZE, 3);
		cout << endl;
		int *p_arrPtr[3]{};
		AllocPosNegZero_(arr, SIZE, p_arrPtr);
		cout << "������ � �������������� ����������:\n\n";
		ShowArrayD(p_arrPtr[0] + 1, *(p_arrPtr[0]), 2);
		cout << endl;
		cout << "������ � �������������� ����������:\n\n";
		ShowArrayD(p_arrPtr[1] + 1, *(p_arrPtr[1]), 3);
		cout << endl;
		cout << "������ � �������� ����������:\n\n";
		ShowArrayD(p_arrPtr[2] + 1, *(p_arrPtr[2]), 2);
		cout << endl;

		delete[] p_arrPtr[0];
		delete[] p_arrPtr[1];
		delete[] p_arrPtr[2];
		p_arrPtr[0] = p_arrPtr[1] = p_arrPtr[2] = nullptr;
	}
	Next();
	
	// ����������� ��������� �� ��������� ����� ������� ����������, ������� ���������� �������

	{
		const int SIZE = 10;
		int arr[SIZE]{};
		InitArray(arr, SIZE, -9, 9);
		ShowArray(arr, SIZE, 3);
		cout << endl;
		int sizePos = 0, sizeNeg = 0, sizeZero = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] > 0 ? ++sizePos : arr[i] < 0 ? ++sizeNeg : ++sizeZero;
		}
		int **pp_arr = _AllocPosNegZero_(arr, SIZE);
		cout << "������ � �������������� ����������:\n\n";
		ShowArrayD(pp_arr[0] + 1, *(pp_arr[0]), 2);
		cout << endl;
		cout << "������ � �������������� ����������:\n\n";
		ShowArrayD(pp_arr[1] + 1, *(pp_arr[1]), 3);
		cout << endl;
		cout << "������ � �������� ����������:\n\n";
		ShowArrayD(pp_arr[2] + 1, *(pp_arr[2]), 2);
		cout << endl;

		delete[] pp_arr[0];
		delete[] pp_arr[1];
		delete[] pp_arr[2];
		pp_arr[0] = pp_arr[1] = pp_arr[2] = nullptr;
	}
	Next();
	
	// ��� ���������� �� ���������, �� ������� ����� ������ � �������, ��� � ������� �� �����

	{
		const int SIZE = 10;
		int arr[SIZE]{};
		InitArray(arr, SIZE, -9, 9);
		ShowArray(arr, SIZE, 3);
		cout << endl;
		AllocPosNegZeroNoPP(arr, SIZE);
	}

		
	
}