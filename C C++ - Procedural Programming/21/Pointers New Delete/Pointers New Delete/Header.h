#pragma once

int GetSize();
int GetNum();
int GetInd();

template<typename T>
void InitArray(T *p_arr, int size, int min = 1, int max = 9) {
	for (int i = 0; i < size; ++i) {
		p_arr[i] = rand() % (max - min + 1) + min;
	}
}

template<typename T>
void ShowArray(T *p_arr, int size, int width = 2) {
	for (int i = 0; i < size; ++i) {
		cout << setw(width) << p_arr[i];
	}
	cout << endl;
}

template<typename T>
void InitArrayTwoDim(T **pp_arr, int size, int min = 1, int max = 9) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			pp_arr[i][j] = rand() % (max - min + 1) + min;
		}
	}
}

template<typename T>
void ShowArrayTwoDim(T **pp_arr, int size, int width = 2) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			cout << setw(width) << pp_arr[i][j];
		}
		cout << endl;
	}
}

void GetSumMulti(int *p_arr, int size, int *p_sum, int *p_mult);

void GetPosNegZero(int *p_arr, int size, int *p_pos, int *p_neg, int *p_zero);

void GetArithMeanAndCountEvenUneven(int **pp_arr, int size, double *p_arithMean, int *p_even, int *p_uneven);
void GetArithMeanAndCountEvenUneven_(int *pp_arr, int size, double *p_arithMean, int *p_even, int *p_uneven);

int* GetPtrSubset(int *p_arr1, int *p_arr2, int size1, int size2);

int* DelNegNums(int *p_arr, int *size);

/*a. ������� ������������� ������������ ������*/

// ������ �������

template<typename T>
T* Malloc(T *ptr, int size) {		// �������� ��������� ������ ��� ����������� ����
	return new T[size]{};
}

// ������ ������� (����� ��������� �� ���������)

template<typename T>
void Calloc(T **p_ptr, int size) {
	delete[] * p_ptr;
	*p_ptr = new T[size]{};
}

/*b. ������� ������������� ������������� �������*/

template<typename T>
void InitArrayD(T *p_arr, int size, int min, int max) {
	for (int i = 0; i < size; ++i) {
		*(p_arr + i) = T((rand() % (max - min + 1) + min) + (rand() % 100 / 100.));
	}
}

/* c. ������� ������ ������������� �������*/

template<typename T>
void ShowArrayD(T *p_arr, int size, int width) {
	for (int i = 0; i < size; ++i) {
		cout << setw(width) << *(p_arr + i);
	}
	cout << endl;
}

/* d. ������� �������� ������������� �������*/

// ������ �������

template<typename T>
void Free(T *ptr) {
	delete[] ptr;
}

// ������ �������

template<typename T>
void FreeNull(T **p_ptr) {
	delete[] * p_ptr;
	*p_ptr = nullptr;
}

/*e. ������� ���������� �������� � ����� �������*/

// ������ �������

template<typename T>
T* AddToArrayEnd(T *p_arr, int *p_size, int num) {
	++*p_size;
	T *p_arrTemp = new T[*p_size]{};
	p_arrTemp[*p_size - 1] = num;
	for (int i = 0; i < *p_size - 1; ++i) {
		p_arrTemp[i] = p_arr[i];
	}

	delete[] p_arr;
	return p_arrTemp;
}

// ������ �������

template <typename T>
void AddToArrayEnd_(T **pp_arr, int *p_size, int num) {
	++*p_size;
	T *p_arrTemp = new T[*p_size]{};
	p_arrTemp[*p_size - 1] = num;
	for (int i = 0; i < *p_size - 1; ++i) {
		p_arrTemp[i] = (*pp_arr)[i];
	}

	delete[] * pp_arr;
	*pp_arr = p_arrTemp;
}

/*f. ������� ������� �������� �� ���������� �������*/

// ������ �������

template<typename T>
T* PasteToArrayInd(T *p_arr, int *p_size, int num, int index) {
	++*p_size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i == index) {
			p_arrTemp[i] = num;
			continue;
		}
		if (i < index) {
			p_arrTemp[i] = p_arr[i];
			continue;
		}
		p_arrTemp[i] = p_arr[i - 1];
	}

	delete[] p_arr;
	return p_arrTemp;
}

// ������ �������

template<typename T>
void PasteToArrayInd_(T **pp_arr, int *p_size, int num, int index) {
	++*p_size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i == index) {
			p_arrTemp[i] = num;
			continue;
		}
		if (i < index) {
			p_arrTemp[i] = (*pp_arr)[i];
			continue;
		}
		p_arrTemp[i] = (*pp_arr)[i - 1];
	}

	delete[] * pp_arr;
	*pp_arr = p_arrTemp;
}

/*g. ������� �������� �������� �� ���������� �������*/

// ������ �������

template<typename T>
T* DelElemInArrayInd(T *p_arr, int *p_size, int index) {
	--*p_size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = p_arr[i];
			continue;
		}
		p_arrTemp[i] = p_arr[i + 1];
	}

	delete[] p_arr;
	return p_arrTemp;
}

// ������ �������

template<typename T>
void DelElemInArrayInd_(T **pp_arr, int *p_size, int index) {
	--*p_size;
	T *p_arrTemp = new T[*p_size];
	for (int i = 0; i < *p_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = (*pp_arr)[i];
			continue;
		}
		p_arrTemp[i] = (*pp_arr)[i + 1];
	}

	delete[] * pp_arr;
	*pp_arr = p_arrTemp;
}

/*11. ������� �������, ����������� ��������� ���� ��������� � ����� �������*/

// ������ �������

template<typename T, typename N>			// ������ ��� ��� ����������� ������ ����� ������� ���� (���� � ��������� � �����������)
T* AddBlockToArrayEnd(T *p_arr, int *p_size, N *p_block, int size) {
	*p_size += size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i < *p_size - size) {
			p_arrTemp[i] = p_arr[i];
			continue;
		}
		p_arrTemp[i] = p_block[i - (*p_size - size)];
	}
	
	delete[] p_arr;
	return p_arrTemp;
}

// ������ �������

template<typename T, typename N>
void AddBlockToArrayEnd_(T **pp_arr, int *p_size, N *p_block, int size) {
	*p_size += size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i < *p_size - size) {
			p_arrTemp[i] = (*pp_arr)[i];
			continue;
		}
		p_arrTemp[i] = p_block[i - (*p_size - size)];
	}

	delete[] * pp_arr;
	*pp_arr = p_arrTemp;
}

/*12. ������� �������, ����������� ��������� ���� ���������, ������� � ������������� ������� �������*/

// ������ �������

template<typename T, typename N>
T* PasteBlockToArrayInd(T *p_arr, int *p_size, N *p_block, int size, int index) {
	*p_size += size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = p_arr[i];
			continue;
		}
		if (i == index) {
			for (int j = 0; j < size; ++j) {
				p_arrTemp[i++] = p_block[j];
			}
			--i;
			continue;
		}
		p_arrTemp[i] = p_arr[i - size];
	}

	delete[] p_arr;
	return p_arrTemp;
}

// ������ �������

template<typename T, typename N>
void PasteBlockToArrayInd_(T **pp_arr, int *p_size, N *p_block, int size, int index) {
	*p_size += size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = (*pp_arr)[i];
			continue;
		}
		if (i == index) {
			for (int j = 0; j < size; ++j) {
				p_arrTemp[i++] = p_block[j];
			}
			--i;
			continue;
		}
		p_arrTemp[i] = (*pp_arr)[i - size];
	}

	delete[] * pp_arr;
	*pp_arr = p_arrTemp;
}

/*13. ������� �������, ����������� ������� ���� ���������, ������� � ������������� ������� �������*/

template<typename T>
T* DelBlockInArrayInd(T *p_arr, int *p_size, int size, int index) {
	*p_size -= size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = p_arr[i];
			continue;
		}
		p_arrTemp[i] = p_arr[i + size];
	}

	delete p_arr;
	return p_arrTemp;
}

template<typename T>
void DelBlockInArrayInd_(T **pp_arr, int *p_size, int size, int index) {
	*p_size -= size;
	T *p_arrTemp = new T[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		if (i < index) {
			p_arrTemp[i] = (*pp_arr)[i];
			continue;
		}
		p_arrTemp[i] = (*pp_arr)[i + size];
	}

	delete[] * pp_arr;
	*pp_arr = p_arrTemp;
}

int* DelPrimeNums(int *p_arr, int *p_size);
int* DelPrimeNums_(int *p_arr, int *p_size);

void AllocPosNegZero(int *p_arr, int size, int **p_arrPos, int **p_arrNeg, int **p_arrZero);
void AllocPosNegZero_(int *p_arr, int size, int **p_arrPtr);
int** _AllocPosNegZero_(int *p_arr, int size);
void AllocPosNegZeroNoPP(int *p_arr, int size);