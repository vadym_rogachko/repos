#include"Header.h"
#include<iostream>
#include<iomanip>

using namespace std;



int GetSize() {
	int size;
	do {
		cout << "������� ������ �������\n";
		cin >> size;
	} while (size < 1);
	return size;
}

int GetNum() {
	int num;
	cout << "������� �����\n";
	cin >> num;
	return num;
}

int GetInd() {
	int index;
	do {
		cout << "������� ������\n";
		cin >> index;
	} while (index < 0);
	return index;
}

/*1. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ����� � ������������ ��� ��������� � ���� ����������-����������*/

void GetSumMulti(int *p_arr, int size, int *p_sum, int *p_mult) {
	for (int i = 0; i < size; ++i) {
		*p_sum += p_arr[i];
	}
	for (int i = 0; i < size; ++i) {
		*p_mult *= p_arr[i];
	}
}

/*2. �������� �������, ������� �������� ��������� �� ������ � ��� ������, � ���������� ���������� �������������, ������������� � ������� ��������� �������*/

void GetPosNegZero(int *p_arr, int size, int *p_pos, int *p_neg, int *p_zero) {
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? (*p_pos)++ : p_arr[i] < 0 ? (*p_neg)++ : (*p_zero)++;
	}
}

/*3. �������� �������, ������� �������� ��������� �� ��������� ������ � ��� ������, � ���������� ������� �������������� ��������� �������, � ����� ���������� ������ � �������� ���������*/

void GetArithMeanAndCountEvenUneven(int **pp_arr, int size, double *p_arithMean, int *p_even, int *p_uneven) {
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			pp_arr[i][j] % 2 == 0 ? (*p_even)++ : (*p_uneven)++;
			*p_arithMean += pp_arr[i][j];
		}
	}
	*p_arithMean /= size * size;
}

void GetArithMeanAndCountEvenUneven_(int *pp_arr, int size, double *p_arithMean, int *p_even, int *p_uneven) {
	for (int i = 0; i < size; ++i) {
			pp_arr[i] % 2 == 0 ? (*p_even)++ : (*p_uneven)++;
			*p_arithMean += pp_arr[i];
	}
	*p_arithMean /= size;
}

/*4. �������� �������, ����������� � �������� ���������, ��������� �� ��� ������� (� � �) � ������� ��������. ������� ���������, �������� �� ������ � ������������� ������� � � ����������
	 ��������� �� ������ ���������� ���������, ���� ���������� 0 � ��������� ������*/

int* GetPtrSubset(int *p_arr1, int *p_arr2, int size1, int size2) {
	for (int i = 0; i < size1; ++i) {
		if (p_arr1[i] == p_arr2[0]) {
			bool flag = true;
			for (int j = 0; j < size2; ++j) {
				if (p_arr1[i + j] != p_arr2[j]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				return p_arr1 + i;
			}
		}
	}
	return nullptr;
}

/*5. �������� ��������� ������� ��� ������ � ������������ ��������:
	 a. ������� ������������� ������������ ������
	 b. ������� ������������� ������������� �������
	 c. ������� ������ ������������� �������
	 d. ������� �������� ������������� �������
	 e. ������� ���������� �������� � ����� �������
	 f. ������� ������� �������� �� ���������� �������
	 g. ������� �������� �������� �� ���������� ������� */

// ��� � ������������ ����� ���������� ���������

/*6. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������. ������� ������ ������� �� ������� ��� ������������� ����� � ������� ��������� �� ����� ������������ ������*/

int* DelNegNums(int *p_arr, int *p_size) {
	for (int i = 0; i < *p_size; ++i) {
		if (p_arr[i] < 0) {
				for (int j = i; j < *p_size - 1; ++j) {
					p_arr[j] = p_arr[j + 1];
				}
				--*p_size, --i;
		}
	}
	int *p_arrTemp = new int[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		p_arrTemp[i] = p_arr[i];
	}

	delete[] p_arr;
	return p_arrTemp;
}

/*7. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� �������� ����� ��������*/
/*8. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� ����� �������� ���� �������� ��� ����������*/
/*9. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� �������� ������� A, ������� �� ���������� � ������ B, ��� ����������*/
/*10. ���� ��� �������: �[M] � B[N] (M � N �������� � ����������). ���������� ������� ������ ������ ���������� ���������� �������, � ������� ����� ������� �������� �������� A � B, ������� �� �������� ������ ��� ���, ��� ����������*/

// � ������� main()

/*11. ������� �������, ����������� ��������� ���� ��������� � ����� �������*/
/*12. ������� �������, ����������� ��������� ���� ���������, ������� � ������������� ������� �������*/
/*13. ������� �������, ����������� ������� ���� ���������, ������� � ������������� ������� �������*/

// � ������������ ����� ���������� ���������

/*14. �������� �������, ������� �������� ��������� �� ������������ ������ � ��� ������. ������� ������ ������� �� ������� ��� ������� ����� � ������� ��������� �� ����� ������������ ������*/

// ������ �������

int* DelPrimeNums(int *p_arr, int *p_size) {
	for (int i = 0; i < *p_size; ++i) {
		bool flag = true;
		int num = 1;
		while (num++ <= p_arr[i] / 2) {
			if (p_arr[i] == 2) {
				flag = false;
				break;
			}
			flag = p_arr[i] % num == 0 ? true : false;
			if (flag) {
				break;
			}
		}
		if (!flag) {
			for (int j = i; j < *p_size - 1; ++j) {
				p_arr[j] = p_arr[j + 1];
			}
			--*p_size, --i;
		}
	}
	int *p_arrTemp = new int[*p_size]{};
	for (int i = 0; i < *p_size; ++i) {
		p_arrTemp[i] = p_arr[i];
	}

	delete[] p_arr;
	return p_arrTemp;
}

// ������ �������

int* DelPrimeNums_(int *p_arr, int *p_size) {
	for (int i = 0; i < *p_size; ++i) {
		bool flag = true;
		int num = 1;
		while (num++ <= p_arr[i] / 2) {
			if (p_arr[i] == 2) {
				flag = false;
				break;
			}
			flag = p_arr[i] % num == 0 ? true : false;
			if (flag) {
				break;
			}
		}
		if (!flag) {
			//DelElemInArrayInd_(&p_arr, p_size, i);
			p_arr = DelElemInArrayInd(p_arr, p_size, i);
			--i;
		}
	}
	return p_arr;
}

/*15. �������� �������, ������� �������� ��������� �� ����������� ������ � ��� ������. ������� ������������ �������������, ������������� � ������� �������� � ��������� ������������ �������*/

// ������ ������� (��������� ��� ������, �������� �� ������ (��� ������������ �������) � ������ ������� ������� ������ � ��� ��� �������, �.�. �������� ��� ������ ������, � ������� ������� - �������)

void AllocPosNegZero(int *p_arr, int size, int **p_arrPos, int **p_arrNeg, int **p_arrZero) {
	int sizePos = 0, sizeNeg = 0, sizeZero = 0;
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? ++sizePos : p_arr[i] < 0 ? ++sizeNeg : ++sizeZero;
	}
	*p_arrPos = new int[sizePos + 1] {};
	*p_arrNeg = new int[sizeNeg + 1] {};
	*p_arrZero = new int[sizeZero + 1] {};
	*(p_arrPos[0]) = sizePos;
	*(p_arrNeg[0]) = sizeNeg;
	*(p_arrZero[0]) = sizeZero;
	sizePos = sizeNeg = sizeZero = 1;
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? (*p_arrPos)[sizePos++] = p_arr[i] : p_arr[i] < 0 ? (*p_arrNeg)[sizeNeg++] = p_arr[i] : (*p_arrZero)[sizeZero++] = p_arr[i];
	}
}

// ������ ������� (������� ��������� ����� ������� ����������, �������� ������ ��� ������������ �������, ����������� ������ ����������, ������� �������� � ������� ����������)

void AllocPosNegZero_(int *p_arr, int size, int **p_arrPtr) {
	int sizePos = 0, sizeNeg = 0, sizeZero = 0;
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? ++sizePos : p_arr[i] < 0 ? ++sizeNeg : ++sizeZero;
	}
	int *p_arrPos = new int[sizePos + 1]{};
	int *p_arrNeg = new int[sizeNeg + 1]{};
	int *p_arrZero = new int[sizeZero + 1]{};
	p_arrPos[0] = sizePos;
	p_arrNeg[0] = sizeNeg;
	p_arrZero[0] = sizeZero;
	sizePos = sizeNeg = sizeZero = 1;
	for (int i = 0; i < size; ++i) {
		if (p_arr[i] > 0) {
			p_arrPos[sizePos++] = p_arr[i];
			continue;
		}
		if (p_arr[i] < 0) {
			p_arrNeg[sizeNeg++] = p_arr[i];
			continue;
		}
		p_arrZero[sizeZero++] = p_arr[i];
	}
	p_arrPtr[0] = p_arrPos;
	p_arrPtr[1] = p_arrNeg;
	p_arrPtr[2] = p_arrZero;
}

// ������ ������� (������� �������� ������ ��� ��� ������������ �������, ������� ����������� ������ ���������� � �������������� ��� ������� ������������� ���������, ���������� ����� ������� ����������)
// ����� ���� ������� ���������� ������ ����������

int** _AllocPosNegZero_(int *p_arr, int size) {
	int sizePos = 0, sizeNeg = 0, sizeZero = 0;
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? ++sizePos : p_arr[i] < 0 ? ++sizeNeg : ++sizeZero;
	}
	int *p_arrPos = nullptr;
	int *p_arrNeg = nullptr;
	int *p_arrZero = nullptr;
	Calloc(&p_arrPos, 1);							// ��� ������������ ���������� ������� �� ������ �������
	//p_arrPos = Malloc(p_arrPos, 1);
	Calloc(&p_arrNeg, 1);
	//p_arrNeg = Malloc(p_arrNeg, 1);
	Calloc(&p_arrZero, 1);
	//p_arrZero = Malloc(p_arrZero, 1);
	p_arrPos[0] = sizePos;
	p_arrNeg[0] = sizeNeg;
	p_arrZero[0] = sizeZero;
	sizePos = sizeNeg = sizeZero = 1;
	for (int i = 0; i < size; ++i) {
		if (p_arr[i] > 0) {
			AddToArrayEnd_(&p_arrPos, &sizePos, p_arr[i]);
			//p_arrPos = AddToArrayEnd(p_arrPos, &sizePos, p_arr[i]);
			continue;
		}
		if (p_arr[i] < 0) {
			AddToArrayEnd_(&p_arrNeg, &sizeNeg, p_arr[i]);
			//p_arrNeg = AddToArrayEnd(p_arrNeg, &sizeNeg, p_arr[i]);
			continue;
		}
		AddToArrayEnd_(&p_arrZero, &sizeZero, p_arr[i]);
		//p_arrZero = AddToArrayEnd(p_arrZero, &sizeZero, p_arr[i]);
	}
	static int *p_arrPtr[3] = { p_arrPos, p_arrNeg, p_arrZero };
	return p_arrPtr;
}

// ��������� ������� (��� ���������� �� ��������� ������ ���, ���� �� ���������� ��� ���� ������ ������������ �������� �� ������� ��� � ����������� �� ���������. �� ����� ��� ������� ����� ���������
// ������ ������ �������� - ������������ �������� � ���� ������ � ����������� �� ����������� ��������� � ���������� ��� �����, � �� ������� �����, ����� ������� ������������ � ��� ������� �� ���)
// ����� ������� ��� ������ �������� � ����������� �� ����������� ���������, �� ��� �� ������ ��, ��� �����.

void AllocPosNegZeroNoPP(int *p_arr, int size) {
	int sizePos = 0, sizeNeg = 0, sizeZero = 0;
	for (int i = 0; i < size; ++i) {
		p_arr[i] > 0 ? ++sizePos : p_arr[i] < 0 ? ++sizeNeg : ++sizeZero;
	}
	int *p_arrPos = nullptr;
	int *p_arrNeg = nullptr;
	int *p_arrZero = nullptr;
	//Calloc(&p_arrPos, 1);
	p_arrPos = Malloc(p_arrPos, 1);
	//Calloc(&p_arrNeg, 1);
	p_arrNeg = Malloc(p_arrNeg, 1);
	//Calloc(&p_arrZero, 1);
	p_arrZero = Malloc(p_arrZero, 1);
	p_arrPos[0] = sizePos;
	p_arrNeg[0] = sizeNeg;
	p_arrZero[0] = sizeZero;
	sizePos = sizeNeg = sizeZero = 1;
	for (int i = 0; i < size; ++i) {
		if (p_arr[i] > 0) {
			//AddToArrayEnd_(&p_arrPos, &sizePos, p_arr[i]);
			p_arrPos = AddToArrayEnd(p_arrPos, &sizePos, p_arr[i]);
			continue;
		}
		if (p_arr[i] < 0) {
			//AddToArrayEnd_(&p_arrNeg, &sizeNeg, p_arr[i]);
			p_arrNeg = AddToArrayEnd(p_arrNeg, &sizeNeg, p_arr[i]);
			continue;
		}
		//AddToArrayEnd_(&p_arrZero, &sizeZero, p_arr[i]);
		p_arrZero = AddToArrayEnd(p_arrZero, &sizeZero, p_arr[i]);
	}
	cout << "������ � �������������� ����������:\n\n";
	ShowArrayD(p_arrPos + 1, p_arrPos[0], 2);
	cout << endl;
	cout << "������ � �������������� ����������:\n\n";
	ShowArrayD(p_arrNeg + 1, p_arrNeg[0], 3);
	cout << endl;
	cout << "������ � �������� ����������:\n\n";
	ShowArrayD(p_arrZero + 1, p_arrZero[0], 2);
	cout << endl;

	Free(p_arrPos);
	Free(p_arrNeg);
	Free(p_arrZero);
}