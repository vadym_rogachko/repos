#include<iostream>
#include<time.h>
#include<windows.h>

using namespace std;




void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	/*1. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 20. ���������� ����� ��������� �������.*/

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int sum = 0;


		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 21;
				sum += arr[i][j];
				cout << arr[i][j] << (arr[i][j] > 9 ? " " : "  ");
			}
			cout << "\n  ";
		}


		cout << "\n����� ��������� ������� = " << sum << endl << endl;
	}
	system("pause");
	system("cls");

	/*2. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 20. ���������� ����� ���������, ������������� �� ������� ���������,
	     � ����� ����� ���������, ������������� �� �������� ���������.*/

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int sumMainDiag = 0;
		int sumSecDiag = 0;


		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 21;
				if (j == i) {
					sumMainDiag += arr[i][j];
					SetConsoleTextAttribute(handle, 4);
				}
				if (j == COL - 1 - i) {
					sumSecDiag += arr[i][j];
					SetConsoleTextAttribute(handle, 2);
				}
				cout << arr[i][j] << (arr[i][j] > 9 ? " " : "  ");
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			cout << "\n  ";
		}


		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << "\n����� ��������� ������� ��������� = "; SetConsoleTextAttribute(handle, 4);
		cout <<sumMainDiag << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << "����� ��������� �������� ��������� = "; SetConsoleTextAttribute(handle, 2);
		cout << sumSecDiag << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << endl;
	}
	system("pause");
	system("cls");

	/*3. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� -100 �� 100. ���������� ����������� � ������������ �������.*/

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};


		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 201 - 100;
				cout << arr[i][j] << (arr[i][j] == -100 ? "  " : arr[i][j] == 100 ? "   " : arr[i][j] < -9 ? "   " : arr[i][j] > 9 ? "    " : arr[i][j] > 0 ? "     " : "    ");
			}
			cout << "\n  ";
		}

		int min = arr[0][0];
		int max = arr[0][0];
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				if (arr[i][j] < min) {
					min = arr[i][j];
				}
				if (arr[i][j] > max) {
					max = arr[i][j];
				}
			}
		}


		cout << "\n����������� �������  = " << min << endl;
		cout << "������������ ������� = " << max << endl << endl;
	}
	system("pause");
	system("cls");

	/*4. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� -5 �� 5. ���������� ���������� �������������, ������������� � ������� ���������.*/

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int plus = 0, minus = 0, zero = 0;


		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 11 - 5;
				arr[i][j] > 0 ? ++plus : arr[i][j] < 0 ? ++minus : ++zero;
				cout << arr[i][j] << (arr[i][j] < 0 ? "  " : "   ");
			}
			cout << "\n  ";
		}


		cout << "\n���������� ������������� ��������� " << plus << endl;
		cout << "���������� ������������� ��������� " << minus << endl;
		cout << "���������� ������� ��������� " << zero << endl << endl;
	}
	system("pause");
	system("cls");

	/*5. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 20. ���������� ����� �� ������ ������ �������.*/

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int sum;


		cout << endl << ' ';
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 21;
				cout << arr[i][j] << (arr[i][j] > 9 ? "  " : "   ");
			}
			cout << endl << ' ';
		}


		for (int i = 0; i < ROW; ++i) {
			sum = 0;
			for (int j = 0; j < COL; ++j) {
				sum += arr[i][j];
			}
			cout << "\n����� ��������� � ������ � " << i + 1 << " = " << sum;
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*6. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 20. ���������� ����� �� ������� ������� �������.*/

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int sum;


		cout << endl << ' ';
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 21;
				cout << arr[i][j] << (arr[i][j] > 9 ? "  " : "   ");
			}
			cout << endl << ' ';
		}


		for (int i = 0; i < COL; ++i) {
			sum = 0;
			for (int j = 0; j < ROW; ++j) {
				sum += arr[j][i];
			}
			cout << "\n����� ��������� � ������� � " << i + 1 << " = " << sum;
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*7. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� -10 �� 40. ���������� ����� ��������� ��� ��� ��������, ������� �� �������� �� ������ �������������� ��������.*/

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int sum;


		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 51 - 10;
				cout << arr[i][j] << (arr[i][j] >= 0 && arr[i][j] < 10 ? "    " : arr[i][j] > 10 || arr[i][j] != -10 ? "   " : "  ");
			}
			cout << "\n  ";
		}


		bool flag, noMinus = true;
		cout << "\n����� ��������� ��������,������� �� �������� �� ������ �������������� ��������:\n\n\n";
		for (int i = 0; i < COL; ++i) {
			flag = true;
			sum = 0;
			for (int j = 0; j < ROW; ++j) {
				if (arr[j][i] < 0) {
					flag = false;
					break;
				}
				sum += arr[j][i];
			}
			if (flag) {
				noMinus = false;
				cout << "����� ��������� ������� � " << i + 1 << " = " << sum << endl;
			}
		}
		if (noMinus) {
			cout << "��� ������� �������� ���� �� ���� ������������� �������\n";
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*8. ��������� ������ N x M ����������� ������� ���, ����� ������ ����� ��������� ����� ������, � ������ � ����� �������.*/

	{
		const int N = 9;
		const int M = 9;
		int arr[N][M]{};


		cout << "\n\n    ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				arr[i][j] = (i + 1) * 10 + j + 1;
				cout << arr[i][j] << "  ";
			}
			cout << "\n    ";
		}
		cout << "\n\n\n ";
	}
	system("pause");
	system("cls");

	/*9. � ��������� ������� ����������� N x M �������� ������� ������ � ��������� �������, ������ � ������������� � �.�.*/

	{
		const int N = 9;
		const int M = 9;
		int arr[N][M]{};


		cout << "\n\n    ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				SetConsoleTextAttribute(handle, j + 2); // ���� ����� 1 - �������, ������� ������� � 2
				arr[i][j] = (i + 1) * 10 + j + 1;
				cout << arr[i][j] << "  ";
			}
			cout << "\n    ";
		}


		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << "\n\n\n\t       ����� ������\n\n\n\n    ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				if  (j <= M / 2) {
					swap(arr[i][j], arr[i][M - j - 1]);
				}
				SetConsoleTextAttribute(handle, M - j + 1);
				cout << arr[i][j] << "  ";
			}
			cout << "\n    ";
		}
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << "\n\n\n ";
	}
	system("pause");
	system("cls");

	/*10. � ��������� ������� ����������� 6 x 6 �������� ������� �������� ������.*/

	{
		const int ROW = 6;
		const int COL = 6;
		int arr[ROW][COL]{};


		cout << endl << "  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 9 + 1;
				SetConsoleTextAttribute(handle, (i + 1) % 2 + 9);
				cout << arr[i][j] << "  ";
			}
			cout << "\n  ";
		}


		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << "\n\n    ����� ������:\n\n\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				if (i % 2 == 0) {
					swap(arr[i][j], arr[i + 1][j]);
				}
				SetConsoleTextAttribute(handle, i % 2 + 9);
				cout << arr[i][j] << "  ";
			}
			cout << "\n  ";
		}
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << "\n\n\n";
	}
	




}