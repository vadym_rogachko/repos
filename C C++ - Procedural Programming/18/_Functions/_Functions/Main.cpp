#include<iostream>
#include<conio.h>
#include<time.h>

using namespace std;


void Next() {
	system("pause");
	system("cls");
}


void InitArrayRandDiap(int[], int, int, int);
void ShowArray(int[], int);
void InitIntArrayTwoDimRandDiap(int[][5], int, int, int min = 0, int max = 99);
void ShowArrayTwoDim(int[][5], int, int);
void SortArraySelectTwoDim(int[][5], int, int);


void ReplaceNumSpace();
void FilterSymb(bool);		         bool GetChoice();
void CapsLock();
void SortArrayHalf(int[], int);
void SortArrayBetweenNeg(int[], int);
int  SearchLinearArrayTwoDim(int[][5], int, int, int);			int GetKey();
int  SearchBinaryArrayTwoDim(int[][5], int, int, int);
void GameCubes();		            bool FirstThrow();		  	void ShowWhoseMove(bool);			short Throw();			     void ShowMove(bool);		 void ShowCube(short);
						   void ShowWhoWins(int, int);   		void ShowMidPoints(int, int);		void GetAction();	         void ShowStatistic(int, int, int, int, int, int);
void SortArrayBubble(int[], int, bool sortDecr = false);		bool ChooseDir();
int ShiftNum(int num, int shift = 1, bool dirRight = false);	int GetNum();		short GetShift();		bool GetDirection();


void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	
	ReplaceNumSpace();			 cout << endl; Next();
	FilterSymb(GetChoice());	 cout << endl; Next();
	CapsLock();					 cout << endl; Next();
	// 4.
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		InitArrayRandDiap(arr, SIZE, 100, 999);		cout << endl;
		ShowArray(arr, SIZE);
		SortArrayHalf(arr, SIZE);				    cout << endl;
		ShowArray(arr, SIZE);			        	cout << endl << endl;
	}
	Next();
	// 5.
	{
		const int SIZE = 15;
		int arr[SIZE]{};
		InitArrayRandDiap(arr, SIZE, -20, 20);		cout << endl;
		ShowArray(arr, SIZE);
		SortArrayBetweenNeg(arr, SIZE);		        cout << endl;
		ShowArray(arr, SIZE);			        	cout << endl << endl;
	}
	Next();
	// 6.
	{
		int key;
		int row, col, pos;
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		InitIntArrayTwoDimRandDiap(arr, ROW, COL);
		ShowArrayTwoDim(arr, ROW, COL);
		key = GetKey();
	    pos = SearchLinearArrayTwoDim(arr, ROW, COL, key);
		if (pos != ROW * COL) {
		row = pos / COL;
		col = pos - row * COL;
			cout << "\n������� " << key << " ��������� � " << row + 1 << " ������, � " << col + 1 << " �������\n\n";
		}
		else {
			cout << "\n������� �� ������\n\n";
		}
	}
	Next();
	// 7.
	{
		int key;
		int row, col, pos;
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		InitIntArrayTwoDimRandDiap(arr, ROW, COL);
		SortArraySelectTwoDim(arr, ROW, COL);
		ShowArrayTwoDim(arr, ROW, COL);
		key = GetKey();
		pos = SearchBinaryArrayTwoDim(arr, ROW, COL, key);
		if (pos != ROW * COL) {
			row = pos / COL;
			col = pos - row * COL;
			cout << "\n������� " << key << " ��������� � " << row + 1 << " ������, � " << col + 1 << " �������\n\n";
		}
		else {
			cout << "\n������� �� ������\n\n";
		}
	}
	Next();
	// 8.
	{
		cout << "\t\t\t      ���� ������\n";
		_getch();
		system("cls");
		GameCubes();	
	}
	Next();
	// 9.
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		bool dir;
		InitArrayRandDiap(arr, SIZE, 0, 99);		cout << endl;
		ShowArray(arr, SIZE);
		cout << endl << endl;
		dir = ChooseDir();
		SortArrayBubble(arr, SIZE, dir);			cout << endl;
		ShowArray(arr, SIZE);			      	    cout << endl << endl << endl;
	}
	Next();
	// 10.
	{
		int num = GetNum();
		short shift = GetShift();
		bool dirRight = GetDirection();
		num = ShiftNum(num, shift, dirRight);
		cout << endl << num << endl << endl;
	}
}


void InitArrayRandDiap(int arr[], int size, int min, int max) {
	for (int i = 0; i < size; ++i) {
		arr[i] = rand() % (max - min + 1) + min;
	}
}

void ShowArray(int arr[], int size) {
	for (int i = 0; i < size; ++i) {
		cout << arr[i] << (arr[i] / 10 == 0 ? "   " : arr[i] / 100 == 0 ? "  " : " ");
	}
}

void InitIntArrayTwoDimRandDiap(int arr[][5], int row, int col, int min, int max) {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			arr[i][j] = rand() % (max - min + 1) - min;
		}
	}
}

void ShowArrayTwoDim(int arr[][5], int row, int col) {
	cout << "\n  ";
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			cout << arr[i][j] << (arr[i][j] / 10 == 0 ? "   " : arr[i][j] / 100 == 0 ? "  " : " ");
		}
		cout << "\n  ";
	}
}

int GetKey() {
	int key;
	cout << "\n    ������� �����\n";
	cin >> key;
	return key;
}

void SortArraySelectTwoDim(int arr[][5], int row, int col) {
	int min, posMin, posCol;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			min = arr[i][j];
			posMin = i * col + j;
			posCol = j + 1;
			for (int k = i; k < row; ++k) {
				for (int m = posCol; m < col; ++m) {
					if (arr[k][m] < min) {
						min = arr[k][m];
						posMin = k * col + m;
					}
				}
				posCol = 0;
			}
			arr[posMin / col][posMin - posMin / col * col] = arr[i][j];
			arr[i][j] = min;
		}
	}
}

/*1. �������� �������, ������� ��� ����� � ���������� ������ �������� ����� ��������� (getch(), putch(char)).*/

void ReplaceNumSpace() {
	int code = 0;
	while (code != 27) {
		code = _getch();
		if (code >= 48 && code <= 57) {
			_putch(32);
		}
		else if (code == 13) {
			_putch(10);
		}
		else {
			_putch(code);
		}
	}
}

/*2. �������� �������, ������� ��� ����� � ���������� ������ ��������� ���������� ���� ��� ���� � ����������� �� ������ ������������ (getch(), putch(char)).*/

bool GetChoice() {
	char ch;
	do {
		system("cls");
		cout << "�������� ��� ����������:   1. ���������� ����\n\t\t\t   2. ���������� ����\n";
		cin >> ch;
	} while (ch != '1' && ch != '2');
	system("cls");
	return ch == '1';
}

void FilterSymb(bool choice) {
	int code = 0;
	if (choice) {
		while (code != 27) {
			code = _getch();
			if ((code >= 65 && code <= 90) || (code >= 97 && code <= 122)) {
				_putch(code);
			}
			else if (code == 13) {
				_putch(10);
			}
		}
	}
	else {
		while (code != 27) {
			code = _getch();
			if (code >= 48 && code <= 57) {
				_putch(code);
			}
			else if (code == 13) {
				_putch(10);
			}
		}
	}
}

/*3. �������� �������, ������� ��� ����� � ���������� ������ �������� �������� ����� �� ���������, � ��������� � �� �������� (getch(), putch(char)).*/

void CapsLock() {
	int code = 0;
	while (code != 27) {
		code = _getch();
		if (code >= 65 && code <= 90) {
			_putch(code + 32);
			continue;
		}
		else if (code >= 97 && code <= 122) {
			_putch(code - 32);
			continue;
		}
		else if (code == 13) {
			_putch(10);
			continue;
		}
		_putch(code);
	}
}

/*4. �������� �������, ������� ��������� ������ �������� ������� �� ��������, � ������ - �� �����������, ��������� ���������� �������� ���������.*/

void SortArrayHalf(int arr[], int size) {
	for (int i = 0; i < size / 2; ++i) {
		for (int j = i; j > 0; --j) {
			if (arr[j] > arr[j - 1]) {
				swap(arr[j], arr[j - 1]);
				continue;
			}
			break;
		}
	}
	for (int i = size / 2; i < size; ++i) {
		for (int j = i; j > size / 2; --j) {
			if (arr[j] < arr[j - 1]) {
				swap(arr[j], arr[j - 1]);
				continue;
			}
			break;
		}
	}
}

/*5. � ������� ���������� ������ ��������� ����� � ��������� �� -20 �� +20. ���������� ����� ������� ������� ������������� ��������� (������ ������ �������������� �������� � ������ �������
     �������������� ��������) � ������������� ��������, ����������� ����� ����.*/

void SortArrayBetweenNeg(int arr[], int size) {
	int index1, index2;
	bool flag = true;
	for (int i = 0; i < size; ++i) {
		if (arr[i] < 0) {
			index1 = i;
			flag = false;
			break;
		}
	}
	if (flag) {
		return;
	}
	for (int i = size - 1; i >= 0; --i) {
		if (arr[i] < 0) {
			index2 = i;
			break;
		}
	}
	if (index1 == index2) {
		return;
	}
	for (int i = index1 + 1; i < index2; ++i) {
		for (int j = i; j > index1 + 1; --j) {
			if (arr[j] < arr[j - 1]) {
				swap(arr[j], arr[j - 1]);
				continue;
			}
			break;
		}
	}
}

/*6. �������� �������, ����������� �������� ��������� ������ ��������� ����� � ��������� �������.*/

int SearchLinearArrayTwoDim(int arr[][5], int row, int col, int key) {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			if (arr[i][j] == key) {
				return i * col + j;
			}
		}
	}
	return row * col;
}

/*7. �������� �������, ����������� �������� ��������� ������ ��������� ����� � ��������� �������.*/

int SearchBinaryArrayTwoDim(int arr[][5], int row, int col, int key) {
	int middle;
	int minIndex = 0;
	int maxIndex = row * col - 1;
	while (true) {
		middle = (minIndex + maxIndex) / 2;
		if (arr[middle / col][middle - middle / col * col] == key) {
			return middle;
		}
		else if (arr[middle / col][middle - middle / col * col] > key) {
			maxIndex = middle - 1;
		}
		else {
			minIndex = middle + 1;
		}
		if (minIndex > maxIndex) {
			return row * col;
		}
	}
}

/*8. �������� ���� �������. �������: ������� ��� ��������� ������ �� ���������� �� 1 �� 6. ���� ���������� � �����������, ������ ��������� ����������. ��������� ���, � ���� ����� �������� �����
     �� ������ ���� ������� ������. ������������� ����������� ��������� ������� ���� ��������� ��� �����������. ������ ������������ � ������� ��������. � ����� ���� ���������� �������� ������� �����
	 �� ������� ��� ����� ����������.*/

void GameCubes() {
	short throws = 0;
	short firstCube, secondCube;
	int playerPoints = 0, compPoints = 0;
	int static allPlayerPoints = 0, allCompPoints = 0;
	int static games = 0;
	int static playerWins = 0, compWins = 0, draw = 0;
	bool static flag = true;
	bool playerMove = FirstThrow();
	ShowWhoseMove(playerMove);
	_getch();
	system("cls");
	while (throws++ < 5) {
		firstCube = Throw();
		secondCube = Throw();
		playerPoints += firstCube + secondCube;
		cout << endl;
		ShowMove(playerMove);
		ShowCube(firstCube); ShowCube(secondCube);
		firstCube = Throw();
		secondCube = Throw();
		compPoints += firstCube + secondCube;
		ShowMove(!playerMove);
		ShowCube(firstCube); ShowCube(secondCube);
		_getch();
		system("cls");
	}
	allPlayerPoints += playerPoints;
	allCompPoints += compPoints;
	ShowWhoWins(playerPoints, compPoints);
	playerPoints > compPoints ? ++playerWins : compPoints > playerPoints ? ++compWins : ++draw;
	ShowMidPoints(playerPoints, compPoints);
	++games;
	GetAction();
	if (flag) {
		system("cls");
		ShowStatistic(games, playerWins, compWins, draw, allPlayerPoints, allCompPoints);
		flag = false;
	}
}

bool FirstThrow() {
	return rand() % 2 + 0;
}

short Throw() {
	return rand() % 6 + 1;
}

void ShowWhoseMove(bool playerMove) {
	cout << (playerMove ? "\n����� ����� ������\n\n" : "\n��������� ����� ������\n\n");
}

void ShowMove(bool playerMove) {
	if (playerMove) {
		cout << "\t        ���  ������\n\n";
		_getch();
	}
	else {
		cout << "\t      ���  ����������\n\n";
		_getch();
	}
}

void ShowCube(short Cube) {
	int row = 9;
	int col = 11;
	switch (Cube) {
	case 1:
		cout << "\t\t";
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				if (i == 0 || i == row - 1) {
					cout << '*';
					continue;
				}
				if (j == 0 || j == col - 1) {
					cout << '*';
					continue;
				}
				if (i == row / 2 && j == col / 2) {
					cout << '*';
					continue;
				}
				cout << ' ';
			}
			cout << "\n\t\t";
		}
		break;
	case 2:
		cout << "\t\t";
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				if (i == 0 || i == row - 1) {
					cout << '*';
					continue;
				}
				if (j == 0 || j == col - 1) {
					cout << '*';
					continue;
				}
				if ((i == 2 && j == 3) || (i == row - 3 && j == col - 4)) {
					cout << '*';
					continue;
				}
				cout << ' ';
			}
			cout << "\n\t\t";
		}
		break;
	case 3:
		cout << "\t\t";
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				if (i == 0 || i == row - 1) {
					cout << '*';
					continue;
				}
				if (j == 0 || j == col - 1) {
					cout << '*';
					continue;
				}
				if ((i == 2 && j == 3) || (i == row - 3 && j == col - 4) || (i == row / 2 && j == col / 2)) {
					cout << '*';
					continue;
				}
				cout << ' ';
			}
			cout << "\n\t\t";
		}
		break;
	case 4:
		cout << "\t\t";
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				if (i == 0 || i == row - 1) {
					cout << '*';
					continue;
				}
				if (j == 0 || j == col - 1) {
					cout << '*';
					continue;
				}
				if ((i == 2 && (j == 3 || j == col - 4)) || (i == row - 3 && (j == 3 || j == col - 4))) {
					cout << '*';
					continue;
				}
				cout << ' ';
			}
			cout << "\n\t\t";
		}
		break;
	case 5:
		cout << "\t\t";
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				if (i == 0 || i == row - 1) {
					cout << '*';
					continue;
				}
				if (j == 0 || j == col - 1) {
					cout << '*';
					continue;
				}
				if ((i == 2 && (j == 3 || j == col - 4)) || (i == row - 3 && (j == 3 || j == col - 4)) || (i == row / 2 && j == col / 2)) {
					cout << '*';
					continue;
				}
				cout << ' ';
			}
			cout << "\n\t\t";
		}
		break;
	case 6:
		cout << "\t\t";
		for (int i = 0; i < row; ++i) {
			for (int j = 0; j < col; ++j) {
				if (i == 0 || i == row - 1) {
					cout << '*';
					continue;
				}
				if (j == 0 || j == col - 1) {
					cout << '*';
					continue;
				}
				if ((i == 2 && (j == 3 || j == col - 4)) || (i == row - 3 && (j == 3 || j == col - 4)) || (i == row / 2 && (j == 3 || j == col - 4))) {
					cout << '*';
					continue;
				}
				cout << ' ';
			}
			cout << "\n\t\t";
		}
		break;
	}
	cout << endl;
}

void ShowWhoWins(int playerPoints, int compPoints) {
	if (playerPoints > compPoints) {
		cout << "\n�� ��������!!!\n";
	}
	else if (playerPoints < compPoints) {
		cout << "\n��������� ������� :(\n";
	}
	else {
		cout << "\n�����\n";
	}
}

void ShowMidPoints(int playerPoints, int compPoints) {
	cout << "\n����� �����: �����     " << playerPoints << "\n\t     ��������� " << compPoints << "\n";
	cout << "\n������� �����\n �� �������: �����     " << 1. * playerPoints / 5 << "\n\t     ��������� " << 1. * compPoints / 5 << "\n\n";
}

void GetAction() {
	int action = 0;
	cout << "��������� - Enter\n����� - Esc";
	while (action != 27 && action != 13) {
		action = _getch();
	}
	system("cls");
	if (action == 13) {
		cout << endl;
		GameCubes();
	}
}

void ShowStatistic(int games, int playerWins, int compWins, int draw, int playerPoints, int compPoints) {
	cout.precision(1);
	cout << "������� ��� " << games;
	cout << "\n\n������ ������ " << playerWins;
	cout << "\n������ ���������� " << compWins;
	cout << "\n����� " << draw;
	cout << "\n\n���� ������ " << playerPoints;
	cout << "\n� ������� �� ���� " << fixed << 1. * playerPoints / games;
	cout << "\n� ������� �� ��� " << fixed << 1. * playerPoints / games / 5;
	cout << "\n\n���� ���������� " << compPoints;
	cout << "\n� ������� �� ���� " << fixed << 1. * compPoints / games;
	cout << "\n� ������� �� ��� " << fixed << 1. * compPoints / games / 5 << defaultfloat;
	cout.precision(6);
	cout << endl << endl << endl;
}

/*9. ��� ������ ����� ������������ 10 ���������. �������� �������, ������� ��������� ������ �� ����������� ��� �� ��������, � ����������� �� �������� ��������� �������.
     ���� �� ����� true, ���������� ���� �� ��������, ���� false, �� �� �����������. ������ 2 ��������� ������� - ��� ������ � ��� ������, ������ �������� �� ��������� ����� false.*/

void SortArrayBubble(int arr[], int size, bool sortDecr) {
	bool flag;
	int size2 = size;
	if (sortDecr) {
		for (int i = 0; i < size; ++i) {
			flag = true;
			for (int j = 0; j < size2 - 1; ++j) {
				if (arr[j] < arr[j + 1]) {
					swap(arr[j], arr[j + 1]);
					flag = false;
				}
			}
			if (flag) {
				break;
			}
			--size2;
		}
	}
	else {
		for (int i = 0; i < size; ++i) {
			flag = true;
			for (int j = 0; j < size2 - 1; ++j) {
				if (arr[j] > arr[j + 1]) {
					swap(arr[j], arr[j + 1]);
					flag = false;
				}
			}
			if (flag) {
				break;
			}
			--size2;
		}
	}
}

bool ChooseDir() {
	int dir = 0;
	cout << "�������� ����������� ����������: 1. �� �����������\n\t\t\t\t 2. �� ��������\n";
	while (dir != int('1') && dir != int('2')) {
		dir = _getch();
	}
	return dir == int('2');
}

/*10. �������� �������, ���������� ���������� ����� ����� �� N �������� ������ ��� �����, � ����������� �� �������� ��������� �������. ������� ������ ��������� ��� ���������:
      � ������ ��������� ���������� ����� ��� ������, ������ �������� ������ ���������� �������� ��� ������ (�� ��������� ����� �� 1 ������), 3-� ��������� �������� ������
	  ����������� ������ (�� ��������� ����� (false)).*/

int ShiftNum(int num, int shift, bool dirRight) {
	int copy = num;
	short count = 0;
	int index;
	int exp;
	while (copy != 0) {
		copy /= 10;
		++count;
	}
	while (shift-- > 0) {
		exp = 1;
		index = count;
		while (--index > 0) {
			exp *= 10;
		}
		num = dirRight ? num % 10 * exp + num / 10 : num % exp * 10 + num / exp;
	}
	return num;
}

int GetNum() {
	int num;
	cout << "������� �����\n";
	cin >> num;
	return num;
}

short GetShift() {
	int shift;
	cout << "������� ���������� �������� ��� ������\n";
	cin >> shift;
	return shift;
}

bool GetDirection() {
	int dir;
	cout << "�������� ����������� ������: " << char(26) << ' ' << char(27) << endl;
	while (true) {
		dir = _getch();
		if (dir == 224) {
			dir = _getch();
			if (dir == 75 || dir == 77) {
				break;
			}
		}
	}
	return dir == 77;
}






