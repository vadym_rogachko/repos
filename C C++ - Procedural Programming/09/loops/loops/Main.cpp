#include<iostream>
#include<Windows.h>

using namespace std;



void main() {
	setlocale(LC_ALL, "rus");

	/*1. ��������� ��������� ������ �����, �����-����������, ������������, ��������� ���� � ������������.*/

	// ����� �����

	{
		double grn;
		short valuta;
		char contin;
		do {
			system("cls");
			cout << "������� �����,���\n";
			cin >> grn;

			do {
				cout << "�������� ������\n\t1 - usd\n\t2 - eur\n\t3 - ���\n";
				cin >> valuta;
				cout.precision(2);
				switch (valuta) {
				case 1:
					system("cls");
					cout << fixed << grn << " ��� = " << grn / 27.18 << " usd\n" << defaultfloat;
					break;
				case 2:
					system("cls");
					cout << fixed << grn << " ��� = " << grn / 29 << " eur\n" << defaultfloat;
					break;
				case 3:
					system("cls");
					cout << fixed << grn << " ��� = " << grn / 0.45 << " ���\n" << defaultfloat;
					break;
				default:
					cout << "������.�� ������� ������� ������\n";
					system("pause");
					system("cls");
				}
			} while (valuta != 1 && valuta != 2 && valuta != 3);
			cout << "����������? (Y/N)\n";
			cin >> contin;
		} while (contin == 'Y' || contin == 'y');
		cout.precision(6);
		system("cls");
	}
	
	// ����-���������
	
	{
		double Km, Miles;
		char contin;
		const double KM_IN_MILES = 1.609;
		do {
			system("cls");
			do {
				system("cls");
				cout << "������� ���������� ����\n";
				cin >> Miles;
			} while (Miles < 0);
			Km = Miles * KM_IN_MILES;
			// �������� ����������� ���� ��� ����������� ����������� ������������ ��������� � ����� ����
			if (Miles != 0) {
				Km = (long long(Km * 1000000000) + 0.5) / 1000000000;
			}
			// ��������� ���������� ��������� � ����� ����
			if (long long(Miles * 1000000000) % 1000000000 != 0) { // ���� ������� ������ - ����,� ��������� ������ 1 - ����, � ��������� ������ 2,3 ��� 4 - ����,0,5,6,7,8,9 - ����
				system("cls");
				cout << Miles << " ���� = " << Km << " ��\n";
			}
			else {
				switch ((int)Miles % 10) {
				case 0:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
					system("cls");
					cout << Miles << " ���� = " << Km << " ��\n";
					break;
				case 1:
					system("cls");
					cout << Miles << " ���� = " << Km << " ��\n";
					break;
				case 2:
				case 3:
				case 4:
					system("cls");
					cout << Miles << " ���� = " << Km << " ��\n";
				}
			}
			cout << "����������? (Y/N)\n";
			cin >> contin;
		} while (contin == 'Y' || contin == 'y');
		system("cls");
	}
	

	// �����������

	{
		long double num1, num2;
		short action;
		char contin;
		do {
			system("cls");
			cout << "������� ��� �����: x � y\n";
			cin >> num1 >> num2;
			do {
				cout << "�������� ��������:\n\t1. x + y\n\t2. x - y\n\t3. x * y\n\t4. x / y\n";
				cin >> action;
				switch (action) {
				case 1:
					system("cls");
					cout << num1 << " + " << num2 << " = " << num1 + num2 << endl;
					break;
				case 2:
					system("cls");
					cout << num1 << " - " << num2 << " = " << num1 - num2 << endl;
					break;
				case 3:
					system("cls");
					cout << num1 << " * " << num2 << " = " << num1 * num2 << endl;
					break;
				case 4:
					system("cls");
					cout << num1 << " / " << num2 << " = " << num1 / num2 << endl;
					break;
				default :
					cout << "������.�� ������� ������� ��������\n";
					system("pause");
					system("cls");
				}
			} while (action != 1 && action != 2 && action != 3 && action != 4);
			cout << "����������? (Y/N)\n";
			cin >> contin;
		} while (contin == 'Y' || contin == 'y');
		system("cls");
	}
	
	/*2. � ����� � ���������� �������� 10 ����� �����. ��������� ������� ������������� �����, ������� ������������� ����� � ������� �����.*/

	{
		int i, num;
		float plus, minus, zero;
		char contin;
		do {
			i = plus = minus = zero = 0;
			system("cls");
			cout << "������� 10 ����� �����\n";
			Sleep(2500);
			do {
				system("cls");
				cout << "������� �����\n";
				cin >> num;
				(num > 0) ? plus++ : (num < 0) ? minus++ : zero++;
				i++;
			} while (i < 10);
			system("cls");
			// ������ �������� ������������� �����,������������� ����� � �����
			plus = plus / i * 100;
			minus = minus / i * 100;
			zero = zero / i * 100;
			cout << "�� �����:\n\t" << plus << " % ������������� �����\n\t" << minus << " % ������������� �����\n\t" << zero << " % �����\n";
			cout << endl << endl << endl << "����������? (Y/N)\n";
			cin >> contin;
		} while (contin == 'Y' || contin == 'y');
		system("cls");
	}
	
	/*3. � ����� � ���������� �������� 10 ����� �����. ��������� ������� ������ ����� � ������� ��������.*/

	{
		short i;
		int num;
		float even, uneven;
		char contin;
		do {
			i = even = uneven = 0;
			system("cls");
			cout << "������� 10 ����� �����\n";
			Sleep(2000);
			do {
				system("cls");
				cout << "������� �����\n";
				cin >> num;
				(num % 2) ? uneven++ : even++;
				i++;
			} while (i < 10);
			system("cls");
			even = even / i * 100;
			uneven = uneven / i * 100;
			cout << "�� �����:\n\t" << even << " % ������ �����\n\t" << uneven << " % �������� �����\n";
			cout << endl << endl << endl << "����������? (Y/N)\n";
			cin >> contin;
		} while (contin == 'Y' || contin == 'y');
		system("cls");
	}

	/*4. ������� �� ����� ������� ���������.*/

	{
		short i = 1, j, num;
		while (i <= 10) {
			j = 1;
			while (j <= 10) {
				num = j * i;
				cout << num << "\t";
				j++;
			}
			cout << endl;
			i++;
		}
		system("pause");
		system("cls");
	}

	/*5. �������� ���������, ������� ������� �� ����� ��� ������� ����� �� ��������� 1..100.*/

	{
		short i = 2, j, num; // 1 �� �������� ������� ������ - ������� ����� � 2
		while (i <= 100) {  
			j = 2; // �� ���� �� 1
			bool flag = true; // 2 �� ������� � �������� (���� ����� ��� �������� �������� � ��������� ����� 1 � 2) - �� ��������� ����� ������� ������
			while (j < i) { // �� ���� �� ������ ����
				flag = (i % j) ? true : false; // ��� �������� ���� ����� �� ��� ����� �� ��������� �� 1 �� ������ ����,����� 1 � ������ ���� � ���������� ���������� �������� ������� ����������
				(i % j) ? j++ : j = i;         // ��� ������ ������� �� ������ ���� 0 - ����� �� �����, �.�. ����� ��� ����� �������, � ���� �� ���� ���� ������� ���������� ������� != 0 - ����� �������
			}
			if (flag) { // flag == true - ������� �����
				cout << i << "\t";
			}
			i++;
		}
		cout << endl << endl << endl;
		system("pause");
		system("cls");
	}

	/*6. �������� ���������, ������� ������� �� ����� ������� (������������ ������ ����� �������), ��������� �� ������� '*'.*/

	{
		short size, i = 0, j;
		cout << "������� ����� ������� ��������: ";
		cin >> size;
		cout << endl;
		while (i < size) {
			j = 0;
			while (j < size) {
				cout << '*';
				j++;
			}
			cout << endl;
			i++;
		}
		cout << endl << endl << endl;
		system("pause");
		system("cls");
	}

	/*7. ������� ���������� ������, �� ������ ������ ������.*/

	{
		short size, i = 0, j;
		cout << "������� ����� ������� ��������: ";
		cin >> size;
		cout << endl;
		while (i < size) {
			j = 0;
			while (j < size) {
				if (i == size - size || i == size - 1 || j == size - size || j == size - 1) {
					cout << '*';
					j++;
					continue;
				}
				cout << ' ';
				j++;
			}
			cout << endl;
			i++;
		}
		cout << endl << endl << endl;
		system("pause");
		system("cls");
	}

	/*8. �������� ���������, ������� ������� �� ����� ��������� �����������.*/

	// �������� ������ - ����� ������� �������� ������ ������������

	{
		short size, i = 0, j;
		cout << "������� ����� ������� ��������: ";
		cin >> size;
		// ���������� ������� �������� �� ������� � ������ ����� ������� ����� (����� ����� ��� �� ������)
		if (size % 2 == 0) {
			size += 1;
		}
		cout << endl;
		while (i < size) {
			j = 0;
			while (j < size) {
				if (i == size - size || i == size - 1 || i == size / 2 ||
					j == size - size || j == size - 1 || j == size / 2) {
					cout << '*';
					j++;
					continue;
				}
				cout << ' ';
				j++;
			}
			cout << endl;
			i++;
		}
		cout << endl;
		system("pause");
		system("cls");
	}

	/*9. �������� ���������, ������� ������� �� ����� ��������� �����������.*/

	{
		short size, i = 0, j;
		cout << "������� ����� ������� ��������: ";
		cin >> size;
		if (size % 2 == 0) {
			size += 1;
		}
		// ������ ����������, ������ ������ ���������
		short ratio = (size - 1) / 4;
		cout << endl;
		while (i < size) {
			j = 0;
			while (j < size) {                                     // ����� ������������ ���������                               // ������ ������������ ���������  
				if  (i == size - size || i == size - 1 || (j == ratio && (i <= ratio || i >= size - 1 - ratio) ) || (j == size - 1 - ratio && (i <= ratio || i >= size - 1 - ratio) )  ||
					 j == size - size || j == size - 1 || (i == ratio && (j <= ratio || j >= size - 1 - ratio) ) || (i == size - 1 - ratio && (j <= ratio || j >= size - 1 - ratio) ) ) {
					cout << '*';                                   // ������� �������������� ���������                           // ������ �������������� ���������
					j++;
					continue;
				} 
					cout << ' ';
					j++;
				}
				cout << endl;
				i++;
			}
			cout << endl;
			system("pause");
			system("cls");
	}

	/*10. �������� ���������, ������� ������� �� ����� ��������� �����������.*/

	{
		short size, i = 0, j;
		cout << "������� ����� ������� ��������: ";
		cin >> size;
		if (size % 2 == 0) {
			size += 1;
		}
		short ratio = (size - 1) / 4;
		cout << endl;
		while (i < size) {
			j = 0;
			while (j < size) {                             // ����� ������� �������           // ������ ������� �������
				if (i == size - size || i == size - 1 || (j <= ratio && i <= ratio) || (j >= size - 1 - ratio && i <= ratio) ||
					j == size - size || j == size - 1 || (j <= ratio && i >= size - 1 - ratio) || (j >= size - 1 - ratio && i >= size - 1 - ratio)) {
					cout << '*';                                // ����� ������ �������                       // ������ ������ �������
					j++;
					continue;
				}
				cout << ' ';
				j++;
			}
			cout << endl;
			i++;
		}
		cout << endl;
	}
	






	system("pause");
	system("cls");
}