#pragma once
#include"Enums.h"
#include"define.h"

struct Point {
	float x;
	float y;
};

struct Rectangle {
	Point pointUpLeft;
	Point pointUpRight;
	Point pointDownRight;
	Point pointDownLeft;
};

struct Man {
	char *name = new char[NAME_S]{};
	char *surname = new char[SURNAME_S]{};
	short age;
	struct {
		short day;
		short month;
		short year;
	} dateOfBirth;
	float height;
	float weight;
};

struct FractionNum {
	short numerator;
	short denominator;
};

struct Student {
	short points = 0;
	char name[NAME_S]{};
	char surname[SURNAME_S]{};
	char comment[COMMENT_S]{};
};

struct Journal {
	char course = NOINFO;
	short countStudents = 0;
	faculty faculty = NONE;
	Student *students = nullptr;
	char ID[GROUP_S]{};
	char comment[COMMENT_S]{};
};