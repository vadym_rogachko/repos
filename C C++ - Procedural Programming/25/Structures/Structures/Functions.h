#pragma once
#include"Structures.h"

bool IsParallel(Point point1, Point point2);

bool IsHit(Rectangle rectangle, Point point);

void InitMan(Man *man);
void ShowMan(Man man);

bool IsRectangle(Rectangle rectangle);
void ShowCoord(Rectangle rectangle);
void NormRect(Rectangle &rectangle);
void MoveRectangle(Rectangle &rectangle, const Point *point, Point newPoint);
void ChangeSize(Rectangle &rectangle, float height, float width);

bool GetFractionNum(FractionNum &num1, FractionNum &num2);
char GetAction();
FractionNum Calc(const FractionNum *num1, const FractionNum *num2, char &ch);
FractionNum Plus(const FractionNum *num1, const FractionNum *num2);
FractionNum Minus(const FractionNum *num1, const FractionNum *num2);
FractionNum Multi(const FractionNum *num1, const FractionNum *num2);
FractionNum Div(const FractionNum *num1, const FractionNum *num2);
void CutFractionNum(FractionNum &num);
void ShowRes(const FractionNum *num1, const FractionNum *num2, const FractionNum *numRes, char sign);

short CreateJournals(Journal **&journals);
action MenuMain();
action MenuAdd(Journal **journals, short journChoice);
bool AddJournal(Journal **journals, short &journNum);
bool DeleteJournal(Journal **journals, short &journNum, short journChoice);
bool ShowJournal(Journal **journals, short journChoice);
short ChooseJournal(Journal **journals, short journNum);
bool AddStudent(Journal **journals, short journChoice);
bool DeleteStudent(Journal **journals, short journChoice);
short ChooseStudent(Journal **journals, short journChoice);
short FindStudent(Journal **journals, short journNum);
bool ShowStudent(Journal **journals, short id);
bool EditData(Journal **journals, short journChoice, char choice);
bool EditPoints(Journal **journals, short journChoice, short id);
bool EditStudent(Journal **journals, short journChoice, short id);
bool EditJournal(Journal **journals, short journChoice);
void DeleteJournals(Journal **&journals, short journSize);