#pragma once

#define NAME_S 32
#define SURNAME_S 32
#define GROUP_S 32
#define COMMENT_S 256
#define CANCEL -1
#define NOINFO 0
#define JOURNAL '1'
#define STUDENT '2'
#define POINTS '3'
#define ESC 27
#define SURNAME '1'
#define NAME '2'
#define COMMENT_st '3'
#define ZERO '0'
#define GROUP '1'
#define FACULTY '2'
#define COURSE '3'
#define COMMENT_j '4'
#define EMP 1
#define EMP_SIZE 2
#define EMP_SIZE_FILL 3