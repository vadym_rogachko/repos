#include"Functions.h"
#include<iostream>
#include<math.h>
#include<conio.h>

using std::cout;
using std::cin;
using std::endl;
using std::swap;
using std::fixed;
using std::defaultfloat;

/*1. �������� ���������, ����������� ����� � ��������� ������� ��������� (x, y). � ������� ���� ��������� ������� ��� �����.
	 ����� �� ������, ���������� ����� ��� �����, ����������� ��� ������� ��� ��� �������*/

bool IsParallel(Point point1, Point point2) {
	return point1.x == point2.x || point1.y == point2.y;
}

/*2. ������� ���������, ����������� �������������. �������� ������� ��� ����������� ��������� ������������ ����� � ������ �������������. ����� ����� ������ ����������� ����������*/

bool IsHit(Rectangle rectangle, Point point) {
	return point.x >= rectangle.pointUpLeft.x && point.x <= rectangle.pointDownRight.x
		&& point.y <= rectangle.pointUpLeft.y && point.y >= rectangle.pointDownRight.y;
}

/*3. ������� ���������, ����������� ��������. ������� ������� ��� ������ � ���� ���������� (��������, ���������� ������� � ���������� ������ �������)*/

void InitMan(Man *man) {
	cout << "������� ���\n";
	gets_s(man->name, 15);
	cout << "������� �������\n";
	gets_s(man->surname, 15);
	cout << "������� �������\n";
	cin >> man->age;
	cout << "������� ���� �������� (����, �����, ���)\n";
	cin >> man->dateOfBirth.day >> man->dateOfBirth.month >> man->dateOfBirth.year;
	cout << "������� ����\n";
	cin >> man->height;
	cout << "������� ���\n";
	cin >> man->weight;
}

void ShowMan(Man man) {
	cout << "\n���: " << man.name << endl;
	cout << "�������: " << man.surname << endl;
	cout << "�������: " << man.age << endl;
	cout << "���� ��������: " << man.dateOfBirth.day << '.' << man.dateOfBirth.month << '.' << man.dateOfBirth.year << endl;
	cout << "����: " << man.height << endl;
	cout << "���: " << man.weight << endl << endl;
}

/*4. ������� ���������, ����������� �������������. �������� ������� ��� ������ � ���� ���������� (��������, ������������ ��������������, ����������� ��������������,
     ��������� ������� �������������� � �. �.)*/

bool IsRectangle(Rectangle rectangle) {
	float diag1, diag2;
	// ������� ��������
	diag1 = sqrt(pow(rectangle.pointUpRight.x - rectangle.pointDownLeft.x, 2) + pow(rectangle.pointUpRight.y - rectangle.pointDownLeft.y, 2));
	diag2 = sqrt(pow(rectangle.pointUpLeft.x - rectangle.pointDownRight.x, 2) + pow(rectangle.pointUpLeft.y - rectangle.pointDownRight.y, 2));
	return diag1 == diag2;
}

void ShowCoord(Rectangle rectangle) {
	cout << "\n������� A: x = " << fixed << rectangle.pointUpLeft.x << " y = " << rectangle.pointUpLeft.y << endl;
	cout << "������� B: x = " << rectangle.pointUpRight.x << " y = " << rectangle.pointUpRight.y << endl;
	cout << "������� C: x = " << rectangle.pointDownRight.x << " y = " << rectangle.pointDownRight.y << endl;
	cout << "������� D: x = " << rectangle.pointDownLeft.x << " y = " << rectangle.pointDownLeft.y << defaultfloat << endl << endl;
	_getch();
}

void NormRect(Rectangle &rectangle) {
	float height, width;
	// ������� ��������
	height = sqrt(pow(rectangle.pointDownRight.x - rectangle.pointDownLeft.x, 2) + pow(rectangle.pointDownRight.y - rectangle.pointDownLeft.y, 2));
	width = sqrt(pow(rectangle.pointUpRight.x - rectangle.pointDownRight.x, 2) + pow(rectangle.pointUpRight.y - rectangle.pointDownRight.y, 2));
	if (rectangle.pointDownLeft.y < rectangle.pointDownRight.y) {
		rectangle.pointDownRight = rectangle.pointDownLeft;
		rectangle.pointUpRight.x = rectangle.pointDownRight.x;
		rectangle.pointDownLeft.y = rectangle.pointDownRight.y;
		rectangle.pointUpRight.y = height + rectangle.pointDownRight.y;
		rectangle.pointDownLeft.x = rectangle.pointDownRight.x - width;
		rectangle.pointUpLeft.x = rectangle.pointDownLeft.x;
		rectangle.pointUpLeft.y = rectangle.pointUpRight.y;
	}
	else {
		rectangle.pointDownLeft = rectangle.pointDownRight;
		rectangle.pointUpLeft.x = rectangle.pointDownLeft.x;
		rectangle.pointDownRight.y = rectangle.pointDownLeft.y;
		rectangle.pointUpLeft.y = height + rectangle.pointDownLeft.y;
		rectangle.pointDownRight.x = width + rectangle.pointDownLeft.x;
		rectangle.pointUpRight.x = rectangle.pointDownRight.x;
		rectangle.pointUpRight.y = rectangle.pointUpLeft.y;
	}
}

void MoveRectangle(Rectangle &rectangle, const Point *point, Point newPoint) {
	float x = newPoint.x - point->x;
	float y = newPoint.y - point->y;
	rectangle.pointDownLeft.x += x;
	rectangle.pointDownLeft.y += y;
	rectangle.pointDownRight.x += x;
	rectangle.pointDownRight.y += y;
	rectangle.pointUpLeft.x += x;
	rectangle.pointUpLeft.y += y;
	rectangle.pointUpRight.x += x;
	rectangle.pointUpRight.y += y;
}

void ChangeSize(Rectangle &rectangle, float height, float width) {
	// ��������� ��������� � ��� ������� ������� - ���������� ��� ����� ������ �������
	height /= 2;
	width /= 2;
	rectangle.pointDownRight.x += width;
	rectangle.pointUpRight.x += width;
	rectangle.pointDownLeft.x -= width;
	rectangle.pointUpLeft.x -= width;
	rectangle.pointDownRight.y -= height;
	rectangle.pointDownLeft.y -= height;
	rectangle.pointUpRight.y += height;
	rectangle.pointUpLeft.y += height;
}

/*5. ������� ���������, ����������� ������� �����. �������� �������, ����������� �������� ��������, ���������, ��������� � ������� ������*/

bool GetFractionNum(FractionNum &num1, FractionNum &num2) {
	cout << "������� ��������� � ����������� ������ �����\n";
	cin >> num1.numerator >> num1.denominator;
	cout << "������� ��������� � ����������� ������ �����\n";
	cin >> num2.numerator >> num2.denominator;
	if (num1.denominator <= 0 || num2.denominator <= 0) {
		return false;
	}
	return true;
}

char GetAction() {
	cout << "�������� ��������:\n"
			"1.��������\n"
			"2.���������\n"
			"3.���������\n"
			"4.�������\n"
			"Esc - �����\n";
	return _getch();
}

FractionNum Calc(const FractionNum *num1, const FractionNum *num2, char &ch) {
	FractionNum numRes{};
	switch (ch) {
	case '1':
		numRes = Plus(*&num1, *&num2);
		ch = '+';
		break;
	case '2':
		numRes = Minus(*&num1, *&num2);
		ch = '-';
		break;
	case '3':
		numRes = Multi(*&num1, *&num2);
		ch = '*';
		break;
	case '4':
		if (num2->numerator != 0) {
			numRes = Div(*&num1, *&num2);
		}
		ch = '/';
		break;
	}
	return numRes;
}

void ShowRes(const FractionNum *num1, const FractionNum *num2, const FractionNum *numRes, char sign) {
	cout << endl;
	if (num2->numerator == 0 && sign == '/') {
		cout << "������\n\n";
		return;
	}
	if (numRes->numerator == 0) {
		cout << num1->numerator << '/' << num1->denominator << ' ' << sign << ' ' << num2->numerator << '/' << num2->denominator << " = " << 0 << endl << endl;
		return;
	}
	else if (numRes->denominator == 1) {
		cout << num1->numerator << '/' << num1->denominator << ' ' << sign << ' ' << num2->numerator << '/' << num2->denominator << " = " << numRes->numerator << endl << endl;
		return;
	}
	cout << num1->numerator << '/' << num1->denominator << ' ' << sign << ' ' << num2->numerator << '/' << num2->denominator << " = " << numRes->numerator << '/' << numRes->denominator << endl << endl;
}

FractionNum Plus(const FractionNum *num1, const FractionNum *num2) {
	FractionNum temp;
	if (num1->denominator == num2->denominator) {
		temp.denominator = num1->denominator;
		temp.numerator = num1->numerator + num2->numerator;
		CutFractionNum(temp);
		return temp;
	}
	// ���������� ������ ����������� � ����������
	short num = 2;
	while (num1->denominator * num % num2->denominator != 0) {
		++num;
	}
	temp.denominator = num1->denominator * num;
	temp.numerator = num1->numerator * num;
	num = temp.denominator / num2->denominator;
	temp.numerator += num2->numerator * num;
	CutFractionNum(temp);
	return temp;
}

FractionNum Minus(const FractionNum *num1, const FractionNum *num2) {
	FractionNum temp;
	if (num1->denominator == num2->denominator) {
		temp.denominator = num1->denominator;
		temp.numerator = num1->numerator - num2->numerator;
		CutFractionNum(temp);
		return temp;
	}
	// ���������� ������ ����������� � ����������
	short num = 2;
	while (num1->denominator * num % num2->denominator != 0) {
		++num;
	}
	temp.denominator = num1->denominator * num;
	temp.numerator = num1->numerator * num;
	num = temp.denominator / num2->denominator;
	temp.numerator -= num2->numerator * num;
	CutFractionNum(temp);
	return temp;
}

FractionNum Multi(const FractionNum *num1, const FractionNum *num2) {
	FractionNum temp;
	temp.numerator = num1->numerator * num2->numerator;
	temp.denominator = num1->denominator * num2->denominator;
	CutFractionNum(temp);
	return temp;
}

FractionNum Div(const FractionNum *num1, const FractionNum *num2) {
	FractionNum temp;
	temp.numerator = num1->numerator * num2->denominator;
	temp.denominator = num1->denominator * num2->numerator;
	CutFractionNum(temp);
	return temp;
}

// ���������� �����
void CutFractionNum(FractionNum &num) {
	bool minus = num.numerator < 0;
	minus && (num.numerator *= -1);
	short temp = 2;
	if (num.numerator % num.denominator == 0) {
		num.numerator /= num.denominator;
		num.denominator = 1;
		minus && (num.numerator *= -1);
		return;
	}
	else if (num.denominator % num.numerator == 0) {
		num.denominator /= num.numerator;
		num.numerator = 1;
		minus && (num.numerator *= -1);
		return;
	}
	else {
		while (temp < num.numerator && temp < num.denominator) {
			if (num.numerator % temp == 0 && num.denominator % temp == 0) {
				num.numerator /= temp;
				num.denominator /= temp;
				temp = 2;
				continue;
			}
			++temp;
		}
	}
	minus && (num.numerator *= -1);
}