#include"Functions.h"
#include<iostream>
#include<conio.h>

using std::cout;
using std::cin;
using std::endl;

void Next() {
	system("pause");
	system("cls");
}

void main() {

	setlocale(LC_ALL, "rus");
	
	/*1. �������� ���������, ����������� ����� � ��������� ������� ��������� (x, y). � ������� ���� ��������� ������� ��� �����.
		 ����� �� ������, ���������� ����� ��� �����, ����������� ��� ������� ��� ��� �������*/

	//	 {
	//		 cout << "�������� ���������, ����������� ����� � ��������� ������� ��������� (x, y). �   ������� ���� ��������� ������� ��� �����."
	//				 "����� �� ������, ���������� ����� ���  �����, ����������� ��� ������� ��� ��� �������\n\n";
	//		 Point point1;
	//		 Point point2;
	//		 cout << "������� ��������� ������ ����� (x, y)\n";
	//		 cin >> point1.x >> point1.y;
	//		 cout << "������� ���������� ������ ����� (x, y)\n";
	//		 cin >> point2.x >> point2.y;
	//		 IsParallel(point1, point2) ? cout << "\n������, ���������� ����� ����� ����������� ��� ������� ��� ��� �������\n\n"
	//									: cout << "\n������, ���������� ����� ����� �� ����������� ��� ������� ��� ��� �������\n\n";
	//		 Next();
	//	 }

	// /*2. ������� ���������, ����������� �������������. �������� ������� ��� ����������� ��������� ������������ ����� � ������ �������������. ����� ����� ������ ����������� ����������*/

	//	 {
	//		 cout << "������� ���������, ����������� �������������. �������� ������� ��� �����������  ��������� ������������ ����� � ������ �������������. ����� ����� ������         ����������� ����������\n\n";
	//		 Rectangle rectangle;
	//		 cout << "������� ���������� ����� ������� ������� �������������� (x, y)\n";
	//		 cin >> rectangle.pointUpLeft.x >> rectangle.pointUpLeft.y;
	//		 cout << "������� ���������� ������ ������ ������� �������������� (x, y)\n";
	//		 cin >> rectangle.pointDownRight.x >> rectangle.pointDownRight.y;
	//		 if (rectangle.pointUpLeft.x >= rectangle.pointDownRight.x || rectangle.pointUpLeft.y <= rectangle.pointDownRight.y) {
	//			 cout << "\n������. �� ������� ����� ����������\n\n";
	//		 }
	//		 else {
	//			 rectangle.pointUpRight.x = rectangle.pointDownRight.x;
	//			 rectangle.pointUpRight.y = rectangle.pointUpLeft.y;
	//			 rectangle.pointDownLeft.x = rectangle.pointUpLeft.x;
	//			 rectangle.pointDownLeft.y = rectangle.pointDownRight.y;
	//			 Point point;
	//			 cout << "������� ���������� ������������ ����� (x, y)\n";
	//			 cin >> point.x >> point.y;
	//			 IsHit(rectangle, point) ? cout << "\n����� �������� � �������������\n\n"
	//									 : cout << "\n����� �� �������� � �������������\n\n";
	//		 }
	//		 cin.get();
	//		 Next();
	//	 }

	// /*3. ������� ���������, ����������� ��������. ������� ������� ��� ������ � ���� ���������� (��������, ���������� ������� � ���������� ������ �������)*/

	//	 {
	//		 cout << "������� ���������, ����������� ��������. ������� ������� ��� ������ � ����      ���������� (��������, ���������� ������� � ���������� ������ �������)\n\n";
	//		 Man man;
	//		 InitMan(&man);
	//		 system("cls");
	//		 ShowMan(man);
	//		 delete[] man.name;
	//		 delete[] man.surname;
	//		 Next();
	//	 }

	// /*4. ������� ���������, ����������� �������������. �������� ������� ��� ������ � ���� ���������� (��������, ������������ ��������������, ����������� ��������������,
	//	  ��������� ������� �������������� � �. �.)*/
	//  
	//{
	//	cout << "������� ���������, ����������� �������������. �������� ������� ��� ������ � �������������� (��������, ������������ ��������������, ����������� ��������������,"
	//		"  ��������� ������� �������������� � �. �.)\n\n";
	//	Rectangle rectangle;
	//	cout << "������� ���������� ����� ������� ������� A (x, y)\n";
	//	cin >> rectangle.pointUpLeft.x >> rectangle.pointUpLeft.y;
	//	cout << "������� ���������� ������ ������� ������� B (x, y)\n";
	//	cin >> rectangle.pointUpRight.x >> rectangle.pointUpRight.y;
	//	cout << "������� ��������� ������ ������ ������� C (x, y)\n";
	//	cin >> rectangle.pointDownRight.x >> rectangle.pointDownRight.y;
	//	cout << "������� ���������� ����� ������ ������� D (x, y)\n";
	//	cin >> rectangle.pointDownLeft.x >> rectangle.pointDownLeft.y;
	//	cin.get();
	//	cout.precision(1);
	//	if (rectangle.pointUpLeft.x >= rectangle.pointUpRight.x || rectangle.pointUpLeft.y <= rectangle.pointDownLeft.y || rectangle.pointUpLeft.y <= rectangle.pointDownRight.y ||
	//		rectangle.pointDownRight.x <= rectangle.pointDownLeft.x || rectangle.pointDownRight.y >= rectangle.pointUpRight.y) {
	//		cout << "�� ����� �������� ����������\n\n";
	//	}
	//	else if (!IsRectangle(rectangle)) {
	//		cout << "������ � ���������� ������������ �� �������� ���������������\n\n";
	//	}
	//	else {
	//		char ch;
	//		ShowCoord(rectangle);
	//		cout << "��� ����������� �������������� �������� ������� ��������������:\n\nA - ����� ������� �����\nB - ������ ������� �����\nC - ������ ������ �����\nD - ����� ������ �����\n";
	//		do {
	//			ch = toupper(_getch());
	//		} while (ch != 'A' && ch != 'B' && ch != 'C' && ch != 'D');
	//		Point point;
	//		cout << "\n������� ����� ���������� ������� ��������������\n";
	//		cin >> point.x >> point.y;
	//		cin.get();
	//		switch (ch) {
	//		case 'A':
	//			MoveRectangle(rectangle, &rectangle.pointUpLeft, point);
	//			break;
	//		case 'B':
	//			MoveRectangle(rectangle, &rectangle.pointUpRight, point);
	//			break;
	//		case 'C':
	//			MoveRectangle(rectangle, &rectangle.pointDownRight, point);
	//			break;
	//		case 'D':
	//			MoveRectangle(rectangle, &rectangle.pointDownLeft, point);
	//			break;
	//		}
	//		ShowCoord(rectangle);
	//		if (rectangle.pointUpLeft.x == rectangle.pointDownLeft.x || rectangle.pointUpLeft.y == rectangle.pointUpRight.y) {
	//			cout << "������������� ������������\n\n";
	//			ShowCoord(rectangle);
	//		}
	//		else {
	//			cout << "��������������� �������������:\n";
	//			NormRect(rectangle);
	//			ShowCoord(rectangle);
	//		}
	//		float height, width;
	//		cout << "������� �� ������� �������� ������� AB � CD\n";
	//		cin >> width;
	//		cout << "������� �� ������� �������� ������� BC � AD\n";
	//		cin >> height;
	//		ChangeSize(rectangle, height, width);
	//		ShowCoord(rectangle);
	//	}
	//	Next();
	//}

	  /*5. ������� ���������, ����������� ������� �����. �������� �������, ����������� �������� ��������, ���������, ��������� � ������� ������*/

	{
		cout << "5. ������� ���������, ����������� ������� �����. �������� �������, �����������  �������� ��������, ���������, ��������� � ������� ������\n\n";
		FractionNum num1, num2;
		char ch = 0;
		FractionNum numRes;
		while (true) {
			if (GetFractionNum(num1, num2)) {
				do {
					system("cls");
					ch = GetAction();
				} while (ch != '1' && ch != '2' && ch != '3' && ch != '4' && ch != 27);
				if (ch != 27) {
					numRes = Calc(&num1, &num2, ch);
					ShowRes(&num1, &num2, &numRes, ch);
					Next();
					continue;
				}
				break;
			}
			else {
				cout << "\n������\n\n";
				Next();
			}
		}
		cin.get();
		Next();
	}

	/*6. �������� ���������, ����������� ������ ������������� ������ � ������������ ����������� ���������. ��������� ������ ������������� ����������� ���������� �������� � ������,
	     �������� �������� �� ������, �������������� ������ � ��������, ������ ���������� � ��������� �� �����*/

	{
		Journal **journals = nullptr;
		short journSize = CreateJournals(journals);
		short journNum = 0;
		bool exit = false;
		short journChoice;
		action act;
		char choice = NOINFO;

		while (!exit) {
			switch (MenuMain()) {
			case ADD:
				if (journNum == journSize - 1) {
					journSize = CreateJournals(journals);
				}
				if (AddJournal(journals, journNum)) {
					cout << "\n������ ������\n\n";
					Next();
				}
				break;

			case DEL:
				if (journNum <= 0) {
					system("cls");
					cout << "\n������� ����������\n\n";
					Next();
				}
				else {
					journChoice = ChooseJournal(journals, journNum);
					if (DeleteJournal(journals, journNum, journChoice) && journChoice != CANCEL) {
						system("cls");
						cout << "\n������ ������\n\n";
						Next();
					}
				}
				break;

			case SHOW:
				if (journNum <= 0) {
					system("cls");
					cout << "\n������� ����������\n\n";
					Next();
				}
				else {
					if ((journChoice = ChooseJournal(journals, journNum)) != CANCEL) {
						while (true) {
							switch (act = MenuAdd(journals, journChoice)) {
							case ADD:
								AddStudent(journals, journChoice);
								break;

							case DEL:
								if (DeleteStudent(journals, journChoice)) {
									cout << "\n������� ������\n\n";
									Next();
								}
								else if (journals[journChoice - 1]->countStudents <= 0) {
									system("cls");
									cout << "\n������ ����\n\n";
									Next();
								}
								break;

							case SHOW:
								ShowJournal(journals, journChoice);
								break;

							case EDIT:
								while (true) {
									do {
										system("cls");
										cout << "1. ������\n"
												"2. �������\n"
												"3. ������\n"
												"ESC - �����\n";
										choice = _getch();
									} while (choice != JOURNAL && choice != STUDENT && choice != POINTS && choice != ESC);
									if (choice == ESC) {
										break;
									}
									EditData(journals, journChoice, choice);
								}
								break;
							}
							if (act == EXIT) {
								break;
							}
						}
					}
				}
				break;

			case FIND:
				if (journNum <= 0) {
					system("cls");
					cout << "\n������� ����������\n\n";
					Next();
				}
				else {
					ShowStudent(journals, FindStudent(journals, journNum));
				}
				break;
			default :
				exit = true;
				break;
			}
		}
		DeleteJournals(journals, journSize);
		cout << endl;
		Next();
	}
}