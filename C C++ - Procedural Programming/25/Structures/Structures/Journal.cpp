#include"Functions.h"
#include<iostream>
#include<conio.h>

using std::cout;
using std::endl;

/*6. �������� ���������, ����������� ������ ������������� ������ � ������������ ����������� ���������. ��������� ������ ������������� ����������� ���������� �������� � ������,
	 �������� �������� �� ������, �������������� ������ � ��������, ������ ���������� � ��������� �� �����*/

short CreateJournals(Journal **&journals) {
	static short size = 10;
	Journal **temp = new Journal*[size];
	for (int i = 0; i < size; ++i) {
		temp[i] = new Journal;
	}
	if (size > 10) {
		for (int i = 0; i < size - 10; ++i) {
			temp[i] = journals[i];
		}
	}
	delete[] journals;
	journals = temp;
	temp = nullptr;
	size += 10;
	return size - 10;
}

bool AddJournal(Journal **journals, short &journNum) {
	if (journals == nullptr || journNum < 0) {
		return false;
	}
	char choice = NOINFO;
	short count;
	char *temp = new char[8]{};
	do {
		system("cls");
		cout << "1.������� ������ ������\n"
				"2.������� ������ ������ � �������� ��������\n"
				"3.������� � ��������� ������ � �������� ��������\n"
				"0.������\n";
		choice = _getch() - ZERO;
	} while (choice != EMP && choice != EMP_SIZE && choice != EMP_SIZE_FILL && choice - 1 != CANCEL);
	if (choice - 1 == CANCEL) {
		return false;
	}
	system("cls");
	cout << "������� �������� ������\n";
	gets_s(journals[journNum++]->ID, GROUP_S - 1);
	if (choice == EMP) {
		return true;
	}
	do {
		system("cls");
		cout << "������� ���������� ���������\n";
		count = atoi(gets_s(temp, 8));
	} while (count <= 0);
	journals[journNum - 1]->students = new Student[count];
	journals[journNum - 1]->countStudents = count;
	if (choice == EMP_SIZE) {
		return true;
	}
	for (int i = 0; i < count; ++i) {
		system("cls");
		cout << "�������: ";
		gets_s(journals[journNum - 1]->students[i].surname, SURNAME_S - 1);
		cout << "���: ";
		gets_s(journals[journNum - 1]->students[i].name, NAME_S - 1);
	}
	return true;
}

bool DeleteJournal(Journal **journals, short &journNum, short journChoice) {
	if (journals == nullptr || journChoice <= 0 || journChoice > journNum || journNum <= 0) {
		return false;
	}
	delete journals[--journChoice];
	--journNum;
	for (journChoice; journChoice < journNum; ++journChoice) {
		journals[journChoice] = journals[journChoice + 1];
	}
	journals[journNum] = new Journal;
	return true;
}

short ChooseJournal(Journal **journals, short journNum) {
	if (journals == nullptr || journNum <= 0) {
		system("cls");
		cout << "\n������ ����\n\n";
		system("pause");
		return CANCEL;
	}
	short id = NOINFO;
	char *temp = new char[GROUP_S];
	bool cmpID = true;
	short i;
	// ����� ������ �� ����������� ������ ��� �������� ������
	do {
		system("cls");
		cout << "�������� ������:\n";
		for (int i = 0; i < journNum; ++i) {
			cout << i + 1 << "." << journals[i]->ID << endl;
		}
		cout << "0.������\n\n";
		id = atoi(gets_s(temp, GROUP_S - 1));
		if (!strcmp(temp, "0")) {
			return CANCEL;
			delete[] temp;
		}
		for (i = 0; i < journNum && cmpID; ++i) {
			cmpID = strcmp(temp, journals[i]->ID);
		}
		cmpID || (id = i);
		if (id == 0) {
			cout << "\n������ �� �������\n\n" && system("pause");
		}
	} while (id <= 0 || id > journNum);
	delete[] temp;
	temp = nullptr;
	return id;
}

bool ShowJournal(Journal **journals, short journChoice) {
	if (journals == nullptr || journChoice <= 0) {
		return false;
	}
	--journChoice;
	system("cls");
	cout << "������ " << journals[journChoice]->ID << endl;
	cout << "��������� ";
	switch (journals[journChoice]->faculty) {
	case CODER:
		cout << "����������������\n";
		break;
	case ADMIN:
		cout << "�����������������\n";
		break;
	case DESIGN:
		cout << "������\n";
		break;
	default:
		cout << endl;
	}
	if (journals[journChoice]->course == NOINFO) {
		cout << "���� \n";
	}
	else {
		cout << "���� " << journals[journChoice]->course << endl << endl;
	}
	cout << "\n��������:\n\n";
	for (int i = 0; i < journals[journChoice]->countStudents; ++i) {
		cout << i + 1 << '.' << journals[journChoice]->students[i].surname << ' ' << journals[journChoice]->students[i].name << "\t" << journals[journChoice]->students[i].points << endl;
		cout << journals[journChoice]->students[i].comment << endl << endl;
	}
	cout << journals[journChoice]->comment << endl << endl;
	system("pause");
}

action MenuMain() {
	action act;
	char choice;
	do {
		system("cls");
		cout << "1.�������� ������� ������\n"
				"2.�������� ������� ������\n"
				"3.�������� � �������������� ������� ������\n"
				"4.����� ��������\n"
				"ESC - �����\n";
		act = (action)(_getch() - ZERO);
		if (act == EXIT) {
			do {
				system("cls");
				cout << "�� �������, ��� ������ ����� �� ���������? (Y/N)\n";
				choice = toupper(_getch());
			} while (choice != 'Y' && choice != 'N');
			if (choice == 'Y') return act;
		}
	} while (act != ADD && act != DEL && act != SHOW && act != FIND);
	return act;
}

action MenuAdd(Journal **journals, short journChoice) {
	action act;
	--journChoice;
	do {
		system("cls");
		cout << journals[journChoice]->ID << endl;
		cout << "1.���������� ��������\n"
				"2.�������� ��������\n"
				"3.�������� ������\n"
				"4.�������������� ������\n"
				"ESC - �����\n";
		act = (action)(_getch() - '0');
	} while (act != ADD && act != DEL && act != SHOW && act != EDIT && act != EXIT);
	if (act == EXIT) {
		return EXIT;
	}
	return act;
}

bool AddStudent(Journal **journals, short journChoice) {
	if (journals == nullptr || journChoice <= 0) {
		return false;
	}
	--journChoice;
	Student *temp = new Student[journals[journChoice]->countStudents + 1];
	for (int i = 0; i < journals[journChoice]->countStudents; ++i) {
		temp[i] = journals[journChoice]->students[i];
	}
	delete[] journals[journChoice]->students;
	journals[journChoice]->students = temp;
	temp = nullptr;
	system("cls");
	cout << "�������: ";
	gets_s(journals[journChoice]->students[journals[journChoice]->countStudents].surname, SURNAME_S - 1);
	cout << "���: ";
	gets_s(journals[journChoice]->students[journals[journChoice]->countStudents].name, NAME_S - 1);
	++journals[journChoice]->countStudents;
	return true;
}

bool DeleteStudent(Journal **journals, short journChoice) {
	if (journals == nullptr || journChoice <= 0 || journals[journChoice - 1]->countStudents <= 0) {
		return false;
	}
	short id = ChooseStudent(journals, journChoice);
	if (id == CANCEL) {
		return false;
	}
	--journChoice;
	--journals[journChoice]->countStudents;
	for (--id; id < journals[journChoice]->countStudents; ++id) {
		journals[journChoice]->students[id] = journals[journChoice]->students[id + 1];
	}
	return true;
}

short ChooseStudent(Journal **journals, short journChoice) {
	if (journals == nullptr || journChoice <= 0) {
		return CANCEL;
	}
	--journChoice;
	short id = NOINFO;
	char *temp = new char[NAME_S];
	char *tempCat = new char[NAME_S + SURNAME_S + 1];
	bool cmpSurname = true;
	bool cmpSurName = true;
	short j;
	// ����� �������� � ������� ������ �� ����������� ������, ������� ��� ������� � �����
	do {
		system("cls");
		cout << "�������� ��������\n";
		for (int i = 0; i < journals[journChoice]->countStudents; ++i) {
			cout << i + 1 << '.' << journals[journChoice]->students[i].surname << ' ' << journals[journChoice]->students[i].name << endl;
		}
		cout << "0.������\n\n";
		id = atoi(gets_s(temp, NAME_S - 1));
		if (!strcmp(temp, "0")) {
			delete[] temp;
			delete[] tempCat;
			return CANCEL;
		}
		for (j = 0; j < journals[journChoice]->countStudents && cmpSurname && cmpSurName; ++j) {
			strcpy_s(tempCat, SURNAME_S, journals[journChoice]->students[j].surname);
			cmpSurname = strcmp(temp, tempCat);
			tempCat[strlen(journals[journChoice]->students[j].surname)] = ' ';
			tempCat[strlen(journals[journChoice]->students[j].surname) + 1] = '\0';
			strcat_s(tempCat, NAME_S, journals[journChoice]->students[j].name);
			cmpSurName = strcmp(temp, tempCat);
		}
		if (cmpSurname == false || cmpSurName == false) {
			id = j;
		}
		if (id == 0) {
			cout << "\n������� �� ������\n\n" && system("pause");
		}
	} while (id <= 0 || id > journals[journChoice]->countStudents);
	delete[] temp;
	delete[] tempCat;
	return id;
}

bool EditData(Journal **journals, short journChoice, char choice) {
	if (journals == nullptr || journChoice <= 0) {
		return false;
	}
	--journChoice;
	short id = NOINFO;
	switch (choice) {
	case JOURNAL:
		EditJournal(journals, journChoice);
		break;
	case STUDENT:
	case POINTS:
		if (journals[journChoice]->countStudents == 0) {
			system("cls");
			cout << "\n������ ����\n\n";
			system("pause");
			return false;
		}
		id = ChooseStudent(journals, journChoice + 1);
		if (id == CANCEL) {
			return false;
		}
		choice == POINTS ? EditPoints(journals, journChoice, id) : EditStudent(journals, journChoice, id);
		break;
	}
	return true;
}

bool EditPoints(Journal **journals, short journChoice, short id) {
	if (journals == nullptr || journChoice < 0 || id <= 0) {
		return false;
	}
	char choice = NOINFO;
	--id;
	do {
		system("cls");
		cout << journals[journChoice]->students[id].surname << journals[journChoice]->students[id].name << endl;
		cout << "��������� ������ (0-5): ";
		choice = _getch();
	} while (choice < '0' || choice > '5');
	journals[journChoice]->students[id].points += choice - ZERO;
	return true;
}

bool EditStudent(Journal **journals, short journChoice, short id) {
	if (journals == nullptr || journChoice < 0 || id <= 0) {
		return false;
	}
	char choice = NOINFO;
	--id;
	do {
		system("cls");
		cout << journals[journChoice]->students[id].surname << ' ' << journals[journChoice]->students[id].name << endl;
		cout << "1. ������������� �������\n"
				"2. ������������� ���\n"
				"3. �����������\n"
				"ESC - �����";
		choice = _getch();
		switch (choice) {
		case SURNAME:
			system("cls");
			cout << "�������: ";
			gets_s(journals[journChoice]->students[id].surname, SURNAME_S - 1);
			break;
		case NAME:
			system("cls");
			cout << "���: ";
			gets_s(journals[journChoice]->students[id].name, NAME_S - 1);
			break;
		case COMMENT_st:
			system("cls");
			cout << "�����������: ";
			cout << journals[journChoice]->students[id].surname << ' ' << journals[journChoice]->students[id].name << endl;
			gets_s(journals[journChoice]->students[id].comment, COMMENT_S - 1);
			break;
		}
	} while (choice != ESC);
	return true;
}

bool EditJournal(Journal **journals, short journChoice) {
	if (journals == nullptr || journChoice < 0) {
		return false;
	}
	char choice = NOINFO;
	faculty temp = NONE;
	char course;
	do {
		system("cls");
		cout << journals[journChoice]->ID << endl;
		cout << "1. �������� ������\n"
				"2. ���������\n"
				"3. ����\n"
				"4. �����������\n"
				"ESC - �����\n";
		choice = _getch();
		switch (choice) {
		case GROUP:
			system("cls");
			cout << "������: ";
			gets_s(journals[journChoice]->ID, GROUP_S - 1);
			break;
		case FACULTY:
			do {
				system("cls");
				cout << "���������:\n"
						"1. ����������������\n"
						"2. �����������������\n"
						"3. ������\n";
				temp = faculty(_getch() - ZERO);
			} while (temp != CODER && temp != ADMIN && temp != DESIGN);
			journals[journChoice]->faculty = temp;
			break;
		case COURSE:
			system("cls");
			cout << "���� (1-5): ";
			do {
				course = _getch();
			} while (course < '1' || course > '5');
			journals[journChoice]->course = course;
			break;
		case COMMENT_j:
			system("cls");
			cout << "�����������: ";
			cout << journals[journChoice]->ID << endl;
			gets_s(journals[journChoice]->comment, COMMENT_j - 1);
			break;
		}
	} while (choice != ESC);
	return true;
}

short FindStudent(Journal **journals, short journNum) {
	if (journals == nullptr || journNum <= 0) {
		return CANCEL;
	}
	bool flag = true;
	for (int i = 0; i < journNum && flag; ++i) {
		for (int j = 0; j < journals[i]->countStudents; ++j) {
			if (journals[i]->students[j].surname[0] != '\0' || journals[i]->students[j].name[0] != '\0') {
				flag = false;
				break;
			}
		}
	}
	if (flag) {
		system("cls");
		cout << "\n��� ������ � ���������\n\n";
		system("pause");
		return CANCEL;
	}
	short id = NOINFO;
	char *temp = new char[NAME_S];
	char *tempCat = new char[NAME_S + SURNAME_S + 1];
	bool cmpSurname = true;
	bool cmpSurName = true;
	short k;
	short count;
	// ����� �������� �� ���� �������� �� ����������� ������, ������� ��� ������� � �����
	do {
		k = 0;
		system("cls");
		cout << "�������� ��������\n";
		count = 0;
		for (int i = 0; i < journNum; ++i) {
			cout << "\n������ " << journals[i]->ID << endl;
			for (int k = 0; k < journals[i]->countStudents; ++k) {
				cout << count + 1 << '.' << journals[i]->students[k].surname << ' ' << journals[i]->students[k].name << endl;
				++count;
			}
		}
		cout << "\n0.������\n\n";
		id = atoi(gets_s(temp, NAME_S - 1));
		if (!strcmp(temp, "0")) {
			delete[] temp;
			delete[] tempCat;
			return CANCEL;
		}
		for (int i = 0; i < journNum; ++i) {
			for (int j = 0; j < journals[i]->countStudents && cmpSurname && cmpSurName; ++j) {
				strcpy_s(tempCat, SURNAME_S, journals[i]->students[j].surname);
				cmpSurname = strcmp(temp, tempCat);
				tempCat[strlen(journals[i]->students[j].surname)] = ' ';
				tempCat[strlen(journals[i]->students[j].surname) + 1] = '\0';
				strcat_s(tempCat, NAME_S, journals[i]->students[j].name);
				cmpSurName = strcmp(temp, tempCat);
				++k;
			}
		}
		if (cmpSurname == false || cmpSurName == false) {
			id = k;
		}
		if (id == 0) {
			cout << "\n������� �� ������\n\n" && system("pause");
		}
	} while (id <= 0 || id > count);
	delete[] temp;
	delete[] tempCat;
	return id;
}

bool ShowStudent(Journal **journals, short id) {
	if (journals == nullptr || id == CANCEL) {
		return false;
	}
	short journNum = 0;
	int i;
	while (id) {
		for (i = 0; i < journals[journNum]->countStudents && id; ++i) {
			--id;
		}
		++journNum;
	}
	--journNum, --i;
	system("cls");
	cout << "������� " << journals[journNum]->students[i].surname << ' ' << journals[journNum]->students[i].name << "\t" << journals[journNum]->students->points << endl;
	cout << "������ " << journals[journNum]->ID << endl;
	cout << "��������� ";
	switch (journals[journNum]->faculty) {
	case CODER:
		cout << "����������������";
		break;
	case ADMIN:
		cout << "�����������������";
		break;
	case DESIGN:
		cout << "������";
		break;
	}
	cout << "\n���� " << journals[journNum]->course << endl;
	cout << journals[journNum]->students->comment << endl << endl;
	system("pause");
	return true;
}

void DeleteJournals(Journal **&journals, short journSize) {
	if (journals == nullptr || journSize <= 0) {
		return;
	}
	for (int i = 0; i < journSize; ++i) {
		delete journals[i];
	}
	delete[] journals;
	journals = nullptr;
}