#include<iostream>
#include<time.h>
#include<windows.h>

using namespace std;





void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	/*1. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 100. �������� ������� �������� �������, ������������� ����������� ������������ ������� ���������.*/
	
	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 101;
				if (i == j) {
					SetConsoleTextAttribute(handle, 2);
				}
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			cout << "\n  ";
		}
		cout << "\n\n  ";


		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				if (j > i) {
					swap(arr[i][j], arr[j][i]);
				}
				if (i == j) {
					SetConsoleTextAttribute(handle, 2);
				}
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			cout << "\n  ";
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*2. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 100. �������� ������� �������� �������, ������������� �� ������� ���������,
	    � ���������� �������, �������������� �� �������� ���������.*/
	
	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 101;
				if (i == j) {
					SetConsoleTextAttribute(handle, 2);
				}
				else if (j == COL - 1 - i) {
					SetConsoleTextAttribute(handle, 4);
				}
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			cout << "\n  ";
		}

		
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				if (i == j) {
					i > ROW / 2 ? swap(arr[i][j], arr[i][COL - 1 - i]) : swap(arr[i][j], arr[i][COL - 1 - j]);
				}
			}
		}
				
		
		cout << "\n\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				if (i == j) {
					SetConsoleTextAttribute(handle, 4);
				}
				else if (j == COL - 1 - i) {
					SetConsoleTextAttribute(handle, 2);
				}
				if (i == ROW / 2 && j == COL / 2) {
					SetConsoleTextAttribute(handle, 2);
				}
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			cout << "\n  ";
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*3. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� -100 �� 100. ���������� ����� ��������� �������, ������������� ����� ����������� � ������������ ����������.*/
	
	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		bool first = true;
		int min, max;
		int iMin, jMin;
		int iMax, jMax;

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 201 - 100;
				if (first) {
					min = arr[i][j];
					max = arr[i][j];
					iMin = iMax = i;
					jMin = jMax = j;
					first = false;
				}
				if (arr[i][j] < min) {
					min = arr[i][j];
					iMin = i;
					jMin = j;
				}
				if (arr[i][j] > max) {
					max = arr[i][j];
					iMax = i;
					jMax = j;
				}
				cout << arr[i][j] << (arr[i][j] == -100 ? " " : arr[i][j] == 100 || arr[i][j] < -9 ? "  " : arr[i][j] < 0 || arr[i][j] > 9 ? "   " : "    ");
			}
			cout << "\n  ";
		}
		cout << "\n����������� �������  = " << min << " ������ � " << iMin + 1 << " ������� � " << jMin + 1 << endl;
		cout << "������������ ������� =  " << max << " ������ � " << iMax + 1 << " ������� � " << jMax + 1 << endl;

		// ����� ������� �����, ������ ����� ������� � arr[i][j - 1 ��� j + 1] == min ���� �� �������� �������� arr[i][j] == max, �� � ������� ����� ������ ����� ������� ������������ � ������������� ���������

		int minElements = iMin * ROW + jMin;
		int maxElements = iMax * ROW + jMax;
		int sum = 0;
		bool flag = true;
		

		if (minElements < maxElements) {
			for (int i = iMin, j = jMin + 1; flag; j = 0, ++i) {
				for (; j < COL; ++j) {
					if (i == iMax && j == jMax) {
						flag = false;
						break;
					}
					sum += arr[i][j];
				}
			}
		}
		else {
			for (int i = iMin, j = jMin - 1; flag; j = COL - 1, --i) {
				for (; j >= 0; --j) {
					if (i == iMax && j == jMax) {
						flag = false;
						break;
					}
					sum += arr[i][j];
				}
			}
		}


		cout << "����� ��������� ����� ����������� � ������������ ���������� = " << sum << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*4. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 100. �������� ������� ��������� ������� ������� �� ��������, � ������� ��������� ������������ �������.*/
	
	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		bool first = true;
		int max, col;

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 101;
				if (first) {
					max = arr[i][j];
					col = j;
					first = false;
				}
				if (arr[i][j] > max) {
					max = arr[i][j];
					col = j;
				}
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << "\n\n������������ ������� ��������� � ������� � " << col + 1 << "\n������������ ������� = " << max << endl;


		for (int i = 0; i < ROW; ++i) {
			swap(arr[i][col], arr[i][COL - 1]);
		}


		if (col != COL - 1) {
			cout << "\n\n     ����� ������\n\n  ";
			for (int i = 0; i < ROW; ++i) {
				for (int j = 0; j < COL; ++j) {
					cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
				}
				cout << "\n  ";
			}
		}
		else {
			cout << "\n\n������������ ������� ��� ��������� � ��������� �������\n";
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*5. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 100. �������� ������� ��������� ������ ������� �� �������, � ������� ��������� ����������� �������.*/
	
	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		bool first = true;
		int min, row;

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 101;
				if (first) {
					min = arr[i][j];
					row = i;
					first = false;
				}
				if (arr[i][j] < min) {
					min = arr[i][j];
					row = i;
				}
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << "\n\n����������� ������� ��������� � ������ � " << row + 1 << "\n����������� ������� = " << min << endl;


		for (int i = 0; i < COL; ++i) {
			swap(arr[row][i], arr[ROW - 1][i]);
		}


		if (row != ROW - 1) {
			cout << "\n\n     ����� ������\n\n  ";
			for (int i = 0; i < ROW; ++i) {
				for (int j = 0; j < COL; ++j) {
					cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
				}
				cout << "\n  ";
			}
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*6. ��� ��������� ������ ������������ 5�5, ����������� ���������� ������� �� ��������� �� 0 �� 100. ��������������� ������ ����� �������, ����� ��� ������� ������������� �� �������� �� ������������ ����.*/
	
	// ������ � �������� ���� � ��������-������

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int col[COL]{};
		int sum, sumMax;
		bool flag = true;
		int index;

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 101;
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl;

		for (int i = 0; i < COL; ++i) {
			sum = 0;
			for (int j = 0; j < ROW; ++j) {
				sum += arr[j][i];
			}
			col[i] = sum;
			cout << "����� ��������� ������� � " << i + 1 << " = " << sum << endl;
		}


		int k = 0;
		while (k < COL) {
			index = 0;
			bool check = true;
			for (int i = 0; i < COL; ++i) {
				if (check) {
					sumMax = col[0];
					check = false;
				}
				if (col[i] > sumMax) {
					sumMax = col[i];
					index = i;
				}
			}
			col[index] = k++;
		}

		int arrCopy[ROW][COL]{};
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arrCopy[i][j] = arr[i][j];
			}
		}

		for (int i = 0; i < COL; ++i) {
			for (int j = 0; j < ROW; ++j) {
				arr[j][col[i]] = arrCopy[j][i];
			}
		}


		cout << "\n    ����� ������\n\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");
	
	// ������ �����������

	// ����������� ����������

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int sum, sumNext;
		bool flag = true;
		int index;

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 101;
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}

		cout << endl;
		for (int i = 0; i < COL; ++i) {
			sum = 0;
			for (int j = 0; j < ROW; ++j) {
				sum += arr[j][i];
			}
			cout << "����� ��������� ������� � " << i + 1 << " = " << sum << endl;
		}


		int l = 0;
		bool isSwap;
		do {
			isSwap = false;
			for (int i = 0; i < COL; ++i) {
				sum = sumNext = 0;
				for (int j = 0; j < ROW; ++j) {
					sum += arr[j][i];
					sumNext += arr[j][i + 1];
				}
				if (sum < sumNext) {
					for (int k = 0; k < ROW; ++k) {
						swap(arr[k][i], arr[k][i + 1]);
						isSwap = true;
					}
				}
			}
			++l;
		} while (l < COL && isSwap);


		cout << "\n    ����� ������\n\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");
	
	// ���������� �������

	{
		const int ROW = 5;
		const int COL = 5;
		int arr[ROW][COL]{};
		int sum, sumMax;
		bool flag = true;
		int index;

		cout << "\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = rand() % 101;
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}

		cout << endl;
		for (int i = 0; i < COL; ++i) {
			sum = 0;
			for (int j = 0; j < ROW; ++j) {
				sum += arr[j][i];
			}
			cout << "����� ��������� ������� � " << i + 1 << " = " << sum << endl;
		}


		short col;
		int l = 0;
		while (l < COL) {
			for (int m = 0; m < COL; ++m) {
				bool first = true;
				sumMax = 0;
				for (int i = l; i < COL; ++i) {
					sum = 0;
					for (int j = 0; j < ROW; ++j) {
						sum += arr[j][i];
					}
					if (first) {
						sumMax = sum;
						col = i;
						first = false;
					}
					if (sum > sumMax) {
						sumMax = sum;
						col = i;
					}
				}
			}
			for (int i = 0; i < ROW; ++i) {
				swap(arr[i][l], arr[i][col]);
			}
			++l;
		} 


		cout << "\n    ����� ������\n\n  ";
		for (int i = 0; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*7. ��� ��������� ������ ������������ N x M, ����������� ���������� ������� �� ��������� �� 0 �� 100. ��������� ����������� ����� ������� �� �������� ���������� ��������.
	     ����������� ������ ������ ������������.*/
	
	{
		const int N = 10;
		const int M = 5;
		int arr[N][M]{};
		bool first = true;
		short shift, dir;
		int num;

		cout << "\n  ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				arr[i][j] = rand() % 101;
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}

		do {
			if (first) {
				cout << "\n\n������� ���������� �������� ��� ������ (�� 1 �� 5):\n";
				first = false;
			}
			cin >> shift;
		} while (shift < 1 || shift > 5);

		cout << "�������� ����������� ��� ������:\n\t\t\t\t      1 - ������\n\t\t\t\t      2 - �����\n";
		do {
			cin >> dir;
		} while (dir != 1 && dir != 2);


		short index = shift;
		if (dir == 1) {
			while (index > 0) {
				for (int i = 0; i < N; ++i) {
					int num = arr[i][M - 1];
					for (int j = M - 1; j > 0; --j) {
						arr[i][j] = arr[i][j - 1];
					}
					arr[i][0] = num;
				}
				--index;
			}
		}
		else {
			while (index > 0) {
				for (int i = 0; i < N; ++i) {
					int num = arr[i][0];
					for (int j = 0; j < M; ++j) {
						arr[i][j] = arr[i][j + 1];
					}
					arr[i][M - 1] = num;
				}
				--index;
			}
		}


		cout << "\n������ �������" << (dir == 1 ? " ������ " : " ����� ");
		cout << "�� " << shift;
		switch (shift) {
		case 1:
			cout << " �������:\n\n";
			break;
		case 2:
		case 3:
		case 4:
			cout << " �������:\n\n";
			break;
		default:
			cout << " ��������:\n\n";
		}
		cout << "  ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl;
	}
	system("pause");
	system("cls");

	/*8. ��� ��������� ������ ������������ N x M, ����������� ���������� ������� �� ��������� �� 0 �� 100. ��������� ����������� ����� ������� �� �������� ���������� �����.
		 ����������� ������ ������ ������������.*/
	
	{
		const int N = 10;
		const int M = 5;
		int arr[N][M]{};

		bool first = true;
		short shift, dir;
		int num;

		cout << "\n  ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				arr[i][j] = rand() % 101;
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}

		do {
			if (first) {
				cout << "\n\n������� ���������� ����� ��� ������ (�� 1 �� 10):\n";
				first = false;
			}
			cin >> shift;
		} while (shift < 1 || shift > 10);

		cout << "�������� ����������� ��� ������:\n\t\t\t\t      1 - �����\n\t\t\t\t      2 - ����\n";
		do {
			cin >> dir;
		} while (dir != 1 && dir != 2);


		short index = shift;
		if (dir == 1) {
			while (index > 0) {
				for (int i = 0; i < M; ++i) {
					num = arr[0][i];
					for (int j = 0; j < N; ++j) {
						arr[j][i] = arr[j + 1][i];
					}
					arr[N - 1][i] = num;
				}
				--index;
			}
		}
		else {
			while (index > 0) {
				for (int i = 0; i < M; ++i) {
					num = arr[N - 1][i];
					for (int j = N - 1; j >= 0; --j) {
						arr[j][i] = arr[j - 1][i];
					}
					arr[0][i] = num;
				}
				--index;
			}
		}


		cout << "\n������ �������" << (dir == 1 ? " ����� " : " ���� ");
		cout << "�� " << shift;
		switch (shift) {
		case 1:
			cout << " ������:\n\n";
			break;
		case 2:
		case 3:
		case 4:
			cout << " ������:\n\n";
			break;
		default:
			cout << " �����:\n\n";
		}
		cout << "  ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				cout << arr[i][j] << (arr[i][j] == 100 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl;
	}
		 system("pause");
		 system("cls");

	/*9. ��������� ���������� ������� �������� N x N �� ������� (N � �������� �����). ����� 1 �������� � ����� �������, � ����� ������ ����������� �� ������� ������ ������� ������� ���������� �� �����������.*/
		 
	{
		const int N = 19;
		int arr[N][N]{};
		int num = 1;
		short mid = N / 2;
		short loop = 1;
		short left, right, up, down;


		arr[mid][mid] = num++;
		while (loop <= mid) {
			left = mid - loop;
			right = mid + loop;
			up = mid - loop;
			down = mid + loop;
				for (int coord = -(loop - 1); coord <= loop; ++coord) {
					arr[mid + coord][left] = num++;
				}
				for (int coord = -(loop - 1); coord <= loop; ++coord) {
					arr[down][mid + coord] = num++;
				}
				for (int coord = loop - 1; coord >= -loop; --coord) {
					arr[mid + coord][right] = num++;
				}
				for (int coord = loop - 1; coord >= -loop; --coord) {
					arr[up][mid + coord] = num++;
				}
				++loop;
		}


		cout << "\n\n  ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < N; ++j) {
				cout << arr[i][j] << (arr[i][j] > 99 ? " " : arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/* ������ ������������� */

	// ������������� ������� � �������

	{
		const int ROW = 11;
		const int COL = 10;
		int arr[ROW][COL]{};


		cout << "\n  ";
		for (int i = 1; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				arr[i][j] = j * 10 + i;
				cout << arr[i][j] << (arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl;
	}
	system("pause");
	system("cls");

	// ������������� ������� �������

	{
		const int ROW = 11;
		const int COL = 10;
		int arr[ROW][COL]{};


		cout << "\n  ";
		for (int i = 1; i < ROW; ++i) {
			for (int j = 0; j < COL; ++j) {
				if (j % 2 == 0) {
					arr[i][j] = j * 10 + i;
				}
				else {
					arr[i][j] = j * 10 + 10 - i + 1;
				}
				cout << arr[i][j] << (arr[i][j] > 9 ? "  " : "   ");
			}
			cout << "\n  ";
		}
		cout << endl;
	}
	system("pause");
	system("cls");

	/*10. ���� ��� ������� N x M � M x K. ��������� ��������� ������ (�������������� ������� ������ ����� ������ N x K).*/
	
	{
		const int N = 4;
		const int M = 7;
		const int K = 5;
		int arr1[N][M]{ {2,7,1,2,4,8,3}, {3,5,4,8,2,1,1,}, {9,5,1,4,0,7,6}, {7,0,5,8,2,1,4} };
		int arr2[M][K]{ {7,8,3,5,2}, {5,6,2,8,4}, {8,0,7,5,3}, {0,3,1,2,3}, {8,0,3,9,7}, {3,1,6,4,5}, {3,4,2,8,4} };
		int arr[N][K]{};

		cout << "  ������ �������:\n\n   ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < M; ++j) {
				cout << arr1[i][j] << "  ";
			}
			cout << "\n   ";
		}
		cout << endl;
		cout << "  ������ �������:\n\n   ";
		for (int i = 0; i < M; ++i) {
			for (int j = 0; j < K; ++j) {
				cout << arr2[i][j] << "  ";
			}
			cout << "\n   ";
		}


		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < K; ++j) {
				for (int m = 0; m < M; ++m) {
					arr[i][j] += arr1[i][m] * arr2[m][j];
				}
			}
		}


		cout << "\n  �������������� �������:\n\n   ";
		for (int i = 0; i < N; ++i) {
			for (int j = 0; j < K; ++j) {
				cout << arr[i][j] << (arr[i][j] > 99 ? "  " : arr[i][j] > 9 ? "   " : "    ");
			}
			cout << "\n   ";
		}
		cout << endl;
	}

	
	
	



}
