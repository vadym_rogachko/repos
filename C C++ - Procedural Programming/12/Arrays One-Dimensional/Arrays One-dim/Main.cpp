#include<iostream>
#include<time.h>
#include<Windows.h>

using namespace std;




void main() {

	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

	/*1. ������ ������ �� 5 �����, � ����� ������� ��� � �������� �������.*/
	
	{
		cout << "������� 5 �����\n";
		const int SIZE = 5;
		int arr[SIZE]{};
		for (int i = 0; i < SIZE; ++i) {
			cin >> arr[i];
		}
		cout << "\n����� � �������� �������:\n\n";
		for (int i = SIZE - 1; i >= 0; --i) {
			cout << arr[i] << '\t';
		}
		cout << endl << endl;;
	}
	system("pause");
	system("cls");

	/*2. ������� ������ �� 20 ��������� �����. ������� ��� �������� ������� � ������� ���������.*/
	
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		cout << "�������� � ������� ��������� ������� �� 20 ��������� �����:\n\n";
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand();
			if (i % 2 == 0) {
				cout << i << ". " << arr[i] << endl;
			}
		}
		cout << endl;
	}
	system("pause");
	system("cls");

	/*3. ������� ������ �� 10 ��������� ����� � ��������� �� -20 �� 20. ���������� ������� �������������� ������������� ��������� �������.*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int sum = 0, j = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 41 - 20;
			cout << arr[i] << '\t';
			if (arr[i] > 0) {
				sum += arr[i];
				++j;
			}
		}
		cout << "\n������� �������������� ������������� ��������� �������: " << double(sum) / j << endl << endl;
	}
	system("pause");
	system("cls");

	/*4. ������� ������ �� 10 ��������� ����� � ��������� �� -5 �� 5. ���������� ���������� �������������, ������������� � ������� ��������� �������.*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int plus = 0, minus = 0, zero = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 11 - 5;
			cout << arr[i] << '\t';
			arr[i] > 0 ? ++plus : arr[i] < 0 ? ++minus : ++zero;
		}
		cout << "\n���������� ������������� ��������� �������: " << plus << endl;
		cout << "���������� ������������� ��������� �������: " << minus << endl;
		cout << "���������� ������� ��������� �������: " << zero << endl << endl;
		system("pause");
		system("cls");
	}

	/*5. ������� ������ �� 10 ����� ��������� �����. ��������� ����� ������ ��������� ������� � ������� �������������� ��������.*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int sumEven = 0, sumUneven = 0, j = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand();
			cout << arr[i] << '\t';
			arr[i] % 2 == 0 ? sumEven += arr[i] : sumUneven += arr[i];
			if (arr[i] % 2 != 0) {
				++j;
			}
		}
		cout << endl;
		cout << "\n����� ������ ��������� �������: " << sumEven << endl;
		cout << "������� �������������� �������� ��������� �������: " << double(sumUneven) / j << endl << endl;
	}
	system("pause");
	system("cls");

	/*6. ������� ���������� ������ (char-������) �� 10 ���������. ���������� ������� � ��� ����, ���� � ������ ����������.*/
	
	{
		const int SIZE = 10;
		char arr[SIZE]{};
		int numbers = 0;
		int letters = 0;
		int pointing = 0;
		cout << "������� 10 ����� ��������\n";
		for (int i = 0; i < SIZE; ++i) {
			cin >> arr[i];
			if (arr[i] >= 48 && arr[i] <= 57) {
				++numbers;
			}
			else if ((arr[i] >= 65 && arr[i] <= 90) || (arr[i] >= 97 && arr[i] <= 122) || arr[i] == 168 || arr[i] == 184 || (arr[i] >= 192 && arr[i] <= 255)) {
				++letters;
			}
			else if (arr[i] == 33 || arr[i] == 34 || (arr[i] >= 39 && arr[i] <= 41) || (arr[i] >= 44 && arr[i] <= 47) || arr[i] == 58 || arr[i] == 59 || arr[i] == 63 || arr[i] == 92 || arr[i] == 130 || arr[i] == 133 || (arr[i] >= 145 && arr[i] <= 148) || arr[i] == 150 || arr[i] == 151 || arr[i] == 183) {
				++pointing;
			}
		}
		cout << endl;
		cout << "�� �����:\n\t����: " << numbers << "\n\t����: " << letters << "\n\t������ ����������: " << pointing << endl << endl;
	}
	system("pause");
	system("cls");

	/*7. �������� ���������, ������� ���������� ������������ ������ ����� � ����� ������������, ������� ��� ��� ����� ����������� � �������.*/
	
	{
		const int SIZE = 100;
		int arr[SIZE]{};
		int num;
		int count = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 10 + 0;
		}
		do {
			system("cls");
			cout << "������� ����� �� 0 �� 9\n";
			cin >> num;
			if (num < 0 || num > 9) {
				cout << "������.��������� ����� �� ������ � �������� �� 0 �� 9\n";
				system("pause");
			}
		} while (num < 0 || num > 9);
		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] == num) {
				++count;
			}
		}
		cout << endl;
		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] == num) {
				SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			}
			cout << arr[i] << '\t';
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		}
		cout << endl << endl;
		cout << "��������� ����� ����������� � ������� " << count << " ���\n\n";
	}
	system("pause");
	system("cls");

	/*8. ������� ������ �� 10 ����� ��������� ����� � ��������� �� 0 �� 100. ���������� ���������� ��������� �������, ������� 3, 5, 7.*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int three = 0;
		int five = 0;
		int seven = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 101;
			if (arr[i] % 3 == 0) {
				++three;
			}
			if (arr[i] % 5 == 0) {
				++five;
			}
			if (arr[i] % 7 == 0) {
				++seven;
			}
			cout << arr[i] << '\t';
		}
		cout << endl << endl;
		cout << "� ������ ������� ���������� ���������, �������\n\t\t\t\t\t\t3: " << three << "\n\t\t\t\t\t\t5: " << five << "\n\t\t\t\t\t\t7: " << seven << endl;
	}
	system("pause");
	system("cls");

	/*9. ������� ������ �� 10 ����� ��������� ����� � ��������� �� 0 �� 100. ���������� ����� ��������� �������, ������� 3, �� �� ������� 5.*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		int sum = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 101;
			if (arr[i] % 3 == 0 && arr[i] % 5 != 0) {
				SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
				sum += arr[i];
			}
			cout << arr[i] << '\t';
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		}
		cout << endl;
		cout << "����� ��������� �������, ������� 3, �� �� ������� 5: " << sum << endl << endl;
	}
	system("pause");
	system("cls");

	/*10. ������� ������ �� 20 ��������� ����� � ��������� �� -10 �� 30. ���������� ����� ��������� �������, ������������� � ������� ����� ������� �������������� ��������.*/
	
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		int sum = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 41 - 10;
			cout << arr[i] << '\t';
		}
		cout << endl;
		for (int i = 0; i < SIZE; ++i) {
			if (arr[i] < 0) {
				for (i = i + 1; i < SIZE; ++i) {
					sum += arr[i];
				}
			}
		}
		cout << endl;
		cout << "����� ��������� �������, ������������� ����� ������� �������������� ��������:   " << sum << endl << endl;
	}
	system("pause");
	system("cls");

	/*11. ������� ������ �� 20 ��������� ����� � ��������� �� -30 �� 10. ���������� ����� ��������� �������, ������������� � ������� �� ������� �������������� ��������.*/
	
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		int sum = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 41 - 30;
			cout << arr[i] << '\t';
		}
		cout << endl;
		for (int i = 0; i < SIZE; ++i) {
			while (arr[i] < 0 && i < SIZE) {
				sum += arr[i];
				++i;
			}
			if (arr[i] >= 0) {
				break;
			}
		}
		cout << endl;
		cout << "����� ��������� �������, ������������� �� ������� �������������� ��������: " << sum << endl << endl;
	}
	system("pause");
	system("cls");

	/*12. ������� ������ �� 20 ��������� �����. ���������� ����������� � ������������ ������� ������� (������� �������� � ���������� �����).*/
	
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		int min, max, minNum, maxNum;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand();
			cout << arr[i] << '\t';
		}
		cout << endl;
		int i = 0;
		min = arr[0];
		max = arr[0];
		minNum = i;
		maxNum = i;
		for (; i < SIZE; ++i) {
			if (arr[i] < min) {
				min = arr[i];
				minNum = i;
			}
			if (arr[i] > max) {
				max = arr[i];
				maxNum = i;
			}
		}
		cout << endl;
		cout << "����������� ������� �������: " << min << " � ���������� ������� � ������� " << ++minNum << endl;
		cout << "������������ ������� �������: " << max << " � ���������� ������� � ������� " << ++maxNum << endl << endl;
	}
	system("pause");
	system("cls");

	/*13. ������ ������ �� 10 ������������ �����. ����������, ������� ��������� ������� �� ����� ������� �����.*/
	
	{
		const int SIZE = 10;
		double arr[SIZE]{};
		int count = 0;
		cout << "������� 10 ������������ �����\n";
		for (int i = 0; i < SIZE; ++i) {
			cin >> arr[i];
			if (int(arr[i]) == arr[i]) {
				++count;
			}
		}
		cout << endl << "���������� ���������, ������� �� ����� ������� �����: " << count << endl << endl;
	}
	system("pause");
	system("cls");

	/*14. ������� ������ �� 20 ��������� ����� � ��������� �� 1 �� 200. ���������� ���������� �������������, ������������� � ������������� ����� � ���������� ���������.*/
	
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		float one = 0;
		float two = 0;
		float three = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 200 + 1;
			cout << arr[i] << '\t';
			if (arr[i] / 10 == 0) {
				++one;
			}
			else if (arr[i] / 100 == 0) {
				++two;
			}
			else {
				++three;
			}
		}
		cout << endl;
		one = one / SIZE * 100;
		two = two / SIZE * 100;
		three = three / SIZE * 100;
		cout << "���������� ������������� �����: " << one << " %\n";
		cout << "���������� ������������� �����: " << two << " %\n";
		cout << "���������� ������������� �����: " << three << "%\n\n";
	}
	system("pause");
	system("cls");

	/*15. ������� ������ �� 20 ����� ��������� ����� � ��������� �� 2 �� 100. ���������� � ������� �� ����� ��� ������� ����� �� ����� �������.*/
	
	{
		const int SIZE = 20;
		int arr[SIZE]{};
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand() % 99 + 2;
			cout << arr[i] << '\t';
		}
		cout << "\n\n��� ������� ����� �� �������: \n\n";
		bool flag = true;
		for (int i = 0; i < SIZE; ++i) {
			for (int j = 2; j <= arr[i] / 2; ++j) {
				flag = arr[i] % j ? true : false;
				if (!flag) {
					break;
				}
			}
			if (flag || arr[i] == 2) {
				cout << arr[i] << '\t';
			}
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*16. ������� ������ �� 10 ����� ��������� �����. �������� ������� ���������� ��������� ������� �� ��������������� (0-� ������� �������� � 9-�, 1-� ������� � 8-� � �.�.).*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand();
			cout << arr[i] << '\t';
		}
		for (int i = 0; i < SIZE / 2; ++i) {
			arr[i] = arr[i] + arr[SIZE - 1 - i];
			arr[SIZE - 1 - i] = arr[i] - arr[SIZE - 1 - i];
			arr[i] = arr[i] - arr[SIZE - 1 - i];
		}
		cout << "\n\n��������������� ������� ���������� ��������� �������: \n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << '\t';
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*17. ������� ������ �� 10 ����� ��������� �����. �������� ������� �������� �������� ������� (0-� ������� �������� � 1-�, 2-� ������� � 3-� � �.�.).*/
	
	{
		const int SIZE = 10;
		int arr[SIZE]{};
		for (int i = 0; i < SIZE; ++i) {
			arr[i] = rand();
			cout << arr[i] << '\t';
		}
		for (int i = 0; i < SIZE; i += 2) {
			arr[i] = arr[i] + arr[i + 1];
			arr[i + 1] = arr[i] - arr[i + 1];
			arr[i] = arr[i] - arr[i + 1];
		}
		cout << "\n\n�������� �������� ������� ���������� �������: \n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			cout << arr[i] << '\t';
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*18. ������� ��� ������� A � B �� 5 ���������. ������� ������ ������ C �� 10 ���������, � ������� ���������� �������� �� ������� � ������� ������� ����������
	      (��������, �[0]=A[0], �[1]=B[0], C[2]=A[1], C[3]=B[1] � �.�.).*/
	
	{
		const int SIZE1 = 5;
		const int SIZE2 = 10;
		int A[SIZE1]{}, B[SIZE1]{}, C[SIZE2]{};
		cout << "������ A:\n\n";
		for (int i = 0; i < SIZE1; ++i) {
			A[i] = rand();
			cout << A[i] << '\t';
		}
		cout << "\n\n������ B:\n\n";
		for (int i = 0; i < SIZE1; ++i) {
			B[i] = rand();
			cout << B[i] << '\t';
		}
		cout << "\n\n������ C:\n\n";
		for (int i = 0; i < SIZE2; ++i) {
			C[i] = i % 2 == 0 ? A[i / 2] : B[i / 2];
			cout << C[i] << '\t';
		}
		cout << endl;
		system("pause");
		system("cls");
	}

	/*19. �������� ���������, ���������� ���� ������ � ������ ��������� �������: ������� ���������� ��������������� ��� ��������, ������� 0, ����� ��������������� ��� ��������, ������ 0, � �����
	      ��������������� ��� ��������, ������� 0.*/
	
	{
		const int SIZE = 10;
		int arr1[SIZE]{}, arr2[SIZE]{};
		int j = 0;
		for (int i = 0; i < SIZE; ++i) {
			arr1[i] = rand() % 21 - 10;
			cout << arr1[i] << '\t';
		}
		cout << "\n\n����� ������:\n\n\n";
		for (int i = 0; i < SIZE; ++i) {
			if (arr1[i] > 0) {
				arr2[j] = arr1[i];
				cout << arr2[j] << '\t';
				++j;
			}
		}
		for (int i = 0; i < SIZE; ++i) {
			if (arr1[i] == 0) {
				arr2[j] == arr1[i];
				cout << arr2[j] << '\t';
				++j;
			}
		}
		for (int i = 0; i < SIZE; ++i) {
			if (arr1[i] < 0) {
				arr2[j] = arr1[i];
				cout << arr2[j] << '\t';
				++j;
			}
		}
		cout << endl << endl;
	}
	system("pause");
	system("cls");

	/*20. �������� ���������, ���������� �������� ������ ������� �������� 10 ��������� � 2 ������� �������� 5 ��������� ���������� (��������, A[0]=C[0], B[0]=C[1], A[1]=C[2], B[1]=C[3] � �.�.).*/
	
	{
		const int SIZE1 = 10;
		const int SIZE2 = 5;
		int C[SIZE1]{}, A[SIZE2]{}, B[SIZE2]{};
		cout << "������ C:\n\n";
		for (int i = 0; i < SIZE1; ++i) {
			C[i] = rand();
			cout << C[i] << '\t';
			i % 2 == 0 ? A[i / 2] = C[i] : B[i / 2] = C[i];
		}
		cout << "\n������ A:\n\n";
		for (int i = 0; i < SIZE2; ++i) {
			cout << A[i] << '\t';
		}
		cout << "\n\n������ B:\n\n";
		for (int i = 0; i < SIZE2; ++i) {
			cout << B[i] << '\t';
		}
		cout << endl << endl << endl;
	}
	system("pause");
	system("cls");

	/*2. � ����� � ���������� �������� 15 ����� �����. ���������� ����� ����� ������� ����������� ������� �����. �� ����� ������� ��������� ������������ ����� ������� � ���������� ����� ���� �����,
	     � �������� ������� ��������.*/

	{
		const int SIZE = 15;
		int arr[SIZE]{};
		int sizeMax = 0;
		int numberMax;
		cout << "������� 15 �����\n";
		for (int i = 0; i < SIZE; ++i) {
			cin >> arr[i];
		}
		for (int i = 1; i < SIZE; ++i) {
			int size = 1;
			int number = i;
			for (; arr[i - 1] <= arr[i]; ++i) {
				++size;
			}
			if (sizeMax < size) {
				sizeMax = size;
				numberMax = number;
			}
		}
		cout << "\n����� ������ ������� ����������� �����:\n\t\t\t����� �������: " << sizeMax << " �����\n\t\t\t� ������� ����� �������: " << numberMax << endl << endl;
	}

	



}