#include"Header.h"
#include<iostream>
#include<iomanip>
#include<time.h>
#include<conio.h>

using std::cout;
using std::cin;
using std::endl;
using std::setw;

void Next() {
	system("pause");
	system("cls");
}


void main() {

	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	/*1. �������� �������, ��������� ��������� ������������ ������*/
	/*2. �������� �������, ��������� ��������� ������������ ������*/
	/*3. �������� �������, ����������� ��������� ������������ ������ ���������� �������*/
	/*4. �������� �������, ���������� ��������� ������������ ������*/
	
	{
		cout << "1. �������� �������, ��������� ��������� ������������ ������\n"
				"2. �������� �������, ��������� ��������� ������������ ������\n"
				"3. �������� �������, ����������� ��������� ������������ ������ ����������       �������\n"
				"4. �������� �������, ���������� ��������� ������������ ������\n\n\n";
		int row, col;
		cout << "������� ���������� �����\n";
		cin >> row;
		cout << "������� ���������� ��������\n";
		cin >> col;
		cout << endl;
		int **pp_arr = nullptr;
		if (AllocArrTwoDim(pp_arr, row, col)) {
			InitArrTwoDim(pp_arr, row, col);
			ShowArrTwoDim(pp_arr, row, col);
			FreeArrTwoDim(pp_arr, row);
			//FreeArrTwoDim_p(&pp_arr, row);
			//pp_arr = FreeArrTwoDim_(pp_arr, row);
		}
		else {
			cout << "������\n\n";
		}
	}
	Next();

	/*5. �������� �������, ����������� ������ (���������� ������) � ����� ����������� �������*/
	/*6. �������� �������, ����������� ������ (���������� ������) � ������ ����������� �������*/
	/*7. �������� �������, ����������� ������ � ��������� ������� ���������� �������*/
	/*8. �������� �������, ��������� ������ ����������� ������� �� ���������� ������*/
	/*9. �������� �������, ����������� ������� (���������� ������) � ����� ����������� �������*/
	/*10. �������� �������, ����������� ������� � ������ ���������� �������*/
	/*11. �������� �������, ����������� ������� � ��������� ������� ����������� �������*/
	/*12. �������� �������, ��������� ������� ����������� ������� �� ���������� ������*/
	
	{
		int row, col;
		cout << "������� ���������� �����\n";
		cin >> row;
		cout << "������� ���������� ��������\n";
		cin >> col;
		cout << endl;
		int **pp_arr = nullptr;
		if (AllocArrTwoDim_p(&pp_arr, row, col)) {
			InitArrTwoDim(pp_arr, row, col);
			ShowArrTwoDim(pp_arr, row, col);
			cout << "5. �������� �������, ����������� ������ (���������� ������) � ����� ����������� �������\n\n\n";
			int *arr = nullptr;
			AllocArrOneDim(arr, col);
			InitArrOneDim(arr, col);
			AddRowTwoDimEnd(pp_arr, row, col, arr);
			//AddRowTwoDimEnd_p(&pp_arr, row, col, arr);
			//pp_arr = AddRowTwoDimEnd_(pp_arr, row, col, arr);
			ShowArrTwoDim(pp_arr, row, col);
			delete[] arr;
			arr = nullptr;

			cout << "6. �������� �������, ����������� ������ (���������� ������) � ������ ������������������\n\n\n";
			AllocArrOneDim(arr, col);
			InitArrOneDim(arr, col);
			AddRowTwoDimBeg(pp_arr, row, col, arr);
			//AddRowTwoDimBeg_p(&pp_arr, row, col, arr);
			//pp_arr = AddRowTwoDimBeg_(pp_arr, row, col, arr);
			ShowArrTwoDim(pp_arr, row, col);
			delete[] arr;
			arr = nullptr;

			cout << "7. �������� �������, ����������� ������ � ��������� ������� ����������� �������\n\n\n";
			AllocArrOneDim(arr, col);
			InitArrOneDim(arr, col);
			int index;
			cout << "������� ������� ������\n";
			cin >> index;
			cout << endl;
			//AddRowTwoDimInd_p(&pp_arr, row, col, arr, index - 1) &&
			AddRowTwoDimInd(pp_arr, row, col, arr, index - 1) &&
				ShowArrTwoDim(pp_arr, row, col) || cout << "������\n\n";
			delete[] arr;
			arr = nullptr;

			cout << "8. �������� �������, ��������� ������ ����������� ������� �� ���������� ������\n\n\n";
			cout << "������� ����� ������\n";
			cin >> index;
			cout << endl;
			//DelRowTwoDimInd_p(&pp_arr, row, index - 1) &&
			DelRowTwoDimInd(pp_arr, row, index - 1) &&
				ShowArrTwoDim(pp_arr, row, col) || cout << "������\n\n";

			cout << "9. �������� �������, ����������� ������� (���������� ������) � ����� ����������� �������\n\n\n";
			AllocArrOneDim(arr, row);
			InitArrOneDim(arr, row);
			AddColTwoDimEnd(pp_arr, row, col, arr);
			//AddColTwoDimEnd_p(&pp_arr, row, col, arr);
			//pp_arr = AddColTwoDimEnd_(pp_arr, row, col, arr);
			ShowArrTwoDim(pp_arr, row, col);
			delete[] arr;
			arr = nullptr;

			cout << "10. �������� �������, ����������� ������� � ������ ���������� �������\n\n\n";
			AllocArrOneDim(arr, row);
			InitArrOneDim(arr, row);
			AddColTwoDimBeg(pp_arr, row, col, arr);
			//AddColTwoDimBeg_p(&pp_arr, row, col, arr);
			//pp_arr = AddColTwoDimBeg_(pp_arr, row, col, arr);
			ShowArrTwoDim(pp_arr, row, col);
			delete[] arr;
			arr = nullptr;

			cout << "11. �������� �������, ����������� ������� � ��������� ������� �����������       �������\n\n\n";
			AllocArrOneDim(arr, row);
			InitArrOneDim(arr, row);
			cout << "������� ������� �������\n";
			cin >> index;
			cout << endl;
			//AddColTwoDimInd_p(&pp_arr, row, col, arr, index - 1) &&
			AddColTwoDimInd(pp_arr, row, col, arr, index - 1) &&
				ShowArrTwoDim(pp_arr, row, col) || cout << "������\n\n";
			delete[] arr;
			arr = nullptr;

			cout << "12. �������� �������, ��������� ������� ����������� ������� �� ���������� ������\n\n\n";
			cout << "������� ����� �������\n";
			cin >> index;
			cout << endl;
			//DelColTwoDimInd_p(&pp_arr, row, col, index - 1) &&
			DelColTwoDimInd(pp_arr, row, col, index - 1) &&
				ShowArrTwoDim(pp_arr, row, col) || cout << "������\n\n";
			FreeArrTwoDim_p(&pp_arr, row);
		}
		else {
			cout << "������\n\n";
		}
	}
	Next();

	/*13. �������� ������� ��� ��������� ���������� ������� � ���������� ������*/

	{
		cout << "13. �������� ������� ��� ��������� ���������� ������� � ���������� ������\n\n\n";
		int row, col;
		cout << "������� ���������� �����\n";
		cin >> row;
		cout << "������� ���������� ��������\n";
		cin >> col;
		cout << endl;
		int **pp_arr = nullptr;
		if (AllocArrTwoDim(pp_arr, row, col)) {
			InitArrTwoDim(pp_arr, row, col);
			ShowArrTwoDim(pp_arr, row, col);
			cout << endl;
			/*int *p_arrD = nullptr;
			int size = ToOneDimArrS(pp_arrD, row, col, p_arrD);
			size && ShowArrOneDim(p_arrD, size) || cout << "������";*/
			int *p_arr = ToOneDimArr(pp_arr, row, col);
			p_arr && ShowArrOneDim(p_arr, row * col) || cout << "������\n";
			cout << endl << endl;
			FreeArrTwoDim(pp_arr, row);
			delete p_arr;
			p_arr = nullptr;
		}
		else {
			cout << "������\n\n";
		}
	}
	Next();
	
	/*14. �������� ������� ��� ��������� ����������� ������� � ��������� ������. ������ ����������� ��������������� ��������*/

	{
		cout << "14. �������� ������� ��� ��������� ����������� ������� � ��������� ������.      ������ ����������� ��������������� ��������\n\n\n";
		int size;
		cout << "������� ������ ����������� �������\n";
		cin >> size;
		cout << endl;
		int *p_arr = nullptr;
		int **pp_arr = nullptr;
		int row = 0, col = 0;
		if (AllocArrOneDim(p_arr, size)) {
			InitArrOneDim(p_arr, size);
			ShowArrOneDim(p_arr, size);
			pp_arr = ToTwoDimArr(p_arr, size, row, col);
		}
		if (pp_arr) {
			cout << endl;
			ShowArrTwoDim(pp_arr, row, col);
		}
		else if (size > 0) {
			cout << "\n\n������...������ ������������� ������ ���������� ������� � ���������\n\n��� ����� ����������?\n(��� �������� � ��������� ������ - ��������� ���� �������, ������ 0)\n\n Y / N\n";
			if (toupper(_getch()) == 'Y') {
				++size;
				pp_arr = ToTwoDimArr(p_arr, size, row, col);
				pp_arr[row - 1][col - 1] = 0;
				cout << endl << endl;
				pp_arr && ShowArrTwoDim(pp_arr, row, col) || cout << "������\n\n";
			}
		}
		else {
			cout << "������\n\n";
		}
		FreeArrTwoDim(pp_arr, row);
		delete p_arr;
		p_arr = nullptr;
	}
	Next();
}