#pragma once

template<typename T>
bool AllocArrOneDim(T *&p_arr, int size) {
	if (size <= 0) {
		return false;
	}
	p_arr = new T[size];
	return true;
}

template<typename T>
bool InitArrOneDim(T *p_arr, int size) {
	if (p_arr == nullptr || size <= 0) {
		return false;
	}
	for (int i = 0; i < size; ++i) {
		p_arr[i] = T(rand() % 1000 / 10.);
	}
	return true;
}

template<typename T>
bool ShowArrOneDim(T *p_arr, int size) {
	if (p_arr == nullptr || size <= 0) {
		return false;
	}
	for (int i = 0; i < size; ++i) {
		cout << p_arr[i] << ' ';
	}
	cout << endl;
	return true;
}

/*1. �������� �������, ��������� ��������� ������������ ������*/

// �������� ��������� �� ������
template<typename T>
bool AllocArrTwoDim(T **&pp_ref, int row, int col) {
	if (row <= 0 || col <= 0) {
		return false;
	}
	pp_ref = new T*[row];
	for (int i = 0; i < row; ++i) {
		pp_ref[i] = new T[col]{};
	}
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool AllocArrTwoDim_p(T ***ppp, int row, int col) {
	if (row <= 0 || col <= 0) {
		return false;
	}
	*ppp = new T*[row];
	for (int i = 0; i < row; ++i) {
		(*ppp)[i] = new T[col]{};
	}
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** AllocArrTwoDim_(T **pp, int row, int col) {
	if (row <= 0 || col <= 0) {
		return nullptr;
	}
	T **temp = new T*[row];
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col]{};
	}
	return temp;
}

/*2. �������� �������, ��������� ��������� ������������ ������*/

// �������� ��������� �� ������
template<typename T>
bool FreeArrTwoDim(T **&pp_ref, int row) {
	if (row <= 0 || pp_ref == nullptr) {
		return false;
	}
	for (int i = 0; i < row; ++i) {
		delete[] pp_ref[i];
		pp_ref[i] = nullptr;
	}
	delete[] pp_ref;
	pp_ref = nullptr;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool FreeArrTwoDim_p(T ***ppp, int row) {
	if (row <= 0 || *ppp == nullptr) {
		return false;
	}
	for (int i = 0; i < row; ++i) {
		delete[] (*ppp)[i];
		(*ppp)[i] = nullptr;
	}
	delete[] *ppp;
	*ppp = nullptr;
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** FreeArrTwoDim_(T **pp, int row) {
	if (row <= 0 || pp == nullptr) {
		return pp;
	}
	for (int i = 0; i < row; ++i) {
		delete[] pp[i];
		pp[i] = nullptr;
	}
	delete[] pp;
	pp = nullptr;
	return pp;
}

/*3. �������� �������, ����������� ��������� ������������ ������ ���������� �������*/

template<typename T>
bool InitArrTwoDim(T **pp, int row, int col) {
	if (row <= 0 || col <= 0 || pp == nullptr) {
		return false;
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			pp[i][j] = T(rand() % 1000 / 10.);
		}
	}
	return true;
}

/*4. �������� �������, ���������� ��������� ������������ ������*/

template<typename T>
bool ShowArrTwoDim(T **pp, int row, int col, short width = 4) {
	if (row <= 0 || col <= 0 || pp == nullptr) {
		return false;
	}
	cout << "  ";
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			cout << setw(width) << pp[i][j];
		}
		cout << endl << "  ";
	}
	cout << endl;
	return true;
}

/*5. �������� �������, ����������� ������ (���������� ������) � ����� ����������� �������*/

// �������� ��������� �� ������
template<typename T>
bool AddRowTwoDimEnd(T **& pp_ref, int &row, int col, T *arr) {
	if (pp_ref == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[row + 1];
	temp[row] = new T[col];
	for (int i = 0; i < col; ++i) {
		temp[row][i] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		temp[i] = pp_ref[i];
	}
	++row;
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool AddRowTwoDimEnd_p(T ***ppp, int &row, int col, T *arr) {
	if (*ppp == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[row + 1];
	temp[row] = new T[col];
	for (int i = 0; i < col; ++i) {
		temp[row][i] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		temp[i] = (*ppp)[i];
	}
	++row;
	delete[] *ppp;
	(*ppp) = temp;
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** AddRowTwoDimEnd_(T **pp, int &row, int col, T *arr) {
	if (pp == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return pp;
	}
	T **temp = new T*[row + 1];
	temp[row] = new T[col];
	for (int i = 0; i < col; ++i) {
		temp[row][i] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		temp[i] = pp[i];
	}
	++row;
	delete[] pp;
	return temp;
}

/*6. �������� �������, ����������� ������ (���������� ������) � ������ ����������� �������*/

// �������� ��������� �� ������
template<typename T>
bool AddRowTwoDimBeg(T **&pp_ref, int &row, int col, T *arr) {
	if (pp_ref == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[++row];
	*temp = new T[col];
	for (int i = 0; i < col; ++i) {
		(*temp)[i] = arr[i];
	}
	for (int i = 1; i < row; ++i) {
		temp[i] = pp_ref[i - 1];
	}
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool AddRowTwoDimBeg_p(T ***ppp, int &row, int col, T *arr) {
	if ((*ppp) == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[++row];
	*temp = new T[col];
	for (int i = 0; i < col; ++i) {
		(*temp)[i] = arr[i];
	}
	for (int i = 1; i < row; ++i) {
		temp[i] = (*ppp)[i - 1];
	}
	delete[] * ppp;
	(*ppp) = temp;
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** AddRowTwoDimBeg_(T **pp, int &row, int col, T *arr) {
	if (pp == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return pp;
	}
	T **temp = new T*[++row];
	*temp = new T[col];
	for (int i = 0; i < col; ++i) {
		(*temp)[i] = arr[i];
	}
	for (int i = 1; i < row; ++i) {
		temp[i] = pp[i - 1];
	}
	delete[] pp;
	pp = nullptr;
	return temp;
}

/*7. �������� �������, ����������� ������ � ��������� ������� ���������� �������*/

// �������� ��������� �� ������
template<typename T>
bool AddRowTwoDimInd(T **&pp_ref, int &row, int col, T *arr, int index) {
	if (pp_ref == nullptr || arr == nullptr || row <= 0 || col <= 0 || index < 0 || index > row) {
		return false;
	}
	T **temp = new T*[++row];
	temp[index] = new T[col];
	for (int i = 0; i < index; ++i) {
		temp[i] = pp_ref[i];
	}
	for (int i = 0; i < col; ++i) {
		temp[index][i] = arr[i];
	}
	for (int i = index + 1; i < row; ++i) {
		temp[i] = pp_ref[i - 1];
	}
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool AddRowTwoDimInd_p(T ***ppp, int &row, int col, T *arr, int index) {
	if ((*ppp) == nullptr || arr == nullptr || row <= 0 || col <= 0 || index < 0 || index > row) {
		return false;
	}
	T **temp = new T*[++row];
	temp[index] = new T[col];
	for (int i = 0; i < index; ++i) {
		temp[i] = (*ppp)[i];
	}
	for (int i = 0; i < col; ++i) {
		temp[index][i] = arr[i];
	}
	for (int i = index + 1; i < row; ++i) {
		temp[i] = (*ppp)[i - 1];
	}
	delete[] * ppp;
	(*ppp) = temp;
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** AddRowTwoDimInd_(T **pp, int &row, int col, T *arr, int index) {
	if (pp == nullptr || arr == nullptr || row <= 0 || col <= 0 || index < 0 || index > row) {
		return pp;
	}
	T **temp = new T*[++row];
	temp[index] = new T[col];
	for (int i = 0; i < index; ++i) {
		temp[i] = pp[i];
	}
	for (int i = 0; i < col; ++i) {
		temp[index][i] = arr[i];
	}
	for (int i = index + 1; i < row; ++i) {
		temp[i] = pp[i - 1];
	}
	delete[] pp;
	pp = nullptr;
	return temp;
}

/*8. �������� �������, ��������� ������ ����������� ������� �� ���������� ������*/

// �������� ��������� �� ������
template<typename T>
bool DelRowTwoDimInd(T **&pp_ref, int &row, int index) {
	if (pp_ref == nullptr || row <= 0 || index < 0 || index >= row) {
		return false;
	}
	T **temp = new T*[--row];
	// ����� ��� �����, ��� �������� if else ������ ��������
	for (int i = 0; i < index; ++i) {
		temp[i] = pp_ref[i];
	}
	for (int i = index; i < row; ++i) {
		temp[i] = pp_ref[i + 1];
	}
	delete[] pp_ref[index];
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool DelRowTwoDimInd_p(T ***ppp, int &row, int index) {
	if ((*ppp) == nullptr || row <= 0 || index < 0 || index >= row) {
		return false;
	}
	T **temp = new T*[--row];
	for (int i = 0; i < index; ++i) {
		temp[i] = (*ppp)[i];
	}
	for (int i = index; i < row; ++i) {
		temp[i] = (*ppp)[i + 1];
	}
	delete[] (*ppp)[index];
	delete[] * ppp;
	(*ppp) = temp;
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** DelRowTwoDimInd_(T **pp, int &row, int index) {
	if (pp == nullptr || row <= 0 || index < 0 || index >= row) {
		return pp;
	}
	T **temp = new T*[--row];
	for (int i = 0; i < index; ++i) {
		temp[i] = pp[i];
	}
	for (int i = index; i < row; ++i) {
		temp[i] = pp[i + 1];
	}
	delete[] pp[index];
	delete[] pp;
	return temp;
}

/*9. �������� �������, ����������� ������� (���������� ������) � ����� ����������� �������*/

// �������� ��������� �� ������
template<typename T>
bool AddColTwoDimEnd(T **&pp_ref, int row, int &col, T *arr) {
	if (pp_ref == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][col - 1] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col - 1; ++j) {
			temp[i][j] = pp_ref[i][j];
		}
		delete[] pp_ref[i];
	}
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool AddColTwoDimEnd_p(T ***ppp, int row, int &col, T *arr) {
	if ((*ppp) == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][col - 1] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col - 1; ++j) {
			temp[i][j] = (*ppp)[i][j];
		}
		delete[](*ppp)[i];
	}
	delete[](*ppp);
	(*ppp) = temp;
	return true;
}

// �������� ��������� �� �������
template<typename T>
T** AddColTwoDimEnd_(T **pp, int row, int &col, T *arr) {
	if (pp == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return pp;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][col - 1] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col - 1; ++j) {
			temp[i][j] = pp[i][j];
		}
		delete[] pp[i];
	}
	delete[] pp;
	return temp;
}

/*10. �������� �������, ����������� ������� � ������ ���������� �������*/

// �������� ��������� �� ������
template<typename T>
bool AddColTwoDimBeg(T **&pp_ref, int row, int &col, T *arr) {
	if (pp_ref == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][0] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 1; j < col; ++j) {
			temp[i][j] = pp_ref[i][j - 1];
		}
		delete[] pp_ref[i];
	}
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool AddColTwoDimBeg_p(T ***ppp, int row, int &col, T *arr) {
	if ((*ppp) == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return false;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][0] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 1; j < col; ++j) {
			temp[i][j] = (*ppp)[i][j - 1];
		}
		delete[](*ppp)[i];
	}
	delete[](*ppp);
	(*ppp) = temp;
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** AddColTwoDimBeg_(T **pp, int row, int &col, T *arr) {
	if (pp == nullptr || arr == nullptr || row <= 0 || col <= 0) {
		return pp;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][0] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 1; j < col; ++j) {
			temp[i][j] = pp[i][j - 1];
		}
		delete[] pp[i];
	}
	delete[] pp;
	return temp;
}

/*11. �������� �������, ����������� ������� � ��������� ������� ����������� �������*/

// �������� ��������� �� ������
template<typename T>
bool AddColTwoDimInd(T **&pp_ref, int row, int &col, T *arr, int index) {
	if (pp_ref == nullptr || arr == nullptr || row <= 0 || col <= 0 || index < 0 || index > col) {
		return false;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][index] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < index; ++j) {
			temp[i][j] = pp_ref[i][j];
		}
	}
	for (int i = 0; i < row; ++i) {
		for (int j = index + 1; j < col; ++j) {
			temp[i][j] = pp_ref[i][j - 1];
		}
		delete[] pp_ref[i];
	}
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool AddColTwoDimInd_p(T ***ppp, int row, int &col, T *arr, int index) {
	if ((*ppp) == nullptr || arr == nullptr || row <= 0 || col <= 0 || index < 0 || index > col) {
		return false;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][index] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < index; ++j) {
			temp[i][j] = (*ppp)[i][j];
		}
	}
	for (int i = 0; i < row; ++i) {
		for (int j = index + 1; j < col; ++j) {
			temp[i][j] = (*ppp)[i][j - 1];
		}
		delete[](*ppp)[i];
	}
	delete[](*ppp);
	(*ppp) = temp;
	return true;
}

// �������� ��������� �� ��������
template<typename T>
T** AddColTwoDimInd_(T **pp, int row, int &col, T *arr, int index) {
	if (pp == nullptr || arr == nullptr || row <= 0 || col <= 0 || index < 0 || index > col) {
		return pp;
	}
	T **temp = new T*[row];
	++col;
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
		temp[i][index] = arr[i];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < index; ++j) {
			temp[i][j] = pp[i][j];
		}
	}
	for (int i = 0; i < row; ++i) {
		for (int j = index + 1; j < col; ++j) {
			temp[i][j] = pp[i][j - 1];
		}
		delete[] pp[i];
	}
	delete[] pp;
	return temp;
}

/*12. �������� �������, ��������� ������� ����������� ������� �� ���������� ������*/

// ��������� ��������� �� ������
template<typename T>
bool DelColTwoDimInd(T **&pp_ref, int row, int &col, int index) {
	if (pp_ref == nullptr || row <= 0 || col <= 0 || index < 0 || index >= col) {
		return false;
	}
	--col;
	T **temp = new T*[row];
	for (int i = 0; i < row; ++i) {
		temp[i] = new T[col];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < index; ++j) {
			temp[i][j] = pp_ref[i][j];
		}
	}
	for (int i = 0; i < row; ++i) {
		for (int j = index; j < col; ++j) {
			temp[i][j] = pp_ref[i][j + 1];
		}
		delete[] pp_ref[i];
	}
	delete[] pp_ref;
	pp_ref = temp;
	return true;
}

// �������� ��������� �� ���������
template<typename T>
bool DelColTwoDimInd_p(T ***ppp, int row, int &col, int index) {
	if ((*ppp) == nullptr || row <= 0 || col <= 0 || index < 0 || index >= col) {
		return false;
	}
	--col;
	T **temp = nullptr;
	AllocArrTwoDim(temp, row, col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < index; ++j) {
			temp[i][j] = (*ppp)[i][j];
		}
	}
	for (int i = 0; i < row; ++i) {
		for (int j = index; j < col; ++j) {
			temp[i][j] = (*ppp)[i][j + 1];
		}
		delete[](*ppp)[i];
	}
	delete[](*ppp);
	(*ppp) = temp;
	return true;
}

// �������� ���������� �� ��������
template<typename T>
T** DelColTwoDimInd_(T **pp, int row, int &col, int index) {
	if (pp == nullptr || row <= 0 || col <= 0 || index < 0 || index >= col) {
		return false;
	}
	--col;
	T **temp = nullptr;
	AllocArrTwoDim(temp, row, col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < index; ++j) {
			temp[i][j] = pp[i][j];
		}
	}
	for (int i = 0; i < row; ++i) {
		for (int j = index; j < col; ++j) {
			temp[i][j] = pp[i][j + 1];
		}
		delete[] pp[i];
	}
	delete[] pp;
	return temp;
}

/*13. �������� ������� ��� ��������� ���������� ������� � ���������� ������*/

// ������� ���������� ��������� �� ���������� ������������ ������
template<typename T>
T* ToOneDimArr(T **pp, int row, int col) {
	if (pp == nullptr || row <= 0 || col <= 0) {
		return nullptr;
	}
	int size = row * col;
	T* arr = new T[size];
	size = 0;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			arr[size++] = pp[i][j];
		}
	}
	return arr;
}

// ������� ���������� ������ ����������� �������. ��������� ������������� ����� ����������� ������������� �������

template<typename T>
int ToOneDimArrS(T **pp, int row, int col, T *&p_ref) {
	if (pp == nullptr || row <= 0 || col <= 0) {
		return 0;
	}
	int size = row * col;
	p_ref = new T[size];
	size = 0;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			p_ref[size++] = pp[i][j];
		}
	}
	return size;
}

/*14. �������� ������� ��� ��������� ����������� ������� � ��������� ������. ������ ����������� ��������������� ��������*/

template<typename T>
T** ToTwoDimArr(T *arr, int size, int &row, int &col) {
	if (arr == nullptr || size <= 0) {
		return nullptr;
	}
	// �������� ������� - ������� �� �����
	int div = 2;
	if (size != 2 && size != 1) {
		while (size % div != 0) {
			++div;
		}
		if (div == size) {
			return nullptr;
		}
	}
	if (size == 1) {
		row = 1, col = 1;
	}
	else if (size == 2) {
		row = 2, col = 1;
	}
	// �������� ����������� ������������� � ���������� �������
	else if (sqrt(size) == int(sqrt(size))) {
		row = col = sqrt(size);
	}
	// ����� ����������� �������� ����� � �������� - �����, ����� ������� ���� ��� ����� ����� ����������
	else {
		col = div;
		row = size / col;
		while (row != col) {
			if (size % row == 0) {
				col = size / row;
			}
			--row;
		}
		row = size / col;
	} 
	T **temp = nullptr;
	AllocArrTwoDim(temp, row, col);
	size = 0;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			temp[i][j] = arr[size++];
		}
	}
	return temp;
}