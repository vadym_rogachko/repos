#include<iostream>
#include<iomanip>


using namespace std;


void main()
{
	cout << left << setfill('*') << setw(28) << "        " << endl;
	cout << right << setfill(' ') << setw(9) << "*|" << setw(20) << "**\n";
	cout << setw(9) << "* |" << setw(20) << "* *\n";
	cout << setw(9) << "*  |" << setw(20) << "*  *\n";
	cout << setw(9) << "*   |" << setw(20) << "*   *\n";
	cout << setw(9) << "*    |" << setw(20) << "*    *\n";
	cout << setw(9) << "*     |" << setw(20) << "*     *\n";
	cout << left << setfill('*') << setw(21) << " " << right << setfill(' ') << setw(8) << "*\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(9) << "*      |" << setw(20) << "*      *\n";
	cout << setw(29) << "*      |___________*______*\n";
	cout << setw(28) << "*     /            *     *\n";
	cout << setw(27) << "*    /             *    *\n";
	cout << setw(26) << "*   /              *   *\n";
	cout << setw(25) << "*  /               *  *\n";
	cout << setw(24) << "* /                * *\n";
	cout << setw(23) << "*/                 **\n";
	cout << left << setfill('*') << setw(21) << " " << endl;
	cout << endl;


	system("pause");



}