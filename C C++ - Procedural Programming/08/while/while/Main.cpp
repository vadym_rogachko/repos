﻿#include<iostream>
#include<Windows.h>

using namespace std;




void main() {
	setlocale(LC_ALL, "rus");

	/*1. Вывести на экран таблицу символов в формате «ASCII-код – символ».*/

	short ascii = 0;
	while (ascii < 256) {
		cout << ascii << " " << char(ascii) << endl;
		ascii++;
	}
	system("pause");
	system("cls");

	/*2. Вывести на экран горизонтальную линию из звездочек. Число звездочек указывает пользователь.*/

	short star;
	cout << "Введите количество звездочек\n";
	cin >> star;

	while (star > 0) {
		cout << '*';
		star--;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*3. Написать программу, выводящую на экран только четные целые числа из диапазона от 1 до 20.*/

	short even = 1;
	while (even <= 21) {
		if (even % 2 == 0) {
			cout << even << endl;
		}
		even++;
	}
	system("pause");
	system("cls");

	/*4. Вычислить сумму чисел в заданном диапазоне.*/

	int  sum = 0,min4 = 0, max4 = 0, i;
	// Числа в диапазоне не должны быть равны
	while (min4 == max4) {
		cout << "Задайте диапазон чисел\nЧисла от ";
		cin >> min4;
		cout << "до ";
		cin >> max4;
	}
	// Меняем местами числа, если пользователь их ввел не в том порядке или если числа отрицательные
	if (min4 > max4) {
		min4 = min4 + max4;
		max4 = min4 - max4;
		min4 = min4 - max4;
	}
	else {
		i = min4;

		while (i <= max4) {
			sum += i;
			i++;
		}
		cout << "Сумма чисел в диапазоне от " << min4 << " до " << max4 << " = " << sum << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*5. Вычислить сумму четных чисел и произведение нечетных в заданном диапазоне.*/

	int min5 = 0, max5 = 0, sumEven = 0, multiNotEven = 1, index5;
	while (min5 == max5) {
		cout << "Задайте минимальное и максимальное значение диапазона\n";
		cin >> min5 >> max5;
	}
	if (min5 > max5) {
		min5 = min5 + max5;
		max5 = min5 - max5;
		min5 = min5 - max5;
	}
	else {
		index5 = min5;

		while (index5 <= max5) {
			if (!(index5 % 2)) {
				sumEven += index5;
			}
			if (index5 % 2) {
				multiNotEven *= index5;
			}
			index5++;
		}
		cout << "Сумма четных чисел в диапазоне от " << min5 << " до " << max5 << " = " << sumEven << endl;
		cout << "Произведение нечетных чисел в диапазоне от " << min5 << " до " << max5 << " = " << multiNotEven << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*6. Вывести на экран все числа, кратные 3, в диапазоне от 0 до 100.*/

	short multipleThree = 0;
	while (multipleThree <= 100) {
		if (!(multipleThree % 3)) {
			cout << multipleThree << endl;
		}
		multipleThree++;
	}
	system("pause");
	system("cls");

	/*7. Написать программу, вычисляющую факториал введенного числа!*/
	
	short fact = -1,index7 = 1; // Факториал числа n - произведение всех натуральных чисел от 1 до n.Факториал нуля равен 1.Считаю только факториал не отрицательных чисел
	long double factorial = 1; // Для точного расчета использую тип данных long double
	while (fact < 0) {
		cout << "Введите число(не отрицательное)\n";
		cin >> fact;
	}
	while (index7 <= fact) {
		factorial *= index7;
		index7++;
	}
	cout.precision(15);
	cout << fact << "! = " << factorial << endl;
	cout.precision(6);
	system("pause");
	system("cls");

	/*8. Написать программу, вычисляющую степень числа.*/

	double num8, powerNumber = 1;
	int power, index8 = 0;
	cout << "Введите число и степень числа(целое число)\n";
	cin >> num8 >> power;

	if (!(num8 == 0 && power == 0)) { // Число не 0 и степень не 0
		// Положительная степень
		if (power > 0) {
			while (index8 < power) {
				powerNumber *= num8;
				index8++;
			}
		}
		// Отрицательная степень
		else if (power < 0) { 
			while (index8 > power) {
				powerNumber *= num8;
				index8--;
			}
			powerNumber = 1 / powerNumber;
		}
		// Степень 0
		cout << num8 << "^" << power << " = " << powerNumber << endl;
	}
	else {
		// 0 в степени 0
		cout << num8 << "^" << power << " = " << "Неопределенность\n"; 
	}
	system("pause");
	system("cls");

	/*9. С клавиатуры вводится целое число любой разрядности. Определить количество цифр в нем и их сумму.*/

	long long num9, index9; // Для расчета чисел как можно большей разрадности использую тип данных long long
	short numbers = 1, sumNumbers = 0;
	cout << "Введите целое число\n";
	cin >> num9;
	index9 = num9;

	if (num9 != 0) {
		while (index9 != 0) {
			sumNumbers += index9 % 10;
			numbers++;
			index9 /= 10;
		}
		if (num9 > 0) {
			cout << "Количество цифр в числе " << num9 << " = " << --numbers << "\nСумма цифр числа " << num9 << " = " << sumNumbers << endl;
		}
		else if (num9 < 0) {
			cout << "Количество цифр в числе " << num9 << " = " << --numbers << "\nСумма цифр числа " << num9 << " = " << -sumNumbers << endl;
		}
	}
	else {
		cout << "Количество цифр в числе " << num9 << " = " << numbers << "\nСумма цифр числа " << num9 << " = " << sumNumbers << endl;
	}
	system("pause");
	system("cls");

	/*10. С клавиатуры вводится целое число любой разрядности. Необходимо перевернуть это число, т.е. цифры должны располагаться в обратном порядке (например, вводим число 1234 – в результате будет 4321).*/

	long long num10, versNum, index10;
	short lastNum;
	cout << "Введите целое число\n";
	cin >> num10;
	versNum = num10 % 10;
	index10 = num10 / 10;

	while (index10 != 0) {
		lastNum = index10 % 10;
		versNum = versNum * 10 + lastNum;
		index10 /= 10;
	}
	cout << num10 << " в обратном порядке = " << versNum << endl;;
	system("pause");
	system("cls");

	/*11. С клавиатуры вводится целое число. Вывести на экран все числа, на которые введенное число делится без остатка.*/

	long long num11, index11 = 1, zero;
	cout << "Введите число\n";
	cin >> num11;
	cout << num11 << " делится без остатка на:\n";
	(num11 < 0) ? num11 = -num11 : num11 = +num11;

	while (index11 <= num11) {
		zero = num11 % index11;
		if (zero == 0) {
			cout << "\t\t\t\t " << index11 << ", " << -index11 << endl;
		}
		index11++;
	}
	if (num11 == 0) {
		cout << "\t\t\t\tInfinity\n";
	}
	cout << endl << endl << endl;
	system("pause");
	system("cls");

	/*12. С клавиатуры вводится целое число. Определить, является ли оно простым.*/

	// с flag и через тернарные операторы
	long long Num12, Index12 = 2, SimpleNum;
	bool flag12;
	cout << "Введите число\n";
	cin >> Num12;
	SimpleNum = Num12;
	(Num12 == 2) ? flag12 = true : flag12 = false; // 0 и 1 не являются простыми числами

	while (Index12 < Num12) {
		(SimpleNum % Index12) ? flag12 = true : flag12 = false;
		(SimpleNum % Index12) ? Index12++ : Index12 = Num12;
	}
	(flag12) ? cout << Num12 << " простое число\n" : cout << Num12 << " не является простым числом\n";
	system("pause");
	system("cls");

	// if else
	long long num12, index12 = 2, simpleNum;
	cout << "Введите число\n";
	cin >> num12;
	simpleNum = num12;

	if (num12 > 1) {
		while (index12 < num12) {
			if (simpleNum % index12 == 0) {
				cout << simpleNum << " не является простым числом\n";
				index12 = num12;
			}
			index12++;
		}
	}
	else {
		cout << simpleNum << " не является простым числом\n";
	}
	if (simpleNum == index12) {
		cout << simpleNum << " простое число\n";
	}
	system("pause");
	system("cls");

	/*13. Проверить, есть ли во введенном числе одинаковые цифры подряд.*/

	long long num13, index13;
	bool flag13 = false;
	cout << "Введите число\n";
	cin >> num13;
	index13 = num13;

	while (index13 != 0) {
		(index13 % 10 == index13 / 10 % 10) ? flag13 = true : flag13 = false;
		(index13 % 10 == index13 / 10 % 10) ? index13 = 0 : index13 /= 10;
	}
	(flag13) ? cout << "В " << num13 << " есть одинаковые цифры подряд\n" : cout << "В " << num13 << " нету одинаковых цифр подряд\n";
	system("pause");
	system("cls");

	/*14. Ввести с клавиатуры число. Проверить, что цифры этого числа расположены в неубывающем порядке (например, число 11299 соответствует заданию, а число 22044 нет).*/

	long long num14, index14;
	bool flag14;
	cout << "Введите число\n";
	cin >> num14;
	(num14 < 10 && num14 > -10) ? cout << "В числе " << num14 << " одна цифра.Цифры числа " : cout << "Цифры числа ";
	if (num14 <= -10) {
		num14 = -num14;
	}
	index14 = num14;

	while (index14 != 0) {
		(index14 % 10 >= index14 / 10 % 10) ? flag14 = true : flag14 = false;
		(index14 % 10 >= index14 / 10 % 10) ? index14 /= 10 : index14 = 0;
	}
	(num14 < 10 && num14 > -10) ? flag14 = false : flag14 = flag14;
	(flag14) ? cout << num14 << " расположены в неубывающем порядке\n" : cout << num14 << " не расположены в неубывающем порядке\n";
	system("pause");
	system("cls");

	/*15. Написать игру «Угадай число!».*/

	int num15, guessNum, diff;
	char Exit = 'y';
	while (Exit != 'n' && Exit != 'N') {
		cout << "\t\t\t    Игра \"Угадай число!\"\n";
		cout << "Первый игрок, загадайте число.\nВведите число: ";
		cin >> num15;
		system("cls");
		guessNum = num15 + 1;
		cout << "\t\t\t    Игра \"Угадай число!\"\n";
		cout << "Второй игрок отгадывает число.\n";
		HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);

		while (guessNum != num15) {
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			cout << "Введите число\n";
			cin >> guessNum;
			diff = num15 - guessNum;

			if ((diff > 0 && diff <= 2) || (diff < 0 && diff >= -2)) {
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_INTENSITY);
				cout << "Очень жарко\n";
			}
			else if ((diff > 2 && diff <= 10) || (diff < -2 && diff >= -10)) {
				SetConsoleTextAttribute(handle, FOREGROUND_RED);
				cout << "Жарко\n";
			}
			else if ((diff > 10 && diff <= 50) || (diff < -10 && diff >= -50)) {
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
				cout << "Горячо\n";
			}
			else if ((diff > 50 && diff <= 100) || (diff < -50 && diff >= -100)) {
				SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN);
				cout << "Тепло\n";
			}
			else if ((diff > 100 && diff <= 1000) || (diff < -100 && diff >= -1000)) {
				SetConsoleTextAttribute(handle, FOREGROUND_GREEN | FOREGROUND_BLUE);
				cout << "Холодно\n";
			}
			else if ((diff > 1000 && diff <= 10000) || (diff < -1000 && diff >= -10000)) {
				SetConsoleTextAttribute(handle, FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
				cout << "Очень холодно\n";
			}
			else if ((diff > 10000 && diff <= 100000) || (diff < -10000 && diff >= -100000)) {
				SetConsoleTextAttribute(handle, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
				cout << "Слишком холодно\n";
			}
			else if (diff == 0) {
				SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
				cout << "\aВЫ УГАДАЛИ ЧИСЛО!!!\n\t" << num15 << endl;
			}
			else {
				SetConsoleTextAttribute(handle, FOREGROUND_BLUE);
				cout << "Абсолютный ноль\n";
			}
		}
		cout << endl << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		cout << "Повторить игру? (Any key - да, N/n - нет)\n";
		cin >> Exit;
		system("cls");
	}
	
	

}