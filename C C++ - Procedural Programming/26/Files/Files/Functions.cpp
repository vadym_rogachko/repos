#include"Functions.h"

using std::cout;
using std::endl;

bool SplitFile(FILE *file, char *path, short count) {
	FILE *fileTemp = nullptr;
	errno_t error = fopen_s(&fileTemp, "pieces\\data.dat", "wb");
	if (error != 0) {
		perror("pieces\\data.dat");
		cout << endl;
		if (fileTemp != nullptr) {
			fclose(fileTemp);
			return false;
		}
		return false;
	}
	fwrite(&count, sizeof(short), 1, fileTemp);
	fclose(fileTemp);
	fseek(file, 0, SEEK_END);
	int size = ftell(file);
	fseek(file, 0, SEEK_SET);
	int sizePiece = size / count;
	int sizeLast = size - (sizePiece * (count - 1));
	int sizeBuffer = sizePiece >= sizeLast ? sizePiece : sizeLast;
	char *buffer = new char[sizeBuffer];
	char *num = new char[SIZE];
	for (int i = 0; i < count - 1; ++i) {
		strcpy_s(path, _MAX_PATH, "pieces\\piece");
		_itoa_s(i + 1, num, SIZE, 10);
		strcat_s(path, _MAX_PATH, num);
		strcat_s(path, _MAX_PATH, ".bin");
		error = fopen_s(&fileTemp, path, "wb");
		if (error != 0) {
			perror(path);
			cout << endl;
			fclose(fileTemp);
			return false;
		}
		fread_s(buffer, sizeBuffer, sizeof(char), sizePiece, file);
		fwrite(buffer, sizeof(char), sizePiece, fileTemp);
		fclose(fileTemp);
	}
	strcpy_s(path, _MAX_PATH, "pieces\\piece");
	_itoa_s(count, num, SIZE, 10);
	strcat_s(path, _MAX_PATH, num);
	strcat_s(path, _MAX_PATH, ".bin");
	error - fopen_s(&fileTemp, path, "wb");
	if (error != 0) {
		perror(path);
		cout << endl;
		fclose(fileTemp);
		return false;
	}
	fread_s(buffer, sizeBuffer, sizeof(char), sizeLast, file);
	fwrite(buffer, sizeof(char), sizeLast, fileTemp);
	cout << "\n���� ������� ������ �� �����\n";
	fclose(fileTemp);
	delete[] buffer;
	delete[] num;
	return true;
}

bool AsmFile(char *path) {
	FILE* file = nullptr;
	FILE *fileTemp = nullptr;
	errno_t error = fopen_s(&file, path, "wb");
	if (error != 0) {
		perror(path);
		cout << endl;
		if (file != nullptr) {
			fclose(file);
			return false;
		}
		return false;
	}
	short count;
	error = fopen_s(&fileTemp, "pieces\\data.dat", "rb");
	if (error != 0) {
		perror("pieces\\data.dat");
		cout << endl;
		if (fileTemp != nullptr) {
			_fcloseall();
			return false;
		}
		return false;
	}
	fread_s(&count, sizeof count, sizeof(short), 1, fileTemp);
	fclose(fileTemp);
	char temp;
	char *num = new char[SIZE];
	for (int i = 0; i < count; ++i) {
		strcpy_s(path, _MAX_PATH, "pieces\\piece");
		_itoa_s(i + 1, num, SIZE, 10);
		strcat_s(path, _MAX_PATH, num);
		strcat_s(path, _MAX_PATH, ".bin");
		error = fopen_s(&fileTemp, path, "rb");
		if (error != 0) {
			perror(path);
			cout << endl;
			if (fileTemp != nullptr) {
				_fcloseall();
				return false;
			}
			return false;
		}
		fread_s(&temp, sizeof temp, sizeof(char), 1, fileTemp);
		while (!feof(fileTemp)) {
			fwrite(&temp, sizeof(char), 1, file);
			fread_s(&temp, sizeof temp, sizeof(char), 1, fileTemp);
		}
		fclose(fileTemp);
	}
	cout << "\n����� ����� �� ������ ������� �������\n\n";
	fclose(file);
	delete[] num;
	return true;
}