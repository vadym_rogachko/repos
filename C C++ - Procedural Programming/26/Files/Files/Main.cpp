#include<iostream>
#include"Functions.h"
#include<time.h>

using std::cout;
using std::cin;
using std::endl;

#define next system("pause");\
			 system("cls")

#define cls  system("cls")

void main(int argc, char *argv[]) {

	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	/*2. �������� ���������, ������� �������� �� ������ ��������� ��������� ����. ������������� ����������� ������� ����� ����� ����� ��������� ��������� ������.*/

	{
		cout << "�������� ���������, ������� �������� �� ������ ���������         ��������� ����. ������������� ����������� ������� ����� �����    ����� ��������� ��������� ������\n\n";
		char path[_MAX_PATH];
		FILE *file = nullptr;
		char str[STR_S];
		if (argc == 2) {
			errno_t error = fopen_s(&file, argv[1], "rt");
			if (error != 0) {
				perror(argv[1]);
				cout << endl;
				if (file != nullptr) {
					fclose(file);
				}
			}
			else {
				while (true) {
					if (fgets(str, STR_S, file) != nullptr) {
						fputs(str, stdout);
					}
					else {
						fclose(file);
						cout << endl;
						break;
					}
				}
			}
		}
		else {
			cout << "������� ��� �����\n";
			cin >> path;
			cin.get();
			cls;
			errno_t error = fopen_s(&file, path, "rt");
			if (error != 0) {
				perror(path);
				cout << endl;
				if (file != nullptr) {
					fclose(file);
				}
			}
			else {
				while (true) {
					if (fgets(str, STR_S, file) != nullptr) {
						fputs(str, stdout);
					}
					else {
						fclose(file);
						cout << endl;
						break;
					}
				}
			}
		}
		next;
	}

	/*1. �������� ���������, ������� ��������� ��� ��������� ������������� ������ � ����. ������ � ���� �������������, ����� ������������ ������ ������ ������.
		 ��� ������ ��������� ������� ��������� ���� �� ��������� ������, � �����������.*/
	 
		 {
			 cout << "�������� ���������, ������� ��������� ��� ��������� ������������������� � ����. ������ � ���� �������������, ����� ������������   ������ ������ ������."
					 "��� ������ ��������� ������� ���������      ���� �� ��������� ������, � �����������\n\n";
			 char path[_MAX_PATH];
			 FILE *file = nullptr;
			 char str[STR_S]{};
			 cout << "������� ��� �����\n";
			 cin >> path;
			 cin.get();
			 errno_t error = fopen_s(&file, path, "at");
			 if (error != 0) {
				 perror(path);
				 cout << endl;
				 if (file != nullptr) {
					 fclose(file);
				 }
			 }
			 else {
				 cout << "������� ������:\n";
				 while (true) {
					 fgets(str, STR_S, stdin);
					 if (str[0] == '\n') break;
					 fputs(str, file);
				 }
				 fclose(file);
			 }
			 next;
		 }
	 
	 /*3. �������� ���������, ������� ������������ ���������� ����� � ���������� ���� � ��������� �����.*/

	 {
		 cout << "�������� ���������, ������� ������������ ���������� ����� �      ���������� ���� � ��������� �����\n\n";
		 char path[_MAX_PATH];
		 FILE *file = nullptr;
		 char str[STR_S]{};
		 short strings = 0;
		 short words = 0;
		 cout << "������� ��� �����\n";
		 cin >> path;
		 cin.get();
		 errno_t error = fopen_s(&file, path, "rt");
		 if (error != 0) {
			 perror(path);
			 cout << endl;
			 if (file != nullptr) {
				 fclose(file);
			 }
		 }
		 else {
			 while (true) {
				 if (fgets(str, STR_S, file) != nullptr) {
					 ++strings;
					 for (int i = 0; str[i]; ++i) {
						 str[i] != ' ' && (str[i + 1] == ' ' || str[i + 1] == '\n') && ++words;
					 }
				 }
				 else {
					 fclose(file);
					 break;
				 }
			 }
			 cout << "\n���������� ����� � ����� " << strings << endl;
			 cout << "���������� ����  � ����� " << words << endl << endl;
		 }
		 next;
	 }

	 /*4. �������� ���������, ������� ��������� ������ � �������� ����, � ����� ��������� ������ ������� �� ����� �����*/

	 {
		 cout << "�������� ���������, ������� ��������� ������ � �������� ����, �  ����� ��������� ������ ������� �� ����� �����\n\n";
		 FILE *file = nullptr;
		 int arr1[SIZE]{};
		 int arr2[SIZE]{};
		 for (int i = 0; i < SIZE; ++i) {
			 arr1[i] = rand() % 99 + 1;
			 cout << arr1[i] << ' ';
		 }
		 errno_t error = fopen_s(&file, "Array.bin", "w+b");
		 if (error != 0) {
			 perror("Array.bin");
			 cout << endl;
			 if (file != nullptr) {
				 fclose(file);
			 }
		 }
		 else {
			 int sizeElem = sizeof arr1[0];
			 int sizeArr = sizeof arr1 / sizeElem;
			 fwrite(&sizeElem, sizeof (int), 1, file);
			 fwrite(&sizeArr, sizeof (int), 1, file);
			 fwrite(arr1, sizeof (int), SIZE, file);
			 fseek(file, 0, SEEK_SET);
			 fread_s(&sizeElem, sizeof sizeElem, sizeof(int), 1, file);
			 fread_s(&sizeArr, sizeof sizeArr, sizeof(int), 1, file);
			 fread_s(arr2, sizeof arr2, sizeElem, sizeArr, file);
			 cout << "\n\n���� ������:\n\n";
			 for (int i = 0; i < sizeArr; ++i) {
				 cout << arr2[i] << ' ';
			 }
			 cout << endl << endl;
			 fclose(file);
		 }
		 next;
	 }

	 /*5. �������� ���������, ������� ������� ����� �� ������������ �����.*/
 
	 {
		 cout << "�������� ���������, ������� ������� ����� �� ������������ �����\n\n";
		 FILE *file = nullptr;
		 FILE *fileCopy = nullptr;
		 char path[_MAX_PATH];
		 cout << "������� ��� ������������ �����\n";
		 cin >> path;
		 errno_t error = fopen_s(&file, path, "rb");
		 if (error != 0) {
			 perror(path);
			 cout << endl;
			 if (file != nullptr) {
				 fclose(file);
			 }
		 }
		 else {
			 cout << "������� ��� ����� ������������ �����\n";
			 cin >> path;
			 error = fopen_s(&fileCopy, path, "wb");
			 if (error != 0) {
				 perror(path);
				 cout << endl;
				 if (file != nullptr) {
					 _fcloseall();
				 }
			 }
			 else {
				 char temp;
				 // �������� ����� ����� ����������� ����� �������, ����� � ����� �� ����������� ������ ����
				 fread_s(&temp, sizeof temp, sizeof(char), 1, file);
				 while (!feof(file)) {
					 fwrite(&temp, sizeof(char), 1, fileCopy);
					 fread_s(&temp, sizeof temp, sizeof(char), 1, file);
				 }
				 cout << "\n����������� ���� ������� ����������\n\n";
				 _fcloseall();
			 }
		 }
		 next;
	 }
	 
	 /*6. �������� ���������, ������� ������ ������� ���������� ���� ������.*/

	{
		cout << "�������� ���������, ������� ������ ������� ���������� ���� ������\n";
		FILE *file1 = nullptr;
		FILE *file2 = nullptr;
		char temp1, temp2;
		char path1[_MAX_PATH];
		char path2[_MAX_PATH];
		cout << "������� ��� ������� �����\n";
		cin >> path1;
		errno_t error = fopen_s(&file1, path1, "rb");
		if (error != 0) {
			perror(path1);
			cout << endl;
			if (file1 != nullptr) {
				fclose(file1);
			}
		}
		else {
			cout << "������� ��� ������� �����\n";
			cin >> path2;
			error = fopen_s(&file2, path2, "rb");
			if (error != 0) {
				perror(path2);
				cout << endl;
				fclose(file1);
				if (file2 != nullptr) {
					fclose(file2);
				}
			}
			else {
				int size1, size2;
				fseek(file1, 0, SEEK_END);
				size1 = ftell(file1);
				fseek(file1, 0, SEEK_SET);
				fseek(file2, 0, SEEK_END);
				size2 = ftell(file2);
				fseek(file2, 0, SEEK_SET);
				char *buffer1 = new char[size1];
				char *buffer2 = new char[size2];
				fread_s(buffer1, size1, sizeof(char), size1, file1);
				fread_s(buffer2, size2, sizeof(char), size2, file2);
				_fcloseall();

				error = fopen_s(&file1, path1, "wb");
				if (error != 0) {
					perror(path1);
					cout << endl;
					if (file1 != nullptr) {
						fclose(file1);
					}
				}
				else {
					error = fopen_s(&file2, path2, "wb");
					if (error != 0) {
						perror(path2);
						cout << endl;
						fclose(file1);
						if (file2 != nullptr) {
							fclose(file2);
						}
					}
					else {
						fwrite(buffer2, sizeof(char), size2, file1);
						fwrite(buffer1, sizeof(char), size1, file2);
						cout << "���������� ������ ���������� �������\n\n";
						delete[] buffer1;
						delete[] buffer2;
						_fcloseall();
					}
				}
			}
		}
		next;
	}

	/*7. �������� ���������, ������� ��������� ���� �� ������� ���������� �������, � ����� ������� ����� ��������� ����� �� ���� ������.*/

	{
		cout << "�������� ���������, ������� ��������� ���� �� ������� ���������� �������, � ����� ������� ����� ��������� ����� �� ���� ������\n\n";
		char pathSource[_MAX_PATH];
		char pathDest[_MAX_PATH];
		FILE *file = nullptr;
		short count;
		cout << "������� ��� �����\n";
		cin >> pathSource;
		errno_t error = fopen_s(&file, pathSource, "rb");
		if (error != 0) {
			perror(pathSource);
			cout << endl;
			if (file != nullptr) {
				fclose(file);
			}
		}
		else {
			cout << "������� �� ������� ������ ������� ����\n";
			cin >> count;
			cout << "������� ��� ����� �����\n";
			cin >> pathDest;
			SplitFile(file, pathSource, count) && AsmFile(pathDest);
			fclose(file);
		}
		next;
	}
}