#include<iostream>
#include"Functions.h"

bool SavePhonebook(Contact **phonebook, short countContacts, unsigned int id, bool isSort, bool isRev) {
	if (phonebook == nullptr || countContacts < 0) {
		return false;
	}
	FILE *file = nullptr;
	if (fopen_s(&file, "Phonebook.bin", "wb") != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return false;
	}
	fwrite(&countContacts, sizeof(short), 1, file);
	fwrite(&id, sizeof(unsigned int), 1, file);
	fwrite(&isSort, sizeof(bool), 1, file);
	fwrite(&isRev, sizeof(bool), 1, file);
	for (int i = 0; i < countContacts; ++i) {
		fwrite(phonebook[i], sizeof(Contact), 1, file);
	}
	fclose(file);
	return true;
}

bool LoadPhonebook(Contact **&phonebook, short &sizePhonebook, short &countContacts, unsigned int &id, bool &isSort, bool &isRev) {
	if (phonebook == nullptr || sizePhonebook <= 0) {
		return false;
	}
	FILE *file = nullptr;
	if (fopen_s(&file, "Phonebook.bin", "rb") != 0) {
		if (countContacts != 0) {
			MessageFileNotFound();
		}
		if (file != nullptr) {
			fclose(file);
		}
		return false;
	}
	short tempCount;
	fread_s(&tempCount, sizeof tempCount, sizeof(short), 1, file);
	if (tempCount < 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return false;
	}
	sizePhonebook = PHONEBOOK_S;
	countContacts = 0;
	fread_s(&id, sizeof id, sizeof(unsigned int), 1, file);
	fread_s(&isSort, sizeof isSort, sizeof(bool), 1, file);
	fread_s(&isRev, sizeof isRev, sizeof(bool), 1, file);
	Contact temp;
	for (int i = 0; i < tempCount; ++i) {
		fread_s(&temp, sizeof temp, sizeof(Contact), 1, file);
		AddContactFromFile(phonebook, sizePhonebook, countContacts, &temp);
	}
	fclose(file);
	return true;
}

bool AddContactFromFile(Contact **&phonebook, short &sizePhonebook, short &countContacts, const Contact *source) {
	if (phonebook == nullptr || sizePhonebook <= 0 || countContacts < 0 || source == nullptr) {
		return false;
	}
	if (countContacts == sizePhonebook - 1) {
		AddContacts(phonebook, sizePhonebook);
	}
	cls
	phonebook[countContacts]->group = source->group;
	phonebook[countContacts]->id = source->id;
	strcpy_s(phonebook[countContacts]->name, NAME_S, source->name);
	strcpy_s(phonebook[countContacts]->surname, SURNAME_S, source->surname);
	strcpy_s(phonebook[countContacts]->phoneNumMob, PHONE_S, source->phoneNumMob);
	strcpy_s(phonebook[countContacts]->phoneNumJob, PHONE_S, source->phoneNumJob);
	strcpy_s(phonebook[countContacts]->phoneNumHome, PHONE_S, source->phoneNumHome);
	strcpy_s(phonebook[countContacts]->email, EMAIL_S, source->email);
	strcpy_s(phonebook[countContacts]->comment, COMMENT_S, source->comment);
	strcpy_s(phonebook[countContacts]->age, AGE_S, source->age);
	++countContacts;
	return true;
}