#pragma once

typedef enum { NONE = '0', FAMILY, FRIEND, JOB, STUDY, OTHER } group;

enum { CANCEL = '0', ADD, SHOW_NUM, DEL, SAVE, LOAD, SHOW_ALL_CHOOSE, SORT, FIND, EDIT };

enum { NAME = '1' , SURNAME, PHONE_MOB, PHONE_JOB, PHONE_HOME, AGE, EMAIL, GROUP, COMMENT };

enum { LIST = '1', NUM, NAME_PHONE, ALL , GROUPS = '2' };

enum { ALPHA = '1', REV, RAND };

enum {

	NAME_S = 32,
	SURNAME_S = 32,
	PHONE_S = 32,
	EMAIL_S = 64,
	COMMENT_S = 256,
	PHONEBOOK_S = 16,
	CONTACT_NUM_S = 6,
	AGE_S = 3,
	GROUP_S = 16,

	ENTER = 13,
	BACKSPACE = 8,
	ESC = 27,
	MOVE = 224,
	UP = 72,
	DOWN = 80,
	MOVE_ = -32,
	
	YES = 'Y',
	NO = 'N',
	EXIT = 0,

	NOPE = -1,
	CONTACT_LIST = -1,

	};