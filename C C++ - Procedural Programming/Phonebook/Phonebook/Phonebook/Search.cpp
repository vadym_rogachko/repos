#include<iostream>
#include"Functions.h"

using std::cout;
using std::endl;

bool GetStr(char* strSearch) {
	cls
	MyGets_s(strSearch, SEARCH_STR, NAME_S + SURNAME_S);
	if (strSearch[0] == '\0') {
		return false;
	}
	_strlwr_s(strSearch, NAME_S + SURNAME_S); // ��� ���������������������
	return true;
}

// �) ����� �������� �� ����� ��� ������ �������� (��� ����� ������)
bool LinearSearch(Contact **phonebook, const char* strSearch, short &countContacts, pFunc func, bool isDel, bool nameSurnameSearch) {
	char *temp = new char[NAME_S + SURNAME_S];
	// ����� �� ������� � �����
	if (nameSurnameSearch) {
		for (int i = 0; i < countContacts; ++i) {
			strcpy_s(temp, NAME_S, phonebook[i]->name);
			temp[strlen(phonebook[i]->name)] = ' ';
			temp[strlen(phonebook[i]->name) + 1] = '\0';
			strcat_s(temp, SURNAME_S, phonebook[i]->surname);
			_strlwr_s(temp, NAME_S + SURNAME_S); // ��� ���������������������
			if (!strcmp(temp, strSearch)) {
				if (isDel) {
					cout << phonebook[i]->name << ' ' << phonebook[i]->surname << endl << phonebook[i]->phoneNumMob << endl << endl;
					if (IsDelete(DEL_MSG)) {
						func(phonebook, i + 1, countContacts);
						return true;
					}
					return true;
				}
				func(phonebook, i + 1, countContacts);
				return true;
			}
		}
	}
	// ����� �� ������ �������� (������ ����������)
	for (int i = 0; i < countContacts; ++i) {
		if (!strcmp(phonebook[i]->phoneNumMob, strSearch) || !strcmp(phonebook[i]->phoneNumJob, strSearch) || !strcmp(phonebook[i]->phoneNumHome, strSearch)) {
			cout << phonebook[i]->name << ' ' << phonebook[i]->surname << endl;
			if (isDel) {
				cout << phonebook[i]->name << ' ' << phonebook[i]->surname << endl << phonebook[i]->phoneNumMob << endl << endl;
				if (IsDelete(DEL_MSG)) {
					func(phonebook, i + 1, countContacts);
					return true;
				}
				return true;
			}
			func(phonebook, i + 1, countContacts);
			return true;
		}
	}
	short indexArr[PHONEBOOK_S];
	for (int i = 0; i < PHONEBOOK_S; ++i) {
		indexArr[i] = NOPE;
	}
	// ����� �� ������� (�� 16 ����������)
	for (int i = 0, j = 0; i < countContacts && j < PHONEBOOK_S; ++i) {
		strcpy_s(temp, SURNAME_S, phonebook[i]->surname);
		_strlwr_s(temp, SURNAME_S);
		if (!strcmp(temp, strSearch)) {
			indexArr[j++] = i;
		}
	}
	if (indexArr[0] != NOPE) {
		if (indexArr[1] == NOPE) {
			if (isDel) {
				cout << phonebook[indexArr[0]]->name << ' ' << phonebook[indexArr[0]]->surname << endl << phonebook[indexArr[0]]->phoneNumMob << endl << endl;
				if (IsDelete(DEL_MSG)) {
					func(phonebook, indexArr[0] + 1, countContacts);
					return true;
				}
				return true;
			}
			func(phonebook, indexArr[0] + 1, countContacts);
			return true;
		}
		ShowResOfSearch(phonebook, indexArr, PHONEBOOK_S, countContacts, "", func);
		return true;
	}
	// ����� �� ����� (�� 16 ����������)
	for (int i = 0, j = 0; i < countContacts && j < PHONEBOOK_S; ++i) {
		strcpy_s(temp, NAME_S, phonebook[i]->name);
		_strlwr_s(temp, NAME_S);
		if (!strcmp(temp, strSearch)) {
			indexArr[j++] = i;
		}
	}
	if (indexArr[0] != NOPE) {
		if (indexArr[1] == NOPE) {
			if (isDel) {
				cout << phonebook[indexArr[0]]->name << ' ' << phonebook[indexArr[0]]->surname << endl << phonebook[indexArr[0]]->phoneNumMob << endl << endl;
				if (IsDelete(DEL_MSG)) {
					func(phonebook, indexArr[0] + 1, countContacts);
					return true;
				}
				return true;
			}
			func(phonebook, indexArr[0] + 1, countContacts);
			return true;
		}
		ShowResOfSearch(phonebook, indexArr, PHONEBOOK_S, countContacts, "", func);
		return true;
	}
	if ((strSearch[0] < '0' || strSearch[0] > '9') && strSearch[0] != '+') {
		return false;
	}
	if (strlen(strSearch) < 3 || strlen(strSearch) == 3 && strSearch[0] == '+') {
		MessageAccur();
		return true;
	}
	// ����� �� ����� ������ �������� (�� 16 ����������) �� 3 ����
	for (int i = 0, j = 0; i < countContacts && j < PHONEBOOK_S; ++i) {
		if (strstr(phonebook[i]->phoneNumMob, strSearch) || strstr(phonebook[i]->phoneNumJob, strSearch) || strstr(phonebook[i]->phoneNumHome, strSearch)) {
			indexArr[j++] = i;
		}
	}
	if (indexArr[0] != NOPE) {
		if (indexArr[1] == NOPE) {
			if (isDel) {
				cout << phonebook[indexArr[0]]->name << ' ' << phonebook[indexArr[0]]->surname << endl << phonebook[indexArr[0]]->phoneNumMob << endl << endl;
				if (IsDelete(DEL_MSG)) {
					func(phonebook, indexArr[0] + 1, countContacts);
					return true;
				}
				return true;
			}
			func(phonebook, indexArr[0] + 1, countContacts);
			return true;
		}
		ShowResOfSearch(phonebook, indexArr, PHONEBOOK_S, countContacts, "", func);
		return true;
	}
	return false;
}

// �) ����� �������� �� ����� ��� ������ �������� (��� ����� ������)
bool BinarySearch(Contact **phonebook, const char* strSearch, short &countContacts, bool isRev, pFunc func, bool isDel) {
	char *temp = new char[NAME_S + SURNAME_S];
	short min = 0;
	short max = countContacts - 1;
	short mid;
	short cmp;
	// ������ ������������� - ����� ������ �� ����� � �������
	if (isRev) {
		while (min <= max) {
			mid = (min + max) / 2;
			strcpy_s(temp, NAME_S, phonebook[mid]->name);
			temp[strlen(phonebook[mid]->name)] = ' ';
			temp[strlen(phonebook[mid]->name) + 1] = '\0';
			strcat_s(temp, SURNAME_S, phonebook[mid]->surname);
			_strlwr_s(temp, NAME_S + SURNAME_S);
			cmp = strcmp(temp, strSearch);
			if (cmp < 0) {
				max = mid - 1;
			}
			else if (cmp > 0) {
				min = mid + 1;
			}
			else {
				if (isDel) {
					cout << phonebook[mid]->name << ' ' << phonebook[mid]->surname << endl << phonebook[mid]->phoneNumMob << endl << endl;
					if (IsDelete(DEL_MSG)) {
						func(phonebook, mid + 1, countContacts);
						return true;
					}
					return true;
				}
				func(phonebook, mid + 1, countContacts);
				return true;
			}
		}
	}
	else {
		while (min <= max) {
			mid = (min + max) / 2;
			strcpy_s(temp, NAME_S, phonebook[mid]->name);
			temp[strlen(phonebook[mid]->name)] = ' ';
			temp[strlen(phonebook[mid]->name) + 1] = '\0';
			strcat_s(temp, SURNAME_S, phonebook[mid]->surname);
			_strlwr_s(temp, NAME_S + SURNAME_S);
			cmp = strcmp(temp, strSearch);
			if (cmp > 0) {
				max = mid - 1;
			}
			else if (cmp < 0) {
				min = mid + 1;
			}
			else {
				if (isDel) {
					cout << phonebook[mid]->name << ' ' << phonebook[mid]->surname << endl << phonebook[mid]->phoneNumMob << endl << endl;
					if (IsDelete(DEL_MSG)) {
						func(phonebook, mid + 1, countContacts);
						return true;
					}
					return true;
				}
				func(phonebook, mid + 1, countContacts);
				return true;
			}
		}
	}
	// ������� � �������� �����, �.�. ������ ���� ���� �� ������ �������� ���� ����� ������� �� 16 ����������
	return LinearSearch(phonebook, strSearch, countContacts, func, isDel, false);
}