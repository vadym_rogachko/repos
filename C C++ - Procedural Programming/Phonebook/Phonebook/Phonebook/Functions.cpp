#include<iostream>
#include"Functions.h"
#include<conio.h>

using std::cout;
using std::cin;

bool IsExit() {
	char choice;
	MessageSure();
	do {
		choice = toupper(_getch());
	} while (choice != YES && choice != NO);
	return choice == YES;
}

bool IsEdit(const char* message, const char* record) {
	char action;
	cls
	cout << message << record << EDIT_MSG;
	do {
		action = toupper(_getch());
		action == ENTER && (action = YES);
		action == ESC && (action = NO);
	} while (action != YES && action != NO);
	cls
	return action == YES;
}

char GetGroup(char *message) {
	char choice;
	cls
	cout << GROUP_MSG  << message;
	do {
		choice = _getch();
		if (choice == ENTER) {
			choice = NONE;
			break;
		}
	} while (choice < NONE || choice > OTHER);
	cls
	return choice;
}

char EditGroup() {
	char choice;
	cout << GROUP_MSG;
	do {
		choice = _getch();
	} while (choice < FAMILY || choice > OTHER);
	cls
		return choice;
}

char GetChoice() {
	cout << NOT_FOUND REPEAT;
	char choice;
	do {
		choice = toupper(_getch());
		choice == ENTER && (choice = YES);
		choice == ESC && (choice = NO);
	} while (choice != YES && choice != NO);
	return choice;
}

bool SaveChoice(bool exit) {
	char choice;
	if (!exit) cls;
	cout << SAVE_MSG;
	do {
		choice = toupper(_getch());
	} while (choice != YES && choice != NO);
	return choice == YES;
}

bool LoadChoice() {
	char choice;
	cls
	cout << LOAD_MSG;
	do {
		choice = toupper(_getch());
	} while (choice != YES && choice != NO);
	return choice == YES;
}

bool IsDelete(char *message) {
	char choice;
	cout << message;
	do {
		choice = toupper(_getch());
	} while (choice != YES && choice != NO);
	return choice == YES;
}