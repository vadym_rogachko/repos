#pragma once

#define NAME_STR	   "���: "
#define SURNAME_STR    "�������: "
#define PHONE_MOB_STR  "��������� �������: "
#define PHONE_JOB_STR  "������� �������: "
#define PHONE_HOME_STR "�������� �������: "
#define AGE_STR        "�������: "
#define EMAIL_STR      "Email: "
#define COMMENT_STR    "�����������: "
#define GROUP_STR      "\n������: "
#define FAMILY_STR     "�����"
#define FRIEND_STR     "������"
#define JOB_STR        "������"
#define STUDY_STR      "�����"
#define OTHER_STR      "������"
#define SEARCH_STR     "�����: "

#define CONTACT_INFO   "1.���\n"\
			 	       "2.�������\n"\
				       "3.��������� �������\n"\
				       "4.������� �������\n"\
					   "5.�������� �������\n"\
					   "6.�������\n"\
					   "7.Email\n"\
					   "8.������\n"\
					   "9.�����������\n"\
			           "0.�����\n"

#define GROUP_MSG	   "������:\n" \
					   "1.�����\n" \
					   "2.������\n"\
					   "3.������\n"\
					   "4.�����\n" \
					   "5.������\n"

#define OMIT		   "0.�����\n"
#define CANCEL_MSG     "0.������\n"

#define MENU_MSG	   "1.���������� ��������\n"\
					   "2.�������� ��������\n"\
					   "3.�������� ��������\n"\
					   "4.���������� ���������\n"\
					   "5.�������� ���������\n"\
					   "6.�������� ���� ���������\n"\
					   "7.���������� ���������\n"\
					   "8.����� ��������\n"\
					   "9.�������������� ��������\n"\
					   "ESC - �����\n"

#define SWITCH_MSG	   "1.����� �� ������\n"\
					   "2.����� �� ����������� ������\n"\
					   "3.����� �� ����� ��� ������ ��������\n"\
					   "0.������\n"

#define SHOW_MSG       "1.�������� ������ ���������\n"\
					   "2.�������� �����\n"\
					   "0.������\n"

#define DELETE_MSG     "1.����� �� ������\n"\
					   "2.����� �� ����������� ������\n"\
					   "3.����� �� ����� ��� ������ ��������\n"\
					   "4.������� ��� ��������\n"\
					   "0.������\n"

#define SORT_MSG	   "1.����������� �-�\n"\
					   "2.����������� �-�\n"\
					   "3.��� ����������\n"

#define NUM_MSG		   "������� ���������� ����� �������� (0.�����)\n�������� ������ ��������� - ENTER\n���������� �����: "
#define EDIT_MSG	   "\n\n�������������? (Y/N) "
#define EXIT_MSG	   "\nESC - �����\n"
#define NOT_FOUND	   "\n������� �� ������\n\n"
#define REPEAT		   "��������� ? (Y / N) "
#define SORT_ALPHA	   "������������� �-�\n"
#define SORT_REV	   "������������� �-�\n"
#define SORT_NOPE	   "�� �������������\n"
#define SAVE_MSG	   "\n��������� ������ ���������? (Y/N) "
#define LOAD_MSG	   "\n��������� ������ ���������? (Y/N) "
#define DEL_ALL        "�� �������, ��� ������ ������� ��� ��������? (Y/N)\n"
#define DEL_MSG        "�������? (Y/N)\n"

#define cls  system("cls");

#define next system("pause");\
			 system("cls");