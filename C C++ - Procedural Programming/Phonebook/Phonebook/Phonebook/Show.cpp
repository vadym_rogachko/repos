#include<iostream>
#include"Functions.h"
#include<conio.h>
#include<iomanip>
#include<windows.h>

using std::cout;
using std::endl;
using std::setw;
using std::left;

// �) ����������� ����������� �������� (�� ����������� ������)
// ���� ������������� ����������� ������ � ����� �� ����� �� ������� ������ ��������� � ����������� �������� � ������� � ���������
bool ShowContactNum(Contact **phonebook, short &countContacts, pFunc func, bool isEdit, bool isDel) {
	if (phonebook == nullptr) {
		return false;
	}
	short num;
	char temp[CONTACT_NUM_S];
	do {
		cls
		MyGets_sNum(temp, NUM_MSG, CONTACT_NUM_S);
		if (temp[0] == '\0') {
			cls
			for (int i = 0; i < countContacts; ++i) {
				cout << i + 1 << '.' << phonebook[i]->name << ' ' << phonebook[i]->surname << endl << endl;
			}
			next
			num = CONTACT_LIST;
			continue;
		}
		num = atoi(temp);
		if (num >= 1 && num <= countContacts) {
			if (isDel) {
				cout << phonebook[num - 1]->name << ' ' << phonebook[num -1]->surname << endl << phonebook[num - 1]->phoneNumMob << endl << endl;
				if (IsDelete(DEL_MSG)) {
					func(phonebook, num, countContacts);
					return true;
				}
				return true;
			}
			func(phonebook, num, countContacts);
			if (isEdit) return true;
		}
		else if (num > countContacts) {
			MessageNotFound();
		}
	} while (num != EXIT && countContacts > 0);
	if (countContacts == 0) {
		MessageEmpty();
	}
	return true;
}

// �) ����������� ����� ������ ��������� � ������������ ������ �����������
short ShowAndChoose(Contact **phonebook, short &countContacts, pFunc func, bool isEdit) {
	if (phonebook == nullptr || countContacts <= 0) {
		return EXIT;
	}
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	short counter = 0;
	short action;
	short width;
	short maxLengthSurname = 0;
	short maxLengthName = 0;
	short countContactsCheck = countContacts;
	for (int i = 0; i < countContacts; ++i) {
		if (strlen(phonebook[i]->name) > maxLengthName) {
			maxLengthName = strlen(phonebook[i]->name);
		}
		if (strlen(phonebook[i]->surname) > maxLengthSurname) {
			maxLengthSurname = strlen(phonebook[i]->surname);
		}
	}
	width = maxLengthSurname + maxLengthName;
	do {
		cls
		for (int i = 0; i < countContacts; ++i) {
			if (i == counter) {
				SetConsoleTextAttribute(handle, 10);
				if (phonebook[i]->surname[0] == '\0') {
					cout << setw(width + 1) << left << phonebook[i]->name << ' ' << phonebook[i]->phoneNumMob << endl;
				}
				else {
					cout << phonebook[i]->name << ' ' << setw(width - strlen(phonebook[i]->name)) << left << phonebook[i]->surname << ' ' << phonebook[i]->phoneNumMob << endl;
				}
				SetConsoleTextAttribute(handle, 7);
				continue;
			}
			if (phonebook[i]->surname[0] == '\0') {
				cout << setw(width + 1) << left << phonebook[i]->name << ' ' << phonebook[i]->phoneNumMob << endl;
			}
			else {
				cout << phonebook[i]->name << ' ' << setw(width - strlen(phonebook[i]->name)) << left << phonebook[i]->surname << ' ' << phonebook[i]->phoneNumMob << endl;
			}
		}
		cout << EXIT_MSG;
		do {
			action = _getch();
			if (action == MOVE) {
				action = _getch();
				if (action != UP && action != DOWN) continue;
				action == UP ? --counter : ++counter;
			}
		} while (action != ESC && action != UP && action != DOWN && action != ENTER);
		if (action == ENTER) {
			func(phonebook, counter + 1, countContacts);
			if (isEdit) return EXIT;
			if (countContactsCheck != countContacts) {
				if (countContacts == 0) return EXIT;
				countContactsCheck = countContacts;
				counter = 0;
			}
			continue;
		}
		counter == countContacts && (counter = 0);
		counter < 0 && (counter = countContacts - 1);
	} while (action != ESC);
	return EXIT;
}

// ����� �� ����� ������ ��������� ���������� (����� 1 ����������) � ������� ����������� ��������� ����� � ����
void ShowResOfSearch(Contact **phonebook, short arr[], short size, short &countContacts, char *message, pFunc func) {
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	short counter = 0;
	short action;
	short width;
	short maxLengthSurname = 0;
	short maxLengthName = 0;
	short countContactsCheck = countContacts;
	while (arr[size - 1] == NOPE) {
		--size;
	}
	for (int i = 0; i < size; ++i) {
		if (strlen(phonebook[arr[i]]->name) > maxLengthName) {
			maxLengthName = strlen(phonebook[arr[i]]->name);
		}
		if (strlen(phonebook[arr[i]]->surname) > maxLengthSurname) {
			maxLengthSurname = strlen(phonebook[arr[i]]->surname);
		}
	}
	width = maxLengthSurname + maxLengthName;
	do {
		cls
			if (message[0] != '\0') {
				cout << message;
		}
		for (int i = 0; i < size; ++i) {
			if (i == counter) {
				SetConsoleTextAttribute(handle, 10);
				if (phonebook[arr[i]]->surname[0] == '\0') {
					cout << setw(width + 1) << left << phonebook[arr[i]]->name << ' ' << phonebook[arr[i]]->phoneNumMob << endl;
				}
				else {
					cout << phonebook[arr[i]]->name << ' ' << setw(width - strlen(phonebook[arr[i]]->name)) << left << phonebook[arr[i]]->surname << ' ' << phonebook[arr[i]]->phoneNumMob << endl;
				}
				SetConsoleTextAttribute(handle, 7);
				continue;
			}
			if (phonebook[arr[i]]->surname[0] == '\0') {
				cout << setw(width + 1) << left << phonebook[arr[i]]->name << ' ' << phonebook[arr[i]]->phoneNumMob << endl;
			}
			else {
				cout << phonebook[arr[i]]->name << ' ' << setw(width - strlen(phonebook[arr[i]]->name)) << left << phonebook[arr[i]]->surname << ' ' << phonebook[arr[i]]->phoneNumMob << endl;
			}
		}
		cout << EXIT_MSG;
		do {
			action = _getch();
			if (action == MOVE) {
				action = _getch();
				if (action != UP && action != DOWN) continue;
				action == UP ? --counter : ++counter;
			}
		} while (action != ESC && action != UP && action != DOWN && action != ENTER);
		if (action == ENTER) {
			func(phonebook, arr[counter] + 1, countContacts);
			if (countContactsCheck != countContacts) {
				return;
			}
			continue;
		}
		counter == size && (counter = 0);
		counter < 0 && (counter = size - 1);
	} while (action != ESC);
}

void ShowGroup(Contact **phonebook, char groupChoice, short countContacts) {
	if (phonebook == nullptr || countContacts <= 0 || groupChoice < FAMILY || groupChoice > OTHER) {
		return;
	}
	short size = 0;
	for (int i = 0; i < countContacts; ++i) {
		if (phonebook[i]->group == (group)groupChoice) {
			++size;
		}
	}
	if (size == 0) {
		MessageGroupIsEmpty();
		return;
	}
	short *temp = new short[size];
	for (int i = 0, j = 0; i < countContacts; ++i) {
		if (phonebook[i]->group == (group)groupChoice) {
			temp[j++] = i;
		}
	}
	char *message = new char[GROUP_S];
	switch (groupChoice) {
	case FAMILY:
		strcpy_s(message, GROUP_S, FAMILY_STR);
		strcat_s(message, GROUP_S, ":\n\n");
		break;
	case FRIEND:
		strcpy_s(message, GROUP_S, FRIEND_STR);
		strcat_s(message, GROUP_S, ":\n\n");
		break;
	case JOB:
		strcpy_s(message, GROUP_S, JOB_STR);
		strcat_s(message, GROUP_S, ":\n\n");
		break;
	case STUDY:
		strcpy_s(message, GROUP_S, STUDY_STR);
		strcat_s(message, GROUP_S, ":\n\n");
		break;
	case OTHER:
		strcpy_s(message, GROUP_S, OTHER_STR);
		strcat_s(message, GROUP_S, ":\n\n");
		break;
	}
	ShowResOfSearch(phonebook, temp, size, countContacts, message, ShowContact);
	delete[] message;
	delete[] temp;
}