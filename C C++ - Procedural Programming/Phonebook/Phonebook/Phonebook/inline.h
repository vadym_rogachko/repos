#pragma once
#include<iostream>
#include"define.h"
#include<windows.h>

using std::cout;

inline void MessageError() {
	cls
		cout << "\n�������� �����������\n\n";
	next
}

inline void MessageOK() {
	cout << "\n������� ��������\n\n";
	next
}

inline void MessageEmpty() {
	cout << "\n������ ��������� ������\n\n";
	next
}

inline void MessageNotFound() {
	cls
		cout << NOT_FOUND;
	system("pause");
}

inline void MessageDelete() {
	cls
		cout << "\n������� ������\n\n";
	next
}

inline void MessageSure() {
	cls
		cout << "�� �������, ��� ������ ����� �� ���������? (Y/N)\n";
}

inline void MessageInfo() {
	cls
		cout << CONTACT_INFO;
}

inline void MessageAccur() {
	cout << "������ ���������� �� ����������\n��� ������ �� ����� ������ ������� ����� 2 ����\n\n";
	next
}

inline void MessageSortAlpha() {
	cls
		cout << "\n" SORT_ALPHA "\n";
	next
}

inline void MessageSortRev() {
	cls
		cout << "\n" SORT_REV "\n";
	next
}

inline void MessageNoSort() {
	cls
		cout << "\n" SORT_NOPE "\n";
	next
}

inline void MessageSave() {
	cls
		cout << "\n������ ��������� ��������\n\n";
	next
}

inline void MessageLoad() {
	cls
		cout << "\n������ ��������� ��������\n\n";
	next
}

inline void MessageFileNotFound() {
	cls
		cout << "\n���� �� ������\n\n";
	next
}

inline void MessageGroupIsEmpty() {
	cls
		cout << "\n������ �����\n\n";
	next
}