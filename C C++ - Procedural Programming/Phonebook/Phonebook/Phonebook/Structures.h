#pragma once
#include"enums.h"

struct Contact {
	char name[NAME_S];
	char surname[SURNAME_S];
	char phoneNumMob[PHONE_S];
	char phoneNumJob[PHONE_S];
	char phoneNumHome[PHONE_S];
	char email[EMAIL_S];
	char comment[COMMENT_S];
	char age[AGE_S];
	group group;
	unsigned int id;
};