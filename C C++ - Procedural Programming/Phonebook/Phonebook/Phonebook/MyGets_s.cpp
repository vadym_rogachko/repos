#include<iostream>
#include"Functions.h"
#include<conio.h>

using std::cout;

// ����� ��� ���������� ������ ��������� ��������
void Pause() {
	char ch = 3;
	Sleep(1000);;
	while (ch--) {
		cout << '.';
	Sleep(1000);
	}
	// ������� ������ ���������� �� ������, ���� ������������ ���-�� ������ �� ����� �����
	while (_kbhit()) {
		_getch();
	}
}

void Translate(char *str, const char *translate, short size) {
	size /= 2;
	for (int i = 0; str[i] != '\0'; ++i) {
		for (int j = 0; j < size; ++j) {
			if (str[i] == translate[j]) {
				str[i] = translate[j + size];
				break;
			}
		}
	}
}

// gets_s � ���������� ��������� ���������� ��������� �������� (��� ������� ��� ������������)
bool MyGets_s(char *str, const char *message, short size) {
	if (str == nullptr || size <= 0) {
		return false;
	}
	cout << message;
	short i = 0;
	char translate[] = "qwertyuiop[]asdfghjkl;'zxcvbnm,.`QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>~/@#$^&|?��������������������������������`��������������������������������~.\"�;:?/,";
	for (; i < size - 1; ++i) {
		do {
			str[i] = _getch();
			if (str[i] == MOVE_) {
				_getch();
			}
		} while ((str[i] < ' ' || str[i] > '~') && str[i] != ENTER && str[i] != BACKSPACE);
		if (str[i] == ENTER) {
			break;
		}
		if (str[i] == BACKSPACE) {
			if (i == 0) {
				str[i] = '\0';
				--i;
				continue;
			}
			str[--i] = '\0';
			cls
			cout << message << str;
			--i;
			continue;
		}
		str[i + 1] = '\0';
		Translate(str, translate, sizeof translate);
		_putch(str[i]);
	}
	str[i] = '\0';
	if (i == size - 1) {
		Pause();
	}
	cls
	return true;
}

bool MyGets_sPhone(char *str, const char *message, short size) {
	if (str == nullptr || size <= 0) {
		return false;
	}
	cout << message;
	short i = 0;
	for (; i < size - 1; ++i) {
		do {
			str[i] = _getch();
			if (i == 0 && str[i] == '+') {
				break;
			}
			if (i > 0 && str[i - 1] != '+' && str[i - 1] != ' ' && str[i] == ' ') {
				break;
			}
		} while ((str[i] < '0' || str[i] > '9') && str[i] != ENTER && str[i] != BACKSPACE);
		if (str[i] == ENTER) {
			if (i == 1 && str[i - 1] == '+') {
				--i;
				continue;
			}
			break;
		}
		if (str[i] == BACKSPACE) {
			if (i == 0) {
				str[i] = '\0';
				--i;
				continue;
			}
			str[--i] = '\0';
			cls
			cout << message << str;
			--i;
			continue;
		}
		str[i + 1] = '\0';
		_putch(str[i]);
	}
	str[i] = '\0';
	if (i == size - 1) {
		Pause();
	}
	cls
	return true;
}

bool MyGets_sEmail(char *str, short size) {
	if (str == nullptr || size <= 0) {
		return false;
	}
	short i;
	char check = 0;
	while (check < 3) {
		cls
		cout << EMAIL_STR;
		check = i = 0;
		for (; i < size - 1; ++i) {
			do {
				str[i] = _getch();
				if (str[i] == MOVE_) {
					_getch();
				}
			} while ((str[i] < ' ' || str[i] > '~') && str[i] != ENTER && str[i] != BACKSPACE);
			if (str[i] == ENTER) {
				break;
			}
			if (str[i] == BACKSPACE) {
				if (i == 0) {
					str[i] = '\0';
					--i;
					continue;
				}
				str[--i] = '\0';
				cls
				cout << EMAIL_STR << str;
				--i;
				continue;
			}
			str[i + 1] = '\0';
			_putch(str[i]);
		}
		str[i] = '\0';
		if (i == 0) break;
		for (int j = 1; j < i; ++j) {
			if (str[j] == '@' && !check) {
				++check;
				continue;
			}
			if (check && str[j] == '.' && str[j - 1] != '@') {
				++check;
				continue;
			}
			if (check == 2 && str[j] != '.') {
				++check;
			}
		}
	}
	if (i == size - 1) {
		Pause();
	}
	cls
	return true;
}

bool MyGets_sNum(char *str, const char *message, short size) {
	if (str == nullptr || size <= 0) {
		return false;
	}
	cout << message;
	short i = 0;
	while (_kbhit()) {
		_getch();
	}
	for (; i < size - 1; ++i) {
		do {
			str[i] = _getch();
		} while ((str[i] < '0' || str[i] > '9') && str[i] != ENTER && str[i] != BACKSPACE);
		if (str[i] == ENTER) {
			break;
		}
		if (str[i] == BACKSPACE) {
			if (i == 0) {
				str[i] = '\0';
				--i;
				continue;
			}
			str[--i] = '\0';
			cls
				cout << message << str;
			--i;
			continue;
		}
		str[i + 1] = '\0';
		_putch(str[i]);
	}
	str[i] = '\0';
	if (i == size - 1) {
		Pause();
	}
	cls
		return true;
}