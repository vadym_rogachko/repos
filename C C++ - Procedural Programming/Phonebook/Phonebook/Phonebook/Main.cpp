#include<iostream>
#include"Functions.h"
#include<conio.h>

/*	
	����������� �����

����������� ���������� ������ ������ ��������� ���������� ��������. ��������� ������ ������������ ��������� ��������� � ������������� ����, ���������� ��������� ������:
�) ���������� �������� � ������
�) ����������� ����������� �������� (�� ����������� ������)
�) �������� �������� �� ������
�) ���������� ����� ������ ��������� � ����
�) �������� ������ ��������� �� �����
�) ����������� ����� ������ ��������� � ������������ ������ �����������
�) ���������� ��������� � ���������� �������
�) ����� �������� �� ����� ��� ������ �������� (��� ����� ������)
�) �������������� ������������� ��������

������ ���������� ������� ������ ��������� ��������� ����: ����� ��������, ������� � ���, �������. ��������� ���� � �� �������.

*/

using std::cout;
using std::endl;

void main() {

	setlocale(LC_ALL, "rus");

	Contact **phonebook = nullptr;
	short sizePhonebook = CreatePhonebook(phonebook);;
	char strSearch[NAME_S + SURNAME_S]{};
	short countContacts = 0;
	unsigned int counter = 0;
	bool exit = false;
	bool isSort = false;
	bool isRev = false;
	char choice;
	LoadPhonebook(phonebook, sizePhonebook, countContacts, counter, isSort, isRev);
	while (!exit) {
		switch (Menu()) {
		case ADD:
			if (countContacts == sizePhonebook - 1) {
				AddContacts(phonebook, sizePhonebook);
			}
			AddContact(phonebook, countContacts, counter);
			MessageOK();
			if (isSort) {
				InsertAlpha(phonebook, countContacts, isRev);
			}
			break;
		case SHOW_NUM:
			if (countContacts <= 0) {
				MessageError();
			}
			else {
				ShowContactNum(phonebook, countContacts, ShowContact);
			}
			break;
		case DEL:
			if (countContacts <= 0) {
				MessageError();
			}
			else {
				switch (DeleteMenu_()) {
				case LIST:
					ShowAndChoose(phonebook, countContacts, DeleteContact);
					break;
				case NUM:
					ShowContactNum(phonebook, countContacts, DeleteContact, false, true);
					break;
				case NAME_PHONE:
					do {
						if (GetStr(strSearch)) {
							if (isSort ? BinarySearch(phonebook, strSearch, countContacts, isRev, DeleteContact, true) : LinearSearch(phonebook, strSearch, countContacts, DeleteContact, true)) {
								break;
							}
						}
					} while (GetChoice() == YES);
					break;
				case ALL:
					cls
					if (IsDelete(DEL_ALL)) {
						DeletePhonebook(phonebook, sizePhonebook);
						sizePhonebook = CreatePhonebook(phonebook);
						countContacts = 0;
					}
					break;
				}
				if (countContacts == 0) {
					MessageEmpty();
				}
				while (countContacts < sizePhonebook / 2 && sizePhonebook > PHONEBOOK_S) {
					DeleteContacts(phonebook, sizePhonebook);
				}
			}
			break;
		case SAVE:
			if (SaveChoice() && SavePhonebook(phonebook, countContacts, counter, isSort, isRev)) {
				MessageSave();
			}
			break;
		case LOAD:
			if (LoadChoice() && LoadPhonebook(phonebook, sizePhonebook, countContacts, counter, isSort, isRev)) {
				MessageLoad();
			}
			break;
		case SHOW_ALL_CHOOSE:
			if (countContacts <= 0) {
				MessageError();
			}
			else {
				switch (ShowMenu()) {
				case LIST:
					ShowAndChoose(phonebook, countContacts, ShowContact);
					break;
				case GROUPS:
					do {
						choice = GetGroup(CANCEL_MSG);
						if (choice != NONE) {
							ShowGroup(phonebook, choice, countContacts);
						}
					} while (choice != NONE);
					break;
				}
			}
			break;
		case SORT:
			if (countContacts <= 0) {
				MessageError();
			}
			else if (countContacts == 1) {
				MessageNoSort();
			}
			else {
				switch (SortMenu(isSort, isRev)) {
				case ALPHA:
					if (isSort && isRev) {
						SwapContacts(phonebook, countContacts);
					}
					else if (!isSort) {
						QuickSortAlpha(phonebook, 0, countContacts - 1);
					}
					isSort = true, isRev = false;
					MessageSortAlpha();
					break;
				case REV:
					if (isSort && !isRev) {
						SwapContacts(phonebook, countContacts);
					}
					else if (!isSort) {
						QuickSortRev(phonebook, 0, countContacts - 1);
					}
					isSort = true, isRev = true;
					MessageSortRev();
					break;
				case RAND:
					if (isSort) {
						QuickSortNum(phonebook, 0, countContacts - 1);
					}
					isSort = false, isRev = false;
					MessageNoSort();
					break;
				}
			}
			break;
		case FIND:
			if (countContacts <= 0) {
				MessageError();
			}
			else {
				do {
					if (GetStr(strSearch)) {
						if (isSort ? BinarySearch(phonebook, strSearch, countContacts, isRev, ShowContact) : LinearSearch(phonebook, strSearch, countContacts, ShowContact)) {
							break;
						}
					}
				} while (GetChoice() == YES);
			}
			break;
		case EDIT:
			if (countContacts <= 0) {
				MessageError();
			}
			else {
				choice = SwitchMenu();
				switch (choice) {
				case LIST:
					ShowAndChoose(phonebook, countContacts, EditContact, true);
					break;
				case NUM:
					ShowContactNum(phonebook, countContacts, EditContact, true);
					break;
				case NAME_PHONE:
					do {
						if (GetStr(strSearch)) {
							if (isSort ? BinarySearch(phonebook, strSearch, countContacts, isRev, EditContact) : LinearSearch(phonebook, strSearch, countContacts, EditContact)) {
								break;
							}
						}
					} while (GetChoice() == YES);
					break;
				}
				if (choice != ESC && choice != CANCEL) {
					if (isSort) {
						isRev ? QuickSortRev(phonebook, 0, countContacts - 1) : QuickSortAlpha(phonebook, 0, countContacts - 1);
					}
				}
			}
			break;
		case ESC:
			exit = true;
			if (SaveChoice(true) && SavePhonebook(phonebook, countContacts, counter, isSort, isRev)) {
				MessageSave();
			}
		}
	}
	DeletePhonebook(phonebook, sizePhonebook);
	cls
		cout << endl;
}