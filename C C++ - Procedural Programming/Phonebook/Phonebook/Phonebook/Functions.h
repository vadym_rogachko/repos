#pragma once
#include"Structures.h"
#include"inline.h"
#include"define.h"

typedef bool (*pFunc) (Contact **phonebook, short contactNum, short &countContacts);

// ���������� ������� ���������� �����
bool AddContacts(Contact **&phonebook, short &sizePhonebook);
// ���������� ������� ���������� �����
bool DeleteContacts(Contact **&phonebook, short &sizePhonebook);
// ���������� ��������
bool AddContact(Contact **&phonebook, short &countContacts, unsigned int &counter);
// �������� ��������
bool DeleteContact(Contact **phonebook, short contactNum, short &countContacts);
bool EditContact(Contact **phonebook, short contactNum, short &countContacts);
bool ShowContact(Contact **phonebook, short contactNum, short &countContacts);

// ���� ������������� ����������� ������ � ����� �� ����� �� ������� ������ ��������� � ����������� �������� � ������� � ���������
bool ShowContactNum(Contact **phonebook, short &countContacts, pFunc func, bool isEdit = false, bool isDel = false);
// ����������� ����� ������ ��������� � ������������ ������ �����������
short ShowAndChoose(Contact **phonebook, short &countContacts, pFunc func, bool isEdit = false);
// ����������� ������ ��������� ��������� � ������������ ������ �����������
void ShowResOfSearch(Contact **phonebook, short arr[], short size, short &countContacts, char *message, pFunc func);
void ShowGroup(Contact **phonebook, char groupChoice, short countContacts);

/// �����
bool GetStr(char* strSearch);
bool LinearSearch(Contact **phonebook, const char* strSearch, short &countContacts, pFunc func, bool isDel = false, bool nameSurnameSearch = true);
bool BinarySearch(Contact **phonebook, const char* strSearch, short &countContacts, bool isRev, pFunc func, bool isDel = false);

/// ����������
void QuickSortAlpha(Contact **phonebook, short left, short right);
void QuickSortRev(Contact **phonebook, short left, short right);
void QuickSortNum(Contact **phonebook, short left, short right);
bool SwapContacts(Contact **phonebook, short countContacts);
bool InsertAlpha(Contact **phonebook, short countContacts, bool isRev);

/// �����
bool SavePhonebook(Contact **phonebook, short countContacts, unsigned int id, bool isSort, bool isRev);
bool LoadPhonebook(Contact **&phobebook, short &sizePhonebook, short &countContacts, unsigned int &id, bool &isSort, bool &isRev);
bool AddContactFromFile(Contact **&phonebook, short &sizePhonebook, short &countContacts, const Contact *source);

short CreatePhonebook(Contact **&phonebook);
void DeletePhonebook(Contact **&phonebook, short &sizePhoneBook);

bool MyGets_s(char *str, const char *message, short size);
bool MyGets_sPhone(char *str, const char* message, short size);
bool MyGets_sEmail(char *str, short size);
bool MyGets_sNum(char *str, const char *message, short size);
void Translate(char *str, const char *translate, short size);
void Pause();

bool IsExit();
char GetChoice();
char GetGroup(char *message);
bool SaveChoice(bool exit = false);
bool LoadChoice();
bool IsDelete(char *message);
bool IsEdit(const char* message, const char* record);

char Menu();
char SwitchMenu();
char ShowMenu();
char DeleteMenu_();
char SortMenu(bool isSort, bool isRev);