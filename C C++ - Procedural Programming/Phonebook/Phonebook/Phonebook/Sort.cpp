#include<iostream>
#include"Functions.h"

using std::swap;

void QuickSortAlpha(Contact **phonebook, short left, short right) {
	short l = left, r = right;
	char mid[NAME_S];
	strcpy_s(mid, NAME_S, phonebook[(l + r) / 2]->name);
	while (l <= r) {
		while (strcmp(phonebook[l]->name, mid) < 0) ++l;
		while (strcmp(phonebook[r]->name, mid) > 0) --r;
		if (l <= r) {
			swap(phonebook[l++], phonebook[r--]);
		}
	}
	if (l < right) QuickSortAlpha(phonebook, l, right);
	if (r > left) QuickSortAlpha(phonebook, left, r);
}

void QuickSortRev(Contact **phonebook, short left, short right) {
	short l = left, r = right;
	char mid[NAME_S];
	strcpy_s(mid, NAME_S, phonebook[(l + r) / 2]->name);
	while (l <= r) {
		while (strcmp(phonebook[l]->name, mid) > 0) ++l;
		while (strcmp(phonebook[r]->name, mid) < 0) --r;
		if (l <= r) {
			swap(phonebook[l++], phonebook[r--]);
		}
	}
	if (l < right) QuickSortRev(phonebook, l, right);
	if (r > left) QuickSortRev(phonebook, left, r);
}

// ���������� �� id �������� ��� �������� ��������� � ������� �� �������� � ���������� �����
void QuickSortNum(Contact **phonebook, short left, short right) {
	short l = left, r = right;
	short mid = phonebook[(l + r) / 2]->id;
	while (l <= r) {
		while (phonebook[l]->id < mid) ++l;
		while (phonebook[r]->id > mid) --r;
		if (l <= r) {
			swap(phonebook[l++], phonebook[r--]);
		}
	}
	if (l < right) QuickSortNum(phonebook, l, right);
	if (r > left) QuickSortNum(phonebook, left, r);
}

// ���� ���������� ����� ��� ������������,�� ����� ������������� � �������� �������
bool SwapContacts(Contact **phonebook, short countContacts) {
	if (phonebook == nullptr || countContacts <= 1) {
		return false;
	}
	for (int i = 0, j = countContacts - 1; i < j; ++i, --j) {
		swap(phonebook[i], phonebook[j]);
	}
	return true;
}


// ����� ���������� ������� �������� � ��������������� ���������� ����� � ������� - ���� ����� ������������� � �������� ����� �������
bool InsertAlpha(Contact **phonebook, short countContacts, bool isRev) {
	if (phonebook == nullptr || countContacts <= 1) {
		return false;
	}
	--countContacts;
	Contact *temp = phonebook[countContacts--];
	if (isRev) {
		for (int i = 0; i < countContacts; ++i) {
			if (strcmp(phonebook[i]->name, temp->name) >= 0 && strcmp(phonebook[i + 1]->name, temp->name) <= 0) {
				++i;
				for (int j = countContacts + 1; j > i; --j) {
					phonebook[j] = phonebook[j - 1];
				}
				phonebook[i] = temp;
				return true;
			}
		}
	}
	else {
		for (int i = 0; i < countContacts; ++i) {
			if (strcmp(phonebook[i]->name, temp->name) <= 0 && strcmp(phonebook[i + 1]->name, temp->name) >= 0) {
				++i;
				for (int j = countContacts + 1; j > i; --j) {
					phonebook[j] = phonebook[j - 1];
				}
				phonebook[i] = temp;
				return true;
			}
		}
	}
	return true;
}