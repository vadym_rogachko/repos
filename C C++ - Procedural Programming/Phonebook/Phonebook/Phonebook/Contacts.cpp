#include<iostream>
#include"Functions.h"
#include<conio.h>

using std::cout;
using std::endl;

// �) ���������� �������� � ������
bool AddContact(Contact **&phonebook, short &countContacts, unsigned int &counter) {
	if (phonebook == nullptr || countContacts < 0) {
		return false;
	}
	phonebook[countContacts]->id = counter++;
	cls
	do {
		MyGets_s(phonebook[countContacts]->name, NAME_STR, NAME_S);
	} while (strlen(phonebook[countContacts]->name) == 0);
	_strlwr_s(phonebook[countContacts]->name, NAME_S);
	phonebook[countContacts]->name[0] = toupper(phonebook[countContacts]->name[0]);
	MyGets_s(phonebook[countContacts]->surname, SURNAME_STR, SURNAME_S);
	_strlwr_s(phonebook[countContacts]->surname, SURNAME_S);
	phonebook[countContacts]->surname[0] = toupper(phonebook[countContacts]->surname[0]);
	do {
		MyGets_sPhone(phonebook[countContacts]->phoneNumMob, PHONE_MOB_STR, PHONE_S);
	} while (strlen(phonebook[countContacts]->phoneNumMob) == 0);
	MyGets_sPhone(phonebook[countContacts]->phoneNumJob, PHONE_JOB_STR, PHONE_S);
	MyGets_sPhone(phonebook[countContacts]->phoneNumHome, PHONE_HOME_STR, PHONE_S);
	MyGets_sNum(phonebook[countContacts]->age, AGE_STR, AGE_S);
	MyGets_sEmail(phonebook[countContacts]->email, EMAIL_S);
	phonebook[countContacts]->group = (group)GetGroup(OMIT);
	MyGets_s(phonebook[countContacts]->comment, COMMENT_STR, COMMENT_S);
	++countContacts;
	return true;
}

// �) ����������� ����������� �������� (�� ����������� ������)
bool ShowContact(Contact **phonebook, short contactNum, short &countContacts) {
	if (phonebook == nullptr || contactNum <= 0 || contactNum > countContacts || countContacts <= 0) {
		return false;
	}
	--contactNum;
	cls
	if (phonebook[contactNum]->group != NONE) {
		switch (phonebook[contactNum]->group) {
		case FAMILY:
			cout << FAMILY_STR "\n";
			break;
		case FRIEND:
			cout << FRIEND_STR "\n";
			break;
		case JOB:
			cout << JOB_STR "\n";
			break;
		case STUDY:
			cout << STUDY_STR "\n";
			break;
		case OTHER:
			cout << OTHER_STR "\n";
			break;
		}
	}
	cout << phonebook[contactNum]->name << ' ' << phonebook[contactNum]->surname;
	if (atoi(phonebook[contactNum]->age) > 0) {
		cout << ' ' << phonebook[contactNum]->age << ' ';
		if (atoi(phonebook[contactNum]->age) >= 5 && atoi(phonebook[contactNum]->age) <= 20 || atoi(phonebook[contactNum]->age) % 10 >= 5 && atoi(phonebook[contactNum]->age) % 10 <= 9 || atoi(phonebook[contactNum]->age) % 10 == 0) {
			cout << "���";
		}
		else if (atoi(phonebook[contactNum]->age) % 10 == 1) {
			cout << "���";
		}
		else {
			cout << "����";
		}
	}
	cout << endl;
	cout << PHONE_MOB_STR << phonebook[contactNum]->phoneNumMob << endl;
	if (phonebook[contactNum]->phoneNumJob[0]) {
		cout << PHONE_JOB_STR << phonebook[contactNum]->phoneNumJob << endl;
	}
	if (phonebook[contactNum]->phoneNumHome[0]) {
		cout << PHONE_HOME_STR << phonebook[contactNum]->phoneNumHome << endl;
	}
	if (phonebook[contactNum]->email[0]) {
		cout << EMAIL_STR << phonebook[contactNum]->email << endl;
	}
	if (phonebook[contactNum]->comment[0]) {
		cout << endl << phonebook[contactNum]->comment << endl;
	}
	cout << endl;
	_getch();
	return true;
}

// �) �������� �������� �� ������
bool DeleteContact(Contact **phonebook, short contactNum, short &countContacts) {
	if (phonebook == nullptr || contactNum <= 0 || contactNum > countContacts || countContacts <= 0) {
		return false;
	}
	--contactNum;
	delete phonebook[contactNum];
	--countContacts;
	for (int i = contactNum; i < countContacts; ++i) {
		phonebook[i] = phonebook[i + 1];
	}
	phonebook[countContacts] = new Contact;
	MessageDelete();
	return true;
}

// �) �������������� ������������� ��������
bool EditContact(Contact **phonebook, short contactNum, short &countContacts) {
	if (phonebook == nullptr || contactNum <= 0 || contactNum > countContacts || countContacts <= 0) {
		return false;
	}
	--contactNum;
	char choice, choiceGroup;
	bool editName = false;
	do {
		MessageInfo();
		do {
			choice = _getch();
		} while ((choice < CANCEL || choice > COMMENT) && choice != ESC);
		if (choice != CANCEL) {
			switch (choice) {
			case NAME:
				if (IsEdit("\n" NAME_STR, phonebook[contactNum]->name)) {
					do {
						MyGets_s(phonebook[contactNum]->name, NAME_STR, NAME_S);
					} while (strlen(phonebook[contactNum]->name) == 0);
					editName = true;
				}
				break;
			case SURNAME:
				if (IsEdit("\n" SURNAME_STR, phonebook[contactNum]->surname)) {
					MyGets_s(phonebook[contactNum]->surname, SURNAME_STR, SURNAME_S);
				}
				break;
			case PHONE_MOB:
				if (IsEdit("\n" PHONE_MOB_STR, phonebook[contactNum]->phoneNumMob)) {
					do {
						MyGets_sPhone(phonebook[contactNum]->phoneNumMob, PHONE_MOB_STR, PHONE_S);
					} while (strlen(phonebook[contactNum]->phoneNumMob) == 0);
				}
				break;
			case PHONE_JOB:
				if (IsEdit("\n" PHONE_JOB_STR, phonebook[contactNum]->phoneNumJob)) {
					MyGets_sPhone(phonebook[contactNum]->phoneNumJob, PHONE_JOB_STR, PHONE_S);
				}
				break;
			case PHONE_HOME:
				if (IsEdit("\n" PHONE_HOME_STR, phonebook[contactNum]->phoneNumHome)) {
					MyGets_sPhone(phonebook[contactNum]->phoneNumHome, PHONE_HOME_STR, PHONE_S);
				}
				break;
			case AGE:
				if (IsEdit("\n" AGE_STR, phonebook[contactNum]->age)) {
					MyGets_sNum(phonebook[countContacts]->age, AGE_STR, AGE_S);
				}
				break;
			case EMAIL:
				if (IsEdit("\n" AGE_STR, phonebook[contactNum]->email)) {
					MyGets_sEmail(phonebook[contactNum]->email, EMAIL_S);
				}
				break;
			case GROUP:
				switch (phonebook[contactNum]->group) {
				case FAMILY:
					if (IsEdit(GROUP_STR, FAMILY_STR))
						phonebook[contactNum]->group = (group)GetGroup(OMIT);
					break;
				case FRIEND:
					if (IsEdit(GROUP_STR, FRIEND_STR))
						phonebook[contactNum]->group = (group)GetGroup(OMIT);
					break;
				case JOB:
					if (IsEdit(GROUP_STR, JOB_STR))
						phonebook[contactNum]->group = (group)GetGroup(OMIT);
					break;
				case STUDY:
					if (IsEdit(GROUP_STR, STUDY_STR))
						phonebook[contactNum]->group = (group)GetGroup(OMIT);
					break;
				case OTHER:
					if (IsEdit(GROUP_STR, OTHER_STR))
						phonebook[contactNum]->group = (group)GetGroup(OMIT);
					break;
				default:
					phonebook[contactNum]->group = (group)GetGroup(OMIT);
				}
			break;
			case COMMENT:
				if (IsEdit("\n" COMMENT_STR, phonebook[contactNum]->comment)) {
					MyGets_s(phonebook[contactNum]->comment, COMMENT_STR, COMMENT_S);
				}
				break;
			}
		}
	} while (choice != CANCEL && choice != ESC);
	return editName;
}