#include<iostream>
#include"Functions.h"
#include<conio.h>

using std::cout;

char Menu() {
	char action;
	cls
	cout << MENU_MSG;
	do {
		action = _getch();
		if (action == ESC) {
			if (IsExit()) return ESC;
			else return CANCEL;
		}
	} while (action < ADD || action > EDIT);
	return action;
}

char SwitchMenu() {
	char choice;
	cls
	cout << SWITCH_MSG;
	do {
		choice = _getch();
	} while (choice != LIST && choice != NUM && choice != NAME_PHONE && choice != ESC && choice != CANCEL);
	return choice;
}

char ShowMenu() {
	char choice;
	cls
		cout << SHOW_MSG;
	do {
		choice = _getch();
	} while (choice != LIST && choice != GROUPS && choice != ESC && choice != CANCEL);
	return choice;
}

char DeleteMenu_() {
	char choice;
	cls
		cout << DELETE_MSG;
	do {
		choice = _getch();
	} while (choice != LIST && choice != NUM && choice != NAME_PHONE && choice != ALL && choice != ESC && choice != CANCEL);
	return choice;
}

char SortMenu(bool isSort, bool isRev) {
	char choice;
	cls
	cout << (isSort ? isRev ? SORT_REV : SORT_ALPHA : SORT_NOPE);
	cout << SORT_MSG;
	do {
		choice = _getch();
	} while (choice != ALPHA && choice != REV && choice != RAND && choice != ESC);
	return choice;
}