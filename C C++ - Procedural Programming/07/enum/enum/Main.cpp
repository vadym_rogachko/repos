#include<iostream>
#include<Windows.h>

using namespace std;

/*�������� ���������, ����������� ���������� ������������� ���� "��� ����� ����� �����������"*/


void main() {
	setlocale(LC_ALL, "rus");
	
	cout << "�������� ���� \"��� ����� ����� �����������\"\n\n";
	// ������������ ��������� ������ �� ������ 
	enum variant {A = 65, B, C, D, a = 97, b, c, d};

	// ���������� ��� ������
	char answer;
	// �������� ���������� �������
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	// ������� ���������� ������� - ����� �� ������
	short counter = 3;
	
	// ����������� �����
	int sum = 0;
	// �������
	int winning = 100;
	cout << "1 ������\n����� �� ���� ������� ����� ���������?\nA. ������\nB. �����\nC. ����\nD. ���-���������\n";
	cin >> answer;
	switch (answer) {
	case A:
	case B:
	case D:
	case a:
	case b:
	case d:
		--counter; // � ������ ��������� ������ ���������� ������� ����������� �� 1
		SetConsoleTextAttribute(handle, FOREGROUND_RED); // ��������� ������� ������������ �������
		// ������ ��� �������� ������
		cout << "\a����� ��������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case C:
	case c:
		SetConsoleTextAttribute(handle, FOREGROUND_GREEN); // ��������� ������� ���������� �������
		cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	default :
		// ��������� ������������ ������
		cout << "������.�� ������� ������� �����.����� �� ���� :(\n"; 
		exit(0);
	}

	winning = 200;
	cout << "2 ������\n����� ����� ����� ���� �������� ������� ��������?\nA. ���������\nB. �������\nC. ������\nD. ���������\n";
	cin >> answer;
	switch (answer) {
	case A: 
	case C:
	case D:
	case a:
	case c:
	case d:
		--counter;
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "\a����� ��������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case B:
	case b:
		SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
		cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	default:
		cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
		exit(0);
	}

	winning = 300;
	cout << "3 ������\n������ ������ ��������� �������� \"������� ������\"?\nA. ������ I\nB. ��������� I\nC. ����� I\nD. ������ I\n";
	cin >> answer;
	switch (answer) {
	case B:
	case C:
	case D:
	case b:
	case c:
	case d:
		--counter;
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "\a����� ��������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case A:
	case a:
		SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
		cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	default:
		cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
		exit(0);
	}

	winning = 500;
	cout << "4 ������\n��� � ������ ���������� ���������� ���������, ��������� ���������� ������� ����?\nA. �������� \nB. �����������\nC. �������\nD. ��������\n";
	cin >> answer;
	switch (answer) {
	case A:
	case B:
	case C:
	case a:
	case b:
	case c:
		--counter;
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "\a����� ��������! ";
		(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	case D:
	case d:
		SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
		cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		break;
	default:
		cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
		exit(0);
	}

	// ������ ������ ���������� ���������� ������ - 4 ������(����� 3 �������)
	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� " << sum << " ������!\n����� ����.\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 1000;
		cout << "5 ������\n� ����� ������ ������� ��������� ������ ������?\nA. ������\nB. �����\nC. ���������\nD. ����\n";
		cin >> answer;
		switch (answer) {
		case A:
		case B:
		case D:
		case a:
		case b:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n��� ����������� �����!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case C:
		case c:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n��� ����������� �����!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����.\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		sum = winning; // ������ ����������� �����
		winning = 2000;
		cout << "6 ������\n��� ���������� ��������� ����������� ������?\nA. �������\nB. ����\nC. �����\nD. ��������\n";
		cin >> answer;
		switch (answer) {
		case B:
		case C:
		case D:
		case b:
		case c:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case A:
		case a:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: "<< counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 4000;
		cout << "7 ������\n��� ������� ����� ������ ���������� ��������?\nA. ������� �����\nB. ���� �������\nC. ������ � �������\nD. ������ ����\n";
		cin >> answer;
		switch (answer) {
		case A:
		case B:
		case C:
		case a:
		case b:
		case c:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case D:
		case d:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 8000;
		cout << "8 ������\n�������� ������ �� ������������� ���������� ���������� ���������� ���������?\nA. �������\nB. �������\nC. ���������\nD. �������\n";
		cin >> answer;
		switch (answer) {
		case A:
		case C:
		case D:
		case a:
		case c:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case B:
		case b:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 16000;
		cout << "9 ������\n�� ��� ������ ���� ����� ����������� �������� ����� �� ����� �������?\nA. ������������� ���\nB. ���������� ���\nC. ��������\nD. ��������� ����\n";
		cin >> answer;
		switch (answer) {
		case A:
		case C:
		case D:
		case a:
		case c:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case B:
		case b:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 32000;
		cout << "10 ������\n��� ��������� �������� �������, �� ������� ���������� �������� ����� I � �����-����������?\nA. ���-������\nB. ����-������\nC. ����-������\nD. ������-������\n";
		cin >> answer;
		switch (answer) {
		case A:
		case C:
		case D:
		case a:
		case c:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n��� ����������� �����!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case B:
		case b:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������! ��� ������� ���������� " << winning << " ������!\n��� ����������� �����!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		sum = winning; // ������ ����������� �����
		winning = 64000;
		cout << "11 ������\n��� ���� ����� ���������� ������ ���� �������� ��� ������� � ��������� ��-������ �����?\nA. ��������������\nB. ���������������\nC. ������������\nD. ��������������\n";
		cin >> answer;
		switch (answer) {
		case A:
		case C:
		case D:
		case a:
		case c:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case B:
		case b:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������!��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 125000;
		cout << "12 ������\n������ ������ �� ���� � ������� ���������� ���������� ���� ��������?\nA. \"�������� ����\" (������)\nB. \"����� ����������\" (������)\nC. \"����� �����\" (����)\nD. \"����� �������� �������\" (�����)\n";
		cin >> answer;
		switch (answer) {
		case A:
		case B:
		case C:
		case a:
		case b:
		case c:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning <<  " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case D:
		case d:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������!��� ������� ���������� " << winning << " �������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 250000;
		cout << "13 ������\n����� �������� ����� ������ �������� - ������?\nA. ������\nB. �������\nC. ����\nD. �������\n";
		cin >> answer;
		switch (answer) {
		case A:
		case B:
		case D:
		case a:
		case b:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case C:
		case c:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������!��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 500000;
		cout << "14 ������\n��� � ����� 19 ���� ������ � ��������?\nA. ���\nB. �����\nC. ������ ������\nD. ��������\n";
		cin >> answer;
		switch (answer) {
		case A:
		case C:
		case D:
		case a:
		case c:
		case d:
			--counter;
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! ";
			(counter < 0) ? cout << "�� ��������� :(\n" : cout << "��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case B:
		case b:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "����� ������!��� ������� ���������� " << winning << " ������!\n����� �� ������: " << counter << endl << endl;
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
			exit(0);
		}
	}

	if (counter < 0) {
		SetConsoleTextAttribute(handle, FOREGROUND_RED);
		cout << "��� ������� ���������� " << sum << " ������!\n����� ����\n\n";
		SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		exit(0);
	}
	else {
		winning = 1000000;
		cout << "15 ������\n����� ���������� ������� ������ � ����� ����� ���������� �����?\nA. ������\nB. �������\nC. ��������\nD. ������\n";
		cin >> answer;
		switch (answer) {
		case A:
		case C:
		case D:
		case a:
		case c:
		case d:
			// �� �������� ���� ���� ���� ����� ����� �� ���������� �������, ���� 1 � ����� ����� �� ������ - ��� ������������ ������ �� ����������� ������.���������� ������ ��� ���������� ������
			SetConsoleTextAttribute(handle, FOREGROUND_RED);
			cout << "\a����� ��������! �� ��������� :(\n��� ������� ���������� " << sum << " ������!\n��� ��� ��������� ������\n"; // ������� ��� �� ����� - ����� ����
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		case B:
		case b:
			SetConsoleTextAttribute(handle, FOREGROUND_GREEN);
			cout << "\n\n\n\n����� ������!!!\n\n�����������!!!�����������!!!�����������!!!\n\n\t�� ����� �����������!!!\n\n��� ������� ���������� " << winning << " ������!\n\n\n\n\n\n";
			SetConsoleTextAttribute(handle, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			cout << "������.�� ������� ������� �����.����� �� ���� :(\n";
		}
	}

	




}