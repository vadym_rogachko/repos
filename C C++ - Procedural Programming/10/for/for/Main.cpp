#include<iostream>

using namespace std;




void main() {
	setlocale(LC_ALL, "rus");

	int size;

	/*1. �������� ���������, ������� ������� �� ����� ��������� �����������.*/
	
	cout << "������� ����� ������� ��������: ";
	cin >> size;
	cout << endl;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			// ������� ��������
			if (j == 0 || j == size - 1 || i == 0 || i == size - 1) {
				cout << '*';
				continue;
			}
			// ���������
			else if (j == i || j == size - 1 - i) {
				cout << '*';
				continue;
			}
				cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*2. �������� ���������, ������� ������� �� ����� �������������� �����������. ��� ���� ������������ ������ ������ ������������.*/
	
	cout << "������� ������ ������ ��������������� ������������: ";
	cin >> size;
	cout << endl;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j <= (size - 1) * 2; ++j) {
			//      ����� �������       ������ �������       ���������
			if ( j == size - 1 - i || j == size - 1 + i || i == size - 1) {
				cout << '*';
				continue;
			}
				cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*3. �������� ���������, ������� ������� �� ����� ����������� ����������� �������������� �����������. ��� ���� ������������ ������ ������ ������������.*/
	
	cout << "������� ������ ��������������� ������������: ";
	cin >> size;
	cout << endl;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size * 2 ; ++j) {
			//      ������            ����� �����         ������ �����
			if (j == size - 1 || (j >= size - 1 - i && j <= size - 1 + i)) {
				cout << '*';
				continue;
			}
			cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*4. �������� ���������, ������� ������� �� ����� ����. ��� ���� ������������ ������ ��������� �����.*/
	
	cout << "������� ��������� �����: ";
	cin >> size;
	if (size % 2 == 0) {
		size--;
	}
	cout << endl;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			//����
			//     ����� �������      ������ �������
			if (j == size / 2 - i || j == size / 2 + i) {
				cout << '*';
				continue;
			}
			//           ���              ����� �������            ������ �������
			else if (i >= size / 2 && (j == i - size / 2 || j == size - 1 - (i - size /2) ) ) {
				cout << '*';
				continue;
			}
			cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*5. �������� ���������, ������� ������� �� ����� ����������� ����������� ����. ��� ���� ������������ ������ ��������� �����.*/
	
	cout << "������� ��������� �����: ";
	cin >> size;
	if (size % 2 == 0) {
		size--;
	}
	cout << endl;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			//       ����� �������       ������ �������          ����
			if ((j >= size / 2 - i && j <= size / 2 + i) && i <= size / 2) {
				cout << '*';
				continue;
			}
			//           ���              ����� �������            ������ �������
			else if (i >= size / 2 && (j >= i - size / 2 && j <= size - 1 - (i - size / 2) ) ) {
				cout << '*';
				continue;
			}
				cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*6. �������� ���������, ������� ������� �� ����� ��������� �����������.*/
	
	cout << "������� ������ �������: ";
	cin >> size;
	if (size % 2 == 0) {
		size--;
	}
	cout << endl;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			// �����
			if (j == size / 2 || i == size / 2) {
				cout << '*';
				continue;
			}
			//           ����           �����         ������
			else if (i <= size / 2 && (j <= i || j >= size - 1 - i)) {
				cout << '*';
				continue;
			}
			//             ���                �����                      ������
			else if (i >= size / 2 && (j <= size - 1 - i || j >= size - 1 - (size - 1 - i))) {
				cout << '*';
				continue;
			}
			cout << ' ';
		}
		cout << endl;
	}
	cout << endl;
	system("pause");
	system("cls");

	/*7. ������� �� ����� ��������� �����, ��������� ������ � ������ � ����� 219.*/
	
	setlocale(LC_ALL, "C"); // ����������� ��������� �������� ASCII
	size = 8;
	for (int i = 0; i < size; ++i) {
		for (int j = 0; j < size; ++j) {
			if (i % 2 == 0 && j % 2 == 0) {
				cout << char(219);
				continue;
			}
			else if (i % 2 != 0 && j % 2 != 0) {
				cout << char(219);
				continue;
			}
			cout << ' ';
		}
		cout << endl;
	}
	cout << endl << endl << endl;
	setlocale(LC_ALL, "rus");
	system("pause");
	system("cls");

	/*8. ����������, �������� �� ��������� ����� ����� ����������� ����������� (��������, 1234321 � ���������, 12345 � �� ���������).*/

	// ������� ������ ������� ���������� ������� ���������� � �� ����� - ����� ������� ������ ������ ���� ��� ����������� ���������� ��������, ��� �������� ���������� ���������� ������ �����, �� ���� ����������
	// ���� � ����� � �.�. ���������� ������� ����� ���� �� ��������� �� ���������, �������� ����� �� ��������� �����, ��������� ������������,��� � ������ � ������������ �������(��. �����)

	long long num;
	{
		cout << "������� �����\n";
		cin >> num;

		// ���������� ��� ���������� �������� � �����
		long long count = 1;
		// ������� ���������� �������� � �����
		for (long long i = num; i > 9; i /= 10) {
			count *= 10;
		}

		bool flag;
		// ������� ��������� ����� � �����
		long long lastNum = num % 10;
		// ��������� ����������,������ ���������� ����� ��� ��� �������� ��������� �����
		long long number = num / 10;

		for (; count != 0;) {
			// ������� ������ �����, ������ �������� ������� ����� �� ������� ������, �.� count ����������� �� ������� ������ ��������
			int firstNum = num / count;
			// ����������� ������ ����� � ����������(��� �������� ��������) (������� ���� �������, ������ ���, ������ ���  � �.�.)
			flag = firstNum == lastNum ? true : false;
			// ������� ��������� ����� ���������� �������, � ������ �������
			lastNum = lastNum * 10 + number % 10;
			// � ������ ����������� ������ ������� ����� ������, �.�.  ��� ����, ��� ����� �� ���������, � ������ ��������� - ����������� ��� �������� ��������� ����� � ��������� �������,��������� ����
			firstNum == lastNum ? count /= 10 : count = 0;
		}
		flag ? cout << endl << num << " ���������\n" : cout << endl << num << " �� ���������\n";
	}
	system("pause");
	system("cls");

	// ���������� �������, �� ��� ����� ��� ���������� ����� (�� � �������� 9 � 10 ��� ������� ��� �� ��������� � ��� ����� �������� ������������ ������ �������� - ����� ���������� ��������)

	{
		cout << "������� �����\n";
		cin >> num;
		long long newNum = num % 10;
		long long i = num / 10;

		// �������������� ��������� �����
		for (; i != 0; i /= 10) {
			newNum = newNum * 10 + i % 10;
		}
		newNum == num ? cout << endl << num << " ���������\n" : cout << endl << num << " �� ���������\n";
	}
	system("pause");
	
	// ��������� ����� - �������� �����

	{
		long long min, max;
		do {
			system("cls");
			cout << "������� �������� �����\n";
			cin >> min >> max;
		} while (min == max);

		system("cls");

		if (min > max) {
			max = min + max;
			min = max - min;
			max = max - min;
		}

		long long count, number, firstNum, lastNum;
		bool flag;

		for (; min <= max; ++min) {
			 number = min;
			 lastNum = min % 10;
			 firstNum = 1;

			 count = 1;
			 for (long long i = min; i > 9; i /= 10) {
				 count *= 10;
			 }

			for (; count > 0; count /= 10) {
				firstNum = min / count;
				flag = firstNum == lastNum ? true : false;
				firstNum = lastNum ? number /= 10 : count = 0;
				lastNum = lastNum * 10 + number % 10;
			}
			if (flag) {
				cout << min << "\t";
			}
		}
	}
	cout << endl;
	system("pause");
	system("cls");

	/*9. ����������� ����������� ����� ����� ���������� ����� �� N �������� (��������, ��� ������ ����� 12345 ����� �� 3 ������� ��������� ����� 45123).*/
	
	// ������� ������� ���������� ������� ���������� � �� �����
	
	{
		short N;
		cout << "������� �����\n";
		cin >> num;
		cout << "������� ���������� �������� ��� ������ �����\n";
		cin >> N;
		long long count = 1, i, newNumber = num;
		short shift = N;

		for (i = num; i > 9; i /= 10) {
			count *= 10;
		}
		// � ����� ������ �������� �������� ������ ����� ����� � �����
		for (; N > 0; --N) {
			newNumber = newNumber % count * 10 + newNumber / count;
		}
		system("cls");
		cout << num << endl << "<";
		for (; shift > 0; --shift) {
			cout << "=";
		}
		cout << endl << newNumber << endl << endl;
		system("pause");
	}

	// ��������� ����� - �������� �����

	{
		long long min, max;
		do {
			system("cls");
			cout << "������� �������� �����\n";
			cin >> min >> max;
		} while (min == max);

		system("cls");

		if (min > max) {
			max = min + max;
			min = max - min;
			max = max - min;
		}

		short N;
		cout << "������� ���������� �������� ��� ������ �����\n";
		cin >> N;



		long long count, i, newNumber;
		short shift, n;
		system("cls");

		for (; min <= max; min++) {
			newNumber = min;
			count = 1;
			shift = n = N;

			for (i = min; i > 9; i /= 10) {
				count *= 10;
			}
			for (; n > 0; --n) {
				newNumber = newNumber % count * 10 + newNumber / count;
			}
			cout << min << " <";
			for (; shift > 0; --shift) {
				cout << "=";
			}
			cout << " " << newNumber << endl;
		}
		cout << endl;
		system("pause");
		system("cls");
	}

	/*10. ����������� ����������� ����� ������ ���������� ����� �� N �������� (��������, ��� ������ ����� 12345 ������ �� 3 ������� ��������� ����� 34512).*/
	// ������� ������� ���������� ������� ���������� � �� �����

	{
		short N;
		cout << "������� �����\n";
		cin >> num;
		cout << "������� ���������� �������� ��� ������ ������\n";
		cin >> N;
		long long count = 1, i, newNumber = num;
		short shift = N;

		for (i = num; i > 9; i /= 10) {
			count *= 10;
		}
		// � ����� ������ �������� �������� ��������� ����� � ������
		for (; N > 0; --N) {
			newNumber = newNumber % 10 * count + newNumber / 10;
		}
		system("cls");
		cout << num << endl;
		for (; shift > 0; --shift) {
			cout << "=";
		}
		cout << ">" << endl << newNumber << endl << endl;
	}
	system("pause");

	// ��������� �����

	{
		long long min, max;
		do {
			system("cls");
			cout << "������� �������� �����\n";
			cin >> min >> max;
		} while (min == max);

		system("cls");

		if (min > max) {
			max = min + max;
			min = max - min;
			max = max - min;
		}

		short N;
		cout << "������� ���������� �������� ��� ������ �����\n";
		cin >> N;
		long long count, i, newNumber;
		short shift;
		short n;
		system("cls");

		for (; min <= max; min++) {
			newNumber = min;
			count = 1;
			n = shift = N;


			for (i = min; i > 9; i /= 10) {
				count *= 10;
			}
			for (; n > 0; --n) {
				newNumber = newNumber % 10 * count + newNumber / 10;
			}
			cout << min << " ";
			for (; shift > 0; --shift) {
				cout << "=";
			}
			cout << "> " << newNumber << endl;
		}
		system("pause");
		system("cls");
	}


	

}