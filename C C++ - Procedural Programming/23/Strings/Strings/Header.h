#pragma once

void ReplaceSpaceToTab(char *str);
void ReplaceSpaceToTabCon(char *str);

void GetAmount(const char *str, short & letters, short & numbers, short & symbols);
void GetAmountConDis(const char *str, short & letters, short & numbers, short & symbols);

short GetAmountWords(const char *str);

void ShowRus(char ch, const char *translate, int size);

short CalcVowelLetters(char *buffer);
// ��� ������ �� ����� �������� �������� true
void TranslatePrint(char *buffer, bool print = false);

// size - ���������� �������� ��������
bool IsPalindrome(const unsigned char *str, short size);
// size - ���������� �������� ��������
bool IsPalindromeText(const unsigned char *str, short size);

int mystrlen(const char *str);
char* mystrcpy(char *str1, const char *str2);
char* mystrcat(char *str1, const char *str2);
char* mystrchr(char *str, char s);
char* mystrstr(char *str1, char *str2);
int mystrcmp(const char *str1, const char *str2);
int StringToNumber(char *str);
char* NumberToString(int number);
char* Uppercase(char *str1);
char* Lowercase(char *str1);
char* mystrrev(char *str);

char* DelSymbNum(char *str, short num);

char* DelSymbAll(char *str, char symb);

char* PasteSymb(char *str, char symb, short num);

char* GetBiggestWord(char *str);

int GetRes(char *str);
bool CheckInput(char *str);

double PolishCalc(char *&str);
bool CheckInput_(char *str);
void PlusMinus(char *str);
char* PolishNotation(char *str, short size);
void push(char *stack, int &index, char symb);
char pop(char *stack, int &index);
double Calc(char *str);
void push(double num);
double pop();