#include<iostream>

/*7. ������� ���������� �������*/

/*a. int mystrlen(const char * str); - ������� ���������� ����� ������*/

// ���������� ���������� �������� ��� ����� ����-�������
int mystrlen(const char *str) {
	short size = 0;
	while (str[size++] != '\0') {}
	return size - 1;
}

/*b. char * mystrcpy (char * str1, const char * str2); - ������� �������� ������ str2 � �����, ���������� ����� str1. ������� ���������� ��������� �� ������ ������ str1*/

char* mystrcpy(char *str1, const char *str2) {
	for (int i = 0; str2[i - 1] != '\0'; ++i) {
		str1[i] = str2[i];
	}
	return str1;
}

/*c. char * mystrcat (char * str1, const char * str2); - ������� ������������ ������ str2 � ������ str1. ������� ���������� ��������� �� ������ ������ str1*/

char* mystrcat(char *str1, const char *str2) {
	short size1 = mystrlen(str1);
	short size2 = mystrlen(str2);
	for (int i = size1, j = 0; i < size1 + size2; ++i, ++j) {
		str1[i] = str2[j];
	}
	str1[size1 + size2] = '\0';
	return str1;
}

/*d. char * mystrchr (char * str, char s); - ������� ������������ ����� ������� s � ������ str. ������� ���������� ��������� �� ������ ��������� ������� � ������, � ��������� ������ 0*/

char* mystrchr(char *str, char s) {
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == s) {
			return str + i;
		}
	}
	return nullptr;
}

/*e. char * mystrstr (char * str1, char * str2); - ������� ������������ ����� ��������� str2 � ������ str1. ������� ���������� ��������� �� ������ ��������� ��������� str2 � ������ str1, � ��������� ������ 0*/

char * mystrstr(char *str1, char *str2) {
	short size1 = mystrlen(str1);
	short size2 = mystrlen(str2);
	for (int i = 0; str1[i] != '\0' ; ++i) {
		if (str1[i] == str2[0]) {
			bool flag = true;
			for (int j = 1, k = i + 1; str2[j] != '\0'; ++j, ++k) {
				if (str1[k] != str2[j]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				return str1 + i;
			}
		}
	}
	return nullptr;
}

/*f. int mystrcmp (const char * str1, const char * str2); - ������� ���������� ��� ������, � , ���� ������ ����� ���������� 0, ���� ������ ������ ������ ������, �� ���������� 1, ����� -1*/

int mystrcmp(const char *str1, const char *str2) {
	int i = 0;
	for (; str1[i] != '\0' && str2[i] != '\0'; ++i) {
		if (str1[i] > str2[i]) {
			return 1;
		}
		else if (str1[i] < str2[i]) {
			return -1;
		}
	}
	if (str1[i] == str2[i]) {
		return 0;
	}
	if (str1[i] == '\0') {
		return -1;
	}
	return 1;
}

/*g. int StringToNumber( char * str); - ������� ������������ ������ � ����� � ���������� ��� �����*/

int StringToNumber(char *str) {
	int num = 0;
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] >= 48 && str[i] <= 57) {
			num = num * 10 + str[i] - 48;
		}
	}
	return num;
}

/*h. char * NumberToString ( int number); - ������� ������������ ����� � ������ � ���������� ��������� �� ��� ������*/

char* NumberToString(int number) {
	int num = number;
	short size = 0;
	if (num < 0) {
		++size;
	}
	while (num) {
		num /= 10;
		++size;
	}
	char *str = nullptr;
	if (number == 0) {
		str = new char[2]{ '0', '\0' };
	}
	else {
		str = new char[size + 1]{};
		str[size] = '\0';
	}
	if (number < 0) {
		str[0] = '-';
		number = -number;
	}
	for (int i = size - 1; number; --i) {
		str[i] = number % 10 + 48;
		number /= 10;
	}
	return str;
}

/*i. char * Uppercase (char * str1); - ������� ����������� ������ � ������� �������*/

char* Uppercase(char *str1) {
	for (int i = 0; str1[i] != '\0'; ++i) {
		if (str1[i] >= 97 && str1[i] <= 122 || str1[i] >= 224 && str1[i] <= 255) {
			str1[i] -= 32;
		}
	}
	return str1;
}

/*j. char * Lowercase (char * str1); - ������� ����������� ������ � ������ �������*/

char* Lowercase(char *str1) {
	for (int i = 0; str1[i] != '\0'; ++i) {
		if (str1[i] >= 65 && str1[i] <= 90 || str1[i] >= 192 && str1[i] <= 223) {
			str1[i] += 32;
		}
	}
	return str1;
}

/*k. char * mystrrev (char * str); - ������� ����������� ������ � ���������� ��������� �� ����� ������*/

char* mystrrev(char *str) {
	int size = strlen(str);
	char *str2 = new char[size + 1];
	for (int i = 0; i < size; ++i) {
		str2[i] = str[size - 1 - i];
	}
	str2[size] = '\0';
	return str2;
}