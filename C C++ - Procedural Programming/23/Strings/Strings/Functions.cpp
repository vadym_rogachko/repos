#define _CRT_SECURE_NO_WARNINGS
#include"Header.h"
#include<iostream>
#include<conio.h>



/*1. ���� ������ ��������. �������� � ��� ��� ������� �� ���������*/

void ReplaceSpaceToTab(char *str) {
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == ' ') {
			str[i] = '\t';
		}
	}
}

void ReplaceSpaceToTabCon(char *str) {
	for (int i = 0; str[i] != '\0'; ++i) {
		str[i] == ' ' && (str[i] = '\t');
	}
}

/*2. ���� ������ ��������. ���������� ���������� ����, ���� � ��������� ��������, �������������� � ������*/

void GetAmount(const char *str, short & letters, short & numbers, short & symbols) {
	// ������� �� ��������
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] >= 65 && str[i] <= 90 || str[i] >= 97 && str[i] <= 122) {
			++letters;
		}
		else if (str[i] >= 48 && str[i] <= 57) {
			++numbers;
		}
		else if (str[i] >= 33 && str[i] <= 47 || str[i] >= 58 && str[i] <= 64 || str[i] >= 91 && str[i] <= 96 || str[i] >= 123 && str[i] <= 126) {
			++symbols;
		}
	}
}

void GetAmountConDis(const char *str, short & letters, short & numbers, short & symbols) {
	for (int i = 0; str[i] != '\0'; ++i) {
		(str[i] >= 65 && str[i] <= 90 || str[i] >= 97 && str[i] <= 122) && ++letters ||
		(str[i] >= 48 && str[i] <= 57) && ++numbers ||
		(str[i] >= 33 && str[i] <= 47 || str[i] >= 58 && str[i] <= 64 || str[i] >= 91 && str[i] <= 96 || str[i] >= 123 && str[i] <= 126) && ++symbols;
	}
}

/*3. ���������� ���������� ���� �� ��������� �����������*/

short GetAmountWords(const char *str) {
	short count = 1;
	if (str[0] == ' ' || str[0] == '\t') {
		--count;
	}
	bool flag = false;
	// ��������� ���� �� � ��������� ������ �����
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] >= 65 && str[i] <= 90 || str[i] >= 97 && str[i] <= 122) {
			flag = true;
			break;
		}
	}
	if (flag) {
		for (int i = 0; str[i] != '\0'; ++i) {
			// ����� ������ ������ ��� ���������, � ����� ���������� � �����
			if ((str[i] == ' ' || str[i] == '\t') && (str[i + 1] >= 65 && str[i + 1] <= 90 || str[i + 1] >= 97 && str[i + 1] <= 122)) {
				++count;
			}
		}
		return count;
	}
	return 0;
}

/*4. ������� ������� ��� ������ �������� ������ � ���� �������*/

void ShowRus(char ch, const char *translate, int size) {
	size /= 2;
	// ������ � ���������� ������ � �����, ����� �� ��������� ��� ��� ������ ������ ������� � �� ������ ���������� ������
	for (int i = 0; i < size; ++i) {
		if (translate[i] == ch) {
			ch = translate[i + size];
			break;
		}
	}
	_putch(ch);
}

/*5. �������� ������� ��� �������� ������� ���� � ������� ������, ��������� � ����������*/

short CalcVowelLetters(char *buffer) {
	char vowelLetters[] = "�����������������ި�";
	short size = sizeof vowelLetters;
	short counter = 0;
	for (int i = 0; buffer[i] != '\0'; ++i) {
		for (int j = 0; j < size; ++j) {
			if (buffer[i] == vowelLetters[j]) {
				++counter;
				break;
			}
		}
	}
	return counter;
}

// ����� ��� �����-�� ���������� (����� ������� ��� ��������, ��������� � ����������� ������������� �������� ������� ���� � ������� CalcVowelLetters, � ��� ������ �� ��������)
void TranslatePrint(char *buffer, bool print) {
	char translate[] = "qwertyuiop[]asdfghjkl;'zxcvbnm,.`QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>~/@#$^&|?����������������������������������������������������������������ި.\"�;:?/,";
	short size = sizeof translate / 2;
	for (int i = 0; buffer[i] != '\0'; ++i) {
		for (int j = 0; j < size; ++j) {
			if (buffer[i] == translate[j]) {
				buffer[i] = translate[j + size];
				break;
			}
		}
	}
	print && puts(buffer);
}

/*6. ���� ������ ��������. ���������� ��������� �������� �� ��� ������ �����������*/

// ��������� ��� �������
bool IsPalindrome(const unsigned char *str, short size) {

	// ��� ����������� �������� ������ � �������� ascii ����� ��� unsigned char, �.�. � ��������� ������ ����������� ������������� ��������, � ��������� ���������� � �������������� ���������� �� �������

	/*
	char ch = '�';
	bool b;
	b = ch == 255 ? true : false;
	std::cout << b << std::endl;
	unsigned char unCh = '�';
	b = unCh == 255 ? true : false;
	std::cout << b << std::endl;
	*/

	unsigned char  *strcopy = new unsigned char[size] {};
	for (int i = 0, j = 0; str[i] != '\0'; ++i) {
		// ����������� ��� ����� � ������ ������� � �������� �������
		if (str[i] >= 65 && str[i] <= 90 || str[i] >= 192 && str[i] <= 223) {
			strcopy[j++] = str[i] + 32;
			continue;
		}
		else {
			strcopy[j++] = str[i];
		}
	}
	for (int i = 0; i < size / 2; ++i) {
		if (strcopy[i] != strcopy[size - 1 - i]) {
			delete[] strcopy;
			return false;
		}
	}
	delete[] strcopy;
	return true;
}

// ���������� ������ ����� (�� ��������� ������� � ����� ���������� � �������)
bool IsPalindromeText(const unsigned char *str, short size) {
	unsigned char  *strcopy = new unsigned char[size] {};
	// �������� � ����� ������ ������ �������, ������� �������� �������, ����������� ��� ����� � ������ �������
	for (int i = 0, j = 0; str[i] != '\0'; ++i) {
		if (str[i] >= 65 && str[i] <= 90 || str[i] >= 192 && str[i] <= 223) {
			strcopy[j++] = str[i] + 32;
			continue;
		}
		if (str[i] >= 97 && str[i] <= 122 || str[i] >= 48 && str[i] <= 57 || str[i] >= 224 && str[i] <= 255) {
			strcopy[j++] = str[i];
		}
	}
	size = 0;
	for (int i = 0; strcopy[i] != '\0'; ++i) {
		++size;
	}
	for (int i = 0; i < size / 2; ++i) {
		if (strcopy[i] != strcopy[size - 1 - i]) {
			delete[] strcopy;
			return false;
		}
	}
	delete[] strcopy;
	return true;
}

/*7. ������� ���������� �������*/

// � ����� Library.cpp

/*8. �������� �������, ������� ������� �� ������ ������ � �������� �������*/

char* DelSymbNum(char *str, short num) {
	if (num == 0 || num > mystrlen(str)) {
		return str;
	}
	for (int i = num - 1; str[i] != '\0'; ++i) {
		str[i] = str[i + 1];
	}
	return str;
}

/*9. �������� �������, ������� ������� �� ������ ��� ��������� � ��� ��������� �������*/

char* DelSymbAll(char *str, char symb) {
	bool flag = true;
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == symb) {
			flag = false;
			break;
		}
	}
	if (flag) {
		return str;
	}
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == symb) {
			for (int j = i; str[j] != '\0'; ++j) {
				str[j] = str[j + 1];
			}
			--i;
		}
	}
	return str;
}

/*10. �������� �������, ������� ��������� � ������ � ��������� ������� �������� ������*/

char* PasteSymb(char *str, char symb, short num) {
	if (num == 0 || num - 1 > mystrlen(str)) {
		return str;
	}
	int size = mystrlen(str) + 2;
	for (int i = size - 1; i >= num; --i) {
		str[i] = str[i - 1];
	}
	str[num - 1] = symb;
	return str;
}

/*11. �������� ���������, ������� ���������� � ������� �� ����� ����� ������� ����� � �����������. ��� ��������� ��������� ������������ ���������� ����������, � ����� ����������� ������� ��� ������ �� �������� (strlen, strchr, strcpy, strncpy)*/

char* GetBiggestWord(char *str) {
	int size = strlen(str);
	char *strTemp = new char[size + 1]{};
	strcpy(strTemp, str);
	strTemp[size] = ' ';
	char *str1, *str2;
	str1 = str2 = strTemp;
	short sizeMax = 0;
	char *p_maxWord = nullptr;
	while (size > 0) {
		str2 = strchr(str1, ' ');
		if (str2 - str1 > sizeMax) {
			sizeMax = str2 - str1;
			p_maxWord = str1;
		}
		size = size - (str2 - str1) - 1;
		str1 = str2 + 1;
	}
	str1 = new char[sizeMax + 1]{};
	strncpy(str1, p_maxWord, sizeMax);
	str1[sizeMax] = '\0';
	delete[] strTemp;
	return str1;
}

/*12. �������� ���������, ������� ���������� ��������� ��������������� ���������, ���������� �������������. ��� ���� � ��������� ��������� ������ ����� ����� ����� ����������� � ����� �������� �+� � �-�*/

int GetRes(char *str) {
	enum sign { plus = '+', minus = '-' };
	int res = 0;
	int num = 0;
	short action = plus;
	// ���������� ���� ������� �����
	if (str[0] >= 48 && str[0] <= 57 || str[0] == '+') {
		action = plus;
	}
	else if (str[0] == '-') {
		action = minus;
	}
	else {
		for (int i = 1; str[i] < 48 || str[i] > 57; ++i) {
			if (str[i] == '-') {
				action = minus;
			}
			else if (str[i] == '+') {
				action = plus;
			}
		}
	}
	// flag == false - ����� �� ������ � ����� ���������� ���� �������� � ���������� �����, flag == true - ������������ ������ �����, ���� �������� ��� ��������
	bool flag = true;
	for (int i = 0; str[i] != '\0'; ++i) {
		// ��� ������ �������� �����, � ����� ��� �� ����� - ���������� ������ �����
		if ((str[i] >= 48 && str[i] <= 57) && (str[i + 1] < 48 || str[i + 1] > 57)) {
			flag = false;
		}
		if (str[i] == ' ') {
			continue;
		}
		// ��������� �����, ����������� �� ������� � ���������� �������� �������� ���� ��������� � ����������� �� ����� �����
		if (str[i] >= 48 && str[i] <= 57) {
			switch (action) {
			case plus:
				num = num * 10 + (str[i] - 48);
				break;
			case minus:
				num = num * 10 - (str[i] - 48);
				break;
			}
			continue;
		}
		// ���� ������� ����� ��� ��������
		if (!flag) {
			// ������� ���������� ���� ��������
			if (str[i] == '+') {
				action = plus;
				// ���������� ��������� ����� (������������� ��� �������������) � ������ ����������
				res += num;
				num = 0;
			}
			// ���������� ����� ������ ��� ��������� ����� ����� � ��� �������, � ��������� ��� ���������� ������ ���������� ��� ����� �������� ��������
			else if (str[i] == '-') {
				action = minus;
				// ���������� ��������� ����� (������������� ��� �������������) � ������ ����������
				res += num;
				num = 0;
			}
			// �������� ����� �����
			// ��������� ��������� ������, ���� ��� �� �����, ����������� i � ��������� ������� - ���� + ��� ������, �� ���� �������� �� ��������, ���� -, �� ������ ���� �������� �� ���������������, ���� ����-������ - �����
			while ((str[i + 1] < 48 || str[i + 1] > 57) && ++i) {
				if (str[i] == '-') {
					action = action == plus ? minus : plus;
				}
				else if (str[i] == '\0') {
					--i;
					break;
				}
			}
		}
	}
	// ���������� � res ��������� �����
	return res + num;
}

// �������� ����� - ��������� ������ �����, ������� � �������� ��� ������ ������� + ��� -
bool CheckInput(char *str) {
	for (int i = 0; str[i] != '\0'; ++i) {
		if (!(str[i] >= 48 && str[i] <= 57 || str[i] == ' ' || str[i] == '+' || str[i] == '-')) {
			return true;
		}
	}
	short signs = 0;
	for (int i = 0; str[i] != '\0'; ++i) {
		if (signs > 2) {
			return true;
		}
		if (str[i] == '+' || str[i] == '-') {
			++signs;
		}
		else if (str[i] >= 48 && str[i] <= 57) {
			signs = 0;
		}
	}
	return false;
}

// �������� �������� �������

double PolishCalc(char *&str) {
	DelSymbAll(str, ' ');
	PlusMinus(str);
	short size = mystrlen(str);
	str = PolishNotation(str, size);
	return 0;
}

// �������� �����

bool CheckInput_(char *str) {
	bool flag = true;
	int j = 0;
	for (; str[j] != '\0'; ++j) {
		if (str[j] == '(') {
			flag = false;
		}
		if (str[j] == ')' && flag) {
			return true;
		}
	}
	flag = true;
	for (; j >= 0; --j) {
		if (str[j] == ')') {
			flag = false;
		}
		if (str[j] == '(' && flag) {
			return true;
		}
	}
	for (int i = 0; str[i] != '\0'; ++i) {
		if (!(str[i] >= 48 && str[i] <= 57 || str[i] == '+' || str[i] == '-' || str[i] == '.' || str[i] == '*' || str[i] == '/' || str[i] == '(' || str[i] == ')' || str[i] == ' ')) {
			return true;
		}
	}
	short signs = 0;
	for (int i = 0; str[i] != '\0'; ++i) {
		if (signs > 2) {
			return true;
		}
		if (str[i] == '+' || str[i] == '-' || str[i] == '*' || str[i] == '/') {
			++signs;
		}
		else if (str[i] >= '0' && str[i] <= '9' || str[i] == ')' || str[i] == '(') {
			signs = 0;
		}
	}
	int l = 0, r = 0;
	for (int i = 0; str[i] != '\0'; ++i) {
		str[i] == '(' && ++l || str[i] == ')' && ++r;
	}
	if (r != l) {
		return true;
	}
	flag = false;
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == '.' && !(str[i + 1] >= '0' && str[i + 1] <= '9')) {
			return true;
		}
		if ((str[i] == '*' || str[i] == '/' || str[i] == '%') && (str[i + 1] == '*' || str[i + 1] == '/')) {
			return true;
		}
		if (str[i] == '.') {
			if (flag) {
				return true;
			}
			else {
				flag = true;
			}
		}
		if (str[i] == '+' || str[i] == '-' || str[i] == '*' || str[i] == '/' || str[i] == ')' || str[i] == '(') {
			flag = false;
		}
	}
	return false;
}

// �������������� ������ ����� � ���� �������� ���� ����������

void PlusMinus(char *str) {
	for (int i = 0; str[i] != '\0'; ++i) {
		if (str[i] == '+' && str[i + 1] == '-') {
			DelSymbNum(str, i + 1);
		}
		else if (str[i] == '-' && str[i + 1] == '-') {
			DelSymbNum(str, i + 1);
			DelSymbNum(str, i + 1);
			PasteSymb(str, '+', i + 1);
		}
		else if ((str[i] == '+' || str[i] == '-') && str[i + 1] == '+') {
			DelSymbNum(str, i + 2);
		}
	}
}

// �������������� � �������� �������� �������

char* PolishNotation(char *str, short size) {
	char *stack = new char[size] {};
	char *notation = new char[size * 2] {};
	char symb;
	int index1 = 0, index2 = 0;
	for (int i = 0; str[i] != 0; ++i) {
		if (str[i] >= '0' && str[i] <= '9' || str[i] == '.') {
			notation[index2++] = str[i];
			if (!(str[i + 1] >= '0' && str[i + 1] <= '9' || str[i + 1] == '.')) {
				notation[index2++] = ' ';
			}
			continue;
		}
		if (str[i] == '(') {
			push(stack, index1, '(');
			continue;
		}
		if (str[i] == '+' || str[i] == '-') {
			if (stack[index1 - 1] == '*' || stack[index1 - 1] == '/') {
				while (index1 >= 1) {
					symb = pop(stack, index1);
					symb != '(' && (notation[index2++] = symb);
					if (symb == '(') {
						break;
					}
				}
				push(stack, index1, str[i]);
				continue;
			}
			stack[0] == 0 || stack[index1 - 1] == '(' ? push(stack, index1, str[i]) : (notation[index2++] = pop(stack, index1), push(stack, index1, str[i]));
			continue;
		}
		if (str[i] == '*' || str[i] == '/') {
			stack[index1 - 1] == '(' || stack[index1 - 1] == '+' || stack[index1 - 1] == '-' || stack[0] == 0 ? push(stack, index1, str[i]) : (notation[index2++] = pop(stack, index1), push(stack, index1, str[i]));
			continue;
		}
		if (str[i] == ')') {
			while (index1 >= 1) {
				symb = pop(stack, index1);
				symb != '(' && (notation[index2++] = symb);
				if (symb == '(') {
					break;
				}
			}
			continue;
		}
	}
	while (index1 >= 1) {
		symb = pop(stack, index1);
		symb != '(' && (notation[index2++] = symb);
	}
	for (int i = 0; notation[i] != '\0'; ++i) {
		if (notation[i] == '+' || notation[i] == '-' || notation[i] == '*' || notation[i] == '/') {
			PasteSymb(notation, ' ', i + 2);
		}
	}
	delete[] stack;
	return notation;
}

// ������ �� ������

void push(char *stack, int &index, char symb) {
	stack[index++] = symb;
}

char pop(char *stack, int &index) {
	index == -1 && ++index;
	char symb = stack[--index];
	stack[index] = 0;
	return symb;
}

double *stackG = new double[128]{};
int sizeG = 128;
int indexG = 0;

// �����������, ����������� ��������� � �������� �������� �������

double Calc(char *str) {
	double num, num1, num2;
	for (int i = 0; str[i] != '\0'; ++i) {
		num = 0;
		if (str[i] == ' ') {
			continue;
		}
		if (str[i] >= '0' && str[i] <= '9') {
			for (; str[i] >= '0' && str[i] <= '9'; ++i) {
				num = num * 10 + (str[i] - '0');
			}
			push(num);
			continue;
		}
		switch (str[i]) {
		case '+':
			num1 = pop(), num2 = pop();
			push(num1 + num2);
			break;
		case '-':
			num1 = pop(), num2 = pop();
			push(num2 - num1);
			break;
		case '*':
			num1 = pop(), num2 = pop();
			push(num2 * num1);
			break;
		case '/':
			if (num1 == 0) {
				std::cout << "\n������\n";
				return 0;
			}
			num1 = pop();
			num2 = pop();
			push(num2 / num1);
			break;
		}
	}
	num = pop();
	delete[] stackG;
	return num;
}

// ����

void push(double num) {
	if (indexG >= sizeG) {
		sizeG *= 2;
		double *temp = new double[sizeG] {};
		for (int i = 0; i < indexG; ++i) {
			temp[i] = stackG[i];
		}
		delete[] stackG;
		stackG = temp;
	}
	stackG[indexG++] = num;
}

double pop() {
	if (indexG - 1 < 0) {
		return 0;
	}
	return stackG[--indexG];
}