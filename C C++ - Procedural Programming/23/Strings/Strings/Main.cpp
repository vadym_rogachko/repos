#include"Header.h"
#include<iostream>
#include<conio.h>

using namespace std;														// Добавил обратную польскую нотацию - конвертер и калькулятор
													// Проверяет корректность ввода, поддерживает операции сложения, вычитания, умножения и деления, вещественные числа и скобки(приоритет)
void Next() {														// Выводит на экран запись выражения в польской нотации, обрабатывает его и выводит на экран резуьтат
	system("pause");
	system("cls");
}


void main() {

	setlocale(LC_ALL, "rus");
	
	/*1. Дана строка символов. Заменить в ней все пробелы на табуляции*/
	
	cout << "Дана строка символов. Заменить в ней все пробелы на табуляции\n\n";
	{
		char str[] = "a b c d e f g h i j";
		cout << str << endl << endl;
		ReplaceSpaceToTab(str);
		cout << str << endl << endl;
	}
	Next();
	{
		char str[] = "a b c de fg hj ik lmn opq rst";
		cout << endl << str << endl << endl;
		ReplaceSpaceToTabCon(str);
		cout << str << endl << endl;
	}
	Next();

	/*2. Дана строка символов. Определить количество букв, цифр и остальных символов, присутствующих в строке*/
	
	cout << "Дана строка символов. Определить количество букв, цифр и остальных символов,    присутствующих в строке\n\n";
	{
		char str[] = "Hello,WOR7LD!1245_-@.$%&*?+}\\~";
		cout << endl << str << endl << endl;
		short lett = 0, num = 0, symb = 0;
		GetAmount(str, lett, num, symb);
		cout << "Количество букв " << lett << endl << "Количество цифр " << num << endl << "Количество символов " << symb << endl << endl;
	}
	Next();
	{
		char str[] = "Hello12345@#$%673&*?+}~";
		cout << endl << str << endl << endl;
		short lett = 0, num = 0, symb = 0;
		GetAmountConDis(str, lett, num, symb);
		cout << "Количество букв " << lett << endl << "Количество цифр " << num << endl << "Количество символов " << symb << endl << endl;
	}
	Next();

	/*3. Подсчитать количество слов во введенном предложении*/
	
	cout << "Подсчитать количество слов во введенном предложении\n\n";
	{
		char str[128]{};
		cout << "Введите предложение латиницей\n";
		gets_s(str,127);
		short amount = GetAmountWords(str);
		cout << "\nКоличество слов во введенном предложении " << amount << endl << endl;
	}
	Next();

	/*4. Создать функцию для вывода русского текста в окно консоли*/
	
	cout << "Создать функцию для вывода русского текста в окно консоли\n\n";
	{
		cout << "Введите текст (Выход - ESC)\n\n";
		char ch;
		char translate[] = "qwertyuiop[]asdfghjkl;'zxcvbnm,.`QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>~/@#$^&|?йцукенгшщзхъфывапролджэячсмитьбюёЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁ.\"№;:?/,";
		while (true) {
			ch = _getch();
			if (ch == 13) {
				cout << '\n';
				continue;
			}
			else if (ch == 27) {
				break;
			}
			else if (ch < 0) {
				continue;
			}
			ShowRus(ch, translate, sizeof translate);
		}
		cout << endl;
	}
	Next();

	/*5. Написать функцию для подсчета гласных букв в русском тексте, введенном с клавиатуры*/
	
	cout << "Написать функцию для подсчета гласных букв в русском тексте, введенном с        клавиатуры\n\n";
	{
		cout << "Введите текст (Выход - ESC)\n\n";
		char ch = 0;
		char translate[] = "qwertyuiop[]asdfghjkl;'zxcvbnm,.`QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>~/@#$^&|?йцукенгшщзхъфывапролджэячсмитьбюёЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁ.\"№;:?/,";
		char buffer[1024]{};
		short counter = 0;
		while (true) {
			ch = _getch();
			if (ch == 13) {
				cout << '\n';
				continue;
			}
			else if (ch == 27) {
				break;
			}
			else if (ch < 0) {
				continue;
			}
			ShowRus(ch, translate, sizeof translate);
			buffer[counter++] = ch;
		}
		cout << endl;
		TranslatePrint(buffer);
		counter = CalcVowelLetters(buffer);
		cout << "\nКоличество гласных букв " << counter << endl << endl;
	}
	Next();

	/*6. Дана строка символов. Необходимо проверить является ли эта строка палиндромом*/
	
	cout << "Дана строка символов. Необходимо проверить является ли эта строка палиндромом\n\n";
	{
		unsigned char str[] = "+  8!% топот saippuakivikauppias топот %!8  +";
		IsPalindrome(str, sizeof str - 1) ? cout << str << "\n\nявляется палиндромом\n\n" : cout << str << "\nне является палиндромом\n\n";
	}
	Next();
	
	{
		 unsigned char str[] = "Аргентина манит негра";
		IsPalindromeText(str, sizeof str - 1) ? cout << endl << str << "\n\nявляется палиндромом\n\n" : cout << endl << str << "\n\nне является палиндромом\n\n";
	}
	Next();
	
	/*7. Создать библиотеку функций*/
	
	/*a. int mystrlen(const char * str); - функция определяет длину строки*/

	cout << "int mystrlen(const char * str); - функция определяет длину строки\n\n";
	{
		char str[] = "int mystrlen(const char * str); - функция определяет длину строки";
		cout << str << "\n\nДлина строки " << mystrlen(str) << endl << endl;
	}
	Next();

	/*b. char * mystrcpy (char * str1, const char * str2); - функция копирует строку str2 в буфер, адресуемый через str1. Функция возвращает указатель на первую строку str1*/

	{
		char str1[256] = {};
		char str2[] = "char * mystrcpy (char * str1, const char * str2); - функция копирует строку str2 в буфер, адресуемый через str1. Функция возвращает указатель на первую строку str1";
		puts(str2);
		cout << endl;
		mystrcpy(str1, str2);
		puts(str1);
		cout << endl;
	}
	Next();

	/*c. char * mystrcat (char * str1, const char * str2); - функция присоединяет строку str2 к строке str1. Функция возвращает указатель на первую строку str1*/

	{
		char str1[256] = "char * mystrcat (char * str1, const char * str2); - функция присоединяет строку str2 к строке str1. ";
		char str2[] = "Функция возвращает указатель на первую строку str1";
		puts(str1);
		cout << endl;
		puts(str2);
		cout << endl;
		mystrcat(str1, str2);
		puts(str1);
		cout << endl;
	}
	Next();

	/*d. char * mystrchr (char * str, char s); - функция осуществляет поиск символа s в  строке str. Функция возвращает указатель на первое вхождение символа в строку, в противном случае 0*/

	cout << "char * mystrchr (char * str, char s); - функция осуществляет поиск символа s в строке str. Функция возвращает указатель на первое вхождение символа в строку, в противном случае 0\n\n";
	{
		char str[] = "Hello world!";
		puts(str);
		char s;
		cout << "\nВведите символ\n";
		cin >> s;
		cout << endl;
		char *strs = mystrchr(str, s);
		if (strs) {
			cout << "Первое вхождение символа " << s << " в строку под порядковым номером " << strs - str + 1 << endl << endl;
		}
		else {
			cout << "Символ " << s << " в строке не найден\n\n";
		}
	}
	Next();

	/*e. char * mystrstr (char * str1, char * str2); - функция осуществляет поиск подстроки str2 в строке str1. Функция возвращает указатель на первое вхождение подстроки str2 в строку str1, в противном случае 0*/

	{
		char str1[] = "char * mystrstr (char * str1, char * str2); - функция осуществляет поиск подстроки str2 в строке str1. Функция возвращает указатель на первое вхождение подстроки str2 в строку str1, в противном случае 0";
		char str2[] = "Функция возвращает указатель на первое вхождение подстроки str2 в строку str1";
		puts(str1);
		cout << endl;
		char *str12 = mystrstr(str1, str2);
		if (str12) {
			cout << "str2 является подстрокой str1\n\nstr2:\n\n";
			puts(str2);
			cout << endl;
			cout << "str1 начиная с вхождения str2:\n\n";
			puts(str12);
			cout << endl << endl;
		}
		else {
			cout << "str2 не является подстрокой str1\n\n";
		}
	}
	Next();

	/*f. int mystrcmp (const char * str1, const char * str2); - функция сравнивает две строки, и , если строки равны возвращает 0, если первая строка больше второй, то возвращает 1, иначе -1*/

	cout << "int mystrcmp (const char * str1, const char * str2); - функция сравнивает две   строки, и , если строки равны возвращает 0, если первая строка больше второй, товозвращает 1, иначе -1\n\n";
	{
		char str1[64]{};
		char str2[64]{};
		bool flag = false;
		// Цикл для возможности ввести ввести символы без закомментирования предыдущих заданий (наверно в потоке Enter мешает или что-то другое)
		cout << "Введите пароль\n";
		while (str1[0] == '\0') {
			gets_s(str1, 63);
		}
		do {
			system("cls");
			cout << "Повторите ввод пароля\n";
			gets_s(str2, 63);
		} while (mystrcmp(str1, str2));
		cout << endl << str1 << " == " << str2 << endl << endl;
	}
	Next();

	/*g. int StringToNumber( char * str); - функция конвертирует строку в число и возвращает это число*/

	cout << "int StringToNumber( char * str); - функция конвертирует строку в число и        возвращает это число\n\n";
	{
		char str[64]{};
		cout << "Введите число\n";
		gets_s(str, 64);
		cout << "\nВы ввели " << StringToNumber(str) << endl << endl;
	}
	Next();

	/*h. char * NumberToString ( int number); - функция конвертирует число в строку и возвращает указатель на эту строку*/

	cout << "char * NumberToString ( int number); - функция конвертирует число в строку и    возвращает указатель на эту строку\n\n";
	{
		int num;
		cout << "Введите число\n";
		cin >> num;
		cout << num << endl;
		char *str = NumberToString(num);
		cout << "\nВы ввели " << str << endl << endl;
		delete[] str;
	}
	Next();

	/*i. char * Uppercase (char * str1); - функция преобразует строку в верхний регистр*/

	cout << "char * Uppercase (char * str1); - функция преобразует строку в верхний регистр\n\n";
	{
		cout << "Введите строку\n";
		char str[64]{};
		while (str[0] == '\0') {
			gets_s(str, 63);
		}
		Uppercase(str);
		puts(str);
	}
	Next();

	/*j. char * Lowercase (char * str1); - функция преобразует строку в нижний регистр*/

	cout << "char * Lowercase (char * str1); - функция преобразует строку в нижний регистр\n\n";
	{
		cout << "Введите строку\n";
		char str[64]{};
		while (str[0] == '\0') {
			gets_s(str, 63);
		}
		Lowercase(str);
		puts(str);
	}
	Next();

	/*k. char * mystrrev (char * str); - функция реверсирует строку и возвращает указатель на новую строку*/

	cout << "char * mystrrev (char * str); - функция реверсирует строку и возвращает         указатель на новую строку\n\n";
	{
		cout << "Введите строку\n";
		char str[64]{};
		while (str[0] == '\0') {
			gets_s(str, 63);
		}
		char *str2 = mystrrev(str);
		puts(str2);
		delete[] str2;
	}
	Next();
	
	{
		char str1[] = "Sum summus mus";
		char *str2 = mystrrev(str1);
		puts(str1);
		puts(str2);
		Lowercase(str1);
		Lowercase(str2);
		if (!mystrcmp(str1, str2)) {
			cout << "\nпалиндром\n";
		}
		else {
			cout << "\nне палиндром\n";
		}
		delete[] str2;
	}
	Next();

	/*8. Написать функцию, которая удаляет из строки символ с заданным номером*/

	cout << "Написать функцию, которая удаляет из строки символ с заданным номером\n\n";
	{
		cout << "Введите строку\n";
		char str[64]{};
		gets_s(str, 63);
		short num;
		cout << "Введите порядковый номер символа\n";
		cin >> num;
		if (num < 0) {
			num = -num;
		}
		DelSymbNum(str, num);
		cout << "\nНовая строка:\n\n";
		puts(str);
		cout << endl;
	}
	Next();

	/*9. Написать функцию, которая удаляет из строки все вхождения в нее заданного символа*/

	cout << "Написать функцию, которая удаляет из строки все вхождения в нее заданного       символа\n\n";
	{
		cout << "Введите строку\n";
		char str[64]{};
		while (str[0] == '\0') {
			gets_s(str, 63);
		}
		char symb;
		cout << "Введите символ\n";
		symb = _getch();
		cout << symb << endl;
		DelSymbAll(str, symb);
		cout << "\nНовая строка:\n\n";
		cout << str << endl << endl;
	}
	Next();

	/*10. Написать функцию, которая вставляет в строку в указанную позицию заданный символ*/

	cout << "Написать функцию, которая вставляет в строку в указанную позицию заданный символ\n\n";
	{
		cout << "Введите строку\n";
		char str[64]{};
		gets_s(str, 63);
		char symb;
		short num;
		cout << "Введите символ\n";
		symb = _getch();
		cout << symb << endl;
		cout << "Введите порядковый номер\n";
		cin >> num;
		if (num < 0) {
			num = -num;
		}
		PasteSymb(str, symb, num);
		cout << "\nНовая строка:\n\n";
		puts(str);
		cout << endl;
	}
	Next();

	/*11. Написать программу, которая определяет и выводит на экран самое длинное слово в предложении. При написании программы использовать арифметику указателей, а также стандартные функции для работы со строками (strlen, strchr, strcpy, strncpy)*/

	cout << "Написать программу, которая определяет и выводит на экран самое длинное слово в предложении. При написании программы использовать арифметику указателей, "
			"а такжестандартные функции для работы со строками (strlen, strchr, strcpy, strncpy)\n\n";
	{
		cout << "Введите предложение\n";
		char str[256]{};
		while (str[0] == '\0') {
			gets_s(str, 255);
		}
		char *p_word = GetBiggestWord(str);
		cout << "\nСамое длинное слово в предложении: " << p_word << endl << endl;
		delete[] p_word;
	}
	Next();

	/*12. Написать программу, которая определяет результат арифметического выражения, введенного пользователем. При этом в выражении допустимы только целые числа любой разрядности и знаки операций «+» и «-»*/

	{
		char str[64]{};
		bool flag = true;
		while (flag) {
			system("cls");
			cout << "Написать программу, которая определяет результат арифметического выражения,     введенного пользователем. При этом в выражении допустимы только целые числа     любой разрядности и знаки операций «+» и «-»\n\n";
			cout << "Введите арифметическое выражение\n\n";
			gets_s(str, 63);
			flag = CheckInput(str);
		}
		cout << endl << str << " = " << GetRes(str) << endl << endl;
	}
	Next();
	
	// Обратная польская нотация

	{
		char *str = new char[128]{};
		bool flag = true;
		char *strCopy = new char[128]{};
		while (flag) {
			system("cls");
			cout << "Введите арифметическое выражение\n\n";
			gets_s(str, 127);
			flag = CheckInput_(str);
			flag && cout << "Ошибка компиляции\n" && system("pause");
		}
		mystrcpy(strCopy, str);
		PolishCalc(str);
		cout << endl << str << endl << endl;
		cout << strCopy << " = " << Calc(str) << endl << endl;
		delete[] strCopy;
	}
	Next();
}