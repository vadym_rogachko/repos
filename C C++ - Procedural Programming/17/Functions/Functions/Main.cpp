#include<iostream>
#include<time.h>

using namespace std;                                         // ��������� �� ����� ���� ������� main() ��� �������,��� � ������� � ����� ����� Main.cpp 20 ������ �������?
														   	
															 // ���������� ������ ��� �� ��� �������� �� main() ������ �������� �������, � ���� ��������� - �� ��� void One(), void Two() � �.�, ������ � main()???
															
                                                             
void Next() {                                               
	system("pause");                                       
	system("cls");                                         
}															
                                                              

const int SIZE = 10;


void One();			double GetCube(double);
void Two();			int GetMaxNum(int, int);
void Three();		bool IsPositive(int);
void Calc();
void Five();        void ShowRecktangle(short, short);
void Six();			bool IsPrimeNum(long long);
void Seven();		long double GetFactorial(short);
void Eight();		double GetPow(double, short);
void Nine();		int GetSumDiap(int, int);
void Ten();			void SearchPerfectNum(int[], int, int);
void Eleven();		void ShowCard(short);
void Twelve();		bool IsSymbol(char);
void Thirteen();	double Rounding(double, short);
void Fourteen();	bool IsHappyNum(int);
void Fifteen();		int GetDifDays(short, short, short, short, short, short);
void Sixteen();		double GetArithMean(int[], int);
void Seventeen();	void CountPosNeg(int[], int);
void Eighteen();	void GetMinMax(int[], int);
void Nineteen();	void ChangeOrder(int[], int);
void Twenty();		short GetCountPrimeNums(int[], int);


void main() {
	setlocale(LC_ALL, "rus");
	srand(time(NULL));
	One();						Next();
	Two();						Next();
	Three();					Next();
	Calc();						Next();
	Five();           			Next();
	Six();					    Next();
	Seven();				    Next();
	Eight();				    Next();
	Nine();					    Next();
	Ten();					    Next();
	Eleven();					Next();
	Twelve();				    Next();
	Thirteen();				    Next();
	Fourteen();				    Next();
	Fifteen();				    Next();
	Sixteen();				    Next();
	Seventeen();			    Next();
	Eighteen();				    Next();
	Nineteen();				    Next();
	Twenty();
}


/*1. �������� �������, ������� ���������� ��� �����.*/

void One() {
	double num;
	cout << "������� �����\n";
	cin >> num;
	cout << num << "^3 = " << GetCube(num) << endl << endl;
}

double GetCube(double a) {
	return a * a * a;
}

/*2. �������� ������� ��� ���������� ����������� �� ���� �����.*/

void Two() {
	int num1, num2;
	cout << "������� ��� �����\n";
	cin >> num1 >> num2;
	if (num1 == num2) {
		char ch;
		cout << "\n����� �����. ���������?(Y/N)\n";
		cin >> ch;
		if (ch == 'Y' || ch == 'y') {
			system("cls");
			Two();
		}
		return;
	}
	cout << num1 << endl << num2 << endl << "\n���������� ����� = " << GetMaxNum(num1, num2) << endl << endl;
}

int GetMaxNum(int a, int b) {
	return (a > b) ? a : b;
}

/*3. �������� �������, ������� ���������� ������, ���� ������������ �������� ������������� � ����, ���� �������������.*/

void Three() {
	int num;
	cout << "������� �����\n";
	cin >> num;
	if (num == 0) {
		char ch;
		cout << "�� �����������? (Y/N)\n";
		cin >> ch;
		if (ch != 'Y' && ch != 'y') {
			cout << num << " �� ����� �����. ���������? (Y/N)\n";
			cin >> ch;
			if (ch == 'Y' || ch == 'y') {
				system("cls");
				Three();
			}
			return;
		}
	}
	cout << num << (IsPositive(num) ? " ������������� �����" : " ������������� �����") << endl << endl;
}

bool IsPositive(int a) {
	return a >= 0 ? true : false;
}

/*4. �������� �������, ������� � ����������� �� ������ ������������ �������� ������� ��������, ������������, ���������, ������� ���� ����� (��� ������� ���� ����� �������� ��������������).*/

double Plus(double, double);
double Minus(double, double);
double Multi(double, double);
double Div(double, double);
short Menu();
double GetNum();

void Calc() {
	double res;
	double x = GetNum();
	double y = GetNum();
	char action;
	switch (Menu()) {
	case 1:
		res = Plus(x, y);
		action = '+';
		break;
	case 2:
		res = Minus(x, y);
		action = '-';
		break;
	case 3:
		res = Multi(x, y);
		action = '*';
		break;
	case 4:
		if (y == 0) {
			cout << "\n������� �� ���� ����������\n\n";
			system("pause");
			system("cls");
			Calc();
			return;
		}
		res = Div(x, y);
		action = '/';
		break;
	default:
		system("cls");
		Calc();
		return;
	}
	cout << endl << x << ' ' << action << ' ' << y << " = " << res << endl << endl;
	char ch;
	cout << "����������? (Y/N)\n";
	cin >> ch;
	if (ch == 'Y' || ch == 'y') {
		system("cls");
		Calc();
		return;
	}
}

short Menu() {
	short action;
	cout << "�������� ��������:\n\t\t\t1. ��������\n\t\t\t2. ���������\n\t\t\t3. ���������\n\t\t\t4. �������\n";
	cin >> action;
	return action;
}

double GetNum() {
	double num;
	cout << "������� �����\n";
	cin >> num;
	return num;
}

double Plus(double num1, double num2) {
	return num1 + num2;
}

double Minus(double num1, double num2) {
	return num1 - num2;
}

double Multi(double num1, double num2) {
	return num1 * num2;
}

double Div(double num1, double num2) {
	return num1 / num2;
}

/*5. �������� �������, ��������� �� ����� ������������� � ������� N � ������� K.*/

void Five() {
	short N, K;
	cout << "������� ������ ��������������\n";
	cin >> N;
	cout << "������� ������ �������������\n";
	cin >> K;
	ShowRecktangle(N, K);
}

void ShowRecktangle(short N, short K) {
	cout << "\n ";
	for (int i = 0; i <= N; ++i) {
		for (int j = 0; j <= K; ++j) {
			if (i == 0 || i == N) {
				cout << "*";
				continue;
			}
			if (j == 0 || j == K) {
				cout << "*";
				continue;
			}
			cout << ' ';
		}
		cout << "\n ";
	}
	cout << endl;
}

/*6. �������� �������, ������� ���������, �������� �� ���������� �� ����� �������? ����� ���������� �������, ���� ��� ������� ��� ������� ������ �� ���� � �� �������.*/

void Six() {
	int num;
	cout << "������� �����(�������������)\n";
	cin >> num;
	if (num <= 0) {
		system("cls");
		Six();
		return;
	}
	cout << num << (IsPrimeNum(num) ? " ������� �����" : " �� ������� �����") << endl << endl;
}

bool IsPrimeNum(long long num) {
	long long index = 2;
	if (num <= 1) {
		return false;
	}
	if (num == 2) {
		return true;
	}
	while (index <= num / 2) {
		if (num % index == 0) {
			return false;
		}
		index++;
	}
	return true;
}

/*7. �������� �������, ����������� ��������� ����������� �� �����.*/

void Seven() {
	int num;
	cout << "������� �����(�� �������������)\n";
	cin >> num;
	if (num < 0) {
		system("cls");
		Seven();
		return;
	}
	cout.precision(15);
	cout << num << "! = " << GetFactorial(num) << endl << endl;
	cout.precision(6);
}

long double GetFactorial(short num) {
	long double factorial = 1;
	for (int i = 1; i <= num; ++i) {
		factorial *= i;
	}
	return factorial;
}

/*8. �������� �������, ������� ��������� ��� ���������: ��������� ������� � ���������� �������, � ��������� ������� ����� �� ������ ���������� ������.*/

void Eight() {
	double num;
	short power;
	cout << "������� �����\n";
	cin >> num;
	cout << "������� �������\n";
	cin >> power;
	if (num == 0 && num == power) {
		Eight();
		return;
	}
	cout.precision(15);
	cout << num << "^" << power << " = " << GetPow(num, power) << endl << endl;
	cout.precision(6);
}

double GetPow(double num, short power) {
	double powerNumber = 1;
	if (power > 0) {
		for (int i = 0; i < power; ++i) {
			powerNumber *= num;
		}
	}
	else if (power < 0) {
		for (int i = 0; i > power; --i) {
			powerNumber *= num;
		}
		powerNumber = 1 / powerNumber;
	}
	return powerNumber;
}

/*9. �������� �������, ������� �������� � �������� ���������� 2 ����� ����� � ���������� ����� ����� �� ��������� ����� ����.*/

void Nine() {
	int num1, num2;
	cout << "������� ��� �����\n";
	cin >> num1 >> num2;
	cout << "����� ����� ����� " << num1 << " � " << num2 << " = " << GetSumDiap(num1, num2) << endl << endl;
}

int GetSumDiap(int num1, int num2) {
	int sum = 0;
	for (int i = ++num1; i < num2; ++i) {
		sum += i;
	}
	return sum;
}

/*10. ����� ���������� �����������, ���� ����� ���� ��� ��������� ����� ��� ������. �������� ������� ������ ����� ����� �� ��������� ���������.*/

void Ten() {
	int min, max;
	const int SIZE = 5;
	int arr[SIZE]{};
	cout << "������� �������� �����\n";
	cin >> min >> max;
	if (min > max) {
		swap(min, max);
	}
	SearchPerfectNum(arr, min, max);
	bool flag = true;
	system("cls");
	cout << "����������� ����� � ��������� �� " << min << " �� " << max << ":\n\n";
	for (int i = 0; i < SIZE; ++i) {
		if (arr[i] != 0) {
			cout << arr[i] << '\t';
			flag = false;
		}
	}
	if (flag) {
		cout << "����������� ����� �����������";
	}
	cout << endl << endl;
}

void SearchPerfectNum(int arr[], int min, int max) {
	int sum;
	short k = 0;
	for (int i = min; i <= max; ++i) {
		sum = 0;
		for (int j = 1; j < i; ++j) {
			if (i % j == 0) {
				sum += j;
			}
		}
		if (sum == i) {
			arr[k++] = i;
		}
	}
}

/*11. �������� �������, ��������� �� ����� ���������� �� ��������� �����.*/

void Eleven() {
	int card;
	cout << "������� ���������� ����� �����\n";
	cin >> card;
	ShowCard(card);
}

void ShowCard(short card) {
	cout << endl;

	enum suits { HEARTS, DIAMONDS, CLUBS, SPADES };
	switch (card / 13 % 4) {
	case SPADES:
		cout << char(6);
		break;
	case CLUBS:
		cout << char(5);
		break;
	case DIAMONDS:
		cout << char(4);
		break;
	case HEARTS:
		cout << char(3);
		break;
	}

	enum cards { DEUCE, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };
	switch (card % 13) {
	case DEUCE:
		cout << "2\n";
		break;
	case THREE:
		cout << "3\n";
		break;
	case FOUR:
		cout << "4\n";
		break;
	case FIVE:
		cout << "5\n";
		break;
	case SIX:
		cout << "6\n";
		break;
	case SEVEN:
		cout << "7\n";
		break;
	case EIGHT:
		cout << "8\n";
		break;
	case NINE:
		cout << "9\n";
		break;
	case TEN:
		cout << "T\n";
		break;
	case JACK:
		cout << "J\n";
		break;
	case QUEEN:
		cout << "Q\n";
		break;
	case KING:
		cout << "K\n";
		break;
	case ACE:
		cout << "A\n";
		break;
	}
	cout << endl;
}

/*12. �������� �������, ����������� �������� �� ���������� �� �������� ���������-��������.*/

void Twelve() {
	char ch;
	cout << "������� ������\n";
	cin >> ch;
	cout << endl << ch << (IsSymbol(ch) ? " �������� ���������-�������� ����������\n\n" : " �� �������� ��������� �������� ����������\n\n");
}

bool IsSymbol(char ch) {
	if ((ch >= 48 && ch <= 57) || (ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122) || ch >= 192) {
		return true;
	}
	return false;
}

/*13. �������� ������� ��� ���������� ��������� ����� � �������� ���������.*/

void Thirteen() {
	double num;
	short acc;
	cout << "������� ������������ �����\n";
	cin >> num;
	cout << "������� ��������, �� ������� ����� ��������� �����\n";
	cin >> acc;
	num = Rounding(num, acc);
	cout << "\n����������� ����� = " << num << endl << endl;
}

double Rounding(double num, short acc) {
	for (int i = 0; i < acc; ++i) {
		num *= 10;
	}
	num = int(num + 0.5);
	for (int i = 0; i < acc; ++i) {
		num /= 10;
	}
	return num;
}

/*14. �������� �������, ������� ����������, �������� �� ���������� ������������ �����.*/

void Fourteen() {
	int num;
	do {
		system("cls");
		cout << "������� ������������ �����\n";
		cin >> num;
	} while (num > 999999 || num < 100000);
	cout << endl << num << (IsHappyNum(num) ? " �������� ���������� ������\n\n" : " �� �������� ���������� ������\n\n");
}

bool IsHappyNum(int num) {
	return num / 100000 + num / 10000 % 10 + num / 1000 % 10 == num % 10 + num % 100 / 10 + num % 1000 / 100 ? true : false;
}

/*15. �������� �������, ������� ��������� ��� ���� (�.�. ������� ��������� ����� ����������) � ��������� �������� � ���� ����� ����� ������.��� ������� ���� ������ ���������� ����� �������� �������,
������� ����������, �������� �� ��� ����������.*/

void Fifteen() {
	int day1, day2, month1, month2, year1, year2;
	bool flag, checkDate = false;
	do {
		if (!checkDate) {
			cout << "������� ������ ���� - ����, �����, ���\n\n";
			checkDate = true;
		}
		else {
			system("cls");
			cout << "������...\n��������� ����: ����, �����, ���\n";
		}
		cin >> day1 >> month1 >> year1;
		switch (month1) {
		case 4:
		case 6:
		case 9:
		case 11:
			(day1 == 31) ? flag = false : flag = true;
			break;
		case 2:
			(day1 == 30 || day1 == 31) ? flag = false : flag = true;
			if (day1 == 29) {
				(year1 % 4 == 0 && year1 % 100 != 0) ? flag = true : (year1 % 4 == 0 && year1 % 100 == 0 && year1 % 400 == 0) ? flag = true : flag = false;
			}
			break;
		default:
			flag = true;
		}
	} while (day1 < 1 || day1 > 31 || month1 < 1 || month1 > 12 || year1 == 0 || flag == false);
	checkDate = false;
	do {
		if (!checkDate) {
			cout << "������� ������ ���� - ����, �����, ���\n\n";
			checkDate = true;
		}
		else {
			system("cls");
			cout << "������...\n��������� ����: ����, �����, ���\n";
		}
		cin >> day2 >> month2 >> year2;
		switch (month2) {
		case 4:
		case 6:
		case 9:
		case 11:
			(day2 == 31) ? flag = false : flag = true;
			break;
		case 2:
			(day2 == 30 || day2 == 31) ? flag = false : flag = true;
			if (day2 == 29) {
				(year2 % 4 == 0 && year2 % 100 != 0) ? flag = true : (year2 % 4 == 0 && year2 % 100 == 0 && year2 % 400 == 0) ? flag = true : flag = false;
			}
			break;
		default:
			flag = true;
		}
	} while (day2 < 1 || day2 > 31 || month2 < 1 || month2 > 12 || year2 == 0 || flag == false);
	cout << "\n�������� � ���� ����� ���������� ������ = " << GetDifDays(day1, month1, year1, day2, month2, year2) << endl << endl;
}

bool IsLeapYear(int);
bool Is30DaysInMonth(int);

int GetDifDays(short day, short month, short year, short day2, short month2, short year2) {
	int sumOfDays = 0;
	if (year != year2) {
		for (; month <= 12; day = 1, ++month) {
			for (; day <= 31; ++day) {
				if (month == 2 && day == 29) {
					if (!IsLeapYear(year)) {
						break;
					}
					++sumOfDays;
					break;
				}
				if (day == 30) {
					Is30DaysInMonth(month) ? ++sumOfDays : ++sumOfDays += 1;
					break;
				}
				++sumOfDays;
			}
		}
		++year;
		for (; year < year2; ++year) {
			IsLeapYear(year) ? ++sumOfDays += 365 : sumOfDays += 365;
		}
		for (month = 1; month < month2; day = 1, ++month) {
			for (day = 1; day <= 31; ++day) {
				if (month == 2 && day == 29) {
					if (!IsLeapYear(year)) {
						break;
					}
					++sumOfDays;
					break;
				}
				if (day == 30) {
					Is30DaysInMonth(month) ? ++sumOfDays : ++sumOfDays += 1;
					break;
				}
				++sumOfDays;
			}
		}
	}
	else {
		if (month == month2) {
			return day2 - day;
		}
		for (; month < month2; day = 1, ++month) {
			for (; day <= 31; ++day) {
				if (month == 2 && day == 29) {
					if (!IsLeapYear(year)) {
						break;
					}
					++sumOfDays;
					break;
				}
				if (day == 30) {
					Is30DaysInMonth(month) ? ++sumOfDays : ++sumOfDays += 1;
					break;
				}
				++sumOfDays;
			}
		}
	}
	sumOfDays += day2;
	return --sumOfDays;
}

bool IsLeapYear(int year) {
	return year % 4 == 0 && year % 100 != 0 ? true : year % 4 == 0 && year % 100 == 0 && year % 400 == 0 ? true : false;
}

bool Is30DaysInMonth(int month) {
	switch (month) {
	case 4:
	case 6:
	case 9:
	case 11:
		return true;
	}
	return false;
}

/*16. �������� �������, ������������ ������� �������������� ��������� ������������� �� �������.*/

void InitIntArray(int[], int);
void ShowIntArray(int[], int);
void Sixteen() {
	int arr[SIZE]{};
	InitIntArray(arr, SIZE);
	ShowIntArray(arr, SIZE);
	cout << "\n\n������� �������������� ������� = " << GetArithMean(arr, SIZE) << endl << endl;
}

void InitIntArray(int arr[], int size) {
	for (int i = 0; i < size; ++i) {
		arr[i] = rand() % 100;
	}
}

void ShowIntArray(int arr[], int size) {
	for (int i = 0; i < size; ++i) {
		cout << arr[i] << ' ';
	}
}

double GetArithMean(int arr[], int size) {
	int sum = 0;
	for (int i = 0; i < size; ++i) {
		sum += arr[i];
	}
	return 1. * sum / size;
}

/*17. �������� �������, ������������ ���������� �������������, ������������� � ������� ��������� ������������� �� �������.*/

void InitIntArrayPosNeg(int[], int);
void Seventeen() {
	int arr[SIZE]{};
	InitIntArrayPosNeg(arr, SIZE);
	ShowIntArray(arr, SIZE);
	CountPosNeg(arr, SIZE);
}

void InitIntArrayPosNeg(int arr[], int size) {
	for (int i = 0; i < size; ++i) {
		arr[i] = rand() % 21 - 10;
	}
}

void CountPosNeg(int arr[], int size) {
	int plus = 0, minus = 0, zero = 0;
	for (int i = 0; i < size; ++i) {
		arr[i] > 0 ? ++plus : arr[i] < 0 ? ++minus : ++zero;
	}
	cout << "\n\n���������� ������������� ��������� �������: " << plus << endl;
	cout << "���������� ������������� ��������� �������: " << minus << endl;
	cout << "���������� ������� ��������� �������: " << zero << endl << endl;
}

/*18. �������� �������, ������������ ������� � �������� (�������� � �����) ��������� ������������� �� �������.*/

void Eighteen() {
	int arr[SIZE]{};
	InitIntArrayPosNeg(arr, SIZE);
	ShowIntArray(arr, SIZE);
	GetMinMax(arr, SIZE);
}

void GetMinMax(int arr[], int size) {
	int min, max, minNum, maxNum;
	min = arr[0];
	max = arr[0];
	minNum = 0;
	maxNum = 0;
	for (int i = 0; i < size; ++i) {
		if (arr[i] < min) {
			min = arr[i];
			minNum = i;
		}
		if (arr[i] > max) {
			max = arr[i];
			maxNum = i;
		}
	}
	cout << "\n\n����������� ������� �������: " << min << " � ���������� ������� � ������� " << minNum + 1 << endl;
	cout << "������������ ������� �������: " << max << " � ���������� ������� � ������� " << maxNum + 1 << endl << endl;
}

/*19. �������� �������, �������� ������� ���������� ��������� ������������� �� ������� �� ���������������.*/

void Nineteen() {
	int arr[SIZE]{};
	InitIntArray(arr, SIZE);
	ShowIntArray(arr, SIZE);
	cout << "\n\n��������������� ������� ���������� ��������� �������: \n\n";
	ChangeOrder(arr, SIZE);
	for (int i = 0; i < SIZE; ++i) {
		cout << arr[i] << ' ';
	}
	cout << endl << endl;
}

void ChangeOrder(int arr[], int size) {
	for (int i = 0; i < size / 2; ++i) {
		swap(arr[i], arr[size - 1 - i]);
	}
}

/*20. �������� �������, ������������ ���������� ������� ����� � ������������ �� �������.*/

void Twenty() {
	int arr[SIZE]{};
	InitIntArray(arr, SIZE);
	ShowIntArray(arr, SIZE);
	cout << "\n\n���������� ������� ����� � ������� = " << GetCountPrimeNums(arr, SIZE) << endl << endl;
}

short GetCountPrimeNums(int arr[], int size) {
	int count = 0;
	bool flag;
	for (int i = 0; i < size; ++i) {
		for (int j = 2; j <= arr[i] / 2; ++j) {
			flag = arr[i] % j ? true : false;
			if (!flag) {
				break;
			}
		}
		if (flag || arr[i] == 2) {
			++count;
		}
	}
	return count;
}