#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<tchar.h>
#include<vector>
#include<ctime>
#include"resource.h"

using std::vector;

/*	�������� ����������, ���������� ��������� �����������������:
	- ����� ������� ������� <Enter> ����� ������ ������� (��� ���� ���������� �������) ���������� ���� �� ������ "������������", ��������� ��������� �������;
	- ����� ������� ������� <Esc> ������ ������� ��������������� */

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
VOID CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime);
BOOL CALLBACK EnumChildProc(HWND hWnd, LPARAM lParam);
vector<HWND> vecHwnd;

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow) {

	srand(time(NULL));
	TCHAR szClassWindow[] = TEXT("Hide Buttons");
	HWND hWnd;
	MSG msg;
	WNDCLASSEX wndcls;

	wndcls.cbClsExtra = 0;
	wndcls.cbWndExtra = 0;
	wndcls.cbSize = sizeof wndcls;
	wndcls.lpfnWndProc = WindowProc;
	wndcls.hInstance = hInst;
	wndcls.style = CS_HREDRAW | CS_VREDRAW;
	wndcls.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));
	wndcls.hCursor = LoadCursor(hInst, MAKEINTRESOURCE(IDC_POINTER));
	wndcls.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndcls.lpszMenuName = NULL;
	wndcls.lpszClassName = szClassWindow;
	wndcls.hIconSm = NULL;

	if (!RegisterClassEx(&wndcls)) {
		return 0;
	}

	hWnd = CreateWindowEx(
		0,
		szClassWindow,
		TEXT("Press Enter to start"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInst,
		NULL
	);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	while (GetMessage(&msg, NULL, 0, 0)) {
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) {

	static HWND hWndCalc;
	switch (uMessage) {
	case WM_CREATE:
		while (!(hWndCalc = FindWindow(TEXT("CalcFrame"), TEXT("�����������")))) {
			if (IDCANCEL == MessageBox(hWnd, TEXT("�������� ���������� �����������"), TEXT(""), MB_OKCANCEL | MB_ICONEXCLAMATION)) {
				PostQuitMessage(0);
				return 0;
			}
		}
		vecHwnd.push_back(hWndCalc);
		EnumChildWindows(hWndCalc, EnumChildProc, 0);
		break;
	case WM_KEYDOWN:
		if (wParam == VK_RETURN) {
			SetTimer(hWnd, 1, 1000, TimerProc);
			SetWindowText(hWnd, TEXT("Press Esc to stop"));
		}
		else if (wParam == VK_ESCAPE) {
			KillTimer(hWnd, 1);
			SetWindowText(hWnd, TEXT("Press Enter to start"));
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}

VOID CALLBACK TimerProc(HWND hWnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime) {
	static int iButtonWnd;
	ShowWindow(vecHwnd[iButtonWnd], SW_SHOW);
	iButtonWnd = rand() % (vecHwnd.size() - 1) + 1;
	ShowWindow(vecHwnd[iButtonWnd], SW_HIDE);
	RedrawWindow(vecHwnd[0], nullptr, nullptr, RDW_INVALIDATE);
	//InvalidateRect(vec[0], nullptr, FALSE);
}

BOOL CALLBACK EnumChildProc(HWND hWnd, LPARAM lParam) {
	TCHAR szClassName[100]{};
	GetClassName(hWnd, szClassName, sizeof szClassName);
	if (lstrcmp(szClassName, TEXT("Button")) == 0) {
		vecHwnd.push_back(hWnd);
	}
	return TRUE;
}