#define UNICODE
#include<windows.h>
#include<tchar.h>

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, INT nCmdShow) {

	enum { MIN = 1, MAX = 100 };
	const size_t SIZE_BUFFER = 64;
	TCHAR szBuffer[SIZE_BUFFER]{};
	const TCHAR szCaption[] = TEXT("������� �����");
	UINT uMinNumber = MIN;
	UINT uMaxNumber = MAX;
	UINT uCounter = 1;
	UINT uMidNumber;
	wsprintf(szBuffer, TEXT("��������� ����� �� %d �� %d"), MIN, MAX);
	MessageBox(nullptr, szBuffer, szCaption, MB_OK | MB_ICONEXCLAMATION);
	while (true) {
		uMidNumber = (uMinNumber + uMaxNumber) / 2;
		wsprintf(szBuffer, TEXT("�� �������� %d?"), uMidNumber);
		if (MessageBox(nullptr, szBuffer, szCaption, MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2) != IDYES) {
			if (uCounter == 7) {
				if (MessageBox(nullptr, TEXT("�� ����������"), szCaption, MB_RETRYCANCEL | MB_ICONHAND) == IDRETRY) {
					uMinNumber = MIN;
					uMaxNumber = MAX;
					uCounter = 1;
					continue;
				}
				return 0;
			}
			wsprintf(szBuffer, TEXT("����� ������ %d?"), uMidNumber);
			if (MessageBox(nullptr, szBuffer, szCaption, MB_YESNO | MB_ICONQUESTION) == IDYES) {
				uMinNumber = uMidNumber + 1;
			}
			else {
				uMaxNumber = uMidNumber;
			}
		}
		else {
			wsprintf(szBuffer, TEXT("����� ��������\n���������� �������: %d"), uCounter);
			if (MessageBox(nullptr, szBuffer, szCaption, MB_RETRYCANCEL | MB_ICONINFORMATION) == IDRETRY) {
				uMinNumber = MIN;
				uMaxNumber = MAX;
				uCounter = 1;
				continue;
			}
			return 0;
		}
		++uCounter;
	}
}