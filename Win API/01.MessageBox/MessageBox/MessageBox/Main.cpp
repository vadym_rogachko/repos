#define UNICODE
#include<windows.h>
#include<tchar.h>


INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, INT nCmdShow) {

	LPTSTR alpszCV[] = { TEXT("Vadym Rogachko"), TEXT("26 years"), TEXT("Ukraine, Odessa"), TEXT("Student") };
	const size_t SIZE_BUFFER = 64;
	TCHAR szBuffer[SIZE_BUFFER];
	const UINT nBoxes = sizeof alpszCV / sizeof alpszCV[0];
	UINT nSymbols = 0;
	for (INT i = 0; i < nBoxes; ++i) {
		nSymbols += lstrlen(alpszCV[i]);
		MessageBox(nullptr, alpszCV[i], i < nBoxes - 1 ? TEXT("CV") : (_stprintf(szBuffer, TEXT("%.1f символов"), nSymbols / double(nBoxes)), szBuffer), MB_OK | MB_ICONINFORMATION);
	}
}