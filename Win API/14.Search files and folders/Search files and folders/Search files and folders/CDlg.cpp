#include<windowsx.h>
#include<io.h>
#include<iostream>
#include<tchar.h>
#include<ctime>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
	hThread = nullptr;
	hImgList = nullptr;
	for (int i = 0; i < COUNT_COL; ++i) {
		paramSort[i] = 1;
	}
}

CDlg::~CDlg() {
	CloseHandle(hThread);
	ImageList_Destroy(hImgList);
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	case WM_NOTIFY:
		if (wParam == IDC_LIST_VIEW) {
			if (LPNMHDR(lParam)->code == NM_DBLCLK) {
				if (!SendMessage(ptr->hCtrl[LIST_VIEW], LVM_GETITEMSTATE, SendMessage(ptr->hCtrl[LIST_VIEW], LVM_GETSELECTIONMARK, 0, 0), LVIS_SELECTED)) break;
				ptr->ExecuteFile(LPNMITEMACTIVATE(lParam)->iItem);
			}
			else  if (LPNMHDR(lParam)->code == LVN_COLUMNCLICK) {
				ptr->SortListView(LPNMLISTVIEW(lParam)->iSubItem);
			}
			break;
		}
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	setListView();
	setDrives();
	SendMessage(hCtrl[CHECK_SUBDIR], BM_SETCHECK, BST_CHECKED, 0);
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (hwndCtl == hCtrl[BTN_FIND]) {
		counterFiles = 0;
		TCHAR szBuf[] = TEXT("0");
		SendMessage(hCtrl[STATIC_COUNT_FILES], WM_SETTEXT, 0, (LPARAM)szBuf);
		CloseHandle(hThread);
		ImageList_Destroy(hImgList);
		hImgList = ImageList_Create(GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), ILC_COLOR, 0, 1);
		ListView_SetImageList(hCtrl[LIST_VIEW], hImgList, LVSIL_SMALL);
		hThread = CreateThread(nullptr, 0, ThreadFindFilesAndFolders, this, 0, nullptr);
	}
	else if (hwndCtl == hCtrl[BTN_STOP]) {
		TerminateThread(hThread, 0);
		EnableWindow(hCtrl[BTN_STOP], false);
		EnableWindow(hCtrl[BTN_FIND], true);
	}
}

void CDlg::setCtrlHandles() {
	for (int i = EDIT_FILE; i < COUNT_CTRL; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_EDIT_FILE + i);
	}
}

void CDlg::setListView() {
	SendMessage(hCtrl[LIST_VIEW], LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_GRIDLINES);
	TCHAR szBuf[32];
	int size[COUNT_COL] = { WIDTH_NAME, WIDTH_FOLDER, WIDTH_SIZE, WIDTH_DATE_MOD };
	LVCOLUMN listViewCol;
	listViewCol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT;
	for (int i = 0; i < COUNT_COL; ++i) {
		listViewCol.iSubItem = i;
		listViewCol.pszText = szBuf;
		listViewCol.cx = size[i];
		if (i < 2) {
			listViewCol.fmt = LVCFMT_LEFT;
		}
		else {
			listViewCol.fmt = LVCFMT_CENTER;
		}
		LoadString(GetModuleHandle(nullptr), IDS_STRING_NAME + i, szBuf, sizeof szBuf / sizeof szBuf[0]);
		ListView_InsertColumn(hCtrl[LIST_VIEW], i, &listViewCol);
	}
}

void CDlg::setDrives() {
	DWORD drives = GetLogicalDrives();
	int countDrives = 0;
	for (int i = 0; i < sizeof drives * 8; ++i) {
		if ((drives >> i) & 1) ++countDrives;
	}
	int sizeDriveNamesString = (countDrives * 4) + 1;
	LPTSTR szDriveNames = new TCHAR[sizeDriveNamesString];
	GetLogicalDriveStrings(sizeDriveNamesString, szDriveNames);
	TCHAR szDriveName[32]{};
	for (int i = 0; i < countDrives; ++i) {
		lstrcpy(szDriveName, szDriveNames + (4 * i));
		szDriveName[2] = '\0';
		SendMessage(hCtrl[COMBO_DISKS], CB_ADDSTRING, 0, (LPARAM)szDriveName);
	}
	SendMessage(hCtrl[COMBO_DISKS], CB_SETCURSEL, 0, 0);
	delete[] szDriveNames;
}

void CDlg::ExecuteFile(int iItem) {
	TCHAR szBuf[MAX_PATH]{};
	LV_ITEM lv{};
	lv.iSubItem = 1;
	lv.cchTextMax = MAX_PATH;
	lv.pszText = szBuf;
	SendMessage(hCtrl[LIST_VIEW], LVM_GETITEMTEXT, iItem, (LPARAM)&lv);
	ShellExecute(nullptr, TEXT("open"), lv.pszText, nullptr, nullptr, SW_SHOWNORMAL);
}

void CDlg::SortListView(int iSubItem) {
	ListView_SortItems(hCtrl[LIST_VIEW], CompareListViewItem, iSubItem);
	paramSort[iSubItem] *= -1;
	SetNewIndexOrder();
}

DWORD WINAPI ThreadFindFilesAndFolders(LPVOID lpPraram) {
	CDlg *dlg = (CDlg*)lpPraram;
	SendMessage(dlg->hCtrl[dlg->LIST_VIEW], LVM_DELETEALLITEMS, 0, 0);
	EnableWindow(dlg->hCtrl[dlg->BTN_STOP], true);
	EnableWindow(dlg->hCtrl[dlg->BTN_FIND], false);
	TCHAR szPath[MAX_PATH]{}, szFile[MAX_PATH]{};
	SendMessage(dlg->hCtrl[dlg->COMBO_DISKS], CB_GETLBTEXT, SendMessage(dlg->hCtrl[dlg->COMBO_DISKS], CB_GETCURSEL, 0, 0), (LPARAM)szPath);
	szPath[2] = '\\';
	SendMessage(dlg->hCtrl[dlg->EDIT_FILE], WM_GETTEXT, MAX_PATH, (LPARAM)szFile);
	if (szFile[0] == '\0') {
		MessageBox(dlg->hDlg, TEXT("������� ��� ����� ��� �����"), nullptr, MB_OK | MB_ICONEXCLAMATION);
		EnableWindow(dlg->hCtrl[dlg->BTN_STOP], false);
		EnableWindow(dlg->hCtrl[dlg->BTN_FIND], true);
		return 0;
	}
	dlg->wordInFileLength = SendMessage(dlg->hCtrl[dlg->EDIT_FILE_PART], WM_GETTEXTLENGTH, 0, 0);
	if (BST_UNCHECKED == SendMessage(dlg->hCtrl[dlg->CHECK_SUBDIR], BM_GETCHECK, 0, 0)) {
		dlg->FindFile(szPath, szFile);
	}
	else {
		LPTSTR ptr;
		_tcscat(szPath, TEXT("*"));
		dlg->FindFileSubdirs(szPath, szFile);
	}
	EnableWindow(dlg->hCtrl[dlg->BTN_STOP], false);
	EnableWindow(dlg->hCtrl[dlg->BTN_FIND], true);
	if (!SendMessage(dlg->hCtrl[dlg->LIST_VIEW], LVM_GETITEMCOUNT, 0, 0)) {
		MessageBox(dlg->hDlg, TEXT("�� �������"), TEXT(""), MB_OK | MB_ICONASTERISK);
	}
	return 0;
}

void CDlg::FindFile(LPCTSTR lpszDir, LPCTSTR lpszKey) {
	if (_tcslen(lpszDir) + _tcslen(lpszKey) >= MAX_PATH) return;
	TCHAR szFullPath[MAX_PATH]{};
	_tcscpy(szFullPath, lpszDir);
	_tcscat(szFullPath, lpszKey);
	_wfinddata_t fileinfo;
	intptr_t handle = _wfindfirst(szFullPath, &fileinfo);
	intptr_t result = handle;
	while (result != -1) {
		if (!lstrcmp(fileinfo.name, TEXT(".")) || !lstrcmp(fileinfo.name, TEXT(".."))) {
			result = _wfindnext(handle, &fileinfo);
			continue;
		}
		if (_tcslen(szFullPath) + _tcslen(fileinfo.name) >= MAX_PATH) {
			result = _wfindnext(handle, &fileinfo);
			continue;
		}

		time_t timeMod = 0;
		tm *timeLoc = nullptr;
		TCHAR szBuf[MAX_PATH]{};
		_tcscpy(szBuf, szFullPath);
		_tcsrchr(szBuf, '\\')[1] = '\0';
		_tcscat(szBuf, fileinfo.name);

		if (wordInFileLength) {
			if ((fileinfo.attrib & _A_SUBDIR) || !SearchInFile(szBuf)) {
				result = _wfindnext(handle, &fileinfo);
				continue;
			}
		}

		LV_ITEM lv{};
		lv.iItem = counterFiles++;
		lv.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
		lv.cColumns = COUNT_COL;
		lv.cchTextMax = MAX_PATH;
		lv.pszText = fileinfo.name;
		lv.lParam = lv.iItem;
		SHFILEINFO shFileInfo{};
		SHGetFileInfo(szBuf, 0, &shFileInfo, sizeof(SHFILEINFO), SHGFI_ICON | SHGFI_LARGEICON);
		ImageList_AddIcon(hImgList, shFileInfo.hIcon);
		lv.iImage = lv.iItem;
		DestroyIcon(shFileInfo.hIcon);
		SendMessage(hCtrl[LIST_VIEW], LVM_INSERTITEM, 0, (LPARAM)&lv);
		
		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = szBuf;
		lv.lParam = 0;
		SendMessage(hCtrl[LIST_VIEW], LVM_SETITEM, 0, (LPARAM)&lv);

		if (!(fileinfo.attrib & _A_SUBDIR)) {
			lv.mask = LVIF_TEXT;
			_itot(fileinfo.size, szBuf, 10);
			_tcscat(szBuf, TEXT(" ����"));
			lv.iSubItem = 2;
			lv.pszText = szBuf;
			lv.lParam = 0;
			SendMessage(hCtrl[LIST_VIEW], LVM_SETITEM, 0, (LPARAM)&lv);
		}

		timeMod = fileinfo.time_write;
		timeLoc = localtime(&timeMod);
		lv.mask = LVIF_TEXT;
		if (timeLoc) {
			_tcsftime(szBuf, MAX_PATH, TEXT("%d.%m.%Y %R"), timeLoc);
			lv.iSubItem = 3;
			lv.pszText = szBuf;
			lv.lParam = 0;
		}
		else {
			lv.iSubItem = 3;
			lv.pszText = nullptr;
			lv.lParam = 0;
		}
		SendMessage(hCtrl[LIST_VIEW], LVM_SETITEM, 0, (LPARAM)&lv);
		wsprintf(szBuf, TEXT("%d"), counterFiles);
		SendMessage(hCtrl[STATIC_COUNT_FILES], WM_SETTEXT, 0, (LPARAM)szBuf);
		result = _wfindnext(handle, &fileinfo);
	}
	_findclose(handle);
}

void CDlg::FindFileSubdirs(LPCTSTR lpszDir, LPCTSTR lpszKey) {
	FindFile(lpszDir, lpszKey);
	_wfinddata_t fileinfo;
	intptr_t handle = _wfindfirst(lpszDir, &fileinfo);
	intptr_t result = handle;
	while (result != -1) {
		if (!lstrcmp(fileinfo.name, TEXT(".")) || !lstrcmp(fileinfo.name, TEXT(".."))) {
			result = _wfindnext(handle, &fileinfo);
			continue;
		}
		if (fileinfo.attrib & _A_SUBDIR) {
			if (_tcslen(lpszDir) + _tcslen(fileinfo.name) + 2 >= MAX_PATH) {
				result = _wfindnext(handle, &fileinfo);
				continue;
			}
			TCHAR szBuf[MAX_PATH]{};
			_tcscpy(szBuf, lpszDir);
			szBuf[_tcslen(szBuf) - 1] = '\0';
			_tcscat(szBuf, fileinfo.name);
			_tcscat(szBuf, TEXT("\\*"));
			FindFileSubdirs(szBuf, lpszKey);
		}
		result = _wfindnext(handle, &fileinfo);
	}
	_findclose(handle);
}

int CALLBACK CDlg::CompareListViewItem(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort) {
	TCHAR szFirst[MAX_PATH]{}, szSecond[MAX_PATH]{};
	LV_ITEM lvFirst{}, lvSecond{};
	lvFirst.iSubItem = lvSecond.iSubItem = lParamSort;
	lvFirst.cchTextMax = lvSecond.cchTextMax = MAX_PATH;
	lvFirst.pszText = szFirst;
	lvSecond.pszText = szSecond;
	SendMessage(ptr->hCtrl[LIST_VIEW], LVM_GETITEMTEXT, lParam1, (LPARAM)&lvFirst);
	SendMessage(ptr->hCtrl[LIST_VIEW], LVM_GETITEMTEXT, lParam2, (LPARAM)&lvSecond);
	switch (lParamSort) {
	case COL_NAME:
	case COL_FOLDER:
		return _tcscmp(szFirst, szSecond) * ptr->paramSort[lParamSort];
	case COL_SIZE:
		if (szFirst[0] == '\0' || szSecond[0] == '\0') {
			if (szFirst[0] == '\0' && szSecond != '\0') return ptr->paramSort[lParamSort] * 1;
			if (szFirst[0] != '\0' && szSecond == '\0') return ptr->paramSort[lParamSort] * -1;
			return 0;
		}
		*_tcsstr(szFirst, TEXT(" ����")) = '\0';
		*_tcsstr(szSecond, TEXT(" ����")) = '\0';
		if (_tstoi(szFirst) < _tstoi(szSecond)) return ptr->paramSort[lParamSort] * 1;
		if (_tstoi(szFirst) > _tstoi(szSecond)) return ptr->paramSort[lParamSort] * -1;
		return 0;
	case COL_DATE_MOD: 
	{
		TCHAR szBuf[8]{};
		tm timeFirst{}, timeSecond{};

		_tcsncpy_s(szBuf, szFirst, 2);
		timeFirst.tm_mday = _tstoi(szBuf);
		_tcsncpy_s(szBuf, szSecond, 2);
		timeSecond.tm_mday = _tstoi(szBuf);

		_tcsncpy_s(szBuf, szFirst + 3, 2);
		timeFirst.tm_mon = _tstoi(szBuf) - 1;
		_tcsncpy_s(szBuf, szSecond + 3, 2);
		timeSecond.tm_mon = _tstoi(szBuf) - 1;

		_tcsncpy_s(szBuf, szFirst + 6, 4);
		timeFirst.tm_year = _tstoi(szBuf) - 1900;
		_tcsncpy_s(szBuf, szSecond + 6, 4);
		timeSecond.tm_year = _tstoi(szBuf) - 1900;

		_tcsncpy_s(szBuf, szFirst + 11, 2);
		timeFirst.tm_hour = _tstoi(szBuf);
		_tcsncpy_s(szBuf, szSecond + 11, 2);
		timeSecond.tm_hour = _tstoi(szBuf);

		_tcsncpy_s(szBuf, szFirst + 14, 2);
		timeFirst.tm_min = _tstoi(szBuf);
		_tcsncpy_s(szBuf, szSecond + 14, 2);
		timeSecond.tm_min = _tstoi(szBuf);

		time_t msecFirst = mktime(&timeFirst);
		time_t msecSecond = mktime(&timeSecond);

		if (difftime(msecFirst, msecSecond) < 0) return ptr->paramSort[lParamSort] * 1;
		if (difftime(msecFirst, msecSecond) > 0) return ptr->paramSort[lParamSort] * -1;
		return 0;
	}
	}
}

void CDlg::SetNewIndexOrder() {
	for (int i = 0; i < counterFiles; ++i) {
		LV_ITEM lv{};
		lv.mask = LVIF_PARAM;
		lv.iItem = lv.lParam = i;
		SendMessage(hCtrl[LIST_VIEW], LVM_SETITEM, 0, (LPARAM)&lv);
	}
}

bool CDlg::SearchInFile(LPCTSTR lpszDir) {
	FILE *file = nullptr;
	errno_t error = _tfopen_s(&file, lpszDir, TEXT("rt"));
	if (error) {
		if (file) {
			fclose(file);
		}
		return false;
	}
	int handle = fileno(file);
	size_t size = filelength(handle);
	if (!size) {
		fclose(file);
		return false;
	}
	char *bufFile = new char[size + 1]{};
	LPTSTR bufWord = new TCHAR[wordInFileLength + 1]{};
	fread(bufFile, size, 1, file);
	SendMessage(hCtrl[EDIT_FILE_PART], WM_GETTEXT, wordInFileLength + 1, (LPARAM)bufWord);
	int length = WideCharToMultiByte(CP_ACP, 0, bufWord, -1, nullptr, 0, 0, 0);
	char *bufWordConv = new char[length] {};
	WideCharToMultiByte(CP_ACP, 0, bufWord, -1, bufWordConv, length, 0, 0);
	if (strstr(bufFile, bufWordConv)) {
		delete[] bufFile;
		delete[] bufWord;
		fclose(file);
		return true;
	}
	delete[] bufFile;
	delete[] bufWord;
	fclose(file);
	return false;
}