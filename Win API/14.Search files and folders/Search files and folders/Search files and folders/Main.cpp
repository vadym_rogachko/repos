#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<tchar.h>
#include"resource.h"
#include"CDlg.h"

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow) {
	INITCOMMONCONTROLSEX commctrls = { sizeof(INITCOMMONCONTROLSEX) };
	commctrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&commctrls);
	CDlg& dlg = CDlg::GetRef();
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), nullptr, DLGPROC(CDlg::DlgProc));
}