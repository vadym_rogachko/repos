#pragma once
#include<windows.h>
#include<commctrl.h>

#pragma comment (lib, "comctl32")

class CDlg {

	enum count    { COUNT_CTRL = 8, COUNT_COL = 4 };
	enum cols     { COL_NAME, COL_FOLDER, COL_SIZE, COL_DATE_MOD };
	enum colWidth { WIDTH_NAME = 150, WIDTH_FOLDER = 395, WIDTH_SIZE = 88, WIDTH_DATE_MOD = 155 };
	enum controls { EDIT_FILE, EDIT_FILE_PART, COMBO_DISKS, BTN_FIND, BTN_STOP, CHECK_SUBDIR, LIST_VIEW, STATIC_COUNT_FILES };

	static CDlg* ptr;
	HWND hDlg;
	HWND hCtrl[COUNT_CTRL];
	HANDLE hThread;
	HIMAGELIST hImgList;
	int counterFiles;
	int paramSort[COUNT_COL];
	int wordInFileLength;

	CDlg();
	~CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

	void setCtrlHandles();
	void setListView();
	void setDrives();

	void ExecuteFile(int iItem);
	void SortListView(int iSubItem);
	void FindFile(LPCTSTR lpszDir, LPCTSTR lpszKey);
	void FindFileSubdirs(LPCTSTR lpszDir, LPCTSTR lpszKey);
	static int CALLBACK CompareListViewItem(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	void SetNewIndexOrder();
	bool SearchInFile(LPCTSTR lpszDir);

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();

	friend DWORD WINAPI ThreadFindFilesAndFolders(LPVOID lpPraram);
};

DWORD WINAPI ThreadFindFilesAndFolders(LPVOID lpPraram);