//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Search files and folders.rc
//
#define IDD_DIALOG1                     101
#define IDS_STRING_NAME                 103
#define IDS_STRING_FOLDER               104
#define IDS_STRING_SIZE                 105
#define IDS_STRING_DATE_MOD             106
#define IDC_EDIT_FILE                   1001
#define IDC_EDIT_FILE_PART              1002
#define IDC_COMBO_DISKS                 1003
#define IDC_BUTTON_FIND                 1004
#define IDC_BUTTON_STOP                 1005
#define IDC_CHECK_SUBDIR                1006
#define IDC_LIST_VIEW                   1007
#define IDC_STATIC_FILES_COUNT          1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        107
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1011
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
