//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Menu.rc
//
#define IDD_DIALOG1                     101
#define IDR_MENU_RUS                    103
#define IDR_MENU_ENG                    104
#define IDR_ACCELERATOR1                105
#define IDC_BUTTON1                     1001
#define IDC_BUTTON2                     1002
#define ID_FILE_CREATE                  40000
#define ID_FILE_OPEN                    40001
#define ID_FILE_SAVING                  40002
#define ID_SAVING_SAVE                  40003
#define ID_SAVING_SAVEAS                40004
#define ID_FILE_EXIT                    40005
#define ID_EDIT_CANCEL                  40006
#define ID_Menu                         40007
#define ID_EDIT_CUT                     40008
#define ID_EDIT_COPY                    40009
#define ID_EDIT_PASTE                   40010
#define ID_EDIT_DELETE                  40011
#define ID_EDIT_SEARCH                  40012
#define ID_SEARCH_FIND                  40013
#define ID_SEARCH_FINDNEXT              40014
#define ID_EDIT_REPLACE                 40015
#define ID_EDIT_GOTO                    40016
#define ID_EDIT_SELECTALL               40017
#define ID_FORMAT_WORDWRAP              40018
#define ID_FORMAT_FONT                  40019
#define ID_VIEW_STATUSBAR               40020
#define ID_EDIT_TIMEANDDATE             40021
#define ID_OPEN                         40022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40053
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
