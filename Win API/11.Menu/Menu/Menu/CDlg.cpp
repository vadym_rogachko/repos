#include<windowsx.h>
#include<tchar.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	DestroyWindow(hWnd);
	PostQuitMessage(0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	hMenuRus = LoadMenu(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDR_MENU_RUS));
	hMenuEng = LoadMenu(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDR_MENU_ENG));
	hMenuManual = nullptr;
	SetMenu(hwnd, hMenuEng);
	bEng = true;
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (hwndCtl == hCtrl[BTN1]) {
		ReplaceMenu();
	}
	else if (hwndCtl == hCtrl[BTN2]) {
		CreateAndModifyMenu();
	}
	else if (id >= ID_FILE_CREATE && id <= ID_OPEN) {
		OnAcceleretor(id, codeNotify);
	}
}

void CDlg::setCtrlHandles() {
	for (int i = BTN1; i <= BTN2; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_BUTTON1 + i);
	}
}

void CDlg::ReplaceMenu() {
	SetMenu(hDlg, bEng ? hMenuRus : hMenuEng);
	bEng = !bEng;
}

void CDlg::CreateAndModifyMenu() {
	if (!hMenuManual) {
		bEng = true;
		CreateMenuManual();
	}
	else {
		ModifyMenuManual();
		bEng = !bEng;
	}
	SetMenu(hDlg, hMenuManual);
}

void CDlg::CreateMenuManual() {
	hMenuManual = CreateMenu();
	for (int i = MP_FILE; i <= MP_SEARCH; ++i) {
		hMenuPopup[i] = CreatePopupMenu();
	}
	AppendMenu(hMenuManual, MF_POPUP, (UINT_PTR)hMenuPopup[MP_FILE], TEXT("File"));
	AppendMenu(hMenuManual, MF_POPUP, (UINT_PTR)hMenuPopup[MP_EDIT], TEXT("Edit"));
	AppendMenu(hMenuManual, MF_POPUP, (UINT_PTR)hMenuPopup[MP_FORMAT], TEXT("Format"));
	AppendMenu(hMenuManual, MF_POPUP, (UINT_PTR)hMenuPopup[MP_VIEW], TEXT("View"));

	AppendMenu(hMenuPopup[MP_FILE], MF_STRING, ID_FILE_CREATE, TEXT("Create\tCTRL+N"));
	AppendMenu(hMenuPopup[MP_FILE], MF_STRING, ID_FILE_OPEN, TEXT("Open\tCTRL+O"));
	AppendMenu(hMenuPopup[MP_FILE], MF_POPUP, (UINT_PTR)hMenuPopup[MP_SAVING], TEXT("Saving"));
	AppendMenu(hMenuPopup[MP_FILE], MF_SEPARATOR, 0, nullptr);
	AppendMenu(hMenuPopup[MP_FILE], MF_STRING, ID_FILE_EXIT, TEXT("Exit"));

	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_CANCEL, TEXT("Cancel\tCTRL+Z"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_SEPARATOR, 0, nullptr);
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_CUT, TEXT("Cut\tCTRL+X"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_COPY, TEXT("Copy\tCTRL+C"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_PASTE, TEXT("Paste\tCTRL+V"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_DELETE, TEXT("Delete\tDel"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_SEPARATOR, 0, nullptr);
	AppendMenu(hMenuPopup[MP_EDIT], MF_POPUP, (UINT_PTR)hMenuPopup[MP_SEARCH], TEXT("Search"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_REPLACE, TEXT("Replace\tCTRL+H"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_GOTO, TEXT("Go to\tCTRL+G"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_SEPARATOR, 0, nullptr);
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_SELECTALL, TEXT("Select all\tCTRL+A"));
	AppendMenu(hMenuPopup[MP_EDIT], MF_STRING, ID_EDIT_TIMEANDDATE, TEXT("Time and date\tF5"));

	AppendMenu(hMenuPopup[MP_FORMAT], MF_STRING, ID_FORMAT_WORDWRAP, TEXT("Word wrap"));
	AppendMenu(hMenuPopup[MP_FORMAT], MF_STRING, ID_FORMAT_FONT, TEXT("Font"));

	AppendMenu(hMenuPopup[MP_VIEW], MF_STRING, ID_VIEW_STATUSBAR, TEXT("Status bar"));

	AppendMenu(hMenuPopup[MP_SAVING], MF_STRING, ID_SAVING_SAVE, TEXT("Save\tCTRL+S"));
	AppendMenu(hMenuPopup[MP_SAVING], MF_STRING, ID_SAVING_SAVEAS, TEXT("Save as"));

	AppendMenu(hMenuPopup[MP_SEARCH], MF_STRING, ID_SEARCH_FIND, TEXT("Find\tCTRL+F"));
	AppendMenu(hMenuPopup[MP_SEARCH], MF_STRING, ID_SEARCH_FINDNEXT, TEXT("Find next\tF3"));
}

void CDlg::ModifyMenuManual() {
	ModifyMenu(hMenuManual, 0, MF_BYPOSITION | MF_STRING, UINT_PTR(hMenuPopup[MP_FILE]), bEng ? TEXT("����") : TEXT("File"));
	ModifyMenu(hMenuManual, 1, MF_BYPOSITION | MF_STRING, UINT_PTR(hMenuPopup[MP_EDIT]), bEng ? TEXT("������") : TEXT("Edit"));
	ModifyMenu(hMenuManual, 2, MF_BYPOSITION | MF_STRING, UINT_PTR(hMenuPopup[MP_FORMAT]), bEng ? TEXT("������") : TEXT("Format"));
	ModifyMenu(hMenuManual, 3, MF_BYPOSITION | MF_STRING, UINT_PTR(hMenuPopup[MP_VIEW]), bEng ? TEXT("���") : TEXT("View"));

	ModifyMenu(hMenuPopup[MP_FILE], ID_FILE_CREATE, MF_BYCOMMAND | MF_STRING, ID_FILE_CREATE, bEng ? TEXT("�������\tCTRL+N") : TEXT("Create\tCTRL+N"));
	ModifyMenu(hMenuPopup[MP_FILE], ID_FILE_OPEN, MF_BYCOMMAND | MF_STRING, ID_FILE_OPEN, bEng ? TEXT("�������\tCTRL+O") : TEXT("Open\tCTRL+O"));
	ModifyMenu(hMenuPopup[MP_FILE], ID_FILE_EXIT, MF_BYCOMMAND | MF_STRING, ID_FILE_EXIT, bEng ? TEXT("�����") : TEXT("Exit"));
	ModifyMenu(hMenuPopup[MP_FILE], 2, MF_BYPOSITION | MF_STRING, UINT_PTR(hMenuPopup[MP_SAVING]), bEng ? TEXT("����������") : TEXT("Saving"));

	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_CANCEL, MF_BYCOMMAND | MF_STRING, ID_EDIT_CANCEL, bEng ? TEXT("������\tCTRL+Z") : TEXT("Cancel\tCTRL+Z"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_CUT, MF_BYCOMMAND | MF_STRING, ID_EDIT_CUT, bEng ? TEXT("��������\tCTRL+X") : TEXT("Cut\tCTRL+X"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_COPY, MF_BYCOMMAND | MF_STRING, ID_EDIT_COPY, bEng ? TEXT("����������\tCTRL+C") : TEXT("Copy\tCTRL+C"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_PASTE, MF_BYCOMMAND | MF_STRING, ID_EDIT_PASTE, bEng ? TEXT("��������\tCTRL+V") : TEXT("Paste\tCTRL+V"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_DELETE, MF_BYCOMMAND | MF_STRING, ID_EDIT_DELETE, bEng ? TEXT("�������\tDel") : TEXT("Delete\tDel"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_REPLACE, MF_BYCOMMAND | MF_STRING, ID_EDIT_REPLACE, bEng ? TEXT("��������\tCTRL+H") : TEXT("Replace\tCTRL+H"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_GOTO, MF_BYCOMMAND | MF_STRING, ID_EDIT_GOTO, bEng ? TEXT("�������\tCTRL+G") : TEXT("Go to\tCTRL+G"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_SELECTALL, MF_BYCOMMAND | MF_STRING, ID_EDIT_SELECTALL, bEng ? TEXT("������� ���\tCTRL+A") : TEXT("Select all\tCTRL+A"));
	ModifyMenu(hMenuPopup[MP_EDIT], ID_EDIT_TIMEANDDATE, MF_BYCOMMAND | MF_STRING, ID_EDIT_TIMEANDDATE, bEng ? TEXT("����� � ����\tF5") : TEXT("Time and date\tF5"));
	ModifyMenu(hMenuPopup[MP_EDIT], 7, MF_BYPOSITION | MF_STRING, UINT_PTR(hMenuPopup[MP_SEARCH]), bEng ? TEXT("�����") : TEXT("Search"));

	ModifyMenu(hMenuPopup[MP_FORMAT], ID_FORMAT_WORDWRAP, MF_BYCOMMAND | MF_STRING, ID_FORMAT_WORDWRAP, bEng ? TEXT("������� �� ������") : TEXT("Word wrap"));
	ModifyMenu(hMenuPopup[MP_FORMAT], ID_FORMAT_FONT, MF_BYCOMMAND | MF_STRING, ID_FORMAT_FONT, bEng ? TEXT("�����") : TEXT("Font"));

	ModifyMenu(hMenuPopup[MP_VIEW], ID_VIEW_STATUSBAR, MF_BYCOMMAND | MF_STRING, ID_VIEW_STATUSBAR, bEng ? TEXT("������ ���������") : TEXT("Status bar"));

	ModifyMenu(hMenuPopup[MP_SAVING], ID_SAVING_SAVE, MF_BYCOMMAND | MF_STRING, ID_SAVING_SAVE, bEng ? TEXT("���������\tCTRL+S") : TEXT("Save\tCTRL+S"));
	ModifyMenu(hMenuPopup[MP_SAVING], ID_SAVING_SAVEAS, MF_BYCOMMAND | MF_STRING, ID_SAVING_SAVEAS, bEng ? TEXT("��������� ���") : TEXT("Save as"));

	ModifyMenu(hMenuPopup[MP_SEARCH], ID_SEARCH_FIND, MF_BYCOMMAND | MF_STRING, ID_SEARCH_FIND, bEng ? TEXT("�����\tCTRL+F") : TEXT("Find\tCTRL+F"));
	ModifyMenu(hMenuPopup[MP_SEARCH], ID_SEARCH_FINDNEXT, MF_BYCOMMAND | MF_STRING, ID_SEARCH_FINDNEXT, bEng ? TEXT("����� �����\tF3") : TEXT("Find next\tF3"));
}

void CDlg::OnAcceleretor(int id, UINT codeNotify) {
	TCHAR str1[300], str2[50];
	HMENU hMenu = GetMenu(hDlg);
	GetMenuString(hMenu, id, str2, 50, MF_BYCOMMAND);
	if (codeNotify == 1)
		_tcscpy(str1, TEXT("����� ���� ������ � ������� ������������\n"));
	else if (codeNotify == 0)
		_tcscpy(str1, TEXT("����� ���� ������ ��� ���������������� ��������� � ����\n"));
	_tcscat(str1, str2);
	MessageBox(hDlg, str1, TEXT("���� � ������������"), MB_OK | MB_ICONINFORMATION);
}