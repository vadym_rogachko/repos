#pragma once
#include<windows.h>

class CDlg {

	enum buttons { BTN1, BTN2 };

	enum menuPopup { MP_FILE, MP_EDIT, MP_FORMAT, MP_VIEW, MP_SAVING, MP_SEARCH };

	static CDlg* ptr;
	HWND hDlg;
	HMENU hMenuRus, hMenuEng;
	HWND hCtrl[2];
	HMENU hMenuManual;
	HMENU hMenuPopup[6];
	bool bEng;

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

	void setCtrlHandles();

	void ReplaceMenu();
	void CreateAndModifyMenu();
	void CreateMenuManual();
	void ModifyMenuManual();
	void OnAcceleretor(int id, UINT codeNotify);

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();
};