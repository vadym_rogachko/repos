#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<tchar.h>

/* �������� ����������, ����������� ��� ������� ����� ������ ���� �������� ����� � ��������� ���� ������������ ���������� "�����������" */

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow) {


	TCHAR szClassWindow[] = TEXT("�������� ��������� ������������");
	HWND hWnd;
	MSG msg;
	WNDCLASSEX wndcls;

	wndcls.cbClsExtra = 0;
	wndcls.cbWndExtra = 0;
	wndcls.cbSize = sizeof wndcls;
	wndcls.lpfnWndProc = WindowProc;
	wndcls.hInstance = hInst;
	wndcls.style = CS_HREDRAW | CS_VREDRAW;
	wndcls.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndcls.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndcls.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndcls.lpszMenuName = NULL;
	wndcls.lpszClassName = szClassWindow;
	wndcls.hIconSm = NULL;

	if (!RegisterClassEx(&wndcls)) {
		return 0;
	}

	hWnd = CreateWindowEx(
		0,
		szClassWindow,
		TEXT("������� ����� ������ ����"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInst,
		NULL
	);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	while (!FindWindow(TEXT("CalcFrame"), TEXT("�����������"))) {
		if (IDCANCEL == MessageBox(hWnd, TEXT("�������� ���������� �����������"), TEXT(""), MB_OKCANCEL | MB_ICONEXCLAMATION)) {
			return 0;
		}
	}

	while (GetMessage(&msg, NULL, 0, 0)) {
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) {

	static bool bCalcStdCaption;
	static HWND hWndCalc = FindWindow(TEXT("CalcFrame"), TEXT("�����������"));
	switch (uMessage) {
	case WM_LBUTTONDOWN:
		if (!FindWindow(TEXT("CalcFrame"), bCalcStdCaption ? TEXT("����� ���������") : TEXT("�����������")) || hWndCalc == NULL) {
			while ((hWndCalc = FindWindow(TEXT("CalcFrame"), TEXT("�����������"))) == NULL) {
				if (IDCANCEL == MessageBox(hWnd, TEXT("�������� ���������� �����������"), TEXT(""), MB_OKCANCEL | MB_ICONHAND)) {
					return 0;
				}
			}
		}
		bCalcStdCaption -= 1;
		SetWindowText(hWndCalc, bCalcStdCaption ? TEXT("����� ���������") : TEXT("�����������"));
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}