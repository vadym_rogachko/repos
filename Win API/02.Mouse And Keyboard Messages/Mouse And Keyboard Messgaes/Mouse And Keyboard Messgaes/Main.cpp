#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<tchar.h>

/* �������� ����������, � ������� ������ ������� ���������� "������" �����, ������ � ������� ������ ����. ����������� ���������� ���������� �������� � ��������� ���� */

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow) {


	TCHAR szClassWindow[] = TEXT("������� ������ ����");
	HWND hWnd;
	MSG msg;
	WNDCLASSEX wndcls;

	wndcls.cbClsExtra = 0;
	wndcls.cbWndExtra = 0;
	wndcls.cbSize = sizeof wndcls;
	wndcls.lpfnWndProc = WindowProc;
	wndcls.hInstance = hInst;
	wndcls.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	wndcls.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndcls.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndcls.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndcls.lpszMenuName = NULL;
	wndcls.lpszClassName = szClassWindow;
	wndcls.hIconSm = NULL;

	if (!RegisterClassEx(&wndcls)) {
		return 0;
	}

	hWnd = CreateWindowEx(
		0,
		szClassWindow,
		TEXT("LEFT MOUSE BUTTON = 0 MIDDLE MOUSE BUTTON = 0 RIGHT MOUSE BUTTON = 0"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInst,
		NULL
	);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	while (GetMessage(&msg, NULL, 0, 0)) {
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) {

	TCHAR szCaption[128];
	static UINT uBtnMouseL, uBtnMouseR, uBtnMouseM;
	switch (uMessage) {
	case WM_LBUTTONDBLCLK:
		++uBtnMouseL;
		break;
	case WM_RBUTTONDBLCLK:
		++uBtnMouseR;
		break;
	case WM_MBUTTONDBLCLK:
		++uBtnMouseM;
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	wsprintf(szCaption, TEXT("LEFT MOUSE BUTTON = %d MIDDLE MOUSE BUTTON = %d RIGHT MOUSE BUTTON = %d"), uBtnMouseL, uBtnMouseM, uBtnMouseR);
	SetWindowText(hWnd, szCaption);
	return 0;
}