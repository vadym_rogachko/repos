#pragma once

class CWindowMoving {

	RECT wnd;
	HWND hWnd;

	static const int WIDTH_SCREEN;
	static const int HEIGHT_SCREEN;

protected:

	void UpdateCoord();
	void ToLeft();
	void ToRight();
	void ToUp();
	void ToDown();
	bool ToLeftDiag();
	bool ToRightDiag();
	bool ToUpDiag();
	bool ToDownDiag();

public:

	CWindowMoving(HWND hWnd);
	LONG getLeftCoord() const;
	LONG getRightCoord() const;
	LONG getUpCoord() const;
	LONG getDownCoord() const;
	LONG getWidth() const;
	LONG getHeight() const;
	HWND gethWnd() const;
	void setDefault();
	void MoveLeft();
	void MoveRight();
	void MoveUp();
	void MoveDown();
};