#include<windows.h>
#include"WindowMoving.h"

const int CWindowMoving::WIDTH_SCREEN = GetSystemMetrics(SM_CXSCREEN);
const int CWindowMoving::HEIGHT_SCREEN = GetSystemMetrics(SM_CYSCREEN);

CWindowMoving::CWindowMoving(HWND hWnd) :
	wnd(), hWnd(hWnd) {}

LONG CWindowMoving::getLeftCoord() const {
	return wnd.left;
}

LONG CWindowMoving::getRightCoord() const {
	return wnd.right;
}

LONG CWindowMoving::getUpCoord() const {
	return wnd.top;
}

LONG CWindowMoving::getDownCoord() const {
	return wnd.bottom;
}

LONG CWindowMoving::getWidth() const {
	return wnd.right - wnd.left;
}

LONG CWindowMoving::getHeight() const {
	return wnd.bottom - wnd.top;
}

HWND CWindowMoving::gethWnd() const {
	return hWnd;
}

void CWindowMoving::setDefault() {
	wnd.left = wnd.top = 0;
	wnd.right = wnd.bottom = 300;
}

void CWindowMoving::UpdateCoord() {
	GetWindowRect(hWnd, &wnd);
}

void CWindowMoving::MoveLeft() {
	UpdateCoord();
	ToLeft();
	ToUpDiag() || ToDownDiag();
}

void CWindowMoving::MoveRight() {
	UpdateCoord();
	ToRight();
	ToUpDiag() || ToDownDiag();
}

void CWindowMoving::MoveUp() {
	UpdateCoord();
	ToUp();
	ToLeftDiag() || ToRightDiag();
}

void CWindowMoving::MoveDown() {
	UpdateCoord();
	ToDown();
	ToLeftDiag() || ToRightDiag();
}

void CWindowMoving::ToLeft() {
	if (wnd.right > 0) {
		--wnd.left;
		--wnd.right;
	}
}

void CWindowMoving::ToRight() {
	if (wnd.left < WIDTH_SCREEN) {
		++wnd.left;
		++wnd.right;
	}
}

void CWindowMoving::ToUp() {
	if (wnd.bottom > 0) {
		--wnd.top;
		--wnd.bottom;
	}
}

void CWindowMoving::ToDown() {
	if (wnd.top < HEIGHT_SCREEN) {
		++wnd.top;
		++wnd.bottom;
	}
}

bool CWindowMoving::ToLeftDiag() {
	if ((GetKeyState(VK_LEFT) >> 15) & 1) {
		ToLeft();
		return true;
	}
	return false;
}

bool CWindowMoving::ToRightDiag() {
	if ((GetKeyState(VK_RIGHT) >> 15) & 1) {
		ToRight();
		return true;
	}
	return false;
}

bool CWindowMoving::ToUpDiag() {
	if ((GetKeyState(VK_UP) >> 15) & 1) {
		ToUp();
		return true;
	}
	return false;
}

bool CWindowMoving::ToDownDiag() {
	if ((GetKeyState(VK_DOWN) >> 15) & 1) {
		ToDown();
		return true;
	}
	return false;
}