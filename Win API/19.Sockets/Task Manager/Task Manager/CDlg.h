#pragma once

class CDlg {

	enum controls { BTN_CONNECT, BTN_DISCONNECT, BTN_UPDATE, BTN_END, BTN_CREATE, LIST, EDIT_IP, EDIT_PROC };
	enum { COUNT_CTRLS = 8 };

	static CDlg* ptr;
	HWND hDlg;
	HWND hCtrl[COUNT_CTRLS];
	char path[MAX_PATH];
	WSADATA wsd;
	SOCKET sClient;
	sockaddr_in server;

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

	void MessageAboutError(DWORD dwError);

	void setCtrlHandles();
	void EnableBtns();
	void DisableBtns();

	bool ConnectHost();
	bool DisconnectHost();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();

	friend DWORD WINAPI ThreadConnect(LPVOID lpParam);
	friend DWORD WINAPI ThreadUpdateListProcesses(LPVOID lpParam);
	friend DWORD WINAPI ThreadEndProcess(LPVOID lpParam);
	friend DWORD WINAPI ThreadCreateNewProcess(LPVOID lpParam);
};

DWORD WINAPI ThreadConnect(LPVOID lpParam);
DWORD WINAPI ThreadUpdateListProcesses(LPVOID lpParam);
DWORD WINAPI ThreadEndProcess(LPVOID lpParam);
DWORD WINAPI ThreadCreateNewProcess(LPVOID lpParam);

struct PROCESS {
	TCHAR szExeFile[MAX_PATH];
	DWORD dwProcessID;
};