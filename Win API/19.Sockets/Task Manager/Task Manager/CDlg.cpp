#include<winsock2.h>
#include<windows.h>
#include<windowsx.h>
#include<vector>
#include"CDlg.h"
#include"resource.h"

#pragma comment(lib,"Ws2_32.lib")

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

void CDlg::MessageAboutError(DWORD dwError) {
	LPVOID lpMsgBuf = NULL;
	TCHAR szBuf[300];
	BOOL fOK = FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM
		| FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0,
		NULL
	);
	if (lpMsgBuf != NULL)
	{
		wsprintf(szBuf, TEXT("������ %d: %s"), dwError, lpMsgBuf);
		MessageBox(hDlg, szBuf, TEXT("��������� �� ������"), MB_OK | MB_ICONSTOP);
		LocalFree(lpMsgBuf);
	}
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	shutdown(sClient, SD_BOTH);
	closesocket(sClient);
	WSACleanup();
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (hwndCtl == hCtrl[BTN_CONNECT]) {
		if (!ConnectHost()) return;
		CloseHandle(CreateThread(0, 0, ThreadConnect, this, 0, 0));
	}
	else if (hwndCtl == hCtrl[BTN_DISCONNECT]) {
		DisconnectHost();
	}
	else if (hwndCtl == hCtrl[BTN_END]) {
		CloseHandle(CreateThread(0, 0, ThreadEndProcess, this, 0, 0));
	}
	else if (hwndCtl == hCtrl[BTN_UPDATE]) {
		CloseHandle(CreateThread(0, 0, ThreadUpdateListProcesses, this, 0, 0));
	}
	else if (hwndCtl == hCtrl[BTN_CREATE]) {
		CloseHandle(CreateThread(0, 0, ThreadCreateNewProcess, this, 0, 0));
	}
}

void CDlg::setCtrlHandles() {
	for (int i = 0; i < COUNT_CTRLS; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_BUTTON_CONNECT + i);
	}
}

void CDlg::EnableBtns() {
	EnableWindow(ptr->hCtrl[ptr->BTN_CONNECT], false);
	EnableWindow(ptr->hCtrl[ptr->BTN_DISCONNECT], true);
	EnableWindow(ptr->hCtrl[ptr->BTN_UPDATE], true);
	EnableWindow(ptr->hCtrl[ptr->BTN_END], true);
	EnableWindow(ptr->hCtrl[ptr->BTN_CREATE], true);
}

void CDlg::DisableBtns() {
	EnableWindow(ptr->hCtrl[ptr->BTN_CONNECT], true);
	EnableWindow(ptr->hCtrl[ptr->BTN_DISCONNECT], false);
	EnableWindow(ptr->hCtrl[ptr->BTN_UPDATE], false);
	EnableWindow(ptr->hCtrl[ptr->BTN_END], false);
	EnableWindow(ptr->hCtrl[ptr->BTN_CREATE], false);
}

bool CDlg::ConnectHost() {
	if (WSAStartup(WINSOCK_VERSION, &wsd)) {
		MessageAboutError(WSAGetLastError());
		return false;
	}
	return true;
}

bool CDlg::DisconnectHost() {
	shutdown(ptr->sClient, SD_BOTH);
	closesocket(ptr->sClient);
	WSACleanup();
	ptr->DisableBtns();
	SendMessage(ptr->hCtrl[ptr->LIST], LB_RESETCONTENT, 0, 0);
	return 0;
}

DWORD WINAPI ThreadConnect(LPVOID lpParam) {
	CDlg* ptr = (CDlg*)lpParam;
	ptr->sClient = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ptr->sClient == SOCKET_ERROR) {
		ptr->MessageAboutError(WSAGetLastError());
		WSACleanup();
		return 0;
	}

	ptr->server.sin_family = AF_INET;
	ptr->server.sin_port = htons(49152);
	char ipAddress[32]{};
	GetWindowTextA(ptr->hCtrl[ptr->EDIT_IP], ipAddress, 32);
	ptr->server.sin_addr.S_un.S_addr = inet_addr(ipAddress);

	if (connect(ptr->sClient, (sockaddr*)&ptr->server, sizeof ptr->server) == SOCKET_ERROR) {
		ptr->MessageAboutError(WSAGetLastError());
		closesocket(ptr->sClient);
		WSACleanup();
		return 0;
	}
	CloseHandle(CreateThread(0, 0, ThreadUpdateListProcesses, ptr, 0, 0));
	ptr->EnableBtns();
}

DWORD WINAPI ThreadUpdateListProcesses(LPVOID lpParam) {
	CDlg* ptr = (CDlg*)lpParam;
	SendMessage(ptr->hCtrl[ptr->LIST], LB_RESETCONTENT, 0, 0);
	send(ptr->sClient, "<update>", 9, 0);
	char msg[sizeof PROCESS];
	std::vector<PROCESS> vecProc;
	PROCESS temp;
	int result;

	while (true) {
		result = recv(ptr->sClient, msg, sizeof PROCESS, 0);
		if (result == SOCKET_ERROR || result == 0) {
			ptr->MessageAboutError(WSAGetLastError());
			ptr->DisconnectHost();
			return 0;
		}
		if (strstr(msg, "<acknowledge>") == msg) break;
		if (strstr(msg, "<error>") == msg) {
			MessageBoxA(ptr->hDlg, msg + 7, "��������� �� ������", MB_OK | MB_ICONSTOP);
			return 0;
		}
		memcpy(&temp, msg, sizeof PROCESS);
		vecProc.push_back(temp);
	}

	int index;
	for (PROCESS proc : vecProc) {
		index = SendMessage(ptr->hCtrl[ptr->LIST], LB_ADDSTRING, 0, (LPARAM)proc.szExeFile);
		SendMessage(ptr->hCtrl[ptr->LIST], LB_SETITEMDATA, index, proc.dwProcessID);
	}
	SendMessage(ptr->hCtrl[ptr->LIST], LB_SETCURSEL, 0, 0);
	return 0;
}

DWORD WINAPI ThreadEndProcess(LPVOID lpParam) {
	CDlg* ptr = (CDlg*)lpParam;
	int indexList = SendMessage(ptr->hCtrl[ptr->LIST], LB_GETCURSEL, 0, 0);
	int idProcess = SendMessage(ptr->hCtrl[ptr->LIST], LB_GETITEMDATA, indexList, 0);
	char szIDProc[32]{}, szBuf[64]{};
	itoa(idProcess, szIDProc, 10);
	strcpy(szBuf, "<end>");
	strcat(szBuf, szIDProc);

	send(ptr->sClient, szBuf, strlen(szBuf) + 1, 0);
	char msg[64];
	int result = recv(ptr->sClient, msg, 64, 0);
	if (result == SOCKET_ERROR) {
		ptr->MessageAboutError(WSAGetLastError());
		ptr->DisconnectHost();
		return 0;
	}

	if (strstr(msg, "<acknowledge>") == msg) {
		CloseHandle(CreateThread(0, 0, ThreadUpdateListProcesses, ptr, 0, 0));
	}
	else if (strstr(msg, "<error>") == msg) {
		MessageBoxA(ptr->hDlg, msg + 7, "��������� �� ������", MB_OK | MB_ICONSTOP);
	}
	return 0;
}

DWORD WINAPI ThreadCreateNewProcess(LPVOID lpParam) {
	CDlg* ptr = (CDlg*)lpParam;
	SendMessageA(ptr->hCtrl[ptr->EDIT_PROC], WM_GETTEXT, MAX_PATH, (LPARAM)ptr->path);
	if (!ptr->path[0]) {
		MessageBox(ptr->hDlg, TEXT("������� ��� ��������"), nullptr, MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}
	ptr->path[strlen(ptr->path)] = '\0';
	char szBuf[MAX_PATH + 32]{};
	strcpy(szBuf, "<create>");
	strcat(szBuf, ptr->path);

	send(ptr->sClient, szBuf, strlen(szBuf) + 1, 0);
	char msg[64];
	int result = recv(ptr->sClient, msg, 64, 0);
	if (result == SOCKET_ERROR) {
		ptr->MessageAboutError(WSAGetLastError());
		ptr->DisconnectHost();
		return 0;
	}

	if (strstr(msg, "<acknowledge>") == msg) {
		CloseHandle(CreateThread(0, 0, ThreadUpdateListProcesses, ptr, 0, 0));
	}
	else if (strstr(msg, "<error>") == msg) {
		MessageBoxA(ptr->hDlg, msg + 7, "��������� �� ������", MB_OK | MB_ICONSTOP);
	}
	return 0;
}