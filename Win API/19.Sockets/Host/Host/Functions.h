#pragma once
#include<windows.h>
#include"Header.h"

void MessageAboutError(DWORD dwError);
bool Start(WSADATA &wsd);
DWORD WINAPI ThreadCommunic(LPVOID lpParam);
void UpdateProc(ClientInfo* ptrInfo);
void CreateProc(ClientInfo* ptrInfo, char* szPath);
void EndProc(ClientInfo* ptrInfo, char* szIDProc);