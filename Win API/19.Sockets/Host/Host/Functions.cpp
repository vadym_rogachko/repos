#include<winsock2.h>
#include<windows.h>
#include<TlHelp32.h>
#include<cstdio>
#include"Functions.h"
#include"Header.h"

#pragma comment(lib,"Ws2_32.lib")

void MessageAboutError(DWORD dwError)
{
	if (!dwError) return;
	LPVOID lpMsgBuf = NULL;
	BOOL fOK = FormatMessageA(
		FORMAT_MESSAGE_FROM_SYSTEM
		| FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(char*)&lpMsgBuf,
		0,
		NULL
	);
	if (lpMsgBuf != NULL)
	{
		printf("������ %d: %s\n", dwError, lpMsgBuf);
		LocalFree(lpMsgBuf);
	}
}

bool Start(WSADATA &wsd) {
	if (WSAStartup(WINSOCK_VERSION, &wsd)) {
		MessageAboutError(WSAGetLastError());
		return false;
	}
	return true;
}

DWORD WINAPI ThreadCommunic(LPVOID lpParam) {
	ClientInfo* ptrInfo = (ClientInfo*)lpParam;
	char szBuf[MAX_PATH + 32]{};
	while (true) {
		int result = recv(ptrInfo->socket, szBuf, MAX_PATH + 32, 0);
		if (result == SOCKET_ERROR || result == 0) {
			shutdown(ptrInfo->socket, SD_BOTH);
			closesocket(ptrInfo->socket);
			printf("Disconnected %s\n", inet_ntoa(ptrInfo->addr.sin_addr));
			return 0;
		}

		if (strstr(szBuf, "<update>") == szBuf) {
			UpdateProc(ptrInfo);
		}
		else if (strstr(szBuf, "<create>") == szBuf) {
			CreateProc(ptrInfo, szBuf + 8);
		}
		else if (strstr(szBuf, "<end>") == szBuf) {
			EndProc(ptrInfo, szBuf + 5);
		}
	}
}

void UpdateProc(ClientInfo* ptrInfo) {
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE) {
		send(ptrInfo->socket, "<error>������ ��� ��������� ������ �������", 43, 0);
		CloseHandle(hSnapshot);
		return;
	}

	PROCESSENTRY32 process{ sizeof(PROCESSENTRY32) };
	if (!Process32First(hSnapshot, &process)) {
		send(ptrInfo->socket, "<error>������ ��� �������� ������ ���������", 44, 0);
		CloseHandle(hSnapshot);
		return;
	}

	PROCESS proc;
	char procBuf[sizeof PROCESS];
	do {
		proc.dwProcessID = process.th32ProcessID;
		lstrcpy(proc.szExeFile, process.szExeFile);
		memcpy(procBuf, &proc, sizeof PROCESS);
		send(ptrInfo->socket, procBuf, sizeof PROCESS, 0);
	} while (Process32Next(hSnapshot, &process));
	CloseHandle(hSnapshot);
	send(ptrInfo->socket, "<acknowledge>", 14, 0);
}

void CreateProc(ClientInfo* ptrInfo, char* szPath) {
	STARTUPINFOA startInfo = { sizeof(STARTUPINFO) };
	PROCESS_INFORMATION processInfo;
	if (!CreateProcessA(nullptr, szPath, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &startInfo, &processInfo)) {
		send(ptrInfo->socket, "<error>������ ��� �������� ��������", 36, 0);
		return;
	}
	send(ptrInfo->socket, "<acknowledge>", 14, 0);
}

void EndProc(ClientInfo* ptrInfo, char* szIDProc) {
	int idProcess = atoi(szIDProc);
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, idProcess);
	if (!TerminateProcess(hProcess, 0)) {
		send(ptrInfo->socket, "<error>������ ��� ���������� ��������", 38, 0);
		return;
	}
	send(ptrInfo->socket, "<acknowledge>", 14, 0);
}