#pragma once
#include<windows.h>

struct ClientInfo
{
	sockaddr_in addr;
	SOCKET socket;
};

struct PROCESS {
	TCHAR szExeFile[MAX_PATH];
	DWORD dwProcessID;
};