#include"Header.h"
#include"Functions.h"
#include<iostream>
#include<windows.h>

void main() {

	setlocale(LC_ALL, "rus");

	WSADATA wsd;
	if (!Start(wsd)) {
		return;
	}

	SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == SOCKET_ERROR) {
		MessageAboutError(WSAGetLastError());
		WSACleanup();
		return;
	}

	sockaddr_in local;
	local.sin_addr.S_un.S_addr = INADDR_ANY;
	local.sin_family = AF_INET;
	local.sin_port = htons(49152);

	int res = bind(s, (sockaddr*)&local, sizeof(local));
	if (res == SOCKET_ERROR) {
		MessageAboutError(WSAGetLastError());
		closesocket(s);
		WSACleanup();
		std::cin.get();
		return;
	}

	res = listen(s, 10);
	if (res == SOCKET_ERROR) {
		MessageAboutError(WSAGetLastError());
		closesocket(s);
		WSACleanup();
		std::cin.get();
		return;
	}
	printf("Server online\n");

	while (true) {
		int iAddrSize = sizeof sockaddr_in;
		ClientInfo clientInfo;
		clientInfo.socket = accept(s, (sockaddr*)&clientInfo.addr, &iAddrSize);
		if (clientInfo.socket == INVALID_SOCKET) {
			MessageAboutError(WSAGetLastError());
			std::cin.get();
			return;
		}
		printf("Connected %s\n", inet_ntoa(clientInfo.addr.sin_addr));
		CloseHandle(CreateThread(0, 0, ThreadCommunic, &clientInfo, 0, 0));
	}
}