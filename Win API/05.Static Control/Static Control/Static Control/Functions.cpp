#include<windows.h>
#include<iostream>
#include<tchar.h>
#include<vector>
#include"Header.h"

using std::vector;
using std::swap;

BOOL CALLBACK DlgProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) {
	static HINSTANCE hInst = GetModuleHandle(0);
	static StaticCtrl staticCtrl;
	static TCHAR szStaticNumber[8];
	static TCHAR szStaticInfo[128];
	static vector<StaticCtrl> vecRect;
	static vector<StaticCtrl>::reverse_iterator itRev;
	static vector<StaticCtrl>::iterator itFwd;
	static int counter;

	switch (uMessage) {

	case WM_CLOSE:
		EndDialog(hWnd, 0);
		return TRUE;

	case WM_LBUTTONDOWN:
		staticCtrl.rect.left = LOWORD(lParam);
		staticCtrl.rect.top = HIWORD(lParam);
		return TRUE;

	case WM_LBUTTONUP:
		staticCtrl.rect.right = LOWORD(lParam);
		staticCtrl.rect.bottom = HIWORD(lParam);
		NormalizeStaticRect(staticCtrl.rect);
		if (staticCtrl.rect.right - staticCtrl.rect.left < 10 || staticCtrl.rect.bottom - staticCtrl.rect.top < 10) {
			if (staticCtrl.rect.right - staticCtrl.rect.left == 0 || staticCtrl.rect.bottom - staticCtrl.rect.top == 0) return TRUE;
			MessageBox(hWnd, TEXT("����������� ������ �������������� ���������� 10x10"), nullptr, MB_OK | MB_ICONEXCLAMATION);
			return TRUE;
		}
		wsprintf(szStaticNumber, TEXT("%d"), ++counter);
		staticCtrl.hWnd = CreateWindowEx(
			0,
			TEXT("STATIC"),
			szStaticNumber,
			WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER | WS_EX_CLIENTEDGE,
			staticCtrl.rect.left,
			staticCtrl.rect.top,
			staticCtrl.rect.right - staticCtrl.rect.left,
			staticCtrl.rect.bottom - staticCtrl.rect.top,
			hWnd,
			nullptr,
			hInst,
			0
		);
		vecRect.push_back(staticCtrl);
		return TRUE;

	case WM_RBUTTONDOWN:
		for (itRev = vecRect.rbegin(); itRev != vecRect.rend(); ++itRev) {
			if (isStaticWnd(LOWORD(lParam), HIWORD(lParam), itRev->rect)) break;
		}
		if (itRev == vecRect.rend()) {
			SetWindowText(hWnd, TEXT("Static Control"));
			return TRUE;
		}
		GetWindowText(itRev->hWnd, szStaticNumber, sizeof szStaticNumber / sizeof szStaticNumber[0]);
		wsprintf(
			szStaticInfo, TEXT("�%s WIDTH = %d HEIGHT = %d LEFT = %d TOP = %d RIGHT = %d BOTTOM = %d"),
			szStaticNumber,
			itRev->rect.right - itRev->rect.left,
			itRev->rect.bottom - itRev->rect.top,
			itRev->rect.left,
			itRev->rect.top,
			itRev->rect.right,
			itRev->rect.bottom
		);
		SetWindowText(hWnd, szStaticInfo);
		return TRUE;

	case WM_LBUTTONDBLCLK:
		for (itFwd = vecRect.begin(); itFwd != vecRect.end(); ++itFwd) {
			if (isStaticWnd(LOWORD(lParam), HIWORD(lParam), itFwd->rect)) break;
		}
		if (itFwd == vecRect.end()) return TRUE;
		DestroyWindow(itFwd->hWnd);
		vecRect.erase(itFwd);
		SetWindowText(hWnd, TEXT("Static Control"));
		if (vecRect.size() == 0) counter = 0;
		return TRUE;
	}
	return FALSE;
}

BOOL isStaticWnd(LONG x, LONG y, const RECT& rect) {
	return x >= rect.left && x <= rect.right &&
		y >= rect.top && y <= rect.bottom;
}

VOID NormalizeStaticRect(RECT& rect) {
	if (rect.left > rect.right) {
		swap(rect.left, rect.right);
	}
	if (rect.top > rect.bottom) {
		swap(rect.top, rect.bottom);
	}
}