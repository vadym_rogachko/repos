#pragma once

struct StaticCtrl {
	RECT rect;
	HWND hWnd;
};

BOOL CALLBACK DlgProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
BOOL isStaticWnd(LONG x, LONG y, const RECT& rect);
VOID NormalizeStaticRect(RECT& rect);