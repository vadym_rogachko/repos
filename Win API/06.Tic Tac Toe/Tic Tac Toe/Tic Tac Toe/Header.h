#pragma once

enum state { EMPTY, CROSS = 10, ZERO = 100 };
enum difficulty { EASY, MEDIUM, HARD };

enum line
{
	HORIZ1, HORIZ2, HORIZ3,
	VERT1, VERT2, VERT3,
	DIAG1, DIAG2, NO_LINE
};

BOOL CALLBACK DlgProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam);
void EnableButtons(HWND hWnd, int nIDFirstButton, int nIDLastButton);
void DisableButtons(HWND hWnd, int nIDFirstButton, int nIDLastButton);
void StartGame(HWND hWnd);
void StopGame(HWND hWnd, line lineWin);
line FinishLine(state field[]);
int CompMove(state field[], difficulty level);
int MoveRand(state field[]);
int MoveGood(state field[], state key);
int MovePerfect(state field[]);