#include<windows.h>
#include<iostream>
#include<tchar.h>
#include"Header.h"
#include"resource.h"

BOOL CALLBACK DlgProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) {

	static state field[9]{ EMPTY };
	static HBITMAP cross = LoadBitmap(GetModuleHandle(0), MAKEINTRESOURCE(IDB_CROSS));
	static HBITMAP zero = LoadBitmap(GetModuleHandle(0), MAKEINTRESOURCE(IDB_ZERO));
	static int counter;
	static line lineWin = NO_LINE;
	static difficulty level = EASY;
	static int moveCross, moveZero;
	static int winPlayer, winComp;
	static TCHAR szScore[8]{};

	switch (uMessage) {

	case WM_CLOSE:
		EndDialog(hWnd, 0);
		return TRUE;

	case WM_INITDIALOG:
		CheckDlgButton(hWnd, IDC_CHECK1, BST_CHECKED);
		CheckDlgButton(hWnd, IDC_RADIO_EASY, BST_CHECKED);
		SetClassLong(hWnd, GCL_HICON, LONG(LoadIcon(nullptr, IDI_APPLICATION)));
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) >= IDC_BUTTON1 && LOWORD(wParam) <= IDC_BUTTON9) {
			if (!SendDlgItemMessage(hWnd, LOWORD(wParam), BM_GETIMAGE, 0, 0)) {
				moveCross = LOWORD(wParam) - IDC_BUTTON1;
				SendDlgItemMessage(hWnd, moveCross + IDC_BUTTON1, BM_SETIMAGE, IMAGE_BITMAP, LPARAM(cross));
				field[moveCross] = CROSS;
				++counter;
				if (counter == 9) {
					lineWin = FinishLine(field);
					StopGame(hWnd, lineWin);
					if (lineWin != NO_LINE) {
						wsprintf(szScore, TEXT("%d - %d"), ++winPlayer, winComp);
						SetDlgItemText(hWnd, IDC_STATIC1, szScore);
					}
					return TRUE;
				}
				if ((lineWin = FinishLine(field)) != NO_LINE) {
					StopGame(hWnd, lineWin);
					wsprintf(szScore, TEXT("%d - %d"), ++winPlayer, winComp);
					SetDlgItemText(hWnd, IDC_STATIC1, szScore);
					return TRUE;
				}
				moveZero = CompMove(field, level);
				SendDlgItemMessage(hWnd, moveZero + IDC_BUTTON1, BM_SETIMAGE, IMAGE_BITMAP, LPARAM(zero));
				field[moveZero] = ZERO;
				++counter;
				if (counter == 9) {
					lineWin = FinishLine(field);
					StopGame(hWnd, lineWin);
					if (lineWin != NO_LINE) {
						wsprintf(szScore, TEXT("%d - %d"), winPlayer, ++winComp);
						SetDlgItemText(hWnd, IDC_STATIC1, szScore);
					}
					return TRUE;
				}
				if ((lineWin = FinishLine(field)) != NO_LINE) {
					StopGame(hWnd, lineWin);
					wsprintf(szScore, TEXT("%d - %d"), winPlayer, ++winComp);
					SetDlgItemText(hWnd, IDC_STATIC1, szScore);
					return TRUE;
				}
			}
		}
		else if (LOWORD(wParam) == IDC_BUTTON10) {
			{
				int i = IDC_BUTTON1;
				while (i <= IDC_BUTTON9) {
					SendDlgItemMessage(hWnd, i, BM_SETIMAGE, 0, 0);
					++i;
				}
			}
			counter = 0;
			StartGame(hWnd);
			for (int i = 0; i < 9; ++i) {
				field[i] = EMPTY;
			}
			level = IsDlgButtonChecked(hWnd, IDC_RADIO_EASY)   == BST_CHECKED ? EASY
				  : IsDlgButtonChecked(hWnd, IDC_RADIO_MEDIUM) == BST_CHECKED ? MEDIUM
				                                                              : HARD;
			if (IsDlgButtonChecked(hWnd, IDC_CHECK1) != BST_CHECKED) {
				moveZero = CompMove(field, level);
				SendDlgItemMessage(hWnd, moveZero + IDC_BUTTON1, BM_SETIMAGE, IMAGE_BITMAP, LPARAM(zero));
				field[moveZero] = ZERO;
				++counter;
			}
		}
		return TRUE;
	}
	return FALSE;
}

void EnableButtons(HWND hWnd, int nIDFirstButton, int nIDLastButton) {
	for (int i = nIDFirstButton; i <= nIDLastButton; ++i) {
		EnableWindow(GetDlgItem(hWnd, i), TRUE);
	}
}

void DisableButtons(HWND hWnd, int nIDFirstButton, int nIDLastButton) {
	for (int i = nIDFirstButton; i <= nIDLastButton; ++i) {
		EnableWindow(GetDlgItem(hWnd, i), FALSE);
	}
}

void StartGame(HWND hWnd) {
	EnableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON9);
	DisableButtons(hWnd, IDC_CHECK1, IDC_RADIO_HARD);
}

void StopGame(HWND hWnd, line lineWin) {
	EnableButtons(hWnd, IDC_CHECK1, IDC_RADIO_HARD);
	switch (lineWin) {
	case NO_LINE:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON9);
		break;
	case HORIZ1:
		DisableButtons(hWnd, IDC_BUTTON4, IDC_BUTTON9);
		break;
	case HORIZ2:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON3);
		DisableButtons(hWnd, IDC_BUTTON7, IDC_BUTTON9);
		break;
	case HORIZ3:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON6);
		break;
	case VERT1:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON9);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON1), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON4), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON7), TRUE);
		break;
	case VERT2:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON9);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON2), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON5), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON8), TRUE);
		break;
	case VERT3:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON9);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON3), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON6), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON9), TRUE);
		break;
	case DIAG1:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON9);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON1), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON5), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON9), TRUE);
		break;
	case DIAG2:
		DisableButtons(hWnd, IDC_BUTTON1, IDC_BUTTON9);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON3), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON5), TRUE);
		EnableWindow(GetDlgItem(hWnd, IDC_BUTTON7), TRUE);
		break;
	}
}

line FinishLine(state field[]) {
	if (field[0] != EMPTY) {
		if (field[0] == field[1] && field[0] == field[2]) return HORIZ1;
		if (field[0] == field[3] && field[0] == field[6]) return VERT1;
		if (field[0] == field[4] && field[0] == field[8]) return DIAG1;
	}
	if (field[4] != EMPTY) {
		if (field[3] == field[4] && field[3] == field[5]) return HORIZ2;
		if (field[1] == field[4] && field[1] == field[7]) return VERT2;
		if (field[2] == field[4] && field[2] == field[6]) return DIAG2;
	}
	if (field[2] != EMPTY) {
		if (field[2] == field[5] && field[2] == field[8]) return VERT3;
	}
	if (field[6] != EMPTY) {
		if (field[6] == field[7] && field[6] == field[8]) return HORIZ3;
	}
	return NO_LINE;
}

int CompMove(state field[], difficulty level) {
	int move;
	if (level == EASY) return MoveRand(field);
	if ((move = MoveGood(field, ZERO)) != -1) return move;
	if ((move = MoveGood(field, CROSS)) != -1) return move;
	if (field[4] == EMPTY) return 4;
	if (level == HARD) {
		if ((move = MovePerfect(field)) != -1) return move;
	}
	return MoveRand(field);
}

int MoveRand(state field[]) {
	int move;
	while (true) {
		move = rand() % 9;
		if (field[move] == EMPTY) {
			return move;
		}
	}
}

int MoveGood(state field[], state key) {
	if (field[0] + field[1] + field[2] == key + key) {
		return field[0] == EMPTY ? 0
			 : field[1] == EMPTY ? 1
			 : 2;
	}
	if (field[3] + field[4] + field[5] == key + key) {
		return field[3] == EMPTY ? 3
			 : field[4] == EMPTY ? 4
			 : 5;
	}
	if (field[6] + field[7] + field[8] == key + key) {
		return field[6] == EMPTY ? 6
			 : field[7] == EMPTY ? 7
			 : 8;
	}
	if (field[0] + field[3] + field[6] == key + key) {
		return field[0] == EMPTY ? 0
			 : field[3] == EMPTY ? 3
			 : 6;
	}
	if (field[1] + field[4] + field[7] == key + key) {
		return field[1] == EMPTY ? 1
			 : field[4] == EMPTY ? 4
			 : 7;
	}
	if (field[2] + field[5] + field[8] == key + key) {
		return field[2] == EMPTY ? 2
			 : field[5] == EMPTY ? 5
			 : 8;
	}
	if (field[0] + field[4] + field[8] == key + key) {
		return field[0] == EMPTY ? 0
			 : field[4] == EMPTY ? 4
			 : 8;
	}
	if (field[2] + field[4] + field[6] == key + key) {
		return field[2] == EMPTY ? 2
			 : field[4] == EMPTY ? 4
			 : 6;
	}
	return -1;
}

int MovePerfect(state field[]) {
	if (field[1] == EMPTY) {
		if (field[0] == ZERO && field[2] == EMPTY) return 2;
		if (field[0] == EMPTY && field[2] == ZERO) return 0;
	}
	if (field[3] == EMPTY) {
		if (field[0] == ZERO && field[6] == EMPTY) return 6;
		if (field[0] == EMPTY && field[6] == ZERO) return 0;
	}
	if (field[5] == EMPTY) {
		if (field[8] == ZERO && field[2] == EMPTY) return 2;
		if (field[8] == EMPTY && field[2] == ZERO) return 8;
	}
	if (field[7] == EMPTY) {
		if (field[8] == ZERO && field[6] == EMPTY) return 6;
		if (field[8] == EMPTY && field[6] == ZERO) return 8;
	}
	if (field[7] == CROSS) {
		if (field[0] == CROSS && field[6] == EMPTY) return 6;
		if (field[2] == CROSS && field[8] == EMPTY) return 8;
	}
	if (field[5] == CROSS && field[6] == CROSS && field[8] == EMPTY) return 8;
	if (field[3] == CROSS && field[8] == CROSS && field[6] == EMPTY) return 6;
	if (field[0] == EMPTY && field[8] == EMPTY) return 0;
	if (field[2] == EMPTY && field[6] == EMPTY) return 2;
	return field[0] == EMPTY ? 0
		 : field[2] == EMPTY ? 2
		 : field[6] == EMPTY ? 6
		 : field[8] == EMPTY ? 8 
		 : -1;
}