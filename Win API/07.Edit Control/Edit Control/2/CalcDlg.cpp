#include<windowsx.h>
#include<cstdio>
#include"CalcDlg.h"
#include"resource1.h"


CalcDlg* CalcDlg::ptr = nullptr;

CalcDlg::CalcDlg() {
	ptr = this;
}

CalcDlg CalcDlg::GetRef() {
	static CalcDlg obj;
	return obj;
}

void CalcDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CalcDlg::Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam) {
	for (int i = SIGN; i <= RES; ++i) {
		hCtrl[i] = GetDlgItem(hWnd, IDC_SIGN + i);
	}
	return TRUE;
}

void CalcDlg::Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify) {
	if (id == IDC_SIGN && !CheckSign()) return;
	if (codeNotify == EN_CHANGE) {
		SendMessage(hCtrl[RES], WM_SETTEXT, 0, 0);
	}
	if (id == IDC_EQUAL && !Calc()) return;
}

BOOL CALLBACK CalcDlg::DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hWnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hWnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hWnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

bool CalcDlg::CheckSign() {
	static TCHAR szSign[2];
	if (SendMessage(hCtrl[SIGN], WM_GETTEXTLENGTH, 0, 0) == 1) {
		SendMessage(hCtrl[SIGN], WM_GETTEXT, 2, (LPARAM) szSign);
		if (szSign[0] == '+' || szSign[0] == '-' || szSign[0] == '*' || szSign[0] == '/') return false;
		szSign[0] = '\0';
		SendMessage(hCtrl[SIGN], WM_SETTEXT, 0, (LPARAM) szSign);
		SendMessage(hCtrl[SIGN], EM_SETSEL, 0, 0);
		MessageBeep(MB_OK);
		return false;
	}
	if (SendMessage(hCtrl[SIGN], WM_GETTEXTLENGTH, 0, 0) > 1) {
		SendMessage(hCtrl[SIGN], WM_GETTEXT, 2, (LPARAM)szSign);
		SendMessage(hCtrl[SIGN], WM_SETTEXT, 0, (LPARAM)szSign);
		SendMessage(hCtrl[SIGN], EM_SETSEL, 1, 1);
		MessageBeep(MB_OK);
		return false;
	}
	return true;
}

bool CalcDlg::Calc() {
	static TCHAR szNum1[64], szNum2[64], szRes[64], szSign[2];
	SendMessage(hCtrl[SIGN], WM_GETTEXT, 2, (LPARAM)szSign);
	SendMessage(hCtrl[NUM1], WM_GETTEXT, 64, (LPARAM)szNum1);
	SendMessage(hCtrl[NUM2], WM_GETTEXT, 64, (LPARAM)szNum2);
	if (szNum1[0] == '\0' || szNum2[0] == '\0' || szSign[0] == '\0') {
		MessageBeep(MB_OK);
		return false;
	}
	TCHAR *szEnd;
	double num1 = wcstod(szNum1, &szEnd);
	double num2 = wcstod(szNum2, &szEnd);
	switch (szSign[0]) {
	case '+':
		_snwprintf_s(szRes, 64, TEXT("%.1f"), num1 + num2);
		break;
	case '-':
		_snwprintf_s(szRes, 64, TEXT("%.1f"), num1 - num2);
		break;
	case '*':
		_snwprintf_s(szRes, 64, TEXT("%.1f"), num1 * num2);
		break;
	case '/':
		_snwprintf_s(szRes, 64, TEXT("%.1f"), num1 / num2);
		break;
	}
	SendMessage(hCtrl[RES], WM_SETTEXT, 0, (LPARAM) szRes);
	return true;
}