#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<tchar.h>
#include"resource1.h"
#include"CalcDlg.h"

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow) {
	CalcDlg& dlg = CalcDlg::GetRef();
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), nullptr, DLGPROC(CalcDlg::DlgProc));
}