#pragma once
#include<windows.h>

class CalcDlg {

	enum { SIGN, NUM1, NUM2, EQUAL, RES };

	static CalcDlg* ptr;
	HWND hCtrl[5];

	CalcDlg();
	void Cls_OnClose(HWND hWnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify);

	bool CheckSign();
	bool Calc();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CalcDlg GetRef();
};