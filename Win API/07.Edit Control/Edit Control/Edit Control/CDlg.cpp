#include<windowsx.h>
#include<ctime>
#include<tchar.h>
#include"CDlg.h"
#include"resource1.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam) {
	for (int i = DAY; i <= WEEK_DAY; ++i) {
		hCtrl[i] = GetDlgItem(hWnd, IDC_EDIT1 + i);
	}
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify) {
	if ((id == IDC_EDIT1 || id == IDC_EDIT2) && !CheckDayAndMonth(hWndCtl)) return;
	if (id == IDC_EDIT3 && !CheckYear(hWndCtl)) return;
	if (codeNotify == EN_CHANGE && hWndCtl != hCtrl[WEEK_DAY]) {
		if (SendMessage(hCtrl[DAY],   WM_GETTEXTLENGTH, 0, 0) > 0 &&
			SendMessage(hCtrl[MONTH], WM_GETTEXTLENGTH, 0, 0) > 0 &&
			SendMessage(hCtrl[YEAR],  WM_GETTEXTLENGTH, 0, 0) > 3)
		{
			CalcWeekDay();
		}
		else
		{
			SendMessage(hCtrl[WEEK_DAY], WM_SETTEXT, 0, 0);
		}
	}
}

BOOL CALLBACK CDlg::DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hWnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hWnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hWnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::CalcWeekDay() {
	static time_t msec;
	static tm date;
	static TCHAR szBuf[8];
	static int day, month, year;
	SendMessage(hCtrl[DAY], WM_GETTEXT, 3, (LPARAM) szBuf);
	day = _tstoi(szBuf);
	SendMessage(hCtrl[MONTH], WM_GETTEXT, 3, (LPARAM) szBuf);
	month = _tstoi(szBuf);
	SendMessage(hCtrl[YEAR], WM_GETTEXT, 5, (LPARAM) szBuf);
	year = _tstoi(szBuf);
	date.tm_mday = day;
	date.tm_mon = month - 1;
	date.tm_year = year - 1900;
	msec = mktime(&date);
	if (msec != -1) {
		date = *localtime(&msec);
		if (date.tm_mday != day || date.tm_mon != month - 1 || date.tm_year != year - 1900) {
			MessageBox(GetParent(hCtrl[DAY]), TEXT("������������ ����"), nullptr, MB_OK | MB_ICONEXCLAMATION);
			for (int i = DAY; i <= WEEK_DAY; ++i) {
				SendMessage(hCtrl[i], WM_SETTEXT, 0, 0);
			}
			SetFocus(hCtrl[DAY]);
			return;
		}
		else {
			SendMessage(hCtrl[WEEK_DAY], WM_SETTEXT, 0, 
				LPARAM(date.tm_wday == 0 ? TEXT("����������")  :
					   date.tm_wday == 1 ? TEXT("�����������") :
					   date.tm_wday == 2 ? TEXT("�������")	   :
					   date.tm_wday == 3 ? TEXT("�����")	   :
					   date.tm_wday == 4 ? TEXT("�������")	   :
					   date.tm_wday == 5 ? TEXT("�������")	   :
										   TEXT("�������") ))  ;
		}
	}
}

bool CDlg::CheckDayAndMonth(HWND hWndCtl) {
	static TCHAR szBuf[8];
	if (SendMessage(hWndCtl, WM_GETTEXTLENGTH, 0, 0) > 2) {
		SendMessage(hWndCtl, WM_GETTEXT, 3, (LPARAM) szBuf);
		SendMessage(hWndCtl, WM_SETTEXT, 0, (LPARAM) szBuf);
		SendMessage(hWndCtl, EM_SETSEL, 2, 2);
		MessageBeep(MB_OK);
		return false;
	}
	return true;
}

bool CDlg::CheckYear(HWND hWndCtl) {
	static TCHAR szBuf[8];
	if (SendMessage(hWndCtl, WM_GETTEXTLENGTH, 0, 0) > 4) {
		SendMessage(hWndCtl, WM_GETTEXT, 5, (LPARAM) szBuf);
		SendMessage(hWndCtl, WM_SETTEXT, 0, (LPARAM) szBuf);
		SendMessage(hWndCtl, EM_SETSEL, 4, 4);
		MessageBeep(MB_OK);
		return false;
	}
	return true;
}