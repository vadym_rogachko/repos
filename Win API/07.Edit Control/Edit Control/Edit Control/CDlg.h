#pragma once
#include<windows.h>

class CDlg {

	enum { DAY, MONTH, YEAR, WEEK_DAY };

	static CDlg* ptr;
	HWND hCtrl[4];

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify);

	bool CheckDayAndMonth(HWND hWndCtl);
	bool CheckYear(HWND hWndCtl);
	void CalcWeekDay();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg GetRef();
};