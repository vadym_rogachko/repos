#pragma once
#include<windows.h>

class CDlg {

	enum { DAY, MONTH, YEAR, YEARS, MONTHS, DAYS, HOURS, MINS, SECS, CHECK_HOURS, CHECK_MINS, CHECK_SECS };

	static CDlg* ptr;
	HWND hCtrl[12];

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify);

	bool CheckDayAndMonth(HWND hCtl);
	bool CheckYear(HWND hCtl);
	void CalcTime();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg GetRef();
};