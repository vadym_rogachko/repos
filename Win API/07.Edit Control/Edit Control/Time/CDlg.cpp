#include<windowsx.h>
#include<ctime>
#include<tchar.h>
#include"CDlg.h"
#include"resource.h"


CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam) {
	for (int i = DAY; i <= CHECK_SECS; ++i) {
		hCtrl[i] = GetDlgItem(hWnd, IDC_EDIT1 + i);
	}
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify) {
	if ((id == IDC_EDIT1 || id == IDC_EDIT2) && !CheckDayAndMonth(hWndCtl)) return;
	if ( id == IDC_EDIT3 && !CheckYear(hWndCtl)) return;
	if (codeNotify == EN_CHANGE &&
	   (hWndCtl == hCtrl[DAY]         || hWndCtl == hCtrl[MONTH]      || hWndCtl == hCtrl[YEAR])       ||
		hWndCtl == hCtrl[CHECK_HOURS] || hWndCtl == hCtrl[CHECK_MINS] || hWndCtl == hCtrl[CHECK_SECS]) {

		if (SendMessage(hCtrl[DAY],   WM_GETTEXTLENGTH, 0, 0) > 0 &&
			SendMessage(hCtrl[MONTH], WM_GETTEXTLENGTH, 0, 0) > 0 &&
			SendMessage(hCtrl[YEAR],  WM_GETTEXTLENGTH, 0, 0) > 3)
		{
			CalcTime();
		}
		else {
			for (int i = YEARS; i <= SECS; ++i) {
				SendMessage(hCtrl[i], WM_SETTEXT, 0, 0);
			}
		}
	}
	
}

BOOL CALLBACK CDlg::DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hWnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hWnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hWnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::CalcTime() {
	static TCHAR szBuf[8];
	static time_t msec, dif;
	static time_t todayMsec;
	tm date{};
	static int day, month, year;
	SendMessage(hCtrl[DAY], WM_GETTEXT, 3, (LPARAM) szBuf);
	day = _tstoi(szBuf);
	SendMessage(hCtrl[MONTH], WM_GETTEXT, 3, (LPARAM) szBuf);
	month = _tstoi(szBuf);
	SendMessage(hCtrl[YEAR], WM_GETTEXT, 5, (LPARAM) szBuf);
	year = _tstoi(szBuf);
	date.tm_mday = day;
	date.tm_mon = month - 1;
	date.tm_year = year - 1900;
	msec = mktime(&date);
	if (msec != -1) {
		date = *localtime(&msec);
		if (date.tm_mday != day || date.tm_mon != month - 1 || date.tm_year != year - 1900) {
			MessageBox(GetParent(hCtrl[DAY]), TEXT("������������ ����"), nullptr, MB_OK | MB_ICONEXCLAMATION);
			for (int i = 0; i < 9; ++i) {
				SendMessage(hCtrl[i], WM_SETTEXT, 0, 0);
			}
			SetFocus(hCtrl[DAY]);
			return;
		}
		time(&todayMsec);
		dif = msec - todayMsec;
		if (localtime(&dif)) {
			date = *localtime(&dif);
		}
		else {
			date.tm_mday = date.tm_mon = date.tm_hour = date.tm_min = date.tm_sec = 0;
			date.tm_year = 70;
		}

		wsprintf(szBuf, TEXT("%d"), date.tm_year - 70);
		SendMessage(hCtrl[YEARS], WM_SETTEXT, 0, (LPARAM) szBuf);

		wsprintf(szBuf, TEXT("%d"), date.tm_mon);
		SendMessage(hCtrl[MONTHS], WM_SETTEXT, 0, (LPARAM) szBuf);

		wsprintf(szBuf, TEXT("%d"), date.tm_mday);
		SendMessage(hCtrl[DAYS], WM_SETTEXT, 0, (LPARAM) szBuf);

		if (SendMessage(hCtrl[CHECK_HOURS], BM_GETCHECK, 0, 0) == BST_CHECKED) {
			wsprintf(szBuf, TEXT("%d"), date.tm_hour);
			SendMessage(hCtrl[HOURS], WM_SETTEXT, 0, (LPARAM) szBuf);
		}
		else {
			SendMessage(hCtrl[HOURS], WM_SETTEXT, 0, 0);
		}

		if (SendMessage(hCtrl[CHECK_MINS], BM_GETCHECK, 0, 0) == BST_CHECKED) {
			wsprintf(szBuf, TEXT("%d"), date.tm_min);
			SendMessage(hCtrl[MINS], WM_SETTEXT, 0, (LPARAM) szBuf);
		}
		else {
			SendMessage(hCtrl[MINS], WM_SETTEXT, 0, 0);
		}

		if (SendMessage(hCtrl[CHECK_SECS], BM_GETCHECK, 0, 0) == BST_CHECKED) {
			wsprintf(szBuf, TEXT("%d"), date.tm_sec);
			SendMessage(hCtrl[SECS], WM_SETTEXT, 0, (LPARAM) szBuf);
		}
		else {
			SendMessage(hCtrl[SECS], WM_SETTEXT, 0, 0);
		}
	}
}

bool CDlg::CheckDayAndMonth(HWND hWndCtl) {
	static TCHAR szBuf[8];
	if (SendMessage(hWndCtl, WM_GETTEXTLENGTH, 0, 0) > 2) {
		SendMessage(hWndCtl, WM_GETTEXT, 3, (LPARAM) szBuf);
		SendMessage(hWndCtl, WM_SETTEXT, 0, (LPARAM) szBuf);
		SendMessage(hWndCtl, EM_SETSEL, 2, 2);
		MessageBeep(MB_OK);
		return false;
	}
	return true;
}

bool CDlg::CheckYear(HWND hWndCtl) {
	static TCHAR szBuf[8];
	if (SendMessage(hWndCtl, WM_GETTEXTLENGTH, 0, 0) > 4) {
		SendMessage(hWndCtl, WM_GETTEXT, 5, (LPARAM) szBuf);
		SendMessage(hWndCtl, WM_SETTEXT, 0, (LPARAM) szBuf);
		SendMessage(hWndCtl, EM_SETSEL, 4, 4);
		MessageBeep(MB_OK);
		return false;
	}
	return true;
}