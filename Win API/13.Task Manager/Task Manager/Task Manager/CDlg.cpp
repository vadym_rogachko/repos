#include<windows.h>
#include<TlHelp32.h>
#include<windowsx.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	UpdateListProcesses();
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (hwndCtl == hCtrl[BTN_END]) {
		EndProcess();
	}
	else if (hwndCtl == hCtrl[BTN_UPDATE]) {
		UpdateListProcesses();
	}
	else if (hwndCtl == hCtrl[BTN_CREATE]) {
		CreateNewProcess();
	}
}

void CDlg::setCtrlHandles() {
	for (int i = BTN_UPDATE; i <= EDIT; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_BUTTON_UPDATE + i);
	}
}

void CDlg::UpdateListProcesses() {
	SendMessage(hCtrl[LIST], LB_RESETCONTENT, 0, 0);
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot == INVALID_HANDLE_VALUE) {
		MessageBox(hDlg, TEXT("������ ��� ��������� ������ �������"), nullptr, MB_OK | MB_ICONERROR);
		CloseHandle(hSnapshot);
		return;
	}
	PROCESSENTRY32 process{ sizeof(PROCESSENTRY32) };
	if (!Process32First(hSnapshot, &process)) {
		MessageBox(hDlg, TEXT("������ ��� �������� ������ ���������"), nullptr, MB_OK | MB_ICONERROR);
		CloseHandle(hSnapshot);
		return;
	}
	int index;
	do {
		index = SendMessage(hCtrl[LIST], LB_ADDSTRING, 0, (LPARAM)process.szExeFile);
		SendMessage(hCtrl[LIST], LB_SETITEMDATA, index, process.th32ProcessID);
	} while (Process32Next(hSnapshot, &process));
	CloseHandle(hSnapshot);
	SendMessage(hCtrl[LIST], LB_SETCURSEL, 0, 0);
}

void CDlg::EndProcess() {
	int indexList = SendMessage(hCtrl[LIST], LB_GETCURSEL, 0, 0);
	int idProcess = SendMessage(hCtrl[LIST], LB_GETITEMDATA, indexList, 0);
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, idProcess);
	if (!TerminateProcess(hProcess, 0)) {
		MessageBox(hDlg, TEXT("������ ��� ���������� ��������"), nullptr, MB_OK | MB_ICONERROR);
		return;
	}
	SendMessage(hCtrl[LIST], LB_SETCURSEL, 0, 0);
	SendMessage(hCtrl[LIST], LB_DELETESTRING, indexList, 0);
}

void CDlg::CreateNewProcess() {
	SendMessage(hCtrl[EDIT], WM_GETTEXT, MAX_PATH, (LPARAM)path);
	if (path[0] == '\0') {
		MessageBox(hDlg, TEXT("������� ��� ��������"), nullptr, MB_OK | MB_ICONEXCLAMATION);
		return;
	}
	STARTUPINFO startInfo = { sizeof(STARTUPINFO) };
	PROCESS_INFORMATION processInfo;
	if (!CreateProcess(nullptr, path, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &startInfo, &processInfo)) {
		MessageBox(hDlg, TEXT("������ ��� �������� ��������"), nullptr, MB_OK | MB_ICONERROR);
		return;
	}
	UpdateListProcesses();
}