#pragma once
#include<windows.h>

class CDlg {

	enum controls { BTN_UPDATE, BTN_END, BTN_CREATE, LIST, EDIT };

	static CDlg* ptr;
	HWND hDlg;
	HWND hCtrl[5];
	TCHAR path[MAX_PATH];

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

	void setCtrlHandles();

	void UpdateListProcesses();
	void EndProcess();
	void CreateNewProcess();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();
};