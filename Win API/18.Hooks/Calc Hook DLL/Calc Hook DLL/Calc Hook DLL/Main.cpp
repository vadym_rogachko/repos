#include<windows.h>
#define __MYDLL__
#include"Calc Hook DLL.h"

HHOOK hhMouse, hhKeyb;
HINSTANCE hInstance;
LRESULT CALLBACK HookMouseProc(int nCode, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK HookKeybProc(int nCode, WPARAM wParam, LPARAM lParam);

BOOL WINAPI DllMain(HINSTANCE hInstDLL, DWORD fdwReason, LPVOID lpvReserved) {
	if (fdwReason == DLL_PROCESS_ATTACH) {
		hInstance = hInstDLL;
	}
	return TRUE;
}

bool InstallHooks() {
	HWND hCalc = FindWindow(TEXT("CalcFrame"), nullptr);
	if (!hCalc) {
		return false;
	}
	DWORD calcThreadID = GetWindowThreadProcessId(hCalc, nullptr);
	hhMouse = SetWindowsHookEx(WH_MOUSE, HookMouseProc, hInstance, calcThreadID);
	hhKeyb = SetWindowsHookEx(WH_KEYBOARD, HookKeybProc, hInstance, calcThreadID);
	return hhMouse && hhKeyb;
}

bool RemoveHooks() {
	return UnhookWindowsHookEx(hhMouse) &&
		UnhookWindowsHookEx(hhKeyb);
}

LRESULT CALLBACK HookMouseProc(int idCode, WPARAM wParam, LPARAM lParam) {
	if (idCode == HC_ACTION && wParam == WM_LBUTTONDOWN) {
		MOUSEHOOKSTRUCT *ms = (MOUSEHOOKSTRUCT*)lParam;
		int id = GetDlgCtrlID(ms->hwnd);
		if (id >= 130 && id <= 139) return 1;
	}
	return CallNextHookEx(hhMouse, idCode, wParam, lParam);
}

LRESULT CALLBACK HookKeybProc(int idCode, WPARAM wParam, LPARAM lParam) {
	if (idCode == HC_ACTION) {
		if ((wParam >= 0x30 && wParam <= 0x39) || (wParam >= 0x60 && wParam <= 0x69)) {
			if (!(GetKeyState(VK_SHIFT) & 0x80000)) return 1;
		}
	}
	return CallNextHookEx(hhKeyb, idCode, wParam, lParam);
}