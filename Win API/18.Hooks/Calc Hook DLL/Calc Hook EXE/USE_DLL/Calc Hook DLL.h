#pragma once
#ifdef __MYDLL__
#define __DLLFUNC__ __declspec(dllexport)
#else 
#define __DLLFUNC__ __declspec(dllimport)
#endif

extern "C" {
	__DLLFUNC__ bool InstallHooks();
	__DLLFUNC__ bool RemoveHooks();
}