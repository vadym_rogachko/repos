#include "MyDialog.h"
#include"resource.h"

CMyDialog* CMyDialog::ptr = NULL;

CMyDialog::CMyDialog()
{
	ptr = this;
}

void CMyDialog::OnInitDialog(HWND h)
{
	hDialog = h;
	hButton1 = GetDlgItem(hDialog, IDC_BUTTON1);
	hButton2 = GetDlgItem(hDialog, IDC_BUTTON2);
}

void CMyDialog::OnCommand(WPARAM wp, LPARAM lp)
{
	if (LOWORD(wp) == IDC_BUTTON1)
	{
		if (InstallHooks()) {
			EnableWindow(hButton1, 0);
			EnableWindow(hButton2, 1);
		}
		else {
			MessageBox(hDialog, TEXT("��� �� ��� ����������!!!"), TEXT("HOOK"), MB_OK | MB_ICONSTOP);
		}
	}
	else if (LOWORD(wp) == IDC_BUTTON2)
	{
		if (RemoveHooks()) {
			EnableWindow(hButton1, 1);
			EnableWindow(hButton2, 0);
		}
		else {
			MessageBox(hDialog, TEXT("��� �� ��� ������!!!"), TEXT("HOOK"), MB_OK | MB_ICONSTOP);
		}
	}
}

BOOL CALLBACK CMyDialog::DlgProc(HWND hWnd, UINT mes, WPARAM wp, LPARAM lp)
{
	switch (mes)
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0);
		break;
	case WM_INITDIALOG:
		ptr->OnInitDialog(hWnd);
		break;
	case WM_COMMAND:
		ptr->OnCommand(wp, lp);
		break;
	}
	return 0;
}