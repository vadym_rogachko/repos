#define __MYDLL__
#include "WH_HOOK.h"
#include "stdio.h"

HHOOK hHook; 
HINSTANCE hMod;
HWND hWnd;

LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam);
bool installhook (HWND hWnd)
{
	::hWnd = hWnd;
	return hHook = SetWindowsHookEx(WH_MOUSE, MouseProc, hMod, 0);
}
bool removehook ()
{
	hWnd = 0;
	return UnhookWindowsHookEx(hHook);
}

LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION && wParam == WM_MOUSEMOVE)
	{
		SendMessage(hWnd, WM_APP, wParam, lParam);
	}
	return CallNextHookEx(hHook, nCode, wParam, lParam);
}

BOOL APIENTRY DllMain( HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		hMod = (HINSTANCE) hModule;
	}
	return TRUE;
}
