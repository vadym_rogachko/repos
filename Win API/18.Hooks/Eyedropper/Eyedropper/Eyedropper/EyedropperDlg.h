#pragma once
#include "header.h"
#include<windows.h>
#include"resource.h"

#include "WH_HOOK.h"
#pragma comment (lib, "Global_HOOK_WH_MOUSE.lib")

class CEyedropperDlg
{
public:
	CEyedropperDlg(void);
	~CEyedropperDlg();
public:
	static BOOL CALLBACK DlgProc(HWND hWnd, UINT mes, WPARAM wp, LPARAM lp);
	static CEyedropperDlg* ptr;
	BOOL Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg);
	void Cls_OnMouseMove(HWND hwnd, WPARAM wParam, LPARAM lParam);
	void Cls_OnPaint(HWND hwnd);
	HICON hIcon;
	HCURSOR hCursor;
	COLORREF color;
	RECT TextRect;
	RECT ColorRect;
	HDC hDC, hMemoryDC;
	RECT rect;
	COLORREF CurrentColor;
	HBITMAP hBmp;
	HPEN hPen;
	TCHAR text[200];
};
