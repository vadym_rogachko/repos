#include "EyedropperDlg.h"

CEyedropperDlg* CEyedropperDlg::ptr = NULL;

CEyedropperDlg::CEyedropperDlg(void)
{
	ptr = this;
}

CEyedropperDlg::~CEyedropperDlg()
{
	// ����������� �������
	DeleteObject(hPen);
	DeleteObject(hCursor);
	DeleteObject(hIcon);
}

void CEyedropperDlg::Cls_OnClose(HWND hwnd)
{
	// ��������� ����������� �������� ���������� � ������
	DeleteDC(hMemoryDC); 
	// ��������� �������� ���������� ����
	ReleaseDC(hwnd, hDC);
	if (!removehook())
	{
		MessageBox(hwnd, TEXT("��� �� ��� ������!!!"), TEXT("HOOK"), MB_OK | MB_ICONSTOP);
	}
	// ��������� ������
	EndDialog(hwnd, 0); 
}

BOOL CEyedropperDlg::Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam) 
{
	// ��������� ������ �� �������� ����������
	hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1)); 
	// ������������� ������
	SetClassLong(hwnd, GCL_HICON, LONG(hIcon)); 
	// ��������� ������ �� �������� ����������
	hCursor = LoadCursor(GetModuleHandle(NULL), MAKEINTRESOURCE(IDC_CURSOR1));
	// ������� �������������, ������������ ���������� ������� �������
	GetClientRect(hwnd, &rect);
	ColorRect = TextRect = rect;
	ColorRect.left = (rect.right - rect.left) / 2;
	TextRect.right = (rect.right - rect.left) / 3;
	hPen = CreatePen(PS_NULL, 1, 0); // ���������� ����
	// ������� �������� ���������� ��� ����, � ������� ����� ��������� �����������
	hDC = GetDC(hwnd);
	// ������� ����������� �������� ���������� ������
	hMemoryDC = CreateCompatibleDC(hDC);
	// �������� � ������ ��������� �����������, ����������� � �������� ���������� ����������
	hBmp = CreateCompatibleBitmap(hDC, rect.right, rect.bottom);
	// ������� ��������� ����������� � �������� ���������� ������
	SelectObject(hMemoryDC, hBmp);
	// �������� ����� ������ ��������� �����������, ��������� � �������� ������
	PatBlt(hMemoryDC, 0, 0, rect.right, rect.bottom, WHITENESS);
	// ������� � �������� ������ ����������� ��������� �����
	SelectObject(hMemoryDC, GetStockObject(DC_BRUSH));
	// ������� ���������� ���� � �������� ���������� ������
	SelectObject(hMemoryDC, hPen);
	if (!installhook(hwnd))
	{
		MessageBox(hwnd, TEXT("��� �� ��� ����������!!!"), TEXT("HOOK"), MB_OK | MB_ICONSTOP);
	}
	return TRUE;
}

void CEyedropperDlg::Cls_OnMouseMove(HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	MOUSEHOOKSTRUCT *ms = (MOUSEHOOKSTRUCT*)lParam;
	static COLORREF prevcolor;
	// ������� �������� ���������� ��� ����� ������
	HDC hDesktopDC = GetDC(NULL);
	// ������� ���� ������� ��� ��������
	CurrentColor = GetPixel(hDesktopDC, ms->pt.x, ms->pt.y);
	// ��������� �������� ����������
	ReleaseDC(NULL, hDesktopDC);
	// ��������, ���������� �� ���� �������� ������� �� ����� �����������
	if(CurrentColor != prevcolor) 
	{
		// ��������� ������ ��� ������������ ������ �� �����
		wsprintf(text, TEXT("\n  R: %d\n  G: %d\n  B: %d\n\n  # %.2X%.2X%.2X"),
			GetRValue(CurrentColor), GetGValue(CurrentColor), GetBValue(CurrentColor),
			GetRValue(CurrentColor), GetGValue(CurrentColor), GetBValue(CurrentColor));
		// �������� ����� ������ ��������� �����������, ��������� � �������� ������
		PatBlt(hMemoryDC, 0, 0, rect.right, rect.bottom, WHITENESS);
		// ��������� ���� �����, ��������� � �������� ����������
		SetDCBrushColor(hMemoryDC, CurrentColor);
		// ������ �������������, ������� ����� �������� ������, ��������������� ������� ��� �������� 
		Rectangle(hMemoryDC, ColorRect.left, ColorRect.top, ColorRect.right, ColorRect.bottom);
		// ������� �����, ���������� �������� ������ ������������ ��� �������� �����
		DrawText(hMemoryDC, text, lstrlen(text), &TextRect, DT_CENTER);
		// �������� ���������� ��������� WM_PAINT
		InvalidateRect(hwnd, NULL, TRUE);
	}
	// ���������� ������� ����
	prevcolor = CurrentColor;
}

BOOL CEyedropperDlg::Cls_OnSetCursor(HWND hwnd, HWND hwndCursor, UINT codeHitTest, UINT msg)
{
	// ��������� ������, ����������� �� �������� ����������
	SetCursor(hCursor);
	return TRUE;
}

void CEyedropperDlg::Cls_OnPaint(HWND hwnd)
{
	PAINTSTRUCT paintstruct;
	// ������� �������� ����������, ��������������� � �������� �����
	HDC hFormDC = BeginPaint(hwnd, &paintstruct);
	// ��������� ����������� �� ��������� ���������� ������ � �������� ���������� ���� 
	BitBlt(hFormDC, 0, 0, rect.right, rect.bottom, hMemoryDC, 0, 0, SRCCOPY);
	// ����������� �������� ����������
	EndPaint(hwnd, &paintstruct);
}

BOOL CALLBACK CEyedropperDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_SETCURSOR, ptr->Cls_OnSetCursor);
		HANDLE_MSG(hwnd, WM_PAINT, ptr->Cls_OnPaint);
	case WM_APP:
		ptr->Cls_OnMouseMove(hwnd, wParam, lParam);
		break;
	}
	return FALSE;
}