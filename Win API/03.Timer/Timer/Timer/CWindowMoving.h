#pragma once

class CWindowMoving {

	RECT wnd;
	HWND hWnd;

	static const int WIDTH_SCREEN;
	static const int HEIGHT_SCREEN;

protected:

	void UpdateCoord();
	void ToLeft();
	void ToRight();
	void ToUp();
	void ToDown();

public:

	CWindowMoving(HWND hWnd);
	static int getScreenWidth();
	static int getScreenHeight();
	LONG getLeftCoord() const;
	LONG getRightCoord() const;
	LONG getUpCoord() const;
	LONG getDownCoord() const;
	LONG getWidth() const;
	LONG getHeight() const;
	HWND gethWnd() const;
	virtual void setDefault();
	virtual void MoveLeft();
	virtual void MoveRight();
	virtual void MoveUp();
	virtual void MoveDown();
};