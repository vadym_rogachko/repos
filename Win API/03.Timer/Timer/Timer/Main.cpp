#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<tchar.h>
#include<ctime>
#include"CWindowMoving.h"

/* �������� ����������, ���������� ��������� �����������������:
   - ��� ������� ������� <Enter> ������� ���� ��������������� � ����� ������� ���� ������ � ��������� (300�300) � �������� ������������ �� ��������� ������ � ����������� ���������;
   - ��� ������� ������� <Esc> ����������� ���� ������������ */

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);

INT WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow) {

	srand(time(NULL));
	TCHAR szClassWindow[] = TEXT("��������");
	HWND hWnd;
	MSG msg;
	WNDCLASSEX wndcls;

	wndcls.cbClsExtra = 0;
	wndcls.cbWndExtra = 0;
	wndcls.cbSize = sizeof wndcls;
	wndcls.lpfnWndProc = WindowProc;
	wndcls.hInstance = hInst;
	wndcls.style = CS_HREDRAW | CS_VREDRAW;
	wndcls.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndcls.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndcls.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndcls.lpszMenuName = NULL;
	wndcls.lpszClassName = szClassWindow;
	wndcls.hIconSm = NULL;

	if (!RegisterClassEx(&wndcls)) {
		return 0;
	}

	hWnd = CreateWindowEx(
		0,
		szClassWindow,
		TEXT(""),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInst,
		NULL
	);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	while (GetMessage(&msg, NULL, 0, 0)) {
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMessage, WPARAM wParam, LPARAM lParam) {

	enum direction { DOWN, RIGHT, UP, LEFT };
	static CWindowMoving wnd(hWnd);
	static direction direction;
	switch (uMessage) {
	case WM_KEYDOWN:
		switch (wParam) {
		case VK_RETURN:
			wnd.setDefault();
			MoveWindow(hWnd, wnd.getLeftCoord(), wnd.getUpCoord(), wnd.getWidth(), wnd.getHeight(), false);
			SetTimer(hWnd, 1, 10, NULL);
			break;
		case VK_ESCAPE:
			KillTimer(hWnd, 1);
			break;
		default:
			return DefWindowProc(hWnd, uMessage, wParam, lParam);
		}
		break;
	case WM_TIMER:
		if (wnd.getLeftCoord() == 0 && wnd.getUpCoord() == 0) {
			direction = DOWN;
		}
		else if (wnd.getLeftCoord() == 0 && wnd.getDownCoord() == CWindowMoving::getScreenHeight()) {
			direction = RIGHT;
		}
		if (wnd.getRightCoord() == CWindowMoving::getScreenWidth() && wnd.getDownCoord() == CWindowMoving::getScreenHeight()) {
			direction = UP;
		}
		else if (wnd.getRightCoord() == CWindowMoving::getScreenWidth() && wnd.getUpCoord() == 0) {
			direction = LEFT;
		}
		switch (direction) {
		case DOWN:
			wnd.MoveDown();
			break;
		case RIGHT:
			wnd.MoveRight();
			break;
		case UP:
			wnd.MoveUp();
			break;
		case LEFT:
			wnd.MoveLeft();
			break;
		}
		MoveWindow(hWnd, wnd.getLeftCoord(), wnd.getUpCoord(), wnd.getWidth(), wnd.getHeight(), false);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, uMessage, wParam, lParam);
	}
	return 0;
}