#include<windows.h>
#include"CWindowMoving.h"

const int CWindowMoving::WIDTH_SCREEN = GetSystemMetrics(SM_CXSCREEN);
const int CWindowMoving::HEIGHT_SCREEN = GetSystemMetrics(SM_CYSCREEN);

CWindowMoving::CWindowMoving(HWND hWnd) :
	wnd(), hWnd(hWnd) {}

int CWindowMoving::getScreenWidth() {
	return WIDTH_SCREEN;
}

int CWindowMoving::getScreenHeight() {
	return HEIGHT_SCREEN;
}

LONG CWindowMoving::getLeftCoord() const {
	return wnd.left;
}

LONG CWindowMoving::getRightCoord() const {
	return wnd.right;
}

LONG CWindowMoving::getUpCoord() const {
	return wnd.top;
}

LONG CWindowMoving::getDownCoord() const {
	return wnd.bottom;
}

LONG CWindowMoving::getWidth() const {
	return wnd.right - wnd.left;
}

LONG CWindowMoving::getHeight() const {
	return wnd.bottom - wnd.top;
}

HWND CWindowMoving::gethWnd() const {
	return hWnd;
}

void CWindowMoving::setDefault() {
	wnd.left = wnd.top = 0;
	wnd.right = wnd.bottom = 300;
}

void CWindowMoving::UpdateCoord() {
	GetWindowRect(hWnd, &wnd);
}

void CWindowMoving::MoveLeft() {
	UpdateCoord();
	ToLeft();
}

void CWindowMoving::MoveRight() {
	UpdateCoord();
	ToRight();
}

void CWindowMoving::MoveUp() {
	UpdateCoord();
	ToUp();
}

void CWindowMoving::MoveDown() {
	UpdateCoord();
	ToDown();
}

void CWindowMoving::ToLeft() {
	if (wnd.right > 0) {
		--wnd.left;
		--wnd.right;
	}
}

void CWindowMoving::ToRight() {
	if (wnd.left < WIDTH_SCREEN) {
		++wnd.left;
		++wnd.right;
	}
}

void CWindowMoving::ToUp() {
	if (wnd.bottom > 0) {
		--wnd.top;
		--wnd.bottom;
	}
}

void CWindowMoving::ToDown() {
	if (wnd.top < HEIGHT_SCREEN) {
		++wnd.top;
		++wnd.bottom;
	}
}