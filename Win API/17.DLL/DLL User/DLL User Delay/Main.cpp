#include<windows.h>
#include<iostream>
#include<delayimp.h>
#include"Header.h"
#pragma comment(lib, "DLL.lib")
#pragma comment(lib, "delayimp.lib")

void main() {

	int size = 10;
	int *arr = Malloc(size);
	InitArray(arr, size, 1024);
	ShowArray(arr, size);
	AddToArrayEnd(arr, size, 777);
	ShowArray(arr, size);
	PasteToArrayInd(arr, size, 888, 3);
	ShowArray(arr, size);
	DelElemInArrayInd(arr, size, 3);
	ShowArray(arr, size);
	Free(arr);
	std::cin.get();
	__FUnloadDelayLoadedDLL2("DLL.dll");
}