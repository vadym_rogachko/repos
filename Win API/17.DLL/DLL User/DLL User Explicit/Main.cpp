#include<windows.h>
#include<iostream>

void main() {

	setlocale(LC_ALL, "rus");

	HMODULE hMod = LoadLibrary(TEXT("DLL.dll"));
	if (!hMod)
	{
		std::cout << "������ ��� �������� dll\n";
		std::cin.get();
		return;
	}
	int* (*Malloc)(int) = (int* (*)(int))GetProcAddress(hMod, "Malloc");
	void (*InitArray)(int*, int, int) = (void(*)(int*, int, int))GetProcAddress(hMod, "InitArray");
	void (*ShowArray)(int*, int) = (void (*)(int*, int))GetProcAddress(hMod, "ShowArray");
	void (*Free)(int*) = (void (*)(int*))GetProcAddress(hMod, "Free");
	void (*AddToArrayEnd)(int*&, int&, int) = (void (*)(int*&, int&, int))GetProcAddress(hMod, "AddToArrayEnd");
	void (*PasteToArrayInd)(int*&, int&, int, int) = (void (*)(int*&, int&, int, int))GetProcAddress(hMod, "PasteToArrayInd");
	void (*DelElemInArrayInd)(int*&, int&, int) = (void (*)(int*&, int&, int))GetProcAddress(hMod, "DelElemInArrayInd");
	if (!Malloc || !InitArray || !ShowArray || !Free || !AddToArrayEnd || !PasteToArrayInd || !DelElemInArrayInd) {
		std::cout << "������ ��� �������� ������� �� dll\n";
		FreeLibrary(hMod);
		std::cin.get();
		return;
	}

	int size = 10;
	int *arr = Malloc(size);
	InitArray(arr, size, 1024);
	ShowArray(arr, size);
	AddToArrayEnd(arr, size, 777);
	ShowArray(arr, size);
	PasteToArrayInd(arr, size, 888, 3);
	ShowArray(arr, size);
	DelElemInArrayInd(arr, size, 3);
	ShowArray(arr, size);
	Free(arr);
	FreeLibrary(hMod);
	std::cin.get();
}