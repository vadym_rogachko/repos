#include<iostream>
#define __MYDLL__
#include"Header.h"

/*a. ������� ������������� ������������ ������*/

int* Malloc(int size) {
	if (size <= 0) return nullptr;
	return new int[size]{};
}

/*b. ������� ������������� ������������� �������*/

void InitArray(int *arr, int size, int val) {
	if (size <= 0) return;
	for (int i = 0; i < size; ++i) {
		arr[i] = val;
	}
}

/* c. ������� ������ ������������� �������*/

void ShowArray(int *arr, int size) {
	if (size <= 0) return;
	for (int i = 0; i < size; ++i) {
		std::cout << arr[i] << ' ';
	}
	std::cout << std::endl;
}

/* d. ������� �������� ������������� �������*/

void Free(int *arr) {
	delete[] arr;
	arr = nullptr;
}

/*e. ������� ���������� �������� � ����� �������*/

void AddToArrayEnd(int *&arr, int &size, int val) {
	if (size <= 0) return;
	int *arrTemp = new int[size + 1];
	arrTemp[size] = val;
	for (int i = 0; i < size; ++i) {
		arrTemp[i] = arr[i];
	}
	size++;
	delete[] arr;
	arr = arrTemp;
}

/*f. ������� ������� �������� �� ���������� �������*/

void PasteToArrayInd(int *&arr, int &size, int val, int index) {
	if (size <= 0) return;
	if (index >= size || index < 0) return;
	size++;
	int *arrTemp = new int[size];
	arrTemp[index] = val;
	for (int i = 0; i < index; ++i) {
		arrTemp[i] = arr[i];
	}
	for (int i = index + 1; i < size; ++i) {
		arrTemp[i] = arr[i - 1];
	}
	delete[] arr;
	arr = arrTemp;
}

/*g. ������� �������� �������� �� ���������� �������*/

void DelElemInArrayInd(int *&arr, int &size, int index) {
	if (size <= 0) return;
	if (index >= size || index < 0) return;
	size--;
	int *arrTemp = new int[size];
	for (int i = 0; i < index; ++i) {
		arrTemp[i] = arr[i];
	}
	for (int i = index; i < size; ++i) {
		arrTemp[i] = arr[i + 1];
	}
	delete[] arr;
	arr = arrTemp;
}