#pragma once
#ifdef __MYDLL__
#define MY_DLL_FUNC __declspec(dllexport)
#else
#define MY_DLL_FUNC __declspec(dllimport)
#endif
extern "C" {
	MY_DLL_FUNC int* Malloc(int size);
	MY_DLL_FUNC void InitArray(int *arr, int size, int val);
	MY_DLL_FUNC void ShowArray(int *arr, int size);
	MY_DLL_FUNC void Free(int *arr);
	MY_DLL_FUNC void AddToArrayEnd(int *&arr, int &size, int val);
	MY_DLL_FUNC void PasteToArrayInd(int *&arr, int &size, int val, int index);
	MY_DLL_FUNC void DelElemInArrayInd(int *&arr, int &size, int index);
}