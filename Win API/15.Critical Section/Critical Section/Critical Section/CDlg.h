#pragma once
#include<windows.h>

class CDlg {

	enum count { COUNT_CTRLS = 2, COUNT_FILE_COPIES = 1000 };
	enum controls { EDIT, BTN };

	static CDlg* ptr;
	HWND hDlg;
	HWND hCtrl[COUNT_CTRLS];

	CDlg();
	~CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

	void setCtrlHandles();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();

};