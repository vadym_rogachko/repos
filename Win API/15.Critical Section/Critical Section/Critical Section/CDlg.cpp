#include<windowsx.h>
#include<tchar.h>
#include<fstream>
#include<io.h>
#include"CDlg.h"
#include"resource.h"

const int COUNT_FILES = 1000;
CRITICAL_SECTION arrCritSec[COUNT_FILES];

DWORD WINAPI WriteToFiles(CONST LPVOID lpParam);
DWORD WINAPI ReadFromFiles(CONST LPVOID lpParam);
void MakeFileName(LPTSTR lpszFullPath, LPCTSTR lpszDir, int index);

void MessageAboutError(DWORD dwError) {
	LPVOID lpMsgBuf = NULL;
	TCHAR szBuf[300];
	BOOL fOK = FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	if (lpMsgBuf != NULL) {
		wsprintf(szBuf, TEXT("������ %d: %s"), dwError, lpMsgBuf);
		MessageBox(0, szBuf, TEXT("��������� �� ������"), MB_OK | MB_ICONSTOP);
		LocalFree(lpMsgBuf);
	}
}

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg::~CDlg() {
	for (int i = 0; i < COUNT_FILES; ++i) {
		DeleteCriticalSection(&arrCritSec[i]);
	}
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (hwndCtl == hCtrl[BTN]) {
		TCHAR szFileName[MAX_PATH]{};
		SendMessage(hCtrl[EDIT], WM_GETTEXT, MAX_PATH, (LPARAM)szFileName);
		if (!_tcslen(szFileName)) {
			MessageBox(hDlg, TEXT("������� ��� �����"), TEXT(""), MB_OK | MB_ICONEXCLAMATION);
			return;
		}
		CloseHandle(CreateThread(nullptr, 0, WriteToFiles, szFileName, 0, nullptr));
	}
}

void CDlg::setCtrlHandles() {
	for (int i = 0; i < COUNT_CTRLS; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_EDIT1 + i);
	}
}

DWORD WINAPI WriteToFiles(CONST LPVOID lpParam) {
	LPCTSTR szSourceFileName = (LPCTSTR)lpParam;
	TCHAR szDir[MAX_PATH]{}, szDestFileName[MAX_PATH]{};
	_tcscpy(szDir, szSourceFileName);
	_tcsrchr(szDir, '\\')[1] = '\0';

	InitializeCriticalSection(&arrCritSec[0]);
	EnterCriticalSection(&arrCritSec[0]);
	CloseHandle(CreateThread(nullptr, 0, ReadFromFiles, lpParam, 0, nullptr));
	MakeFileName(szDestFileName, szDir, 0);
	if (!CopyFile(szSourceFileName, szDestFileName, FALSE)) {
		LeaveCriticalSection(&arrCritSec[0]);
		MessageAboutError(GetLastError());
		exit(1);
	}
	LeaveCriticalSection(&arrCritSec[0]);

	for (int i = 1; i < COUNT_FILES; ++i) {
		InitializeCriticalSection(&arrCritSec[i]);
		EnterCriticalSection(&arrCritSec[i]);
		MakeFileName(szDestFileName, szDir, i);
		if (!CopyFile(szSourceFileName, szDestFileName, FALSE)) {
			LeaveCriticalSection(&arrCritSec[i]);
			MessageAboutError(GetLastError());
			exit(1);
		}
		LeaveCriticalSection(&arrCritSec[i]);
	}
	return 0;
}

DWORD WINAPI ReadFromFiles(CONST LPVOID lpParam) {
	LPCTSTR szPath = (LPCTSTR)lpParam;
	TCHAR szDir[MAX_PATH]{}, szSourceFileName[MAX_PATH]{}, szDestFileName[MAX_PATH]{};
	_tcscpy(szDir, szPath);
	_tcsrchr(szDir, '\\')[1] = '\0';
	_tcscpy(szDestFileName, szDir);
	_tcscat(szDestFileName, TEXT("BIG FILE.txt"));

	std::ofstream fout(szDestFileName);
	std::ifstream fin;
	int sizeFile;
	char *buf = nullptr;

	for (int i = 0; i < COUNT_FILES; ++i) {
		EnterCriticalSection(&arrCritSec[i]);
		MakeFileName(szSourceFileName, szDir, i);
		fin.open(szSourceFileName, std::ios::ate);
		if (!fin) {
			LeaveCriticalSection(&arrCritSec[i]);
			MessageAboutError(GetLastError());
			return 1;
		}
		sizeFile = fin.tellg();
		fin.seekg(0);
		fin.clear();
		buf = new char[sizeFile] {};
		fin.read(buf, sizeFile);
		fout.write(buf, sizeFile);
		fin.close();
		delete[] buf;
		LeaveCriticalSection(&arrCritSec[i]);
	}
	MessageBox(0, TEXT("����� ������� �����������"), TEXT(""), MB_OK | MB_ICONINFORMATION);
	return 0;
}

void MakeFileName(LPTSTR lpszFullPath, LPCTSTR lpszDir, int index) {
	TCHAR szBufCounter[8]{};
	_tcscpy(lpszFullPath, lpszDir);
	_itot(index + 1, szBufCounter, 10);
	_tcscat(lpszFullPath, szBufCounter);
	_tcscat(lpszFullPath, TEXT(".txt"));
}