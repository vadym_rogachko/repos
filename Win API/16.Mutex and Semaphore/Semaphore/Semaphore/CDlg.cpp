#include<windowsx.h>
#include<tchar.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg::~CDlg() {
	ReleaseSemaphore(hSemaphore, 1, nullptr);
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	hSemaphore = CreateSemaphore(nullptr, 3, 3, TEXT("A589AFA7 - 26E9 - 4093 - 8578 - 9C8DDD9855C0"));
	switch (WaitForSingleObject(hSemaphore, 0)) {
	case WAIT_TIMEOUT:
	case WAIT_FAILED:
		MessageBox(hDlg, TEXT("����� ��������� �� ����� ���� ����� ����������"), nullptr, MB_OK | MB_ICONEXCLAMATION);
		SendMessage(hDlg, WM_CLOSE, 0, 0);
		hSemaphore = nullptr;
		break;
	}
	return TRUE;
}