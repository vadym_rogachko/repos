#pragma once
#include<windows.h>

class CDlg {

	static CDlg* ptr;
	HWND hDlg;
	HANDLE hSemaphore;

	CDlg();
	~CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();
};