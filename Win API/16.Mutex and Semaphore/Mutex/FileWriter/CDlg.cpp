#include<windowsx.h>
#include<tchar.h>
#include"CDlg.h"
#include"resource.h"

void MessageAboutError(DWORD dwError) {
	LPVOID lpMsgBuf = NULL;
	TCHAR szBuf[300];
	BOOL fOK = FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	if (lpMsgBuf != NULL) {
		wsprintf(szBuf, TEXT("������ %d: %s"), dwError, lpMsgBuf);
		MessageBox(0, szBuf, TEXT("��������� �� ������"), MB_OK | MB_ICONSTOP);
		LocalFree(lpMsgBuf);
	}
}

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	WriteToFiles();
	return TRUE;
}

void CDlg::WriteToFiles() {
	TCHAR szFileName[MAX_PATH]{}, GUID[64]{}, szCountFiles[8]{};
	GetEnvironmentVariable(TEXT("FileName"), szFileName, MAX_PATH);
	GetEnvironmentVariable(TEXT("GUID"), GUID, 64);
	GetEnvironmentVariable(TEXT("CountFiles"), szCountFiles, 8);
	if (!szFileName[0]) {
		MessageBeep(MB_ICONERROR);
		SendMessage(hDlg, WM_CLOSE, 0, 0);
		return;
	}

	const int countFiles = _tstoi(szCountFiles);
	TCHAR szMutexName[128]{}, szCounter[8]{};
	_tcscpy(szMutexName, GUID);
	_itot(0, szCounter, 10);
	_tcscat(szMutexName, szCounter);

	TCHAR szDir[MAX_PATH]{}, szDestFileName[MAX_PATH]{};
	_tcscpy(szDir, szFileName);
	_tcsrchr(szDir, '\\')[1] = '\0';

	HANDLE hMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, szMutexName);
	if (WaitForSingleObject(hMutex, INFINITE) == WAIT_OBJECT_0) {
		MakeFileName(szDestFileName, szDir, 0);
		if (!CopyFile(szFileName, szDestFileName, FALSE)) {
			ReleaseMutex(hMutex);
			MessageAboutError(GetLastError());
			return;
		}
		ReleaseMutex(hMutex);

		STARTUPINFO startInfo = { sizeof(STARTUPINFO) };
		PROCESS_INFORMATION procInfo;
		TCHAR cmd[] = TEXT("FileReader.exe ");

		if (!CreateProcess(nullptr, cmd, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &startInfo, &procInfo)) {
			ReleaseMutex(hMutex);
			MessageAboutError(GetLastError());
			return;
		}
		CloseHandle(procInfo.hProcess);
		CloseHandle(procInfo.hThread);

		for (int i = 1; i < countFiles; ++i) {
			_tcscpy(szMutexName, GUID);
			_itot(i, szCounter, 10);
			_tcscat(szMutexName, szCounter);

			hMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, szMutexName);
			if (WaitForSingleObject(hMutex, INFINITE) == WAIT_OBJECT_0) {
				MakeFileName(szDestFileName, szDir, i);
				if (!CopyFile(szFileName, szDestFileName, FALSE)) {
					ReleaseMutex(hMutex);
					MessageAboutError(GetLastError());
					return;
				}
				ReleaseMutex(hMutex);
			}
			else {
				ReleaseMutex(hMutex);
				MessageAboutError(GetLastError());
				return;
			}
		}
	}
	else {
		ReleaseMutex(hMutex);
		MessageAboutError(GetLastError());
		return;
	}
}

void CDlg::MakeFileName(LPTSTR lpszFullPath, LPCTSTR lpszDir, int index) {
	TCHAR szBufCounter[8]{};
	_tcscpy(lpszFullPath, lpszDir);
	_itot(index + 1, szBufCounter, 10);
	_tcscat(lpszFullPath, szBufCounter);
	_tcscat(lpszFullPath, TEXT(".txt"));
}