#include<windowsx.h>
#include<tchar.h>
#include<fstream>
#include"CDlg.h"
#include"resource.h"

void MessageAboutError(DWORD dwError) {
	LPVOID lpMsgBuf = NULL;
	TCHAR szBuf[300];
	BOOL fOK = FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	if (lpMsgBuf != NULL) {
		wsprintf(szBuf, TEXT("������ %d: %s"), dwError, lpMsgBuf);
		MessageBox(0, szBuf, TEXT("��������� �� ������"), MB_OK | MB_ICONSTOP);
		LocalFree(lpMsgBuf);
	}
}

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	ReadFromFiles();
	return TRUE;
}

void CDlg::ReadFromFiles() {
	TCHAR szFileName[MAX_PATH]{}, GUID[64]{}, szCountFiles[8]{};
	GetEnvironmentVariable(TEXT("FileName"), szFileName, MAX_PATH);
	GetEnvironmentVariable(TEXT("GUID"), GUID, 64);
	GetEnvironmentVariable(TEXT("CountFiles"), szCountFiles, 8);
	if (!szFileName[0]) {
		MessageBeep(MB_ICONERROR);
		SendMessage(hDlg, WM_CLOSE, 0, 0);
		return;
	}

	TCHAR szDir[MAX_PATH]{}, szDestFileName[MAX_PATH]{}, szSourceFileName[MAX_PATH]{};
	_tcscpy(szDir, szFileName);
	_tcsrchr(szDir, '\\')[1] = '\0';
	_tcscpy(szDestFileName, szDir);
	_tcscat(szDestFileName, TEXT("BIG FILE.txt"));

	std::ofstream fout(szDestFileName);
	std::ifstream fin;
	int sizeFile;
	char *buf = nullptr;
	const int countFiles = _tstoi(szCountFiles);
	TCHAR szMutexName[128]{}, szCounter[8]{};

	for (int i = 0; i < countFiles; ++i) {
		_tcscpy(szMutexName, GUID);
		_itot(i, szCounter, 10);
		_tcscat(szMutexName, szCounter);

		hMutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, szMutexName);
		if (WaitForSingleObject(hMutex, INFINITE) == WAIT_OBJECT_0) {
			MakeFileName(szSourceFileName, szDir, i);
			fin.open(szSourceFileName, std::ios::ate);
			if (!fin) {
				ReleaseMutex(hMutex);
				MessageAboutError(GetLastError());
				return;
			}
			sizeFile = fin.tellg();
			fin.seekg(0);
			fin.clear();
			buf = new char[sizeFile] {};
			fin.read(buf, sizeFile);
			fout.write(buf, sizeFile);
			fin.close();
			delete[] buf;
			ReleaseMutex(hMutex);
		}
		else {
			ReleaseMutex(hMutex);
			MessageAboutError(GetLastError());
			return;
		}
	}
	MessageBox(0, TEXT("����� ������� �����������"), TEXT(""), MB_OK | MB_ICONINFORMATION);
}

void CDlg::MakeFileName(LPTSTR lpszFullPath, LPCTSTR lpszDir, int index) {
	TCHAR szBufCounter[8]{};
	_tcscpy(lpszFullPath, lpszDir);
	_itot(index + 1, szBufCounter, 10);
	_tcscat(lpszFullPath, szBufCounter);
	_tcscat(lpszFullPath, TEXT(".txt"));
}