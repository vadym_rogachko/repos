#include<windowsx.h>
#include<tchar.h>
#include"CDlg.h"
#include"resource.h"

void MessageAboutError(DWORD dwError) {
	LPVOID lpMsgBuf = NULL;
	TCHAR szBuf[300];
	BOOL fOK = FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
		NULL,
		dwError,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	if (lpMsgBuf != NULL) {
		wsprintf(szBuf, TEXT("������ %d: %s"), dwError, lpMsgBuf);
		MessageBox(0, szBuf, TEXT("��������� �� ������"), MB_OK | MB_ICONSTOP);
		LocalFree(lpMsgBuf);
	}
}

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	_tcscpy(GUID, TEXT("C74DBD7D-3770-4FB1-A37B-439854B2D4FC"));
	TCHAR szMutexName[64]{}, szBuf[8]{};
	for (int i = 0; i < COUNT_FILE_COPIES; ++i) {
		_tcscpy(szMutexName, GUID);
		_itot(i, szBuf, 10);
		_tcscat(szMutexName, szBuf);
		CreateMutex(nullptr, FALSE, szMutexName);
	}
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (hwndCtl == hCtrl[BTN]) {
		if (hwndCtl == hCtrl[BTN]) {
			TCHAR szFileName[MAX_PATH]{};
			SendMessage(hCtrl[EDIT], WM_GETTEXT, MAX_PATH, (LPARAM)szFileName);
			if (!_tcslen(szFileName)) {
				MessageBox(hDlg, TEXT("������� ��� �����"), TEXT(""), MB_OK | MB_ICONEXCLAMATION);
				return;
			}

			TCHAR szBuf[8]{};
			_itot(COUNT_FILE_COPIES, szBuf, 10);
			SetEnvironmentVariable(TEXT("Filename"), szFileName);
			SetEnvironmentVariable(TEXT("GUID"), GUID);
			SetEnvironmentVariable(TEXT("CountFiles"), szBuf);

			STARTUPINFO startInfo = { sizeof(STARTUPINFO) };
			PROCESS_INFORMATION procInfo;
			TCHAR cmd[] = TEXT("FileWriter.exe ");

			if(!CreateProcess(nullptr, cmd, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &startInfo, &procInfo)) {
				MessageAboutError(GetLastError());
				return;
			}
			CloseHandle(procInfo.hProcess);
			CloseHandle(procInfo.hThread);
		}
	}
}

void CDlg::setCtrlHandles() {
	for (int i = 0; i < COUNT_CTRLS; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_BUTTON1 + i);
	}
}