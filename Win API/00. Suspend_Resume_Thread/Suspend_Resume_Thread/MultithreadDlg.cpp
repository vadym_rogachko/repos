#include "MultithreadDlg.h"

CMultithreadDlg* CMultithreadDlg::ptr = NULL;

CMultithreadDlg::CMultithreadDlg(void)
{
	ptr = this;
}

CMultithreadDlg::~CMultithreadDlg(void)
{
	CloseHandle(hEv1);
	CloseHandle(hEv2);
	CloseHandle(hEv3);
	CloseHandle(hTh1);
	CloseHandle(hTh2);
	CloseHandle(hTh3);
}

void CMultithreadDlg::Cls_OnClose(HWND hwnd)
{
	EndDialog(hwnd, 0);
}

DWORD WINAPI Thread1(LPVOID lp)
{
	HWND hProgress = (HWND)lp;
	HANDLE hEv = OpenEvent(EVENT_ALL_ACCESS, false, TEXT("{07B9600C-54AE-46E4-88C6-46E9BBB48A3A1}"));
	while (WaitForSingleObject(hEv, 0) != WAIT_OBJECT_0)
	{
		SendMessage(hProgress, PBM_SETPOS, rand() % 200 + 1, 0);
		Sleep(250);
	}
	return 0;
}

DWORD WINAPI Thread2(LPVOID lp)
{
	HWND hProgress = (HWND)lp;
	HANDLE hEv = OpenEvent(EVENT_ALL_ACCESS, false, TEXT("{07B9600C-54AE-46E4-88C6-46E9BBB48A3A2}"));
	while (WaitForSingleObject(hEv, 0) != WAIT_OBJECT_0)
	{
		SendMessage(hProgress, PBM_SETPOS, rand() % 200 + 1, 0);
		Sleep(250);
	}
	return 0;
}

DWORD WINAPI Thread3(LPVOID lp)
{
	HWND hProgress = (HWND)lp;
	HANDLE hEv = OpenEvent(EVENT_ALL_ACCESS, false, TEXT("{07B9600C-54AE-46E4-88C6-46E9BBB48A3A3}"));
	while (WaitForSingleObject(hEv, 0) != WAIT_OBJECT_0)
	{
		SendMessage(hProgress, PBM_SETPOS, rand() % 200 + 1, 0);
		Sleep(250);
	}
	return 0;
}

BOOL CMultithreadDlg::Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam)
{
	srand(time(0));
	hProgress1 = GetDlgItem(hwnd, IDC_PROGRESS1);
	SendMessage(hProgress1, PBM_SETRANGE, 0, MAKELPARAM(0, 200));
	SendMessage(hProgress1, PBM_SETSTEP, 1, 0);
	SendMessage(hProgress1, PBM_SETPOS, 0, 0);
	SendMessage(hProgress1, PBM_SETBKCOLOR, 0, LPARAM(RGB(0, 0, 255)));
	SendMessage(hProgress1, PBM_SETBARCOLOR, 0, LPARAM(RGB(255, 255, 0)));

	hProgress2 = GetDlgItem(hwnd, IDC_PROGRESS2);
	SendMessage(hProgress2, PBM_SETRANGE, 0, MAKELPARAM(0, 200));
	SendMessage(hProgress2, PBM_SETSTEP, 1, 0);
	SendMessage(hProgress2, PBM_SETPOS, 0, 0);
	SendMessage(hProgress2, PBM_SETBKCOLOR, 0, LPARAM(RGB(0, 255, 0)));
	SendMessage(hProgress2, PBM_SETBARCOLOR, 0, LPARAM(RGB(255, 0, 255)));

	hProgress3 = GetDlgItem(hwnd, IDC_PROGRESS3);
	SendMessage(hProgress3, PBM_SETRANGE, 0, MAKELPARAM(0, 200));
	SendMessage(hProgress3, PBM_SETSTEP, 1, 0);
	SendMessage(hProgress3, PBM_SETPOS, 0, 0);
	SendMessage(hProgress3, PBM_SETBKCOLOR, 0, LPARAM(RGB(255, 0, 0)));
	SendMessage(hProgress3, PBM_SETBARCOLOR, 0, LPARAM(RGB(0, 255, 255)));

	hPlay1 = GetDlgItem(hwnd, IDC_PLAY1);
	hPlay2 = GetDlgItem(hwnd, IDC_PLAY2);
	hPlay3 = GetDlgItem(hwnd, IDC_PLAY3);

	hEv1 = CreateEvent(nullptr, false, false, TEXT("{07B9600C-54AE-46E4-88C6-46E9BBB48A3A1}"));
	hEv2 = CreateEvent(nullptr, false, false, TEXT("{07B9600C-54AE-46E4-88C6-46E9BBB48A3A2}"));
	hEv3 = CreateEvent(nullptr, false, false, TEXT("{07B9600C-54AE-46E4-88C6-46E9BBB48A3A3}"));

	hTh1 = hTh2 = hTh3 = nullptr;

	return TRUE;
}

void CMultithreadDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify)
{

	if (id == IDC_PLAY1)
	{
		if (!hTh1) {
			hTh1 = CreateThread(NULL, 0, Thread1, hProgress1, 0, NULL);
		}
	}
	else if (id == IDC_PLAY2)
	{
		if (!hTh2) {
			hTh2 = CreateThread(NULL, 0, Thread2, hProgress2, 0, NULL);
		}
	}
	else if (id == IDC_PLAY3)
	{
		if (!hTh3) {
			hTh3 = CreateThread(NULL, 0, Thread3, hProgress3, 0, NULL);
		}
	}
	else if (id == IDC_STOP1)
	{
		SetEvent(hEv1);
		CloseHandle(hTh1);
		hTh1 = nullptr;
	}
	else if (id == IDC_STOP2)
	{
		SetEvent(hEv2);
		CloseHandle(hTh1);
		hTh2 = nullptr;
	}
	else if (id == IDC_STOP3)
	{
		SetEvent(hEv3);
		CloseHandle(hTh1);
		hTh3 = nullptr;
	}
}

BOOL CALLBACK CMultithreadDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}