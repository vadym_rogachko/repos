#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<tchar.h>
#include<ctime>
#include"resource.h"
#include"CDlg.h"

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow) {
	srand(time(0));
	INITCOMMONCONTROLSEX icc = { sizeof(INITCOMMONCONTROLSEX) };
	icc.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&icc);
	CDlg& dlg = CDlg::GetRef();
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), nullptr, DLGPROC(CDlg::DlgProc));
}