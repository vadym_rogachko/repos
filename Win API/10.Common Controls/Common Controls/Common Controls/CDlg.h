#pragma once
#include<windows.h>
#include<commctrl.h>

#pragma comment(lib, "comctl32")

class CDlg {

	enum range { MIN_VALUE_BUTTON = 0, MAX_VALUE_BUTTON = 100 };

	enum time  { START_TIME = 30, MIN_TIME = 5, MAX_TIME = 60 };

	enum count { COUNT_CONTROLS = 5, COUNT_BUTTONS = 16 };

	enum ctrls { LIST, PROGRESS, EDIT_SPIN, SPIN, BUTTON_NEW_GAME };

	static CDlg* ptr;
	HWND hDlg;
	HWND hBtn[COUNT_BUTTONS];
	HWND hCtrl[COUNT_CONTROLS];

	TCHAR szBuf[8];
	int field[COUNT_BUTTONS];
	int fieldSort[COUNT_BUTTONS];
	int timeSpin;
	int move;
	bool bGameInProgress;

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
	void Cls_OnTimer(HWND hwnd, UINT id);

	void setCtrlHandles();
	void setSpin();
	void setProgress();

	void StartGame();
	void GenerateField();
	void FillField();
	void RefreshProgress();
	void EnableAllButtons();
	void CheckTime();
	void ClearList();
	void setCaptionDlg(LPCTSTR lpszTime);
	void MakeMove(int idButton);
	void StopGame();
	void OnTimerProgress();
	void OnTimerCaption();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();
};