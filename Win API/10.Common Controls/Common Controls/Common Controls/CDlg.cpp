#include<windowsx.h>
#include<tchar.h>
#include<algorithm>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
	bGameInProgress = false;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
		HANDLE_MSG(hwnd, WM_TIMER, ptr->Cls_OnTimer);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	setSpin();
	setProgress();
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (id == IDC_BUTTON_NEW_GAME) {
		StartGame();
	}
	else if (id >= IDC_BUTTON1 && id <= IDC_BUTTON16) {
		if (bGameInProgress) {
			if (field[id - IDC_BUTTON1] == fieldSort[move]) {
				MakeMove(id);
			}
		}
	}
}

void CDlg::Cls_OnTimer(HWND hwnd, UINT id) {
	OnTimerProgress();
	OnTimerCaption();
}

void CDlg::setCtrlHandles() {
	for (int i = 0; i <= COUNT_BUTTONS; ++i) {
		hBtn[i] = GetDlgItem(hDlg, IDC_BUTTON1 + i);
	}
	for (int i = LIST; i <= BUTTON_NEW_GAME; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_LIST1 + i);
	}
}

void CDlg::setSpin() {
	SendMessage(hCtrl[SPIN], UDM_SETRANGE32, MIN_TIME, MAX_TIME);
	SendMessage(hCtrl[SPIN], UDM_SETPOS32, 0, START_TIME);
}

void CDlg::setProgress() {
	SendMessage(hCtrl[PROGRESS], PBM_SETBARCOLOR, 0, RGB(0, 150, 255));
	SendMessage(hCtrl[PROGRESS], PBM_SETSTEP, 1, 0);
}

void CDlg::StartGame() {
	bGameInProgress = true;
	move = 0;
	GenerateField();
	FillField();
	RefreshProgress();
	EnableAllButtons();
	CheckTime();
	ClearList();
	setCaptionDlg(TEXT("00:00"));
	SetTimer(hDlg, 1, 1000, nullptr);
}

void CDlg::GenerateField() {
	for (int i = 0; i < COUNT_BUTTONS; ++i) {
		do {
			fieldSort[i] = field[i] = rand() % (MAX_VALUE_BUTTON + 1);
		} while (std::find(field, field + i, field[i]) != field + i);
	}
	std::sort(fieldSort, fieldSort + COUNT_BUTTONS);
}

void CDlg::FillField() {
	for (int i = 0; i < COUNT_BUTTONS; ++i) {
		wsprintf(szBuf, TEXT("%d"), field[i]);
		SetWindowText(hBtn[i], szBuf);
	}
}

void CDlg::RefreshProgress() {
	timeSpin = SendMessage(hCtrl[SPIN], UDM_GETPOS32, 0, 0);
	SendMessage(hCtrl[PROGRESS], PBM_SETRANGE, 0, MAKELPARAM(0, timeSpin));
	SendMessage(hCtrl[PROGRESS], PBM_SETPOS, 0, 0);
}

void CDlg::EnableAllButtons() {
	for (int i = 0; i < COUNT_BUTTONS; ++i) {
		EnableWindow(hBtn[i], TRUE);
	}
}

void CDlg::CheckTime() {
	int num;
	SendMessage(hCtrl[EDIT_SPIN], EM_GETLINE, 0, (LPARAM)szBuf);
	szBuf[SendMessage(hCtrl[EDIT_SPIN], EM_LINELENGTH, 0, 0)] = '\0';
	num = _tstoi(szBuf);
	if (num < MIN_TIME) {
		_itot(MIN_TIME, szBuf, 10);
		SendMessage(hCtrl[EDIT_SPIN], WM_SETTEXT, 0, (LPARAM)szBuf);
	}
	else if (num > MAX_TIME) {
		_itot(MAX_TIME, szBuf, 10);
		SendMessage(hCtrl[EDIT_SPIN], WM_SETTEXT, 0, (LPARAM)szBuf);
	}
}

void CDlg::ClearList() {
	SendMessage(hCtrl[LIST], LB_RESETCONTENT, 0, 0);
}

void CDlg::setCaptionDlg(LPCTSTR lpszTime) {
	SetWindowText(hDlg, lpszTime);
}

void CDlg::MakeMove(int idButton) {
	MessageBeep(MB_ICONASTERISK);
	_itot_s(fieldSort[move], szBuf, 10);
	SendMessage(hCtrl[LIST], LB_ADDSTRING, 0, LPARAM(szBuf));
	EnableWindow(hBtn[idButton - IDC_BUTTON1], FALSE);
	++move;
	if (move == 16) {
		StopGame();
	}
}

void CDlg::StopGame() {
	bGameInProgress = false;
	SetFocus(hCtrl[BUTTON_NEW_GAME]);
	KillTimer(hDlg, 1);
}

void CDlg::OnTimerProgress() {
	SendMessage(hCtrl[PROGRESS], PBM_STEPIT, 0, 0);
}

void CDlg::OnTimerCaption() {
	int timeSec;
	GetWindowText(hDlg, szBuf, sizeof szBuf / sizeof szBuf[0]);
	timeSec = _tstoi(szBuf + 3) + 1;
	if (timeSec == timeSpin) {
		setCaptionDlg(TEXT("����� �����"));
		StopGame();
		return;
	}
	wsprintf(szBuf, timeSec > 9 ? TEXT("00:%d") : TEXT("00:0%d"), timeSec);
	setCaptionDlg(szBuf);
}