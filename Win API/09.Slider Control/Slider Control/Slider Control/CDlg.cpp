#include<windowsx.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCtrlHandles();
	setSlidersRange();
	setSliderStartPos();
	Update();
	return TRUE;
}

void CDlg::Cls_OnHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos) {
	Update();
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_HSCROLL, ptr->Cls_OnHScroll);
	}
	return FALSE;
}

void CDlg::setCtrlHandles() {
	for (int i = RED; i <= PROGRESS; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_SLIDER_RED + i);
	}
}

void CDlg::setSlidersRange() {
	for (int i = RED; i <= BLUE; ++i) {
		SendMessage(hCtrl[i], TBM_SETRANGE, FALSE, MAKELPARAM(MIN, MAX));
	}
}

void CDlg::setSliderStartPos() {
	for (int i = RED; i <= BLUE; ++i) {
		SendMessage(hCtrl[i], TBM_SETPOS, TRUE, (MIN + MAX) / 2);
	}
}

void CDlg::Update() {
	UpdateSlidersPos();
	UpdateCaption();
	UpdateColor();
}

void CDlg::UpdateSlidersPos() {
	for (int i = RED; i <= BLUE; ++i) {
		pos[i] = SendMessage(hCtrl[i], TBM_GETPOS, 0, 0);
	}
}

void CDlg::UpdateCaption() {
	static TCHAR caption[32];
	wsprintf(caption, TEXT("Red: %d  Green: %d  Blue: %d"), pos[RED], pos[GREEN], pos[BLUE]);
	SetWindowText(hDlg, caption);
}

void CDlg::UpdateColor() {
	SendMessage(hCtrl[PROGRESS], PBM_SETBKCOLOR, 0, LPARAM(RGB(pos[RED], pos[GREEN], pos[BLUE])));
}