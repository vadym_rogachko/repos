#pragma once
#include<windows.h>
#include<commctrl.h>

#pragma comment(lib, "comctl32")

class CDlg {

	enum positions { MIN = 0, MAX = 255 };
	enum controls { RED, GREEN, BLUE, PROGRESS };

	static CDlg* ptr;
	HWND hDlg;
	HWND hCtrl[4];
	int pos[3];

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void Cls_OnHScroll(HWND hwnd, HWND hwndCtl, UINT code, int pos);

	void setCtrlHandles();
	void setSlidersRange();
	void setSliderStartPos();

	void Update();
	void UpdateSlidersPos();
	void UpdateCaption();
	void UpdateColor();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg GetRef();
};