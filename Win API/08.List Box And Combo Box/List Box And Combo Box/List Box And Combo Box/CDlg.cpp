#include<windowsx.h>
#include<tchar.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hWnd;
	setCtrlHandles();
	SendDlgItemMessage(hWnd, IDC_RADIO_SUM, BM_SETCHECK, BST_CHECKED, 0);
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify) {
	if (codeNotify == BN_CLICKED) {
		if (hWndCtl == hCtrl[BUTTON]) {
			FillList();
		}
		ArithmAction();
	}
}

BOOL CALLBACK CDlg::DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hWnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hWnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hWnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::setCtrlHandles() {
	for (int i = BUTTON; i <= RADIO_MEAN; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_BUTTON1 + i);
	}
}

void CDlg::FillList() {
	int count = rand() % 11 + 10;
	int num = 0;
	TCHAR szBuf[8]{};
	SendMessage(hCtrl[LIST], LB_RESETCONTENT, 0, 0);
	for (int i = 0; i < count; ++i) {
		while (!num) num = rand() % 21 - 10;
		wsprintf(szBuf, TEXT("%d"), num);
		SendMessage(hCtrl[LIST], LB_ADDSTRING, 0, (LPARAM) szBuf);
		num = 0;
	}
}

void CDlg::ArithmAction() {
	int count = SendMessage(hCtrl[LIST], LB_GETCOUNT, 0, 0);
	if (!count) return;
	if (BST_CHECKED == SendDlgItemMessage(hDlg, IDC_RADIO_SUM, BM_GETCHECK, 0, 0)) {
		SetSum(count);
	}
	else if (BST_CHECKED == SendDlgItemMessage(hDlg , IDC_RADIO_MULT, BM_GETCHECK, 0, 0)) {
		SetMult(count);
	}
	else if (BST_CHECKED == SendDlgItemMessage(hDlg, IDC_RADIO_MEAN, BM_GETCHECK, 0, 0)) {
		SetMean(count);
	}
}

void CDlg::SetSum(int count) {
	int sum = 0;
	TCHAR szBuf[8]{};
	for (int i = 0; i < count; ++i) {
		SendMessage(hCtrl[LIST], LB_GETTEXT, i, (LPARAM) szBuf);
		sum += _tstoi(szBuf);
	}
	wsprintf(szBuf, TEXT("%d"), sum);
	SendMessage(hCtrl[EDIT], WM_SETTEXT, 0, (LPARAM) szBuf);
}

void CDlg::SetMult(int count) {
	double mult = 1;
	TCHAR szBuf[64]{};
	for (int i = 0; i < count; ++i) {
		SendMessage(hCtrl[LIST], LB_GETTEXT, i, (LPARAM)szBuf);
		mult *= _tstoi(szBuf);
	}
	_stprintf(szBuf, TEXT("%.0f"), mult);
	SendMessage(hCtrl[EDIT], WM_SETTEXT, 0, (LPARAM)szBuf);
}

void CDlg::SetMean(int count) {
	double sum = 0;
	TCHAR szBuf[8]{};
	for (int i = 0; i < count; ++i) {
		SendMessage(hCtrl[LIST], LB_GETTEXT, i, (LPARAM)szBuf);
		sum += _tstoi(szBuf);
	}
	_stprintf(szBuf, TEXT("%.2f"), sum / count);
	SendMessage(hCtrl[EDIT], WM_SETTEXT, 0, (LPARAM)szBuf);
}