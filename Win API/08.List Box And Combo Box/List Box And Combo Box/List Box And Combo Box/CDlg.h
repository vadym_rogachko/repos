#pragma once
#include<windows.h>

class CDlg {

	enum { BUTTON, EDIT, LIST, RADIO_SUM, RADIO_MULT, RADIO_MEAN };

	static CDlg* ptr;
	HWND hDlg;
	HWND hCtrl[6];

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify);

	void setCtrlHandles();
	void FillList();
	void ArithmAction();
	void SetSum(int count);
	void SetMult(int count);
	void SetMean(int count);

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg GetRef();
};