#include<windowsx.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam) {
	hCtrl = GetDlgItem(hWnd, IDC_COMBO1);
	GetDrives();
	return TRUE;
}

BOOL CALLBACK CDlg::DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hWnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hWnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
	}
	return FALSE;
}

void CDlg::GetDrives() {
	DWORD drives = GetLogicalDrives();
	int countDrives = 0;
	for (int i = 0; i < sizeof drives * 8; ++i) {
		if ((drives >> i) & 1) ++countDrives;
	}
	int sizeDriveNamesString = (countDrives * 4) + 1;
	LPTSTR szDriveNames = new TCHAR[sizeDriveNamesString];
	GetLogicalDriveStrings(sizeDriveNamesString, szDriveNames);
	LPCTSTR names[] = { TEXT("(�����������)"), TEXT("(�� ����������)"), TEXT("(�������)"), TEXT("(���������)"), TEXT("(�������)"), TEXT("(CDROM)"), TEXT("(�����������)") };
	TCHAR szDriveName[32]{};
	for (int i = 0; i < countDrives; ++i) {
		lstrcpy(szDriveName, szDriveNames + (4 * i));
		szDriveName[2] = ' ';
		lstrcat(szDriveName, names[GetDriveType(szDriveNames + (4 * i))]);
		SendMessage(hCtrl, CB_ADDSTRING, 0, (LPARAM) szDriveName);
	}
	delete[] szDriveNames;
}