#pragma once
#include<windows.h>

class CDlg {

	static CDlg* ptr;
	HWND hCtrl;

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);

	void GetDrives();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg GetRef();
};