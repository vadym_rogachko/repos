#include<windowsx.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hWnd;
	setCtrlsStatHandles();
	setCtrlsDynNames();
	setCtrlsDynProperties();
	setRectClient();
	setRectComboBox();
	return TRUE;
}

void CDlg::Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify) {
	if (codeNotify == BN_CLICKED) {
		if (id == IDC_BTN_CREATE) {
			CreateControl();
		}
		else if (id == IDC_BTN_DELETE) {
			DestroyControl();
		}
	}
}

BOOL CALLBACK CDlg::DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hWnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hWnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hWnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::setCtrlsStatHandles() {
	for (int i = BTN_CREATE; i <= COMBO_CONTROLS; ++i) {
		hCtrl[i] = GetDlgItem(hDlg, IDC_BTN_CREATE + i);
	}
}

void CDlg::setCtrlsDynNames() {
	TCHAR lpszControls[COUNT_CONTROLS_DYN][32]{ TEXT("BUTTON"), TEXT("COMBOBOX"), TEXT("EDIT"), TEXT("LISTBOX"), TEXT("STATIC") };
	for (int i = 0; i < COUNT_CONTROLS_DYN; ++i) {
		SendMessage(hCtrl[COMBO_CONTROLS], CB_ADDSTRING, 0, (LPARAM)lpszControls[i]);
	}
}

void CDlg::setCtrlsDynProperties() {

	ctrls[BUTTON] =
	{
		WS_EX_DLGMODALFRAME,
		TEXT("BUTTON"),
		TEXT("Button"),
		WS_CHILD | WS_VISIBLE,
		WIDTH_BUTTON, HEIGHT_BUTTON,
		hDlg,
		nullptr,
		GetModuleHandle(nullptr),
		nullptr 
	};

	ctrls[COMBOBOX] =
	{
		WS_EX_DLGMODALFRAME,
		TEXT("COMBOBOX"),
		TEXT(""),
		WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST,
		WIDTH_COMBOBOX, HEIGHT_COMBOBOX,
		hDlg,
		nullptr,
		GetModuleHandle(nullptr),
		nullptr
	};

	ctrls[EDIT] = 
	{
		WS_EX_CLIENTEDGE,
		TEXT("EDIT"),
		TEXT("Edit field"),
		WS_CHILD | WS_VISIBLE,
		WIDTH_EDIT, HEIGHT_EDIT,
		hDlg,
		nullptr,
		GetModuleHandle(nullptr),
		nullptr
	};

	ctrls[LISTBOX] =
	{
		WS_EX_CLIENTEDGE,
		TEXT("LISTBOX"),
		TEXT(""),
		WS_CHILD | WS_VISIBLE,
		WIDTH_LISTBOX, HEIGHT_LISTBOX,
		hDlg,
		nullptr,
		GetModuleHandle(nullptr),
		nullptr
	};

	ctrls[STATIC] =
	{
		WS_EX_CLIENTEDGE,
		TEXT("STATIC"),
		TEXT("Static Text"),
		WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER,
		WIDTH_STATIC, HEIGHT_STATIC,
		hDlg,
		nullptr,
		GetModuleHandle(nullptr),
		nullptr
	};

}

void CDlg::setRectClient() {
	GetClientRect(hDlg, &rectClient);
}

void CDlg::setRectComboBox() {
	GetWindowRect(hCtrl[COMBO_CONTROLS], &rectComboBox);
	POINT point;
	point.x = rectComboBox.right;
	point.y = rectComboBox.bottom;
	ScreenToClient(hDlg, &point);
	rectComboBox.right = point.x;
	rectComboBox.bottom = point.y;
}

void CDlg::CreateControl() {
	LRESULT ctrlSelect = SendMessage(hCtrl[COMBO_CONTROLS], CB_GETCURSEL, 0, 0);
	if (ctrlSelect == CB_ERR) {
		MessageNoChoose();
		return;
	}
	RECT rect = { 0, 0, ctrls[ctrlSelect].nWidth, ctrls[ctrlSelect].nHeight };
	GenerateCoord(ctrlSelect, rect);
	avhCtrlDyn[ctrlSelect].push_back(
		CreateWindowEx(
			ctrls[ctrlSelect].dwExStyle,
			ctrls[ctrlSelect].szClassName,
			ctrls[ctrlSelect].szWindowName,
			ctrls[ctrlSelect].dwStyle,
			rect.left,  rect.top,
			rect.right, rect.bottom,
			ctrls[ctrlSelect].hWndParent,
			ctrls[ctrlSelect].hMenu,
			ctrls[ctrlSelect].hInstance,
			ctrls[ctrlSelect].lpParam));
}

void CDlg::GenerateCoord(LRESULT ctrlSelect, RECT& rect) {
	do {
		rect.left = rand() % (rectClient.right - rect.right);
		rect.top = rand() % (rectClient.bottom - rect.bottom);
	} while (rect.left <= rectComboBox.right && rect.top <= rectComboBox.bottom);
}

void CDlg::DestroyControl() {
	LRESULT ctrlSelect = SendMessage(hCtrl[COMBO_CONTROLS], CB_GETCURSEL, 0, 0);
	if (ctrlSelect == CB_ERR) {
		MessageNoChoose();
		return;
	}
	if (avhCtrlDyn[ctrlSelect].size()) {
		DestroyWindow(avhCtrlDyn[ctrlSelect].back());
		avhCtrlDyn[ctrlSelect].pop_back();
	}
	else {
		MessageNoControls();
	}
}

void CDlg::MessageNoChoose() {
	MessageBox(hDlg, TEXT("������� ���������� �� ������"), nullptr, MB_OK | MB_ICONEXCLAMATION);
}

void CDlg::MessageNoControls() {
	InvalidateRect(hDlg, nullptr, true);
	MessageBox(hDlg, TEXT("��������� �������� ���������� �����������"), nullptr, MB_OK | MB_ICONEXCLAMATION);
}