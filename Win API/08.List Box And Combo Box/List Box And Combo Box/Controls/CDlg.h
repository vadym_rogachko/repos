#pragma once
#include<windows.h>
#include<vector>

using std::vector;

class CDlg {

	enum controlsCount { COUNT_CONTROLS_STAT = 3, COUNT_CONTROLS_DYN = 5 };

	enum controlsStat  { BTN_CREATE, BTN_DELETE, COMBO_CONTROLS };
	enum controlsDyn   { BUTTON, COMBOBOX, EDIT, LISTBOX, STATIC };

	enum controlsWidth  { WIDTH_BUTTON = 100, WIDTH_COMBOBOX = 100,  WIDTH_EDIT = 100, WIDTH_LISTBOX = 50,   WIDTH_STATIC = 100 };
	enum controlsHeight { HEIGHT_BUTTON = 50, HEIGHT_COMBOBOX = 150, HEIGHT_EDIT = 30, HEIGHT_LISTBOX = 115, HEIGHT_STATIC = 50 };

	struct CONTROL {
		DWORD dwExStyle;
		TCHAR szClassName[32];
		TCHAR szWindowName[32];
		DWORD dwStyle;
		int nWidth;
		int nHeight;
		HWND hWndParent;
		HMENU hMenu;
		HINSTANCE hInstance;
		LPVOID lpParam;
	};

	static CDlg* ptr;
	RECT rectClient;
	RECT rectComboBox;
	HWND hDlg;
	HWND hCtrl[COUNT_CONTROLS_STAT];
	vector<HWND> avhCtrlDyn[COUNT_CONTROLS_DYN];
	CONTROL ctrls[COUNT_CONTROLS_DYN];

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam);
	void Cls_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT codeNotify);

	void setCtrlsStatHandles();
	void setCtrlsDynNames();
	void setCtrlsDynProperties();
	void setRectClient();
	void setRectComboBox();

	void CreateControl();
	void GenerateCoord(LRESULT ctrlSelect, RECT& rect);
	void DestroyControl();

	void MessageNoChoose();
	void MessageNoControls();

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg GetRef();
};