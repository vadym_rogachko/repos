#pragma once
#include<windows.h>

class CDlg {

	enum { EDIT, STATIC, BUTTON };

	enum { WIDTH = 130, HEIGHT = 30 };

	static CDlg* ptr;
	HWND hDlg;
	HMENU hMenuContext;
	int anCountersCtrls[3];

	struct CONTROL {
		DWORD dwExStyle;
		TCHAR szClassName[32];
		DWORD dwStyle;
	}   ctrls[3];

	CDlg();
	void Cls_OnClose(HWND hwnd);
	BOOL Cls_OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
	void Cls_OnContextMenu(HWND hwnd, HWND hwndContext, UINT xPos, UINT yPos);
	void Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);

	void setCountersCtrls();
	void setContextMenu();
	void setCtrlsProperties();

	void CreateCtrlWindow(int ctrlID, int x, int y);
	void ModifyMenuItem(int ctrlID);

public:

	static BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static CDlg& GetRef();
};