#include<windowsx.h>
#include<tchar.h>
#include"CDlg.h"
#include"resource.h"

CDlg* CDlg::ptr = nullptr;

CDlg::CDlg() {
	ptr = this;
}

CDlg& CDlg::GetRef() {
	static CDlg obj;
	return obj;
}

BOOL CALLBACK CDlg::DlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		HANDLE_MSG(hwnd, WM_CLOSE, ptr->Cls_OnClose);
		HANDLE_MSG(hwnd, WM_INITDIALOG, ptr->Cls_OnInitDialog);
		HANDLE_MSG(hwnd, WM_CONTEXTMENU, ptr->Cls_OnContextMenu);
		HANDLE_MSG(hwnd, WM_COMMAND, ptr->Cls_OnCommand);
	}
	return FALSE;
}

void CDlg::Cls_OnClose(HWND hWnd) {
	EndDialog(hWnd, 0);
}

BOOL CDlg::Cls_OnInitDialog(HWND hwnd, HWND hWndFocus, LPARAM lParam) {
	hDlg = hwnd;
	setCountersCtrls();
	setContextMenu();
	setCtrlsProperties();
	return TRUE;
}

void CDlg::Cls_OnContextMenu(HWND hwnd, HWND hwndContext, UINT xPos, UINT yPos) {
	TrackPopupMenu(hMenuContext, TPM_LEFTALIGN, xPos, yPos, 0, hDlg, nullptr);
}

void CDlg::Cls_OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify) {
	if (id >= ID_MENU_EDIT && id <= ID_MENU_BUTTON) {
		POINT cursorPos;
		GetCursorPos(&cursorPos);
		ScreenToClient(hDlg, &cursorPos);
		int ctrlID = id - ID_MENU_EDIT;
		CreateCtrlWindow(ctrlID, cursorPos.x, cursorPos.y);
		--anCountersCtrls[ctrlID];
		ModifyMenuItem(ctrlID);
	}
}

void CDlg::setCountersCtrls() {
	for (int i = EDIT; i <= BUTTON; ++i) {
		anCountersCtrls[i] = 3;
	}
}

void CDlg::setContextMenu() {
	TCHAR szItemName[16], szNewName[16];
	HMENU  hDummyMenu = LoadMenu(GetModuleHandle(0), MAKEINTRESOURCE(IDR_MENU1));
	hMenuContext = GetSubMenu(hDummyMenu, 0);
	for (int i = EDIT; i <= BUTTON; ++i) {
		GetMenuString(hMenuContext, ID_MENU_EDIT + i, szItemName, 16, MF_BYCOMMAND);
		wsprintf(szNewName, TEXT("%s %d"), szItemName, anCountersCtrls[i]);
		ModifyMenu(hMenuContext, ID_MENU_EDIT + i, MF_BYCOMMAND | MF_STRING, ID_MENU_EDIT + i, szNewName);
	}
}

void CDlg::setCtrlsProperties() {

	ctrls[EDIT] =
	{
		WS_EX_CLIENTEDGE,
		TEXT("EDIT"),
		WS_CHILD | WS_VISIBLE,
	};

	ctrls[STATIC] =
	{
		WS_EX_CLIENTEDGE,
		TEXT("STATIC"),
		WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER,
	};

	ctrls[BUTTON] =
	{
		WS_EX_DLGMODALFRAME,
		TEXT("BUTTON"),
		WS_CHILD | WS_VISIBLE,
	};

}

void CDlg::CreateCtrlWindow(int ctrlID, int x, int y) {
	TCHAR szItemName[16];
	GetMenuString(hMenuContext, ID_MENU_EDIT + ctrlID, szItemName, 16, MF_BYCOMMAND);
	CreateWindowEx(
		ctrls[ctrlID].dwExStyle,
		ctrls[ctrlID].szClassName,
		szItemName,
		ctrls[ctrlID].dwStyle,
		x, y,
		WIDTH, HEIGHT,
		hDlg,
		nullptr,
		GetModuleHandle(nullptr),
		nullptr);
}

void CDlg::ModifyMenuItem(int ctrlID) {
	TCHAR szItemName[16], szCounter[16];
	GetMenuString(hMenuContext, ID_MENU_EDIT + ctrlID, szItemName, 16, MF_BYCOMMAND);
	_tcschr(szItemName, ' ')[1] = '\0';
	_itot(anCountersCtrls[ctrlID], szCounter, 10);
	_tcscat(szItemName, szCounter);
	ModifyMenu(hMenuContext, ID_MENU_EDIT + ctrlID, MF_BYCOMMAND | MF_STRING, ID_MENU_EDIT + ctrlID, szItemName);
	if (anCountersCtrls[ctrlID] == 0) {
		DeleteMenu(hMenuContext, ID_MENU_EDIT + ctrlID, MF_BYCOMMAND);
	}
}