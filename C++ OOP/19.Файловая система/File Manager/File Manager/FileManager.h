#pragma once
#include<string>

using std::wstring;

class FileManager {

	static wstring directory;

	FileManager(const FileManager &f) = delete;
	FileManager& operator = (const FileManager &f) = delete;

protected:

	FileManager() {};

	void Convert(wstring& path) const;
	void setPaths(const wstring& paths, wstring& oldPath, wstring& newPath) const;
	wstring PathAndName(const wstring& path, const wstring& name) const;
	void Error(const wstring& path) const;

	void CheckAccess(const wstring& path, int mode) const;
	void CheckPath(const wstring& path) const;
	void CheckPaths(wstring& oldPath, wstring& newPath) const;
	bool isSameDir(const wstring& dir1, const wstring& dir2) const;

	intptr_t getHandle(const wstring& path, _wfinddata_t& fileinfo) const;
	void CloseHandle(intptr_t handle) const;

	void CopyFile_(const wstring& sourceFilePath, const wstring& destFilePath) const;

	virtual void ShowFolders(const wstring& path) const;
	virtual void ShowFiles(const wstring& path) const;

public:

	static FileManager& getReference();
	const wstring& getDirectory() const;
	void setDirectory(wstring path);
	virtual void ShowDirectory(wstring path = L"") const;
	void MakeDirectory(wstring path) const;
	void Rename(const wstring& names) const;
	void Copy(wstring names, bool deleteFile = false) const;
	void Delete(wstring path) const;
	void Move(wstring names) const;

	void Help() const;
};