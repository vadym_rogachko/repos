#include <windows.h>
#include <iostream>
#include<fstream>
#include <iomanip>
#include <io.h>
#include<direct.h>
#include<memory>
#include"FileManager.h"

using namespace std;

wstring FileManager::directory = L"C:";

FileManager& FileManager::getReference() {
	static FileManager FileManager;
	return FileManager;
}

const wstring& FileManager::getDirectory() const {
	return directory;
}

void FileManager::setDirectory(wstring path) {
	Convert(path);
	CheckAccess(path, 2);
	_wfinddata_t fileinfo;
	intptr_t handle = getHandle(PathAndName(path, L"*"), fileinfo);
	directory = path;
	CloseHandle(handle);
}

void FileManager::ShowDirectory(wstring path) const {
	system("cls");
	if (path.size() == 0) {
		path = directory;
	}
	Convert(path);
	wcout << path << endl << endl;
	ShowFolders(path);
	ShowFiles(path);
}

void FileManager::MakeDirectory(wstring path) const {
	Convert(path);
	CheckPath(path);
	CheckAccess(path.substr(0, path.rfind(L'\\')), 4);
	if (_wmkdir(path.c_str()) == -1) {
		Error(path); throw errno;
	}
}

void FileManager::Rename(const wstring& paths) const {
	wstring oldPath, newPath;
	setPaths(paths, oldPath, newPath);
	Convert(oldPath);
	Convert(newPath);
	CheckPath(oldPath);
	CheckAccess(oldPath, 6);
	newPath = PathAndName(oldPath.substr(0, oldPath.rfind(L'\\')), newPath);
	if (!isSameDir(oldPath, newPath)) {
		throw runtime_error("Incorrect name");
	}
	if (_wrename(oldPath.c_str(), newPath.c_str()) != 0) {
		Error(newPath); throw errno;
	}
}

void FileManager::Copy(wstring paths, bool deleteFile) const {
	Convert(paths);
	wstring oldPath, newPath;
	setPaths(paths, oldPath, newPath);
	CheckPath(oldPath);
	CheckPath(newPath);
	CheckAccess(oldPath, 2);
	CheckAccess(newPath, 4);
	CheckPaths(oldPath, newPath);
	_wfinddata_t fileinfo;
	intptr_t handle = getHandle(oldPath, fileinfo);
	if (fileinfo.attrib & _A_SUBDIR) {
		CloseHandle(handle);
		wstring folderName = fileinfo.name;
		MakeDirectory(PathAndName(newPath, folderName));
		handle = getHandle(PathAndName(oldPath, L"*"), fileinfo);
		intptr_t result = handle;
		while (result != -1) {
			if (lstrcmp(fileinfo.name, L".") && lstrcmp(fileinfo.name, L"..")) {
				Copy(PathAndName(oldPath, fileinfo.name) + L" > " + PathAndName(newPath, folderName));
			}
			result = _wfindnext(handle, &fileinfo);
		}
		CloseHandle(handle);
	}
	else {
		CloseHandle(handle);
		if (!deleteFile) {
			CopyFile_(oldPath, PathAndName(newPath, fileinfo.name));
		}
		else {
			if (_wrename(oldPath.c_str(), (PathAndName(newPath, fileinfo.name)).c_str()) != 0) {
				Error(PathAndName(newPath, fileinfo.name)); throw errno;
			}
		}
	}
}

void FileManager::Delete(wstring path) const {
	Convert(path);
	CheckPath(path);
	CheckAccess(path, 0);
	_wfinddata_t fileinfo;
	intptr_t handle = getHandle(path, fileinfo);
	if (fileinfo.attrib & _A_SUBDIR) {
		CloseHandle(handle);
		handle = getHandle(PathAndName(path, L"*"), fileinfo);
		intptr_t result = handle;
		while (result != -1) {
			if (lstrcmp(fileinfo.name, L".") && lstrcmp(fileinfo.name, L"..")) {
				Delete(PathAndName(path, fileinfo.name));
			}
			result = _wfindnext(handle, &fileinfo);
		}
		CloseHandle(handle);
		if (_wrmdir(path.c_str()) == -1) {
			Error(path); throw errno;
		}
	}
	else {
		CloseHandle(handle);
		if (_wremove(path.c_str()) != 0) {
			Error(path); throw errno;
		}
	}
}

void FileManager::Move(wstring paths) const {
	Copy(paths, true);
	Convert(paths);
	CheckPath(paths);
	try {
		Delete(paths.substr(0, paths.find(L" > ")));
	}
	catch (int e) {
		if (e != ENOENT) {
			Error(paths.substr(0, paths.find(L" > ")));
			throw;
		}
	}
}

void FileManager::CopyFile_(const wstring& sourceFilePath, const wstring& destFilePath) const {
	ifstream fin;
	ofstream fout;
	fin.exceptions(ifstream::failbit | ifstream::badbit);
	fout.exceptions(ofstream::failbit | ofstream::badbit);
	fin.open(sourceFilePath.c_str(), ifstream::in | ifstream::binary | ifstream::ate);
	fout.open(destFilePath.c_str(), ofstream::out | ofstream::binary);
	streamoff sizeFile = fin.tellg();
	fin.seekg(0);
	fin.clear();
	int MB = 0x100000;
	int sizeBuffer = sizeFile > 100 * MB ? 100 * MB
		: sizeFile > 10 * MB ? 10 * MB
		: MB;
	unique_ptr<char[]> buffer(new char[sizeBuffer]);
	int countParts = sizeFile / sizeBuffer;
	int sizePart = sizeBuffer;
	try {
		while (countParts--) {
			fin.read(buffer.get(), sizePart);
			fout.write(buffer.get(), sizePart);
		}
		sizePart = sizeFile - (sizeFile / sizeBuffer * sizeBuffer);
		fin.read(buffer.get(), sizePart);
		fout.write(buffer.get(), sizePart);
	}
	catch (const ios_base::failure& e) {
		fout.close();
		if (_wremove(destFilePath.c_str()) != 0) {
			system("cls");
			_wperror(destFilePath.c_str());
		}
		Error(destFilePath); throw;
	}
	fin.close();
	fout.close();
}

void FileManager::ShowFolders(const wstring& path) const {
	CheckAccess(path, 2);
	_wfinddata_t fileinfo;
	intptr_t handle = getHandle(PathAndName(path, L"*"), fileinfo);
	intptr_t result = handle;
	while (result != -1) {
		if (fileinfo.attrib & _A_SUBDIR) {
			wcout << setw(40) << left << fileinfo.name;
			wchar_t buffer[30];
			_wctime_s(buffer, 30, &fileinfo.time_write);
			wcout << setw(30) << buffer << endl;
		}
		result = _wfindnext(handle, &fileinfo);
	}
	CloseHandle(handle);
}

void FileManager::ShowFiles(const wstring& path) const {
	CheckAccess(path, 2);
	_wfinddata_t fileinfo;
	intptr_t handle = getHandle(PathAndName(path, L"*"), fileinfo);
	intptr_t result = handle;
	while (result != -1) {
		if (!(fileinfo.attrib & _A_SUBDIR)) {
			wcout << setw(25) << fileinfo.name;
			wcout << setw(15) << fileinfo.size;
			wchar_t buffer[30];
			_wctime_s(buffer, 30, &fileinfo.time_write);
			wcout << setw(30) << buffer << endl;
		}
		result = _wfindnext(handle, &fileinfo);
	}
	CloseHandle(handle);
}

void FileManager::Convert(wstring& path) const {
	if (path[0] == L'.') {
		path = directory + path.substr(1);
	}
}

void FileManager::setPaths(const wstring& paths, wstring& oldPath, wstring& newPath) const {
	oldPath = paths.substr(0, paths.find(L" > "));
	newPath = paths.substr(paths.find(L" > ") + 3);
}

wstring FileManager::PathAndName(const wstring& path, const wstring& name) const {
	return path + L'\\' + name;
}

void FileManager::Error(const wstring& msg) const {
	system("cls");
	wcerr << msg << ": ";
}

void FileManager::CheckAccess(const wstring& path, int mode) const {
	if (_waccess(path.c_str(), mode) == -1) {
		Error(path); throw errno;
	}
}

void FileManager::CheckPath(const wstring& path) const {
	if (path.size() < 2 || path[1] != L':') {
		throw runtime_error("Incorrect path");
	}
}

void FileManager::CheckPaths(wstring& oldPath, wstring& newPath) const {
	if (oldPath.back() == L'\\') {
		oldPath.pop_back();
	}
	if (newPath.back() != L'\\') {
		newPath.push_back(L'\\');
	}
	if (isSameDir(oldPath, newPath)) {
		throw runtime_error("Incorrect path");
	}
	if (isSameDir(oldPath + L'\\', newPath)) {
		Error(L"Error"); throw runtime_error("Recursively copying");
	}
}

bool FileManager::isSameDir(const wstring& dir1, const wstring& dir2) const {
	return dir1.substr(0, dir1.rfind(L'\\')) == dir2.substr(0, dir2.rfind(L'\\'));
}

intptr_t FileManager::getHandle(const wstring& path, _wfinddata_t& fileinfo) const {
	intptr_t handle = _wfindfirst(path.c_str(), &fileinfo);
	if (handle == -1) {
		Error(path); throw errno;
	}
	return handle;
}

void FileManager::CloseHandle(intptr_t handle) const {
	if (_findclose(handle) == -1) {
		system("cls"); throw errno;
	}
}

void FileManager::Help() const {
	system("cls");
	cout << "Commands:\n\n"

		"COPY, DEL, RENAME, MKDIR, CHDIR, MOVE, EXIT, HELP\n\n"
		"..     - go to parent directory\n"
		"\\      - go to root directory\n\n"

		".      - current directory\n"
		".\\     - path to current directory\n"
		".\\name - show folder in current directory\n"
		"CHDIR .\\name - go to folder in current directory\n"


		"\n\n\nExaples:\n\n"

		"COPY path\\name > new_path\n"
		"COPY .\\name > D:\n"
		"COPY C:\\name > D:\\\n"
		"COPY C:\\folder\\name > C:\\otherFolder\\\n"

		"\nDEL path\\name\n"
		"DEL .\\name\n"
		"DEL C:\\name\n"

		"\nRENAME path\\old_name > new_name\n"
		"RENAME .\\name > otherName\n"
		"RENAME C:\\name > otherName\n"

		"\nMKDIR path\\name\n"
		"MKDIR .\\newFolder\n"
		"MKDIR C:\\newFolder\n"

		"\nCHDIR path\\name\n"
		"CHDIR .\\name\n"
		"CHDIR C:\\name\n"

		"\nMOVE path\\name > new_path\n"
		"MOVE .\\name > C:\\\n"
		"MOVE C:\\folder\\name > D:\n"
		"MOVE D:\\folder\\name > C:\\otherFolder\n";
	cin.get();
}