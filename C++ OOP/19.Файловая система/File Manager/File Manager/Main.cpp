#ifndef UNICODE
#define UNICODE
#endif
#include<windows.h>
#include<iostream>
#include<string>
#include<algorithm>
#include"FileManager.h"

using namespace std;

void main(int argc, const char **argv) {

	wcout.imbue(locale("rus_rus.866"));
	wcin.imbue(locale("rus_rus.866"));

	FileManager& manager = FileManager::getReference();
	int length = MultiByteToWideChar(CP_ACP, 0, argv[0], -1, nullptr, 0);
	wchar_t *ptr = new wchar_t[length];
	MultiByteToWideChar(CP_ACP, 0, argv[0], -1, ptr, length);
	wstring path = ptr;
	delete[] ptr;
	ptr = nullptr;
	path.erase(path.begin() + path.rfind(L'\\'), path.end());
	wstring action;
	manager.setDirectory(path);
	while (true) {
		manager.ShowDirectory();
		getline(wcin, action);
		try {
			if (action == L"..") {
				if (path.size() == 2) {
					system("cls"); throw runtime_error("This is root directory");
				}
				path.erase(path.begin() + path.rfind(L'\\'), path.end());
				manager.setDirectory(path);
			}
			else if (action[0] == L'.') {
				manager.ShowDirectory(action);
				system("pause");
			}
			else if (action == L"\\") {
				path.erase(path.begin() + 2, path.end());
				manager.setDirectory(path);
			}
			else if (action.substr(0, 6) == L"CHDIR ") {
				manager.setDirectory(action.substr(6));
				path = manager.getDirectory();
			}
			else if (action.substr(0, 6) == L"MKDIR ") {
				manager.MakeDirectory(action.substr(6));
			}
			else if (action.substr(0, 7) == L"RENAME ") {
				manager.Rename(action.substr(7));
			}
			else if (action.substr(0, 5) == L"MOVE ") {
				manager.Move(action.substr(5));
			}
			else if (action.substr(0, 5) == L"COPY ") {
				manager.Copy(action.substr(5));
			}
			else if (action.substr(0, 4) == L"DEL ") {
				manager.Delete(action.substr(4));
			}
			else if (action.substr(0, 4) == L"HELP") {
				manager.Help();
			}
			else if (action == L"EXIT") return;
		}
		catch (const exception& e) {
			cerr << e.what() << endl;
			cin.get();
		}
		catch (int e) {
			char msg[512]{};
			strerror_s(msg, e);
			cerr << msg << endl;
			cin.get();
		}
	}
}