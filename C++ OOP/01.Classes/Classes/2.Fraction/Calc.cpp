#include<iostream>
#include<conio.h>
#include"CFraction.h"
#include"enum.h"

using std::cout;
using std::cin;
using std::endl;

void GetFraction(CFraction &fract) {
	int numerator, denominator;
	cout << "������� ��������� � ����������� �����\n";
	cin >> numerator >> denominator;
	fract.InitFract(numerator, denominator);
}

char GetAction() {
	char ch;
	do {
		system("cls");
		cout << "�������� ��������:\n"
				"1.��������\n"
				"2.���������\n"
				"3.���������\n"
				"4.�������\n"
				"Esc - �����\n";
		ch = _getch();
	} while (ch != PLUS && ch != MINUS && ch != MULTI && ch != DIV && ch != ESC);
	return ch;
}

CFraction Calc(CFraction fract1, CFraction fract2, char &ch) {
	CFraction fractRes;
	switch (ch) {
	case PLUS:
		fractRes = PlusFraction(fract1, fract2);
		ch = '+';
		break;
	case MINUS:
		fractRes = MinusFraction(fract1, fract2);
		ch = '-';
		break;
	case MULTI:
		fractRes = MultiFraction(fract1, fract2);
		ch = '*';
		break;
	case DIV:
		if (fract2.getNumerator() != 0) {
			fractRes = DivFraction(fract1, fract2);
		}
		ch = '/';
		break;
	}
	fractRes.ToInteger();
	return fractRes;
}

void ShowRes(CFraction fract1, CFraction fract2, CFraction fractRes, char sign) {
	cout << endl;
	if (fract2.getNumerator() == 0 && sign == '/') {
		cout << "������\n\n";
		return;
	}
	fract1.Print();
	cout << ' ' << sign << ' ';
	fract2.Print();
	cout << " = ";
	if (fractRes.getNumerator() == 0) {
		cout << fractRes.getInteger();
	}
	else {
		fractRes.Print();
	}
	cout << endl << endl;
}