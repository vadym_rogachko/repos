#include<iostream>
#include"CFraction.h"
#include"Calc.h"
#include"enum.h"

using std::cout;
using std::endl;

void main() {

	setlocale(LC_ALL, "rus");

	/*2. ����������� ����� CFraction. ���������� ����� �������������, � ����� ����� ������������ ��������-�����������. ������������� � ������ ������ ��������� ����� ����� � �������� ����� � ������������ �����.
		 �������� �������, ����������� � �������� ���������� ��� ����� � ����������� �������� ��������, ���������, ��������� � ������� ������.*/

	{
		/*������� ����� � ������������ �����*/
		CFraction fract1(5, 4);
		CFraction fract2(1, 8);
		CFraction fract3(4, 1, 5);
		fract1.Print();
		cout << " --> " << fract1.RealNum() << endl;
		fract2.Print();
		cout << " --> " << fract2.RealNum() << endl;
		fract3.Print();
		cout << " --> " << fract3.RealNum() << endl << endl;
		system("pause");
		system("cls");
	}

	/*�����������(� ���������� ����� �����)*/
	CFraction fract1, fract2, fractRes;
	char ch;
	while (true) {
		GetFraction(fract1);
		GetFraction(fract2);
		if ((ch = GetAction()) == ESC) break;
		fractRes = Calc(fract1, fract2, ch);
		ShowRes(fract1, fract2, fractRes, ch);
		system("pause");
		system("cls");
	}
	system("pause");
}