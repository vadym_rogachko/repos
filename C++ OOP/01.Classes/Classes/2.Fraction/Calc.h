#pragma once

void GetFraction(CFraction &fract);
char GetAction();
CFraction Calc(CFraction fract1, CFraction fract2, char &ch);
void ShowRes(CFraction fract1, CFraction fract2, CFraction fractRes, char sign);