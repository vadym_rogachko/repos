#pragma once

class CFraction {

	int numerator;
	int denominator;
	int integer;

public:

	int getNumerator() {
		return numerator;
	}
	int getDenominator() {
		return denominator;
	}
	int getInteger() {
		return integer;
	}

	void setNumerator(int n) {
		numerator = n;
	}
	void setDenominator(int d) {
		denominator = d == 0 ? 1 : d;
	}
	void setInteger(int i) {
		integer = i;
	}

	CFraction();
	CFraction(int n, int d);
	CFraction(int i, int n, int d);

	void ToInteger();
	void NoInteger();
	double RealNum();
	void Print();
	void CutFraction();
	void InitFract(int n, int d);
	void InitFract(int i, int n, int d);
	void InitZero();
};

CFraction PlusFraction(CFraction fract1, CFraction fract2);
CFraction MinusFraction(CFraction fract1, CFraction fract2);
CFraction MultiFraction(CFraction fract1, CFraction fract2);
CFraction DivFraction(CFraction fract1, CFraction fract2);