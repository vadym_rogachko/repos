#include<iostream>
#include"CFraction.h"

using std::cout;

CFraction::CFraction() {
	numerator = 0;
	denominator = 1;
	integer = 0;
}

CFraction::CFraction(int n, int d) {
	numerator = n;
	d != 0 ? (denominator = d) : (denominator = 1);
	integer = 0;
}

CFraction::CFraction(int i, int n, int d) {
	numerator = n;
	denominator = d == 0 ? 1 : d;
	integer = i;
}

void CFraction::ToInteger() {
	integer += numerator / denominator;
	numerator %= denominator;
}

void CFraction::NoInteger() {
	numerator += integer * denominator;
	integer = 0;
}

double CFraction::RealNum() {
	return integer + ((double)numerator / denominator);
}

void CFraction::Print() {
	if (integer != 0) {
		cout << integer << ' ';
	}
	cout << numerator << '/' << denominator;
}

void CFraction::InitFract(int n, int d) {
	numerator = n;
	denominator = d == 0 ? 1 : d;
	integer = 0;
}

void CFraction::InitFract(int i, int n, int d) {
	numerator = n;
	denominator = d == 0 ? 1 : d;
	integer = i;
}

void CFraction::InitZero() {
	numerator = 0;
	denominator = 1;
	integer = 0;
}

// ���������� �����
void CFraction::CutFraction() {
	bool minus = numerator < 0;
	if (minus) {
		numerator *= -1;
	}
	if (numerator % denominator == 0) {
		numerator = numerator / denominator;
		denominator = 1;
		if (minus) {
			numerator *= -1;
		}
		return;
	}
	if (denominator % numerator == 0) {
		denominator = denominator / numerator;
		numerator = 1;
		if (minus) {
			numerator *= -1;
		}
		return;
	}
	short num = 2;
	while (num < numerator && num < denominator) {
		if (numerator % num == 0 && denominator % num == 0) {
			numerator = numerator / num;
			denominator = denominator / num;
			num = 2;
			continue;
		}
		++num;
	}
	if (minus) {
		numerator *= -1;
	}
}

CFraction PlusFraction(CFraction fract1, CFraction fract2) {
	CFraction fract;
	fract1.NoInteger();
	fract2.NoInteger();
	if (fract1.getDenominator() == fract2.getDenominator()) {
		fract.setDenominator(fract1.getDenominator());
		fract.setNumerator(fract1.getNumerator() + fract2.getNumerator());
		fract.CutFraction();
		return fract;
	}
	// ���������� ������ ����������� � ����������
	short num = 2;
	while (fract1.getDenominator() * num % fract2.getDenominator() != 0) {
		++num;
	}
	fract.setDenominator(fract1.getDenominator() * num);
	fract.setNumerator(fract1.getNumerator() * num);
	num = fract.getDenominator() / fract2.getDenominator();
	fract.setNumerator(fract.getNumerator() + (fract2.getNumerator() * num));
	fract.CutFraction();
	return fract;
}

CFraction MinusFraction(CFraction fract1, CFraction fract2) {
	CFraction fract;
	fract1.NoInteger();
	fract2.NoInteger();
	if (fract1.getDenominator() == fract2.getDenominator()) {
		fract.setDenominator(fract1.getDenominator());
		fract.setNumerator(fract1.getNumerator() - fract2.getNumerator());
		fract.CutFraction();
		return fract;
	}
	// ���������� ������ ����������� � ����������
	short num = 2;
	while (fract1.getDenominator() * num % fract2.getDenominator() != 0) {
		++num;
	}
	fract.setDenominator(fract1.getDenominator() * num);
	fract.setNumerator(fract1.getNumerator() * num);
	num = fract.getDenominator() / fract2.getDenominator();
	fract.setNumerator(fract.getNumerator() - (fract2.getNumerator() * num));
	fract.CutFraction();
	return fract;
}

CFraction MultiFraction(CFraction fract1, CFraction fract2) {
	CFraction fract;
	fract1.NoInteger();
	fract2.NoInteger();
	fract.setNumerator(fract1.getNumerator() * fract2.getNumerator());
	fract.setDenominator(fract1.getDenominator() * fract2.getDenominator());
	fract.CutFraction();
	return fract;
}

CFraction DivFraction(CFraction fract1, CFraction fract2) {
	CFraction fract;
	fract1.NoInteger();
	fract2.NoInteger();
	fract.setNumerator(fract1.getNumerator() * fract2.getDenominator());
	fract.setDenominator(fract1.getDenominator() * fract2.getNumerator());
	fract.CutFraction();
	return fract;
}