#include<iostream>
#include<math.h>
#include<iomanip>
#include"CRectangle.h"

using std::swap;
using std::cout;
using std::endl;
using std::setw;

CRect::CRect() {
	left = 0;
	right = 0;
	top = 0;
	bottom = 0;
}
CRect::CRect(int l, int t, int r, int b) {
	left = l;
	right = r;
	top = t;
	bottom = b;
}

int CRect::Width() {
	return right - left;
}

int CRect::Height() {
	return bottom - top;
}

bool CRect::IsRectNull() {
	return left == 0 && right == 0
		&& top == 0 && bottom == 0;
}

bool CRect::PtInRect(int x, int y) {
	return x >= left && x <= right
		&& y >= top && y <= bottom;
}

void CRect::SetRect(int x1, int y1, int x2, int y2) {
	left = x1;
	right = x2;
	top = y1;
	bottom = y2;
}

void CRect::SetRectEmpty() {
	left = 0;
	right = 0;
	top = 0;
	bottom = 0;
}

void CRect::InflateRect(int l, int t, int r, int b) {
	left -= l;
	right += r;
	top -= t;
	bottom += b;
}

void CRect::DeflateRect(int l, int t, int r, int b) {
	left += l;
	right -= r;
	top += t;
	bottom -= b;
}

// ������ �������
void CRect::NormalizeRect() {
	if (left > right) swap(left, right);
	if (top > bottom) swap(top, bottom);
}

// ������ �������
void CRect::NormalizeRect_() {
	(left > right) &&
		(left ^= right, right = left ^ right, left ^= right);
	(top > bottom) &&
		(top ^= bottom, bottom = top ^ bottom, top ^= bottom);
}

void CRect::OffsetRect(int x, int y) {
	left += x;
	right += x;
	top += y;
	bottom += y;
}

void CRect::Print() {
	cout << "x1 = " << setw(3) << std::left << left << " y1 = " << top << endl;
	cout << "x2 = " << setw(3) << std::left << right << " y2 = " << top << endl;
	cout << "x3 = " << setw(3) << std::left << right << " y3 = " << bottom << endl;
	cout << "x4 = " << setw(3) << std::left << left << " y4 = " << bottom << endl;
}

int CRect::Area() {
	return Width() * Height();
}

int CRect::Perimeter() {
	return 2 * (Width() + Height());
}

double CRect::Diagonal() {
	return sqrt((Width() * Width()) + (Height() * Height()));
}