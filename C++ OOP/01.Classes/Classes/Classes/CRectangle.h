#pragma once

class CRect {

	int left;
	int right;
	int top;
	int bottom;

public:

	int getLeft() {
		return left;
	}
	int getRight() {
		return right;
	}
	int getTop() {
		return top;
	}
	int getBottom() {
		return bottom;
	}

	void setLeft(int l) {
		left = l;
	}
	void setRight(int r) {
		right = r;
	}
	void setTop(int t) {
		top = t;
	}
	void setBottom(int b) {
		bottom = b;
	}

	CRect();
	CRect(int l, int t, int r, int b);

	int Width();
	int Height();
	bool IsRectNull();
	bool PtInRect(int x, int y);
	void SetRect(int x1, int y1, int x2, int y2);
	void SetRectEmpty();
	void InflateRect(int l, int t, int r, int b);
	void DeflateRect(int l, int t, int r, int b);
	void NormalizeRect();
	void NormalizeRect_();
	void OffsetRect(int x, int y);
	void Print();
	int Area();
	int Perimeter();
	double Diagonal();
};