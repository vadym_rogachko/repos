#include<iostream>
#include"CRectangle.h"

using std::cout;
using std::endl;

void main() {

	setlocale(LC_ALL, "rus");

	/*1. ����������� ����� CRectangle. ���������� ����� �������������, � ����� ����� ������������ ��������-�����������. ������������� � ������ ������ ��� ������������ ��������������,
	����������� ��������������, ��������� ������� ��������������, ���������� ������� �������������� � ��.*/

	CRect rect1;
	CRect rect2(4, 2, 12, 6);
	cout << "����������� �� ���������\n";
	cout << "rect1:\n";
	rect1.Print();
	cout << "rect1: " << (rect1.IsRectNull() ? "��� ��������� == 0\n" : "��� ��������� != 0\n") << endl;
	cout << "����������� � �����������\n";
	cout << "rect2:\n";
	rect2.Print();
	cout << "rect2: " << (rect2.IsRectNull() ? "��� ��������� == 0\n" : "��� ��������� != 0\n") << endl;
	cout << "������-���������\n";
	rect1.setLeft(rect2.getLeft());
	rect1.setTop(rect2.getTop());
	rect1.setRight(rect2.getRight());
	rect1.setBottom(rect2.getBottom());
	cout << "rect1:\n";
	rect1.Print();
	cout << "rect1: " << (rect1.IsRectNull() ? "��� ��������� == 0\n" : "��� ��������� != 0\n") << endl;
	cout << "����� x = 5 y = 3 " << (rect1.PtInRect(5, 3) ? "����� ������ �������������� " : "����� ������� �������������� ") << "rect1\n";
	cout << "����� x = 5 y = 12 " << (rect1.PtInRect(5, 12) ? "����� ������ �������������� " : "����� ������� �������������� ") << "rect1\n" << endl;
	cout << "��������� ���� ��������� �������������� � 0\n";
	rect2.SetRectEmpty();
	cout << "rect2:\n";
	rect2.Print();
	cout << "rect2: " << (rect2.IsRectNull() ? "��� ��������� == 0\n" : "��� ��������� != 0\n") << endl;
	cout << "��������� ������� � ���������\n";
	rect2.SetRect(7, 0, 0, 3);
	cout << "������������ �1\n";
	rect2.NormalizeRect();
	cout << "rect2:\n";
	rect2.Print();
	cout << endl;
	cout << "���������� �������\n";
	rect1.InflateRect(4, 2, 0, 1);
	cout << "rect1:\n";
	rect1.Print();
	cout << endl;
	cout << "���������� �������\n";
	rect2.DeflateRect(2, 1, 4, 0);
	cout << "rect2:\n";
	rect2.Print();
	cout << endl;
	rect2.SetRect(0, 3, 7, 0);
	cout << "������������ �2\n";
	rect2.NormalizeRect_();
	cout << "rect2:\n";
	rect2.Print();
	cout << endl;
	cout << "��������\n";
	rect2.OffsetRect(5, 3);
	cout << "rect2:\n";
	rect2.Print();
	cout << endl;
	cout << "��������, �������, ����� ���������\n";
	cout << "rect1:\n";
	rect1.Print();
	cout << "������ " << rect1.Width() << endl;
	cout << "������ " << rect1.Height() << endl;
	cout << "P = " << rect1.Perimeter() << "\nS = " << rect1.Area() << "\n��������� = " << int((rect1.Diagonal() * 10) + .5) / 10. << endl << endl;
	system("pause");
}