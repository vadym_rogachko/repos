#pragma once

class CFraction {

	int integer;
	int numerator;
	int denominator;

public:

	CFraction();
	CFraction(int n, int d);
	CFraction(int i, int n, int d);

	int getNumerator() const;
	int getDenominator() const;
	int getInteger() const;

	void setNumerator(int n);
	void setDenominator(int d);
	void setInteger(int i);

	CFraction operator +(const CFraction &fract) const;
	CFraction operator -(const CFraction &fract) const;
	CFraction operator *(const CFraction &fract) const;
	CFraction operator /(const CFraction &fract) const;

	bool operator ==(const CFraction &fract) const;
	bool operator !=(const CFraction &fract) const;
	bool operator >(const CFraction &fract) const;
	bool operator <(const CFraction &fract) const;
	bool operator >=(const CFraction &fract) const;
	bool operator <=(const CFraction &fract) const;

	void ToInteger();
	void NoInteger();
	double RealNum() const;
	void Print() const;
	void CutFraction();
	void InitFract(int n, int d);
	void InitFract(int i, int n, int d);
	void InitZero();
};