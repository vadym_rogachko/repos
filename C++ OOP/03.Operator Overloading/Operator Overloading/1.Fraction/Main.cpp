﻿#include<iostream>
#include"CFractionMethods.h"
#include"Calc.h"

/*	1. Для разработанного ранее класса CFraction перегрузить арифметические операции сложения вычитания, умножения и деления, а также операции отношения. Предусмотреть два способа перегрузки:
	   - методами класса;
	   - глобальными функциями.
*/

using std::cout;
using std::endl;
using std::boolalpha;

void main() {

	setlocale(LC_ALL, "rus");

	{
		/*Тест операций отношений*/

		CFraction fract1(5, 4);
		CFraction fract2(1, 8);
		CFraction fract3(4, 1, 5);
		
		/* fract1 && fract2 */
		fract1.Print();
		cout << " == ";
		fract2.Print();
		cout << ' ';
		cout << boolalpha << (fract1 == fract2) << endl;

		fract1.Print();
		cout << " != ";
		fract2.Print();
		cout << ' ';
		cout << boolalpha << (fract1 != fract2) << endl;

		fract1.Print();
		cout << " >  ";
		fract2.Print();
		cout << ' ';
		cout << boolalpha << (fract1 > fract2) << endl;

		fract1.Print();
		cout << " <  ";
		fract2.Print();
		cout << ' ';
		cout << boolalpha << (fract1 < fract2) << endl;

		fract1.Print();
		cout << " >= ";
		fract2.Print();
		cout << ' ';
		cout << boolalpha << (fract1 >= fract2) << endl;

		fract1.Print();
		cout << " <= ";
		fract2.Print();
		cout << ' ';
		cout << boolalpha << (fract1 <= fract2) << endl << endl;

		/* fract1 && fract3 */
		fract1.Print();
		cout << " == ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract1 == fract3) << endl;

		fract1.Print();
		cout << " != ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract1 != fract3) << endl;

		fract1.Print();
		cout << " >  ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract1 > fract3) << endl;

		fract1.Print();
		cout << " <  ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract1 < fract3) << endl;

		fract1.Print();
		cout << " >= ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract1 >= fract3) << endl;

		fract1.Print();
		cout << " <= ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract1 <= fract3) << endl << endl;


		/* fract2 && fract3 */
		fract2.Print();
		cout << " == ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract2 == fract3) << endl;

		fract2.Print();
		cout << " != ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract2 != fract3) << endl;

		fract2.Print();
		cout << " >  ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract2 > fract3) << endl;

		fract2.Print();
		cout << " <  ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract2 < fract3) << endl;

		fract2.Print();
		cout << " >= ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract2 >= fract3) << endl;

		fract2.Print();
		cout << " <= ";
		fract3.Print();
		cout << ' ';
		cout << boolalpha << (fract2 <= fract3) << endl << endl;

		system("pause");
		system("cls");
	}

	CFraction fract1, fract2, fractRes;
	char ch;
	while (true) {
		GetFraction(fract1);
		GetFraction(fract2);
		if ((ch = GetAction()) == ESC) break;
		fractRes = Calc(fract1, fract2, ch);
		ShowRes(fract1, fract2, fractRes, ch);
		system("pause >> void");
		system("cls");
	}
	system("cls");
	system("pause");
}