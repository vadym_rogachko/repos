#include<iostream>
#include"CFractionMethods.h"

using std::cout;

CFraction::CFraction() : numerator(0), denominator(1), integer(0) {}

CFraction::CFraction(int n, int d) : numerator(n), integer(0), denominator(d > 0 ? d : 1) {}

CFraction::CFraction(int i, int n, int d) : integer(i), numerator(i < 0 ? abs(n) : n), denominator(d > 0 ? d : 1) {}

int CFraction::getNumerator() const {
	return numerator;
}

int CFraction::getDenominator() const {
	return denominator;
}

int CFraction::getInteger() const {
	return integer;
}

void CFraction::setInteger(int i) {
	integer = i;
}

void CFraction::setNumerator(int n) {
	numerator = integer < 0 ? abs(n) : n;
}

void CFraction::setDenominator(int d) {
	denominator = d > 0 ? d : 1;
}

CFraction CFraction::operator +(const CFraction &fract) const {
	CFraction temp;
	// ������� � ���������� ��������� ��-�� ������������ ���������� ������ - � ����������� ������������ ����� NoInteger, ���������� ������(�������� � ������������ ����� ���� ���� ����� �����)
	CFraction fract1 = *this;
	CFraction fract2 = fract;
	fract1.NoInteger();
	fract2.NoInteger();
	if (fract1.denominator == fract2.denominator) {
		temp.denominator = fract1.denominator;
		temp.numerator = fract1.numerator + fract2.numerator;
		temp.CutFraction();
		return temp;
	}
	short num = 2;
	while (fract1.denominator * num % fract2.denominator != 0) {
		++num;
	}
	temp.denominator = fract1.denominator * num;
	temp.numerator = fract1.numerator * num;
	num = temp.denominator / fract2.denominator;
	temp.numerator += fract2.numerator * num;
	temp.CutFraction();
	return temp;
}

CFraction CFraction::operator -(const CFraction &fract) const {
	CFraction temp;
	CFraction fract1 = *this;
	CFraction fract2 = fract;
	fract1.NoInteger();
	fract2.NoInteger();
	if (fract1.denominator == fract2.denominator) {
		temp.denominator = fract1.denominator;
		temp.numerator = fract1.numerator - fract2.numerator;
		temp.CutFraction();
		return temp;
	}
	short num = 2;
	while (fract1.denominator * num % fract2.denominator != 0) {
		++num;
	}
	temp.denominator = fract1.denominator * num;
	temp.numerator = fract1.numerator * num;
	num = temp.denominator / fract2.denominator;
	temp.numerator += fract2.numerator * num;
	temp.CutFraction();
	return temp;
}

CFraction CFraction::operator *(const CFraction &fract) const {
	CFraction temp;
	CFraction fract1 = *this;
	CFraction fract2 = fract;
	fract1.NoInteger();
	fract2.NoInteger();
	temp.numerator = fract1.numerator * fract2.numerator;
	temp.denominator = fract1.denominator * fract2.denominator;
	temp.CutFraction();
	return temp;
}

CFraction CFraction::operator /(const CFraction &fract) const {
	CFraction temp;
	CFraction fract1 = *this;
	CFraction fract2 = fract;
	fract1.NoInteger();
	fract2.NoInteger();
	temp.numerator = fract1.numerator * fract2.denominator;
	temp.denominator = fract1.denominator * fract2.numerator;
	temp.CutFraction();
	return temp;
}

bool CFraction::operator ==(const CFraction &fract) const {
	return RealNum() == fract.RealNum();
}

bool CFraction::operator !=(const CFraction &fract) const {
	return RealNum() != fract.RealNum();
}

bool CFraction::operator >(const CFraction &fract) const {
	return RealNum() > fract.RealNum();
}

bool CFraction::operator <(const CFraction &fract) const {
	return RealNum() < fract.RealNum();
}

bool CFraction::operator >=(const CFraction &fract) const {
	return RealNum() >= fract.RealNum();
}

bool CFraction::operator <=(const CFraction &fract) const {
	return RealNum() <= fract.RealNum();
}

void CFraction::ToInteger() {
	integer += numerator / denominator;
	numerator %= denominator;
	if (integer < 0) {
		numerator = abs(numerator);
	}
}

void CFraction::NoInteger() {
	numerator += integer * denominator;
	integer = 0;
}

double CFraction::RealNum() const {
	return integer + ((double)numerator / denominator);
}

void CFraction::Print() const {
	if (integer != 0) {
		cout << integer << ' ';
	}
	cout << numerator << '/' << denominator;
}

void CFraction::InitFract(int n, int d) {
	integer = 0;
	numerator = n;
	denominator = d > 0 ? d : 1;
}

void CFraction::InitFract(int i, int n, int d) {
	integer = i;
	numerator = integer < 0 ? abs(n) : n;
	denominator = d > 0 ? d : 1;
}

void CFraction::InitZero() {
	numerator = 0;
	denominator = 1;
	integer = 0;
}

void CFraction::CutFraction() {
	bool minus = numerator < 0 || integer < 0;
	if (minus) {
		numerator *= -1;
	}
	if (numerator % denominator == 0) {
		numerator = numerator / denominator;
		denominator = 1;
		if (minus) {
			if (integer == 0) {
				numerator *= -1;
			}
		}
		return;
	}
	if (denominator % numerator == 0) {
		denominator = denominator / numerator;
		numerator = 1;
		if (minus) {
			if (integer == 0) {
				numerator *= -1;
			}
		}
		return;
	}
	short num = 2;
	while (num < numerator && num < denominator) {
		if (numerator % num == 0 && denominator % num == 0) {
			numerator = numerator / num;
			denominator = denominator / num;
			num = 2;
			continue;
		}
		++num;
	}
	if (minus) {
		if (integer == 0) {
			numerator *= -1;
		}
	}
}