#pragma once

void GetFraction(CFraction &fract);
char GetAction();
CFraction Calc(const CFraction &fract1, const CFraction &fract2, char &ch);
void ShowRes(const CFraction &fract1, const CFraction &fract2, const CFraction &fractRes, char sign);

enum actions {
	PLUS = '1',
	MINUS,
	MULTI,
	DIV,
	ESC = 27
};