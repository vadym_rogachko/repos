#include<iostream>
#include"CDate.h"

using std::cout;
using std::endl;

CDate::CDate() : day(1), month(1), year(1) {}

CDate::CDate(int day, int month, int year) {
	if (year < 1) {
		this->year = 1;
	}
	else {
		this->year = year;
	}
	if (month < 1 || month > 12) {
		this->month = 1;
	}
	else {
		this->month = month;
	}
	switch (this->month) {
	case JAN:
	case MAR:
	case MAY:
	case JUL:
	case AUG:
	case OCT:
	case DEC:
		if (day < 1 || day > 31) {
			this->day = 1;
		}
		else {
			this->day = day;
		}
		break;
	case APR:
	case JUN:
	case SEP:
	case NOV:
		if (day < 1 || day > 30) {
			this->day = 1;
		}
		else {
			this->day = day;
		}
		break;
	case FEB:
		if (isLeapYear()) {
			if (day < 1 || day > 29) {
				this->day = 1;
			}
			else {
				this->day = day;
			}
		}
		else {
			if (day < 1 || day > 28) {
				this->day = 1;
			}
			else {
				this->day = day;
			}
		}
		break;
	}
}

int CDate::getDay() const {
	return day;
}

int CDate::getMonth() const {
	return month;
}

int CDate::getYear() const {
	return year;
}

void CDate::setDay(int day) {
	switch(month) {
		case JAN:
		case MAR:
		case MAY:
		case JUL:
		case AUG:
		case OCT:
		case DEC:
			if (day < 1 || day > 31) {
				this->day = 1;
			}
			else {
				this->day = day;
			}
			break;
		case APR:
		case JUN:
		case SEP:
		case NOV:
			if (day < 1 || day > 30) {
				this->day = 1;
			}
			else {
				this->day = day;
			}
			break;
		case FEB:
			if (isLeapYear()) {
				if (day < 1 || day > 29) {
					this->day = 1;
				}
				else {
					this->day = day;
				}
			}
			else {
				if (day < 1 || day > 28) {
					this->day = 1;
				}
				else {
					this->day = day;
				}
			}
			break;
	}
}

void CDate::setMonth(int month) {
	if (month < 1 || month > 12) {
		this->month = 1;
		return;
	}
	this->month = month;
}

void CDate::setYear(int year) {
	if (year < 1) {
		this->year = 1;
		return;
	}
	this->year = year;
}

CDate CDate::operator +(int days) const {
	days += getAllDays();
	CDate date;
	date.setDate(days);
	return date;
}

int CDate::operator -(const CDate &date) const {
	return abs(getAllDays() - date.getAllDays());
}

CDate& CDate::operator ++() {
	setDate(getAllDays() + 1);
	return *this;
}

CDate& CDate::operator --() {
	setDate(getAllDays() - 1);
	return *this;
}

const CDate CDate::operator ++(int) {
	CDate temp = *this;
	setDate(getAllDays() + 1);
	return temp;
}

const CDate CDate::operator --(int) {
	CDate temp = *this;
	setDate(getAllDays() - 1);
	return temp;
}

bool CDate::operator >(const CDate &date) const {
	if (year > date.year) return true;
	if (year < date.year) return false;
	if (month > date.month) return true;
	if (month < date.month) return false;
	if (day > date.day) return true;
	return false;
}

bool CDate::operator <(const CDate &date) const {
	if (year < date.year) return true;
	if (year > date.year) return false;
	if (month < date.month) return true;
	if (month > date.month) return false;
	if (day < date.day) return true;
	return false;
}

bool CDate::operator ==(const CDate &date) const {
	return day == date.day &&
		month == date.month &&
		year == date.year;
}

bool CDate::operator !=(const CDate &date) const {
	return !(day == date.day &&
		month == date.month &&
		year == date.year);
}

const char* CDate::DayOfWeek() const {
	enum daysOfWeek { MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY };
	CDate date(1, 1, 1);
	int dayOfWeek = (*this - date) % 7;
	switch (dayOfWeek) {
	case MONDAY:
		return "�����������";
	case TUESDAY:
		return "�������";
	case WEDNESDAY:
		return "�����";
	case THURSDAY:
		return "�������";
	case FRIDAY:
		return "�������";
	case SATURDAY:
		return "�������";
	case SUNDAY:
		return "�����������";
	}
}

void CDate::PrintDate() const {
	cout << day << '.' << month << '.' << year;
}

bool CDate::isLeapYear() const {
	return (year % 4 == 0 && year % 100 != 0) || (year % 4 == 0 && year % 400 == 0);
}

int CDate::getAllDays() const {
	CDate temp(1, 1, 1);
	int allDays = 0;
	while (temp.year < year) {
		if (temp.isLeapYear()) {
			allDays += 366;
		}
		else {
			allDays += 365;
		}
		++temp.year;
	}
	while (temp.month < month) {
		switch (temp.month) {
		case JAN:
		case MAR:
		case MAY:
		case JUL:
		case AUG:
		case OCT:
		case DEC:
			allDays += 31;
			break;
		case APR:
		case JUN:
		case SEP:
		case NOV:
			allDays += 30;
			break;
		case FEB:
			if (temp.isLeapYear()) {
				allDays += 29;
			}
			else {
				allDays += 28;
			}
			break;
		}
		++temp.month;
	}
	allDays += day;
	return allDays;
}

void CDate::setDate(int days) {
	day = month = year = 1;
	while (days > 0) {
		if (isLeapYear() && days >= 366) {
			++year;
			days -= 366;
			continue;
		}
		else if (!isLeapYear() && days >= 365) {
			++year;
			days -= 365;
			continue;
		}
		if (days <= 31) {
			day = days;
			return;
		}
		while (month < 12) {
			switch (month) {
			case JAN:
			case MAR:
			case MAY:
			case JUL:
			case AUG:
			case OCT:
			case DEC:
				if (days <= 31) {
					day = days;
					return;
				}
				days -= 31;
				++month;
				break;
			case APR:
			case JUN:
			case SEP:
			case NOV:
				if (days <= 30) {
					day = days;
					return;
				}
				days -= 30;
				++month;
				break;
			case FEB:
				if (isLeapYear() && days <= 29) {
					day = days;
					return;
				}
				if (!isLeapYear() && days <= 28) {
					day = days;
					return;
				}
				if (isLeapYear()) {
					days -= 29;
				}
				else {
					days -= 28;
				}
				++month;
				break;
			}
		}
		day = days;
		return;
	}
}