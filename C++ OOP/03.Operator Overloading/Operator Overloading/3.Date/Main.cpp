#include<iostream>
#include"CDate.h"

using std::cout;
using std::endl;
using std::boolalpha;

void main() {
	
	setlocale(LC_ALL, "rus");

	CDate error(100, 387, -11);
	cout << "������������ ������\n";
	error.PrintDate();
	cout << endl;

	cout << "\n���������� ������\n";
	CDate bDay(23, 3, 1991);
	CDate today(15, 6, 2017);
	CDate yearAgo(15, 6, 2016);
	bDay.PrintDate();
	cout << ' ' << bDay.DayOfWeek() << (bDay.isLeapYear() ? " ��� ����������\n" : " ��� �� ����������\n");
	today.PrintDate();
	cout << ' ' << today.DayOfWeek() << (today.isLeapYear() ? " ��� ����������\n" : " ��� �� ����������\n");
	yearAgo.PrintDate();
	cout << ' ' << yearAgo.DayOfWeek() << (yearAgo.isLeapYear() ? "   ��� ����������\n" : "   ��� �� ����������\n");

	cout << "\n������� � ����\n";
	cout << today - yearAgo << endl;
	cout << today - bDay << endl;

	cout << "\n����������� ����\n";
	yearAgo = yearAgo + 350;
	yearAgo.PrintDate();
	cout << endl;

	cout << "\n��������� ����������\n";
	++yearAgo;
	yearAgo.PrintDate();
	cout << endl;

	cout << "\n��������� �����������\n";
	yearAgo++;
	yearAgo.PrintDate();
	cout << endl;

	cout << "\n��������� ���������� � ���������\n";
	cout << "� ������������ �������������� �����\n";
	today = --today + (-11);
	today.PrintDate();
	cout << endl;
	cout << "\n��������� ����������� � ��������� - ������� ����\n";
	today = today-- + 12;
	today.PrintDate();
	cout << endl;

	CDate incr_decr;
	cout << "\n���������� �������� ������������ ����������\n";
	incr_decr = today--;
	incr_decr.PrintDate();
	cout << endl;
	++today;
	cout << "�������� ����������� ���������� �� �����\n";
	incr_decr = --today;
	incr_decr.PrintDate();
	cout << endl;
	++today;

	cout << "\n���������� �������� ������������ ����������\n";
	incr_decr = today++;
	incr_decr.PrintDate();
	cout << endl;
	--today;
	cout << "�������� ����������� ���������� �� �����\n";
	incr_decr = ++today;
	incr_decr.PrintDate();
	cout << endl;
	--today;

	incr_decr = yearAgo = today + (-365);

	cout << "\n��������� ���\n";
	bDay.PrintDate();
	cout << endl;
	today.PrintDate();
	cout << endl;
	yearAgo.PrintDate();
	cout << endl;
	incr_decr.PrintDate();
	cout << endl << endl;

	////////////////////////////////////////////////////
	bDay.PrintDate();
	cout << " > ";
	bDay.PrintDate();
	cout << boolalpha << ' ' << (bDay > bDay) << endl;

	bDay.PrintDate();
	cout << " < ";
	bDay.PrintDate();
	cout << boolalpha << ' ' << (bDay < bDay) << endl;

	bDay.PrintDate();
	cout << " == ";
	bDay.PrintDate();
	cout << boolalpha << ' ' << (bDay == bDay) << endl;

	bDay.PrintDate();
	cout << " != ";
	bDay.PrintDate();
	cout << boolalpha << ' ' << (bDay != bDay) << endl << endl;
	////////////////////////////////////////////////////
	bDay.PrintDate();
	cout << " > ";
	today.PrintDate();
	cout << boolalpha << ' ' << (bDay > today) << endl;

	bDay.PrintDate();
	cout << " < ";
	today.PrintDate();
	cout << boolalpha << ' ' << (bDay < today) << endl;

	bDay.PrintDate();
	cout << " == ";
	today.PrintDate();
	cout << boolalpha << ' ' << (bDay == today) << endl;

	bDay.PrintDate();
	cout << " != ";
	today.PrintDate();
	cout << boolalpha << ' ' << (bDay != today) << endl << endl;
	////////////////////////////////////////////////////
	bDay.PrintDate();
	cout << " > ";
	yearAgo.PrintDate();
	cout << boolalpha << ' ' << (bDay > yearAgo) << endl;

	bDay.PrintDate();
	cout << " < ";
	yearAgo.PrintDate();
	cout << boolalpha << ' ' << (bDay < yearAgo) << endl;

	bDay.PrintDate();
	cout << " == ";
	yearAgo.PrintDate();
	cout << boolalpha << ' ' << (bDay == yearAgo) << endl;

	bDay.PrintDate();
	cout << " != ";
	yearAgo.PrintDate();
	cout << boolalpha << ' ' << (bDay != yearAgo) << endl << endl;
	////////////////////////////////////////////////////
	yearAgo.PrintDate();
	cout << " > ";
	incr_decr.PrintDate();
	cout << boolalpha << ' ' << (yearAgo > incr_decr) << endl;

	yearAgo.PrintDate();
	cout << " < ";
	incr_decr.PrintDate();
	cout << boolalpha << ' ' << (yearAgo < incr_decr) << endl;

	yearAgo.PrintDate();
	cout << " == ";
	incr_decr.PrintDate();
	cout << boolalpha << ' ' << (yearAgo == incr_decr) << endl;

	yearAgo.PrintDate();
	cout << " != ";
	incr_decr.PrintDate();
	cout << boolalpha << ' ' << (yearAgo != incr_decr) << endl << endl;

	system("pause");
}