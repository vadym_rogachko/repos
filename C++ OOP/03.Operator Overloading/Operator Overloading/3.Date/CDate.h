#pragma once

class CDate {

	int day;
	int month;
	int year;

	int getAllDays() const;
	void setDate(int days);

	enum months {
		JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC
	};

public:

	CDate();
	CDate(int day, int month, int year);

	int getDay() const;
	int getMonth() const;
	int getYear() const;

	void setDay(int day);
	void setMonth(int month);
	void setYear(int year);

	CDate operator +(int days) const;
	int operator -(const CDate &date) const;

	CDate& operator ++();
	CDate& operator --();
	const CDate operator ++(int);
	const CDate operator --(int);

	bool operator >(const CDate &date) const;
	bool operator <(const CDate &date) const;
	bool operator ==(const CDate &date) const;
	bool operator !=(const CDate &date) const;

	const char* DayOfWeek() const;
	void PrintDate() const;
	bool isLeapYear() const;
};