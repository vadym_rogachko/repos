#include<iostream>
#include<conio.h>
#include"CFractionFunctions.h"
#include"Calc.h"

using std::cout;
using std::cin;
using std::endl;

void GetFraction(CFraction &fract) {
	int numerator, denominator;
	cout << "������� ��������� � ����������� �����\n";
	cin >> numerator >> denominator;
	fract.InitFract(numerator, denominator);
}

char GetAction() {
	char ch;
	system("cls");
	cout << "�������� ��������:\n"
			"1.��������\n"
			"2.���������\n"
			"3.���������\n"
			"4.�������\n"
			"Esc - �����\n";
	do {
		ch = _getch();
	} while (ch != PLUS && ch != MINUS && ch != MULTI && ch != DIV && ch != ESC);
	return ch;
}

CFraction Calc(const CFraction &fract1, const CFraction &fract2, char &ch) {
	CFraction fract;
	switch (ch) {
	case PLUS:
		fract = fract1 + fract2;
		ch = '+';
		break;
	case MINUS:
		fract = fract1 - fract2;
		ch = '-';
		break;
	case MULTI:
		fract = fract1 * fract2;
		ch = '*';
		break;
	case DIV:
		fract = fract1 / fract2;
		ch = '/';
		break;
	}
	fract.ToInteger();
	return fract;
}

void ShowRes(const CFraction &fract1, const CFraction &fract2, const CFraction &fractRes, char sign) {
	cout << endl;
	fract1.Print();
	cout << ' ' << sign << ' ';
	fract2.Print();
	cout << " = ";
	if (fractRes.getNumerator() == 0) {
		cout << fractRes.getInteger();
	}
	else {
		fractRes.Print();
	}
	cout << endl << endl;
}