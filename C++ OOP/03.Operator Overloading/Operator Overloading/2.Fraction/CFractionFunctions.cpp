#include<iostream>
#include"CFractionFunctions.h"

using std::cout;

CFraction::CFraction() : numerator(0), denominator(1), integer(0) {}

CFraction::CFraction(int n, int d) : numerator(n), integer(0), denominator(d > 0 ? d : 1) {}

CFraction::CFraction(int i, int n, int d) : integer(i), numerator(i < 0 ? abs(n) : n), denominator(d > 0 ? d : 1) {}

int CFraction::getNumerator() const {
	return numerator;
}

int CFraction::getDenominator() const {
	return denominator;
}

int CFraction::getInteger() const {
	return integer;
}

void CFraction::setInteger(int i) {
	integer = i;
}

void CFraction::setNumerator(int n) {
	numerator = integer < 0 ? abs(n) : n;
}

void CFraction::setDenominator(int d) {
	denominator = d > 0 ? d : 1;
}

CFraction operator +(CFraction fract1, CFraction fract2) {
	CFraction temp;
	// ������� ������ � ������� �� �������� ��-�� ������������ ���������� ������ - � ����������� ������������ ����� NoInteger, ���������� ������(�������� � ������������ ����� ���� ���� ����� �����)
	fract1.NoInteger();
	fract2.NoInteger();
	if (fract1.getDenominator() == fract2.getDenominator()) {
		temp.setDenominator(fract1.getDenominator());
		temp.setNumerator(fract1.getNumerator() + fract2.getNumerator());
		temp.CutFraction();
		return temp;
	}
	short num = 2;
	while (fract1.getDenominator() * num % fract2.getDenominator() != 0) {
		++num;
	}
	temp.setDenominator(fract1.getDenominator() * num);
	temp.setNumerator(fract1.getNumerator() * num);
	num = temp.getDenominator() / fract2.getDenominator();
	temp.setNumerator(temp.getNumerator() + (fract2.getNumerator() * num));
	temp.CutFraction();
	return temp;
}

CFraction operator -(CFraction fract1, CFraction fract2) {
	CFraction temp;
	fract1.NoInteger();
	fract2.NoInteger();
	if (fract1.getDenominator() == fract2.getDenominator()) {
		temp.setDenominator(fract1.getDenominator());
		temp.setNumerator(fract1.getNumerator() - fract2.getNumerator());
		temp.CutFraction();
		return temp;
	}
	short num = 2;
	while (fract1.getDenominator() * num % fract2.getDenominator() != 0) {
		++num;
	}
	temp.setDenominator(fract1.getDenominator() * num);
	temp.setNumerator(fract1.getNumerator() * num);
	num = temp.getDenominator() / fract2.getDenominator();
	temp.setNumerator(temp.getNumerator() - (fract2.getNumerator() * num));
	temp.CutFraction();
	return temp;
}

CFraction operator *(CFraction fract1, CFraction fract2) {
	CFraction temp;
	fract1.NoInteger();
	fract2.NoInteger();
	temp.setNumerator(fract1.getNumerator() * fract2.getNumerator());
	temp.setDenominator(fract1.getDenominator() * fract2.getDenominator());
	temp.CutFraction();
	return temp;
}

CFraction operator /(CFraction fract1, CFraction fract2) {
	CFraction temp;
	fract1.NoInteger();
	fract2.NoInteger();
	temp.setNumerator(fract1.getNumerator() * fract2.getDenominator());
	temp.setDenominator(fract1.getDenominator() * fract2.getNumerator());
	temp.CutFraction();
	return temp;
}

bool operator ==(const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() == fract2.RealNum();
}

bool operator !=(const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() != fract2.RealNum();
}

bool operator >(const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() > fract2.RealNum();
}

bool operator <(const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() < fract2.RealNum();
}

bool operator >=(const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() >= fract2.RealNum();
}

bool operator <=(const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() <= fract2.RealNum();
}

void CFraction::ToInteger() {
	integer += numerator / denominator;
	numerator %= denominator;
	if (integer < 0) {
		numerator = abs(numerator);
	}
}

void CFraction::NoInteger() {
	numerator += integer * denominator;
	integer = 0;
}

double CFraction::RealNum() const {
	return integer + ((double)numerator / denominator);
}

void CFraction::Print() const {
	if (integer != 0) {
		cout << integer << ' ';
	}
	cout << numerator << '/' << denominator;
}

void CFraction::InitFract(int n, int d) {
	integer = 0;
	numerator = n;
	denominator = d > 0 ? d : 1;
}

void CFraction::InitFract(int i, int n, int d) {
	integer = i;
	numerator = integer < 0 ? abs(n) : n;
	denominator = d > 0 ? d : 1;
}

void CFraction::InitZero() {
	numerator = 0;
	denominator = 1;
	integer = 0;
}

void CFraction::CutFraction() {
	bool minus = numerator < 0 || integer < 0;
	if (minus) {
		numerator *= -1;
	}
	if (numerator % denominator == 0) {
		numerator = numerator / denominator;
		denominator = 1;
		if (minus) {
			if (integer == 0) {
				numerator *= -1;
			}
		}
		return;
	}
	if (denominator % numerator == 0) {
		denominator = denominator / numerator;
		numerator = 1;
		if (minus) {
			if (integer == 0) {
				numerator *= -1;
			}
		}
		return;
	}
	short num = 2;
	while (num < numerator && num < denominator) {
		if (numerator % num == 0 && denominator % num == 0) {
			numerator = numerator / num;
			denominator = denominator / num;
			num = 2;
			continue;
		}
		++num;
	}
	if (minus) {
		if (integer == 0) {
			numerator *= -1;
		}
	}
}