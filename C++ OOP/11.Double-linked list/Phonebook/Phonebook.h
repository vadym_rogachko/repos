#pragma once
#include"DList.h"
#include"Subscriber.h"

class Phonebook {

	List<Subscriber> data;

public:

	void Add();
	void Delete();
	void Edit();
	int Find();
	void Print();
	void Save(const char* path);
	void Load(const char* path);
};