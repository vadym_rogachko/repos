#pragma once

enum size {
	NAME_S = 16,
	SURNAME_S = 16,
	PHONE_S = 12,
	WIDTH = SURNAME_S
};

enum action {
	EXIT = '0',
	ADD, 
	DEL,
	EDIT,
	FIND,
	PRINT,
	ENTER = 13,
	ESC = 27
};

enum choice {
	YES = 'Y',
	NO = 'N',
	SURNAME = '1',
	PHONE = '2'
};