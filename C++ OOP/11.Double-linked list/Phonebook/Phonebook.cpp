#include<iostream>
#include<conio.h>
#include"Phonebook.h"
#include"enum.h"

using std::cout;
using std::cin;
using std::endl;

void Phonebook::Add() {
	int addCount;
	system("cls");
	cout << "How many subscribers you wish to add?\n";
	cin >> addCount;
	cin.get();
	if (addCount < 0) {
		system("cls");
		cout << "Error\n";
		system("pause >> void");
		return;
	}
	Subscriber temp;
	while (addCount > 0) {
		system("cls");
		cin >> temp;
		data.AddTail(temp);
		--addCount;
	}
}

void Phonebook::Delete() {
	system("cls");
	if (data.GetCount() == 0) {
		cout << "The phonebook is empty\n";
		system("pause >> void");
		return;
	}
	int index = Find();
	if (index != -1) {
		char choice;
		cout << "Delete?(Y/N)";
		do {
			choice = toupper(_getch());
		} while (choice != YES && choice != NO && choice != ESC && choice != ENTER);
		if (choice == YES || choice == ENTER) {
			data.Del(index);
			system("cls");
			cout << "Subscriber deleted\n";
			system("pause >> void");
		}
	}
	else {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
	}
}

void Phonebook::Edit() {
	system("cls");
	if (data.GetCount() == 0) {
		cout << "The phonebook is empty\n";
		system("pause >> void");
		return;
	}
	int index = Find();
	if (index != -1) {
		cout << "\nEdit student:\n";
		cin >> data[index];
		system("cls");
		cout << "Subscriber edited\n";
		system("pause >> void");
	}
	else {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
	}
}

int Phonebook::Find() {
	system("cls");
	if (data.GetCount() == 0) {
		cout << "The phonebook is empty\n";
		system("pause >> void");
		return -1;
	}
	Subscriber temp;
	char str[NAME_S];
	char choice;
	cout << "Find the subscriber\n"
			"1.By surname\n"
			"2.By phone\n";
	do {
		choice = _getch();
	} while (choice != SURNAME && choice != PHONE && choice != ESC);
	system("cls");
	switch (choice) {
	case SURNAME:
		cout << "Enter the surname: ";
		cin.getline(str, SURNAME_S);
		temp.setSurname(str);
		break;
	case PHONE:
		cout << "Enter the phone: ";
		cin.getline(str, PHONE_S);
		temp.setPhone(str);
		break;
	default:
		return -1;
	}
	int index = data.Find(temp);
	if (index == -1) {
		system("cls");
		cout << "Not found\n";
		return index;
	}
	system("cls");
	cout << data[index];
	return index;
}

void Phonebook::Print() {
	system("cls");
	data.Sort();
	int count = data.GetCount();
	if (count == 0) {
		cout << "The phonebook is empty\n";
		system("pause >> void");
		return;
	}
	for (int i = 0; i < count; ++i) {
		cout << data[i] << endl;
	}
	system("pause >> void");
}

void Phonebook::Save(const char *path) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, path, "wb");
	if (error != 0) {
		perror(path);
		cout << endl;
		if (file != nullptr) {
			fclose(file);
		}
	}
	else {
		int count = data.GetCount();
		int size = sizeof data[0];
		fwrite(&count, sizeof (int), 1, file);
		for (int i = 0; i < count; ++i) {
			fwrite(data.GetElem(i), size, 1, file);
		}
		fclose(file);
	}
}

void Phonebook::Load(const char* path) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, path, "rb");
	if (error != 0) {
		perror(path);
		cout << endl;
		if (file != nullptr) {
			fclose(file);
		}
	}
	else {
		Subscriber temp;
		int count;
		fread_s(&count, sizeof count, sizeof(int), 1, file);
		for (int i = 0; i < count; ++i) {
			fread_s(&temp, sizeof temp, sizeof Subscriber, 1, file);
			data.AddTail(temp);
		}
		fclose(file);
	}
}