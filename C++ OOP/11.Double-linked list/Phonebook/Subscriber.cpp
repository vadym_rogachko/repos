#include<iostream>
#include<iomanip>
#include"Subscriber.h"
#include"enum.h"

using std::cout;
using std::cin;
using std::endl;
using std::setw;
using std::left;

Subscriber::Subscriber() {
	memset(surname, '\0', SURNAME_S);
	memset(name, '\0', NAME_S);
	memset(phone, '\0', PHONE_S);
}

Subscriber::Subscriber(const char* surname, const char* name, const char* phone) {
	setSurname(surname);
	setName(name);
	setPhone(phone);
}

const char* Subscriber::getSurname() const {
	return surname;
}

const char* Subscriber::getName() const {
	return name;
}

const char* Subscriber::getPhone() const {
	return phone;
}

void Subscriber::setSurname(const char* surname) {
	strcpy_s(this->surname, surname);
	strlwr(this->surname);
	this->surname[0] = toupper(this->surname[0]);
}

void Subscriber::setName(const char* name) {
	strcpy_s(this->name, name);
	strlwr(this->name);
	this->name[0] = toupper(this->name[0]);
}

void Subscriber::setPhone(const char* phone) {
	strcpy_s(this->phone, phone);
}

bool operator == (const Subscriber &sub1, const Subscriber &sub2) {
	if (sub2.surname[0] != '\0') {
		return !strcmp(sub1.surname, sub2.surname);
	}
	else {
		return !strcmp(sub1.phone, sub2.phone);
	}
}

bool operator > (const Subscriber &sub1, const Subscriber &sub2) {
	return strcmp(sub1.getSurname(), sub2.getSurname()) > 0;
}

bool operator < (const Subscriber &sub1, const Subscriber &sub2) {
	return sub2 > sub1;
}

istream& operator >> (istream &is, Subscriber &sub) {
	cout << "Surname: ";
	cin.getline(sub.surname, SURNAME_S);
	strlwr(sub.surname);
	sub.surname[0] = toupper(sub.surname[0]);
	cout << "Name: ";
	cin.getline(sub.name, NAME_S);
	strlwr(sub.name);
	sub.name[0] = toupper(sub.name[0]);
	cout << "Phone: ";
	cin.getline(sub.phone, PHONE_S);
	return is;
}

ostream& operator << (ostream &os, const Subscriber &sub) {
	os << setw(WIDTH) << left << sub.surname;
	os << setw(WIDTH) << left << sub.name;
	os << setw(WIDTH) << left << sub.phone << endl;
	return os;
}