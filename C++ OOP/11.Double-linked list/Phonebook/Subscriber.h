#pragma once

using std::istream;
using std::ostream;

class Subscriber {

	char surname[16];
	char name[16];
	char phone[12];

public:

	Subscriber();
	Subscriber(const char* surname, const char* name, const char* phone);
	const char* getSurname() const;
	const char* getName() const;
	const char* getPhone() const;
	void setSurname(const char* surname);
	void setName(const char* name);
	void setPhone(const char* phone);

	friend bool operator == (const Subscriber &sub1, const Subscriber &sub2);
	friend bool operator > (const Subscriber &sub1, const Subscriber &sub2);
	friend bool operator < (const Subscriber &sub1, const Subscriber &sub2);

	friend istream& operator >> (istream &is, Subscriber &sub);
	friend ostream& operator << (ostream &os, const Subscriber &sub);
};