#include<iostream>
#include<conio.h>
#include"DList.h"
#include"Subscriber.h"
#include"Phonebook.h"
#include"enum.h"

using std::cout;

void main() {

	Phonebook phonebook;
	phonebook.Load("Phonebook.bin");
	int action;
	do {
		system("cls");
		cout << "Phonebook\n"
				"1.Add\n"
				"2.Delete\n"
				"3.Edit\n"
				"4.Find\n"
				"5.Print\n";
		do {
			action = _getch();
			if (action == ESC) {
				int choice;
				system("cls");
				cout << "Are you sure you want to exit? (Y/N)\n";
				do {
					choice = toupper(_getch());
				} while (choice != YES && choice != NO);
				if (choice == YES) action = EXIT;
			}
		} while ((action < EXIT || action > PRINT) && action != ESC);
		switch (action) {
		case ADD:
			phonebook.Add();
			break;
		case DEL:
			phonebook.Delete();
			break;
		case EDIT:
			phonebook.Edit();
			break;
		case FIND:
			phonebook.Find();
			system("pause >> void");
			break;
		case PRINT:
			phonebook.Print();
			break;
		case EXIT:
			phonebook.Save("Phonebook.bin");
			break;
		}
	} while (action != EXIT);

	system("cls");
	system("pause");
}