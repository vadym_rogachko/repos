#include <iostream>
#include "List_t.h"

using namespace std;

void main()
{
	setlocale(LC_ALL, "rus");

	List<char> lst;
	char s[] = "Hello, World !!!";
	int len = strlen(s);
	for (int i = len - 1; i >= 0; i--)
		lst.AddHead(s[i]);
	lst.PrintHead();

	cout << "\n����� ��������� �������� �� �����\n";
	cout << "���� \'r\'\n";
	cout << lst.Find('r') << endl;
	cout << "���� \'s\'\n";
	cout << lst.Find('s') << endl << endl;

	cout << "������ �������� ��������\n";
	lst.PrintHead();
	lst.Replace('h', 0);
	lst.Replace('*', 15);
	lst.Replace('w', 7);
	lst.PrintHead();

	cout << "\n�������� ���������\n";
	lst.PrintHead();
	lst.DelElementByKey('!');
	lst.DelElementByKey('*');
	lst.PrintHead();

	cout << "\n�������� ��������������\n";
	for (int i = 0; i < lst.GetCount(); ++i) {
		cout << lst[i];
	}
	cout << endl << endl;

	lst[0] = 'c';
	lst[1] = 'f';
	lst[2] = 'e';
	lst[3] = 'i';
	lst[4] = 'h';
	lst[5] = 'a';
	lst[6] = 'm';
	lst[7] = 'l';
	lst[8] = 'j';
	lst[9] = 'b';
	lst[10] = 'd';
	lst[11] = 'g';
	lst[12] = 'k';
	lst.PrintHead();

	cout << "\n����������\n";
	lst.Sort();
	lst.PrintHead();
	cout << endl;
}
