#include<iostream>
#include"CStudent.h"

CStudent::CStudent() : name(nullptr), surname(nullptr), age(0), average(0) {
	memset(phone, '\0', PHONE_S);
}

CStudent::CStudent(const char *name, const char *surname, int age, const char *phone, double average) {
	int size;
	size = strlen(name) + 1;
	this->name = new char[size];
	strcpy_s(this->name, size, name);
	size = strlen(surname) + 1;
	this->surname = new char[size];
	strcpy_s(this->surname, size, surname);
	strncpy_s(this->phone, PHONE_S, phone, PHONE_S - 1);
	this->age = age;
	this->average = average;
}

CStudent::CStudent(const CStudent &student) {
	int size;
	size = strlen(student.name) + 1;
	name = new char[size];
	for (int i = 0; i < size; ++i) {
		name[i] = student.name[i];
	}
	size = strlen(student.surname) + 1;
	surname = new char[size];
	for (int i = 0; i < size; ++i) {
		surname[i] = student.surname[i];
	}
	for (int i = 0; i < PHONE_S; ++i) {
		phone[i] = student.phone[i];
	}
	age = student.age;
	average = student.average;
}

CStudent::~CStudent() {
	delete[] name;
	delete[] surname;
}

const char* CStudent::getName() const {
	return name;
}

const char* CStudent::getSurname() const {
	return surname;
}

int CStudent::getAge() const {
	return age;
}

double CStudent::getAverage() const {
	return average;
}

const char* CStudent::getPhone() const {
	return phone;
}

void CStudent::setName(const char *name) {
	delete[] this->name;
	int size = strlen(name) + 1;
	this->name = new char[size];
	strcpy_s(this->name, size, name);
}

void CStudent::setSurname(const char *surname) {
	delete[] this->surname;
	int size = strlen(surname) + 1;
	this->surname = new char[size];
	strcpy_s(this->surname, size, surname);
}

void CStudent::setPhone(const char *phone) {
	strncpy_s(this->phone, PHONE_S, phone, PHONE_S - 1);
}

void CStudent::setAge(int age) {
	this->age = age;
}

void CStudent::setAverage(double average) {
	this->average = average;
}