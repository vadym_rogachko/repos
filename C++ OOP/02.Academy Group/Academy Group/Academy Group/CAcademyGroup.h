#pragma once
#include"CStudent.h"

class CAcademyGroup
{
	CStudent **pSt;
	int count;

	enum criterion {
		SURNAME = '1',
		NAME,
		PHONE,
		AGE,
		AVERAGE
	};

public:
	CAcademyGroup();
	CAcademyGroup(const CAcademyGroup &group);
	~CAcademyGroup();

	int getCount() const; // ����� ����� ��� ����������
	void AddStudents();
	void DeleteStudent();
	void EditStudent() const;
	void Print() const;
	void FindStudent() const;
	void Sort(short left, short right) const;
	void Save() const;
	void Load();
};