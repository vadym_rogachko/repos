#include<iostream>
#include<conio.h>
#include<iomanip>
#include<windows.h>
#include"CAcademyGroup.h"

using std::cout;
using std::cin;
using std::endl;
using std::setw;
using std::left;
using std::swap;

CAcademyGroup::CAcademyGroup() : pSt(nullptr), count(0) {}

CAcademyGroup::~CAcademyGroup() {
	if (pSt) {
		for (int i = 0; i < count; ++i) {
			delete pSt[i];
		}
		delete[] pSt;
	}
}

CAcademyGroup::CAcademyGroup(const CAcademyGroup &group) {
	pSt = new CStudent*[group.count];
	for (int i = 0; i < group.count; ++i) {
		pSt[i] = new CStudent(*group.pSt[i]);
	}
	count = group.count;
	/*int **arr = new int*[10];
	arr[0] = new int[10 * 10];
	for (int i = 1; i < 10; ++i) {
	arr[i] = arr[i - 1] + 10;
	}
	delete[] arr[0];
	delete[] arr;

	pSt = new CStudent*[group.count];
	pSt[0] = new CStudent[10];
	for (int i = 1; i < group.count; ++i) {
	pSt[i] = pSt[i - 1] + sizeof(CStudent);
	}
	delete[] pSt[0];
	delete[] pSt;*/
}

// метод нужен для сортировки
int CAcademyGroup::getCount() const {
	return count;
}

void CAcademyGroup::AddStudents() {
	int addCount;
	system("cls");
	cout << "How many students you wish to add?\n";
	cin >> addCount;
	if (addCount < 0) {
		system("cls");
		cout << "Error\n";
		system("pause >> void");
		return;
	}
	count += addCount;
	CStudent **temp = new CStudent*[count];
	for (int i = 0; i < count - addCount; ++i) {
		temp[i] = new CStudent(*pSt[i]); // срабатывает коструктор копирования CStudent
		delete pSt[i]; // срабатывает деструктор CStudent
	}
	delete[] pSt;
	pSt = temp;
	temp = nullptr;
	char name[NAME_S]{};
	char surname[SURNAME_S]{};
	int age;
	char phone[PHONE_S]{};
	double average;
	for (int i = count - addCount; i < count; ++i) {
		system("cls");
		cin.get();
		char *temp;
		cout << "Enter the surname\n";
		fgets(surname, NAME_S, stdin);
		if (temp = strchr(surname, '\n')) {
			temp[0] = '\0';
		}
		else {
			scanf_s("%*[^\n]");
			scanf_s("%*c");
		}
		_strlwr_s(surname, SURNAME_S);
		surname[0] = toupper(surname[0]);
		cout << "Enter the name\n";
		fgets(name, NAME_S, stdin);
		if (temp = strchr(name, '\n')) {
			temp[0] = '\0';
		}
		else {
			scanf_s("%*[^\n]");
			scanf_s("%*c");
		}
		_strlwr_s(name, NAME_S);
		name[0] = toupper(name[0]);
		cout << "Enter the phone number\n";
		fgets(phone, NAME_S, stdin);
		if (temp = strchr(phone, '\n')) {
			temp[0] = '\0';
		}
		else {
			scanf_s("%*[^\n]");
			scanf_s("%*c");
		}
		cout << "Enter the age\n";
		cin >> age;
		cin.get();
		cout << "Enter the average point\n";
		cin >> average;
		pSt[i] = new CStudent(name, surname, age, phone, average); // срабатывает конструктор с параметрами CStudent
		cout << "\nStudent added\n";
		system("pause >> void");
	}
}

void CAcademyGroup::DeleteStudent() {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	char choice;
	char *temp;
	char surname[SURNAME_S]{};
	cout << "Enter the surname\n";
	fgets(surname, NAME_S, stdin);
	if (temp = strchr(surname, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(surname, SURNAME_S);
	surname[0] = toupper(surname[0]);
	int num = 0;
	while (num < count) {
		if (!strcmp(pSt[num]->getSurname(), surname)) break;
		++num;
	}
	if (num == count) {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
		return;
	}
	system("cls");
	cout << pSt[num]->getSurname() << ' ' << pSt[num]->getName() << endl;
	cout << "Delete?(Y/N)";
	do {
		choice = toupper(_getch());
	} while (choice != YES && choice != NO && choice != ESC && choice != ENTER);
	while (_kbhit()) {
		_getch();
	}
	if (choice == YES || choice == ENTER) {
		CStudent **temp = new CStudent*[count - 1];
		for (int i = 0; i < num; ++i) {
			temp[i] = new CStudent(*pSt[i]); // срабатывает конструктор копирования CStudent
		}
		for (int i = num; i < count - 1; ++i) {
			temp[i] = new CStudent(*pSt[i + 1]); // срабатывает конструктор копирования CStudent
		}
		for (int i = 0; i < count; ++i) {
			delete pSt[i]; // Срабатывает деструктор CStudent
		}
		delete[] pSt;
		pSt = temp;
		temp = nullptr;
		--count;
		system("cls");
		cout << "Student deleted\n";
		system("pause >> void");
	}
}

void CAcademyGroup::EditStudent() const {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	char choice;
	char name[NAME_S]{};
	char surname[SURNAME_S]{};
	int age;
	char phone[PHONE_S]{};
	double average;
	char *temp;
	cout << "Enter the surname\n";
	fgets(surname, NAME_S, stdin);
	if (temp = strchr(surname, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(surname, SURNAME_S);
	surname[0] = toupper(surname[0]);
	int num = 0;
	while (num < count) {
		if (!strcmp(pSt[num]->getSurname(), surname)) break;
		++num;
	}
	if (num == count) {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
		return;
	}
	while (true) {
		system("cls");
		cout << pSt[num]->getSurname() << ' ' << pSt[num]->getName() << endl;
		cout << "1.Edit surname" << endl;
		cout << "2.Edit name" << endl;
		cout << "3.Edit phone" << endl;
		cout << "4.Edit age" << endl;
		cout << "5.Edit avearge point" << endl;
		do {
			choice = _getch();
		} while (choice != SURNAME && choice != NAME && choice != PHONE && choice != AGE && choice != AVERAGE && choice != ESC);
		if (choice == ESC) return;
		while (_kbhit()) {
			_getch();
		}
		system("cls");
		switch (choice) {
		case SURNAME:
			cout << "Surname: " << pSt[num]->getSurname() << endl;
			cout << "\nEnter the surname: ";
			fgets(surname, NAME_S, stdin);
			if (temp = strchr(surname, '\n')) {
				temp[0] = '\0';
			}
			else {
				scanf_s("%*[^\n]");
				scanf_s("%*c");
			}
			_strlwr_s(surname, SURNAME_S);
			surname[0] = toupper(surname[0]);
			pSt[num]->setSurname(surname);
			break;
		case NAME:
			cout << "Name: " << pSt[num]->getName() << endl;
			cout << "\nEnter the name: ";
			fgets(name, NAME_S, stdin);
			if (temp = strchr(name, '\n')) {
				temp[0] = '\0';
			}
			else {
				scanf_s("%*[^\n]");
				scanf_s("%*c");
			}
			_strlwr_s(name, NAME_S);
			name[0] = toupper(name[0]);
			pSt[num]->setName(name);
			break;
		case PHONE:
			cout << "Phone: " << pSt[num]->getPhone() << endl;
			cout << "\nEnter the phone number: ";
			fgets(phone, NAME_S, stdin);
			if (temp = strchr(phone, '\n')) {
				temp[0] = '\0';
			}
			else {
				scanf_s("%*[^\n]");
				scanf_s("%*c");
			}
			pSt[num]->setPhone(phone);
			break;
		case AGE:
			cout << "Age: " << pSt[num]->getAge() << endl;
			cout << "\nEnter the age: ";
			cin >> age;
			pSt[num]->setAge(age);
			break;
		case AVERAGE:
			cout << "Avearge point: " << pSt[num]->getAverage() << endl;
			cout << "\nEnter the average point: ";
			cin >> average;
			pSt[num]->setAverage(average);
			break;
		}
	}
}

void CAcademyGroup::Print() const {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	cout << setw(WIDTH) << left << "Surname";
	cout << setw(WIDTH) << left << "Name";
	cout << setw(WIDTH) << left << "Phone";
	cout << setw(WIDTH - 7) << left << "Age";
	cout << setw(WIDTH - 5) << left << "Av.Point" << endl << endl;
	for (int i = 0; i < count; ++i) {
		cout << setw(WIDTH) << left << pSt[i]->getSurname();
		cout << setw(WIDTH) << left  << pSt[i]->getName();
		cout << setw(WIDTH) << left << pSt[i]->getPhone();
		cout << setw(WIDTH - 7) << left << pSt[i]->getAge();
		cout << setw(WIDTH - 5) << left << pSt[i]->getAverage() << endl;
	}
	system("pause >> void");
}

void CAcademyGroup::FindStudent() const {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	char choice;
	char isMask;
	cout << "Find the student\n"
			"1.By name\n"
			"2.By surname\n"
			"3.By phone\n";
	do {
		choice = _getch();
	} while (choice != NAME && choice != SURNAME && choice != PHONE && choice != ESC);
	if (choice == ESC) return;
	while (_kbhit()) {
		_getch();
	}
	cout << "\nFind by mask?(Y/N)\n";
	do {
		isMask = toupper(_getch());
	} while (isMask != YES && isMask != NO && isMask != ESC && isMask != ENTER);
	if (isMask == ESC) return;
	isMask = isMask == YES || isMask == ENTER;
	while (_kbhit()) {
		_getch();
	}
	system("cls");
	char str[NAME_S]{};
	char *temp;
	switch (choice) {
	case NAME:
		cout << "Enter name\n";
		break;
	case SURNAME:
		cout << "Enter surname\n";
		break;
	case PHONE:
		cout << "Enter phone\n";
		break;
	}
	fgets(str, NAME_S, stdin);
	if (temp = strchr(str, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(str, NAME_S);
	str[0] = toupper(str[0]);
	system("cls");
	cout << setw(WIDTH) << left << "Surname";
	cout << setw(WIDTH) << left << "Name";
	cout << setw(WIDTH) << left << "Phone";
	cout << setw(WIDTH - 7) << left << "Age";
	cout << setw(WIDTH - 5) << left << "Average" << endl << endl;
	switch (choice) {
	case NAME:
		for (int i = 0; i < count; ++i) {
			if (isMask ? strstr(pSt[i]->getName(), str) : !strcmp(str, pSt[i]->getName())) {
				cout << setw(WIDTH) << left << pSt[i]->getName();
				cout << setw(WIDTH) << left << pSt[i]->getSurname();
				cout << setw(WIDTH) << left << pSt[i]->getPhone();
				cout << setw(WIDTH - 7) << left << pSt[i]->getAge();
				cout << setw(WIDTH - 5) << left << pSt[i]->getAverage() << endl;;
			}
		}
		break;
	case SURNAME:
		for (int i = 0; i < count; ++i) {
			if (isMask ? strstr(pSt[i]->getSurname(), str) : !strcmp(str, pSt[i]->getSurname())) {
				cout << setw(WIDTH) << left << pSt[i]->getName();
				cout << setw(WIDTH) << left << pSt[i]->getSurname();
				cout << setw(WIDTH) << left << pSt[i]->getPhone();
				cout << setw(WIDTH - 7) << left << pSt[i]->getAge();
				cout << setw(WIDTH - 5) << left << pSt[i]->getAverage() << endl;;
			}
		}
		break;
	case PHONE:
		for (int i = 0; i < count; ++i) {
			if (isMask ? strstr(pSt[i]->getPhone(), str) : !strcmp(str, pSt[i]->getPhone())) {
				cout << setw(WIDTH) << left << pSt[i]->getName();
				cout << setw(WIDTH) << left << pSt[i]->getSurname();
				cout << setw(WIDTH) << left << pSt[i]->getPhone();
				cout << setw(WIDTH - 7) << left << pSt[i]->getAge();
				cout << setw(WIDTH - 5) << left << pSt[i]->getAverage() << endl;;
			}
		}
		break;
	}
	system("pause >> void");
}

void CAcademyGroup::Sort(short left, short right) const {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	short l = left, r = right;
	char mid[SURNAME_S];
	strcpy_s(mid, SURNAME_S, pSt[(l + r) / 2]->getSurname());
	while (l <= r) {
		while (strcmp(pSt[l]->getSurname(), mid) < 0) ++l;
		while (strcmp(pSt[r]->getSurname(), mid) > 0) --r;
		if (l <= r) {
			swap(pSt[l++], pSt[r--]);
		}
	}
	if (l < right) Sort(l, right);
	if (r > left) Sort(left, r);
}

void CAcademyGroup::Save() const {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "AcademyGroup.bin", "wb");
	if (error != 0) {
		perror("AcademyGroup.bin");
		cout << endl;
		if (file != nullptr) {
			fclose(file);
		}
		exit(1);
	}
	fwrite(&count, sizeof(int), 1, file);
	int size;
	int tempInt;
	double tempDouble;
	for (int i = 0; i < count; ++i) {
		size = strlen(pSt[i]->getName()) + 1;
		fwrite(&size, sizeof(int), 1, file);
		fwrite(pSt[i]->getName(), sizeof(char), size, file);
		size = strlen(pSt[i]->getSurname()) + 1;
		fwrite(&size, sizeof(int), 1, file);
		fwrite(pSt[i]->getSurname(), sizeof(char), size, file);
		tempInt = pSt[i]->getAge();
		fwrite(&tempInt, sizeof(int), 1, file);
		fwrite(pSt[i]->getPhone(), sizeof(char), PHONE_S, file);
		tempDouble = pSt[i]->getAverage();
		fwrite(&tempDouble, sizeof(double), 1, file);
	}
	fclose(file);
}

void CAcademyGroup::Load() {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "AcademyGroup.bin", "rb");
	if (error != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	fread_s(&count, sizeof(count), sizeof(int), 1, file);
	if (count == 0) {
		fclose(file);
		return;
	}
	char name[NAME_S]{};
	char surname[SURNAME_S]{};
	int age;
	char phone[PHONE_S]{};
	double average;
	int size;
	pSt = new CStudent*[count];
	for (int i = 0; i < count; ++i) {
		fread_s(&size, sizeof size, sizeof(int), 1, file);
		fread_s(name, sizeof name, sizeof(char), size, file);
		fread_s(&size, sizeof size, sizeof(int), 1, file);
		fread_s(surname, sizeof surname, sizeof(char), size, file);
		fread_s(&age, sizeof age, sizeof(int), 1, file);
		fread_s(phone, sizeof phone, sizeof(char), PHONE_S, file);
		fread_s(&average, sizeof average, sizeof(double), 1, file);
		pSt[i] = new CStudent(name, surname, age, phone, average); // срабатывает конструктор с параметрами CStudent
	}
	fclose(file);
}