#pragma once
#include"enum.h"

class CStudent
{
	char *name;
	char *surname;
	int age;
	char phone[PHONE_S];
	double average;

public:
	CStudent();
	CStudent(const char *name, const char *surname, int age, const char * phone, double average);
	CStudent(const CStudent &student);
	~CStudent();

	const char* getName() const;
	const char* getSurname() const;
	int getAge() const;
	double getAverage() const;
	const char* getPhone() const;
	void setName(const char *name);
	void setSurname(const char *surname);
	void setPhone(const char *phone);
	void setAge(int age);
	void setAverage(double average);
};