#include<iostream>
#include<conio.h>
#include<windows.h>
#include"CAcademyGroup.h"
#include"CStudent.h"


/*

�������� ���������, ����������� ����������� ������ ��������� ������������� ������. ��������� ��������� ������ ������������� ������������ ��������� �����������:
a. ���������� ��������� � ������;
b. �������� �������� �� ������;
c. ����������� ������ ��������;
d. ������ ������������� ������;
e. ���������� ������ ���������;
f. ����� �������� �� ��������� ��������;
g. ���������� ������ ��������� � ���� (��� ���������� ������ ���������);
h. �������� ������ ��������� �� ����� (��� ������ ���������);
i. ����� �� ���������.
��� ���������� ��������� ������� ������������ ��������� ������������ ������ ��� �������� ������ ��������� (���������� ������������ ������ ���������� �� ������� ������ Student).

*/

using std::cout;
using std::endl;

void main() {

	CAcademyGroup group; // ����������� ����������� CAcademyGropup
	group.Load();
	bool isExit = false;
	while (!isExit) {
		char choice;
		system("cls");
		cout << "Academy group\n"
				"1.Add\n"
				"2.Delete\n"
				"3.Edit\n"
				"4.Print\n"
				"5.Find\n"
				"6.Sort\n";
		do {
			choice = _getch();
		} while ((choice < ADD || choice > SORT) && choice != ESC);
		switch (choice) {
		case ADD:
			group.AddStudents();
			break;
		case DEL:
			group.DeleteStudent();
			break;
		case EDIT:
			group.EditStudent();
			break;
		case PRINT:
			group.Print();
			break;
		case FIND:
			group.FindStudent();
			break;
		case SORT:
			group.Sort(0, group.getCount() - 1);
			if (group.getCount() != 0) {
				system("cls");
				cout << "Sorted\n";
				system("pause >> void");
			}
			break;
		case ESC:
			char choice;
			system("cls");
			cout << "Are you sure you want to exit?(Y/N)\n";
			do {
				choice = toupper(_getch());
			} while (choice != YES && choice != NO);
			isExit = choice == YES;
			system("cls");
			break;
		}
	}
	CAcademyGroup copyGroup = group; // ����������� ����������� ����������� CAcademyGroup
	cout << "Test constructor-copy\n\n";
	system("pause");
	copyGroup.Print(); // �������� ������ ���������������������� ������� ����� ����������� �����������
	group.Save();
} // ����������� ���������� CAcademyGroup ��� group � copyGroup