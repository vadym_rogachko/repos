#pragma once

enum action {
	ADD = '1',
	DEL,
	EDIT,
	PRINT,
	FIND,
	SORT
};

enum size {
	PHONE_S = 12,
	NAME_S = 16,
	SURNAME_S = NAME_S,
	WIDTH = NAME_S
};

enum keys {
	ESC = 27,
	ENTER = 13
};

enum { 
	YES = 'Y',
	NO = 'N'
};

enum {
	NO_CHOICE = -1,
	EXIT
};
