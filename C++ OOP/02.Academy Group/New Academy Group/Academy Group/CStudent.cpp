#include<iostream>
#include"CStudent.h"

CStudent::CStudent() : name(nullptr), surname(nullptr), age(0), average(0) {
	memset(phone, '\0', PHONE_S);
}

CStudent::CStudent(const char *name, const char *surname, int age, const char *phone, double average) {
	int size;
	size = strlen(name) + 1;
	this->name = new char[size];
	strcpy_s(this->name, size, name);
	size = strlen(surname) + 1;
	this->surname = new char[size];
	strcpy_s(this->surname, size, surname);
	strncpy_s(this->phone, PHONE_S, phone, PHONE_S - 1);
	if (age <= 0 || age > 99) {
		this->age = 0;
	}
	else {
		this->age = age;
	}
	if (average <= 1. || average > 12.) {
		this->average = 0;
	}
	else {
		this->average = average;
	}
}

CStudent::CStudent(const CStudent &student) : age(student.age), average(student.average) {
	int size;
	size = strlen(student.name) + 1;
	name = new char[size];
	memcpy(name, student.name, size);
	size = strlen(student.surname) + 1;
	surname = new char[size];
	memcpy(surname, student.surname, size);
	memcpy(phone, student.phone, PHONE_S);
}

CStudent::~CStudent() {
	delete[] name;
	delete[] surname;
}

const char* CStudent::getName() const {
	return name;
}

const char* CStudent::getSurname() const {
	return surname;
}

int CStudent::getAge() const {
	return age;
}

double CStudent::getAverage() const {
	return average;
}

const char* CStudent::getPhone() const {
	return phone;
}

void CStudent::setName(const char *name) {
	delete[] this->name;
	int size = strlen(name) + 1;
	this->name = new char[size];
	strcpy_s(this->name, size, name);
}

void CStudent::setSurname(const char *surname) {
	delete[] this->surname;
	int size = strlen(surname) + 1;
	this->surname = new char[size];
	strcpy_s(this->surname, size, surname);
}

void CStudent::setPhone(const char *phone) {
	strncpy_s(this->phone, PHONE_S, phone, PHONE_S - 1);
}

void CStudent::setAge(int age) {
	if (age <= 0 || age > 99) {
		this->age = 0;
		return;
	}
	this->age = age;
}

void CStudent::setAverage(double average) {
	if (average <= 1. || average > 12.) {
		this->average = 0;
		return;
	}
	this->average = average;
}