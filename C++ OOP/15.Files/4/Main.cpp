#include<iostream>
#include<fstream>

using std::fstream;
using std::ifstream;
using std::ofstream;
using std::runtime_error;
using std::exception;
using std::ios;
using std::cerr;
using std::cout;

/*4. �������� ���������, ����������� ���������� � ������������ ���������� �����. ��� ���������� ������������ �������� XOR �^� */

void main() {

	try {
		ifstream fin("ReadMe.txt", ios::binary | ios::in | ios::ate);
		if (!fin.is_open()) {
			throw runtime_error("File not found");
		}
		size_t size = fin.tellg();
		fin.seekg(0);
		char *buf = new char[size];
		fin.read(buf, size);
		unsigned char ch = 33;
		for (int i = 0; i < size; ++i) {
			buf[i] ^= ch++;
		}
		fin.close();
		ofstream fout("ReadMe.txt", ios::binary | ios::out);
		fout.write(buf, size);
		fout.close();
		delete[] buf;
		cout << "OK\n";
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what();
	}
	system("pause >> void");
}