#include<iostream>
#include<fstream>
#include"Functions.h"

using std::ifstream;
using std::ios;
using std::exception;
using std::runtime_error;
using std::cerr;
using std::cout;
using std::cin;

/*2. �������� ���������, ������� ��������� ���� ������ ������� �� ������� ���������� �������, � ����� ��������������� ���� �� ���� ������ */

void main() {

	setlocale(LC_ALL, "rus");

	try {
		ifstream fin("pieces.jpg", ios::binary | ios::ate);
		size_t sizePiece, sizeFile;
		if (!fin.is_open()) {
			throw runtime_error("File not found");
		}
		sizeFile = fin.tellg();
		fin.seekg(0);
		cout << "������ ����� �����: " << sizeFile << " ����\n";
		cout << "������ ����� �����: ";
		cin >> sizePiece;
		SplitFile(fin, sizePiece);
		AsmFile("piecesCopy.jpg");
		fin.close();
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what();
	}
	system("pause >> void");
}