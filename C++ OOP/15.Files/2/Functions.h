#pragma once

using std::ifstream;

void SplitFile(ifstream &fin, size_t sizePiece);
void AsmFile(const char *path);