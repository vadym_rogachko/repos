#include<iostream>
#include<fstream>
#include"Functions.h"

using std::cout;
using std::endl;
using std::ios;
using std::ifstream;
using std::ofstream;
using std::runtime_error;
using std::length_error;

void SplitFile(ifstream &fin, size_t sizePiece) {
	ofstream fout("pieces\\data.dat", ios::binary); // ��������� �����, ����� �� �������� ����� � ��������
	if (!fout.is_open()) {
		throw runtime_error("File not found");
	}
	size_t countFiles, sizeFile;
	size_t sizeLast = 0;
	fin.seekg(0, ios::end);
	sizeFile = fin.tellg();
	if (sizePiece > sizeFile) {
		throw length_error("The size of part of the file bigger than the file\n");
	}
	fin.seekg(0);
	countFiles = sizeFile / sizePiece;
	if (countFiles * sizePiece != sizeFile) {
		sizeLast = sizeFile - (countFiles * sizePiece);
		++countFiles;
	}
	else {
		sizeLast = sizePiece;
	}
	fout.write((char*)&countFiles, sizeof countFiles);
	fout.close();
	char path[_MAX_PATH];
	char num[8];
	size_t sizeBuf = sizePiece >= sizeLast ? sizePiece : sizeLast;
	char *buf = new char[sizeBuf];
	for (int i = 0; i < countFiles - 1; ++i) {
		strcpy_s(path, _MAX_PATH, "pieces\\piece");
		_itoa_s(i + 1, num, sizeof num, 10);
		strcat_s(path, _MAX_PATH, num);
		strcat_s(path, _MAX_PATH, ".bin");
		fout.open(path, ios::binary);
		if (!fout.is_open()) {
			throw runtime_error("File not found");
		}
		fin.read(buf, sizePiece);
		fout.write(buf, sizePiece);
		fout.close();
	}
	strcpy_s(path, _MAX_PATH, "pieces\\piece");
	_itoa_s(countFiles, num, sizeof num, 10);
	strcat_s(path, _MAX_PATH, num);
	strcat_s(path, _MAX_PATH, ".bin");
	fout.open(path, ios::binary);
	if (!fout.is_open()) {
		throw runtime_error("File not found");
	}
	fin.read(buf, sizeLast);
	fout.write(buf, sizeLast);
	fout.close();
	cout << "\n���� ������� ������ �� �����\n";
	delete[] buf;
}

void AsmFile(const char *path) {
	ifstream fin("pieces\\data.dat", ios::binary);
	if (!fin.is_open()) {
		throw runtime_error("File not found");
	}
	ofstream fout(path, ios::binary);
	size_t count;
	fin.read((char*)&count, sizeof count);
	fin.close();
	char temp;
	char num[8];
	char pathTemp[_MAX_PATH];
	strcpy_s(pathTemp, _MAX_PATH, path);
	for (int i = 0; i < count; ++i) {
		strcpy_s(pathTemp, _MAX_PATH, "pieces\\piece");
		_itoa_s(i + 1, num, sizeof num, 10);
		strcat_s(pathTemp, _MAX_PATH, num);
		strcat_s(pathTemp, _MAX_PATH, ".bin");
		fin.open(pathTemp, ios::binary);
		if (!fin.is_open()) {
			throw runtime_error("File not found");
		}
		while (fin.read(&temp, sizeof temp)) {
			fout.write(&temp, sizeof temp);
		}
		fin.close();
	}
	cout << "\n����� ����� �� ������ ������� �������\n\n";
	fout.close();
}