#include<iostream>
#include<fstream>

using std::ifstream;
using std::ios;
using std::exception;
using std::runtime_error;
using std::cerr;
using std::cout;
using std::endl;

/*1. �������� ���������, ������� ������������ ���������� ����� � ���������� ���� � ��������� ����� */

void main() {

	setlocale(LC_ALL, "rus");

	try {
		ifstream fin("TextFile.txt");
		if (!fin.is_open()) {
			throw runtime_error("File not found");
		}
		int words = 0;
		int strings = 0;
		char buf[1024];
		char ch;

		while (fin.get(ch)) {
			cout << ch;
		}
		fin.clear();
		fin.seekg(0);

		while (fin.getline(buf, sizeof buf)) {
			++strings;
		}
		fin.clear();
		fin.seekg(0);

		while (fin >> buf) {
			++words;
		}
		fin.close();

		cout << "\n���������� ����� � ����� " << strings << endl;
		cout << "���������� ����  � ����� " << words << endl << endl;
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what();
	}
	system("pause >> void");
}