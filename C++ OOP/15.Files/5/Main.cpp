#include<iostream>
#include<fstream>
#include<string>
#include"List.h"
#include"Word.h"

using std::ifstream;
using std::ofstream;
using std::runtime_error;
using std::exception;
using std::cout;
using std::cerr;

/*5. �������� ���������, ������� ��������� ����� �� ����� � ������� � ������ ���� ����� �� ����� ������, ���������� ����� � ������� �������� ������� �� ���������.
     ��� ���� ��� ������� ����� ���������� ���������� ����� ��� ��������� � �������� ����� */

void main() {

	try {
		List<Word> list;
		ifstream fin("ReadMe.txt");
		if (!fin.is_open()) {
			throw runtime_error("File not found\n");
		}
		ofstream fout("Words.txt");
		string str;
		Word word;
		int pos;
		while (fin >> str) {
			if (str.length() > 1 && (str.back() == '.' || str.back() == ',' || str.back() == ':' || str.back() == ';' || str.back() == '!' || str.back() == '?' || str.back() == ')')) {
				str.pop_back();
			}
			if (str.length() > 1 && str.front() == '(') {
				str.erase(str.begin());
			}
			word.setWord(str);
			pos = list.Find(word);
			if (pos == -1) {
				list.AddTail(word);
			}
			else {
				++list[pos];
			}
		}
		list.Sort();
		for (int i = list.GetCount() - 1; i >= 0; --i) {
			fout << list[i].getWord() << ' ' << list[i].getCount() << '\n';
		}
		cout << "OK\n";
		fin.close();
		fout.close();
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what();
	}
	system("pause >> void");
}