#pragma once

using std::swap;

template <class T>
struct Elem {
	T data;
	Elem<T> *next, *prev;
};

template <class T>
class List {

	Elem<T> *Head, *Tail;
	int count;

public:

	List();
	List(const List<T> &l);
	~List();
	int GetCount();
	Elem<T>* GetElem(int pos);
	void DelAll();
	void Del(int pos);
	void Insert(T data, int pos);
	void AddTail(T data);
	void AddHead(T data);
	void DelHead();
	void DelTail();
	void PrintHead();
	void PrintTail();
	List<T>& operator = (const List<T> &l);

	List(List<T> &&l);
	List<T>& operator = (List<T> &&l);

	int Find(T key);
	void Replace(T val, int pos);
	void DelElementByKey(T key);
	T& operator [] (int pos);
	void Sort();
};

template <typename T>
List<T>::List() {
	Head = Tail = nullptr;
	count = 0;
}

template <typename T>
List<T>::List(const List<T> &l) {
	Head = Tail = nullptr;
	count = 0;
	Elem<T> *temp = l.Head;
	while (temp != nullptr) {
		AddTail(temp->data);
		temp = temp->next;
	}
}

template <typename T>
List<T>::~List() {
	DelAll();
}

template <typename T>
void List<T>::AddHead(T data) {
	Elem<T> *temp = new Elem<T>;
	temp->prev = nullptr;
	temp->data = data;
	temp->next = Head;
	if (Head != nullptr)
		Head->prev = temp;
	Head = temp;
	if (Tail == nullptr)
		Tail = temp;
	++count;
}

template <typename T>
void List<T>::AddTail(T data) {
	Elem<T> *temp = new Elem<T>;
	temp->next = nullptr;
	temp->data = data;
	temp->prev = Tail;
	if (Tail != nullptr)
		Tail->next = temp;
	Tail = temp;
	if (Head == nullptr)
		Head = temp;
	++count;
}

template <typename T>
void List<T>::DelHead() {
	if (Head != nullptr) {
		Elem<T> *temp = Head;
		Head = Head->next;
		if (Head != nullptr)
			Head->prev = nullptr;
		else
			Tail = nullptr;
		delete temp;
		temp = nullptr;
		--count;
	}
}

template <typename T>
void List<T>::DelTail() {
	if (Tail != nullptr) {
		Elem<T> *temp = Tail;
		Tail = Tail->prev;
		if (Tail != nullptr)
			Tail->next = nullptr;
		else
			Head = nullptr;
		delete temp;
		temp = nullptr;
		--count;
	}
}

template <typename T>
void List<T>::Insert(T data, int pos) {
	if (pos < 0 || pos > count)
		return;
	if (pos == 0) {
		AddHead(data);
		return;
	}
	if (pos == count) {
		AddTail(data);
		return;
	}
	Elem<T> *current = nullptr;
	if (pos < count / 2) {
		current = Head;
		int i = 0;
		while (i < pos) {
			current = current->next;
			++i;
		}
	}
	else {
		current = Tail;
		int i = count - 1;
		while (i > pos)
		{
			current = current->prev;
			--i;
		}
	}
	Elem<T> *temp = new Elem<T>;
	temp->data = data;
	temp->prev = current->prev;
	temp->next = current;
	current->prev->next = temp;
	current->prev = temp;
	++count;
}

template <typename T>
void List<T>::Del(int pos) {
	if (pos < 0 || pos >= count)
		return;
	if (pos == 0) {
		DelHead();
		return;
	}
	if (pos == count - 1) {
		DelTail();
		return;
	}
	Elem<T> *delElement = nullptr;
	if (pos < count / 2) {
		delElement = Head;
		int i = 0;
		while (i < pos) {
			delElement = delElement->next;
			++i;
		}
	}
	else {
		delElement = Tail;
		int i = count - 1;
		while (i > pos) {
			delElement = delElement->prev;
			--i;
		}
	}
	delElement->prev->next = delElement->next;
	delElement->next->prev = delElement->prev;
	delete delElement;
	delElement = nullptr;
	--count;
}

template <typename T>
void List<T>::PrintHead() {
	Elem<T> *current = Head;
	while (current != nullptr) {
		cout << current->data;
		current = current->next;
	}
	cout << endl;
}

template <typename T>
void List<T>::PrintTail() {
	Elem<T> *current = Tail;
	while (current != nullptr) {
		cout << current->data;
		current = current->prev;
	}
	cout << endl;
}

template <typename T>
void List<T>::DelAll() {
	while (Head != nullptr)
		DelHead();
}

template <typename T>
int List<T>::GetCount() {
	return count;
}

template <typename T>
Elem<T>* List<T>::GetElem(int pos) {
	Elem<T> *temp = Head;
	if (pos < 0 || pos >= count)
		return nullptr;
	int i = 0;
	while (i < pos) {
		temp = temp->next;
		++i;
	}
	return temp;
}

template <typename T>
List<T>& List<T>::operator = (const List<T> &l) {
	if (this == &l)
		return *this;
	DelAll();
	Elem<T> *temp = l.Head;
	while (temp != nullptr) {
		AddTail(temp->data);
		temp = temp->next;
	}
	return *this;
}

/////////////////////////////////////////////////

template <typename T>
List<T>::List(List<T> &&l) {
	*this = move(l);
}

template <typename T>
List<T>& List<T>::operator = (List &&l) {
	if (this == &l) {
		return *this;
	}
	count = l.count;
	Head = l.Head;
	Tail = l.Tail;
	l.count = 0;
	l.Head = l.Tail = nullptr;
	return *this;
}

template <typename T>
int List<T>::Find(T key) {
	Elem<T> *current = Head;
	int pos = 0;
	while (current != nullptr) {
		if (current->data == key) {
			return pos;
		}
		current = current->next;
		++pos;
	}
	return -1;
}

template <typename T>
void List<T>::Replace(T val, int pos) {
	if (pos < 0 || pos >= count) {
		return;
	}
	Elem<T> *current = nullptr;
	if (pos < count / 2) {
		int i = 0;
		current = Head;
		while (i != pos) {
			current = current->next;
			++i;
		}
	}
	else {
		int i = count - 1;
		current = Tail;
		while (i != pos) {
			current = current->prev;
			--i;
		}
	}
	current->data = val;
}

template <typename T>
void List<T>::DelElementByKey(T key) {
	Elem<T> *current = Head;
	Elem<T> *delElem = nullptr;
	while (current != nullptr) {
		if (current->data == key) {
			if (current->prev == nullptr) {
				current = current->next;
				DelHead();
				continue;
			}
			if (current->next == nullptr) {
				DelTail();
				return;
			}
			delElem = current;
			current->prev->next = current->next;
			current->next->prev = current->prev;
			current = current->next;
			delete delElem;
			delElem = nullptr;
			--count;
			continue;
		}
		current = current->next;
	}
}

// �������, �� ��������� ������
//template <typename T>
//void List<T>::DelElementByKey(T key) {
//	int delPos;
//	while ((delPos = Find(key)) != -1) {
//		Del(delPos);
//	}
//}

template <typename T>
T& List<T>::operator [] (int pos) {
	if (Head == nullptr) {
		// ����������
		AddHead(T());
		return Head->data;
	}
	if (pos < 0) {
		return Head->data;
	}
	if (pos >= count) {
		return Tail->data;
	}
	return GetElem(pos)->data;
}

// ���������� �������������� ����������
template <typename T>
void List<T>::Sort() {
	if (Head == nullptr) {
		return;
	}
	int count = this->count;
	int i;
	bool flag;
	Elem<T> *current = nullptr;
	while (count > 1) {
		current = Head;
		flag = true;
		i = 0;
		while (i < count && current->next != nullptr) {
			if (current->data > current->next->data) {
				if (current->prev == nullptr) {
					current->next->prev = nullptr;
					current->next->next->prev = current;
					current->prev = current->next;
					current->next = current->next->next;
					current->prev->next = current;
					Head = current->prev;
					flag = false;
					++i;
					continue;
				}
				if (current->next->next == nullptr) {
					current->next->prev = current->prev;
					current->prev->next = current->next;
					current->prev = current->next;
					current->next = nullptr;
					current->prev->next = current;
					Tail = current;
					flag = false;
					++i;
					continue;
				}
				current->next->prev = current->prev;
				current->prev->next = current->next;
				current->next->next->prev = current;
				current->prev = current->next;
				current->next = current->next->next;
				current->prev->next = current;
				flag = false;
				++i;
				continue;
			}
			++i;
			current = current->next;
		}
		if (flag) {
			return;
		}
		--count;
	}
}

template <>
inline void List<char*>::Sort() {
	if (Head == nullptr) {
		return;
	}
	int count = this->count;
	int i;
	bool flag;
	Elem<char*> *current = nullptr;
	while (count > 1) {
		current = Head;
		flag = true;
		i = 0;
		while (i < count && current->next != nullptr) {
			if (strcmp(current->data, current->next->data) > 0) {
				if (current->prev == nullptr) {
					current->next->prev = nullptr;
					current->next->next->prev = current;
					current->prev = current->next;
					current->next = current->next->next;
					current->prev->next = current;
					Head = current->prev;
					flag = false;
					++i;
					continue;
				}
				if (current->next->next == nullptr) {
					current->next->prev = current->prev;
					current->prev->next = current->next;
					current->prev = current->next;
					current->next = nullptr;
					current->prev->next = current;
					Tail = current;
					flag = false;
					++i;
					continue;
				}
				current->next->prev = current->prev;
				current->prev->next = current->next;
				current->next->next->prev = current;
				current->prev = current->next;
				current->next = current->next->next;
				current->prev->next = current;
				flag = false;
				++i;
				continue;
			}
			++i;
			current = current->next;
		}
		if (flag) {
			return;
		}
		--count;
	}
}