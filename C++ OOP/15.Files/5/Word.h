#pragma once

using std::string;

class Word {

	string word;
	int count;

public:

	Word();
	Word(const string &str);
	const string& getWord() const;
	int getCount() const;
	void setWord(const string& str);
	bool operator == (const Word &word) const;
	bool operator != (const Word &word) const;
	bool operator > (const Word &word) const;
	bool operator < (const Word &word) const;
	Word& operator ++ ();
	Word& operator -- ();
	const Word operator ++ (int);
	const Word operator -- (int);
};