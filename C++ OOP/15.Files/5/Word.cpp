#include<string>
#include"Word.h"

using std::string;

Word::Word() : count(0) {}

Word::Word(const string &str) {
	word = str;
	count = 1;
}

const string& Word::getWord() const {
	return word;
}

int Word::getCount() const {
	return count;
}

void Word::setWord(const string& str) {
	word = str;
	count = 1;
}

bool Word::operator == (const Word &word) const {
	return this->word == word.word;
}

bool Word::operator != (const Word &word) const {
	return !(*this == word);
}

bool Word::operator > (const Word &word) const {
	return this->count > word.count;
}

bool Word::operator < (const Word &word) const {
	return word > *this;
}

Word& Word::operator ++ () {
	++count;
	return *this;
}

Word& Word::operator -- () {
	--count;
	return *this;
}

const Word Word::operator ++ (int) {
	Word temp = *this;
	++count;
	return temp;
}

const Word Word::operator -- (int) {
	Word temp = *this;
	--count;
	return temp;
}