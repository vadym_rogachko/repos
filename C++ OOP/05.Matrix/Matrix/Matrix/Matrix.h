#pragma once
#include<iostream>

using std::ostream;
using std::istream;

class Matrix
{

	int **p;
	int row, col;

public:

	Matrix();
	Matrix(int row, int col);
	Matrix(const Matrix &m);
	Matrix(Matrix &&m);
	~Matrix();

	Matrix& operator = (const Matrix &m);
	Matrix& operator = (Matrix &&m);

	Matrix& operator ++ ();
	Matrix operator ++ (int);
	Matrix& operator -- ();
	Matrix operator -- (int);
	Matrix operator + (const Matrix &m) const;
	Matrix operator * (const Matrix &m) const;

	int& operator () (int row, int col);
	int* operator [] (int index);

	friend istream& operator >> (istream &in, Matrix &m);
	friend ostream& operator << (ostream &out, const Matrix &m);
};