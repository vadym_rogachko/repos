#include"Matrix.h"

using std::endl;
using std::cout;

Matrix::Matrix() : row(0), col(0), p(nullptr) {}

Matrix::Matrix(int row, int col) {
	if (row <= 0 || col <= 0) {
		this->Matrix::Matrix();
		return;
	}
	this->row = row;
	this->col = col;
	p = new int*[row];
	p[0] = new int[row * col];
	for (int i = 1; i < row; ++i) {
		p[i] = p[i - 1] + col;
	}
	memset(p[0], 0, row * col * sizeof p[0][0]);
}

Matrix::Matrix(const Matrix &m) {
	if (m.row <= 0 || m.col <= 0) {
		this->Matrix::Matrix();
		return;
	}
	this->Matrix::Matrix(m.row, m.col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			p[i][j] = m.p[i][j];
		}
	}
}

Matrix::Matrix(Matrix &&m) : row(m.row), col(m.col), p(m.p) {
	m.p = nullptr;
	m.row = m.col = 0;
}

Matrix::~Matrix() {
	if (p) {
		delete[] p[0];
		delete[] p;
		row = col = 0;
	}
}

Matrix& Matrix::operator = (const Matrix &m) {
	if (this == &m) {
		return *this;
	}
	this->~Matrix();
	if (m.p == nullptr) {
		this->Matrix::Matrix();
		return *this;
	}
	this->Matrix::Matrix(m.row, m.col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			p[i][j] = m.p[i][j];
		}
	}
	return *this;
}

Matrix& Matrix::operator = (Matrix &&m) {
	if (this == &m) {
		return *this;
	}
	this->~Matrix();
	row = m.row;
	col = m.col;
	p = m.p;
	m.p = nullptr;
	m.row = m.col = 0;
	return *this;
}

Matrix& Matrix::operator ++ () {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			++p[i][j];
		}
	}
	return *this;
}

Matrix Matrix::operator ++ (int) {
	Matrix temp = *this;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			++p[i][j];
		}
	}
	return temp;
}

Matrix& Matrix::operator -- () {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			--p[i][j];
		}
	}
	return *this;
}

Matrix Matrix::operator -- (int) {
	Matrix temp = *this;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			--p[i][j];
		}
	}
	return temp;
}

Matrix Matrix::operator + (const Matrix &m) const {
	if (row != m.row || col != m.col) {
		return Matrix(0, 0);
	}
	Matrix temp(row, col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			temp.p[i][j] = p[i][j] + m.p[i][j];
		}
	}
	return temp;
}

Matrix Matrix::operator * (const Matrix &m) const {
	if (p == nullptr || m.p == nullptr) {
		return Matrix(0, 0);
	}
	if (col != m.row) {
		return Matrix(0, 0);
	}
	Matrix temp(row, m.col);
	for (int i = 0; i < temp.row; ++i) {
		for (int j = 0; j < temp.col; ++j) {
			for (int k = 0; k < col; ++k) {
				temp.p[i][j] += p[i][k] * m.p[k][j];
			}
		}
	}
	return temp;
}

int& Matrix::operator () (int row, int col) {
	if (this->row == 0 || this->col == 0) {
		cout << "Error\nMatrix is empty\n\n";
		exit(1);
	}
	if (row < 0 || col < 0) {
		return p[0][0];
	}
	if (row >= this->row || col >= this->col) {
		return p[this->row - 1][this->col - 1];
	}
	return p[row][col];
}

// �������� [] -> [][]
int* Matrix::operator [] (int index) {
	if (this->row == 0 || this->col == 0) {
		cout << "Error\nMatrix is empty\n\n";
		exit(1);
	}
	if (index < 0) {
		return p[0];
	}
	if (index >= this->row) {
		return p[this->row - 1];
	}
	return p[index];
}

istream& operator >> (istream &in, Matrix &m) {
	if (m.row == 0 || m.col == 0) {
		cout << "Matrix is empty\n\n";
		return in;
	}
	for (int i = 0; i < m.row; ++i) {
		for (int j = 0; j < m.col; ++j) {
			in >> m.p[i][j];
		}
	}
	return in;
}

ostream& operator << (ostream &out, const Matrix &m) {
	if (m.row == 0 || m.col == 0) {
		cout << "Matrix is empty\n\n";
		return out;
	}
	for (int i = 0; i < m.row; ++i) {
		for (int j = 0; j < m.col; ++j) {
			out << m.p[i][j] << ' ';
		}
		out << endl;
	}
	out << endl;
	return out;
}