#include<iostream>
#include"Matrix.h"

using std::cout;
using std::cin;
using std::endl;

void main() {

	setlocale(LC_ALL, "rus");

	cout << "���������� ��������� <<\n";
	cout << "\n����������� �� ���������\n";
	Matrix matr1, matr2(4, 2);
	cout << matr1;
	cout << "����������� � ����� �����������\n";
	cout << matr2;
	cout << "���������� ��������� >>\n\n";
	cin >> matr2;
	cout << endl << matr2;

	cout << "����������� ����������� � �������� ������������\n(������������� ������������)\n\n";
	cout << matr2;
	Matrix matr3 = matr1 = matr2;
	cout << matr3 << matr1 << endl;

	cout << "\n���������� ����������� ����������\n\n";
	cout << matr1;
	cout << ++matr1;
	cout << matr1;

	cout << "\n���������� ����������� ����������\n\n";
	cout << matr1;
	cout << --matr1;
	cout << matr1;

	cout << "\n���������� ������������ ����������\n\n";
	cout << matr1;
	cout << matr1++;
	cout << matr1;

	cout << "\n���������� ������������ ����������\n\n";
	cout << matr1;
	cout << matr1--;
	cout << matr1;

	cout << "\n���������� ��������� +(�������� ������) � ����������� ��������\n\n";
	cout << matr1 << "+\n\n" << matr2;
	Matrix matr4 = matr1 + matr2;
	cout << "=\n\n" << matr4;

	cout << "\n���������� ��������� *(��������� ������) � �������� ������������ � ���������\n";
	Matrix matr5(2, 3);
	cout << "������� �����\n";
	cin >> matr5;
	cout << endl << matr4 << "*\n\n" << matr5;
	matr1 = matr4 * matr5;
	cout << "=\n\n" << matr1;

	cout << "\n��������� ������ � ������ �������� �������� ������ ������� �     ����� ������ �������\n";
	Matrix matr = matr5 * matr4;
	cout << matr;

	cout << "\n�������� ������ ������� �������\n";
	matr = matr1 + matr4;
	cout << matr;

	cout << "\n���������� ��������� ()\n";
	cout << "\n����\n";
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 3; ++j) {
			cin >> matr5(i, j);
		}
	}
	cout << "\n�����\n";
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 3; ++j) {
			cout << matr5(i, j) << ' ';
		}
		cout << endl;
	}
	cout << endl;

	cout << "\n���������� ��������� []\n";
	cout << "\n����\n";
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 3; ++j) {
			cin >> matr5[i][j];
		}
	}
	cout << "\n�����\n";
	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 3; ++j) {
			cout << matr5[i][j] << ' ';
		}
		cout << endl;
	}
	cout << endl;

	system("pause");
}