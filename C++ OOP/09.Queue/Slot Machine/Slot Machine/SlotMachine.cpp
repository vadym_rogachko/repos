#include<iostream>
#include<conio.h>
#include<windows.h>
#include"SlotMachine.h"
#include<string>

using std::string;
using std::cin;

SlotMachine::Drum::Drum() {
	signs signs[SIGNS] = { HEARTS, SEVEN, NOTE, DOLLAR };
	for (int i = 0; i < SIGNS; ++i) {
		reel1.Add(signs[i]);
		reel2.Add(signs[i]);
		reel3.Add(signs[i]);
	}
	for (int i = 0; i < REELS; ++i) {
		screen[i] = DOLLAR;
	}
}

SlotMachine::SlotMachine() :
	machineCoins(100000),
	playerCoins(0),
	jackpot(0),
	jackpotSum(JACKPOT),
	machineON_OFF(OFF) {}

SlotMachine::~SlotMachine() {
	machineCoins += abs(playerCoins);
	playerCoins = 0;
	jackpot = 0;
	jackpotSum = 0;
	while (BankRequest(machineCoins, PROFIT) != true) {}
	SwitchOFF();
}

void SlotMachine::Switch() {
	if (machineCoins <= 0) {
		GetMoney();
		SwitchOFF();
	}
	if (playerCoins <= 0) {
		SwitchOFF();
		PutMoney();
		ShowScreen();
		system("pause >> void");
	}
}

void SlotMachine::SwitchON() {
	machineON_OFF = ON;
}

void SlotMachine::SwitchOFF() {
	machineON_OFF = OFF;
}

bool SlotMachine::isON() {
	return machineON_OFF == ON;
}

void SlotMachine::ShowScreen() {
	system("cls");
	for (int i = 0; i < Drum::REELS; ++i) {
		cout << char(drum.screen[i]) << '\t';
	}
	cout << endl;
	cout << playerCoins << endl;
	cout << "JACKPOT: " << jackpotSum << endl;
}

void SlotMachine::PressButton() {
	if (!isON()) return;
	char ch;
	do {
		ch = _getch();
	} while (ch != ESC && ch != ENTER);
	if (ch == ESC) GetMoney(), SwitchOFF();
	else Switch();
}

void SlotMachine::Play() {
	while (isON()) {
		TurnDrum();
	}
}

void SlotMachine::TurnDrum() {
	if (!isON()) return;
	int reel1Turn, reel2Turn, reel3Turn;
	reel1Turn = rand() % 45 + 20;
	reel2Turn = rand() % 45 + 20;
	reel3Turn = rand() % 45 + 20;
	int turnMax = reel1Turn > reel2Turn && reel1Turn > reel3Turn ? reel1Turn :
		reel2Turn > reel1Turn && reel2Turn > reel3Turn ? reel2Turn : reel3Turn;
	while (turnMax) {
		if (reel1Turn) {
			drum.reel1.Remove(drum.screen[0]);
			--reel1Turn;
		}
		if (reel2Turn) {
			drum.reel2.Remove(drum.screen[1]);
			--reel2Turn;
		}
		if (reel3Turn) {
			drum.reel3.Remove(drum.screen[2]);
			--reel3Turn;
		}
		ShowScreen();
		Sleep(1000 / turnMax);
		--turnMax;
	}
	Winning();
}

void SlotMachine::Winning() {
	if (!isON()) return;
	int hearts(0), seven(0), notes(0), dollars(0);
	int winning(0);
	for (int i = 0; i < Drum::REELS; ++i) {
		if (drum.screen[i] == Drum::HEARTS && ++hearts) 
			continue;
		if (drum.screen[i] == Drum::SEVEN && ++seven)
			continue;
		if (drum.screen[i] == Drum::NOTE && ++notes)
			continue;
		if (drum.screen[i] == Drum::DOLLAR && ++dollars)
			continue;
	}
	if (hearts != 3) {
		if (jackpot == 2) {
			jackpotSum += 777;
		}
		jackpot = 0;
	}
	if (dollars == 3) {
		winning = 22;
	}
	else if (notes == 3) {
		winning = 11;
	}
	else if (seven == 3) {
		winning = 7;
	}
	else if (hearts == 3) {
		winning = -33;
		jackpotSum += 77;
		++jackpot;
		if (jackpot == 2) {
			winning -= 33;
		}
		if (jackpot == 3) {
			cout << "JACKPOT!!!\n";
			cout << jackpotSum << endl;
			winning += jackpotSum;
		}
	}
	else if (dollars == 2) {
		winning = -3;
		jackpotSum += 7;
	}
	else if (seven == 2) {
		winning = -7;
		jackpotSum += 7;
	}
	else if (notes == 2) {
		winning = -11;
		jackpotSum += 7;
	}
	else if (hearts == 2) {
		winning = 22;
	}
	else {
		winning = -1;
		jackpotSum += 1;
	}
	if (winning >= 0) cout << '+';
	cout << winning << endl;
	playerCoins += winning;
	machineCoins -= winning;
	PressButton();
}

bool SlotMachine::BankRequest(int money, request operation) {
	if (operation == PROFIT) {
		machineCoins -= money;
		return true;
	}
	if (money <= 0) return false;
	cout << "Sending request";
	int dot = rand() % 4 + 3;
	while (dot--) {
		cout << '.';
		Sleep(1000);
	}
	if (operation == DEPOSIT) {
		if (!(rand() % 11)) return false;
		machineCoins += money;
	}
	else {
		machineCoins -= money;
	}
	return true;
}

void SlotMachine::PutMoney() {
	ShowScreen();
	system("pause >> void");
	system("cls");
	int money;
	if (!creditCard[0]) {
		while (!creditCard[0]) {
			system("cls");
			cout << "Enter your credit card: ";
			getline(cin, creditCard);
		}
	}
	cout << "Your money: " << playerCoins;
	cout << "\nDeposit: ";
	cin >> money;
	if (money < 0) {
		cout << "Error\n";
		system("pause >> void");
		return;
	}
	else if (money == 0) {
		return;
	}
	if (BankRequest(money, DEPOSIT)) {
		playerCoins += money;
		if (playerCoins > 0) {
			SwitchON();
		}
		cout << "OK\n";
		system("pause >> void");
		system("cls");
	}
	else {
		cout << "\nNo money on your credit card\n";
		system("pause >> void");
	}
}

void SlotMachine::GetMoney() {
	ShowScreen();
	system("pause >> void");
	system("cls");
	if (playerCoins <= 0) {
		system("cls");
		cout << "Your money: " << playerCoins;
		cout << endl;
		system("pause >> void");
		return;
	}
	int money;
	do {
		system("cls");
		cout << "Your money: " << playerCoins;
		cout << "\nWithdraw: ";
		cin >> money;
	} while (money > playerCoins);
	if (BankRequest(money, WITHDRAW)) {
		playerCoins -= money;
		cout << "OK\n";
		system("pause >> void");
		SwitchOFF();
	}
	else {
		cout << "Error\n";
		system("pause >> void");
	}
}