#include<iostream>
#include<time.h>
#include<windows.h>
#include"SlotMachine.h"


void main() {

	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO info;
	info.bVisible = false;
	info.dwSize = 10;
	SetConsoleCursorInfo(handle, &info);

	SlotMachine machine;
	machine.Switch();
	while (machine.isON()) {
		machine.Play();
	}

	system("cls");
}
