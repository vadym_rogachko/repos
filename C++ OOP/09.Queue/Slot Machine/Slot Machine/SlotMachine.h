#pragma once
#include"Queue.h"

using std::string;

class SlotMachine {

	enum request {
		DEPOSIT = '+',
		WITHDRAW = '-',
		PROFIT = '$'
	};
	enum {
		JACKPOT = 7777,
	};
	enum buttons {
		ENTER = 13,
		ESC = 27,
	};
	enum ON_OFF {
		OFF,
		ON,
	};

	struct Drum {
		enum {
			REELS = 3,
			SIGNS = 4
		};
		enum signs {
			HEARTS = 3,
			SEVEN = '7',
			NOTE = 14,
			DOLLAR = 36,
		};

		signs screen[REELS];

		Queue<signs> reel1;
		Queue<signs> reel2;
		Queue<signs> reel3;

		Drum();
	};

	ON_OFF machineON_OFF;
	Drum drum;

	int machineCoins;
	int playerCoins;
	string creditCard;

	int jackpot;
	int jackpotSum;


	void TurnDrum();
	void Winning();
	void ShowScreen();
	void PressButton();
	
	void SwitchON();
	void SwitchOFF();

	void PutMoney();
	void GetMoney();
	bool BankRequest(int money, request operation);

public:

	SlotMachine();
	~SlotMachine();

	void Play();
	bool isON();
	void Switch();
};