#include<iostream>
#include<iomanip>
#include"CStudent.h"

using std::cout;
using std::endl;
using std::setw;
using std::left;
using std::cin;

CStudent::CStudent() : name(nullptr), surname(nullptr), age(0), average(0) {
	memset(phone, '\0', PHONE_S);
}

CStudent::CStudent(const char *name, const char *surname, int age, const char *phone, double average) {
	setName(name);
	setSurname(surname);
	setPhone(phone);
	setAge(age);
	setAverage(average);
}

CStudent::~CStudent() {
	delete[] name;
	delete[] surname;
}

CStudent::CStudent(const CStudent &student) {
	setName(student.name);
	setSurname(student.surname);
	setPhone(student.phone);
	setAge(student.age);
	setAverage(student.average);
}

CStudent::CStudent(CStudent &&student) : age(student.age), average(student.average) {
	name = student.name;
	surname = student.surname;
	student.name = student.surname = nullptr;
	memcpy(phone, student.phone, PHONE_S);
}

// ���������� ��������� ������������ � ������������ � ������� ������ swap
CStudent& CStudent::operator = (const CStudent &student) {
	if (this == &student) {
		return *this;
	}
	// ����������� ����������� CStudent
	CStudent(student).swap(*this);
	return *this;
}

void CStudent::swap(CStudent &student) {
	std::swap(name, student.name);
	std::swap(surname, student.surname);
	std::swap(age, student.age);
	std::swap(average, student.average);
	std::swap(phone, student.phone);
}

CStudent& CStudent::operator = (CStudent &&student) {
	if (this == &student) {
		return *this;
	}
	this->~CStudent();
	name = student.name;
	surname = student.surname;
	student.name = student.surname = nullptr;
	age = student.age;
	average = student.average;
	memcpy(phone, student.phone, PHONE_S);
	return *this;
}

istream& operator >> (istream& in, CStudent &student) {
	char name[NAME_S]{};
	char surname[SURNAME_S]{};
	int age;
	char phone[PHONE_S]{};
	double average;
	char *temp;
	cout << "Enter the surname\n";
	fgets(surname, NAME_S, stdin);
	if (temp = strchr(surname, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(surname, SURNAME_S);
	surname[0] = toupper(surname[0]);
	cout << "Enter the name\n";
	fgets(name, NAME_S, stdin);
	if (temp = strchr(name, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(name, NAME_S);
	name[0] = toupper(name[0]);
	cout << "Enter the phone number\n";
	fgets(phone, NAME_S, stdin);
	if (temp = strchr(phone, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	cout << "Enter the age\n";
	cin >> age;
	cin.get();
	cout << "Enter the average point\n";
	cin >> average;
	student.CStudent::CStudent(name, surname, age, phone, average);
	return in;
}

ostream& operator << (ostream& out, const CStudent &student) {
	if (student.name == nullptr) {
		out << "No data\n";
		return out;
	}
	out << setw(WIDTH) << left << student.surname;
	out << setw(WIDTH) << left << student.name;
	out << setw(WIDTH) << left << student.phone;
	out << setw(WIDTH - 7) << left << student.age;
	out << setw(WIDTH - 5) << left << student.average << endl;
	return out;
}

const char* CStudent::getName() const {
	return name;
}

const char* CStudent::getSurname() const {
	return surname;
}

int CStudent::getAge() const {
	return age;
}

double CStudent::getAverage() const {
	return average;
}

const char* CStudent::getPhone() const {
	return phone;
}

void CStudent::setName(const char *name) {
	delete[] this->name;
	int size = strlen(name) + 1;
	this->name = new char[size];
	strcpy_s(this->name, size, name);
}

void CStudent::setSurname(const char *surname) {
	delete[] this->surname;
	int size = strlen(surname) + 1;
	this->surname = new char[size];
	strcpy_s(this->surname, size, surname);
}

void CStudent::setPhone(const char *phone) {
	strncpy_s(this->phone, PHONE_S, phone, PHONE_S - 1);
}

void CStudent::setAge(int age) {
	if (age <= 0 || age > 99) {
		this->age = 0;
		return;
	}
	this->age = age;
}

void CStudent::setAverage(double average) {
	if (average <= 1. || average > 12.) {
		this->average = 0;
		return;
	}
	this->average = average;
}