#include<iostream>
#include<conio.h>
#include<windows.h>
#include"CAcademyGroup.h"
#include"CStudent.h"


/*

�������� ���������, ����������� ����������� ������ ��������� ������������� ������. ��������� ��������� ������ ������������� ������������ ��������� �����������:
a. ���������� ��������� � ������;
b. �������� �������� �� ������;
c. ����������� ������ ��������;
d. ������ ������������� ������;
e. ���������� ������ ���������;
f. ����� �������� �� ��������� ��������;
g. ���������� ������ ��������� � ���� (��� ���������� ������ ���������);
h. �������� ������ ��������� �� ����� (��� ������ ���������);
i. ����� �� ���������.
��� ���������� ��������� ������� ������������ ��������� ������������ ������ ��� �������� ������ ��������� (���������� ������������ ������ ���������� �� ������� ������ Student).

*/

using std::cout;
using std::endl;
using std::cin;

/*��� �����*/
////////////////////////////////////////////////////
CAcademyGroup foo(const CAcademyGroup &group) {   //
	CAcademyGroup test(group);                    //
	return test;                                  //
}                                                 //
                                                  //
CStudent bar(const CStudent &student) {           //
	CStudent test(student);                       //
	return test;                                  //
}                                                 //
////////////////////////////////////////////////////

void main() {

	setlocale(LC_ALL, "rus");

	CAcademyGroup group;
	group.Load();
	bool isExit = false;
	while (!isExit) {
		char choice;
		system("cls");
		cout << "Academy group\n"
				"1.Add\n"
				"2.Delete\n"
				"3.Edit\n"
				"4.Print\n"
				"5.Find\n"
				"6.Sort\n";
		do {
			choice = _getch();
		} while ((choice < ADD || choice > SORT) && choice != ESC);
		while (_kbhit()) {
			_getch();
		}
		switch (choice) {
		case ADD:
			group.AddStudents();
			break;
		case DEL:
			group.DeleteStudent();
			break;
		case EDIT:
			group.EditStudent();
			break;
		case PRINT:
			// ������������� �������� << CAcademyGroup
			cout << group;
			break;
		case FIND:
			group.FindStudent();
			break;
		case SORT:
			group.Sort(0, group.getCount() - 1);
			if (group.getCount() != 0) {
				system("cls");
				cout << "Sorted\n";
				system("pause >> void");
			}
			break;
		case ESC:
			char choice;
			system("cls");
			cout << "Are you sure you want to exit?(Y/N)\n";
			do {
				choice = toupper(_getch());
			} while (choice != YES && choice != NO);
			isExit = choice == YES;
			system("cls");
			break;
		}
	}

	// ������������� �������� >> ��� ����� ������� � ������ ������ ������������ � ����������

	/*����*/
	CAcademyGroup testGroup1 = group; // ����������� ����������� CAcademyGroup
	cout << "Test\n\n";
	system("pause");
	system("cls");
	cout << "������������� �������� << CAcademyGroup\n\n";
	system("pause");
	cout << testGroup1; // ������������� �������� << CAcademyGroup

	CAcademyGroup testGroup2;
	testGroup2 = testGroup1; // ������������� �������� = � ������������ CAcademyGroup
	system("cls");
	cout << "������������� �������� = � ������������ CAcademyGroup\n\n";
	system("pause");
	cout << testGroup2;

	CStudent testStudent1 = group[3]; // ����������� ����������� CStudent � �������� [] CAcademyGroup ������
	system("cls");
	cout << "����������� ����������� CStudent � �������� [] CAcademyGroup ������ � ������������� �������� << CStudent\n\n";
	system("pause");
	system("cls");
	cout << testStudent1;  // ������������� �������� << CStudent
	system("pause");

	system("cls");
	cout << "������������� �������� = � ������������ CStudent � �������� [] CAcademyGroup ������ � ������\n\n";
	system("pause");
	for (int i = 0; i < testGroup2.getCount() && i < group.getCount(); ++i) {
		testGroup2[i] = group[i]; // ������������� �������� = � ������������ CStudent � �������� [] CAcademyGroup ������ � ������
	}
	cout << testGroup2;

	CAcademyGroup testGroup3 = foo(group); // ����������� ����������� � ��������� CAcademyGroup
	system("cls");
	cout << "����������� ����������� � ��������� CAcademyGroup\n\n";
	system("pause");
	cout << testGroup3;

	testGroup1 = foo(testGroup2); // ������������� �������� = � ��������� CAdaemyGroup
	
	system("cls");
	cout << "������������� �������� = � ��������� CAdaemyGroup\n\n";
	system("pause");
	cout << testGroup1;

	CStudent testStudent2 = bar(group[6]); // ����������� ����������� � ��������� CStudent
	system("cls");
	cout << "����������� ����������� � ��������� CStudent\n\n";
	cout << testStudent2;
	system("pause");

	system("cls");
	cout << "������������� �������� = � ��������� CStudent\n\n";
	cin >> testStudent1;
	testStudent2 = bar(testStudent1); // ������������� �������� = � ��������� CStudent
	cout << endl << testStudent2;

	group.Save();
	system("pause");
}