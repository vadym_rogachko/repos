#pragma once
#include"CStudent.h"

class CAcademyGroup
{
	CStudent **pSt;
	int count;

	enum criterion {
		NAME = '1',
		SURNAME,
		PHONE,
		AGE,
		AVERAGE
	};

public:

	CAcademyGroup();
	CAcademyGroup(const CAcademyGroup &group);
	CAcademyGroup(CAcademyGroup &&group);
	~CAcademyGroup();

	CAcademyGroup& operator = (const CAcademyGroup &group);
	CAcademyGroup& operator = (CAcademyGroup &&group);

	CStudent& operator [] (int index);
	const CStudent& operator [] (int index) const;

	friend ostream& operator << (ostream &out, const CAcademyGroup &group);

	int getCount() const;
	void AddStudents();
	void DeleteStudent();
	void EditStudent();
	void Print() const;
	void FindStudent() const;
	void Sort(short left, short right);
	void Save() const;
	void Load();
};