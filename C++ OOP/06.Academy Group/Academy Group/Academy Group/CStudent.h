#pragma once
#include"enum.h"
#include<iostream>

using std::istream;
using std::ostream;

class CStudent
{
	char *name;
	char *surname;
	int age;
	char phone[PHONE_S];
	double average;

	void swap(CStudent &student); // ����� ����� ��� ���������� �������� ����� ���������� ��������� ������������ � ������������

public:

	CStudent();
	CStudent(const char *name, const char *surname, int age, const char * phone, double average);
	CStudent(const CStudent &student);
	CStudent(CStudent &&student);
	~CStudent();

	CStudent& operator = (const CStudent &student);
	CStudent& operator = (CStudent &&student);

	friend istream& operator >> (istream& in, CStudent &student);
	friend ostream& operator << (ostream& out, const CStudent &student);

	const char* getName() const;
	const char* getSurname() const;
	int getAge() const;
	double getAverage() const;
	const char* getPhone() const;
	void setName(const char *name);
	void setSurname(const char *surname);
	void setPhone(const char *phone);
	void setAge(int age);
	void setAverage(double average);
};