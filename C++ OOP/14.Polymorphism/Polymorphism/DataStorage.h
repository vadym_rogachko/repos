#pragma once
#include<iostream>

using std::istream;
using std::ostream;


class DataStorage {

protected:

	char name[32];
	char manufacturer[16];
	char model[32];
	double volume;
	unsigned int count;

	void setName(const char* name);

public:
	
	DataStorage();
	DataStorage(const char* manufacturer, const char* model, const char* name, double volume, unsigned int count);
	virtual ~DataStorage() = 0;

	const char* getManufacturer() const;
	const char* getModel() const;
	const char* getName() const;
	double getVolume() const;
	unsigned int getCount() const;

	void setManufacturer(const char* manufacturer);
	void setModel(const char* model);
	void setVolume(double volume);
	void setCount(unsigned int count);

	virtual void Print() const;
	virtual void Save(FILE* file) const;
	virtual void Load(FILE* file);

	virtual bool operator == (DataStorage &storage);

	friend istream& operator >> (istream &is, DataStorage &storage);
	friend ostream& operator << (ostream &os, const DataStorage &storage);

	enum size {
		MANUF_S = 16,
		MODEL_S = 32,
		NAME_S = 32,
		VOL_S = 8,
		COUNT_S = 8
	};
};