#include<iostream>
#include"ListDataStorage.h"
#include"DataStorage.h"
#include"Console.h"

using std::cout;

void main() {
	
	ListDataStorage list;
	list.Load("ListDataStorage.bin");
	ConsoleDataStorage console;
	while (console.isNoExit()) {
		console.Menu();
		switch (console.Choice()) {
		case ConsoleDataStorage::ADD:
			list.Add();
			break;
		case ConsoleDataStorage::DEL:
			list.Delete();
			break;
		case ConsoleDataStorage::PRINT:
			list.Print();
			break;
		case ConsoleDataStorage::EDIT:
			list.Edit();
			break;
		case ConsoleDataStorage::FIND:
			list.Find();
			system("pause >> void");
			break;
		}
	}
	list.Save("ListDataStorage.bin");
}