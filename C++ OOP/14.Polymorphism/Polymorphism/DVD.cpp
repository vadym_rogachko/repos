#include<iostream>
#include<iomanip>
#include"DVD.h"

using std::cout;
using std::setw;
using std::left;

DVDdisc::DVDdisc() :
	speedRec(0)
{
	setName(typeid(DVDdisc).name());
}

DVDdisc::DVDdisc(const char* manufacturer, const char* model, double volume, int count, unsigned int speedRec) :
	DataStorage(manufacturer, model, typeid(DVDdisc).name(), volume, count)
{
	setSpeedRec(speedRec);
}

unsigned int DVDdisc::getSpeedRec() const {
	return speedRec;
}

void DVDdisc::setSpeedRec(unsigned int speedRec) {
	if (speedRec >= 0) {
		this->speedRec = speedRec;
	}
	else {
		this->speedRec = 0;
	}
}

void DVDdisc::Print() const {
	DataStorage::Print();
	cout << setw(SPEED_S) << left << speedRec;
}

void DVDdisc::Save(FILE* file) const {
	DataStorage::Save(file);
	fwrite(&speedRec, sizeof(unsigned int), 1, file);
}

void DVDdisc::Load(FILE* file) {
	DataStorage::Load(file);
	fread_s(&speedRec, sizeof speedRec, sizeof(unsigned int), 1, file);
}

bool DVDdisc::operator == (DataStorage &storage) {
	bool isFound = false;
	bool notFound = false;
	try {
		isFound = DataStorage::operator==(storage);
	}
	catch (int e) {
		notFound = true;
	}
	if (notFound) {
		if (dynamic_cast<DVDdisc&>(storage).speedRec) {
			isFound = speedRec == dynamic_cast<DVDdisc&>(storage).speedRec;
		}
	}
	return isFound;
}

istream& operator >> (istream &is, DVDdisc &dvd) {
	unsigned int speedRec;
	is >> (DataStorage &)dvd;
	cout << "\nRecording speed: ";
	is >> speedRec;
	dvd.setSpeedRec(speedRec);
	is.get();
	return is;
}

ostream& operator << (ostream &os, const DVDdisc &dvd) {
	os << (const DataStorage &)dvd;
	os << setw(DVDdisc::SPEED_S) << left << dvd.speedRec;
	return os;
}