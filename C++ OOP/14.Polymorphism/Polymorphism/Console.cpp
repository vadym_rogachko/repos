#include"Console.h"
#include<conio.h>
#include<iostream>

using std::cout;

ConsoleDataStorage::ConsoleDataStorage() :
	choice(0)
{}

void ConsoleDataStorage::Menu() const {
	system("cls");
	cout << "Data Storage\n"
			"1.Add\n"
			"2.Delete\n"
			"3.Print\n"
			"4.Edit\n"
			"5.Find\n";
}

char ConsoleDataStorage::Choice() {
	do {
		choice = _getch();
	} while (choice != EXIT && (choice < ADD || choice > FIND));
	return choice;
}

bool ConsoleDataStorage::isNoExit() const {
	return choice != EXIT;
}