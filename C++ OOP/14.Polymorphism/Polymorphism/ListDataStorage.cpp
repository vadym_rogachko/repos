#include<iostream>
#include<conio.h>
#include<iomanip>
#include<io.h>
#include"ListDataStorage.h"
#include"DVD.h"
#include"HDDPortable.h"
#include"USBFlash.h"
#include"DataStorage.h"

using std::cout;
using std::cin;
using std::setw;
using std::left;
using std::endl;

enum ListDataStorage::storage {
	USB = '1', HDD, DVD,
	NONE = 27
};

enum ListDataStorage::criterion {
	ALL = '0',
	MANUF, MODEL, VOL, COUNT,
	SPEED,
	STD
};

ListDataStorage::~ListDataStorage() {
	for (int i = 0; i < list.GetCount(); ++i) {
		delete list[i];
	}
}

void ListDataStorage::Add() {
	DataStorage* temp;
	switch (getChoice()) {
	case USB:
		temp = new USBFlash;
		cin >> *dynamic_cast<USBFlash*>(temp);
		break;
	case HDD:
		temp = new HDDPortable;
		cin >> *dynamic_cast<HDDPortable*>(temp);
		break;
	case DVD:
		temp = new DVDdisc;
		cin >> *dynamic_cast<DVDdisc*>(temp);
		break;
	default:
		return;
	}
	list.AddTail(temp);
	system("cls");
	cout << "\nData storage added\n";
	system("pause >> void");
}

void ListDataStorage::Delete() {
	if (list.GetCount() == 0) {
		system("cls");
		cout << "List is empty\n\n";
		system("pause >> void");
		return;
	}
	int index = Find();
	if (index == -1) {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
		return;
	}
	system("cls");
	cout << *list[index];
	cout << "\nDelete? (Y/N)\n";
	char ch;
	do {
		ch = toupper(_getch());
	} while (ch != 'Y' && ch != 'N');
	if (ch == 'Y') {
		delete list[index];
		list.Del(index);
		cout << "\nData storage deleted\n";
		system("pause >> void");
	}
}

void ListDataStorage::Print() {
	if (list.GetCount() == 0) {
		system("cls");
		cout << "List is empty\n\n";
		system("pause >> void");
		return;
	}
	bool flag = true;
	switch (getChoice()) {
	case USB:
		for (int i = 0; i < list.GetCount(); ++i) {
			if (typeid(*list[i]) == typeid(USBFlash)) {
				if (flag) {
					PrintHeader(typeid(*list[i]));
					flag = false;
				}
				list[i]->Print();
				cout << endl;
			}
		}
		break;
	case HDD:
		for (int i = 0; i < list.GetCount(); ++i) {
			if (typeid(*list[i]) == typeid(HDDPortable)) {
				if (flag) {
					PrintHeader(typeid(*list[i]));
					flag = false;
				}
				list[i]->Print();
				cout << endl;
			}
		}
		break;
	case DVD:
		for (int i = 0; i < list.GetCount(); ++i) {
			if (typeid(*list[i]) == typeid(DVDdisc)) {
				if (flag) {
					PrintHeader(typeid(*list[i]));
					flag = false;
				}
				list[i]->Print();
				cout << endl;
			}
		}
		break;
	}
	if (flag) {
		system("cls");
		cout << "List is empty\n\n";
		system("pause >> void");
	}
	else {
		system("pause >> void");
	}
}

void ListDataStorage::Edit() {
	if (list.GetCount() == 0) {
		system("cls");
		cout << "List is empty\n\n";
		system("pause >> void");
		return;
	}
	int index = Find();
	if (index == -1) {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
		return;
	}
	bool edit = true;
	while (true) {
		PrintHeader(typeid(*list[index]));
		list[index]->Print();
		double keyDouble;
		unsigned int keyInt;
		char keyString[DataStorage::MANUF_S > DataStorage::MODEL_S ? DataStorage::MANUF_S : DataStorage::MODEL_S];
		switch (getCriterion(index)) {
		case MANUF:
			cout << "Manufacturer: ";
			cin.getline(keyString, list[index]->MANUF_S);
			list[index]->setManufacturer(keyString);
			break;
		case MODEL:
			cout << "Model: ";
			cin.getline(keyString, list[index]->MODEL_S);
			list[index]->setModel(keyString);
			break;
		case VOL:
			cout << "Volume: ";
			cin >> keyDouble;
			list[index]->setVolume(keyDouble);
			break;
		case COUNT:
			cout << "Count: ";
			cin >> keyInt;
			list[index]->setCount(keyInt);
			break;
		case SPEED:
			if (typeid(*list[index]) == typeid(USBFlash)) {
				cout << "Speed usb: ";
				cin >> keyInt;
				dynamic_cast<USBFlash*>(list[index])->setSpeedUSB(keyInt);
			}
			else if (typeid(*list[index]) == typeid(HDDPortable)) {
				cout << "Speed rotation: ";
				cin >> keyInt;
				dynamic_cast<HDDPortable*>(list[index])->setSpeedRotation(keyInt);
			}
			else if (typeid(*list[index]) == typeid(DVDdisc)) {
				cout << "Speed recording: ";
				cin >> keyInt;
				dynamic_cast<DVDdisc*>(list[index])->setSpeedRec(keyInt);
			}
			break;
		case STD:
			if (typeid(*list[index]) == typeid(USBFlash)) {
				cout << "USB standart: ";
				cin >> keyDouble;
				dynamic_cast<USBFlash*>(list[index])->setStandart(keyDouble);
			}
			break;
		case ALL:
			cout << "Edit:\n\n";
			if (typeid(*list[index]) == typeid(USBFlash)) {
				cin >> *dynamic_cast<USBFlash*>(list[index]);
			}
			else if (typeid(*list[index]) == typeid(HDDPortable)) {
				cin >> *dynamic_cast<HDDPortable*>(list[index]);
			}
			else if (typeid(*list[index]) == typeid(DVDdisc)) {
				cin >> *dynamic_cast<DVDdisc*>(list[index]);
			}
			break;
		default:
			return;
		}
	}
}

int ListDataStorage::Find() {
	system("cls");
	if (list.GetCount() == 0) {
		cout << "List is empty\n\n";
		return -1;
	}
	DataStorage* temp;
	switch (getChoice()) {
	case USB:
		temp = new USBFlash;
		break;
	case HDD:
		temp = new HDDPortable;
		break;
	case DVD:
		temp = new DVDdisc;
		break;
	default:
		return -1;
	}
	system("cls");
	cout << "Find by:\n"
			"1.Manufacturer\n"
			"2.Model\n"
			"3.Volume\n"
			"4.Count\n";
	if (typeid(*temp) == typeid(USBFlash)) {
		cout << "5.Speed USB\n";
		cout << "6.USB standart\n";
	}
	else if (typeid(*temp) == typeid(HDDPortable)) {
		cout << "5.Speed Rotation\n";
	}
	else if (typeid(*temp) == typeid(DVDdisc)) {
		cout << "5.Speed recording\n";
	}
	char ch;
	do {
		ch = _getch();
	} while ((ch < MANUF || ch > STD) && ch != NONE);
	double keyDouble;
	unsigned int keyInt;
	char keyString[DataStorage::MODEL_S];
	system("cls");
	cout << "Enter the key";
	switch (ch) {
	case MANUF:
		cout << "(manufacturer): ";
		cin.getline(keyString, temp->MANUF_S);
		temp->setManufacturer(keyString);
		break;
	case MODEL:
		cout << "(model): ";
		cin.getline(keyString, temp->MODEL_S);
		temp->setModel(keyString);
		break;
	case VOL:
		cout << "(volume): ";
		cin >> keyDouble;
		temp->setVolume(keyDouble);
		break;
	case COUNT:
		cout << "(count): ";
		cin >> keyInt;
		temp->setCount(keyInt);
		break;
	case SPEED:
		if (typeid(*temp) == typeid(USBFlash)) {
			cout << "(speed usb): ";
			cin >> keyInt;
			dynamic_cast<USBFlash*>(temp)->setSpeedUSB(keyInt);
		}
		else if (typeid(*temp) == typeid(HDDPortable)) {
			cout << "(speed rotation): ";
			cin >> keyInt;
			dynamic_cast<HDDPortable*>(temp)->setSpeedRotation(keyInt);
		}
		else if (typeid(*temp) == typeid(DVDdisc)) {
			cout << "(speed recording): ";
			cin >> keyInt;
			dynamic_cast<DVDdisc*>(temp)->setSpeedRec(keyInt);
		}
		break;
	case STD:
		if (typeid(*temp) == typeid(USBFlash)) {
			cout << "(USB standart): ";
			cin >> keyDouble;
			dynamic_cast<USBFlash*>(temp)->setStandart(keyDouble);
		}
		else {
			system("cls");
			delete temp;
			return -1;
		}
		break;
	default:
		system("cls");
		delete temp;
		return -1;
	}
	system("cls");
	for (int i = 0; i < list.GetCount(); ++i) {
		if (typeid(*list[i]) == typeid(*temp)) {
			if (*list[i] == *temp) {
				PrintHeader(typeid(*list[i]));
				list[i]->Print();
				cout << endl;
				delete temp;
				return i;
			}
		}
	}
	cout << "Not found\n\n";
	delete temp;
	return -1;
}

void ListDataStorage::Save(const char *path) {
	FILE* file = nullptr;
	errno_t error = fopen_s(&file, path, "wb");
	if (error != 0) {
		perror(path);
		cout << endl;
		system("pause >> void");
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	for (int i = 0; i < list.GetCount(); ++i) {
		list[i]->Save(file);
	}
	fclose(file);
}

void ListDataStorage::Load(const char *path) {
	FILE* file = nullptr;
	errno_t error = fopen_s(&file, path, "rb");
	if (error != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	int handle = _fileno(file);
	DataStorage* temp = nullptr;
	char name[DataStorage::NAME_S];
	for (int i = 0; ftell(file) < _filelength(handle); ++i) {
		fread_s(name, sizeof name, sizeof(char), DataStorage::NAME_S, file);
		if (strcmp(name, typeid(USBFlash).name()) == 0) {
			temp = new USBFlash;
		}
		else if (strcmp(name, typeid(HDDPortable).name()) == 0) {
			temp = new HDDPortable;
		}
		else if (strcmp(name, typeid(DVDdisc).name()) == 0) {
			temp = new DVDdisc;
		}
		if (temp) {
			temp->Load(file);
			list.AddTail(temp);
		}
	}
	fclose(file);
}

char ListDataStorage::getChoice() const {
	system("cls");
	cout << "Data storage:\n"
			"1.USB Flash\n"
			"2.Portable HDD\n"
			"3.DVD\n";
	char ch;
	do {
		ch = _getch();
	} while (ch != USB && ch != HDD && ch != DVD && ch != NONE);
	system("cls");
	return ch;
}

char ListDataStorage::getCriterion(int index) {
	cout << "\n\nEdit:\n"
			"1.Manufacturer\n"
			"2.Model\n"
			"3.Volume\n"
			"4.Count\n";
	if (typeid(*list[index]) == typeid(USBFlash)) {
		cout << "5.Speed USB\n";
		cout << "6.USB standart\n";
	}
	else if (typeid(*list[index]) == typeid(HDDPortable)) {
		cout << "5.Speed Rotation\n";
	}
	else if (typeid(*list[index]) == typeid(DVDdisc)) {
		cout << "5.Speed recording\n";
	}
	cout << "0.Edit all\n\n";
	char ch;
	do {
		ch = _getch();
	} while ((ch < ALL || ch > STD) && ch != NONE);
	return ch;
}

void ListDataStorage::PrintHeader(const type_info &elem) {
	system("cls");
	cout << setw(DataStorage::MANUF_S) << left << "Manufacturer";
	cout << setw(DataStorage::MODEL_S / 2) << left << "Model";
	cout << setw(DataStorage::VOL_S) << left << "Volume";
	cout << setw(DataStorage::COUNT_S) << left << "Count";
	if (elem == typeid(USBFlash)) {
		cout << setw(USBFlash::SPEED_S) << left << "SpeedUSB ";
		cout << setw(USBFlash::STD_S) << left << "Standart\n\n";
	}
	else if (elem == typeid(HDDPortable)) {
		cout << setw(HDDPortable::SPEED_S) << left << "Speed Rot.\n\n";
	}
	else if (elem == typeid(DVDdisc)) {
		cout << setw(DVDdisc::SPEED_S) << left << "Speed Rec.\n\n";
	}
}