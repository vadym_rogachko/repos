#include<iostream>
#include<iomanip>
#include"HDDPortable.h"

using std::cout;
using std::setw;
using std::left;

HDDPortable::HDDPortable() :
	speedRotation(0)
{
	setName(typeid(HDDPortable).name());
}

HDDPortable::HDDPortable(const char* manufacturer, const char* model, double volume, int count, unsigned int speedRotation) :
	DataStorage(manufacturer, model, typeid(HDDPortable).name(), volume, count)
{
	setSpeedRotation(speedRotation);
}

unsigned int HDDPortable::getSpeedRotation() const {
	return speedRotation;
}

void HDDPortable::setSpeedRotation(unsigned int speedRotation) {
	if (speedRotation >= 0) {
		this->speedRotation = speedRotation;
	}
	else {
		this->speedRotation = 0;
	}
}

void HDDPortable::Print() const {
	DataStorage::Print();
	cout << setw(SPEED_S) << left << speedRotation;
}

void HDDPortable::Save(FILE* file) const {
	DataStorage::Save(file);
	fwrite(&speedRotation, sizeof(unsigned int), 1, file);
}

void HDDPortable::Load(FILE* file) {
	DataStorage::Load(file);
	fread_s(&speedRotation, sizeof speedRotation, sizeof(unsigned int), 1, file);
}

bool HDDPortable::operator == (DataStorage &storage) {
	bool isFound = false;
	bool notFound = false;
	try {
		isFound = DataStorage::operator==(storage);
	}
	catch (int e) {
		notFound = true;
	}
	if (notFound) {
		if (dynamic_cast<HDDPortable&>(storage).speedRotation) {
			isFound = speedRotation == dynamic_cast<HDDPortable&>(storage).speedRotation;
		}
	}
	return isFound;
}

istream& operator >> (istream &is, HDDPortable &hdd) {
	unsigned int speedRotation;
	is >> (DataStorage &)hdd;
	cout << "\nRotation speed: ";
	is >> speedRotation;
	hdd.setSpeedRotation(speedRotation);
	is.get();
	return is;
}

ostream& operator << (ostream &os, const HDDPortable &hdd) {
	os << (const DataStorage &)hdd;
	os << setw(HDDPortable::SPEED_S) << left << hdd.speedRotation;
	return os;
}