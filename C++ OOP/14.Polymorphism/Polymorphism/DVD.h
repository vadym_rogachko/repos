#pragma once
#include<iostream>
#include"DataStorage.h"

using std::istream;
using std::ostream;

class DVDdisc :
	public DataStorage {

protected:

	unsigned int speedRec;

public:

	DVDdisc();
	DVDdisc(const char* manufacturer, const char* model, double volume, int count, unsigned int speedRec);

	unsigned int getSpeedRec() const;
	void setSpeedRec(unsigned int speedRec);

	virtual void Print() const override;
	virtual void Save(FILE* file) const override;
	virtual void Load(FILE* file) override;

	virtual bool operator == (DataStorage &storage) override;

	friend istream& operator >> (istream &is, DVDdisc &dvd);
	friend ostream& operator << (ostream &os, const DVDdisc &dvd);

	enum size {
		SPEED_S = 8
	};
};