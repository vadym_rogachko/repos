#include<iostream>
#include<iomanip>
#include"USBFlash.h"

using std::cout;
using std::setw;
using std::left;

USBFlash::USBFlash() :
	speedUSB(0),
	standart(0)
{
	setName(typeid(USBFlash).name());
}

USBFlash::USBFlash(const char* manufacturer, const char* model, double volume, int count, unsigned int speedUSB, double standart) :
	DataStorage(manufacturer, model, typeid(USBFlash).name(), volume, count)
{
	setSpeedUSB(speedUSB);
	setStandart(standart);
}

unsigned int USBFlash::getSpeedUSB() const {
	return speedUSB;
}

double USBFlash::getStandart() const {
	return standart;
}

void USBFlash::setSpeedUSB(unsigned int speedUSB) {
	if (speedUSB >= 0) {
		this->speedUSB = speedUSB;
	}
	else {
		this->speedUSB = 0;
	}
}

void USBFlash::setStandart(double standart) {
	if (standart > 0) {
		this->standart = standart;
	}
	else {
		standart = 0;
	}
}

void USBFlash::Print() const {
	DataStorage::Print();
	cout << setw(SPEED_S) << left << speedUSB;
	if (standart == (int)standart) {
		cout << " " << standart << ".0";
	}
	else {
		cout << setw(STD_S) << left << standart;
	}
}

void USBFlash::Save(FILE* file) const {
	DataStorage::Save(file);
	fwrite(&speedUSB, sizeof(unsigned int), 1, file);
	fwrite(&standart, sizeof(double), 1, file);
}

void USBFlash::Load(FILE* file) {
	DataStorage::Load(file);
	fread_s(&speedUSB, sizeof speedUSB, sizeof(unsigned int), 1, file);
	fread_s(&standart, sizeof standart, sizeof(double), 1, file);
}

bool USBFlash::operator == (DataStorage &storage) {
	bool isFound = false;
	bool notFound = false;
	try {
		isFound = DataStorage::operator==(storage);
	}
	catch (int e) {
		notFound = true;
	}
	if (notFound) {
		if (dynamic_cast<USBFlash&>(storage).speedUSB) {
			isFound = speedUSB == dynamic_cast<USBFlash&>(storage).speedUSB;
		}
		else if (dynamic_cast<USBFlash&>(storage).standart) {
			isFound = standart == dynamic_cast<USBFlash&>(storage).standart;
		}
	}
	return isFound;
}

istream& operator >> (istream &is, USBFlash &usb) {
	unsigned int speedUSB;
	double standart;
	is >> (DataStorage &)usb;
	cout << "\nUSB speed: ";
	is >> speedUSB;
	usb.setSpeedUSB(speedUSB);
	cout << "\nUSB standart: ";
	is >> standart;
	usb.setStandart(standart);
	is.get();
	return is;
}

ostream& operator << (ostream &os, const USBFlash &usb) {
	os << (const DataStorage &)usb;
	os << setw(USBFlash::SPEED_S) << left << usb.speedUSB;
	if (usb.standart == (int)usb.standart) {
		os << " " << usb.standart << ".0";
	}
	else {
		os << setw(USBFlash::STD_S) << left << usb.standart;
	}
	return os;
}