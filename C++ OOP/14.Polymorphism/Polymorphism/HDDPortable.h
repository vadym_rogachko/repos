#pragma once
#include<iostream>
#include"DataStorage.h"

using std::istream;
using std::ostream;

class HDDPortable :
	public DataStorage {

protected:

	unsigned int speedRotation;

public:

	HDDPortable();
	HDDPortable(const char* manufacturer, const char* model, double volume, int count, unsigned int speedRotation);

	unsigned int getSpeedRotation() const;
	void setSpeedRotation(unsigned int speedRotation);

	virtual void Print() const override;
	virtual void Save(FILE* file) const override;
	virtual void Load(FILE* file) override;

	virtual bool operator == (DataStorage &storage) override;

	friend istream& operator >> (istream &is, HDDPortable &dvd);
	friend ostream& operator << (ostream &os, const HDDPortable &dvd);

	enum size {
		SPEED_S = 8
	};
};