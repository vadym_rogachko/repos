#pragma once
#include"DList.h"
#include"DataStorage.h"

class ListDataStorage {

	List<DataStorage*> list;

	enum storage;
	enum criterion;

	char getChoice() const;
	char getCriterion(int index);
	void PrintHeader(const type_info &elem);

public:

	~ListDataStorage();

	void Add();
	void Delete();
	void Print();
	void Edit();
	int Find();
	void Save(const char *path);
	void Load(const char *path);

};