#pragma once

class ConsoleDataStorage {

	char choice;

public:

	ConsoleDataStorage();
	void Menu() const;
	char Choice();
	bool isNoExit() const;

	enum choice {
		ADD = '1', DEL, PRINT, EDIT, FIND,
		EXIT = 27,
		NO_CHOICE = 0
	};
};