#pragma once
#include<iostream>
#include"DataStorage.h"

using std::istream;
using std::ostream;

class USBFlash :
	public DataStorage {

protected:

	unsigned int speedUSB;
	double standart;

public:

	USBFlash();
	USBFlash(const char* manufacturer, const char* model, double volume, int count, unsigned int speedUSB, double standart);

	unsigned int getSpeedUSB() const;
	double getStandart() const;
	void setSpeedUSB(unsigned int speedUSB);
	void setStandart(double standart);

	virtual void Print() const override;
	virtual void Save(FILE* file) const override;
	virtual void Load(FILE* file) override;

	virtual bool operator == (DataStorage &storage) override;

	friend istream& operator >> (istream &is, USBFlash &dvd);
	friend ostream& operator << (ostream &os, const USBFlash &dvd);

	enum size {
		SPEED_S = 8,
		STD_S = 4
	};
};