#include<iostream>
#include<iomanip>
#include"DataStorage.h"

using std::cout;
using std::setw;
using std::left;

DataStorage::DataStorage() :
	volume(0),
	count(0)
{
	memset(manufacturer, 0, MANUF_S);
	memset(model, 0, MODEL_S);
	memset(name, 0, NAME_S);
}

DataStorage::DataStorage(const char* manufacturer, const char* model, const char* name, double volume, unsigned int count) :
	count(count)
{
	setManufacturer(manufacturer);
	setModel(model);
	setName(name);
	setVolume(volume);
}

DataStorage::~DataStorage() 
{}

const char* DataStorage::getManufacturer() const {
	return manufacturer;
}

const char* DataStorage::getModel() const {
	return model;
}

const char* DataStorage::getName() const {
	return name;
}

double DataStorage::getVolume() const {
	return volume;
}

unsigned int DataStorage::getCount() const {
	return count;
}

void DataStorage::setManufacturer(const char* manufacturer) {
	strcpy_s(this->manufacturer, manufacturer);
}

void DataStorage::setModel(const char* model) {
	strcpy_s(this->model, model);
}

void DataStorage::setName(const char* name) {
	strcpy_s(this->name, name);
}

void DataStorage::setVolume(double volume) {
	if (volume >= 0) {
		this->volume = volume;
	}
	else {
		this->volume = 0;
	}
}

void DataStorage::setCount(unsigned int count) {
	this->count = count;
}

void DataStorage::Print() const {
	cout << setw(MANUF_S) << left << manufacturer;
	cout << setw(MODEL_S / 2) << left << model;
	cout << setw(VOL_S) << left << volume;
	cout << setw(COUNT_S) << left << count;
}

void DataStorage::Save(FILE* file) const {
	fwrite(name, sizeof(char), NAME_S, file);
	fwrite(manufacturer, sizeof(char), MANUF_S, file);
	fwrite(model, sizeof(char), MODEL_S, file);
	fwrite(&volume, sizeof (double), 1, file);
	fwrite(&count, sizeof (unsigned int), 1, file);
}

void DataStorage::Load(FILE* file) {
	fread_s(manufacturer, sizeof manufacturer, sizeof(char), MANUF_S, file);
	fread_s(model, sizeof model, sizeof(char), MODEL_S, file);
	fread_s(&volume, sizeof volume, sizeof(double), 1, file);
	fread_s(&count, sizeof count, sizeof(unsigned int), 1, file);
}

bool DataStorage::operator == (DataStorage &storage) {
	if (storage.manufacturer[0]) {
		return !(strcmp(manufacturer, storage.manufacturer));
	}
	if (storage.model[0]) {
		return !(strcmp(model, storage.model));
	}
	if (storage.volume) {
		return volume == storage.volume;
	}
	if (storage.count) {
		return count == storage.count;
	}
	throw -1;
	return false;
}

istream& operator >> (istream &is, DataStorage &storage) {
	double volume;
	unsigned int count;
	cout << "Manufacturer: ";
	is.getline(storage.manufacturer, storage.MANUF_S);
	cout << "\nModel: ";
	is.getline(storage.model, storage.MODEL_S / 2);
	cout << "\nVolume: ";
	is >> volume;
	storage.setVolume(volume);
	cout << "\nCount: ";
	is >> count;
	storage.setCount(count);
	return is;
}

ostream& operator << (ostream &os, const DataStorage &storage) {
	storage.Print();
	return os;
}