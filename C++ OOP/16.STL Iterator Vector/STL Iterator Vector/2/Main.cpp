#include<iostream>
#include<vector>
#include<iterator>
#include<algorithm>
#include<time.h>

using namespace std;

/*2. ���� ��� ������� ��������� �����������: �[M] � B[N]. ���������� ������� ������ ������, � ������� ����� ������� �������� ������ ���� �������� */

int getRandNum() {
	return rand() % 100;
}

void main() {

	srand(time(NULL));

	try {
		int M = 12, N = 5;
		vector<int> A(M), B(N), C(M + N);
		ostream_iterator<int> out(cout, " ");

		generate(A.begin(), A.end(), getRandNum);
		generate(B.begin(), B.end(), getRandNum);

		cout << "A:\n";
		copy(A.begin(), A.end(), out);
		cout << endl << endl;
		cout << "B:\n";
		copy(B.begin(), B.end(), out);
		cout << endl << endl;
		
		copy(A.begin(), A.end(), C.begin());
		copy_backward(B.begin(), B.end(), C.end());

		cout << "C:\n";
		copy(C.begin(), C.end(), out);
		cout << endl << endl;
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what() << endl;
	}
	system("pause >> void");
}