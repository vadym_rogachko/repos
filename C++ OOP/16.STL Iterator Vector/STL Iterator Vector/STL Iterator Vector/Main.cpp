#include<iostream>
#include<vector>
#include<iterator>
#include<algorithm>
#include<time.h>

using namespace std;

/*1. ��� ������, �������� ����� �����. ���������� ������� �� ������� ��� ������������� ����� */

void main() {

	srand(time(NULL));

	try {
		vector<int> vec(20);
		ostream_iterator<int> out(cout, " ");

		generate(vec.begin(), vec.end(), []() {return rand() % 199 - 99; });

		copy(vec.begin(), vec.end(), out);
		cout << endl << endl;
		
		auto it = remove_if(vec.begin(), vec.end(), [](int x) {return x < 0; });
		vec.erase(it, vec.end());
		
		copy(vec.begin(), vec.end(), out);
		cout << endl << endl;
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what() << endl;
	}
	system("pause >> void");
}