#include<iostream>
#include<vector>
#include<iterator>
#include<algorithm>
#include<time.h>

using namespace std;

/*3. ���� ��� ������� ��������� �����������: �[M] � B[N]. ���������� ������� ������ ������, � ������� ����� ������� ����� �������� ������ ���� �������� */

int getRandNum() {
	return rand() % 31;
}

void main() {

	srand(time(NULL));

	try {
		int M = 10, N = 20;
		vector<int> A(M), B(N), C;
		ostream_iterator<int> out(cout, " ");

		generate(A.begin(), A.end(), getRandNum);
		generate(B.begin(), B.end(), getRandNum);

		cout << "A:\n";
		copy(A.begin(), A.end(), out);
		cout << endl << endl;
		cout << "B:\n";
		copy(B.begin(), B.end(), out);
		cout << endl << endl;

		for (int i : A) {
			if (find(B.begin(), B.end(), i) != B.end()) {
				if (C.size() == 0 || find(C.begin(), C.end(), i) == C.end()) {
					C.push_back(i);
				}
			}
		}
		
		cout << "C:\n";
		copy(C.begin(), C.end(), out);
		cout << endl << endl;
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what() << endl;
	}
	system("pause >> void");
}