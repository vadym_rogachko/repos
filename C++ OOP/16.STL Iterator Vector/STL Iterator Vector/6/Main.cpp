#include<iostream>
#include<vector>
#include<iterator>
#include<algorithm>
#include<time.h>

using namespace std;

/*6. ��� ������, �������� ������������� �����. ���������� ������� �� ������� ��� ������� ����� */

bool IsPrimeNum(unsigned int num) {
	if (num <= 1) {
		return false;
	}
	if (num == 2) {
		return true;
	}
	for (int i = 2; i <= num / 2; ++i) {
		if (num % i == 0) {
			return false;
		}
	}
	return true;
}

void main() {

	srand(time(NULL));

	try {
		vector<unsigned int> vec(20);
		ostream_iterator<unsigned int> out(cout, " ");

		generate(vec.begin(), vec.end(), []() {return rand() % 100; });

		copy(vec.begin(), vec.end(), out);
		cout << endl << endl;

		auto it = remove_if(vec.begin(), vec.end(), IsPrimeNum);
		vec.erase(it, vec.end());

		cout << "No prime numbers:\n";
		copy(vec.begin(), vec.end(), out);
		cout << endl << endl;
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what() << endl;
	}
	system("pause >> void");
}