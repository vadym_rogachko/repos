#include<iostream>
#include<vector>
#include<iterator>
#include<algorithm>
#include<time.h>

using namespace std;

/*7. ��� ������, �������� ����� �����. ���������� ������������ �������������, ������������� � ������� �������� � ������ ������� */

void main() {

	srand(time(NULL));

	try {
		vector<int> vec(20), pos, neg, zero;
		ostream_iterator<int> out(cout, " ");
		back_insert_iterator<vector<int>> insPos(pos);
		back_insert_iterator<vector<int>> insNeg(neg);
		back_insert_iterator<vector<int>> insZero(zero);

		generate(vec.begin(), vec.end(), []() {return rand() % 19 - 9; });

		copy(vec.begin(), vec.end(), out);
		cout << endl << endl;

		copy_if(vec.begin(), vec.end(), insPos, [](int x) {return x > 0; });
		copy_if(vec.begin(), vec.end(), insNeg, [](int x) {return x < 0; });
		copy_if(vec.begin(), vec.end(), insZero, [](int x) {return x == 0; });

		cout << "Positive:\n";
		copy(pos.begin(), pos.end(), out);
		cout << endl << endl;
		cout << "Negative:\n";
		copy(neg.begin(), neg.end(), out);
		cout << endl << endl;
		cout << "Zero:\n";
		copy(zero.begin(), zero.end(), out);
		cout << endl << endl;
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what() << endl;
	}
	system("pause >> void");
}