#include<iostream>
#include<iterator>
#include<algorithm>
#include<conio.h>
#include"Filter.h"

using namespace std;

enum Filter::action {
	ENTER = 13, ESC = 27,
	MOVE = 224,
	UP = 72, UP_ = 73,
	DOWN = 80, DOWN_ = 81,
	LEFT = 75, RIGHT = 77,
	LATIN = 'l', CYRIL = 'c', PUNCT = 'p', NUM = 'n'
};

Filter::Filter(ifstream& fin, unsigned int countLines, bool isPrintPageNum) :
	filter(), countLines(countLines), printPageNum(isPrintPageNum)
{
	setFile(fin);
}

Filter::FilterPage::FilterPage() {
	latin = cyrillic = punctuation = numbers = false;
}

void Filter::setFile(ifstream& fin) {
	string str;
	int page = 0;
	int counter = 0;
	file.clear();
	file.push_back(string());
	while (getline(fin, str)) {
		if (counter == countLines) {
			file.push_back(string());
			counter = 0;
			++page;
		}
		file[page] += str + '\n';
		++counter;
	}
	fin.clear();
	fin.seekg(0);
}

void Filter::Show() {
	int firstPage = 0;
	int lastPage = file.size() - 1;
	int page = 0;
	PrintPage(firstPage);
	while (true) {
		switch (getAction()) {
		case UP_:
		case UP:
			if (page == firstPage) {
				continue;
			}
			--page;
			break;
		case DOWN_:
		case DOWN:
			if (page == lastPage) {
				continue;
			}
			++page;
			break;
		case LEFT:
			if (page == firstPage) {
				continue;
			}
			page = firstPage;
			break;
		case RIGHT:
			if (page == lastPage) {
				continue;
			}
			page = lastPage;
			break;
		case LATIN:
			filter.latin -= 1;
			break;
		case CYRIL:
			filter.cyrillic -= 1;
			break;
		case PUNCT:
			filter.punctuation -= 1;
			break;
		case NUM:
			filter.numbers -= 1;
			break;
		case ENTER:
			filter.latin = filter.cyrillic = filter.punctuation = filter.numbers = false;
			break;
		case ESC:
			return;
		}
		PrintPage(page);
	}
}

void Filter::PrintPage(int page) const {
	system("cls");
	replace_copy_if(file.at(page).begin(), file.at(page).end(), ostream_iterator<char> (cout), filter, ' ');
	cout << endl;
	if (printPageNum) {
		cout << page + 1 << endl;
	}
}

bool Filter::FilterPage::isLatin(unsigned char symb) const {
	return latin && isalpha(symb) && symb <= 127;
}

bool Filter::FilterPage::isCyrillic(unsigned char symb) const {
	return cyrillic && isalpha(symb) && symb > 127;
}

bool Filter::FilterPage::isPunctuation(unsigned char symb) const {
	return punctuation && ispunct(symb);
}

bool Filter::FilterPage::isNumber(unsigned char symb) const {
	return numbers && isdigit(symb);
}

bool Filter::FilterPage::operator() (unsigned char symb) const {
	return isLatin(symb) || isCyrillic(symb) || isPunctuation(symb) || isNumber(symb);
}

int Filter::getAction() const {
	unsigned char action;
	while (_kbhit()) {
		_getch();
	}
	while (true) {
		action = _getch();
		if (action == MOVE) {
			action = _getch();
		}
		else {
			action = tolower(action);
		}
		if (action == UP || action == UP_ ||
			action == DOWN || action == DOWN_ ||
			action == LEFT || action == RIGHT ||
			action == LATIN || action == CYRIL ||
			action == PUNCT || action == NUM ||
			action == ENTER || action == ESC)
			break;
	}
	return action;
}

void Filter::setCountLines(unsigned int count) {
	countLines = count;
}

void Filter::setPrintPageNum(bool isPrint) {
	printPageNum = isPrint;
}