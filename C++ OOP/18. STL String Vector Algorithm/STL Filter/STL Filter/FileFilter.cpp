#include"FileFilter.h"

FileFilter::FileFilter(const string& path) :
	console(),
	FileReader(path),
	Filter(getFile())
{}

char FileFilter::Action() {
	console.ShowMenu();
	return console.Choice();
}

void FileFilter::OpenFile() {
	ReadFile(console.EnterPath());
	setFile(getFile());
}

void FileFilter::ShowFile() {
	Show();
}

void FileFilter::Setting() {
	switch (console.Setting()) {
	case ConsoleFilter::COUNT_LINES:
		setCountLines(console.SettingsCountLines());
		setFile(getFile());
		break;
	case ConsoleFilter::PAGE_NUM:
		setPrintPageNum(console.SettingsPrintPageNum());
		break;
	}
}

void FileFilter::Help() const {
	console.ShowHelp();
}