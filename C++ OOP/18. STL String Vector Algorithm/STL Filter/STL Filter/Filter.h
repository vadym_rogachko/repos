#pragma once
#include<fstream>
#include<string>
#include<vector>

using std::string;
using std::vector;
using std::ifstream;

class Filter {

protected:

	enum action;

	struct FilterPage {

		bool latin;
		bool cyrillic;
		bool punctuation;
		bool numbers;

		FilterPage();
		bool isLatin(unsigned char symb) const;
		bool isCyrillic(unsigned char symb) const;
		bool isPunctuation(unsigned char symb) const;
		bool isNumber(unsigned char symb) const;
		bool operator() (unsigned char symb) const;
	};
	
	FilterPage filter;
	vector<string> file;

	bool printPageNum;
	unsigned int countLines;

	int getAction() const;
	void PrintPage(int page) const;

public:

	Filter(ifstream& fin, unsigned int countLines = 31, bool isPrintPageNum = true);
	void setFile(ifstream& fin);
	void setCountLines(unsigned int count);
	void setPrintPageNum(bool isPrint);
	void Show();
};