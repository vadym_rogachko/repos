#pragma once
#include"Filter.h"
#include"FileReader.h"

class FileFilter :
	private FileReader, private Filter {

	class ConsoleFilter {

		enum keys;
		char choice;

		void setConsole();

	public:

		enum {
			COUNT_LINES = '1', PAGE_NUM = '2'
		};

		ConsoleFilter();
		void ShowMenu() const;
		void ShowHelp() const;
		char Setting() const;
		unsigned int SettingsCountLines() const;
		bool SettingsPrintPageNum() const;
		string EnterPath() const;
		char Choice();
		bool isNoExit() const;
	};

	ConsoleFilter console;

public:

	enum action {
		OPEN = '1', SHOW = '2', SETTINGS = '3', HELP = '4', EXIT = 27
	};

	FileFilter(const string& path);
	char Action();
	void OpenFile();
	void ShowFile();
	void Help() const;
	void Setting();
};