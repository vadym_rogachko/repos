#include<conio.h>
#include<iostream>
#include<string>
#include<windows.h>
#include"FileFilter.h"

using std::cout;
using std::cin;
using std::endl;
using std::getline;
using std::string;

enum FileFilter::ConsoleFilter::keys {
	UP = 24, DOWN = 25, RIGHT = 26, LEFT = 27
};

FileFilter::ConsoleFilter::ConsoleFilter() :
	choice(0)
{
	setConsole();
}

void FileFilter::ConsoleFilter::setConsole() {
	setlocale(LC_ALL, "rus");
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO info;
	info.bVisible = false;
	info.dwSize = 10;
	SetConsoleCursorInfo(handle, &info);
}

void FileFilter::ConsoleFilter::ShowMenu() const {
	system("cls");
	cout << "1. ������� ����\n"
			"2. �������� ����\n"
			"3. ���������\n"
			"4. Help\n";
}

string FileFilter::ConsoleFilter::EnterPath() const {
	system("cls");
	string path;
	cout << "������� ��� �����: ";
	getline(cin, path);
	return path;
}

void FileFilter::ConsoleFilter::ShowHelp() const {
	system("cls");
	cout << "L/l   - ������ �������� ���������� ��������\n"
			"C/c   - ������ �������� ���������\n"
			"P/p   - ������ �������� ����������\n"
			"N/n   - ������ ����\n"
			"Enter - �������� ��� �������\n";

	cout << endl << char(LEFT) << " - ������ �������\n";
	cout << char(RIGHT) << " - ��������� ��������\n";

	cout << endl << char(DOWN) << "/Page Down - ��������� ��������\n";
	cout << char(UP) << "/Page Up   - ���������� ��������\n";
	cout << "\nEsc - �����\n";
	system("pause >> void");
}

char FileFilter::ConsoleFilter::Setting() const {
	system("cls");
	char choice;
	cout << "1. ���������� ����� �� ��������\n"
			"2. ������ ������ ��������\n";
	do {
		choice = _getch();
	} while (choice != COUNT_LINES && choice != PAGE_NUM && choice != EXIT);
	return choice;
}

unsigned int FileFilter::ConsoleFilter::SettingsCountLines() const {
	system("cls");
	int countLines;
	cout << "������� ���������� ����� �� ��������: ";
	cin >> countLines;
	return countLines;
}

bool FileFilter::ConsoleFilter::SettingsPrintPageNum() const {
	system("cls");
	char choice;
	cout << "�������� ����� ��������? (Y/N)\n";
	do {
		choice = toupper(_getch());
	} while (choice != 'Y' && choice != 'N');
	return choice == 'Y';
}

char FileFilter::ConsoleFilter::Choice() {
	do {
		choice = _getch();
	} while (choice != OPEN && choice != SHOW && choice != SETTINGS && choice != HELP && choice != EXIT);
	return choice;
}

bool FileFilter::ConsoleFilter::isNoExit() const {
	return choice != EXIT;
}