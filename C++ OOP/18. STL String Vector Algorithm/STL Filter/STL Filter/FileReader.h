#pragma once
#include<fstream>
#include<string>

using std::ifstream;
using std::string;

class FileReader {

protected:

	ifstream fin;

	void OpenFile(const string& path);
	void CloseFile();

public:

	FileReader(const string& path);
	~FileReader();
	ifstream& getFile();
	void ReadFile(const string& path);

};