#include<iostream>
#include<fstream>
#include"FileFilter.h"

void main() {

	FileFilter filter("book.txt");

	while (true) {
		switch (filter.Action()) {
		case FileFilter::OPEN:
			filter.OpenFile();
			break;
		case FileFilter::SHOW:
			filter.ShowFile();
			break;
		case FileFilter::SETTINGS:
			filter.Setting();
			break;
		case FileFilter::HELP:
			filter.Help();
			break;
		case FileFilter::EXIT:
			return;
		}
	}
}