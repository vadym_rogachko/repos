#include<iostream>
#include"FileReader.h"

using std::exception;
using std::runtime_error;
using std::cerr;
using std::endl;

FileReader::FileReader(const string& path) {
	try {
		OpenFile(path);
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what() << endl;
		system("pause >> void");
	}
}

FileReader::~FileReader() {
	CloseFile();
}

ifstream& FileReader::getFile() {
	return fin;
}

void FileReader::OpenFile(const string& path) {
	if (path.size() == 0) return;
	fin.open(path);
	if (!fin.is_open()) {
		throw runtime_error("File not found\n");
	}
}

void FileReader::CloseFile() {
	fin.close();
}

void FileReader::ReadFile(const string& path) {
	CloseFile();
	try {
		OpenFile(path);
	}
	catch (exception &e) {
		system("cls");
		cerr << e.what() << endl;
		system("pause >> void");
	}
}