#include<iostream>
#include"Expression.h"
#include<algorithm>
#include<utility>
#include<math.h>

using namespace std;

enum Expression::operation {
	UNKNOWN, SIN, COS, TG, CTG, SQRT, LG, LN, ABS, EXP,
	UNARY_MINUS, PLUS, MINUS, MULTI, DIV, MOD, POW,
	BRACKET
};

enum Expression::priority {
	BRACKET_OPEN,
	PLUS_MINUS,
	MULTI_DIV_MOD,
	POWER,
	FUNCTION,
	UNARY,
	NO_INFO
};

enum Expression::what {
	FINISH, UNO, NUM, SIGN, ALPHA, OPEN, CLOSE, NONE
};

enum Expression::error {
	CLOSE_FUNC, CLOSE_NUM, CLOSE_OPEN,
	OPEN_CLOSE, OPEN_SIGN,
	NUM_OPEN, NUM_FUNC,
	SIGN_CLOSE,
	DOUBLE_SIGN, DOUBLE_DOT,
	BEGIN_SIGN, BEGIN_CLOSE,
	END_SIGN,
	COUNT_BRACKET,
	FRACT_ERROR, SYMB_ERROR, FUNC_ERROR,
	LOG_ERROR, SQRT_ERROR, ZERO_ERROR
};

Expression::Expression() {
	result = 0;
}

Expression::Expression(const string& expr) {
	setExpression(expr);
}

void Expression::setExpression(const string& expr) {
	try {
		setResult(expr);
		expression = expr;
	}
	catch (exception& e) {
		throw;
	}
}

double Expression::getResult() const {
	return result;
}

const string& Expression::getExpression() const {
	return expression;
}

void Expression::setResult(const string& expr) {
	FormatExpression(expr);
	if (str.size() > 4) {
		ParseExpression();
		str.clear();
		result = numbers.top();
		numbers.pop();
	}
	else {
		result = 0;
	}
}

void Expression::FormatExpression(const string& expr) {
	str = "(0+" + expr + ')';
	auto it = remove_if(str.begin(), str.end(), [](char x) {return x == ' ' || x == '\t' || x == '\n'; });
	str.erase(it, str.end());
}

void Expression::ParseExpression() {
	CheckBeginEnd();
	auto it = str.begin();
	what toDo;
	do {
		toDo = Parse(it);
		switch (toDo) {
		case FINISH:
			continue;
		case UNO:
			pushUnaryMinus();
			break;
		case NUM:
			pushNumber(it);
			break;
		case SIGN:
			if (isGreaterPriority(it)) {
				Calc();
				continue;
			}
			pushSign(it);
			break;
		case ALPHA:
			pushAlpha(it);
			break;
		case OPEN:
			pushBracket();
			break;
		case CLOSE:
			popBracket();
			break;
		default:
			if (*it == '.') {
				Error(error::FRACT_ERROR);
			}
			Error(error::SYMB_ERROR);
		}
		++it;
	} while (toDo);
	CheckStackSymb();
}

bool Expression::Calc() {
	CheckStackNum();
	double num1, num2;
	num2 = numbers.top();
	numbers.pop();
	num1 = numbers.top();
	numbers.pop();
	switch (symbols.top().first) {
	case UNARY_MINUS:
		numbers.push(num1);
		numbers.push(-num2);
		break;
	case PLUS:
		numbers.push(num1 + num2);
		break;
	case MINUS:
		numbers.push(num1 - num2);
		break;
	case MULTI:
		numbers.push(num1 * num2);
		break;
	case DIV:
		if (num2 == 0) {
			Error(error::ZERO_ERROR);
		}
		numbers.push(num1 / num2);
		break;
	case MOD:
		if (num2 == 0) {
			Error(error::ZERO_ERROR);
		}
		numbers.push(num1 - (trunc(num1 / num2) * num2));
		break;
	case POW:
		numbers.push(pow(num1, num2));
		break;
	case SIN:
		numbers.push(sin(num2));
		break;
	case COS:
		numbers.push(cos(num2));
		break;
	case TG:
		numbers.push(tan(num2));
		break;
	case CTG:
		numbers.push(1 / tan(num2));
		break;
	case SQRT:
		if (num2 < 0) {
			Error(error::SQRT_ERROR);
		}
		numbers.push(sqrt(num2));
		break;
	case LG:
		if (num2 <= 0) {
			Error(error::LOG_ERROR);
		}
		numbers.push(log10(num2));
		break;
	case LN:
		if (num2 <= 0) {
			Error(error::LOG_ERROR);
		}
		numbers.push(log(num2));
		break;
	case ABS:
		numbers.push(abs(num2));
		break;
	case EXP:
		numbers.push(exp(num2));
		break;
	default:
		numbers.push(num1);
		numbers.push(num2);
		return false;
	}
	symbols.pop();
	return true;
}

void Expression::pushUnaryMinus() {
	symbols.push(pr(operation::UNARY_MINUS, priority::UNARY));
}

void Expression::pushNumber(string::iterator& it) {
	numbers.push(ParseNum(it));
}

bool Expression::pushSign(string::iterator& it) {
	switch (ParseSign(it)) {
	case '+':
		symbols.push(pr(operation::PLUS, priority::PLUS_MINUS));
		break;
	case '-':
		symbols.push(pr(operation::MINUS, priority::PLUS_MINUS));
		break;
	case '*':
		symbols.push(pr(operation::MULTI, priority::MULTI_DIV_MOD));
		break;
	case '/':
		symbols.push(pr(operation::DIV, priority::MULTI_DIV_MOD));
		break;
	case '%':
		symbols.push(pr(operation::MOD, priority::MULTI_DIV_MOD));
		break;
	case '^':
		symbols.push(pr(operation::POW, priority::POWER));
		break;
	default:
		return false;
	}
	return true;
}

void Expression::pushAlpha(string::iterator& it) {
	numbers.push(0);
	symbols.push(ParseAlpha(it));
	pushBracket();
	numbers.push(0);
	symbols.push(pr(operation::PLUS, priority::PLUS_MINUS));
}

void Expression::pushBracket() {
	symbols.push(pr(operation::BRACKET, priority::BRACKET_OPEN));
}

void Expression::popBracket() {
	CheckBrackets();
	while ((symbols.top().first) != BRACKET) {
		CheckStackNum();
		Calc();
		CheckBrackets();
	}
	symbols.pop();
	if (symbols.size() > 0 && symbols.top().first < UNARY_MINUS) {
		Calc();
	}
}

bool Expression::isGreaterPriority(string::iterator& it) {
	CheckBrackets();
	if (*it == '^') return false;
	if ((*it == '*' || *it == '/' || *it == '%') && symbols.top().second < MULTI_DIV_MOD) return false;
	if ((*it == '+' || *it == '-') && symbols.top().second < PLUS_MINUS) return false;
	return true;
}

Expression::what Expression::Parse(string::iterator& it) {
	if (it == str.end()) {
		return what::FINISH;
	}
	if (*it == '-' && (*(it - 1) == '+' || *(it - 1) == '-' || *(it - 1) == '*' || *(it - 1) == '/' || *(it - 1) == '%' || *(it - 1) == '^' || *(it - 1) == '(')) {
		CheckUnaryMinus();
		return what::UNO;
	}
	if (*it >= '0' && *it <= '9') {
		CheckNext(it);
		return what::NUM;
	}
	if (*it == '+' || *it == '-' || *it == '*' || *it == '/' || *it == '%' || *it == '^') {
		CheckNext(it);
		return what::SIGN;
	}
	if (*it >= 'a' && *it <= 'z') {
		return what::ALPHA;
	}
	if (*it == '(') {
		CheckNext(it);
		return what::OPEN;
	}
	if (*it == ')') {
		CheckNext(it);
		return what::CLOSE;
	}
	return what::NONE;
}

double Expression::ParseNum(string::iterator& it) {
	size_t pos1, pos2;
	double num, numFract = 0;
	num = stod(str.substr(it - str.begin()), &pos1);
	it += pos1;
	if (*it == '.') {
		++it;
		try {
			numFract = stod(str.substr(it - str.begin()), &pos2);
		}
		catch (exception& e) {
			if (*it == '.') {
				Error(error::DOUBLE_DOT);
			}
			else {
				Error(error::FRACT_ERROR);
			}
		}
		while (pos2 > 0) {
			numFract /= 10;
			--pos2;
		}
		it += pos2;
	}
	else {
		--it;
	}
	return num + numFract;
}

char Expression::ParseSign(string::iterator& it) {
	CheckBrackets();
	return *it;
}

Expression::pr Expression::ParseAlpha(string::iterator& it) {
	operation oper = ParseFunc(it);
	CheckFunc(it);
	if (oper != UNKNOWN) {
		return pr(oper, priority::FUNCTION);
	}
	else {
		return pr(oper, priority::NO_INFO);
	}
}

Expression::operation Expression::ParseFunc(string::iterator& it) {
	operation oper = operation::UNKNOWN;
	if (FindSubstr(it, func[SIN])) {
		oper = operation::SIN;
	}
	else if (FindSubstr(it, func[COS])) {
		oper = operation::COS;
	}
	else if (FindSubstr(it, func[TG])) {
		oper = operation::TG;
	}
	else if (FindSubstr(it, func[CTG])) {
		oper = operation::CTG;
	}
	else if (FindSubstr(it, func[SQRT])) {
		oper = operation::SQRT;
	}
	else if (FindSubstr(it, func[LG])) {
		oper = operation::LG;
	}
	else if (FindSubstr(it, func[LN])) {
		oper = operation::LN;
	}
	else if (FindSubstr(it, func[ABS])) {
		oper = operation::ABS;
	}
	else if (FindSubstr(it, func[EXP])) {
		oper = operation::EXP;
	}
	it += strlen(func[oper]);
	return oper;
}

bool Expression::FindSubstr(string::iterator& it, const string& substr) {
	return str.compare(distance(str.begin(), it), substr.size(), substr) == 0;
}

void Expression::CheckNext(string::iterator& it) {
	if (it + 1 == str.end()) {
		return;
	}
	if (*it == ')' && (*(it + 1) >= 'a' && *(it + 1) <= 'z')) {
		Error(error::CLOSE_FUNC);
	}
	if (*it == ')' && (*(it + 1) >= '0' && *(it + 1) <= '9')) {
		Error(error::CLOSE_NUM);
	}
	if (*it == ')' && (*(it + 1) == '(')) {
		Error(error::CLOSE_OPEN);
	}
	if (*it >= '0' && *it <= '9' && *(it + 1) >= 'a' && *(it + 1) <= 'z') {
		Error(error::NUM_FUNC);
	}
	if (*it >= '0' && *it <= '9' && *(it + 1) == '(') {
		Error(error::NUM_OPEN);
	}
	if (*it == '(' && *(it + 1) == ')') {
		if (it + 1 == str.end() - 1) {
			Error(error::COUNT_BRACKET);
		}
		Error(error::OPEN_CLOSE);
	}
	if (*it == '(' && (*(it + 1) == '+' || *(it + 1) == '*' || *(it + 1) == '/' || *(it + 1) == '%' || *(it + 1) == '^')) {
		Error(error::OPEN_SIGN);
	}
	if ((*it == '+' || *it == '-' || *it == '*' || *it == '/' || *it == '%' || *it == '^') && *(it + 1) == ')') {
		if (it + 1 == str.end() - 1) {
			Error(error::END_SIGN);
		}
		Error(error::SIGN_CLOSE);
	}
}

void Expression::CheckBeginEnd() {
	if (str[3] == ')') {
		Error(error::BEGIN_CLOSE);
	}
	if (Parse(str.begin() + 3) == SIGN && str[3] != '-') {
		Error(error::BEGIN_SIGN);
	}
	Parse(str.end() - 2);
}

void Expression::CheckFunc(string::iterator& it) {
	if (*it != '(') {
		Error(error::FUNC_ERROR);
	}
	CheckNext(it);
}

void Expression::CheckBrackets() {
	if (symbols.size() == 0) {
		Error(error::COUNT_BRACKET);
	}
}

void Expression::CheckUnaryMinus() {
	if (symbols.size() != 0 && symbols.top().first == UNARY_MINUS) {
		Error(error::DOUBLE_SIGN);
	}
}

void Expression::CheckStackSymb() {
	if (symbols.size() != 0) {
		Error(error::COUNT_BRACKET);
	}
}

void Expression::CheckStackNum() {
	if (numbers.size() < 2) {
		Error(error::DOUBLE_SIGN);
	}
}

void Expression::Error(error msg) {
	throw runtime_error(errors[msg]);
}

Expression::operator double() {
	return result;
}

void Expression::ShowInfo() const {
	cout << operations;
}

const char* Expression::getInfo() const {
	return operations;
}

void Expression::Show() const {
	if (expression.size() == 0) {
		cout << endl;
		return;
	}
	cout << expression << " = " << result << endl;
}

void Expression::Calculate() {
	try {
		Expression temp;
		cin >> temp;
		system("cls");
		temp.Show();
	}
	catch (exception& e) {
		cerr << e.what();
		system("pause >> void");
		system("cls");
	}
}

void Expression::Calculate(ostream& os) {
	try {
		Expression temp;
		cin >> temp;
		system("cls");
		os << temp;
	}
	catch (exception& e) {
		os << e.what();
	}
}

void Expression::Calculate(const string& expr) {
	try {
		Expression temp(expr);
		system("cls");
		temp.Show();
	}
	catch (exception& e) {
		cerr << e.what();
		system("pause >> void");
		system("cls");
	}
}

void Expression::Calculate(const string& expr, ostream& os) {
	try {
		Expression temp(expr);
		system("cls");
		os << temp;
	}
	catch (exception& e) {
		os << e.what();
	}
}

void Expression::Calculate(istream& is, ostream& os) {
	try {
		Expression temp;
		is >> temp;
		system("cls");
		os << temp;
	}
	catch (exception& e) {
		os << e.what();
	}
}

istream& operator >> (istream& is, Expression& expr) {
	string temp;
	getline(is, temp);
	expr.setExpression(temp);
	return is;
}

ostream& operator << (ostream& os, const Expression& expr) {
	if (expr.expression.size() == 0) return os << endl;
	return os << expr.expression << " = " << expr.result << endl;
}