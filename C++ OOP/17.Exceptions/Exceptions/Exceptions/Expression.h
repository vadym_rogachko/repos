#pragma once
#include<string>
#include<stack>
#include<utility>
#include<vector>

using std::string;
using std::stack;
using std::pair;
using std::ostream;
using std::istream;

class Expression {

protected:

	enum operation;
	enum priority;
	enum what;
	enum error;

	typedef pair<operation, priority> pr;

	string str;
	stack<pr> symbols;
	stack<double> numbers;

	static char func[][8];
	static char errors[][128];
	static char operations[];

	double result;
	string expression;

	void setResult(const string& expr);
	void ParseExpression();
	void FormatExpression(const string& expr);

	virtual bool Calc();

	void pushUnaryMinus();
	void pushNumber(string::iterator& it);
	virtual bool pushSign(string::iterator& it);
	void pushAlpha(string::iterator& it);
	void pushBracket();
	void popBracket();
	virtual bool isGreaterPriority(string::iterator& it);

	virtual what Parse(string::iterator& it);
	double ParseNum(string::iterator& it);
	char ParseSign(string::iterator& it);
	virtual pr ParseAlpha(string::iterator& it);
	virtual operation ParseFunc(string::iterator& it);
	bool FindSubstr(string::iterator& it, const string& substr);

	virtual void CheckNext(string::iterator& it);
	void CheckBeginEnd();
	void CheckFunc(string::iterator& it);
	void CheckBrackets();
	void CheckUnaryMinus();
	void CheckStackSymb();
	void CheckStackNum();
	virtual void Error(error msg);

public:

	Expression();
	explicit Expression(const string &expr);
	virtual ~Expression() {}
	void setExpression(const string &expr);
	double getResult() const;
	const string& getExpression() const;

	operator double();

	virtual void ShowInfo() const;
	virtual const char* getInfo() const;

	void Show() const;
	static void Calculate();
	static void Calculate(ostream& os);
	static void Calculate(const string &expr);
	static void Calculate(const string& expr, ostream& os);
	static void Calculate(istream& is, ostream& os);

	friend istream& operator >> (istream& is, Expression& expr);
	friend ostream& operator << (ostream& os, const Expression& expr);
};