#include<iostream>
#include"Expression.h"
#include<fstream>

using namespace std;

void main() {

	setlocale(LC_ALL, "rus");
	
	ifstream fin("test.txt");
	ofstream fout("testRes.txt");
	while (!fin.eof()) {
		Expression::Calculate(fin, fout);
	}
	fin.close();
	Expression::Calculate("sqrt(16)", fout);
	fout.close();

	fin.open("testError.txt");
	fout.open("testErrorRes.txt");
	while (!fin.eof()) {
		Expression::Calculate(fin, fout);
	}
	fin.close();
	fout.close();

	cout << "Help\n";
	cout << Expression().getInfo();
	system("pause >> void");

	Expression::Calculate("sqrt(16)", cout);
	system("pause");
	system("cls");

	cout << "������� ���������\n";
	Expression::Calculate();
	system("pause");
	system("cls");

	cout << "������� ���������\n";
	Expression::Calculate(cin, cout);
	system("pause");
	system("cls");

	try {
		Expression expr("5 * sqrt(16)+sin(17)*3");
		expr.Show();
		cout << expr;
		cout << expr.getResult() << endl;
		system("pause");
		system("cls");

		cout << expr + 2 << endl;
		double num1 = expr + 2;
		double num2 = (expr + 2) * 10;
		cout << num1 << ' '  << num2 << endl;
		system("pause");
		system("cls");

		Expression a;
		cout << "������� ���������\n";
		a.Calculate();
		system("pause");
		system("cls");
		cout << "������� ���������\n";
		expr.Calculate();
		expr.Show();
		system("pause");
		system("cls");

		cout << Expression("sqrt(16)") + Expression("2^10 - 3") << endl;
		Expression expr2("sqrt(81)");
		cout << (expr < expr2 ? expr : expr2);
		system("pause");
		system("cls");

		Expression expr3("3 ++ 3");
		cout << "No message because of exception\n";
		system("pause");
	}
	catch (exception& e) {
		cerr << e.what();
		Expression().ShowInfo();
		system("pause >> void");
		system("cls");
	}
	system("pause");
}