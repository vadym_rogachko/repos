#include<iostream>
#include<iomanip>
#include"CPerson.h"
#include"enum.h"

using std::cout;
using std::setw;
using std::left;
using std::cin;

CPerson::CPerson() :
	name(nullptr),
	surname(nullptr),
	age(0)
{}

CPerson::CPerson(const char *name, const char *surname, int age) {
	setName(name);
	setSurname(surname);
	setAge(age);
}

CPerson::~CPerson() {
	delete[] name;
	delete[] surname;
}

CPerson::CPerson(const CPerson &person) :
	CPerson()
{
	*this = person;
}

CPerson::CPerson(CPerson &&person) :
	CPerson()
{
	*this = std::move(person);
}

CPerson& CPerson::operator = (const CPerson &person) {
	if (this == &person) {
		return *this;
	}
	setName(person.name);
	setSurname(person.surname);
	setAge(person.age);
	return *this;
}

CPerson& CPerson::operator = (CPerson &&person) {
	if (this == &person) {
		return *this;
	}
	this->~CPerson();
	name = person.name;
	surname = person.surname;
	age = person.age;
	person.name = person.surname = nullptr;
	person.age = 0;
	return *this;
}

istream& operator >> (istream& is, CPerson &person) {
	char name[CPerson::NAME_S];
	char surname[CPerson::SURNAME_S];
	int age;
	char *temp;
	cout << "Enter the surname\n";
	fgets(surname, CPerson::NAME_S, stdin);
	if (temp = strchr(surname, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(surname, CPerson::SURNAME_S);
	surname[0] = toupper(surname[0]);
	cout << "Enter the name\n";
	fgets(name, CPerson::NAME_S, stdin);
	if (temp = strchr(name, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(name, CPerson::NAME_S);
	name[0] = toupper(name[0]);
	cout << "Enter the age\n";
	is >> age;
	cin.get();
	person.CPerson::CPerson(name, surname, age);
	return is;
}

ostream& operator << (ostream& os, const CPerson &person) {
	os << setw(WIDTH) << left << person.surname;
	os << setw(WIDTH) << left << person.name;
	os << setw(WIDTH - 7) << left << person.age;
	return os;
}

const char* CPerson::getName() const {
	return name;
}

const char* CPerson::getSurname() const {
	return surname;
}

int CPerson::getAge() const {
	return age;
}

void CPerson::setName(const char *name) {
	delete[] this->name;
	int size = strlen(name) + 1;
	this->name = new char[size];
	strcpy_s(this->name, size, name);
}

void CPerson::setSurname(const char *surname) {
	delete[] this->surname;
	int size = strlen(surname) + 1;
	this->surname = new char[size];
	strcpy_s(this->surname, size, surname);
}

void CPerson::setAge(int age) {
	if (age <= 0 || age > 99) {
		this->age = 0;
		return;
	}
	this->age = age;
}