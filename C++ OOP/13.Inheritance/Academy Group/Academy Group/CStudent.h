#pragma once
#include<iostream>
#include"CPerson.h"

using std::istream;
using std::ostream;

class CStudent : 
	public CPerson {

protected:

	char phone[12];
	double average;

public:

	CStudent();
	CStudent(const char *name, const char *surname, int age, const char *phone, double average);

	friend istream& operator >> (istream& is, CStudent &student);
	friend ostream& operator << (ostream& os, const CStudent &student);
	
	const char* getPhone() const;
	double getAverage() const;
	void setPhone(const char *phone);
	void setAverage(double average);

	enum size {
		PHONE_S = 12,
	};
};