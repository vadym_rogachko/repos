#include<iostream>
#include<iomanip>
#include"CStudent.h"
#include"enum.h"

using std::cout;
using std::endl;
using std::setw;
using std::left;
using std::cin;

CStudent::CStudent() :
	average(0) 
{
	memset(phone, '\0', PHONE_S);
}

CStudent::CStudent(const char *name, const char *surname, int age, const char *phone, double average) :
	CPerson(name, surname, age)
{
	setPhone(phone);
	setAverage(average);
}

istream& operator >> (istream& is, CStudent &student) {
	char phone[CStudent::PHONE_S];
	double average;
	char *temp;
	is >> (CPerson &)student;
	cout << "Enter the phone number\n";
	fgets(phone, CPerson::NAME_S, stdin);
	if (temp = strchr(phone, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	cout << "Enter the average point\n";
	is >> average;
	student.setPhone(phone);
	student.setAverage(average);
	return is;
}

ostream& operator << (ostream& os, const CStudent &student) {
	os << (const CPerson &)student;
	os << setw(WIDTH) << left << student.phone;
	os << setw(WIDTH - 5) << left << student.average << endl;
	return os;
}

double CStudent::getAverage() const {
	return average;
}

const char* CStudent::getPhone() const {
	return phone;
}

void CStudent::setPhone(const char *phone) {
	strncpy_s(this->phone, PHONE_S, phone, PHONE_S - 1);
}

void CStudent::setAverage(double average) {
	if (average <= 1. || average > 12.) {
		this->average = 0;
		return;
	}
	this->average = average;
}