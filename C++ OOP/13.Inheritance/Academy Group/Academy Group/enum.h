#pragma once

enum {
	WIDTH = 16
};

enum keys {
	ESC = 27,
	ENTER = 13
};
