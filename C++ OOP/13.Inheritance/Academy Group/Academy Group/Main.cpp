#include<iostream>
#include<conio.h>
#include<windows.h>
#include"CAcademyGroup.h"
#include"CStudent.h"
#include"enum.h"

using std::cout;
using std::endl;
using std::cin;

CAcademyGroup foo(const CAcademyGroup &group) {   
	CAcademyGroup test(group);                    
	return test;                                  
}                                                                                                

void main() {

	setlocale(LC_ALL, "rus");

	CAcademyGroup group;
	group.Load();
	bool isExit = false;
	while (!isExit) {
		char choice;
		system("cls");
		cout << "Academy group\n"
				"1.Add\n"
				"2.Delete\n"
				"3.Edit\n"
				"4.Print\n"
				"5.Find\n"
				"6.Sort\n";
		do {
			choice = _getch();
		} while ((choice < CAcademyGroup::ADD || choice > CAcademyGroup::SORT) && choice != ESC);
		while (_kbhit()) {
			_getch();
		}
		switch (choice) {
		case CAcademyGroup::ADD:
			group.AddStudents();
			break;
		case CAcademyGroup::DEL:
			group.DeleteStudent();
			break;
		case CAcademyGroup::EDIT:
			group.EditStudent();
			break;
		case CAcademyGroup::PRINT:
			cout << group;
			break;
		case CAcademyGroup::FIND:
			group.FindStudent();
			break;
		case CAcademyGroup::SORT:
			group.Sort(0, group.getCount() - 1);
			if (group.getCount() != 0) {
				system("cls");
				cout << "Sorted\n";
				system("pause >> void");
			}
			break;
		case ESC:
			char choice;
			system("cls");
			cout << "Are you sure you want to exit?(Y/N)\n";
			do {
				choice = toupper(_getch());
			} while (choice != CAcademyGroup::YES && choice != CAcademyGroup::NO);
			isExit = choice == CAcademyGroup::YES;
			system("cls");
			break;
		}
	}

	CAcademyGroup testGroup1 = group;
	cout << "Test\n\n";
	system("pause");
	system("cls");
	cout << "������������� �������� << CAcademyGroup\n\n";
	system("pause");
	cout << testGroup1;

	CAcademyGroup testGroup2;
	testGroup2 = testGroup1;
	system("cls");
	cout << "������������� �������� = � ������������ CAcademyGroup\n\n";
	system("pause");
	cout << testGroup2;

	CStudent testStudent1 = group[3];
	system("cls");
	cout << "�������� [] CAcademyGroup ������ � ������������� �������� << CStudent\n\n";
	system("pause");
	system("cls");
	cout << testStudent1;
	system("pause");

	system("cls");
	cout << "�������� [] CAcademyGroup ������ � ������\n\n";
	system("pause");
	for (int i = 0; i < testGroup2.getCount() && i < group.getCount(); ++i) {
		testGroup2[i] = group[i];
	}
	cout << testGroup2;

	CAcademyGroup testGroup3 = foo(group);
	system("cls");
	cout << "����������� ����������� � ��������� CAcademyGroup\n\n";
	system("pause");
	cout << testGroup3;

	testGroup1 = foo(testGroup2);
	system("cls");
	cout << "������������� �������� = � ��������� CAdaemyGroup\n\n";
	system("pause");
	cout << testGroup1;

	group.Save();
	system("pause");
}