#include<iostream>
#include<conio.h>
#include<iomanip>
#include<windows.h>
#include"CAcademyGroup.h"
#include"enum.h"

using std::cout;
using std::cin;
using std::endl;
using std::setw;
using std::left;
using std::swap;

CAcademyGroup::CAcademyGroup() :
	pSt(nullptr),
	count(0) 
{}

CAcademyGroup::~CAcademyGroup() {
	if (pSt) {
		for (int i = 0; i < count; ++i) {
			delete pSt[i];
		}
		delete[] pSt;
		count = 0;
	}
}

CAcademyGroup::CAcademyGroup(const CAcademyGroup &group) :
	CAcademyGroup()
{
	*this = group;
}

CAcademyGroup::CAcademyGroup(CAcademyGroup &&group) : 
	CAcademyGroup()
{
	*this = std::move(group);
}

CAcademyGroup& CAcademyGroup::operator = (const CAcademyGroup &group) {
	if (this == &group) {
		return *this;
	}
	this->~CAcademyGroup();
	count = group.count;
	pSt = new CStudent*[count];
	for (int i = 0; i < count; ++i) {
		pSt[i] = new CStudent(group[i]);
	}
	return *this;
}

CAcademyGroup& CAcademyGroup::operator = (CAcademyGroup &&group) {
	if (this == &group) {
		return *this;
	}
	this->~CAcademyGroup();
	pSt = group.pSt;
	count = group.count;
	group.pSt = nullptr;
	group.count = 0;
	return *this;
}

CStudent& CAcademyGroup::operator [] (int index) {
	if (count == 0) {
		cout << "Error\n";
		exit(1);
	}
	if (index < 0) {
		return *pSt[0];
	}
	if (index >= count) {
		return *pSt[count - 1];
	}
	return *pSt[index];
}

const CStudent& CAcademyGroup::operator [] (int index) const {
	if (count == 0) {
		cout << "Error\n";
		exit(1);
	}
	if (index < 0) {
		return *pSt[0];
	}
	if (index >= count) {
		return *pSt[count - 1];
	}
	return *pSt[index];
}

ostream& operator << (ostream &os, const CAcademyGroup &group) {
	group.Print();
	return os;
}

int CAcademyGroup::getCount() const {
	return count;
}

void CAcademyGroup::AddStudents() {
	int addCount;
	system("cls");
	cout << "How many students you wish to add?\n";
	cin >> addCount;
	if (addCount < 0) {
		system("cls");
		cout << "Error\n";
		system("pause >> void");
		return;
	}
	count += addCount;
	CStudent **temp = new CStudent*[count];
	for (int i = 0; i < count - addCount; ++i) {
		temp[i] = new CStudent(*pSt[i]);
		delete pSt[i];
	}
	delete[] pSt;
	pSt = temp;
	temp = nullptr;
	for (int i = count - addCount; i < count; ++i) {
		system("cls");
		cin.get();
		pSt[i] = new CStudent;
		cin >> *pSt[i];
		cout << "\nStudent added\n";
		system("pause >> void");
	}
}

void CAcademyGroup::DeleteStudent() {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	char choice;
	char *temp;
	char surname[CPerson::SURNAME_S];
	cout << "Enter the surname\n";
	fgets(surname, CPerson::NAME_S, stdin);
	if (temp = strchr(surname, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(surname, CPerson::SURNAME_S);
	surname[0] = toupper(surname[0]);
	int num = 0;
	while (num < count) {
		if (!strcmp(pSt[num]->getSurname(), surname)) break;
		++num;
	}
	if (num == count) {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
		return;
	}
	system("cls");
	cout << pSt[num]->getSurname() << ' ' << pSt[num]->getName() << endl;
	cout << "Delete?(Y/N)";
	do {
		choice = toupper(_getch());
	} while (choice != YES && choice != NO && choice != ESC && choice != ENTER);
	if (choice == YES || choice == ENTER) {
		CStudent **temp = new CStudent*[count - 1];
		for (int i = 0; i < num; ++i) {
			temp[i] = new CStudent(*pSt[i]);
		}
		for (int i = num; i < count - 1; ++i) {
			temp[i] = new CStudent(*pSt[i + 1]);
		}
		for (int i = 0; i < count; ++i) {
			delete pSt[i];
		}
		delete[] pSt;
		pSt = temp;
		temp = nullptr;
		--count;
		system("cls");
		cout << "Student deleted\n";
		system("pause >> void");
	}
}

void CAcademyGroup::EditStudent() {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	char surname[CPerson::SURNAME_S];
	char *temp;
	cout << "Enter the surname\n";
	fgets(surname, CPerson::NAME_S, stdin);
	if (temp = strchr(surname, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(surname, CPerson::SURNAME_S);
	surname[0] = toupper(surname[0]);
	int num = 0;
	while (num < count) {
		if (!strcmp(pSt[num]->getSurname(), surname)) break;
		++num;
	}
	if (num == count) {
		system("cls");
		cout << "Not found\n";
		system("pause >> void");
		return;
	}
	system("cls");
	cout << "Edit student:\n\n" << *pSt[num];
	cin >> *pSt[num];
}

void CAcademyGroup::Print() const {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	cout << setw(WIDTH) << left << "Surname";
	cout << setw(WIDTH) << left << "Name";
	cout << setw(WIDTH - 7) << left << "Age";
	cout << setw(WIDTH) << left << "Phone";
	cout << setw(WIDTH - 5) << left << "Av.Point" << endl << endl;
	for (int i = 0; i < count; ++i) {
		cout << *pSt[i];
	}
	system("pause >> void");
}

void CAcademyGroup::FindStudent() const {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	char choice;
	char isMask;
	cout << "Find the student\n"
			"1.By name\n"
			"2.By surname\n"
			"3.By phone\n";
	do {
		choice = _getch();
	} while (choice != NAME && choice != SURNAME && choice != PHONE && choice != ESC);
	if (choice == ESC) return;
	cout << "\nFind by mask?(Y/N)\n";
	do {
		isMask = toupper(_getch());
	} while (isMask != YES && isMask != NO && isMask != ESC && isMask != ENTER);
	if (isMask == ESC) return;
	isMask = isMask == YES || isMask == ENTER;
	system("cls");
	char str[CPerson::NAME_S];
	char *temp;
	switch (choice) {
	case NAME:
		cout << "Enter name\n";
		break;
	case SURNAME:
		cout << "Enter surname\n";
		break;
	case PHONE:
		cout << "Enter phone\n";
		break;
	}
	fgets(str, CPerson::NAME_S, stdin);
	if (temp = strchr(str, '\n')) {
		temp[0] = '\0';
	}
	else {
		scanf_s("%*[^\n]");
		scanf_s("%*c");
	}
	_strlwr_s(str, CPerson::NAME_S);
	str[0] = toupper(str[0]);
	system("cls");
	cout << setw(WIDTH) << left << "Surname";
	cout << setw(WIDTH) << left << "Name";
	cout << setw(WIDTH - 7) << left << "Age";
	cout << setw(WIDTH) << left << "Phone";
	cout << setw(WIDTH - 5) << left << "Average" << endl << endl;
	switch (choice) {
	case NAME:
		for (int i = 0; i < count; ++i) {
			if (isMask ? strstr(pSt[i]->getName(), str) : !strcmp(str, pSt[i]->getName())) {
				cout << *pSt[i];
			}
		}
		break;
	case SURNAME:
		for (int i = 0; i < count; ++i) {
			if (isMask ? strstr(pSt[i]->getSurname(), str) : !strcmp(str, pSt[i]->getSurname())) {
				cout << *pSt[i];
			}
		}
		break;
	case PHONE:
		for (int i = 0; i < count; ++i) {
			if (isMask ? strstr(pSt[i]->getPhone(), str) : !strcmp(str, pSt[i]->getPhone())) {
				cout << *pSt[i];
			}
		}
		break;
	}
	system("pause >> void");
}

void CAcademyGroup::Sort(short left, short right) {
	system("cls");
	if (count == 0) {
		cout << "The group is empty\n";
		system("pause >> void");
		return;
	}
	short l = left, r = right;
	char mid[CPerson::SURNAME_S];
	strcpy_s(mid, CPerson::SURNAME_S, pSt[(l + r) / 2]->getSurname());
	while (l <= r) {
		while (strcmp(pSt[l]->getSurname(), mid) < 0) ++l;
		while (strcmp(pSt[r]->getSurname(), mid) > 0) --r;
		if (l <= r) {
			swap(*pSt[l++], *pSt[r--]);
		}
	}
	if (l < right) Sort(l, right);
	if (r > left) Sort(left, r);
}

void CAcademyGroup::Save() const {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "AcademyGroup.bin", "wb");
	if (error != 0) {
		perror("AcademyGroup.bin");
		cout << endl;
		if (file != nullptr) {
			fclose(file);
		}
		exit(1);
	}
	fwrite(&count, sizeof(int), 1, file);
	int size;
	int tempInt;
	double tempDouble;
	for (int i = 0; i < count; ++i) {
		size = strlen(pSt[i]->getName()) + 1;
		fwrite(&size, sizeof(int), 1, file);
		fwrite(pSt[i]->getName(), sizeof(char), size, file);
		size = strlen(pSt[i]->getSurname()) + 1;
		fwrite(&size, sizeof(int), 1, file);
		fwrite(pSt[i]->getSurname(), sizeof(char), size, file);
		tempInt = pSt[i]->getAge();
		fwrite(&tempInt, sizeof(int), 1, file);
		fwrite(pSt[i]->getPhone(), sizeof(char), CStudent::PHONE_S, file);
		tempDouble = pSt[i]->getAverage();
		fwrite(&tempDouble, sizeof(double), 1, file);
	}
	fclose(file);
}

void CAcademyGroup::Load() {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "AcademyGroup.bin", "rb");
	if (error != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	fread_s(&count, sizeof count, sizeof(int), 1, file);
	if (count == 0) {
		fclose(file);
		return;
	}
	char name[CPerson::NAME_S];
	char surname[CPerson::SURNAME_S];
	int age;
	char phone[CStudent::PHONE_S];
	double average;
	int size;
	pSt = new CStudent*[count];
	for (int i = 0; i < count; ++i) {
		fread_s(&size, sizeof size, sizeof(int), 1, file);
		fread_s(name, sizeof name, sizeof(char), size, file);
		fread_s(&size, sizeof size, sizeof(int), 1, file);
		fread_s(surname, sizeof surname, sizeof(char), size, file);
		fread_s(&age, sizeof age, sizeof(int), 1, file);
		fread_s(phone, sizeof phone, sizeof(char), CStudent::PHONE_S, file);
		fread_s(&average, sizeof average, sizeof(double), 1, file);
		pSt[i] = new CStudent(name, surname, age, phone, average);
	}
	fclose(file);
}