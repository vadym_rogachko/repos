#pragma once
#include<iostream>

using std::istream;
using std::ostream;

class CPerson {

protected:

	char *name;
	char *surname;
	int age;

public:

	CPerson();
	CPerson(const char *name, const char *surname, int age);
	CPerson(const CPerson &person);
	CPerson(CPerson &&person);
	~CPerson();

	CPerson& operator = (const CPerson &person);
	CPerson& operator = (CPerson &&person);

	friend istream& operator >> (istream& is, CPerson &person);
	friend ostream& operator << (ostream& os, const CPerson &person);

	const char* getName() const;
	const char* getSurname() const;
	int getAge() const;
	void setName(const char *name);
	void setSurname(const char *surname);
	void setAge(int age);

	enum size {
		NAME_S = 16,
		SURNAME_S = 16,
	};
};