#include<iostream>
#include<conio.h>
#include"Equations.h"
#include"Num.h"
#include"CFraction.h"

using std::cout;
using std::cin;
using std::endl;

/*2. �������� ������������� ������� ������� ��� ���������� ������ ��������� (a * x + b = 0) � ����������� (a * x^2 + b * x + c = 0) ���������. ��� ���� � ������� ���������� ������������ ���������*/

void main() {

	setlocale(LC_ALL, "rus");

	/*�������� ���������*/

	cout << "a * x + b = 0\n";

	cout << "\n\n���������� ����\n";

//	double
	Equation(3.0, 15.0);

//	int
	Equation(3, 15);

//	double � int
	Equation(3.0, 15);

//	double � long long
	Equation(3.0, 15LL);

	cout << "\n\n���������������� ��� Num\n";

//	���������������� ��� Num
	Equation(Num(3), Num(15));

//	���������������� ��� Num � double
	Equation(Num(3), 15.0);

//	���������������� ��� Num � int
	Equation(Num(3), 15);

//	���������������� ��� Num � long long
	Equation(3LL, Num(15));

	cout << "\n\nCFraction\n";

//	CFraction
	Equation(CFraction(3, 1), CFraction(15, 1));

//	CFraction � double
	Equation(CFraction(3, 1), 15.0);

//	CFraction � int
	Equation(CFraction(3, 1), 15);

//	CFraction � Num
	Equation(Num(3), CFraction(15, 1));

	cout << endl << endl;
	system("pause");
	system("cls");

	/*���������� ���������*/

	cout << "a * x^2 + b * x + c = 0\n\n";

	cout << "\n\n���������� ����\n";

//	double
	Equation(2.0, 5.0, -3.0);
	cout << endl;

//	int
	Equation(2, 5, -3);
	cout << endl;

//	double � int
	Equation(2.0, 5, -3.0);
	cout << endl;

//	double, int � long long
	Equation(2.0, 5, -3LL);
	cout << endl << endl;

	cout << "\n\n���������������� ��� Num\n";

//	���������������� ��� Num
	Equation(Num(2), Num(5), Num(-3));
	cout << endl;

//	���������������� ��� Num � double 
	Equation(Num(2), 5.0, Num(-3));
	cout << endl;

//	���������������� ��� Num � int
	Equation(Num(2), 5, -3);
	cout << endl;

//	���������������� ��� Num, double � long long
	Equation(Num(2), 5.0, -3LL);
	cout << endl << endl;

	cout << "\n\nCFraction\n";

//	CFraction
	Equation(CFraction(2, 1), CFraction(5, 1), CFraction(-3, 1));
	cout << endl;

//	CFraction � double
	Equation(CFraction(2, 1), 5.0, CFraction(-3, 1));
	cout << endl;

//	CFraction � int
	Equation(CFraction(2, 1), 5, -3);
	cout << endl;

//	CFraction, double � Num
	Equation(CFraction(2, 1), 5.0, Num(-3));
	cout << endl << endl;

	system("pause");
	system("cls");
}