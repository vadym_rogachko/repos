#pragma once
#include<math.h>

template <typename T1, typename T2>
void Equation(const T1 &a, const T2 &b) {
	cout << endl << a << "x + " << b << " = 0\n";
	if (a == 0 && b == 0) {
		cout << "��������� ����� ����������� ���������� ������\n";
		return;
	}
	if (a == 0) {
		cout << "��������� �� ����� ������\n";
		return;
	}
	cout << "������ ��������� x = " << -b / double(a) << endl;
}

template <typename T1, typename T2, typename T3>
void Equation(const T1 &a, const T2 &b, const T3 &c) {
	cout << endl << a << "x^2 + " << b << "x + " << c << " = 0\n";
	if (a == 0) {
		cout << "��������� �������� ��������\n";
		Equation(b, c);
		return;
	}
	if ((b * b) - (4 * a * c) < 0) {
		cout << "��������� �� ����� ������\n";
		return;
	}
	double D = sqrt(double((b * b) - (4 * a * c)));
	if (D == 0) {
		cout << "������ ��������� x = " << -b / double(2 * a) << endl;
		return;
	}
	auto root1 = (-b + D) / double(2 * a);
	auto root2 = (-b - D) / double(2 * a);
	cout << "������ ��������� x1 = " << root1 << endl;
	cout << "������ ��������� x2 = " << root2 << endl;
}