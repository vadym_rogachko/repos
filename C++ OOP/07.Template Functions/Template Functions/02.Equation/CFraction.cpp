#include<iostream>
#include"CFraction.h"

using std::cout;

CFraction::CFraction() :
	integer(0), numerator(0), denominator(1) {}

CFraction::CFraction(double d) :
	integer(d),
	numerator((d - integer + 0.05) * 10),
	denominator(10) {~*this;}

CFraction::CFraction(int n, int d) :
	integer(0), numerator(n),
	denominator(d > 0 ? d : 1) {}


CFraction::operator double() const {
	return RealNum();
}

// ���������� �����
CFraction& CFraction::operator ~ () {
	CutFraction();
	return *this;
}

CFraction CFraction::operator - () const {
	return CFraction(-numerator, denominator);
}

CFraction& CFraction::operator += (const CFraction &fract) {
	CFraction temp = fract;
	this->NoInteger();
	temp.NoInteger();
	if (denominator == temp.denominator) {
		numerator += temp.numerator;
		return ~*this;
	}
	short num = 2;
	while (denominator * num % temp.denominator != 0) {
		++num;
	}
	denominator *= num;
	numerator *= num;
	num = denominator / temp.denominator;
	numerator += temp.numerator * num;
	return ~*this;
}

CFraction operator + (CFraction fract1, const CFraction &fract2) {
	return fract1 += fract2;
}

CFraction operator - (const CFraction &fract1, const CFraction &fract2) {
	return fract1 + (-fract2);
}

CFraction operator * (CFraction fract1, CFraction fract2) {
	CFraction temp;
	fract1.NoInteger();
	fract2.NoInteger();
	temp.numerator = fract1.numerator * fract2.numerator;
	temp.denominator = fract1.denominator * fract2.denominator;
	return ~temp;
}

CFraction operator / (CFraction fract1, CFraction fract2) {
	CFraction temp;
	fract1.NoInteger();
	fract2.NoInteger();
	temp.numerator = fract1.numerator * fract2.denominator;
	temp.denominator = fract1.denominator * fract2.numerator;
	return ~temp;
}

bool operator == (const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() == fract2.RealNum();
}

bool operator != (const CFraction &fract1, const CFraction &fract2) {
	return !(fract1 == fract2);
}

bool operator > (const CFraction &fract1, const CFraction &fract2) {
	return fract1.RealNum() > fract2.RealNum();
}

bool operator < (const CFraction &fract1, const CFraction &fract2) {
	return fract2 > fract1;
}

bool operator >= (const CFraction &fract1, const CFraction &fract2) {
	return !(fract2 > fract1);
}

bool operator <= (const CFraction &fract1, const CFraction &fract2) {
	return !(fract1 > fract2);
}

ostream& operator << (ostream &os, const CFraction &fract) {
	fract.Print();
	return os;
}

void CFraction::ToInteger() {
	integer += numerator / denominator;
	numerator %= denominator;
	if (integer < 0) {
		numerator = abs(numerator);
	}
}

void CFraction::NoInteger() {
	numerator += integer * denominator;
	integer = 0;
}

double CFraction::RealNum() const {
	return integer + ((double)numerator / denominator);
}

void CFraction::Print() const {
	if (integer != 0) {
		cout << integer;
	}
	if (numerator == 0) {
		if (integer == 0) {
			cout << 0;
		}
		return;
	}
	if (integer != 0) {
		cout << ' ';
	}
	if (numerator == denominator || denominator == 1) {
		cout << numerator;
		return;
	}
	cout << numerator << '/' << denominator;
}

void CFraction::InitFract(int n, int d) {
	integer = 0;
	numerator = n;
	denominator = d > 0 ? d : 1;
}

void CFraction::InitFract(int i, int n, int d) {
	integer = i;
	numerator = integer < 0 ? abs(n) : n;
	denominator = d > 0 ? d : 1;
}

void CFraction::InitZero() {
	numerator = 0;
	denominator = 1;
	integer = 0;
}

void CFraction::CutFraction() {
	bool minus = numerator < 0 || integer < 0;
	if (minus) {
		numerator *= -1;
	}
	if (numerator % denominator == 0) {
		numerator = numerator / denominator;
		denominator = 1;
		if (minus) {
			if (integer == 0) {
				numerator *= -1;
			}
		}
		return;
	}
	if (denominator % numerator == 0) {
		denominator = denominator / numerator;
		numerator = 1;
		if (minus) {
			if (integer == 0) {
				numerator *= -1;
			}
		}
		return;
	}
	short num = 2;
	while (num < numerator && num < denominator) {
		if (numerator % num == 0 && denominator % num == 0) {
			numerator = numerator / num;
			denominator = denominator / num;
			num = 2;
			continue;
		}
		++num;
	}
	if (minus) {
		if (integer == 0) {
			numerator *= -1;
		}
	}
}