#include<iostream>
#include"Arithmetic Mean.h"
#include"Num.h"
#include"CFraction.h"

using std::cout;
using std::endl;

/*1. �������� ������ ������� ��� ������ �������� ��������������� �������� �������.*/

void main() {

	setlocale(LC_ALL, "rus");
	srand(time(NULL));

	const int SIZE = 10;
	int intArr[SIZE]{};
	short shortArr[SIZE]{};
	double doubleArr[SIZE]{};
	float floatArr[SIZE]{};
	Num NumArr[SIZE]{};
	CFraction FractArr[SIZE]{};

	InitArray(intArr, SIZE);
	InitArray(shortArr, SIZE);
	InitArray(doubleArr, SIZE);
	InitArray(floatArr, SIZE);
	InitArray(NumArr, SIZE);
	InitArray(FractArr, SIZE);
	
	ShowArray(intArr, SIZE, 3);
	cout << "������� �������������� int = " << GetArithMean(intArr, SIZE) << endl << endl;

	ShowArray(shortArr, SIZE, 3);
	cout << "������� �������������� short = " << GetArithMean(shortArr, SIZE) << endl << endl << endl << endl;

	ShowArray(doubleArr, SIZE);
	cout << "������� �������������� double = " << GetArithMean(doubleArr, SIZE) << endl << endl;

	ShowArray(floatArr, SIZE);
	cout << "������� �������������� float = " << GetArithMean(floatArr, SIZE) << endl << endl;

	ShowArray(NumArr, SIZE);
	cout << "������� �������������� Num = " << GetArithMean(NumArr, SIZE) << endl << endl << endl << endl;

	ShowArray(FractArr, SIZE, 8);
	cout << "\n������� �������������� CFraction = " << GetArithMean(FractArr, SIZE) << endl << endl;

	system("pause >> void");
}