#pragma once
#include<iomanip>

using std::setw;

template <typename T>
double GetArithMean(const T *arr, int size) {
	if (size <= 0) {
		cout << "Error\n";
		return 0;
	}
	T sum = T(0);
	for (int i = 0; i < size; ++i) {
		sum += arr[i];
	}
	return double(sum) / size;
}

template <typename T>
void InitArray(T *arr, int size, int min = 1, int max = 9) {
	if (size <= 0) {
		cout << "Error\n";
		return;
	}
	for (int i = 0; i < size; ++i) {
		arr[i] = T((rand() % (max - min + 1) + min) + (rand() % 10 / 10.));
	}
}

template <typename T>
void ShowArray(const T *arr, int size, int width = 5) {
	if (size <= 0) {
		cout << "Error\n";
		return;
	}
	cout << arr[0];
	for (int i = 1; i < size; ++i) {
		cout << setw(width) << arr[i];
	}
	cout << endl;
}