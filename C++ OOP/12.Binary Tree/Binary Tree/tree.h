#pragma once

struct Elem {
	char eng[15]{};
	char rus[15]{};
	Elem *left, *right, *parent;
};

class Tree {

	Elem *root;
	int count;

	void InsertMid(Elem **arr, int min, int max);

public:

	Tree();
	~Tree();
	void Print(Elem *Node);
	Elem* Search(Elem *Node, char *key);
	Elem* Min(Elem *Node);
	Elem* Max(Elem *Node);
	Elem* Next(Elem *Node);
	Elem* Previous(Elem *Node);
	void Insert(Elem *z);
	void Del(Elem *z);
	int GetCount() {
		return count;
	}
	Elem* GetRoot();

	void SaveStraight(Elem *Node, FILE *f);
	void SaveReverse(Elem *Node, FILE *f);
	void SaveUnbalance(Elem *Node, FILE *f);
	void LoadStraight(Elem *Node, FILE *f);
	void LoadReverse(Elem *Node, FILE *f);
	void LoadBalance(Elem *Node, FILE *f);
	void �alancing();
};

