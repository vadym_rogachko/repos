#include <windows.h>
#include <iostream>
#include <conio.h>
#include <io.h>
#include "tree.h"

using namespace std;

void PrintMenu() {
	char Menu[] = "1.Insert node\n2.Delete node\n3.Print node\n4.Search node\n5.Exit\n";
	system("cls");
	cout << Menu;
}

void Insert(Tree &obj) {
	system("cls");
	cout << "Enter a quantity of elements to add: ";
	int n;
	cin >> n;
	for (int i = 0; i < n; i++) {
		Elem *temp = new Elem;
		cout << "\nEnter an english word " << i + 1 << endl;
		cin >> temp->eng;
		cout << "\nEnter a russian word " << i + 1 << endl;
		cin >> temp->rus;
		obj.Insert(temp);
	}
}

void Print(Tree &obj) {
	system("cls");
	obj.Print(obj.GetRoot());
	_getch();
}

void Search(Tree &obj) {
	system("cls");
	cout << "Enter an english word for search:\n";
	char buf[15];
	cin >> buf;
	Elem *elem = obj.Search(obj.GetRoot(), buf);
	if (elem)
		cout << elem->rus;
	else
		cout << "\nWord not found\n";
	_getch();
}

void Delete(Tree &obj) {
	system("cls");
	cout << "Enter an english word for delete:\n";
	char buf[15];
	cin >> buf;
	Elem *p = obj.Search(obj.GetRoot(), buf);
	obj.Del(p);
}

void SaveBinStraight(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "VocabularyStraight.bin", "wb");
	if (error != 0) {
		perror("VocabularyStraight.bin");
		cout << endl;
		system("pause >> void");
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	obj.SaveStraight(obj.GetRoot(), file);
	fclose(file);
}

void SaveBinReverse(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "VocabularyReverse.bin", "wb");
	if (error != 0) {
		perror("VocabularyReverse.bin");
		cout << endl;
		system("pause >> void");
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	obj.SaveReverse(obj.GetRoot(), file);
	fclose(file);
}

void SaveText(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "VocabularyText.txt", "wt");
	if (error != 0) {
		perror("VocabularyText.txt");
		cout << endl;
		system("pause >> void");
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	obj.SaveUnbalance(obj.GetRoot(), file);
	fclose(file);
}

void LoadBinStraight(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "VocabularyStraight.bin", "rb");
	if (error != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	obj.LoadStraight(obj.GetRoot(), file);
	fclose(file);
}

void LoadBinReverse(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "VocabularyReverse.bin", "rb");
	if (error != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	fseek(file, 0, SEEK_END);
	obj.LoadReverse(obj.GetRoot(), file);
	fclose(file);
}

void LoadText(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "VocabularyText.txt", "rt");
	if (error != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	obj.LoadBalance(obj.GetRoot(), file);
	fclose(file);
}

void SaveTest(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "TestAfter.txt", "wt");
	if (error != 0) {
		perror("TestAfter.txt");
		cout << endl;
		system("pause >> void");
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	obj.SaveStraight(obj.GetRoot(), file);
	fclose(file);
}

void LoadTest(Tree &obj) {
	FILE *file = nullptr;
	errno_t error = fopen_s(&file, "TestBefore.txt", "rt");
	if (error != 0) {
		if (file != nullptr) {
			fclose(file);
		}
		return;
	}
	obj.LoadStraight(obj.GetRoot(), file);
	fclose(file);
}

void main() {
	Tree obj1;
	Tree obj2;
	Tree obj3;
	LoadBinStraight(obj1);
	LoadBinReverse(obj2);
	LoadText(obj3);
	while (true) {
		PrintMenu();
		char res = _getch();
		switch (res) {
		case '1':
			Insert(obj1);
			Insert(obj2);
			Insert(obj3);
			break;
		case '2':
			Delete(obj1);
			Delete(obj2);
			Delete(obj3);
			break;
		case '3':
			Print(obj1);
			Print(obj2);
			Print(obj3);
			break;
		case '4':
			Search(obj1);
			Search(obj2);
			Search(obj3);
			break;
		case '5':
			SaveBinStraight(obj1);
			SaveBinReverse(obj2);
			SaveText(obj3);
			// ���� ������������ � ���������� ��� ���������� - ����� TestBefore � TestAfter
			system("cls");
			cout << "Test - results in files TestBefore and TestAfter\n\n";
			system("pause");
			Tree test;
			LoadTest(test);
			Print(test);
			test.�alancing();
			Print(test);
			SaveTest(test);
			return;
		}
	}
}