#include <iostream>
#include<io.h>
using namespace std;
#include "tree.h"

Tree::Tree() {
	root = nullptr;
	count = 0;
}

Tree::~Tree() {
	while (root != nullptr)
		Del(root);
}

void Tree::Print(Elem *Node) {
	if (Node != nullptr) {
		Print(Node->left);
		cout << Node->eng << " \t\t" << Node->rus << endl;
		Print(Node->right);
	}
}

Elem* Tree::Search(Elem *Node, char *k) {
	while (Node != nullptr && strcmp(k, Node->eng) != 0) {
		if (strcmp(k, Node->eng) < 0)
			Node = Node->left;
		else
			Node = Node->right;
	}
	return Node;
}

Elem* Tree::Min(Elem *Node) {
	if (Node != nullptr) {
		while (Node->left != nullptr)
			Node = Node->left;
	}
	return Node;
}

Elem* Tree::Max(Elem *Node) {
	if (Node != nullptr) {
		while (Node->right != nullptr)
			Node = Node->right;
	}
	return Node;
}

Elem* Tree::Next(Elem *Node) {
	Elem *y = nullptr;
	if (Node != nullptr) {
		if (Node->right != nullptr) {
			return Min(Node->right);
		}
		y = Node->parent;
		while (y != 0 && Node == y->right) {
			Node = y;
			y = y->parent;
		}
	}
	return y;
}

Elem* Tree::Previous(Elem *Node) {
	Elem *y = nullptr;
	if (Node != nullptr) {
		if (Node->left != nullptr) {
			return Max(Node->left);
		}
		y = Node->parent;
		while (y != nullptr && Node == y->left) {
			Node = y;
			y = y->parent;
		}
	}
	return y;
}

Elem* Tree::GetRoot() {
	return root;
}

void Tree::Insert(Elem *z) {
	z->left = nullptr;
	z->right = nullptr;
	Elem *y = nullptr;
	Elem *Node = root;
	while (Node != nullptr) {
		y = Node;
		if (strcmp(z->eng, Node->eng) < 0)
			Node = Node->left;
		else
			Node = Node->right;
	}
	z->parent = y;

	if (y == nullptr)
		root = z;
	else if (strcmp(z->eng, y->eng) < 0)
		y->left = z;
	else y->right = z;

	++count;
}

void Tree::Del(Elem *z) {
	if (z != nullptr) {
		Elem *Node = nullptr;
		Elem *y = nullptr;

		if (z->left == nullptr || z->right == nullptr)
			y = z;
		else y = Next(z);

		if (y->left != nullptr) 
			Node = y->left;
		else Node = y->right;

		if (Node != nullptr)
			Node->parent = y->parent;

		if (y->parent == nullptr)
			root = Node;
		else if (y == y->parent->left)
			y->parent->left = Node;
		else 
			y->parent->right = Node;

		if (y != z) {
			strcpy_s(z->eng, y->eng);
			strcpy_s(z->rus, y->rus);
		}

		delete y;
		--count;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////
// ��� �������� �������       //
// ��� ������ � �������       //     
// �������� ��� ������������� //
// ������������ - ������      //
// �� ��������� ������        //
////////////////////////////////

 // ������ ������� - ������ � ������ ������� - ������ �� �����������
 // ������ ���������� �� �����, ������� ����� ���������, ����� ������
//  ������ - ������ ������� �����
void Tree::SaveStraight(Elem *Node, FILE *f) {
	if (Node != nullptr) {
		fwrite(Node->eng, sizeof Node->eng[0], sizeof Node->eng, f);
		fwrite(Node->rus, sizeof Node->rus[0], sizeof Node->rus, f);
		SaveStraight(Node->left, f);
		SaveStraight(Node->right, f);
	}
}


// ������ ������� - ������ � �������� ������� - ������ �� �����������
// ������ ���������� �� ����������� �����, ������� ����� ���������, ����� ������
// ������ - ��������� ������� �����
void Tree::SaveReverse(Elem *Node, FILE *f) {
	if (Node != nullptr) {
		SaveReverse(Node->left, f);
		SaveReverse(Node->right, f);
		fwrite(Node->eng, sizeof Node->eng[0], sizeof Node->eng, f);
		fwrite(Node->rus, sizeof Node->rus[0], sizeof Node->rus, f);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////
//        ������ �������         //
//  ���� ����� ��������� ������  //
//    � ��������������� ����.    //
//      ������ �����������       //
///////////////////////////////////

void Tree::SaveUnbalance(Elem *Node, FILE *f) {
	if (Node != nullptr) {
		SaveUnbalance(Node->left, f);
		fwrite(Node->eng, sizeof Node->eng[0], sizeof Node->eng, f);
		fwrite(Node->rus, sizeof Node->rus[0], sizeof Node->rus, f);
		char ch = '\n'; // ��� ������ ��������� � ��������� �����
		fwrite(&ch, sizeof (char), 1, f);
		SaveUnbalance(Node->right, f);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

// ��� ������� �������� - ������ �� ������ ����� (��������)
// ������� ����������� ��� ����� ���������, ����� ������
void Tree::LoadStraight(Elem *Node, FILE *f) {
	int handle = _fileno(f);
	if (ftell(f) < _filelength(handle)) {
		Node = new Elem;
		fread_s(Node->eng, sizeof Node->eng, sizeof Node->eng[0], sizeof Node->eng, f);
		fread_s(Node->rus, sizeof Node->rus, sizeof Node->rus[0], sizeof Node->rus, f);
		Insert(Node);
		LoadStraight(Node, f);
	}
}

// ������ ������� - ����� ��� ������ ������ �������� FILE* �� ��������� � ����� �����(SEEK_END) - ��������� � �����
// ������� ����������� ��� ������ ���������, ����� �����
void Tree::LoadReverse(Elem *Node, FILE *f) {
	if (ftell(f) > 0) {
		Node = new Elem;
		fseek(f, -long((sizeof Node->eng + sizeof Node->rus)), SEEK_CUR);
		fread_s(Node->eng, sizeof Node->eng, sizeof Node->eng[0], sizeof Node->eng, f);
		fread_s(Node->rus, sizeof Node->rus, sizeof Node->rus[0], sizeof Node->rus, f);
		Insert(Node);
		fseek(f, -long((sizeof Node->eng + sizeof Node->rus)), SEEK_CUR);
		LoadReverse(Node, f);
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

// ������ ������� - ����� �������� ����������� ������������,
// �.�. ������ �������� � ����������� ���� (���������������)
// � main ������� LoadText � SaveText ��������� ��������� �����
// ��� ����������� ������ ��������������� ������ ��������� ��
// ���������� �����
void Tree::LoadBalance(Elem *Node, FILE *f) {
	int handle = _fileno(f);
	if (ftell(f) < _filelength(handle)) {
		Node = new Elem;
		fread_s(Node->eng, sizeof Node->eng, sizeof Node->eng[0], sizeof Node->eng, f);
		fread_s(Node->rus, sizeof Node->rus, sizeof Node->rus[0], sizeof Node->rus, f);
		fseek(f, 2, SEEK_CUR);
		Insert(Node);
		LoadBalance(Node, f);
	}
	else {
		�alancing();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////

void Tree::�alancing() {
	int size = count;
	Elem **arr = new Elem*[size];
	Elem *temp = Min(root);
	for (int i = 0; i < size; ++i) {
		arr[i] = new Elem;
		*arr[i] = *temp;
		temp = Next(temp);
	}
	this->~Tree();
	InsertMid(arr, 0, size - 1);
}

void Tree::InsertMid(Elem **arr, int min, int max) {
	if (max < min) return;
	int mid = (min + max) / 2;
	Insert(arr[mid]);
	InsertMid(arr, min, mid - 1);
	InsertMid(arr, mid + 1, max);
}