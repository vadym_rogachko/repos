#pragma once
#include<iostream>

using std::ostream;
using std::istream;

class CFraction {

	int integer;
	int numerator;
	int denominator;

public:

	CFraction();
	explicit CFraction(double d);
	CFraction(int n, int d);

	operator double() const;

	CFraction& operator ~ ();

	CFraction operator - () const;

	CFraction& operator += (const CFraction &fract);
	CFraction& operator -= (const CFraction &fract);
	CFraction& operator *= (const CFraction &fract);
	CFraction& operator /= (const CFraction &fract);

	friend CFraction operator + (CFraction fract1, const CFraction &fract2);
	friend CFraction operator - (CFraction fract1, const CFraction &fract2);
	friend CFraction operator * (CFraction fract1, const CFraction &fract2);
	friend CFraction operator / (CFraction fract1, const CFraction &fract2);

	CFraction& operator++ ();
	CFraction& operator++ (int);
	CFraction& operator-- ();
	CFraction& operator-- (int);

	friend bool operator == (const CFraction &fract1, const CFraction &fract2);
	friend bool operator != (const CFraction &fract1, const CFraction &fract2);
	friend bool operator > (const CFraction &fract1, const CFraction &fract2);
	friend bool operator < (const CFraction &fract1, const CFraction &fract2);
	friend bool operator >= (const CFraction &fract1, const CFraction &fract2);
	friend bool operator <= (const CFraction &fract1, const CFraction &fract2);

	friend ostream& operator << (ostream &os, const CFraction &fract);
	friend istream& operator >> (istream &is, CFraction &fract);

	void ToInteger();
	void NoInteger();
	double RealNum() const;
	void Print() const;
	void CutFraction();
	void InitFract(int n, int d);
	void InitFract(int i, int n, int d);
	void InitZero();
};