#pragma once

using std::endl;
using std::cout;
using std::ostream;
using std::istream;

template <class T>
class Matrix
{

	T **p;
	int row, col;

public:

	Matrix();
	Matrix(int row, int col);
	Matrix(const Matrix<T> &m);
	Matrix(Matrix<T> &&m);
	~Matrix();

	Matrix<T>& operator = (const Matrix<T> &m);
	Matrix<T>& operator = (Matrix<T> &&m);

	Matrix<T>& operator ++ ();
	Matrix<T> operator ++ (int);
	Matrix<T>& operator -- ();
	Matrix<T> operator -- (int);
	Matrix<T> operator + (const Matrix<T> &m) const;
	Matrix<T> operator * (const Matrix<T> &m) const;

	T& operator () (int row, int col);
	T* operator [] (int index);

	template <typename T>
	friend istream& operator >> (istream &is, Matrix<T> &m);
	template <typename T>
	friend ostream& operator << (ostream &os, const Matrix<T> &m);
};

template <typename T>
Matrix<T>::Matrix() : row(0), col(0), p(nullptr) {}

template <typename T>
Matrix<T>::Matrix(int row, int col) {
	if (row <= 0 || col <= 0) {
		this->Matrix<T>::Matrix();
		return;
	}
	this->row = row;
	this->col = col;
	p = new T*[row];
	for (int i = 0; i < row; ++i) {
		p[i] = new T[col];
	}
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			p[i][j] = T(0);
		}
	}
}

template <typename T>
Matrix<T>::Matrix(const Matrix<T> &m) {
	if (m.row <= 0 || m.col <= 0) {
		this->Matrix<T>::Matrix();
		return;
	}
	this->Matrix<T>::Matrix(m.row, m.col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			p[i][j] = m.p[i][j];
		}
	}
}

template <typename T>
Matrix<T>::Matrix(Matrix<T> &&m) : row(m.row), col(m.col), p(m.p) {
	m.p = nullptr;
	m.row = m.col = 0;
}

template <typename T>
Matrix<T>::~Matrix() {
	for (int i = 0; i < row; ++i) {
		delete[] p[i];
	}
	delete[] p;
	row = col = 0;
}

template <typename T>
Matrix<T>& Matrix<T>::operator = (const Matrix<T> &m) {
	if (this == &m) {
		return *this;
	}
	this->~Matrix();
	if (m.p == nullptr) {
		this->Matrix<T>::Matrix();
		return *this;
	}
	this->Matrix<T>::Matrix(m.row, m.col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			p[i][j] = m.p[i][j];
		}
	}
	return *this;
}

template <typename T>
Matrix<T>& Matrix<T>::operator = (Matrix<T> &&m) {
	if (this == &m) {
		return *this;
	}
	this->~Matrix();
	row = m.row;
	col = m.col;
	p = m.p;
	m.p = nullptr;
	m.row = m.col = 0;
	return *this;
}

template <typename T>
Matrix<T>& Matrix<T>::operator ++ () {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			++p[i][j];
		}
	}
	return *this;
}

template <typename T>
Matrix<T> Matrix<T>::operator ++ (int) {
	Matrix<T> temp = *this;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			++p[i][j];
		}
	}
	return temp;
}

template <typename T>
Matrix<T>& Matrix<T>::operator -- () {
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			--p[i][j];
		}
	}
	return *this;
}

template <typename T>
Matrix<T> Matrix<T>::operator -- (int) {
	Matrix<T> temp = *this;
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			--p[i][j];
		}
	}
	return temp;
}

template <typename T>
Matrix<T> Matrix<T>::operator + (const Matrix<T> &m) const {
	if (row != m.row || col != m.col) {
		return Matrix<T>(0, 0);
	}
	Matrix<T> temp(row, col);
	for (int i = 0; i < row; ++i) {
		for (int j = 0; j < col; ++j) {
			temp.p[i][j] = p[i][j] + m.p[i][j];
		}
	}
	return temp;
}

template <typename T>
Matrix<T> Matrix<T>::operator * (const Matrix<T> &m) const {
	if (p == nullptr || m.p == nullptr) {
		return Matrix<T>(0, 0);
	}
	if (col != m.row) {
		return Matrix<T>(0, 0);
	}
	Matrix<T> temp(row, m.col);
	for (int i = 0; i < temp.row; ++i) {
		for (int j = 0; j < temp.col; ++j) {
			for (int k = 0; k < col; ++k) {
				temp.p[i][j] += p[i][k] * m.p[k][j];
			}
		}
	}
	return temp;
}

template <typename T>
T& Matrix<T>::operator () (int row, int col) {
	if (this->row == 0 || this->col == 0) {
		cout << "Error\nMatrix is empty\n\n";
		exit(1);
	}
	if (row < 0 || col < 0) {
		return p[0][0];
	}
	if (row >= this->row || col >= this->col) {
		return p[this->row - 1][this->col - 1];
	}
	return p[row][col];
}

template <typename T>
T* Matrix<T>::operator [] (int index) {
	if (this->row == 0 || this->col == 0) {
		cout << "Error\nMatrix is empty\n\n";
		exit(1);
	}
	if (index < 0) {
		return p[0];
	}
	if (index >= this->row) {
		return p[this->row - 1];
	}
	return p[index];
}

template <typename T>
istream& operator >> (istream &is, Matrix<T> &m) {
	if (m.row == 0 || m.col == 0) {
		cout << "Matrix is empty\n\n";
		return is;
	}
	for (int i = 0; i < m.row; ++i) {
		for (int j = 0; j < m.col; ++j) {
			is >> m.p[i][j];
		}
	}
	return is;
}

template <typename T>
ostream& operator << (ostream &os, const Matrix<T> &m) {
	if (m.row == 0 || m.col == 0) {
		cout << "Matrix is empty\n\n";
		return os;
	}
	for (int i = 0; i < m.row; ++i) {
		for (int j = 0; j < m.col; ++j) {
			os << m.p[i][j] << ' ';
		}
		os << endl;
	}
	os << endl;
	return os;
}
