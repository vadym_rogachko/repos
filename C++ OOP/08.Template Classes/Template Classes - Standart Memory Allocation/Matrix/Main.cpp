#include<iostream>
#include<conio.h>
#include"CMatrixT.h"
#include"Num.h"
#include"CFraction.h"

using std::cin;

void main() {

	setlocale(LC_ALL, "rus");

	char ch;
	cout << "1 - int\n"
			"2 - double\n"
			"3 - Num\n"
			"4 - CFraction\n"
			"q - exit\n";
	do {
		ch = _getch();
	} while (ch != 'q' && ch != '1' && ch != '2' && ch != '3' && ch != '4');
	system("cls");
	
	////////////////////////////////////////////////////////////
	// int
	if (ch == '1') {
		cout << "���������� ��������� <<\n";
		cout << "\n����������� �� ���������\n";
		Matrix<int> matr1, matr2(4, 2);
		cout << matr1;
		cout << "����������� � ����� �����������\n";
		cout << matr2;
		cout << "���������� ��������� >>\n\n";
		cin >> matr2;
		cout << endl << matr2;

		cout << "����������� ����������� � �������� ������������\n(������������� ������������)\n\n";
		cout << matr2;
		Matrix<int> matr3 = matr1 = matr2;
		cout << matr3 << matr1 << endl;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << ++matr1;
		cout << matr1;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << --matr1;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1++;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1--;
		cout << matr1;

		cout << "\n���������� ��������� +(�������� ������) � ����������� ��������\n\n";
		cout << matr1 << "+\n\n" << matr2;
		Matrix<int> matr4 = matr1 + matr2;
		cout << "=\n\n" << matr4;

		cout << "\n���������� ��������� *(��������� ������) � �������� ������������ � ���������\n";
		Matrix<int> matr5(2, 3);
		cout << "������� �����\n";
		cin >> matr5;
		cout << endl << matr4 << "*\n\n" << matr5;
		matr1 = matr4 * matr5;
		cout << "=\n\n" << matr1;

		cout << "\n��������� ������ � ������ �������� �������� ������ ������� �     ����� ������ �������\n";
		Matrix<int> matr = matr5 * matr4;
		cout << matr;

		cout << "\n�������� ������ ������� �������\n";
		matr = matr1 + matr4;
		cout << matr;

		cout << "\n���������� ��������� ()\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5(i, j);
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5(i, j) << ' ';
			}
			cout << endl;
		}
		cout << endl;

		cout << "\n���������� ��������� []\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5[i][j];
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5[i][j] << ' ';
			}
			cout << endl;
		}
		cout << endl;
	}
	////////////////////////////////////////////////////////////
	// double
	else if (ch == '2') {
		cout << "���������� ��������� <<\n";
		cout << "\n����������� �� ���������\n";
		Matrix<double> matr1, matr2(4, 2);
		cout << matr1;
		cout << "����������� � ����� �����������\n";
		cout << matr2;
		cout << "���������� ��������� >>\n\n";
		cin >> matr2;
		cout << endl << matr2;

		cout << "����������� ����������� � �������� ������������\n(������������� ������������)\n\n";
		cout << matr2;
		Matrix<double> matr3 = matr1 = matr2;
		cout << matr3 << matr1 << endl;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << ++matr1;
		cout << matr1;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << --matr1;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1++;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1--;
		cout << matr1;

		cout << "\n���������� ��������� +(�������� ������) � ����������� ��������\n\n";
		cout << matr1 << "+\n\n" << matr2;
		Matrix<double> matr4 = matr1 + matr2;
		cout << "=\n\n" << matr4;

		cout << "\n���������� ��������� *(��������� ������) � �������� ������������ � ���������\n";
		Matrix<double> matr5(2, 3);
		cout << "������� �����\n";
		cin >> matr5;
		cout << endl << matr4 << "*\n\n" << matr5;
		matr1 = matr4 * matr5;
		cout << "=\n\n" << matr1;

		cout << "\n��������� ������ � ������ �������� �������� ������ ������� �     ����� ������ �������\n";
		Matrix<double> matr = matr5 * matr4;
		cout << matr;

		cout << "\n�������� ������ ������� �������\n";
		matr = matr1 + matr4;
		cout << matr;

		cout << "\n���������� ��������� ()\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5(i, j);
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5(i, j) << ' ';
			}
			cout << endl;
		}
		cout << endl;

		cout << "\n���������� ��������� []\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5[i][j];
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5[i][j] << ' ';
			}
			cout << endl;
		}
		cout << endl;
	}
	////////////////////////////////////////////////////////////
	// Num
	else if (ch == '3') {
		cout << "���������� ��������� <<\n";
		cout << "\n����������� �� ���������\n";
		Matrix<Num> matr1, matr2(4, 2);
		cout << matr1;
		cout << "����������� � ����� �����������\n";
		cout << matr2;
		cout << "���������� ��������� >>\n\n";
		cin >> matr2;
		cout << endl << matr2;

		cout << "����������� ����������� � �������� ������������\n(������������� ������������)\n\n";
		cout << matr2;
		Matrix<Num> matr3 = matr1 = matr2;
		cout << matr3 << matr1 << endl;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << ++matr1;
		cout << matr1;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << --matr1;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1++;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1--;
		cout << matr1;

		cout << "\n���������� ��������� +(�������� ������) � ����������� ��������\n\n";
		cout << matr1 << "+\n\n" << matr2;
		Matrix<Num> matr4 = matr1 + matr2;
		cout << "=\n\n" << matr4;

		cout << "\n���������� ��������� *(��������� ������) � �������� ������������ � ���������\n";
		Matrix<Num> matr5(2, 3);
		cout << "������� �����\n";
		cin >> matr5;
		cout << endl << matr4 << "*\n\n" << matr5;
		matr1 = matr4 * matr5;
		cout << "=\n\n" << matr1;

		cout << "\n��������� ������ � ������ �������� �������� ������ ������� �     ����� ������ �������\n";
		Matrix<Num> matr = matr5 * matr4;
		cout << matr;

		cout << "\n�������� ������ ������� �������\n";
		matr = matr1 + matr4;
		cout << matr;

		cout << "\n���������� ��������� ()\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5(i, j);
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5(i, j) << ' ';
			}
			cout << endl;
		}
		cout << endl;

		cout << "\n���������� ��������� []\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5[i][j];
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5[i][j] << ' ';
			}
			cout << endl;
		}
		cout << endl;
	}
	////////////////////////////////////////////////////////////
	// CFraction
	else if (ch == '4') {
		cout << "���������� ��������� <<\n";
		cout << "\n����������� �� ���������\n";
		Matrix<CFraction> matr1, matr2(4, 2);
		cout << matr1;
		cout << "����������� � ����� �����������\n";
		cout << matr2;
		cout << "���������� ��������� >>\n\n";
		cin >> matr2;
		cout << endl << matr2;

		cout << "����������� ����������� � �������� ������������\n(������������� ������������)\n\n";
		cout << matr2;
		Matrix<CFraction> matr3 = matr1 = matr2;
		cout << matr3 << matr1 << endl;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << ++matr1;
		cout << matr1;

		cout << "\n���������� ����������� ����������\n\n";
		cout << matr1;
		cout << --matr1;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1++;
		cout << matr1;

		cout << "\n���������� ������������ ����������\n\n";
		cout << matr1;
		cout << matr1--;
		cout << matr1;

		cout << "\n���������� ��������� +(�������� ������) � ����������� ��������\n\n";
		cout << matr1 << "+\n\n" << matr2;
		Matrix<CFraction> matr4 = matr1 + matr2;
		cout << "=\n\n" << matr4;

		cout << "\n���������� ��������� *(��������� ������) � �������� ������������ � ���������\n";
		Matrix<CFraction> matr5(2, 3);
		cout << "������� �����\n";
		cin >> matr5;
		cout << endl << matr4 << "*\n\n" << matr5;
		matr1 = matr4 * matr5;
		cout << "=\n\n" << matr1;

		cout << "\n��������� ������ � ������ �������� �������� ������ ������� �     ����� ������ �������\n";
		Matrix<CFraction> matr = matr5 * matr4;
		cout << matr;

		cout << "\n�������� ������ ������� �������\n";
		matr = matr1 + matr4;
		cout << matr;

		cout << "\n���������� ��������� ()\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5(i, j);
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5(i, j) << ' ';
			}
			cout << endl;
		}
		cout << endl;

		cout << "\n���������� ��������� []\n";
		cout << "\n����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cin >> matr5[i][j];
			}
		}
		cout << "\n�����\n";
		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				cout << matr5[i][j] << ' ';
			}
			cout << endl;
		}
		cout << endl;
	}

	system("pause");
}