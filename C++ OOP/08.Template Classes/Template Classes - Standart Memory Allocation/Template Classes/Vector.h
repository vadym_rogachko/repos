#pragma once

using std::cout;
using std::endl;
using std::cin;
using std::istream;
using std::ostream;

template <class T>
class Vector {

	T *vect;
	int size;

public:

	Vector();
	explicit Vector(int size);
	Vector(const T *ptr, int size);
	Vector(const Vector<T> &v);
	Vector(Vector<T> &&v);
	~Vector();

	Vector<T>& operator = (const Vector<T> &v);
	Vector<T>& operator = (Vector<T> &&v);

	Vector<T>& operator ++ ();
	const Vector<T> operator ++ (int);
	Vector<T>& operator -- ();
	const Vector<T> operator -- (int);
	Vector<T> operator + (const Vector<T> &v) const;
	Vector<T> operator + (T obj) const;
	Vector<T>& operator += (const Vector<T> &v);
	Vector<T> operator - (const Vector<T> &v) const;
	Vector<T> operator - (T obj) const;
	Vector<T>& operator -= (const Vector<T> &v);
	Vector<T> operator * (const Vector<T> &v) const;
	Vector<T> operator * (T obj) const;
	Vector<T>& operator *= (T obj);

	T& operator [] (int index);

	template <typename T>
	friend istream& operator >> (istream &is, Vector<T> &v);
	template <typename T>
	friend ostream& operator << (ostream &os, const Vector<T> &v);

	int GetSize() const;
	void Print() const;
	void Input();
	void Clear();
	bool IsEmpty() const;
	void Add(const T &item);
	void Insert(int index, const T &item);
	void Remove(int index);
};

template <typename T>
Vector<T>::Vector() : size(0), vect(nullptr) {}

template <typename T>
Vector<T>::Vector(int size) {
	if (size <= 0) {
		this->size = 0;
		vect = nullptr;
		return;
	}
	this->size = size;
	vect = new T[size];
	for (int i = 0; i < size; ++i) {
		vect[i] = T(0);
	}
}

template <typename T>
Vector<T>::Vector(const T *ptr, int size) {
	if (size <= 0) {
		this->size = 0;
		vect = nullptr;
		return;
	}
	this->size = size;
	vect = new T[size];
	for (int i = 0; i < size; ++i) {
		vect[i] = ptr[i];
	}
}

template <typename T>
Vector<T>::Vector(const Vector<T> &v) :
	size(v.size) {
	if (v.size == 0) {
		vect = nullptr;
		return;
	}
	vect = new T[size];
	for (int i = 0; i < size; ++i) {
		vect[i] = v.vect[i];
	}
}

template <typename T>
Vector<T>::Vector(Vector<T> &&v) : size(v.size), vect(v.vect) {
	v.vect = nullptr;
	v.size = 0;
}

template <typename T>
Vector<T>::~Vector() {
	delete[] vect;
	size = 0;
}

template <typename T>
Vector<T>& Vector<T>::operator = (const Vector<T> &v) {
	if (this == &v) {
		return *this;
	}
	if (v.size == 0) {
		delete[] vect;
		size = 0;
		vect = nullptr;
		return *this;
	}
	delete[] vect;
	size = v.size;
	vect = new T[size];
	for (int i = 0; i < size; ++i) {
		vect[i] = v.vect[i];
	}
	return *this;
}

template <typename T>
Vector<T>& Vector<T>::operator = (Vector<T> &&v) {
	if (this == &v) {
		return *this;
	}
	delete[] vect;
	vect = v.vect;
	v.vect = nullptr;
	size = v.size;
	v.size = 0;
	return *this;
}

template <typename T>
Vector<T>& Vector<T>::operator ++ () {
	for (int i = 0; i < size; ++i) {
		++vect[i];
	}
	return *this;
}

template <typename T>
const Vector<T> Vector<T>::operator ++ (int) {
	Vector<T> temp = *this;
	for (int i = 0; i < size; ++i) {
		++vect[i];
	}
	return temp;
}

template <typename T>
Vector<T>& Vector<T>::operator -- () {
	for (int i = 0; i < size; ++i) {
		--vect[i];
	}
	return *this;
}

template <typename T>
const Vector<T> Vector<T>::operator -- (int) {
	Vector<T> temp = *this;
	for (int i = 0; i < size; ++i) {
		--vect[i];
	}
	return temp;
}

template <typename T>
Vector<T> Vector<T>::operator + (const Vector<T> &v) const {
	Vector<T> temp(size <= v.size ? size : v.size);
	for (int i = 0; i < temp.size; ++i) {
		temp.vect[i] = vect[i] + v.vect[i];
	}
	return temp;
}

template <typename T>
Vector<T> Vector<T>::operator + (T obj) const {
	Vector<T> temp = *this;
	for (int i = 0; i < size; ++i) {
		temp.vect[i] += obj;
	}
	return temp;
}

template <typename T>
Vector<T>& Vector<T>::operator += (const Vector<T> &v) {
	for (int i = 0; i < size && i < v.size; ++i) {
		vect[i] += v.vect[i];
	}
	return *this;
}

template <typename T>
Vector<T> Vector<T>::operator - (const Vector<T> &v) const {
	Vector<T> temp(size <= v.size ? size : v.size);
	for (int i = 0; i < temp.size; ++i) {
		temp.vect[i] = vect[i] - v.vect[i];
	}
	return temp;
}

template <typename T>
Vector<T> Vector<T>::operator - (T obj) const {
	Vector<T> temp = *this;
	for (int i = 0; i < size; ++i) {
		temp.vect[i] -= obj;
	}
	return temp;
}

template <typename T>
Vector<T>& Vector<T>::operator -= (const Vector<T> &v) {
	for (int i = 0; i < size && i < v.size; ++i) {
		vect[i] -= v.vect[i];
	}
	return *this;
}

template <typename T>
Vector<T> Vector<T>::operator * (const Vector<T> &v) const {
	Vector<T> temp(size <= v.size ? size : v.size);
	for (int i = 0; i < temp.size; ++i) {
		temp.vect[i] = vect[i] * v.vect[i];
	}
	return temp;
}

template <typename T>
Vector<T> Vector<T>::operator * (T obj) const {
	Vector<T> temp = *this;
	for (int i = 0; i < size; ++i) {
		temp.vect[i] *= obj;
	}
	return temp;
}

template <typename T>
Vector<T>& Vector<T>::operator *= (T obj) {
	for (int i = 0; i < size; ++i) {
		vect[i] *= obj;
	}
	return *this;
}

template <typename T>
T& Vector<T>::operator [] (int index) {
	if (index < 0) return vect[0];
	if (index >= size) return vect[size - 1];
	return vect[index];
}

template <typename T>
istream& operator >> (istream &is, Vector<T> &v) {
	v.Input();
	return is;
}

template <typename T>
ostream& operator << (ostream &os, const Vector<T> &v) {
	v.Print();
	return os;
}

template <typename T>
int Vector<T>::GetSize() const {
	return size;
}

template <typename T>
void Vector<T>::Print() const {
	if (size == 0) {
		cout << "Vector is empty\n";
		return;
	}
	for (int i = 0; i < size; ++i) {
		cout << vect[i] << ' ';
	}
	cout << endl;
}

template <typename T>
void Vector<T>::Input() {
	if (size == 0) {
		cout << "Vector is empty\n";
		return;
	}
	for (int i = 0; i < size; ++i) {
		cin >> vect[i];
	}
}

template <typename T>
void Vector<T>::Clear() {
	delete[] vect;
	vect = nullptr;
	size = 0;
}

template <typename T>
bool Vector<T>::IsEmpty() const {
	return size == 0;
}

template <typename T>
void Vector<T>::Add(const T &item) {
	T *temp = new T[size + 1];
	for (int i = 0; i < size; ++i) {
		temp[i] = vect[i];
	}
	delete[] vect;
	vect = temp;
	temp = nullptr;
	vect[size] = item;
	++size;
}

template <typename T>
void Vector<T>::Insert(int index, const T &item) {
	if (index < 0 || index >= size) return;
	++size;
	T *temp = new T[size];
	for (int i = 0; i < index; ++i) {
		temp[i] = vect[i];
	}
	temp[index] = item;
	for (int i = index + 1; i < size; ++i) {
		temp[i] = vect[i - 1];
	}
	delete[] vect;
	vect = temp;
	temp = nullptr;
}

template <typename T>
void Vector<T>::Remove(int index) {
	if (index < 0 || index >= size) return;
	--size;
	T *temp = new T[size];
	for (int i = 0; i < index; ++i) {
		temp[i] = vect[i];
	}
	for (int i = index; i < size; ++i) {
		temp[i] = vect[i + 1];
	}
	delete[] vect;
	vect = temp;
	temp = nullptr;
}