#pragma once
#include"CFraction.h"


class Num {

	double num;

public:

	Num() {};
	Num(double num) : num(num) {};
	Num(CFraction fract) : num(double(fract)) {};

	explicit operator double() const {
		return num;
	}

	Num& operator += (const Num &num) {
		this->num += num.num;
		return *this;
	}
	Num& operator -= (const Num &num) {
		this->num -= num.num;
		return *this;
	}
	Num& operator *= (const Num &num) {
		this->num *= num.num;
		return *this;
	}
	Num& operator /= (const Num &num) {
		this->num /= num.num;
		return *this;
	}

	Num operator - () const {
		return -num;
	}

	friend Num operator + (const Num &num1, const Num &num2) {
		return num1.num + num2.num;
	}
	friend Num operator - (const Num &num1, const Num &num2) {
		return num1.num - num2.num;
	}
	friend Num operator * (const Num &num1, const Num &num2) {
		return num1.num * num2.num;
	}
	friend Num operator / (const Num &num1, const Num &num2) {
		return num1.num / num2.num;
	}

	Num& operator ++ () {
		++num;
		return *this;
	}
	Num operator ++ (int) {
		Num temp = *this;
		++num;
		return temp;
	}
	Num& operator -- () {
		--num;
		return *this;
	}
	Num operator -- (int) {
		Num temp = *this;
		--num;
		return temp;
	}

	friend bool operator == (const Num &num1, const Num &num2) {
		return num1.num == num2.num;
	}
	friend bool operator != (const Num &num1, const Num &num2) {
		return !(num1 == num2);
	}
	friend bool operator < (const Num &num1, const Num &num2) {
		return num1.num < num2.num;
	}
	friend bool operator > (const Num &num1, const Num &num2) {
		return num2 < num1;
	}

	friend ostream& operator << (ostream& os, const Num &num) {
		return os << num.num;
	}
	friend istream& operator >> (istream& is, Num &num) {
		return is >> num.num;
	}
};