#pragma once
#include<iostream>;

using std::istream;
using std::ostream;

class Vector {

	int *vect;
	int size;

public:

	Vector();
	explicit Vector(int size);
	Vector(const int *ptr, int size);
	Vector(const Vector &v);
	Vector(Vector &&v);
	~Vector();

	Vector& operator = (const Vector &v);
	Vector& operator = (Vector &&v);

	Vector& operator ++ ();
	const Vector operator ++ (int);
	Vector& operator -- ();
	const Vector operator -- (int);
	Vector operator + (const Vector &v) const;
	Vector operator + (int n) const;
	Vector& operator += (const Vector &v);
	Vector operator - (const Vector &v) const;
	Vector operator - (int n) const;
	Vector& operator -= (const Vector &v);
	Vector operator * (const Vector &v) const;
	Vector operator * (int n) const;
	Vector& operator *= (int n);

	int& operator [] (int index);

	friend istream& operator >> (istream &in, Vector &v);
	friend ostream& operator << (ostream &out, const Vector &v);

	int GetSize() const;
	void Print() const;
	void Input();
	void Clear();
	bool IsEmpty() const;
	void Add(const int &item);
	void Insert(int index, const int &item);
	void Remove(int index);
};