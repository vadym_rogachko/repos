#include<iostream>
#include"Vektor.h"

using std::cout;
using std::endl;
using std::cin;
using std::boolalpha;

void main() {

	setlocale(LC_ALL, "rus");

	cout << "����������� � ����� ���������� � ����� Print\n";
	Vector vect1(5);
	vect1.Print();

	cout << "\n����� Input\n";
	vect1.Input();
	vect1.Print();

	cout << "\n����������� �����������\n";
	Vector vect2 = vect1;
	vect2.Print();

	cout << "\n����������� � ����� �����������\n";
	const int SIZE = 10;
	int arr[SIZE];
	for (int i = 0; i < SIZE; ++i) {
		arr[i] = i + 1;
	}
	Vector vect3(arr, SIZE);
	vect3.Print();

	cout << "\n�������� ������������\n";
	vect1 = vect3;
	vect1.Print();

	cout << "\n����� GetSize\n";
	cout << vect3.GetSize();

	cout << "\n\n����� IsEmpty � ����������� �� ���������\n";
	Vector vect;
	cout << boolalpha << vect.IsEmpty() << endl;
	cout << boolalpha << vect1.IsEmpty() << endl;

	cout << "\n����� Clear\n";
	cout << boolalpha << vect1.IsEmpty() << endl;
	vect1.Clear();
	cout << boolalpha << vect.IsEmpty() << endl;

	cout << "\n���������� >>\n";
	cin >> vect1;
	cout << "���������� <<\n";
	cout << vect1;

	cout << "\n\n���������� >>\n";
	cin >> vect2;
	cout << "\n���������� <<\n";
	cout << vect2;

	cout << "\n���������������� �����\n";
	cout << vect1 << vect2 << vect3;

	cout << "\n���������������� ����\n";
	cin >> vect1 >> vect2;

	cout << "\n���������� [] ������\n";
	for (int i = 0; i < vect2.GetSize(); ++i) {
		cin >> vect2[i];
	}

	cout << "\n���������� [] ������\n";
	for (int i = 0; i < vect2.GetSize(); ++i) {
		cout << vect2[i];
	}
	cout << endl;

	cout << "\n���������� ����������� ����������\n";
	cout << vect3;
	cout << ++vect3;
	cout << vect3;
	
	cout << "\n���������� ����������� ����������\n";
	cout << vect3;
	cout << --vect3;
	cout << vect3;

	cout << "\n���������� ������������ ����������\n";
	cout << vect3;
	cout << vect3++;
	cout << vect3;
	
	cout << "\n���������� ������������ ����������\n";
	cout << vect3;
	cout << vect3--;
	cout << vect3;

	//++vect3--; //������
	(++vect3)--; //�����

	cout << "\n�������� ������������ � ��������� � �������� + � ������\n";
	cout << vect3 << "+ 110\n";
	vect1 = vect3 + 110;
	cout << vect1;

	cout << "\n����������� �������� � �������� - � ������\n";
	cout << vect1 << "- 10\n";
	Vector vect4 = vect1 - 10;
	cout << vect4;

	cout << "\n�������� - � �������� - ������� ����������� �������\n";
	cout << vect1 << "- " << vect4;
	cout << vect1 - vect4;

	cout << "\n�������� + � �������� - ������� ������� �������\n";
	cout << vect2 << "+ " << vect4;
	cout << vect2 + vect4;

	cout << "\n�������� * � ������\n";
	cout << vect2 << "* 10\n";
	cout << vect2 * 10;

	vect = vect1 - vect4;
	cout << "\n�������� * � ��������\n";
	cout << vect << "* " << vect3;
	cout << vect * vect3;

	cout << "\n�������� *=\n";
	cout << vect3 << "*= 10\n";
	vect3 *= 10;
	cout << vect3;

	cout << "\n�������� +=\n";
	cout << vect << "+= " << vect3;
	vect += vect3;
	cout << vect;

	cout << "\n�������� -=\n";
	cout << vect << "-= " << vect3;
	vect -= vect3;
	cout << vect;

	cout << "\n����� Add\n";
	cout << vect2;
	vect2.Add(777);
	cout << vect2;

	cout << "\n����� Insert\n";
	cout << vect2;
	vect2.Insert(2, 888);
	cout << vect2;

	cout << "\n����� Remove\n";
	cout << vect2;
	vect2.Remove(2);
	cout << vect2;

	cout << "\n������ Insert, Remove � �������� [] � ������� �� ������� �       ������������� ��������\n";
	cout << vect2;

	cout << "\n[]\n\n";
	cout << "������\n";
	cout << "������������� ������ - ������� ������� ��������\n";
	cout << vect2[-1] << endl;
	cout << vect2[-33432423] << endl;
	cout << "����� �� ������� ������� - ������� ���������� ��������\n";
	cout << vect2[vect2.GetSize()] << endl;
	cout << vect2[31234124] << endl;
	cout << "\n������\n";
	cout << "������������� ������ - ������� ������� ��������\n";
	cin >> vect2[-1];
	cout << vect2[0] << endl;
	cin >> vect2[-33432423];
	cout << vect2[0] << endl;
	cout << "����� �� ������� ������� - ������� ���������� ��������\n";
	cin >> vect2[vect2.GetSize()];
	cout << vect2[vect2.GetSize() - 1] << endl;
	cin >> vect2[31234124];
	cout << vect2[vect2.GetSize() - 1] << endl;

	cout << "\nInsert\n";
	vect2.Insert(-1, 8);
	vect2.Insert(-324234, 8);
	vect2.Insert(vect2.GetSize(), 8);
	vect2.Insert(4324324, 8);

	cout << "\nRemove\n";
	vect2.Remove(-1);
	vect2.Remove(-3423423);
	vect2.Remove(vect2.GetSize());
	vect2.Remove(3423421);

	system("pause");
}