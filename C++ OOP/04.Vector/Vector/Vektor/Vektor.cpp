#include"Vektor.h"

using std::cout;
using std::endl;
using std::cin;

Vector::Vector() : size(0), vect(nullptr) {}

Vector:: Vector(int size) {
	if (size <= 0) {
		this->size = 0;
		vect = nullptr;
		return;
	}
	this->size = size;
	vect = new int[size];
	memset(vect, 0, size * sizeof(vect[0]));
}

Vector::Vector(const int *ptr, int size) {
	if (size <= 0) {
		this->size = 0;
		vect = nullptr;
		return;
	}
	this->size = size;
	vect = new int[size];
	for (int i = 0; i < size; ++i) {
		vect[i] = ptr[i];
	}
}

Vector::Vector(const Vector &v) : size(v.size) {
	if (v.size == 0) {
		vect = nullptr;
		return;
	}
	vect = new int[size];
	for (int i = 0; i < size; ++i) {
		vect[i] = v.vect[i];
	}
}

Vector::Vector(Vector &&v) : size(v.size), vect(v.vect) {
	v.vect = nullptr;
	v.size = 0;
}

Vector::~Vector() {
	delete[] vect;
	size = 0;
}

Vector& Vector::operator = (const Vector &v) {
	if (this == &v) {
		return *this;
	}
	if (v.size == 0) {
		delete[] vect;
		size = 0;
		vect = nullptr;
		return *this;
	}
	delete[] vect;
	size = v.size;
	vect = new int[size];
	for (int i = 0; i < size; ++i) {
		vect[i] = v.vect[i];
	}
	return *this;
}

Vector& Vector::operator = (Vector &&v) {
	if (this == &v) {
		return *this;
	}
	delete[] vect;
	vect = v.vect;
	v.vect = nullptr;
	size = v.size;
	v.size = 0;
	return *this;
}

Vector& Vector::operator ++ () {
	for (int i = 0; i < size; ++i) {
		++vect[i];
	}
	return *this;
}

const Vector Vector::operator ++ (int) {
	Vector temp = *this;
	for (int i = 0; i < size; ++i) {
		++vect[i];
	}
	return temp;
}

Vector& Vector::operator -- () {
	for (int i = 0; i < size; ++i) {
		--vect[i];
	}
	return *this;
}

const Vector Vector::operator -- (int) {
	Vector temp = *this;
	for (int i = 0; i < size; ++i) {
		--vect[i];
	}
	return temp;
}

Vector Vector::operator + (const Vector &v) const {
	Vector temp(size <= v.size ? size : v.size);
	for (int i = 0; i < temp.size; ++i) {
		temp.vect[i] = vect[i] + v.vect[i];
	}
	return temp;
}

Vector Vector::operator + (int n) const {
	Vector temp = *this;
	for (int i = 0; i < size; ++i) {
		temp.vect[i] += n;
	}
	return temp;
}

Vector& Vector::operator += (const Vector &v) {
	for (int i = 0; i < size && i < v.size; ++i) {
		vect[i] += v.vect[i];
	}
	return *this;
}

Vector Vector::operator - (const Vector &v) const {
	Vector temp(size <= v.size ? size : v.size);
	for (int i = 0; i < temp.size; ++i) {
		temp.vect[i] = vect[i] - v.vect[i];
	}
	return temp;
}

Vector Vector::operator - (int n) const {
	Vector temp = *this;
	for (int i = 0; i < size; ++i) {
		temp.vect[i] -= n;
	}
	return temp;
}

Vector& Vector::operator -= (const Vector &v) {
	for (int i = 0; i < size && i < v.size; ++i) {
		vect[i] -= v.vect[i];
	}
	return *this;
}

Vector Vector::operator * (const Vector &v) const {
	Vector temp(size <= v.size ? size : v.size);
	for (int i = 0; i < temp.size; ++i) {
		temp.vect[i] = vect[i] * v.vect[i];
	}
	return temp;
}

Vector Vector::operator * (int n) const {
	Vector temp = *this;
	for (int i = 0; i < size; ++i) {
		temp.vect[i] *= n;
	}
	return temp;
}

Vector& Vector::operator *= (int n) {
	for (int i = 0; i < size; ++i) {
		vect[i] *= n;
	}
	return *this;
}

int& Vector::operator [] (int index) {
	if (index < 0) return vect[0];
	if (index >= size) return vect[size - 1];
	return vect[index];
}

istream& operator >> (istream &in, Vector &v) {
	v.Input();
	return in;
}

ostream& operator << (ostream &out, const Vector &v) {
	v.Print();
	return out;
}

int Vector::GetSize() const {
	return size;
}

void Vector::Print() const {
	if (size == 0) {
		cout << "Vector is empty\n";
		return;
	}
	for (int i = 0; i < size; ++i) {
		cout << vect[i] << ' ';
	}
	cout << endl;
}

void Vector::Input() {
	if (size == 0) {
		cout << "Vector is empty\n";
		return;
	}
	for (int i = 0; i < size; ++i) {
		cin >> vect[i];
	}
}

void Vector::Clear() {
	delete[] vect;
	vect = nullptr;
	size = 0;
}

bool Vector::IsEmpty() const {
	return size == 0;
}

void Vector::Add(const int &item) {
	int *temp = new int[size + 1];
	for (int i = 0; i < size; ++i) {
		temp[i] = vect[i];
	}
	delete[] vect;
	vect = temp;
	temp = nullptr;
	vect[size] = item;
	++size;
}

void Vector::Insert(int index, const int &item) {
	if (index < 0 || index >= size) return;
	++size;
	int *temp = new int[size];
	for (int i = 0; i < index; ++i) {
		temp[i] = vect[i];
	}
	temp[index] = item;
	for (int i = index + 1; i < size; ++i) {
		temp[i] = vect[i - 1];
	}
	delete[] vect;
	vect = temp;
	temp = nullptr;
}

void Vector::Remove(int index) {
	if (index < 0 || index >= size) return;
	--size;
	int *temp = new int[size];
	for (int i = 0; i < index; ++i) {
		temp[i] = vect[i];
	}
	for (int i = index; i < size; ++i) {
		temp[i] = vect[i + 1];
	}
	delete[] vect;
	vect = temp;
	temp = nullptr;
}