#pragma once

using std::cout;
using std::endl;

template <typename T>
struct Element {
	T data;
	Element *Next;
};


template <class T>
class List {
	Element<T> *Head;
	int count;

public:

	List();
	~List();
	void Add(T data);
	void Del();
	void DelAll();
	void Print() const;
	int GetCount() const;

	void InsertByPosition(T data, int position);
	void RemoveByPosition(int position);
	int Find(T key) const;
	List(const List<T> &obj);
	List(List<T> &&obj);
	List<T>& operator = (const List<T> &obj);
	List<T>& operator = (List<T> &&obj);
};

template <typename T>
List<T>::List() {
	Head = nullptr;
	count = 0;
}

template <typename T>
List<T>::~List() {
	DelAll();
}

template <typename T>
int List<T>::GetCount() const {
	return count;
}

template <typename T>
void List<T>::Add(T data) {
	Element<T> *temp = new Element<T>;
	temp->data = data;
	temp->Next = Head;
	Head = temp;
	++count;
}

template <typename T>
void List<T>::Del() {
	if (Head) {
		Element<T> *temp = Head->Next;
		delete Head;
		Head = temp;
		--count;
	}
}

template <typename T>
void List<T>::DelAll() {
	while (Head != nullptr)
		Del();
}

template <typename T>
void List<T>::Print() const {
	Element<T> *temp = Head;
	if (temp == nullptr) {
		cout << "List is empty!";
		return;
	}
	while (temp != 0) {
		cout << temp->data << ' ';
		temp = temp->Next;
	}
	cout << endl;
}

//////////////////////////////////////////////////////

template <>
void List<char>::Print() const {
	Element<char> *temp = Head;
	if (temp == nullptr) {
		cout << "List is empty!";
		return;
	}
	while (temp != 0) {
		cout << temp->data;
		temp = temp->Next;
	}
	cout << endl;
}

template <typename T>
void List<T>::InsertByPosition(T data, int position) {
	if (position <= 0 || position > count + 1) {
		return;
	}
	if (position == 1) {
		Add(data);
		return;
	}
	int counter = 1;
	Element<T> *temp = Head;
	while (counter != position - 1) {
		temp = temp->Next;
		++counter;
	}
	Element<T> *elem = new Element<T>;
	elem->data = data;
	elem->Next = temp->Next;
	temp->Next = elem;
	++count;
}

template <typename T>
void List<T>::RemoveByPosition(int position) {
	if (position <= 0 || position > count) {
		return;
	}
	if (position == 1) {
		Del();
		return;
	}
	int counter = 1;
	Element<T> *temp = Head;
	while (counter != position - 1) {
		temp = temp->Next;
		++counter;
	}
	Element<T> *elem = temp->Next;
	temp->Next = temp->Next->Next;
	delete elem;
	--count;
}

template <typename T>
int List<T>::Find(T key) const {
	Element<T> *temp = Head;
	for (int i = 0; i < count; ++i) {
		if (temp->data == key) {
			return i + 1;
		}
		temp = temp->Next;
	}
	return -1;
}

template <typename T>
List<T>::List(const List<T> &obj) :
	Head(nullptr), count(0)
	{
		*this = obj;
	}

template <typename T>
List<T>::List(List<T> &&obj) :
	Head(nullptr), count(0) 
	{
		*this = move(obj);
	}

 // ������ � �������� � ������� - ���������, �������� ������
template <typename T>
List<T>& List<T>::operator = (const List &obj) {
	if (this == &obj) {
		return *this;
	}
	if (obj.count == 0) {
		Head = nullptr;
		count = 0;
		return *this;
	}
	DelAll();
	Element<T> *temp = obj.Head;
	for (int i = 0; i < obj.count; ++i) {
		InsertByPosition(temp->data, i + 1);
		temp = temp->Next;
	}
	return *this;
}

// ������ � �������� - �������, ����� ������
//template <typename T>
//List<T>& List<T>::operator = (const List &obj) {
//	if (this == &obj) {
//		return *this;
//	}
//	if (obj.count == 0) {
//		Head = nullptr;
//		count = 0;
//		return *this;
//	}
//	DelAll();
//	Element<T> **tempArr = new Element<T>*[obj.count];
//	Element<T> *temp = obj.Head;
//	for (int i = 0; i < obj.count; ++i) {
//		tempArr[i] = temp;
//		temp = temp->Next;
//	}
//	for (int i = obj.count - 1; i >= 0; --i) {
//		Add(tempArr[i]->data);
//	}
//	delete[] tempArr;
//	return *this;
//}

// ������ � ��������� ������� - ��� ����� - ����� ������ ������
//template <typename T>
//List<T>& List<T>::operator = (const List &obj) {
//	if (this == &obj) {
//		return *this;
//	}
//	if (obj.count == 0) {
//		Head = nullptr;
//		count = 0;
//		return *this;
//	}
//	DelAll();
//	List<T> tempList;
//	Element<T> *temp = obj.Head;
//	for (int i = 0; i < obj.count; ++i) {
//		tempList.Add(temp->data);
//		temp = temp->Next;
//	}
//	temp = tempList.Head;
//	for (int i = 0; i < obj.count; ++i) {
//		Add(temp->data);
//		temp = temp->Next;
//	}
//	return *this;
//}

template <typename T>
List<T>& List<T>::operator = (List &&obj) {
	if (this == &obj) {
		return *this;
	}
	if (obj.count == 0) {
		Head = nullptr;
		count = 0;
		return *this;
	}
	DelAll();
	Head = obj.Head;
	count = obj.count;
	obj.Head = nullptr;
	obj.count = 0;
	return *this;
}