#include <iostream>
#include "List.h"
#include "Num.h"

using namespace std;

template <typename T>
List<T> foo(List<T> &obj) {
	List<T> temp = obj;
	return temp;
}

void main() {

	setlocale(LC_ALL, "rus");

	// char
	cout << "char\n\n";
	List<char> lst;
	char s[] = "Hello, World !!!\n";
	int len = strlen(s);
	for (int i = len - 1; i >= 0; --i) {
		lst.Add(s[i]);
	}
	lst.Print();

	cout << "������� �������� � �������� �������\n";
	lst.InsertByPosition('a', 5);
	lst.Print();

	cout << "�������� �������� �� �������� �������\n";
	lst.RemoveByPosition(5);
	lst.Print();
	cout << "�������� �������� �� �������� �������\n";
	lst.RemoveByPosition(11);
	lst.RemoveByPosition(13);
	lst.RemoveByPosition(14);
	lst.Print();

	cout << "����� ��������� �������� �� �����\n";
	cout << lst.Find('s') << endl;
	cout << lst.Find('W') << endl << endl;

	cout << "����������� �����������\n";
	List<char> lst2 = lst;
	lst2.Print();

	cout << "������������� �������� =\n";
	List<char> lst3;
	lst3 = lst;
	lst3.Print();

	cout << "����������� ��������\n";
	List<char> lst4 = foo(lst3);
	lst4.Print();

	cout << "������������� �������� = � ���������\n";
	List<char> lst5;
	lst5 = foo(lst3);
	lst5.Print();


	// int
	cout << "\n\nint\n\n";
	List<int> lstInt;
	for (int i = 0; i < 10; ++i) {
		lstInt.InsertByPosition(i + 1, i + 1);
	}
	lstInt.Print();
	cout << endl;

	cout << "������� �������� � �������� �������\n";
	lstInt.InsertByPosition(777, 3);
	lstInt.Print();
	cout << endl;

	cout << "�������� �������� �� �������� �������\n";
	lstInt.RemoveByPosition(3);
	lstInt.Print();
	cout << endl;
	cout << "�������� �������� �� �������� �������\n";
	lstInt.RemoveByPosition(8);
	lstInt.Print();
	cout << endl;

	cout << "����� ��������� �������� �� �����\n";
	cout << lstInt.Find(777) << endl;
	cout << lstInt.Find(7) << endl << endl;

	cout << "����������� �����������\n";
	List<int> lstInt2 = lstInt;
	lstInt2.Print();
	cout << endl;

	cout << "������������� �������� =\n";
	List<int> lstInt3;
	lstInt3 = lstInt;
	lstInt3.Print();
	cout << endl;

	cout << "����������� ��������\n";
	List<int> lstInt4 = foo(lstInt3);
	lstInt4.Print();
	cout << endl;

	cout << "������������� �������� = � ���������\n";
	List<int> lstInt5;
	lstInt5 = foo(lstInt3);
	lstInt5.Print();
	cout << endl;


	// Num
	cout << "\n\nNum\n\n";
	List<Num> lstNum;
	for (int i = 0; i < 10; ++i) {
		lstNum.InsertByPosition(i + 1.5, i + 1);
	}
	lstNum.Print();
	cout << endl;

	cout << "������� �������� � �������� �������\n";
	lstNum.InsertByPosition(3.82, 11);
	lstNum.Print();
	cout << endl;

	cout << "�������� �������� �� �������� �������\n";
	lstNum.RemoveByPosition(7);
	lstNum.Print();
	cout << endl;
	cout << "�������� �������� �� �������� �������\n";
	lstNum.RemoveByPosition(9);
	lstNum.Print();
	cout << endl;

	cout << "����� ��������� �������� �� �����\n";
	cout << lstNum.Find(3) << endl;
	cout << lstNum.Find(2.5) << endl << endl;

	cout << "����������� �����������\n";
	List<Num> lstNum2 = lstNum;
	lstNum2.Print();
	cout << endl;

	cout << "������������� �������� =\n";
	List<Num> lstNum3;
	lstNum3 = lstNum;
	lstNum3.Print();
	cout << endl;

	cout << "����������� ��������\n";
	List<Num> lstNum4 = foo(lstNum3);
	lstNum4.Print();
	cout << endl;

	cout << "������������� �������� = � ���������\n";
	List<Num> lstNum5;
	lstNum5 = foo(lstNum3);
	lstNum5.Print();
	cout << endl << endl;

	system("pause");
}