﻿using System;

namespace Overloading_Exceptions
{
    class Program
    {
        static void Main() {
			
			try {
				Console.WriteLine("Первая дата\n");
				Date date1 = new Date();

				Console.Write("Введите день: ");
				date1.Day = Convert.ToInt32(Console.ReadLine());
				Console.Write("Введите месяц: ");
				date1.Month = Convert.ToInt32(Console.ReadLine());
				Console.Write("Введите год: ");
				date1.Year = Convert.ToInt32(Console.ReadLine());

				Console.WriteLine("\nВторая дата\n");
				int day, month, year;

				Console.Write("Введите день: ");
				day = Convert.ToInt32(Console.ReadLine());
				Console.Write("Введите месяц: ");
				month = Convert.ToInt32(Console.ReadLine());
				Console.Write("Введите год: ");
				year = Convert.ToInt32(Console.ReadLine());
				Date date2 = new Date(day, month, year);


				Console.WriteLine("\n");
				date1.PrintDate();
				Console.WriteLine($"\n{(Date.isLeapYear(date1.Year) ? "Викокосный год\n" : "Не високосный год\n")}");
				date2.PrintDate();
				Console.WriteLine($"\n{(Date.isLeapYear(date2.Year) ? "Викокосный год\n" : "Не високосный год\n")}");

				Console.WriteLine($"Разница в днях {date1 - date2}\n");
				Console.ReadKey(true);
				Console.Clear();


				Console.WriteLine("\nПрибавление дней\n");
				Console.Write("Введите количество дней:\n   ");
				date1.PrintDate();
				Console.Write(" + ");
				date1 = date1 + Convert.ToInt32(Console.ReadLine());
				Console.Write(" =\n   ");
				date1.PrintDate();
				Console.WriteLine("\n");
				Console.ReadKey(true);
				Console.Clear();


				date1.PrintDate();

				Console.WriteLine("\n\nИнкремент");
				++date1;
				date1.PrintDate();

				Console.WriteLine("\n\nДекремент");
				--date1;
				date1.PrintDate();

				Console.WriteLine();
				Console.ReadKey(true);
				Console.Clear();


				Console.WriteLine("Сравнение дат");
				date1.PrintDate();
				Console.WriteLine();
				date2.PrintDate();
				Console.WriteLine("\n\n");

				date1.PrintDate();
				Console.Write(" > ");
				date2.PrintDate();
				Console.Write(" ");
				Console.WriteLine((date1 > date2));
				Console.WriteLine();

				date1.PrintDate();
				Console.Write(" < ");
				date2.PrintDate();
				Console.Write(" ");
				Console.WriteLine((date1 < date2));
				Console.WriteLine();

				date1.PrintDate();
				Console.Write(" == ");
				date2.PrintDate();
				Console.Write(" ");
				Console.WriteLine((date1 == date2));
				Console.WriteLine();

				date1.PrintDate();
				Console.Write(" != ");
				date2.PrintDate();
				Console.Write(" ");
				Console.WriteLine((date1 != date2));
				Console.WriteLine();

				Console.ReadKey(true);
			}
			catch (ArgumentException e) {
				Console.WriteLine($"Ошибка: {e.Message}");
				Console.WriteLine($"Источник: {e.TargetSite}");
				Console.ReadKey(true);
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
        }
    }
}
