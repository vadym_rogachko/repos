﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Overloading_Exceptions
{
    class Date
    {
		enum DaysOfWeek { Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday };
		enum Months { January = 1, February, March, April, May, June, July, August, September, October, November, December }
		
		int day;
        int month;
        int year;

		#region private int AllDays

		private int AllDays {
			get {
				Date temp = new Date();
				int allDays = 0;
				while (temp.Year < Year) {
					if (isLeapYear(temp.Year)) {
						allDays += 366;
					}
					else {
						allDays += 365;
					}
					++temp.Year;
				}
				while (temp.Month < Month) {
					switch ((Months)temp.Month) {
						case Months.January:
						case Months.March:
						case Months.May:
						case Months.July:
						case Months.August:
						case Months.October:
						case Months.December:
							allDays += 31;
							break;
						case Months.April:
						case Months.June:
						case Months.September:
						case Months.November:
							allDays += 30;
							break;
						case Months.February:
							if (isLeapYear(temp.Year)) {
								allDays += 29;
							}
							else {
								allDays += 28;
							}
							break;
					}
					++temp.Month;
				}
				return allDays + Day;
			}
		}
		#endregion
		#region private void setDate(int days)

		private void setDate(int days) {
			Day = Month = Year = 1;
			while (days > 0) {
				if (isLeapYear(Year) && days >= 366) {
					++Year;
					days -= 366;
					continue;
				}
				else if (!isLeapYear(Year) && days >= 365) {
					++Year;
					days -= 365;
					continue;
				}
				if (days <= 31) {
					Day = days;
					return;
				}
				while (Month < 12) {
					switch ((Months)Month) {
						case Months.January:
						case Months.March:
						case Months.May:
						case Months.July:
						case Months.August:
						case Months.October:
						case Months.December:
							if (days <= 31) {
								Day = days;
								return;
							}
							days -= 31;
							++Month;
							break;
						case Months.April:
						case Months.June:
						case Months.September:
						case Months.November:
							if (days <= 30) {
								Day = days;
								return;
							}
							days -= 30;
							++Month;
							break;
						case Months.February:
							if (isLeapYear(Year) && days <= 29) {
								Day = days;
								return;
							}
							if (!isLeapYear(Year) && days <= 28) {
								Day = days;
								return;
							}
							if (isLeapYear(Year)) {
								days -= 29;
							}
							else {
								days -= 28;
							}
							++Month;
							break;
					}
				}
				Day = days;
				return;
			}
		}
		#endregion

		#region public int Day

		public int Day { 
            get { return day; }
            set {
				switch ((Months)Month) {
					case Months.January:
					case Months.March:
					case Months.May:
					case Months.July:
					case Months.August:
					case Months.October:
					case Months.December:
						if (value < 1 || value > 31) {
							throw new ArgumentException("Некорректное значение дня");
						}
						break;
					case Months.April:
					case Months.June:
					case Months.September:
					case Months.November:
						if (value < 1 || value > 30) {
							throw new ArgumentException("Некорректное значение дня");
						}
						break;
					case Months.February:
						if (isLeapYear(Year)) {
							if (value < 1 || value > 29) {
							throw new ArgumentException("Некорректное значение дня");
							}
						}
						else {
							if (value < 1 || value > 28) {
								throw new ArgumentException("Некорректное значение дня");
							}
						}
						break;
				}
				day = value;
			}
        }
		#endregion
		#region public int Month

		public int Month { 
            get { return month; }
            set {
				if (value < 1 || value > 12) {
					throw new ArgumentException("Некорректное значение месяца");
				}
				month = value;
			}
        }
		#endregion
		#region public int Year

		public int Year
        {
            get { return year; }
            set {
				if (value < 1) {
					throw new ArgumentException("Некорректное значение года");
				}
				year = value;
			}
        }
		#endregion
		#region public string DayOfWeek

		public string DayOfWeek
		{
			get {
				return ((DaysOfWeek)((this - new Date()) % 7)).ToString();
			}
		}
		#endregion

		#region public Date()

		public Date() : this(1, 1, 1) { }
		#endregion
		#region public Date(int day, int month, int year)

		public Date(int day, int month, int year)
        {
			Month = month;
			Year = year;
			Day = day;
		}
		#endregion

		#region public static int operator -(Date date1, Date date2

		public static int operator -(Date date1, Date date2) {
			return Math.Abs(date1.AllDays - date2.AllDays);
		}
		#endregion
		#region public static Date operator +(Date date, int days)

		public static Date operator +(Date date, int days) {
			days += date.AllDays;
			Date temp = new Date();
			temp.setDate(days);
			return temp;
		}
		#endregion
		#region public static Date operator +(int days, Date date)

		public static Date operator +(int days, Date date) {
			return date + days;
		}
		#endregion

		#region public static Date operator ++(Date date)

		public static Date operator ++(Date date) {
			date.setDate(date.AllDays + 1);
			return date;
		}
		#endregion
		#region public static Date operator --(Date date)

		public static Date operator --(Date date) {
			date.setDate(date.AllDays - 1);
			return date;
		}
		#endregion

		#region public static bool operator >(Date date1, Date date2)

		public static bool operator >(Date date1, Date date2) {
			if (date1.Year > date2.Year) return true;
			if (date1.Year < date2.Year) return false;
			if (date1.Month > date2.Month) return true;
			if (date1.Month < date2.Month) return false;
			if (date1.Day > date2.Day) return true;
			return false;
		}
		#endregion
		#region public static bool operator <(Date date1, Date date2)

		public static bool operator <(Date date1, Date date2) {
			return !(date1 > date2) && date1 != date2;
		}
		#endregion
		#region public static bool operator ==(Date date1, Date date2)

		public static bool operator ==(Date date1, Date date2) {
			return date1.Day == date2.Day &&
				date1.Month == date2.Month &&
				date1.Year == date2.Year;
		}
		#endregion
		#region public static bool operator !=(Date date1, Date date2)

		public static bool operator !=(Date date1, Date date2) {
			return !(date1 == date2);
		}
		#endregion

		#region public void PrintDate()

		public void PrintDate() {
			Console.Write($"{Day} {((Months)Month).ToString()} {Year} {DayOfWeek}");
		}
		#endregion

		#region public static bool isLeapYear(int year)

		public static bool isLeapYear(int year) {
			return (year % 4 == 0 && year % 100 != 0)
				|| (year % 4 == 0 && year % 400 == 0);
		}
		#endregion

	}
}