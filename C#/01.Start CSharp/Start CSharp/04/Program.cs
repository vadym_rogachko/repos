﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*4. Осуществить циклический сдвиг влево введённого числа
*    на N разрядов (например, при сдвиге числа 12345 влево
*    на 3 разряда получится число 45123) */

namespace _04 {
    class Program {
        static void Main(string[] args) {

            Console.Write("Введите число: ");
            uint num = Convert.ToUInt32(Console.ReadLine());
            Console.Write("Введите количество разрядов для сдвига влево: ");
            uint N = Convert.ToUInt32(Console.ReadLine());
            uint count = 1;
            uint newNum = num;
            for (uint i = num; i > 9; i /= 10) {
                count *= 10;
            }
            while (N > 0) {
                newNum = newNum % count * 10 + newNum / count;
                --N;
            }
            Console.Clear();
            Console.WriteLine("{0}\n<--\n{1}", num, newNum);
			Console.ReadKey(true);
		}
	}
}
