﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*3. Написать игру «Угадай число!» */

namespace _03 {
    class Program {
        static void Main(string[] args) {

            uint min = 1, max = 100;
            uint counter = 1, mid;
            char ch;
            Console.WriteLine("Загадайте число от {0} до {1}", min, max);
			Console.ReadKey(true);
			Console.Clear();
            while (true) {
                mid = (min + max) / 2;
                Console.WriteLine("Вы загадали {0}? (Y/N)", mid);
                do {
                    ch = Convert.ToChar(Console.ReadLine());
                    ch = char.ToUpper(ch);
                } while (ch != 'Y' && ch != 'N');
				Console.Clear();
                if (ch != 'Y') {
                    if (counter == 7) {
                        Console.WriteLine("Вы смухлевали\nПовторить? (Y/N)");
                        do {
                            ch = Convert.ToChar(Console.ReadLine());
                            ch = char.ToUpper(ch);
                        } while (ch != 'Y' && ch != 'N');
						Console.Clear();
                        if (ch == 'Y') {
                            min = 0;
                            max = 100;
                            counter = 1;
                            continue;
                        }
                        return;
                    }
                    Console.WriteLine("Число больше {0}? (Y/N)", mid);
                    do {
                        ch = Convert.ToChar(Console.ReadLine());
                        ch = char.ToUpper(ch);
                    } while (ch != 'Y' && ch != 'N');
					Console.Clear();
                    if (ch == 'Y') {
                        min = mid + 1;
                    }
                    else {
                        max = mid;
                    }
                }
                else {
                    Console.WriteLine("Число отгадано\nКоличество попыток: {0}", counter);
                    Console.WriteLine("Повторить? (Y/N)");
                    do {
                        ch = Convert.ToChar(Console.ReadLine());
                        ch = char.ToUpper(ch);
                    } while (ch != 'Y' && ch != 'N');
					Console.Clear();
                    if (ch == 'Y') {
                        min = 0;
                        max = 100;
                        counter = 1;
                        continue;
                    }
					return;
                }
                ++counter;
            }
        }
    }
}
