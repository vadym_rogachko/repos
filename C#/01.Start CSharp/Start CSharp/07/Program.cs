﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/*7. С клавиатуры вводится произвольная дата (день, месяц, год).
 *   Необходимо вывести название дня недели */

namespace _07 {
    class Program {
        static void Main(string[] args) {
			int day, month, year;
			bool flag;
			do {
				Console.Clear();
				Console.Write("Введите день: ");
				day = Convert.ToInt32(Console.ReadLine());
				Console.Write("Введите месяц: ");
				month = Convert.ToInt32(Console.ReadLine());
				Console.Write("Введите год: ");
				year = Convert.ToInt32(Console.ReadLine());
				switch (month) {
					case 4:
					case 6:
					case 9:
					case 11:
						flag = day != 31;
						break;
					case 2:
						flag = day != 31 && day != 30;
						if (day == 29) {
							flag = year % 4 == 0 && year % 100 != 0 || year % 400 == 0 ? true : false;
						}
						break;
					default:
						flag = true;
						break;
				}
			} while (day < 1 || day > 31 || month < 1 || month > 12 || year == 0 || flag == false);

		int sumOfDays = 0;
		for (; year > 0; month = 12, --year) {
				for (; month > 0; day = 31, --month) {
					for (; day > 0; --day) {
						if (day == 31) {
							switch (month) {
								case 4:
								case 6:
								case 9:
								case 11:
									day = 30;
									break;
								case 2:
									day = year % 4 == 0 && year % 100 != 0 || year % 400 == 0 ? 29 : 28;
									break;
							}
						}
                        ++sumOfDays;
                    }
				}
			}

		switch(sumOfDays % 7) {
				case 0:
					Console.WriteLine("Воскресенье");
					break;
				case 1:
					Console.WriteLine("Понедельник");
					break;
				case 2:
					Console.WriteLine("Вторник");
					break;
				case 3:
					Console.WriteLine("Среда");
					break;
				case 4:
					Console.WriteLine("Четверг");
					break;
				case 5:
					Console.WriteLine("Пятница");
					break;
				case 6:
					Console.WriteLine("Суббота");
					break;
			}
			Console.ReadKey(true);
        }
    }
}
