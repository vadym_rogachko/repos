﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*6. В цикле с клавиатуры вводятся 15 целых чисел.Необходимо найти самую длинную неубывающую цепочку чисел.
 *   На экран вывести найденную максимальную длину цепочки и порядковый номер того числа, с которого цепочка началась*/

namespace _06 {
    class Program {
        static void Main(string[] args) {
            int length = 0; 
			int cur, prev;
			int numMax = 1, numCur = 1;
			Console.WriteLine("Введите 15 целых чисел");
			prev = Convert.ToInt32(Console.ReadLine());
			for (int i = 1; i < 15; ++i) {
				cur = Convert.ToInt32(Console.ReadLine());
				if (cur < prev || i == 14) {
					if (i - numCur >= length) {
						numMax = numCur;
						length = i - numCur;
                        if (i == 14) ++length;
					}
                    numCur = i + 1;
                }
                prev = cur;
			}
			Console.WriteLine("\nМаксимальная длина цепочки {0}\nНачало цепочки {1}", length + 1, numMax);
            Console.ReadKey(true);
        }
    }
}
