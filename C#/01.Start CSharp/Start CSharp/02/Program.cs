﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*2. С клавиатуры вводится целое число любой разрядности.
 *   Необходимо перевернуть это число,
 *   т.е. цифры должны располагаться в обратном порядке
 *   (например, вводим число 1234 – в результате будет 4321) */

namespace _02 {
    class Program {
        static void Main(string[] args) {

            uint num, newNum, lastNum;
            Console.Write("Введите число: ");
            num = Convert.ToUInt32(Console.ReadLine());
            newNum = num % 10;
            uint i = num / 10;
            while (i > 0)
            {
                lastNum = i % 10;
                newNum = newNum * 10 + lastNum;
                i /= 10;
            }
            Console.Clear();
            Console.WriteLine("{0} --> {1}", num, newNum);
			Console.ReadKey(true);
		}
	}
}
