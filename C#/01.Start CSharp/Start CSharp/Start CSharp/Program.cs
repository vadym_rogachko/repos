﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*1. Написать программу, вычисляющую факториал введённого числа */

namespace _01 {
    class Program {
        static void Main(string[] args) {

            decimal fact = 1;
            Console.Write("Введите число: ");
            ushort num = Convert.ToUInt16(Console.ReadLine());
            for (int i = 1; i <= num; ++i) {
                fact *= i;
            }
            Console.Clear();
            Console.WriteLine("{0}! = {1}", num, fact);
			Console.ReadKey(true);
		}
	}
}
