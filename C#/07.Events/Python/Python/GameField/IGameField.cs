﻿
namespace Python
{
	delegate void Move();
	delegate void Eat();
	delegate void Ram();

	interface IGameField
	{
		event Move PythonMove;
		event Eat PythonEatFruit;
		event Ram PythonRam;

		void Show();

		void PrintMsg(string msg);

		void Action();

		int Width { get; set; }

		int Height { get; set; }

		int BorderLeft { get; }

		int BorderRight { get; }

		int BorderUp { get; }

		int BorderDown { get; }
	}
}
