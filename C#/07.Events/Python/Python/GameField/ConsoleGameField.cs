﻿using System;

namespace Python
{

	class ConsoleGameField 
		: IGameField
	{
		public event Move PythonMove;
		public event Eat PythonEatFruit;
		public event Ram PythonRam;

		public ConsoleGameField(int width, int height) {
			Console.CursorVisible = false;
			Width = width;
			Height = height;
		}

		public void Show() {
			if (Python.Alive) {
				Console.ResetColor();
			}
			Console.Clear();
			Console.SetCursorPosition(Fruit.Position.X, Fruit.Position.Y);
			Console.Write(Fruit.view);
			if (!Python.Alive) {
				Console.ForegroundColor = ConsoleColor.Red;
			}
			foreach (var part in Python.Body) {
				Console.SetCursorPosition(part.X, part.Y);
				Console.Write(Python.view);
			}
			Console.SetCursorPosition(Python.Head.X, Python.Head.Y);
			Console.Write(Python.view);
		}

		public void Action() {
			PythonMove();
			if (Python.Head.X == BorderLeft || Python.Head.X == BorderRight ||
				Python.Head.Y == BorderUp || Python.Head.Y == BorderDown) {
				PythonRam();
				return;
			}
			if (Python.Body.Contains(Python.Head)) {
				PythonRam();
				return;
			}
			if (Python.Head.Equals(Fruit.Position)) {
				PythonEatFruit();
				return;
			}
		}

		public void PrintMsg(string msg) {
			Console.SetCursorPosition(Width / 2 - 10, Height / 2);
			Console.Write(msg);
		}

		public int Width
		{
			get { return Console.WindowWidth; }
			set {
				try {
					Console.WindowWidth = value;
					Console.BufferWidth = value;
				}
				catch (Exception) { }
			}
		}

		public int Height
		{
			get { return Console.WindowHeight; }
			set {
				try {
					Console.WindowHeight = value;
					Console.BufferHeight = value;
				}
				catch (Exception) { }
			}
		}

		public int BorderLeft
		{
			get { return -1; }
		}

		public int BorderRight
		{
			get { return Width; }
		}

		public int BorderUp
		{
			get { return -1; }
		}

		public int BorderDown
		{
			get { return Height; }
		}
	}
}
