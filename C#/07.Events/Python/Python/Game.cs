﻿using System;
using System.Timers;

namespace Python
{

	class Game
	{

		IGameField field;
		Timer timer;

		public Game(IGameField field, int speed = 0) {
			this.field = field;
			timer = new Timer();
			timer.Elapsed += delegate {
				field.Action();
				field.Show();
			};
			Speed = speed;
			field.PythonMove += Python.Move;
			field.PythonEatFruit += Python.Eat;
			field.PythonEatFruit += CreateFruit;
			field.PythonRam += timer.Stop;
			field.PythonRam += Python.Die;
		}

		void CreateFruit() {
			Random rand = new Random();
			Fruit.Position = (new Coord (rand.Next(field.Width), rand.Next(field.Height)));
		}

		void CreatePython() {
			Python.Born(field.Width / 2, field.Height / 2);
		}

		public double Speed
		{
			get { return timer.Interval; }
			set {
				if (value >= 0 && value <= 99) {
					timer.Interval = 100 - value;
				}
			}
		}

		void Begin() {
			CreatePython();
			CreateFruit();
			timer.Start();
		}

		public void StartTheGame() {
			field.PrintMsg("Press Enter to Start");
			ConsoleKey key;
			do {
				key = Console.ReadKey(true).Key;
				switch (key) {
					case ConsoleKey.UpArrow:
						Python.Direction = Python.Dir.UP;
						break;
					case ConsoleKey.DownArrow:
						Python.Direction = Python.Dir.DOWN;
						break;
					case ConsoleKey.LeftArrow:
						Python.Direction = Python.Dir.LEFT;
						break;
					case ConsoleKey.RightArrow:
						Python.Direction = Python.Dir.RIGHT;
						break;
					case ConsoleKey.Enter:
						Begin();
						break;

				}
			} while (key != ConsoleKey.Escape);
		}
	}
}
