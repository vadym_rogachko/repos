﻿using System;

namespace Python
{
	public struct Coord : ICloneable
	{
		public int X { get; set; }
		public int Y { get; set; }

		public Coord(int x, int y) {
			X = Math.Abs(x);
			Y = Math.Abs(y);
		}

		public object Clone() {
			return MemberwiseClone();
		}

		public override bool Equals(object obj) {
			if (obj == null || !(obj is Coord)) return false;
			return X == ((Coord)obj).X && Y == ((Coord)obj).Y;
		}

		public override int GetHashCode() {
			return X.GetHashCode() ^ Y.GetHashCode();
		}
	}
}
