﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Python
{
	static class Python
	{
		public enum Dir
		{
			UP = ConsoleKey.UpArrow,
			DOWN = ConsoleKey.DownArrow,
			LEFT = ConsoleKey.LeftArrow,
			RIGHT = ConsoleKey.RightArrow
		};

		static int length;
		static Dir direction;
		public const char view = (char)3;
		static List<Coord> body;

		static Python() {
			body = new List<Coord>();
		}

		static int Length
		{
			get { return length; }
			set {
				if (value < 4) {
					return;
				}
				length = value;
			}
		}

		public static Dir Direction
		{
			get { return direction; }
			set {
				if (direction == Dir.UP && value == Dir.DOWN) return;
				if (direction == Dir.DOWN && value == Dir.UP) return;
				if (direction == Dir.LEFT && value == Dir.RIGHT) return;
				if (direction == Dir.RIGHT && value == Dir.LEFT) return;
				direction = value;
			}
		}

		public static Coord Head
		{
			get { return (Coord)body.Last().Clone(); }
		}

		public static List<Coord> Body
		{
			get {
				List<Coord> bodyCoords = new List<Coord>();
				for (int i = 0; i < body.Count - 1; ++i) {
					bodyCoords.Add((Coord)body.ElementAt(i).Clone());
				}
				return bodyCoords;
			}
		}

		public static bool Alive { get; private set; } = false;

		static void AddTail() {
			if (Direction == Dir.UP) {
				body.Add(new Coord(body.Last().X, body.Last().Y - 1));
			}
			else if (Direction == Dir.DOWN) {
				body.Add(new Coord(body.Last().X, body.Last().Y + 1));
			}
			else if (Direction == Dir.LEFT) {
				body.Add(new Coord(body.Last().X - 1, body.Last().Y));
			}
			else if (Direction == Dir.RIGHT) {
				body.Add(new Coord(body.Last().X + 1, body.Last().Y));
			}
		}

		public static void Move() {
			if (Alive) {
				body.RemoveAt(0);
				AddTail();
			}
		}

		public static void Born(int xCoord, int yCoord) {
			Alive = true;
			Length = 4;
			Direction = Dir.RIGHT;
			body.Clear();
			for (int i = 0; i < Length; ++i) {
				body.Add(new Coord(xCoord + i, yCoord));
			}
		}

		public static void Die() {
			Alive = false;
		}

		public static void Eat() {
			++Length;
			AddTail();
		}
	}
}