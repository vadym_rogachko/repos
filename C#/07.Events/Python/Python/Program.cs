﻿using System;

namespace Python
{
	class Program
	{
		static void Main() {

			Game game = new Game(new ConsoleGameField(50, 25), 25);
			game.StartTheGame();
			Console.Clear();
		}
	}
}
