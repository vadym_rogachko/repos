﻿using System;

/*9. Поменять местами соседние слова во введённом
	 предложении*/

namespace _09
{
	class Program
	{
		static void Main(string[] args) {

			Console.WriteLine("Введите предложение");
			string str = Console.ReadLine();
			string[] arr = str.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
			string temp;
			for (int i = 0; i < arr.Length - 1; i += 2) {
				temp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = temp;
			}
			str = string.Join(" ", arr);
			Console.WriteLine($"\nНовое предложение:\n{str}");
			Console.ReadKey(true);
		}
	}
}
