﻿using System;

/*8. Пользователь вводит текст, строку для поиска и строку для
	 замены. Реализовать поиск в тексте заданной строки и
	 замены ее на заданную подстроку*/

namespace _08
{
	class Program
	{
		static void Main(string[] args) {

			Console.WriteLine("Введите текст");
			string text = Console.ReadLine();
			Console.WriteLine("\nВведите строку для поиска");
			string strSearch = Console.ReadLine();
			Console.WriteLine("Введите строку для замены");
			string strReplace = Console.ReadLine();
			text = text.Replace(strSearch, strReplace);
			Console.WriteLine($"\nНовый текст:\n{text}");
			Console.ReadKey(true);
		}
	}
}
