﻿using System;
using System.Linq;

/*3. Дан двумерный массив размерностью 5х5, заполненный
	 случайными числами из диапазона от -100 до 100.
	 Определить сумму элементов массива, расположенных
	 между минимальным и максимальным элементами*/

namespace _03
{
	class Program
	{
		static void Main(string[] args) {

			int row = 5;
			int col = 5;
			int[,] arr = new int[row, col];
			Random rand = new Random();
			for (int i = 0; i < row; ++i) {
				for (int j = 0; j < col; ++j) {
					arr[i, j] = rand.Next(-100, 101);
					Console.Write($"{arr[i, j],5}");
				}
				Console.WriteLine();
			}
			
			int iMin = 0;
			int jMin = 0;
			int iMax = 0;
			int jMax = 0;
			int min = arr[iMin, jMin];
			int max = arr[iMax, jMax];
			for (int i = 0; i < row; ++i) {
				for (int j = 0; j < col; ++j) {
					if (arr[i, j] < min) {
						min = arr[i, j];
						iMin = i;
						jMin = j;
					}
					if (arr[i, j] > max) {
						max = arr[i, j];
						iMax = i;
						jMax = j;
					}
				}
			}
			Console.WriteLine($"\n   Min = {min, 3} row {iMin} col {jMin}");
			Console.WriteLine($"   Max = {max, 3} row {iMax} col {jMax}");
			
			int sum = 0;
			bool isMax = false;
			if (iMin * row + jMin < iMax * row + jMax) {
				for (int i = iMin, j = jMin + 1; !isMax; j = 0, ++i) {
					for (; j < col; ++j) {
						if (arr[i, j] == max) {
							isMax = true;
							break;
						}
						sum += arr[i, j];
					}
				}
			}
			else {
				for (int i = iMin, j = jMin - 1; !isMax; j = col - 1, --i) {
					for (; j >= 0; --j) {
						if (arr[i, j] == max) {
							isMax = true;
							break;
						}
						sum += arr[i, j];
					}
				}
			}
			Console.WriteLine($"\nСумма элементов между min и max = {sum}");
			Console.ReadKey(true);
		}
	}
}
