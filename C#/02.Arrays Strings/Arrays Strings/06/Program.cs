﻿using System;

/*6. Пользователь вводит строку. Проверить, является ли эта
	 строка палиндромом*/

namespace _06
{
	class Program
	{
		static void Main(string[] args) {

			Console.WriteLine("Введите строку");
			string str = Console.ReadLine();
			str = str.ToLower();
			str = str.Replace(" ", "");
			char[] arr = str.ToCharArray();
			Array.Reverse(arr);
			Console.WriteLine(str == new string(arr) ? "палиндром" : "не палиндром");
			Console.ReadKey(true);
		}
	}
}
