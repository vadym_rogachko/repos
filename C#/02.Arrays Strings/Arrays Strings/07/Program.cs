﻿using System;

/*7. Подсчитать количество слов во введенном предложении*/

namespace _07
{
	class Program
	{
		static void Main(string[] args) {

			Console.WriteLine("Введите предложение");
			string str = Console.ReadLine();
			string[] arr = str.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
			Console.WriteLine($"{arr.Length} слов");
			Console.ReadKey(true);
		}
	}
}
