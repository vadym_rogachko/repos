﻿using System;
using System.Linq;

/*2. Дан двумерный массив размерностью N x M, заполненный
	 случайными числами из диапазона от 0 до 100. Выполнить
	 циклический сдвиг массива на заданное количество
	 столбцов. Направление сдвига задаёт пользователь*/

namespace _02
{
	class Program
	{
		static void Main(string[] args) {

			try {
				uint N, M;
				Console.Write("Введите количество строк: ");
				N = Convert.ToUInt32(Console.ReadLine());
				Console.Write("Введите количество столбцов: ");
				M = Convert.ToUInt32(Console.ReadLine());
				Console.Clear();
				Console.WriteLine();

				int[][] arr = new int[N][];
				Random rand = new Random();
				for (int i = 0; i < arr.Length; ++i) {
					arr[i] = new int[M];
					for (int j = 0; j < arr[i].Length; ++j) {
						arr[i][j] = rand.Next(0, 101);
						Console.Write($"{arr[i][j],5}");
					}
					Console.WriteLine();
				}

				uint shift;
				Console.WriteLine("\nВведите количество столбцов для сдвига");
				do {
					shift = Convert.ToUInt32(Console.ReadLine());
				} while (shift < 1 || shift > M);

				string dir;
				Console.WriteLine("Введите направление для сдвига (вправо/влево)");
				do {
					dir = Console.ReadLine();
				} while (dir != "вправо" && dir != "влево");

				if (dir == "вправо") {
					while (shift > 0) {
						for (int i = 0; i < arr.Length; ++i) {
							int last = arr[i].Last();
							Array.Copy(arr[i], 0, arr[i], 1, arr[i].Length - 1);
							arr[i][0] = last;
						}
						--shift;
					}
				}
				else {
					while (shift > 0) {
						for (int i = 0; i < arr.Length; ++i) {
							int first = arr[i].First();
							Array.Copy(arr[i], 1, arr[i], 0, arr[i].Length - 1);
							arr[i][arr[i].GetUpperBound(0)] = first;
						}
						--shift;
					}
				}
				
				Console.WriteLine("\nМассив сдвинут\n");
				for (int i = 0; i < arr.Length; ++i) {
					for (int j = 0; j < arr[i].Length; ++j) {
						Console.Write($"{arr[i][j],5}");
					}
					Console.WriteLine();
				}
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
			}
			Console.ReadKey(true);
		}
	}
}
