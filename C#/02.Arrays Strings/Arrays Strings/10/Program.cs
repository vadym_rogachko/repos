﻿using System;

/*10. Пользователь вводит русский текст. Подсчитать
	  количество слов, которые заканчиваются на гласную букву*/

namespace _10
{
	class Program
	{
		static void Main(string[] args) {

			Console.WriteLine("Введите русский текст");
			string text = Console.ReadLine();
			string[] arr = text.Split(new char[] { ' ', ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
			int counter = 0;
			foreach (string str in arr) {
				switch(str[str.Length - 1]) {
					case 'а': case 'у': case 'о': case 'ы': case 'и': case 'э': case 'я': case 'ю': case 'ё': case 'е':
					case 'А': case 'У': case 'О': case 'Ы': case 'И': case 'Э': case 'Я': case 'Ю': case 'Ё': case 'Е':
						++counter;
						break;
				}
			}
			Console.WriteLine($"Количество слов, оканчивающихся на гласную букву {counter}");
			Console.ReadKey(true);
		}
	}
}
