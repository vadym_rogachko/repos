﻿using System;
using System.Linq;

/*1. Даны 2 массива размерности M и N соответственно.
  	 Необходимо переписать в третий массив общие элементы
	 первых двух массивов без повторений.*/

namespace Arrays_Strings
{
	class Program
	{
		static void Main(string[] args) {

			try {
				uint M, N;
				Console.Write("Введите размер первого массива: ");
				M = Convert.ToUInt32(Console.ReadLine());
				Console.Write("Введите размер второго массива: ");
				N = Convert.ToUInt32(Console.ReadLine());
				Console.Clear();

				int[] arrM = new int[M];
				int[] arrN = new int[N];
				Random rand = new Random();
				Console.WriteLine("\nПервый массив:");
				for (int i = 0; i < arrM.Length; ++i) {
					arrM[i] = rand.Next(1, 10);
					Console.Write($"{arrM[i]} ");
				}
				Console.WriteLine("\n\nВторой массив:");
				for (int i = 0; i < arrN.Length; ++i) {
					arrN[i] = rand.Next(1, 10);
					Console.Write($"{arrN[i]} ");
				}

				int[] arrMN = arrM.Intersect(arrN).ToArray();

				Console.WriteLine("\n\nТретий массив:");
				foreach (var val in arrMN) {
					Console.Write($"{val} ");
				}
				Console.WriteLine();
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
			}
			Console.ReadKey(true);
		}
	}
}
