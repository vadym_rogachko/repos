﻿using System;

/*5. Заполнить квадратную матрицу размером N x N по спирали
 	 (N – нечётное число). Число 1 ставится в центр матрицы, а
	 затем массив заполняется по спирали против часовой
	 стрелки значениями по возрастанию*/

namespace _05
{
	class Program
	{
		static void Main(string[] args) {

			int N = 15;
			int[,] arr = new int[N, N];
			int num = 1;
			int mid = N / 2;
			int loop = 1;
			int left, right, up, down;


			arr[mid, mid] = num++;
			while (loop <= mid) {
				left = mid - loop;
				right = mid + loop;
				up = mid - loop;
				down = mid + loop;
				for (int coord = -(loop - 1); coord <= loop; ++coord) {
					arr[mid + coord, left] = num++;
				}
				for (int coord = -(loop - 1); coord <= loop; ++coord) {
					arr[down, mid + coord] = num++;
				}
				for (int coord = loop - 1; coord >= -loop; --coord) {
					arr[mid + coord, right] = num++;
				}
				for (int coord = loop - 1; coord >= -loop; --coord) {
					arr[up, mid + coord] = num++;
				}
				++loop;
			}

			for (int i = 0; i < N; ++i) {
				for (int j = 0; j < N; ++j) {
					Console.Write($"{arr[i, j],4}");
				}
				Console.WriteLine();
			}
			Console.WriteLine();
			Console.ReadKey(true);
		}
	}
}
