﻿using System;

/*4. Дан двумерный массив размерностью 5х5, заполненный
	 случайными числами из диапазона от 0 до 100.
	 Переформировать массив таким образом, чтобы его столбцы
	 располагались по убыванию их поэлементных сумм*/

namespace _04
{
	class Program
	{
		static void Main(string[] args) {

			int row = 5;
			int col = 5;
			int[,] arr = new int[row, col];
			Random rand = new Random();
			for (int i = 0; i < row; ++i) {
				for (int j = 0; j < col; ++j) {
					arr[i, j] = rand.Next(0, 101);
					Console.Write($"{arr[i, j],5}");
				}
				Console.WriteLine();
			}
			Console.WriteLine();

			int sum;
			for (int i = 0; i < col; ++i) {
				sum = 0;
				for (int j = 0; j < row; ++j) {
					sum += arr[j, i];
				}
				Console.WriteLine($"Сумма элементов столбца {i} = {sum}");
			}

			bool isSwap;
			int m = 0;
			int sumNext;
			do {
				isSwap = false;
				for (int i = 0; i < col - 1; ++i) {
					sum = sumNext = 0;
					for (int j = 0; j < row; ++j) {
						sum += arr[j, i];
						sumNext += arr[j, i + 1];
					}
					if (sum < sumNext) {
						for (int k = 0; k < row; ++k) {
							arr[k, i] ^= arr[k, i + 1];
							arr[k, i + 1] ^= arr[k, i];
							arr[k, i] ^= arr[k, i + 1];
							isSwap = true;
						}
					}
				}
			} while (++m < col && isSwap);

			Console.WriteLine("\nНовый массив\n");
			for (int i = 0; i < row; ++i) {
				for (int j = 0; j < col; ++j) {
					Console.Write($"{arr[i, j],5}");
				}
				Console.WriteLine();
			}
			Console.ReadKey(true);
		}
	}
}
