﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interfaces.Logger;

namespace Interfaces.AcademyGroup
{
	class Student
		: Person, IComparable
	{
		public int CompareTo(object obj) {
			if (obj is Student)
			return Surname.CompareTo((obj as Student).Surname);
			throw new ArgumentException();
		}

		protected double average;
		protected uint numberOfGroup;

		public double Average
		{
			get { return average; }
			set {
				if (value < 1 || value > 12) {
					throw new ArgumentException();
				}
				average = value;
			}
		}

		public uint NumberOfGroup
		{
			get { return numberOfGroup; }
			set { numberOfGroup = value; }
		}
		
		public Student() { }

		public Student(string name, string surname, uint age, string phone, double average, uint numberOfGroup) 
			: base(name, surname, age, phone) {
			Average = average;
			NumberOfGroup = numberOfGroup;
		}

		public override string ToString() {
			return base.ToString() +
				$"\nAverage mark: {average}" +
				$"\nGroup: {numberOfGroup}";
		}

		public new void Print(ILogger logger) {
			logger.Log(this);
		}

		public class ComparerName
			: IComparer<Student>
		{
			public int Compare(Student x, Student y) {
				return x.Name.CompareTo(y.Name);
			}
		}

		public class ComparerSurname
			: IComparer<Student>
		{
			public int Compare(Student x, Student y) {
				return x.Surname.CompareTo(y.Surname);
			}
		}

		public class ComparerAge
			: IComparer<Student>
		{
			public int Compare(Student x, Student y) {
				return x.Age.CompareTo(y.Age);
			}
		}

		public class ComparerPhone
			: IComparer<Student>
		{
			public int Compare(Student x, Student y) {
				return x.Phone.CompareTo(y.Phone);
			}
		}

		public class ComparerAverage
			: IComparer<Student>
		{
			public int Compare(Student x, Student y) {
				return x.Average.CompareTo(y.Average);
			}
		}

		public class ComparerNumberOfGroup
			: IComparer<Student>
		{
			public int Compare(Student x, Student y) {
				return x.NumberOfGroup.CompareTo(y.NumberOfGroup);
			}
		}
	}
}
