﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Interfaces.Logger;

namespace Interfaces.AcademyGroup
{
	class AcademyGroup
		: ICloneable, IEnumerable, IEnumerator
	{

		public object Clone() {
			return MemberwiseClone();
		}

		public IEnumerator GetEnumerator() {
			return this;
		}

		public object Current => list[pos];

		public bool MoveNext() {
			if (pos < list.Count - 1) {
				++pos;
				return true;
			}
			Reset();
			return false;
		}

		public void Reset() {
			pos = -1;
		}

		List<Student> list;
		int pos;
		
		public AcademyGroup() {
			list = new List<Student>();
			pos = -1;
		}

		enum Choice
		{
			Name,
			Surname,
			Age,
			Phone,
			Average,
			NumberOgGroup
		}

		static class Interface
		{
			public static string GetChoice() {
				Console.WriteLine("Criterion:");
				Console.WriteLine("1.Name");
				Console.WriteLine("2.Surname");
				Console.WriteLine("3.Age");
				Console.WriteLine("4.Phone");
				Console.WriteLine("5.Average mark");
				Console.WriteLine("6.Number of group");
				char ch;
				do {
					ch = Console.ReadKey(true).KeyChar;
				} while (ch < '1' || ch > '6');
				return Enum.GetName(typeof(Choice), ch - ConsoleKey.D1);
			}
		}

		public void Add() {
			Console.Clear();
			Student temp = new Student();
			Console.Write("Name: ");
			temp.Name = Console.ReadLine();
			Console.Write("Surname: ");
			temp.Surname = Console.ReadLine();
			Console.Write("Age: ");
			temp.Age = Convert.ToUInt32(Console.ReadLine());
			Console.Write("Phone: ");
			temp.Phone = Console.ReadLine();
			Console.Write("Avearage mark: ");
			temp.Average = Convert.ToDouble(Console.ReadLine());
			Console.Write("Number of group: ");
			temp.NumberOfGroup = Convert.ToUInt32(Console.ReadLine());
			list.Add(temp);
		}

		public void Remove() {
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			Console.Clear();
			Student temp = new Student();
			Console.Write("Surname: ");
			temp.Surname = Console.ReadLine();
			if (!list.Remove(list.Find(x => x.CompareTo(temp) == 0))) {
				throw new ApplicationException("Not found");
			}
		}

		public void Edit() {
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			Console.Clear();
			Student temp = new Student();
			Console.Write("Surname: ");
			temp.Surname = Console.ReadLine();
			int index = list.FindIndex(x => x.CompareTo(temp) == 0);
			if (index != -1) {
				Console.Clear();
				Console.Write("Name: ");
				list[index].Name = Console.ReadLine();
				Console.Write("Surname: ");
				list[index].Surname = Console.ReadLine();
				Console.Write("Age: ");
				list[index].Age = Convert.ToUInt32(Console.ReadLine());
				Console.Write("Phone: ");
				list[index].Phone = Console.ReadLine();
				Console.Write("Average mark: ");
				list[index].Average = Convert.ToDouble(Console.ReadLine());
				Console.Write("Number of group: ");
				list[index].NumberOfGroup = Convert.ToUInt32(Console.ReadLine());
			}
			else throw new ApplicationException("Not found");
		}

		public void Print() {
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			Console.Clear();
			Console.WriteLine(
				"Print:\n" +
				"1.On the screen\n" +
				"2.In the file\n"
				);
			char ch;
			do {
				ch = Console.ReadKey(true).KeyChar;
			} while (ch != '1' && ch != '2');
			Console.Clear();
			ILogger logger;
			if (ch == '1') {
				logger = new ConsoleLogger();
			}
			else {
				logger = new FileLogger();
			}
			foreach (var student in this) {
				(student as Student)?.Print(logger);
				Console.WriteLine("\n");
			}
			Console.ReadKey(true);
		}

		public void Sort() {
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			Console.Clear();
			switch (Enum.Parse(typeof(Choice), Interface.GetChoice())) {
				case Choice.Name:
					list.Sort(new Student.ComparerName());
					break;
				case Choice.Surname:
					list.Sort(new Student.ComparerSurname());
					break;
				case Choice.Age:
					list.Sort(new Student.ComparerAge());
					break;
				case Choice.Phone:
					list.Sort(new Student.ComparerPhone());
					break;
				case Choice.Average:
					list.Sort(new Student.ComparerAverage());
					break;
				case Choice.NumberOgGroup:
					list.Sort(new Student.ComparerNumberOfGroup());
					break;
			}
		}

		public AcademyGroup Copy() {
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			return Clone() as AcademyGroup;
		}

		public void Save(string path) {
			if (list.Count == 0) return;
			FileStream file = null;
			BinaryWriter writer = null;
			try {
				file = new FileStream(path, FileMode.Create, FileAccess.Write);
				writer = new BinaryWriter(file);
				foreach (var student in this) {
					writer.Write((student as Student).Name);
					writer.Write((student as Student).Surname);
					writer.Write((student as Student).Age);
					writer.Write((student as Student).Phone);
					writer.Write((student as Student).Average);
					writer.Write((student as Student).NumberOfGroup);
				}
			}
			finally {
				writer?.Close();
				file?.Close();
			}

		}

		public void Load(string path) {
			FileStream file = null;
			BinaryReader reader = null;
			try {
				file = new FileStream(path, FileMode.Open, FileAccess.Read);
				reader = new BinaryReader(file);
				Student temp;
				while (reader.PeekChar() != -1) {
					temp = new Student() {
						Name = reader.ReadString(),
						Surname = reader.ReadString(),
						Age = reader.ReadUInt32(),
						Phone = reader.ReadString(),
						Average = reader.ReadDouble(),
						NumberOfGroup = reader.ReadUInt32()
					};
					list.Add(temp);
				}
			}
			catch (FileNotFoundException) { }
			finally {
				reader?.Close();
				file?.Close();
			}
		}

		public Student Search() {
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			Console.Clear();
			Student temp = new Student();
			IComparer<Student> comparer;
			switch ((Choice)Enum.Parse(typeof(Choice), Interface.GetChoice())) {
				case Choice.Name:
					Console.Write("Name: ");
					temp.Name = Console.ReadLine();
					comparer = new Student.ComparerName();
					break;
				case Choice.Surname:
					Console.Write("Surname: ");
					temp.Surname = Console.ReadLine();
					comparer = new Student.ComparerSurname();
					break;
				case Choice.Age:
					Console.Write("Age: ");
					temp.Age = Convert.ToUInt32(Console.ReadLine());
					comparer = new Student.ComparerAge();
					break;
				case Choice.Phone:
					Console.Write("Phone: ");
					temp.Phone = Console.ReadLine();
					comparer = new Student.ComparerPhone();
					break;
				case Choice.Average:
					Console.Write("Average mark: ");
					temp.Average = Convert.ToDouble(Console.ReadLine());
					comparer = new Student.ComparerAverage();
					break;
				case Choice.NumberOgGroup:
					Console.Write("Number of group: ");
					temp.NumberOfGroup = Convert.ToUInt32(Console.ReadLine());
					comparer = new Student.ComparerNumberOfGroup();
					break;
				default: throw new ArgumentException();
			}
			Console.Clear();
			for (int i = 0; i < list.Count; ++i) {
				if (comparer?.Compare(list[i] as Student, temp) == 0) {
					return list[i] as Student;
				}
			}
			throw new ApplicationException("Not found");
		}
	}
}
