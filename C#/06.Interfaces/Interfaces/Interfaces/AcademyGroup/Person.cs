﻿using System;
using Interfaces.Logger;

namespace Interfaces.AcademyGroup
{
	class Person
	{
		protected string name;
		protected string surname;
		protected uint age;
		protected string phone;

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public string Surname
		{
			get { return surname; }
			set { surname = value; }
		}

		public uint Age
		{
			get { return age; }
			set {
				if (value > 125) {
					throw new ArgumentException();
				}
				age = value;
			}
		}

		public string Phone
		{
			get { return phone; }
			set { phone = value; }
		}
		
		public Person() { }

		public Person(string name, string surname, uint age, string phone) {
			Name = name;
			Surname = surname;
			Age = age;
			Phone = phone;
		}

		public override string ToString() {
			return
				$"Name: {name}\n" +
				$"Surname: {surname}\n" +
				$"Age: {age}\n" +
				$"Phone: {phone}";
		}

		public void Print(ILogger logger) {
			logger.Log(this);
		}
	}
}
