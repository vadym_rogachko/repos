﻿using System;

namespace Interfaces.Interface
{
	static class Interface
	{

		public enum Choice
		{
			Add = ConsoleKey.D1,
			Remove,
			Edit,
			Print,
			Sort,
			Search,
			Exit = ConsoleKey.Escape,
			NoChoice = 0
		}

		static Choice choice;

		public static void ShowMenu() {
			Console.Clear();
			Console.WriteLine("Academy group");
			var choice = Enum.GetValues(typeof(Choice));
			for (int i = 2; i < choice.Length; ++i) {
				Console.WriteLine($"{(int)choice.GetValue(i) - ConsoleKey.D1 + 1}.{choice.GetValue(i)}");
			}
		}

		public static Choice UserChoice {
			get {
				do {
					try {
						choice = (Choice)Console.ReadKey(true).KeyChar;
					}
					catch (ArgumentException e) {
						Console.WriteLine(e.Message);
					}
				} while (choice != Choice.Exit && (choice < Choice.Add || choice > Choice.Search));
				return choice;
			}
		}

		public static bool IsNoExit {
			get { return choice != Choice.Exit; }
		}


	}
}
