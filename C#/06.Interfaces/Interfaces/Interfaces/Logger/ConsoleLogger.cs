﻿using System;

namespace Interfaces.Logger
{
	class ConsoleLogger
		: ILogger
	{
		public void Log(object obj) {
			Console.Write(obj);
		}
	}
}
