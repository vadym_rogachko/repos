﻿using System;
using System.IO;

namespace Interfaces.Logger
{
	class FileLogger
		: ILogger
	{
		public void Log(object obj) {
			StreamWriter writer = null;
			try {
				writer = new StreamWriter("AcademyGroup.log", true);
				writer.WriteLine(obj);
				writer.WriteLine();
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
			finally {
				writer?.Close();
			}
		}
	}
}
