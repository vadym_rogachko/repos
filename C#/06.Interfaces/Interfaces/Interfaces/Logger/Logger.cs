﻿
namespace Interfaces.Logger
{
	interface ILogger
	{
		void Log(object obj);
	}
}