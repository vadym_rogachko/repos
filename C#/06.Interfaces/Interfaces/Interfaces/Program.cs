﻿using System;
using Interfaces.Logger;

namespace Interfaces
{
	class Program
	{
		static void Main() {

			AcademyGroup.AcademyGroup group = new AcademyGroup.AcademyGroup();
			group.Load("AcademyGroup.bin");
			while (Interface.Interface.IsNoExit) {
				Console.Clear();
				Interface.Interface.ShowMenu();
				try {
					switch (Interface.Interface.UserChoice) {
						case Interface.Interface.Choice.Add:
							group.Add();
							break;
						case Interface.Interface.Choice.Remove:
							group.Remove();
							break;
						case Interface.Interface.Choice.Edit:
							group.Edit();
							break;
						case Interface.Interface.Choice.Print:
							group.Print();
							break;
						case Interface.Interface.Choice.Sort:
							group.Sort();
							break;
						case Interface.Interface.Choice.Search:
							Console.Clear();
							Console.WriteLine(
								"Print:\n" +
								"1.On the screen\n" +
								"2.In the file\n"
								);
							char ch;
							do {
								ch = Console.ReadKey(true).KeyChar;
							} while (ch != '1' && ch != '2');
							if (ch == '1') {
								group.Search().Print(new ConsoleLogger());
							}
							else {
								group.Search().Print(new FileLogger());
							}
							Console.WriteLine();
							Console.ReadKey(true);
							break;
					}
				}
				catch (ArgumentException) { }
				catch (Exception e) {
					Console.Clear();
					Console.WriteLine(e.Message);
					Console.ReadKey(true);
				}
			}
			group.Save("AcademyGroup.bin");
		}
	}
}
