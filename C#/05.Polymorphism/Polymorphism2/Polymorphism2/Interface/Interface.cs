﻿using System;

namespace Polymorphism2 {
    static class Interface {

		public enum Choice
		{
			Add = ConsoleKey.D1,
			Delete,
			Print,
			Edit,
			Find,
			Exit = ConsoleKey.Escape,
			NoChoice = 0
		}

        static Choice choice;

		public static void ShowMenu() {
			Console.Clear();
			Console.WriteLine("Data Storage");
			var choice = Enum.GetValues(typeof(Choice));
			for (int i = 2; i < choice.Length; ++i) {
				Console.WriteLine($"{(int)choice.GetValue(i) - ConsoleKey.D1 + 1}.{choice.GetValue(i)}");
			}
		}
	
        public static Choice GetChoice() {
            do {
                try {
					choice = (Choice)Console.ReadKey(true).KeyChar;
                }
                catch (ArgumentException e) {
                    Console.WriteLine(e.Message);
                }
            } while (choice != Choice.Exit && (choice < Choice.Add || choice > Choice.Find));
            return choice;
        }

        public static bool IsNoExit() {
            return choice != Choice.Exit;
        }


    }
}
