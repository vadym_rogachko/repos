﻿using System;

namespace Polymorphism2 {
    class Program {
        static void Main() {

			ListDataStorage list = new ListDataStorage();
			try {
				list.Load("ListDataStorage.bin");
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
			while (Interface.IsNoExit()) {
				Interface.ShowMenu();
                try {
                    switch (Interface.GetChoice()) {
                        case Interface.Choice.Add:
                            list.Add();
                            break;
                        case Interface.Choice.Delete:
                            list.Delete();
                            break;
                        case Interface.Choice.Print:
                            list.Print();
                            break;
                        case Interface.Choice.Edit:
                            list.Edit();
                            break;
                        case Interface.Choice.Find:
                            list.Find();
                            Console.ReadKey(true);
                            break;
                    }
                }
                catch (ApplicationException e) {
                    Console.WriteLine(e.Message);
                    Console.ReadKey();
                    Console.Clear();
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    Console.ReadKey(true);
                }
			}
			try {
				list.Save("ListDataStorage.bin");
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
        }
    }
}
