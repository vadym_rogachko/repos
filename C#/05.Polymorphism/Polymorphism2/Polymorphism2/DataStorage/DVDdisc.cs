﻿using System;
using System.IO;

namespace Polymorphism2 {
    class DVDdisc 
		: DataStorage {

		public new enum Criterion
		{
			Manufacturer = 1,
			Model,
			Volume,
			Count,
			Speed
		}

		public uint SpeedRec { get; set; }

        public DVDdisc(string manufacturer = "", string model = "", uint count = 0, double volume = 0, uint speedRec = 0) 
			: base(manufacturer, model, count, volume) {
            Name = GetType().Name;
            SpeedRec = speedRec;
        }

        public override void Print(ILogger logger) {
            base.Print(logger);
			logger.Log(string.Format($"{SpeedRec,10}"));
			logger.Log("\n");
		}

		public override void Save(FileStream stream) {
			base.Save(stream);
			BinaryWriter writer;
			try {
				writer = new BinaryWriter(stream);
				writer.Write(SpeedRec);
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
        }

        public override void Load(FileStream stream) {
            base.Load(stream);
			BinaryReader reader = new BinaryReader(stream);
			SpeedRec = reader.ReadUInt32();
        }

		public override bool Equals(object obj, DataStorage.Criterion criterion) {
			if (base.Equals(obj, criterion)) return true;
			if (criterion == DataStorage.Criterion.Speed) {
				return SpeedRec == (obj as DVDdisc)?.SpeedRec;
			}
			return false;
		}
	}
}
