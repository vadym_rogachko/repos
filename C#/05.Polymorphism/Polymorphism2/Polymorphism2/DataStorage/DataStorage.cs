﻿using System;
using System.IO;

namespace Polymorphism2 {
    abstract class DataStorage {

		public enum Criterion
		{
			Manufacturer = 1,
			Model,
			Volume,
			Count,
			Speed,
			Standart
		}

		double volume;

        public string Name { get; protected set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public uint Count { get; set; }
        public double Volume {
            get { return volume; }
            set {
                volume = Math.Abs(value);
            }
        }

        public DataStorage(string manufacturer = "", string model = "", uint count = 0, double volume = 0) {
            Manufacturer = manufacturer;
            Model = model;
            Count = count;
            Volume = volume;
        }


        public virtual void Print(ILogger logger) {
			logger.Log(string.Format($"{Manufacturer,10}"));
			logger.Log(string.Format($"{Model,10}"));
			logger.Log(string.Format($"{Volume,10}"));
			logger.Log(string.Format($"{Count,10}"));
        }

		public virtual void Save(FileStream file) {
			BinaryWriter writer;
			try {
				writer = new BinaryWriter(file);
				writer.Write(Name);
				writer.Write(Manufacturer);
				writer.Write(Model);
				writer.Write(Volume);
				writer.Write(Count);
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
        }

        public virtual void Load(FileStream stream) {
			BinaryReader reader = new BinaryReader(stream);
			Manufacturer = reader.ReadString();
			Model = reader.ReadString();
			Volume = reader.ReadDouble();
			Count = reader.ReadUInt32();
        }

		public virtual bool Equals(object obj, Criterion criterion) {
			switch (criterion) {
				case Criterion.Manufacturer:
					return Manufacturer == (obj as DataStorage)?.Manufacturer;
				case Criterion.Model:
					return Model == (obj as DataStorage)?.Model;
				case Criterion.Volume:
					return Volume == (obj as DataStorage)?.Volume;
				case Criterion.Count:
				return Count == (obj as DataStorage)?.Count;
				default: return false;
			}
		}
	}
}
