﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Polymorphism2 {
	class ListDataStorage {

		enum Storage {
			USB = 1,
			HDD,
			DVD
		}
		
		class Interface {
			public static string GetChoice(Type enumType) {
				Console.Clear();
				foreach (var val in Enum.GetValues(enumType)) {
					Console.WriteLine($"{(int)val}.{val}");
				}
				int choice;
				if (!int.TryParse(Console.ReadLine(), out choice)) {
					return null;
				}
				Console.Clear();
				return Enum.GetName(enumType, choice);
			}

			public static void EnterData(DataStorage storage) {
				if (storage == null) throw new ApplicationException("Data storage didn't selected");
				Console.Write("Manufacturer: ");
				storage.Manufacturer = Console.ReadLine();
				Console.Write("\nModel: ");
				storage.Model = Console.ReadLine();
				try {
					Console.Write("\nVolume: ");
					storage.Volume = Convert.ToDouble(Console.ReadLine());
					Console.Write("\nCount: ");
					storage.Count = Convert.ToUInt32(Console.ReadLine());
					if (storage is USBFlash) {
						Console.Write("\nUSB speed: ");
						(storage as USBFlash).SpeedUSB = Convert.ToUInt32(Console.ReadLine());
						Console.Write("\nUSB standart: ");
						(storage as USBFlash).Standart = Console.ReadLine();
					}
					else if (storage is HDDPortable) {
						Console.Write("\nRotation speed: ");
						(storage as HDDPortable).SpeedRotation = Convert.ToUInt32(Console.ReadLine());
					}
					else if (storage is DVDdisc) {
						Console.Write("\nRecording speed: ");
						(storage as DVDdisc).SpeedRec = Convert.ToUInt32(Console.ReadLine());
					}
				}
				catch (Exception e) {
					Console.WriteLine(e.Message);
					Console.ReadKey(true);
				}
			}

			public static void PrintHeader(Type enumType, ILogger logger) {
				Console.Clear();
				foreach (var val in Enum.GetValues(enumType)) {
					logger.Log($"{val,10}");
				}
				logger.Log("\n");

			}
		}

		List<DataStorage> list;

        public ListDataStorage() {
            list = new List<DataStorage>();
        }

		protected DataStorage StorageFactory() {
			switch (Enum.Parse(typeof(Storage), Interface.GetChoice(typeof(Storage)))) {
				case Storage.USB:
					return new USBFlash();
				case Storage.HDD:
					return new HDDPortable();
				case Storage.DVD:
					return new DVDdisc();
				default: return null;
			}
		}

		public void Add() {
			DataStorage temp = StorageFactory();
			Interface.EnterData(temp);
			list.Add(temp);
			Console.Clear();
			Console.WriteLine("\nData storage added");
			Console.ReadKey(true);
		}

		public void Delete() {
			Console.Clear();
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			int index = Find();
			if (index == -1) {
				Console.Clear();
				Console.WriteLine("Not found");
				Console.ReadKey(true);
				return;
			}
			Console.WriteLine("\nDelete? (Y/N)");
			char ch;
			do {
				ch = char.ToUpper(Console.ReadKey(true).KeyChar);
			} while (ch != 'Y' && ch != 'N');
			if (ch == 'Y') {
				list.RemoveAt(index);
				Console.WriteLine("Data storage deleted\n");
				Console.ReadKey(true);
			}
		}

		public void Print() {
			Console.Clear();
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			Console.WriteLine(
				"Печать:\n" +
				"1.На экран\n" +
				"2.В файл\n"
				);
			char key;
			do {
				key = Console.ReadKey(true).KeyChar;
			} while (key != '1' && key != '2');
			switch (Enum.Parse(typeof(Storage), Interface.GetChoice(typeof(Storage)))) {
				case Storage.USB:
					if (key == '1') {
						Interface.PrintHeader(typeof(USBFlash.Criterion), new ConsoleLogger());
					}
					else {
						Interface.PrintHeader(typeof(USBFlash.Criterion), new FileLogger());
					}
					foreach (var storage in list) {
						if (key == '1') {
							(storage as USBFlash)?.Print(new ConsoleLogger());
						}
						else {
							(storage as USBFlash)?.Print(new FileLogger());
						}
						Console.WriteLine();
					}
					break;
				case Storage.HDD:
					if (key == '1') {
						Interface.PrintHeader(typeof(HDDPortable.Criterion), new ConsoleLogger());
					}
					else {
						Interface.PrintHeader(typeof(HDDPortable.Criterion), new FileLogger());
					}
					foreach (var storage in list) {
						if (key == '1') {
							(storage as HDDPortable)?.Print(new ConsoleLogger());
						}
						else {
							(storage as HDDPortable)?.Print(new FileLogger());
						}
					}
					break;
				case Storage.DVD:
					if (key == '1') {
						Interface.PrintHeader(typeof(DVDdisc.Criterion), new ConsoleLogger());
					}
					else {
						Interface.PrintHeader(typeof(DVDdisc.Criterion), new FileLogger());
					}
					foreach (var storage in list) {
						if (key == '1') {
							(storage as DVDdisc)?.Print(new ConsoleLogger());
						}
						else {
							(storage as DVDdisc)?.Print(new FileLogger());
						}
					}
					break;
			}
			Console.ReadKey(true);
		}

		public void Edit() {
			Console.Clear();
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			int index = Find();
			if (index == -1) {
				Console.Clear();
				Console.WriteLine("Not found");
				Console.ReadKey(true);
				return;
			}
			Console.WriteLine("\nEdit all? (Y/N)");
			char ch;
			do {
				ch = char.ToUpper(Console.ReadKey(true).KeyChar);
			} while (ch != 'Y' && ch != 'N');
			if (ch == 'Y') {
				Console.Clear();
				Console.WriteLine("Edit:\n");
				Interface.EnterData(list.ElementAt(index));
				return;
			}
			while (true) {
				list.ElementAt(index).Print(new ConsoleLogger());
				string choice = null;
				object choiceParsed = null;
				try {
					if (list.ElementAt(index) is USBFlash) {
						choice = Interface.GetChoice(typeof(USBFlash.Criterion));
						choiceParsed = Enum.Parse(typeof(USBFlash.Criterion), choice);
					}
					else if (list.ElementAt(index) is HDDPortable) {
						choice = Interface.GetChoice(typeof(HDDPortable.Criterion));
						choiceParsed = Enum.Parse(typeof(USBFlash.Criterion), choice);
					}
					else if (list.ElementAt(index) is DVDdisc) {
						choice = Interface.GetChoice(typeof(DVDdisc.Criterion));
						choiceParsed = Enum.Parse(typeof(USBFlash.Criterion), choice);
					}
				}
				catch (ArgumentException) {
					return;
				}
				Console.WriteLine($"Enter the key({choice}):");
				try {
					switch (choiceParsed) {
						case USBFlash.Criterion.Manufacturer:
						case HDDPortable.Criterion.Manufacturer:
						case DVDdisc.Criterion.Manufacturer:
							list.ElementAt(index).Manufacturer = Console.ReadLine();
							break;
						case USBFlash.Criterion.Model:
						case HDDPortable.Criterion.Model:
						case DVDdisc.Criterion.Model:
							list.ElementAt(index).Model = Console.ReadLine();
							break;
						case USBFlash.Criterion.Volume:
						case HDDPortable.Criterion.Volume:
						case DVDdisc.Criterion.Volume:
							list.ElementAt(index).Volume = Convert.ToDouble(Console.ReadLine());
							break;
						case USBFlash.Criterion.Count:
						case HDDPortable.Criterion.Count:
						case DVDdisc.Criterion.Count:
							list.ElementAt(index).Count = Convert.ToUInt32(Console.ReadLine());
							break;
						case USBFlash.Criterion.Speed:
							(list.ElementAt(index) as USBFlash).SpeedUSB = Convert.ToUInt32(Console.ReadLine());
							break;
						case HDDPortable.Criterion.Speed:
							(list.ElementAt(index) as HDDPortable).SpeedRotation = Convert.ToUInt32(Console.ReadLine());
							break;
						case DVDdisc.Criterion.Speed:
							(list.ElementAt(index) as DVDdisc).SpeedRec = Convert.ToUInt32(Console.ReadLine());
							break;
						case USBFlash.Criterion.Standart:
							(list.ElementAt(index) as USBFlash).Standart = Console.ReadLine();
							break;
						default: return;
					}
				}
				catch (FormatException e) {
					Console.WriteLine(e.Message);
					Console.ReadKey(true);
				}
			}
		}

		public int Find() {
			Console.Clear();
			if (list.Count == 0) {
				throw new ApplicationException("List is empty");
			}
			DataStorage temp = StorageFactory();
			string choice = null;
			object choiceParsed = null;
			if (temp is USBFlash) {
				choice = Interface.GetChoice(typeof(USBFlash.Criterion));
				choiceParsed = Enum.Parse(typeof(USBFlash.Criterion), choice);
			}
			else if (temp is HDDPortable) {
				choice = Interface.GetChoice(typeof(HDDPortable.Criterion));
				choiceParsed = Enum.Parse(typeof(HDDPortable.Criterion), choice);

			}
			else if (temp is DVDdisc) {
				choice = Interface.GetChoice(typeof(DVDdisc.Criterion));
				choiceParsed = Enum.Parse(typeof(DVDdisc.Criterion), choice);
			}
			Console.WriteLine($"Enter the key({choice}):");
			int index = -1;
			try {
				switch (choiceParsed) {
					case USBFlash.Criterion.Manufacturer:
					case HDDPortable.Criterion.Manufacturer:
					case DVDdisc.Criterion.Manufacturer:
						temp.Manufacturer = Console.ReadLine();
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Manufacturer));
						break;
					case USBFlash.Criterion.Model:
					case HDDPortable.Criterion.Model:
					case DVDdisc.Criterion.Model:
						temp.Model = Console.ReadLine();
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Model));
						break;
					case USBFlash.Criterion.Volume:
					case HDDPortable.Criterion.Volume:
					case DVDdisc.Criterion.Volume:
						temp.Volume = Convert.ToDouble(Console.ReadLine());
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Volume));
						break;
					case USBFlash.Criterion.Count:
					case HDDPortable.Criterion.Count:
					case DVDdisc.Criterion.Count:
						temp.Count = Convert.ToUInt32(Console.ReadLine());
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Count));
						break;
					case USBFlash.Criterion.Speed:
						(temp as USBFlash).SpeedUSB = Convert.ToUInt32(Console.ReadLine());
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Speed));
						break;
					case HDDPortable.Criterion.Speed:
						(temp as HDDPortable).SpeedRotation = Convert.ToUInt32(Console.ReadLine());
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Speed));
						break;
					case DVDdisc.Criterion.Speed:
						(temp as DVDdisc).SpeedRec = Convert.ToUInt32(Console.ReadLine());
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Speed));
						break;
					case USBFlash.Criterion.Standart:
						(temp as USBFlash).Standart = Console.ReadLine();
						index = list.FindIndex(x => x.Equals(temp, DataStorage.Criterion.Standart));
						break;
					default: break;
				}
			}
			catch (FormatException e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
			if (index == -1) {
				Console.Clear();
				Console.WriteLine("Not found\n");
				return index;
			}
			Console.WriteLine(
				"Печать:\n" +
				"1.На экран\n" +
				"2.В файл\n"
				);
			char key;
			do {
				key = Console.ReadKey(true).KeyChar;
			} while (key != '1' && key != '2');
			if (temp is USBFlash) {
				if (key == '1') {
					Interface.PrintHeader(typeof(USBFlash.Criterion), new ConsoleLogger());
				}
				else {
					Interface.PrintHeader(typeof(USBFlash.Criterion), new FileLogger());
				}
			}
			else if (temp is HDDPortable) {
				if (key == '1') {
					Interface.PrintHeader(typeof(HDDPortable.Criterion), new ConsoleLogger());
				}
				else {
					Interface.PrintHeader(typeof(HDDPortable.Criterion), new FileLogger());
				}
			}
			else if (temp is DVDdisc) {
				if (key == '1') {
					Interface.PrintHeader(typeof(DVDdisc.Criterion), new ConsoleLogger());
				}
				else {
					Interface.PrintHeader(typeof(DVDdisc.Criterion), new FileLogger());
				}
			}
			if (key == '1') {
				list.ElementAt(index).Print(new ConsoleLogger());
			}
			else {
				list.ElementAt(index).Print(new FileLogger());
			}
			return index;
		}

		public void Save(string path) {
            if (list.Count == 0) return;
			FileStream file = new FileStream(path, FileMode.Create, FileAccess.Write);
			foreach (var storage in list) {
				storage.Save(file);
			}
			file.Close();
		}

		public void Load(string path) {
			DataStorage temp = null;
            FileStream file = null;
            BinaryReader reader = null;
            try {
                file = new FileStream(path, FileMode.Open, FileAccess.Read);
                reader = new BinaryReader(file);
            }
            catch (FileNotFoundException) {
                return;
            }
			string name = null;
			while (reader.PeekChar() != -1) {
				name = reader.ReadString();
				if (name == typeof(USBFlash).Name) {
					temp = new USBFlash();
				}
				else if (name == typeof(HDDPortable).Name) {
					temp = new HDDPortable();
				}
				else if (name == typeof(DVDdisc).Name) {
					temp = new DVDdisc();
				}
				temp.Load(file);
				list.Add(temp);
			}
			file.Close();
            reader.Close();
		}
	}
}