﻿using System;
using System.IO;

namespace Polymorphism2 {
    class USBFlash 
		: DataStorage {

		public new enum Criterion
		{
			Manufacturer = 1,
			Model,
			Volume,
			Count,
			Speed,
			Standart
		}

		public uint SpeedUSB { get; set; }
        public string Standart { get; set; }

        public USBFlash(string manufacturer = "", string model = "", uint count = 0, double volume = 0, uint speedUSB = 0, string standart = "")
			: base(manufacturer, model, count, volume) {
            Name = GetType().Name;
            SpeedUSB = speedUSB;
            Standart = standart;
        }


        public override void Print(ILogger logger) {
            base.Print(logger);
			logger.Log(string.Format($"{SpeedUSB,10}"));
			logger.Log(string.Format($"{Standart,10}"));
			logger.Log("\n");
		}

		public override void Save(FileStream stream) {
            base.Save(stream);
			BinaryWriter writer;
			try {
				writer = new BinaryWriter(stream);
				writer.Write(SpeedUSB);
				writer.Write(Standart);
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
		}

        public override void Load(FileStream stream) {
            base.Load(stream);
			BinaryReader reader = new BinaryReader(stream);
			SpeedUSB = reader.ReadUInt32();
			Standart = reader.ReadString();
		}

		public override bool Equals(object obj, DataStorage.Criterion criterion) {
			if (base.Equals(obj, criterion)) return true;
			if (criterion == DataStorage.Criterion.Speed) {
				return SpeedUSB == (obj as USBFlash)?.SpeedUSB;
			}
			if (criterion == DataStorage.Criterion.Standart) {
				return Standart == (obj as USBFlash)?.Standart;
			}
			return false;
		}
	}
}
