﻿using System;
using System.IO;

namespace Polymorphism2 {
    class HDDPortable 
		: DataStorage {

		public new enum Criterion
		{
			Manufacturer = 1,
			Model,
			Volume,
			Count,
			Speed
		}

		public uint SpeedRotation { get; set; }

        public HDDPortable(string manufacturer = "", string model = "", uint count = 0, double volume = 0, uint speedRotation = 0) 
			: base(manufacturer, model, count, volume) {
            Name = GetType().Name;
            SpeedRotation = speedRotation;
        }


        public override void Print(ILogger logger) {
            base.Print(logger);
			logger.Log(string.Format($"{SpeedRotation,10}"));
			logger.Log("\n");
		}

		public override void Save(FileStream stream) {
            base.Save(stream);
			BinaryWriter writer;
			try {
				writer = new BinaryWriter(stream);
				writer.Write(SpeedRotation);
			}
			catch (Exception e) {
				Console.WriteLine(e.Message);
				Console.ReadKey(true);
			}
		}

        public override void Load(FileStream stream) {
            base.Load(stream);
			BinaryReader reader = new BinaryReader(stream);
			SpeedRotation = reader.ReadUInt32();
		}

		public override bool Equals(object obj, DataStorage.Criterion criterion) {
			if (base.Equals(obj, criterion)) return true;
			if (criterion == DataStorage.Criterion.Speed) {
				return SpeedRotation == (obj as HDDPortable)?.SpeedRotation;
			}
			return false;
		}
	}
}
