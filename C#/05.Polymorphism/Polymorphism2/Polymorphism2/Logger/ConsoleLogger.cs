﻿using System;

namespace Polymorphism2 {
    class ConsoleLogger 
		: ILogger {

        public void Log(string str) {
			Console.Write(str);
		}
	}
}
