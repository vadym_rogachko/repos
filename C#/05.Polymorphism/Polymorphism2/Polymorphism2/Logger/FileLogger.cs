﻿using System;
using System.IO;

namespace Polymorphism2 {
    class FileLogger
		: ILogger {

		public void Log(string str) {
			StreamWriter writer = null;
            try {
                writer = new StreamWriter("log.txt", true);
				writer.Write(str);
				writer.Write(writer.NewLine);
			}
			catch (Exception e) {
                Console.WriteLine(e.Message);
                Console.ReadKey(true);
            }
            finally {
				writer?.Close();
            }
        }
    }
}
